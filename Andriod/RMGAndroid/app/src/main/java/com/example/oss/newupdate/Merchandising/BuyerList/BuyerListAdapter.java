package com.example.oss.newupdate.Merchandising.BuyerList;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.oss.newupdate.Model.BuyerOrderListModel;
import com.example.oss.newupdate.Model.BuyerOrderModel;
import com.example.oss.newupdate.R;

import java.util.ArrayList;

/**
 * Created by Adnan on 3/17/2016.
 */
public class BuyerListAdapter extends ArrayAdapter<BuyerOrderListModel> {
    Context mContext;
    int mLayoutResourceId;
    private ArrayList<BuyerOrderListModel> mData=null;

    public BuyerListAdapter(Context context, int resource, BuyerOrderModel data) {
        super(context, resource, data.getBuyerOrder());
        this.mContext=context;
        this.mLayoutResourceId=resource;
        this.mData=data.getBuyerOrder();
    }

    @Override
    public BuyerOrderListModel getItem(int position)
    {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row=convertView;
        PlaceHolder holder=null;
        if(row==null)
        {
            //Create a new view
            LayoutInflater inflater = LayoutInflater.from(mContext);
            row=inflater.inflate(mLayoutResourceId,parent,false);
            holder=new PlaceHolder();
            holder.ID=(TextView) row.findViewById(R.id.textBOListId);
            holder.Item=(TextView) row.findViewById(R.id.textBOListItem);
            row.setTag(holder);
        }
        else{
            holder= (PlaceHolder) row.getTag();
        }
        //Getting the data from the data array
        BuyerOrderListModel place=mData.get(position);
        //Setup and reuse the same listener for each buyer_order_row
        //holder.imageView.setOnClickListener(PopupListener);
        Integer rowPosition=position;
        //holder.imageView.setTag(rowPosition);
        //Setting the view to reflect the data we need to display
        holder.ID.setText(place.getID());
        holder.Item.setText(String.valueOf(place.getItem()));
        //for getting the image
        /*int resId=mContext.getResources().getIdentifier(place.mNameOfImage,"drawable",mContext.getPackageName());
        holder.imageView.setImageResource(resId);*/
        //Returning the buyer_order_row
        return row;
    }

    private static class PlaceHolder{
        TextView ID;
        TextView Item;
    }
}
