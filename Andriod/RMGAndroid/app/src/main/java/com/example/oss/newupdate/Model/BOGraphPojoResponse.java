package com.example.oss.newupdate.Model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by OSS on 12/18/2017.
 */

public class BOGraphPojoResponse {

    BOGraphModel boGraphModel;

    public BOGraphPojoResponse(){
        boGraphModel = new BOGraphModel();
    }

    public BOGraphModel parseBoGrJSON(String response) {
        Gson gson = new GsonBuilder().create();
        boGraphModel = gson.fromJson(response, BOGraphModel.class);
        return boGraphModel;
    }
}
