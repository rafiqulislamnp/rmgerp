package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by OSS on 12/18/2017.
 */

public class PurchaseOrderModel implements Parcelable {
    public ArrayList<PurchaseOrderListModel> getOrderPo() {
        return orderPo;
    }

    public void setOrderPo(ArrayList<PurchaseOrderListModel> orderPo) {
        this.orderPo = orderPo;
    }

    private ArrayList<PurchaseOrderListModel> orderPo;

    public PurchaseOrderModel(){

    }

    protected PurchaseOrderModel(Parcel in) {
        orderPo = in.createTypedArrayList(PurchaseOrderListModel.CREATOR);
    }

    public static final Creator<PurchaseOrderModel> CREATOR = new Creator<PurchaseOrderModel>() {
        @Override
        public PurchaseOrderModel createFromParcel(Parcel in) {
            return new PurchaseOrderModel(in);
        }

        @Override
        public PurchaseOrderModel[] newArray(int size) {
            return new PurchaseOrderModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(orderPo);
    }
}
