package com.example.oss.newupdate.Merchandising.BuyerList;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.oss.newupdate.Model.BuyerOrderListModel;
import com.example.oss.newupdate.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BOListDetailsFragment extends Fragment {

    private BuyerOrderListModel singleBOItemDetails;

    private TextView boDetID;
    private TextView boDetItem;
    private TextView boDetTotPck;
    private TextView boDetPc;
    private TextView boDetQDz;
    private TextView boDetUnPr;
    private TextView boDetTotPr;

    public BOListDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        singleBOItemDetails = getArguments().getParcelable("boListDetParc");
        return inflater.inflate(R.layout.fragment_bolist_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        boDetID = getActivity().findViewById(R.id.boDetIdVal);
        boDetItem = getActivity().findViewById(R.id.boDetItmVal);
        boDetTotPck = getActivity().findViewById(R.id.boDetPkVal);
        boDetPc = getActivity().findViewById(R.id.boDetPcVal);
        boDetQDz = getActivity().findViewById(R.id.boDetQtyVal);
        boDetUnPr = getActivity().findViewById(R.id.boDetUnPrVal);
        boDetTotPr = getActivity().findViewById(R.id.boDetTotPrcVal);

        boDetID.setText(singleBOItemDetails.getID());
        boDetItem.setText(singleBOItemDetails.getItem());
        boDetTotPck.setText(singleBOItemDetails.getTotalPack());
        boDetPc.setText(singleBOItemDetails.getPC());
        boDetQDz.setText(singleBOItemDetails.getQuantityInDz());
        boDetUnPr.setText(singleBOItemDetails.getUnitPrice());
        boDetTotPr.setText(singleBOItemDetails.getTotalPrice());

        Log.d("DetailsBoList","Inside BOListDetails Fragment: "+singleBOItemDetails.getItem());
    }
}
