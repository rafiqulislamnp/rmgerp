package com.example.oss.newupdate.Merchandising;

import android.app.Activity;
import android.app.DialogFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.oss.newupdate.Graph.BWBOChartFragment;
import com.example.oss.newupdate.Merchandising.BuyerList.BOListFragment;
import com.example.oss.newupdate.Model.BuyerNameListModel;
import com.example.oss.newupdate.Model.BuyerNameModel;
import com.example.oss.newupdate.Model.BuyerNamePojoResponse;
import com.example.oss.newupdate.Model.BuyerOrderModel;
import com.example.oss.newupdate.Model.BuyerOrderPojoResponse;
import com.example.oss.newupdate.Model.OrderIdListModel;
import com.example.oss.newupdate.Model.OrderIdModel;
import com.example.oss.newupdate.Model.OrderIdPojoResponse;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;
import com.example.oss.newupdate.Volley.VolleySingleton;

import java.util.ArrayList;
import java.util.Collection;


public class BuyerOrderFragment extends Fragment {

    public static final String TITLE = "Buyer Order";
    public static final int Order_DIALOG_FRAGMENT = 1;
    private Button buttonSearch;
    private EditText editTextFrom;
    private EditText editTextTo;
    public static final int OID_DIALOG_FRAGMENT = 1;
    private EditText buyerOrder;
    private Spinner buyerName;
    private  String boList;
    private String bnList;
    private String fromList;
    private String toList;

    private Toolbar mToolbar;


    private ArrayAdapter<BuyerNameListModel> dataAdapter;
    private BuyerNamePojoResponse buyerNamePojoResponse;

    private BuyerOrderPojoResponse buyerOrderPojoResponse;

    private ArrayList<OrderIdListModel> orderIdListModelArrayList;
    private OrderIdPojoResponse orderIdPojoResponse;

    public static BuyerOrderFragment newInstance() {

        return new BuyerOrderFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_buyer_order, container, false);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        buyerNamePojoResponse = new BuyerNamePojoResponse();

        buyerOrderPojoResponse = new BuyerOrderPojoResponse();

        orderIdPojoResponse = new OrderIdPojoResponse();

        editTextFrom = (EditText) getActivity().findViewById(R.id.fromDateBuyer);
        editTextTo = (EditText) getActivity().findViewById(R.id.toDateBuyer);
        buyerOrder = getActivity().findViewById(R.id.buyerOrderList);
        buyerName = getActivity().findViewById(R.id.buyerNameList);

        /*buyerName.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) getActivity());*/

        buttonSearch = (Button) getActivity().findViewById(R.id.searchButtonBuyer);

        //Fetch data from server [Buyer Name]
        makeBNListRequest(new BWBOChartFragment.VolleyBNListCallback() {
            @Override
            public void getBNListResultVolley(BuyerNameModel result) {
                addBuyerNameSpinner(result);
            }
        });

        editTextFrom.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    DialogFragment datePickerFragment = new DatePickerMerchandiseFragment(editTextFrom,getContext());
                    datePickerFragment.show(getActivity().getFragmentManager(),"ANY_UNIQUE_KEY");
                }
            }
        });

        editTextTo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    DialogFragment datePickerFragment = new DatePickerMerchandiseFragment(editTextTo,getContext());
                    datePickerFragment.show(getActivity().getFragmentManager(),"ANY_UNIQUE_KEY");
                }
            }
        });

        buyerOrder.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {

                    SharedData.startProgress(getActivity(),"Retrieving OrderId...");
                    getAllOrderId();



                    //Log.d("CurrentOrderIdListModel","Current Order Id List Model ArrayList Size: "+orderIdListModelArrayList.size());

                    /*Toast.makeText(getContext(),"Buyer Order has got focused",Toast.LENGTH_SHORT).show();*/
                   /* android.support.v4.app.FragmentManager fm=getFragmentManager();
                    PlayersFragment p=new PlayersFragment();
                    p.show(fm, "Best Players");*/
                }
            }
        });



        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Start progress bar
                /*final ProgressDialog progressDialog = new ProgressDialog(getActivity(),
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Searching...");
                progressDialog.show();*/

                SharedData.startProgress(getActivity(),"Searching...");

                boList = buyerOrder.getText().toString();
                bnList = buyerName.getSelectedItem().toString();
                //Log.d("SelectedBuyerName","Current Selected Buyer Name: "+bnList);
                fromList =editTextFrom.getText().toString();
                toList =editTextTo.getText().toString();

             if((boList.isEmpty() && bnList.isEmpty() && fromList.isEmpty() && toList.isEmpty()) || ( fromList.isEmpty() && toList.isEmpty() ) )

                {
                    //check trimming
                    //Show Alert message and return

                    //You have to optimize it
                    /*new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();
                                    Toast.makeText(getContext(),"Please provide required field",Toast.LENGTH_SHORT).show();


                                }
                            }, 1000);*/
                    SharedData.endProgress(getContext(),"Please provide required field");
                }
                else{
                    Log.d("BuyerOrderResponse","Showing alert message 3");
                    if(boList.isEmpty())
                        boList = "";
                    if(bnList.isEmpty())
                        bnList = "";


                    makeBOListRequest(new VolleyBOListCallback() {
                        @Override
                        public void getListResultVolley(final BuyerOrderModel result) {
                            //Stop the progress bar

                                new android.os.Handler().postDelayed(
                                        new Runnable() {
                                            public void run() {
                                                if(result.getBuyerOrder().size()>0) {
                                                    BOListFragment boListFragment = new BOListFragment();
                                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                                                    Bundle args = new Bundle();
                                                    args.putParcelable("boListParc", (Parcelable) result);
                                                    boListFragment.setArguments(args);

                                                    fragmentTransaction.replace(R.id.nameBO, boListFragment);
                                                    fragmentTransaction.addToBackStack("addBoList");
                                                    fragmentTransaction.commit();
                                                }else{
                                                    Toast.makeText(getContext(),"Empty result",Toast.LENGTH_SHORT).show();
                                                }

                                                SharedData.endOnlyProgress();

                                            }
                                        }, 3000);


                        }
                    });
                }



            }
        });


            if(getArguments()!=null){
                OrderIdListModel orderIdListModel = getArguments().getParcelable("selectedOId");
                if (orderIdListModel != null) {

                    buyerOrder.setText(orderIdListModel.getID());
                }
            }


    }

    public void addDateInEditText(String date){
        if(editTextFrom.hasFocus()){
            editTextFrom.setText(date);
        }else if(editTextTo.hasFocus()){
            editTextTo.setText(date);
        }
    }

    private void makeBOListRequest(final VolleyBOListCallback volleyCallback){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.boListURL.concat("buyerOrder="+boList+"&buyerName="+bnList+"&fromDate="+fromList+"&toDate="+toList),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("BuyerOrderResponse","My response: "+response);
                        BuyerOrderModel dataReturn = buyerOrderPojoResponse.parseBOListJSON(response);
                        volleyCallback.getListResultVolley(dataReturn);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BuyerOrderResponse",error.toString());
                error.printStackTrace();
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        VolleySingleton.getInstance(this.getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public interface VolleyBOListCallback{
        void getListResultVolley(BuyerOrderModel result);
    }

    //Fetch all buyer name from server
    private void makeBNListRequest(final BWBOChartFragment.VolleyBNListCallback volleyBNCallback){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.getAllBuyerName,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     /*   Log.d("BuyerOrderResponse","My response: "+response);*/
                        BuyerNameModel buyerNameModel = buyerNamePojoResponse.pojoResponseBuyerName(response);
                        volleyBNCallback.getBNListResultVolley(buyerNameModel);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        VolleySingleton.getInstance(this.getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void addBuyerNameSpinner(BuyerNameModel result){
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<BuyerNameListModel>(getActivity(), android.R.layout.simple_spinner_item);
        dataAdapter.addAll((Collection<? extends BuyerNameListModel>) result.getBuyerName());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        buyerName.setAdapter(dataAdapter);
    }

    private void getAllOrderId(){
        makeOrderIdListRequest(new VolleyOrderIdListCallback() {
            @Override
            public void getOrderListResultVolley(final OrderIdModel result) {
                //Stop the progress bar

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                if(result.getOrderID().size()>0) {
                                    orderIdListModelArrayList = result.getOrderID();
                                    Log.d("CurrentOrderIdListModel","Current Order Id List Model ArrayList Size: "+orderIdListModelArrayList.size());
                                    //Add code here
                                    /*adapter.addAll(orderIdListModelArrayList);
                                    lv.setAdapter(adapter);*/
                                    //Start the list fragment

                                    android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                    // Create and show the dialog.
                                    PlayersFragment alertDialogFragment = new PlayersFragment ("OrderId List");

                                    Bundle args = new Bundle();
                                    args.putParcelableArrayList("orderIdList",orderIdListModelArrayList);
                                    alertDialogFragment.setArguments(args);
                                    alertDialogFragment.setTargetFragment(BuyerOrderFragment.this, Order_DIALOG_FRAGMENT);
                                    //wrong first argument type found java.lang.runable required android.support.v4.app.Fragment
                                    alertDialogFragment.show(ft, "dialog");

                                }else{
                                    Toast.makeText(getContext(),"There is no order",Toast.LENGTH_SHORT).show();
                                }

                                SharedData.endOnlyProgress();

                            }
                        }, 3000);


            }
        });
    }

    private void makeOrderIdListRequest(final VolleyOrderIdListCallback volleyCallback){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.getOrderId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        OrderIdModel dataReturn = orderIdPojoResponse.parseOrderIdJSON(response);
                        volleyCallback.getOrderListResultVolley(dataReturn);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BuyerOrderResponse",error.toString());
                error.printStackTrace();
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        VolleySingleton.getInstance(this.getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case Order_DIALOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    OrderIdListModel getResult = bundle.getParcelable("selectedOID");
                    // makeBwBoGraphRequest(final VolleyBWBOGraphCallback volleyBWBOGraphCallback, getResult);
                    buyerOrder.setText(getResult.getID());

                } else if (resultCode == Activity.RESULT_CANCELED) {

                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public interface VolleyOrderIdListCallback{
        void getOrderListResultVolley(OrderIdModel result);
    }


}
