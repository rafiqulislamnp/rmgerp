package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OSS on 1/11/2018.
 */

public class OrderIdListModel implements Parcelable {
    @Override
    public String toString() {
        return ID;
    }

    public OrderIdListModel(){

    }
    private String ID;

    protected OrderIdListModel(Parcel in) {
        ID = in.readString();
    }

    public static final Creator<OrderIdListModel> CREATOR = new Creator<OrderIdListModel>() {
        @Override
        public OrderIdListModel createFromParcel(Parcel in) {
            return new OrderIdListModel(in);
        }

        @Override
        public OrderIdListModel[] newArray(int size) {
            return new OrderIdListModel[size];
        }
    };

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
    }
}
