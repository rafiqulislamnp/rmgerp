package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by OSS on 1/11/2018.
 */

public class OrderIdModel implements Parcelable {
    private ArrayList<OrderIdListModel> orderID;

    public OrderIdModel(){

    }

    protected OrderIdModel(Parcel in) {
        orderID = in.createTypedArrayList(OrderIdListModel.CREATOR);
    }

    public static final Creator<OrderIdModel> CREATOR = new Creator<OrderIdModel>() {
        @Override
        public OrderIdModel createFromParcel(Parcel in) {
            return new OrderIdModel(in);
        }

        @Override
        public OrderIdModel[] newArray(int size) {
            return new OrderIdModel[size];
        }
    };

    public ArrayList<OrderIdListModel> getOrderID() {
        return orderID;
    }

    public void setOrderID(ArrayList<OrderIdListModel> orderID) {
        this.orderID = orderID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(orderID);
    }
}
