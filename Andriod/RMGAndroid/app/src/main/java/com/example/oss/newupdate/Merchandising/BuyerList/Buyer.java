package com.example.oss.newupdate.Merchandising.BuyerList;

/**
 * Created by Adnan on 3/17/2016.
 */
public class Buyer {
    public String mNameOfPlace;
    public int mZipCode;
    public String mNameOfImage;
    public String mPopup;

    public Buyer(String startNameOfPlace, int startZipCode, String startNameOfImage, String startPopup)
    {
        this.mNameOfPlace=startNameOfPlace;
        this.mZipCode=startZipCode;
        this.mNameOfImage=startNameOfImage;
        this.mPopup=startPopup;
    }
}
