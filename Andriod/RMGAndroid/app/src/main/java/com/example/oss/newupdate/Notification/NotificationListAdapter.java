package com.example.oss.newupdate.Notification;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.oss.newupdate.R;

/**
 * Created by Adnan on 3/17/2016.
 */
public class NotificationListAdapter extends ArrayAdapter<Place> {
    Context mContext;
    int mLayoutResourceId;
    Place[] mData=null;

    public NotificationListAdapter(Context context, int resource, Place[] data) {
        super(context, resource, data);
        this.mContext=context;
        this.mLayoutResourceId=resource;
        this.mData=data;
    }

    @Override
    public Place getItem(int position)
    {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row=convertView;
        PlaceHolder holder=null;
        if(row==null)
        {
            //Create a new view
            LayoutInflater inflater = LayoutInflater.from(mContext);
            row=inflater.inflate(mLayoutResourceId,parent,false);
            holder=new PlaceHolder();
/*            holder.nameView=(TextView) row.findViewById(R.id.nameTextView);
            holder.zipView=(TextView) row.findViewById(R.id.zipcodeTextView);*/
            holder.imageView=(ImageView) row.findViewById(R.id.imageView);
            row.setTag(holder);
        }
        else{
            holder= (PlaceHolder) row.getTag();
        }
        //Getting the data from the data array
        Place place=mData[position];
        //Setup and reuse the same listener for each buyer_order_row
        holder.imageView.setOnClickListener(PopupListener);
        Integer rowPosition=position;
        holder.imageView.setTag(rowPosition);
        //Setting the view to reflect the data we need to display
        holder.nameView.setText(place.mNameOfPlace);
        holder.zipView.setText(String.valueOf(place.mZipCode));
        //for getting the image
        int resId=mContext.getResources().getIdentifier(place.mNameOfImage,"drawable",mContext.getPackageName());
        holder.imageView.setImageResource(resId);
        //Returning the buyer_order_row
        return row;
    }
    View.OnClickListener PopupListener= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Integer viewPosition= (Integer) v.getTag();
            Place p=mData[viewPosition];
            Toast.makeText(getContext(),p.mPopup, Toast.LENGTH_SHORT).show();
        }
    };
    private static class PlaceHolder{
        TextView nameView;
        TextView zipView;
        ImageView imageView;
    }
}
