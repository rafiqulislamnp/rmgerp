package com.example.oss.newupdate.MISDashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.oss.newupdate.DashBoard.DashBoardActivity;
import com.example.oss.newupdate.R;

/**
 * Created by OSS on 1/15/2018.
 */

public class MISGridAdapter extends BaseAdapter {
    Context context;
    private final String[] values;
    private final int[] images;
    View view;
    LayoutInflater layoutInflater;
    TextView textView;
    ImageView imageView;
    private int[] valuesForImages = {R.drawable.management, R.drawable.store, R.drawable.accounting, R.drawable.procurement};

    public MISGridAdapter(Context context, String[] values, int[] images) {
        this.context = context;
        this.values = values;
        this.images = images;
    }

    @Override

    public int getCount() {
        return values.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            view = new View(context);
            view = layoutInflater.inflate(R.layout.misdash_single_item, null);
            textView = (TextView) view.findViewById(R.id.misDashText);
            imageView = (ImageView) view.findViewById(R.id.imageViewMis);
            textView.setText(values[position]);
            imageView.setImageResource(images[position]);

        }
        final int id = valuesForImages[position];
        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof MISDashActivity) {
                    ((MISDashActivity) context).gridItemOnClickListener(v, id);
                }
            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof MISDashActivity) {
                    ((MISDashActivity) context).gridItemOnClickListener(v, id);
                }
            }
        });

        return view;
    }
}
