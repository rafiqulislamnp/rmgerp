package com.example.oss.newupdate.Merchandising.BuyerList;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.oss.newupdate.Model.BuyerOrderListModel;
import com.example.oss.newupdate.Model.BuyerOrderModel;
import com.example.oss.newupdate.R;

import java.util.List;

/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BOListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BOListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BOListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private BuyerOrderModel buyerOrderModel;

    //private OnFragmentInteractionListener mListener;

    public BOListFragment() {
        // Required empty public constructor
    }

    //List view
    private ListView mListView;
    private BuyerListAdapter mPlaceAdapter;



    /*
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BOListFragment.
     */

    // TODO: Rename and change types and number of parameters
  /*  public static BOListFragment newInstance(String param1, String param2) {
        BOListFragment fragment = new BOListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        buyerOrderModel = getArguments().getParcelable("boListParc");
        return inflater.inflate(R.layout.fragment_bolist, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
       /* if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("BuyerOrderResponse","Inside BOListFragment: "+ buyerOrderModel.getBuyerOrder().size());
        mListView= (ListView) getActivity().findViewById(R.id.myListViewBuyer);
        mPlaceAdapter=new BuyerListAdapter(getActivity().getApplicationContext(),R.layout.buyer_order_row,buyerOrderModel);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = mListView.getItemAtPosition(position);
                BuyerOrderListModel str = (BuyerOrderListModel)o; //As you are using Default String Adapter
                /*Toast.makeText(getActivity(),str.getItem(),Toast.LENGTH_SHORT).show();*/

                if(str!=null){
                    BOListDetailsFragment boListDetailsFragment = new BOListDetailsFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                    Bundle args = new Bundle();
                    args.putParcelable("boListDetParc",str);
                    boListDetailsFragment.setArguments(args);

                    fragmentTransaction.replace(R.id.nameBO, boListDetailsFragment);
                    fragmentTransaction.addToBackStack("addBoListDetails");
                    fragmentTransaction.commit();
                }



            }
        });
        if(mListView!=null)
        {
            mListView.setAdapter(mPlaceAdapter);
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
       // mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
   /* public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/



}
