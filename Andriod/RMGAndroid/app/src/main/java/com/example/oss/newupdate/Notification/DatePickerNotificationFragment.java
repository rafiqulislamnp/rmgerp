package com.example.oss.newupdate.Notification;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import com.example.oss.newupdate.Notification.NotificationActivity;

import java.util.Calendar;

/**
 * Created by OSS on 9/27/2017.
 */

public class DatePickerNotificationFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(),this,year,month,day);

    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        ((NotificationActivity) getActivity()).addDateInEditText(i2+" / "+i1+" / "+i);
    }
}
