package com.example.oss.newupdate.AlertDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.oss.newupdate.Model.BuyerNameListModel;
import com.example.oss.newupdate.Model.BuyerNameModel;
import com.example.oss.newupdate.R;

/**
 * Created by OSS on 12/16/2017.
 */

public class AlertDialogFragment extends android.support.v4.app.DialogFragment {

    private BuyerNameModel buyerNameModel;
    private String alertDialogTitle;


    public AlertDialogFragment(String title){
        alertDialogTitle = title;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        buyerNameModel = getArguments().getParcelable("buyerNameList");
        Log.d("BuyerNameModelSize","Total buyer Name: "+ buyerNameModel.getBuyerName().size());
        AlertDialog.Builder mBuider = new AlertDialog.Builder(getActivity());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.dialog_spinner,null);
        mBuider.setTitle(alertDialogTitle);
        final Spinner mSpinner = mView.findViewById(R.id.spinner);
        final EditText mEditText = mView.findViewById(R.id.edit1);
        ArrayAdapter<BuyerNameListModel> adapter = new ArrayAdapter<BuyerNameListModel>(getActivity(),android.R.layout.simple_spinner_item);
        adapter.addAll(buyerNameModel.getBuyerName());

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mBuider.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if( mSpinner.getSelectedItem().toString().equalsIgnoreCase("Choose a restaurant") ){
                /*Toast.makeText(getActivity(),mSpinner.getSelectedItem().toString()+"-"+mEditText.getText().toString(),Toast.LENGTH_SHORT).show();*/
                String targetResult = mEditText.getText().toString() + "-" + mSpinner.getSelectedItem().toString();
                Intent i = new Intent()
                        .putExtra("monthBuyerName", targetResult);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                dialog.dismiss();
                // }
            }
        });

        mBuider.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        mBuider.setView(mView);
        AlertDialog dialog = mBuider.create();
        return dialog;
    }

    private void sendResult(int requestCode){

    }
}
