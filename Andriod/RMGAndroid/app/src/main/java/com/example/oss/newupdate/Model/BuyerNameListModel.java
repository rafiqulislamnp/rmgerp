package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OSS on 12/16/2017.
 */

public class BuyerNameListModel implements Parcelable {
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    protected BuyerNameListModel(Parcel in) {
        Name = in.readString();
    }

    public BuyerNameListModel(){

    }

    @Override
    public String toString() {
        return Name;
    }

    public static final Creator<BuyerNameListModel> CREATOR = new Creator<BuyerNameListModel>() {
        @Override
        public BuyerNameListModel createFromParcel(Parcel in) {
            return new BuyerNameListModel(in);
        }

        @Override
        public BuyerNameListModel[] newArray(int size) {
            return new BuyerNameListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Name);
    }
}
