package com.example.oss.newupdate.Model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by OSS on 1/11/2018.
 */

public class OrderIdPojoResponse {
    OrderIdModel orderIdModel;

    public OrderIdPojoResponse(){
        orderIdModel = new OrderIdModel();
    }

    public OrderIdModel parseOrderIdJSON(String response) {
        Gson gson = new GsonBuilder().create();
        orderIdModel = gson.fromJson(response, OrderIdModel.class);
        return orderIdModel;
    }
}
