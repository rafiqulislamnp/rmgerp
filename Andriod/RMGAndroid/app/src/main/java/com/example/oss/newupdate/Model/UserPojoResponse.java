package com.example.oss.newupdate.Model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by OSS on 10/4/2017.
 */

public class UserPojoResponse {
    List<User> myUser;
    public UserPojoResponse(){
        myUser = new ArrayList<User>();
    }

    public  void parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        // MyPojoResponse myPojoResponse = gson.fromJson(response, MyPojoResponse.class);

        Type collectionType = new TypeToken<Collection<User>>(){}.getType();
        Collection<User> enums = gson.fromJson(response, collectionType);
        myUser =(List<User>) enums;
        Log.d("codedata", String.valueOf(myUser.get(0).getUserName()) +" Count: "+myUser.size());


    }
}
