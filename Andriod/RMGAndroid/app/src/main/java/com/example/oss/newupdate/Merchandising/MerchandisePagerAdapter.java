package com.example.oss.newupdate.Merchandising;

/**
 * Created by OSS on 9/23/2017.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class MerchandisePagerAdapter extends FragmentStatePagerAdapter {

    private static int TAB_COUNT = 2;

    public MerchandisePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return BuyerOrderFragment.newInstance();
            case 1:
                return PurchaseOrderFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return BuyerOrderFragment.TITLE;

            case 1:
                return PurchaseOrderFragment.TITLE;
        }
        return super.getPageTitle(position);
    }
}
