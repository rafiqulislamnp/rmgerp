package com.example.oss.newupdate.Graph.LCCreation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.oss.newupdate.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BToBLcGraphFragment extends Fragment {

    public static final String TITLE = "BackToBackLC";

    public static BToBLcGraphFragment newInstance() {

        return new BToBLcGraphFragment();
    }


    public BToBLcGraphFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bto_blc_graph, container, false);
    }

}
