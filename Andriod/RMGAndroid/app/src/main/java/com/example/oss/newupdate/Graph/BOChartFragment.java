package com.example.oss.newupdate.Graph;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.oss.newupdate.AlertDialog.BOAlertDialogFragment;
import com.example.oss.newupdate.Model.BOGraphListModel;
import com.example.oss.newupdate.Model.BOGraphModel;
import com.example.oss.newupdate.Model.BOGraphPojoResponse;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;
import com.example.oss.newupdate.Volley.VolleySingleton;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

public class BOChartFragment extends Fragment {
    public static final int BO_DIALOG_FRAGMENT = 1;
    public static final String TITLE = "Buyer Order";
    LineChart mChart;
    private Bundle lineBundle;
    private int lineData;
    private BOGraphPojoResponse boGraphPojoResponse;
    private ImageButton alertBOGrph;
    private ArrayList<BOGraphListModel> boGraphListModelArrayList;
    private ProgressDialog progressDialog;

    public static BOChartFragment newInstance() {

        return new BOChartFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lineBundle = getArguments();
        lineData = lineBundle.getInt("SENTVALUE2");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        boGraphPojoResponse = new BOGraphPojoResponse();
        alertBOGrph = getActivity().findViewById(R.id.grphBOLineRet);

        Toast.makeText(getActivity(),"Current Line Chart Value: "+lineData,Toast.LENGTH_LONG).show();

        mChart = (LineChart) getView().findViewById(R.id.linechart);

        /*mChart.setOnChartGestureListener(Main2Activity.this);
        mChart.setOnChartValueSelectedListener(Main2Activity.this);*/



        alertBOGrph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Fetch data from server [Buyer Name]
                    showAlertBOGraph();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case BO_DIALOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String getResult = bundle.getString("yearMonthBOGr");
                    // makeBwBoGraphRequest(final VolleyBWBOGraphCallback volleyBWBOGraphCallback, getResult);
                    makeBoGraphRequest(new VolleyBOGraphCallback() {
                        @Override
                        public void getBOGrResultVolley(BOGraphModel result) {
                            boGraphListModelArrayList = result.getBuyerOrder();
                            if(boGraphListModelArrayList.size()>0){
                                progressDialog = new ProgressDialog(getActivity(),
                                        R.style.AppTheme_Dark_Dialog);
                                progressDialog.setIndeterminate(true);
                                progressDialog.setMessage("Please wait...");
                                progressDialog.show();

                                showBOGraphData();
                            }

                        }
                    }, getResult);

                } else if (resultCode == Activity.RESULT_CANCELED) {

                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showAlertBOGraph(){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        // Create and show the dialog.
        BOAlertDialogFragment boAlertDialogFragment = new BOAlertDialogFragment ("Buyer Order");

        boAlertDialogFragment.setTargetFragment(this, BO_DIALOG_FRAGMENT);
        boAlertDialogFragment.show(ft, "dialog");
    }

    //Fetch BO graph data from server

    private void makeBoGraphRequest(final VolleyBOGraphCallback volleyBOGraphCallback, String getResult){
        Log.d("CheckGraphData","Inside Volley 0: "+getResult);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.getBOGraphURL.concat(getResult),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     /*   Log.d("BuyerOrderResponse","My response: "+response);*/
                        BOGraphModel boGraphModel = boGraphPojoResponse.parseBoGrJSON(response);
                        Log.d("CheckGraphData","Inside Volley 1: "+boGraphModel.getBuyerOrder().size());
                        volleyBOGraphCallback.getBOGrResultVolley(boGraphModel);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.d("CheckGraphData", error.toString());
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        VolleySingleton.getInstance(this.getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void showBOGraphData(){

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progressDialog.dismiss();

                    }
                }, 2000);

        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        int i=0;
        String[] daysInArray = new String[34];
        ArrayList<Entry> dataset1 = new ArrayList<>();

        for (BOGraphListModel boGraphListModel : boGraphListModelArrayList){
                daysInArray[i] = String.valueOf(boGraphListModel.getMonth());
                dataset1.add(new Entry(i,boGraphListModel.getValue()));
                Log.d("CheckGraphData" ,"Data checking: "+ boGraphListModel.getValue());
                i++;
        }


        LineDataSet set1 = new LineDataSet(dataset1,"Orders");
        set1.setFillAlpha(110);
        set1.setColor(Color.RED);
        set1.setLineWidth(3f);
        set1.setValueTextSize(10f);
        set1.setValueTextColor(Color.BLACK);


        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);


        LineData data = new LineData(dataSets);
        XAxis xAxis = mChart.getXAxis();
        xAxis.setValueFormatter(new MyXAxisValueFormatter(daysInArray));
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);

        mChart.setData(data);
    }

    public interface VolleyBOGraphCallback{
        void getBOGrResultVolley(BOGraphModel result);
    }

}