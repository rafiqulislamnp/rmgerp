package com.example.oss.newupdate.DashBoard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;

import com.example.oss.newupdate.Graph.GraphActivity;
import com.example.oss.newupdate.MISDashboard.MISDashActivity;
import com.example.oss.newupdate.Notification.NotificationActivity;
import com.example.oss.newupdate.Merchandising.MerchandisingActivity;
import com.example.oss.newupdate.Graph.LCCreation.LCCreationActivity;
import com.example.oss.newupdate.R;

public class DashBoardActivity extends AppCompatActivity {
    GridView gridView;
    GridAdapter gridAdapter;
    private Toolbar mToolbar;
    String[] values ={"Buyer Order Search", "Purchase Order Search","Graph Report","Notification","Lc Creation","MIS"};

    int[] images = {R.drawable.search, R.drawable.searchpo,R.drawable.bar_chart,R.drawable.buyerorderlist,R.drawable.production,R.drawable.manager};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        gridView = (GridView)findViewById(R.id.gridview);


/*        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int id = images[i];
               // Log.d("CheckResource",getResources().getResourceName(id));
                Toast.makeText(getApplicationContext(),"Hello", Toast.LENGTH_SHORT).show();
            }
        });*/

    }

    public void gridItemOnClickListener(View v,int id){
        if(id == R.drawable.search){
            Intent intent = new Intent(this,MerchandisingActivity.class);
            intent.putExtra("currentFrag",1);
            startActivity(intent);
        }
        else if(id == R.drawable.searchpo){
            Intent intent = new Intent(this,MerchandisingActivity.class);
            intent.putExtra("currentFrag",2);
            startActivity(intent);
        }
        else if(id == R.drawable.bar_chart){
            Intent intent = new Intent(this,GraphActivity.class);
            startActivity(intent);
        }
        else if(id == R.drawable.buyerorderlist){
            Intent intent = new Intent(this,NotificationActivity.class);
            startActivity(intent);
        }
        else if(id == R.drawable.production){
            Intent intent = new Intent(this, LCCreationActivity.class);
            startActivity(intent);
        }
        else if(id == R.drawable.manager){
            Intent intent = new Intent(this, MISDashActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // Disable going back to the GraphActivity
        moveTaskToBack(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        gridAdapter = new GridAdapter(this,values,images);
        gridView.setAdapter(gridAdapter);
        gridAdapter.notifyDataSetChanged();
        //gridView.notifyAll();
    }
}
