package com.example.oss.newupdate.Notification;

/**
 * Created by Adnan on 3/17/2016.
 */
public class Place {
    public String mNameOfPlace;
    public int mZipCode;
    public String mNameOfImage;
    public String mPopup;

    public Place(String startNameOfPlace, int startZipCode, String startNameOfImage, String startPopup)
    {
        this.mNameOfPlace=startNameOfPlace;
        this.mZipCode=startZipCode;
        this.mNameOfImage=startNameOfImage;
        this.mPopup=startPopup;
    }
}
