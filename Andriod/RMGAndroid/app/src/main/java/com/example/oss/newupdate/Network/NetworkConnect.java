package com.example.oss.newupdate.Network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.example.oss.newupdate.DashBoard.DashBoardActivity;

/**
 * Created by OSS on 10/9/2017.
 */

public class NetworkConnect extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("connectionAdnan", "Network connectivity change: "+isInternetOn(context));
        if(!isInternetOn(context)){
            /*Intent i = new Intent();
            i.setClassName("com.example.oss.newupdate.DashBoard", "DashBoardActivity");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);*/

            /*Intent intentone = new Intent(context.getApplicationContext(), DashBoardActivity.class);
            intentone.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentone);*/

            Toast.makeText(context, "Internet is not available.Please check connection", Toast.LENGTH_SHORT).show();

        }
    }

    public final boolean isInternetOn(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
               /* Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();*/
                Log.d("connectionAdnan", "Connected to WIFI");
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
               /* Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();*/
                Log.d("connectionAdnan", "Connected to the Mobile provider data plan");
               return true;
            }
        }/* else {*/
            Log.d("connectionAdnan", "There is no internet connection");
            // not connected to the internet
            return false;
        //}
    }
}