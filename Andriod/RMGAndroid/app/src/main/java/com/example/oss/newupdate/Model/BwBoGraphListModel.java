package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OSS on 12/16/2017.
 */

public class BwBoGraphListModel implements Parcelable{
    private int Month;

    public int getMonth() {
        return Month;
    }

    public void setMonth(int month) {
        Month = month;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }

    private int Value;

    public BwBoGraphListModel(){

    }
    protected BwBoGraphListModel(Parcel in) {
        Month = in.readInt();
        Value = in.readInt();
    }

    public static final Creator<BwBoGraphListModel> CREATOR = new Creator<BwBoGraphListModel>() {
        @Override
        public BwBoGraphListModel createFromParcel(Parcel in) {
            return new BwBoGraphListModel(in);
        }

        @Override
        public BwBoGraphListModel[] newArray(int size) {
            return new BwBoGraphListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Month);
        dest.writeInt(Value);
    }
}
