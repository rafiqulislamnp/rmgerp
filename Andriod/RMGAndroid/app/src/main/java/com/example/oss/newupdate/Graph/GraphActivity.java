package com.example.oss.newupdate.Graph;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.oss.newupdate.Login.LoginActivity;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;
import com.example.oss.newupdate.DashBoard.ViewPagerAdapter;

public class GraphActivity extends AppCompatActivity {
    private ViewPager mViewPager;
    private Toolbar mToolbar;
    private ViewPagerAdapter mViewPagerAdapter;
    private TabLayout mTabLayout;

    private MenuItem profileItem;
    private MenuItem logoutItem;

    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    public static Boolean saveLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        setViewPager();

    }

    private void setViewPager() {

        Bundle bundle1 = new Bundle();

        bundle1.putInt("SENTVALUE1",19);
        bundle1.putInt("SENTVALUE2",20);
        bundle1.putInt("SENTVALUE3",21);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),bundle1);
        mViewPager.setAdapter(mViewPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tab);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        profileItem=menu.findItem(R.id.action_profile);
        logoutItem=menu.findItem(R.id.action_logout);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.



        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        else if (id == R.id.action_settings) {

            return true;
        }
        /*else if(id==R.id.action_search){

            return true;
        }*/

        else if (id == R.id.action_profile) {
            SharedData.showUserProfile(this);
            return true;
        }
        else if (id == R.id.action_logout) {
            boolean saveLogin = loginPreferences.getBoolean("saveLogin", false);
            if(!saveLogin){
                loginPrefsEditor.remove("useremail");
                loginPrefsEditor.remove("userpassword");
                loginPrefsEditor.commit();
            }
            showProgressBar();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showProgressBar(){
        final ProgressDialog progressDialog = new ProgressDialog(GraphActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Logging Out...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        // onLoginFailed();
                        progressDialog.dismiss();
                        showLoginForm();

                    }
                }, 3000);
    }

    private void showLoginForm(){
        finish();
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);

    }
}
