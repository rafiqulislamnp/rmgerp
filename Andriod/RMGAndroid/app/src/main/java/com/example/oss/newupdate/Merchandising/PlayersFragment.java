package com.example.oss.newupdate.Merchandising;




        import android.app.Activity;
        import android.app.AlertDialog;
        import android.app.Dialog;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.ListView;
        import android.widget.SearchView;

        import com.example.oss.newupdate.Model.BuyerNameListModel;
        import com.example.oss.newupdate.Model.BuyerNameModel;
        import com.example.oss.newupdate.Model.OrderIdListModel;
        import com.example.oss.newupdate.Model.OrderIdPojoResponse;
        import com.example.oss.newupdate.R;

        import java.util.ArrayList;

/**
 * Created by OSS on 12/16/2017.
 */

public class PlayersFragment extends android.support.v4.app.DialogFragment {

    private BuyerNameModel buyerNameModel;
    private String alertDialogTitle;


    Button btn;
    ListView lv;
    SearchView sv;
    ArrayAdapter<OrderIdListModel> adapter;
    private ArrayList<OrderIdListModel> orderIdListModelArrayList;
    private OrderIdListModel orderIdListModel;


    public PlayersFragment(String title){
        alertDialogTitle = title;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        buyerNameModel = getArguments().getParcelable("buyerNameList");
        //Log.d("BuyerNameModelSize","Total buyer Name: "+ buyerNameModel.getBuyerName().size());
        AlertDialog.Builder mBuider = new AlertDialog.Builder(getActivity());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.fragment_players,null);
        mBuider.setTitle(alertDialogTitle);

        orderIdListModelArrayList = getArguments().getParcelableArrayList("orderIdList");
        Log.d("CurrentOrderIdList","Current order id list: "+orderIdListModelArrayList.size());

        //BUTTON,LISTVIEW,SEARCHVIEW INITIALIZATIONS
        lv=(ListView) mView.findViewById(R.id.listView1);
        sv=(SearchView) mView.findViewById(R.id.searchView1);
       // btn=(Button) mView.findViewById(R.id.dismiss);

        adapter=new ArrayAdapter<OrderIdListModel>(getActivity(), android.R.layout.simple_list_item_1);

        adapter.addAll(orderIdListModelArrayList);

        //SEARCH
        sv.setQueryHint("Search..");
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String txt) {
                // TODO Auto-generated method stub
                return false;
            }
            @Override
            public boolean onQueryTextChange(String txt) {
                // TODO Auto-generated method stub
                adapter.getFilter().filter(txt);
                return false;
            }
        });



        lv.setAdapter(adapter);

        mBuider.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if( mSpinner.getSelectedItem().toString().equalsIgnoreCase("Choose a restaurant") ){
                /*Toast.makeText(getActivity(),mSpinner.getSelectedItem().toString()+"-"+mEditText.getText().toString(),Toast.LENGTH_SHORT).show();*/


                /*String targetResult = mEditText.getText().toString() + "-" + mSpinner.getSelectedItem().toString();
                Intent i = new Intent()
                        .putExtra("monthBuyerName", targetResult);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                dialog.dismiss();*/

                if(orderIdListModel!=null){
                    Intent i = new Intent()
                            .putExtra("selectedOID", orderIdListModel);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                    dialog.dismiss();
                }


                // }
            }
        });

        /*mBuider.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });*/

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Log.d("CurrentClickedItem","Current clicked item is: "+orderIdListModelArrayList.get(position));
               orderIdListModel = (OrderIdListModel) parent.getAdapter().getItem(position);                                         //orderIdListModelArrayList.get(position);
            }
        });

        mBuider.setView(mView);
        AlertDialog dialog = mBuider.create();
        return dialog;
    }

    private void sendResult(int requestCode){

    }
}

