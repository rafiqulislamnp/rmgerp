package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by OSS on 12/19/2017.
 */

public class POGraphModel implements Parcelable {
    private ArrayList<POGraphListModel> po;

    public POGraphModel(){

    }

    public ArrayList<POGraphListModel> getPo() {
        return po;
    }

    public void setPo(ArrayList<POGraphListModel> po) {
        this.po = po;
    }

    protected POGraphModel(Parcel in) {
        po = in.createTypedArrayList(POGraphListModel.CREATOR);
    }

    public static final Creator<POGraphModel> CREATOR = new Creator<POGraphModel>() {
        @Override
        public POGraphModel createFromParcel(Parcel in) {
            return new POGraphModel(in);
        }

        @Override
        public POGraphModel[] newArray(int size) {
            return new POGraphModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(po);
    }
}
