package com.example.oss.newupdate.Model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by OSS on 12/16/2017.
 */

public class BwBoGrPojoResponse {
    BwBoGraphModel bwBoGraphModel;
    public BwBoGrPojoResponse(){
        bwBoGraphModel = new BwBoGraphModel();
    }

    public BwBoGraphModel parseBwBoGrJSON(String response) {
        Gson gson = new GsonBuilder().create();
        bwBoGraphModel = gson.fromJson(response, BwBoGraphModel.class);
        return bwBoGraphModel;
    }

}
