package com.example.oss.newupdate.Graph.LCCreation;

/**
 * Created by OSS on 9/23/2017.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class LCViewPagerAdapter extends FragmentStatePagerAdapter {

    private static int TAB_COUNT = 2;
    private final Bundle fragmentBundle1;
    public LCViewPagerAdapter(FragmentManager fm, Bundle bundleFrag1) {
        super(fm);
        fragmentBundle1 = bundleFrag1;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MLCGraphFragment mlcGraphFragment = MLCGraphFragment.newInstance();
                mlcGraphFragment.setArguments(fragmentBundle1);
                return mlcGraphFragment;
            case 1:
                BToBLcGraphFragment bToBLcGraphFragment = BToBLcGraphFragment.newInstance();
                bToBLcGraphFragment.setArguments(fragmentBundle1);
                return bToBLcGraphFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return MLCGraphFragment.TITLE;

            case 1:
                return BToBLcGraphFragment.TITLE;

        }
        return super.getPageTitle(position);
    }
}
