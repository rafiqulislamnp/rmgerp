package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OSS on 12/13/2017.
 */

public class BuyerOrderModel implements Parcelable {
    protected BuyerOrderModel(Parcel in) {
        buyerOrder = in.createTypedArrayList(BuyerOrderListModel.CREATOR);
    }

    public static final Creator<BuyerOrderModel> CREATOR = new Creator<BuyerOrderModel>() {
        @Override
        public BuyerOrderModel createFromParcel(Parcel in) {
            return new BuyerOrderModel(in);
        }

        @Override
        public BuyerOrderModel[] newArray(int size) {
            return new BuyerOrderModel[size];
        }
    };

    public BuyerOrderModel() {
        super();
    }

    public ArrayList<BuyerOrderListModel> getBuyerOrder() {
        return buyerOrder;
    }

    public void setBuyerOrder(ArrayList<BuyerOrderListModel> buyerOrder) {
        this.buyerOrder = buyerOrder;
    }

    private ArrayList<BuyerOrderListModel> buyerOrder;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(buyerOrder);
    }
}
