package com.example.oss.newupdate.Merchandising;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.oss.newupdate.Merchandising.MerchandisingActivity;

import java.util.Calendar;

/**
 * Created by OSS on 9/27/2017.
 */

public class DatePickerMerchandiseFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    EditText editText;
    public DatePickerMerchandiseFragment(EditText editText, Context ctx){
        this.editText = editText;
        //this.editText.setOnFocusChangeListener((View.OnFocusChangeListener) this);
       /* myCalendar = Calendar.getInstance();*/
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(),this,year,month,day);

    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        //((MerchandisingActivity) getActivity()).addDateInEditText(i2+" / "+i1+" / "+i);
         editText.setText(i+" / "+ (i1+1) +" / "+i2);
    }
}
