package com.example.oss.newupdate.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by OSS on 10/4/2017.
 */

public class User {

    @SerializedName("FirstCreatedBy")
    @Expose
    private Integer firstCreatedBy;
    @SerializedName("LastEditeddBy")
    @Expose
    private Integer lastEditeddBy;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("User_AccessLevelFK")
    @Expose
    private Integer userAccessLevelFK;
    @SerializedName("User_DepartmentFK")
    @Expose
    private Integer userDepartmentFK;
    @SerializedName("User_RoleFK")
    @Expose
    private Integer userRoleFK;
    @SerializedName("User_TeamFK")
    @Expose
    private Object userTeamFK;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("RememberMe")
    @Expose
    private Boolean rememberMe;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Active")
    @Expose
    private Boolean active;
    @SerializedName("FirstCreated")
    @Expose
    private Object firstCreated;
    @SerializedName("LastEdited")
    @Expose
    private String lastEdited;

    public Integer getFirstCreatedBy() {
        return firstCreatedBy;
    }

    public void setFirstCreatedBy(Integer firstCreatedBy) {
        this.firstCreatedBy = firstCreatedBy;
    }

    public Integer getLastEditeddBy() {
        return lastEditeddBy;
    }

    public void setLastEditeddBy(Integer lastEditeddBy) {
        this.lastEditeddBy = lastEditeddBy;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getUserAccessLevelFK() {
        return userAccessLevelFK;
    }

    public void setUserAccessLevelFK(Integer userAccessLevelFK) {
        this.userAccessLevelFK = userAccessLevelFK;
    }

    public Integer getUserDepartmentFK() {
        return userDepartmentFK;
    }

    public void setUserDepartmentFK(Integer userDepartmentFK) {
        this.userDepartmentFK = userDepartmentFK;
    }

    public Integer getUserRoleFK() {
        return userRoleFK;
    }

    public void setUserRoleFK(Integer userRoleFK) {
        this.userRoleFK = userRoleFK;
    }

    public Object getUserTeamFK() {
        return userTeamFK;
    }

    public void setUserTeamFK(Object userTeamFK) {
        this.userTeamFK = userTeamFK;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Object getFirstCreated() {
        return firstCreated;
    }

    public void setFirstCreated(Object firstCreated) {
        this.firstCreated = firstCreated;
    }

    public String getLastEdited() {
        return lastEdited;
    }

    public void setLastEdited(String lastEdited) {
        this.lastEdited = lastEdited;
    }

}
