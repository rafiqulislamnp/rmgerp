package com.example.oss.newupdate.Merchandising.BuyerList;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.oss.newupdate.Merchandising.PurchaseList.POListDetailsFragment;
import com.example.oss.newupdate.Merchandising.PurchaseList.Purchase;
import com.example.oss.newupdate.Merchandising.PurchaseList.PurchaseListAdapter;
import com.example.oss.newupdate.Model.BuyerOrderListModel;
import com.example.oss.newupdate.Model.PurchaseOrderListModel;
import com.example.oss.newupdate.Model.PurchaseOrderModel;
import com.example.oss.newupdate.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class POListFragment extends Fragment {

    private PurchaseOrderModel purchaseOrderModel;
    //List view

    private ListView mListView;
    private PurchaseListAdapter mPlaceAdapter;


    public POListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        purchaseOrderModel = getArguments().getParcelable("poListParc");
        return inflater.inflate(R.layout.fragment_polist, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d("PurchaseOrderListFrg","Purchase order List search: "+purchaseOrderModel.getOrderPo().size());
        mListView= (ListView) getActivity().findViewById(R.id.myPurchaseListView);
        mPlaceAdapter=new PurchaseListAdapter(getActivity().getApplicationContext(),R.layout.purchase_order_row,purchaseOrderModel);

        //PoList Item click listener start

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = mListView.getItemAtPosition(position);
                PurchaseOrderListModel str = (PurchaseOrderListModel)o; //As you are using Default String Adapter
                /*Toast.makeText(getActivity(),str.getItem(),Toast.LENGTH_SHORT).show();*/

                if(str!=null){
                    POListDetailsFragment poListDetailsFragment = new POListDetailsFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                    Bundle args = new Bundle();
                    args.putParcelable("poListDetParc",str);
                    poListDetailsFragment.setArguments(args);

                    fragmentTransaction.replace(R.id.nameBO, poListDetailsFragment);
                    fragmentTransaction.addToBackStack("addPoListDetails");
                    fragmentTransaction.commit();
                }



            }
        });

        //End

        if(mListView!=null)
        {
            mListView.setAdapter(mPlaceAdapter);
        }

    }
}
