package com.example.oss.newupdate.Graph.LCCreation;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.oss.newupdate.AlertDialog.BOAlertDialogFragment;
import com.example.oss.newupdate.Graph.MyXAxisValueFormatter;
import com.example.oss.newupdate.Model.MLCGraphListModel;
import com.example.oss.newupdate.Model.MLCGraphModel;
import com.example.oss.newupdate.Model.MLCGraphPojoResponse;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;
import com.example.oss.newupdate.Volley.VolleySingleton;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MLCGraphFragment extends Fragment {
    public static final int BO_DIALOG_FRAGMENT = 1;
    public static final String TITLE = "MasterLC";
    private LineChart mlcLinechart;
    private MLCGraphPojoResponse mlcGraphPojoResponse;
    private ImageButton alertMLCGrph;
    private ProgressDialog progressDialog;
    private ArrayList<MLCGraphListModel> mlcGraphListModelArrayList;

    public static MLCGraphFragment newInstance() {

        return new MLCGraphFragment();
    }

    public MLCGraphFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mlcgraph, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mlcLinechart = (LineChart) getView().findViewById(R.id.masterLCLine);

        mlcGraphPojoResponse = new MLCGraphPojoResponse();
        alertMLCGrph = getActivity().findViewById(R.id.grphMLCLineRet);

        alertMLCGrph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Fetch data from server [Buyer Name]
                showAlertMLCGraph();
            }
        });

    }

    private void showAlertMLCGraph(){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        // Create and show the dialog.
        BOAlertDialogFragment boAlertDialogFragment = new BOAlertDialogFragment ("Master LC");

        boAlertDialogFragment.setTargetFragment(this, BO_DIALOG_FRAGMENT);
        boAlertDialogFragment.show(ft, "dialog");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {



        switch (requestCode) {
            case BO_DIALOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String getResult = bundle.getString("yearMonthBOGr");
                    //Log.d("CurrentResult","Current result: "+getResult);
                    // makeBwBoGraphRequest(final VolleyBWBOGraphCallback volleyBWBOGraphCallback, getResult);
                    makeBoGraphRequest(new VolleyMLCGraphCallback() {
                        @Override
                        public void getBOGrResultVolley(MLCGraphModel result) {
                            //Log.d("MasterLCSize","Size of master LC: "+mlcGraphListModelArrayList.size());
                            mlcGraphListModelArrayList = result.getMasterLc();
                            if(mlcGraphListModelArrayList.size()>0){
                                progressDialog = new ProgressDialog(getActivity(),
                                        R.style.AppTheme_Dark_Dialog);
                                progressDialog.setIndeterminate(true);
                                progressDialog.setMessage("Please wait...");
                                progressDialog.show();

                                showMLCGraphData();
                            }

                            else{
                                progressDialog.dismiss();
                                mlcLinechart.clear();
                            }
                        }
                    }, getResult);

                } else if (resultCode == Activity.RESULT_CANCELED) {

                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Fetch MLC graph data from server

    private void makeBoGraphRequest(final VolleyMLCGraphCallback volleyMLCGraphCallback, String getResult){
       // Log.d("CheckGraphData","Inside Volley 0: "+getResult);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.getMLCGraphURL.concat(getResult),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     /*   Log.d("BuyerOrderResponse","My response: "+response);*/
                        MLCGraphModel mlcGraphModel = mlcGraphPojoResponse.parseMLcGrJSON(response);
                        //Log.d("CheckGraphData","Inside Volley 1: "+mlcGraphModel.getMasterLc().size());
                        volleyMLCGraphCallback.getBOGrResultVolley(mlcGraphModel);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
               // Log.d("CheckGraphData", error.toString());
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        VolleySingleton.getInstance(this.getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void showMLCGraphData(){

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progressDialog.dismiss();

                    }
                }, 2000);

        mlcLinechart.setDragEnabled(true);
        mlcLinechart.setScaleEnabled(true);

        int i=0;
        String[] daysInArray = new String[34];
        ArrayList<Entry> dataset1 = new ArrayList<>();

        for (MLCGraphListModel mlcGraphListModel : mlcGraphListModelArrayList){
            daysInArray[i] = String.valueOf(mlcGraphListModel.getDay());
            dataset1.add(new Entry(i,mlcGraphListModel.getValue()));
            i++;
            //Log.d("CurrentResult","Current result in int : "+i);
        }


        LineDataSet set1 = new LineDataSet(dataset1,"Orders");
        set1.setFillAlpha(110);
        set1.setColor(Color.RED);
        set1.setLineWidth(3f);
        set1.setValueTextSize(10f);
        set1.setValueTextColor(Color.BLACK);


        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);


        LineData data = new LineData(dataSets);
        XAxis xAxis = mlcLinechart.getXAxis();
        xAxis.setValueFormatter(new MyXAxisValueFormatter(daysInArray));
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);

        mlcLinechart.setData(data);

    }

    public interface VolleyMLCGraphCallback{
        void getBOGrResultVolley(MLCGraphModel result);
    }
}
