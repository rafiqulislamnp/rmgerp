package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by OSS on 12/16/2017.
 */

public class BuyerNameModel implements Parcelable {
    private ArrayList<BuyerNameListModel> buyerName;

    @Override
    public String toString() {
        return "BuyerNameModel{" +
                "buyerName=" + buyerName +
                '}';
    }

    public ArrayList<BuyerNameListModel> getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(ArrayList<BuyerNameListModel> buyerName) {
        this.buyerName = buyerName;
    }

    protected BuyerNameModel(Parcel in) {
        buyerName = in.createTypedArrayList(BuyerNameListModel.CREATOR);
    }

    public BuyerNameModel(){

    }

    public static final Creator<BuyerNameModel> CREATOR = new Creator<BuyerNameModel>() {
        @Override
        public BuyerNameModel createFromParcel(Parcel in) {
            return new BuyerNameModel(in);
        }

        @Override
        public BuyerNameModel[] newArray(int size) {
            return new BuyerNameModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(buyerName);
    }
}
