package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by OSS on 12/18/2017.
 */

public class BOGraphModel implements Parcelable {
    private ArrayList<BOGraphListModel> BuyerOrder;

    public ArrayList<BOGraphListModel> getBuyerOrder() {
        return BuyerOrder;
    }

    public void setBuyerOrder(ArrayList<BOGraphListModel> buyerOrder) {
        BuyerOrder = buyerOrder;
    }

    protected BOGraphModel(Parcel in) {
        BuyerOrder = in.createTypedArrayList(BOGraphListModel.CREATOR);
    }

    public static final Creator<BOGraphModel> CREATOR = new Creator<BOGraphModel>() {
        @Override
        public BOGraphModel createFromParcel(Parcel in) {
            return new BOGraphModel(in);
        }

        @Override
        public BOGraphModel[] newArray(int size) {
            return new BOGraphModel[size];
        }
    };

    public BOGraphModel(){

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(BuyerOrder);
    }
}
