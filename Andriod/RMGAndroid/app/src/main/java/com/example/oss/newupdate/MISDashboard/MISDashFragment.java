package com.example.oss.newupdate.MISDashboard;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.GridView;

import com.example.oss.newupdate.DashBoard.GridAdapter;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;

/**
 * A simple {@link Fragment} subclass.
 */
public class MISDashFragment extends Fragment {

    GridView gridView;
    MISGridAdapter gridAdapter;

    String[] values ={"Merchandising", "Store","Accounting","Procurement"};

    int[] images =  {R.drawable.management, R.drawable.store, R.drawable.accounting, R.drawable.procurement};


    public MISDashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_misdash, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        gridView = getActivity().findViewById(R.id.gridviewMis);
        //Hide keyboard
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        gridAdapter = new MISGridAdapter(getActivity(),values,images);
        gridView.setAdapter(gridAdapter);
        gridAdapter.notifyDataSetChanged();

    }

}
