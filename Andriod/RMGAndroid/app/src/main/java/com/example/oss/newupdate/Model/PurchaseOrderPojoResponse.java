package com.example.oss.newupdate.Model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by OSS on 12/18/2017.
 */

public class PurchaseOrderPojoResponse {
    PurchaseOrderModel purchaseOrderModel;
    public PurchaseOrderPojoResponse(){
        purchaseOrderModel = new PurchaseOrderModel();
    }

    public PurchaseOrderModel parsePOListJSON(String response) {
        Gson gson = new GsonBuilder().create();
        purchaseOrderModel = gson.fromJson(response, PurchaseOrderModel.class);
        return purchaseOrderModel;
    }
}
