package com.example.oss.newupdate.AlertDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.oss.newupdate.Model.BuyerNameListModel;
import com.example.oss.newupdate.R;

/**
 * Created by OSS on 12/18/2017.
 */

public class BOAlertDialogFragment extends android.support.v4.app.DialogFragment {

    private String alertDialogTitle;

    public BOAlertDialogFragment(String title){
        alertDialogTitle = title;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder mBuider = new AlertDialog.Builder(getActivity());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.dialog_bogrph,null);
        mBuider.setTitle(alertDialogTitle);
        final EditText mEdiTextYear = mView.findViewById(R.id.editYear);
        final EditText mEditTextMonth = mView.findViewById(R.id.editMonth);

        mBuider.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if( mSpinner.getSelectedItem().toString().equalsIgnoreCase("Choose a restaurant") ){
                /*Toast.makeText(getActivity(),mSpinner.getSelectedItem().toString()+"-"+mEditText.getText().toString(),Toast.LENGTH_SHORT).show();*/
                String targetResult = mEdiTextYear.getText().toString() + "-" + mEditTextMonth.getText().toString();
                Intent i = new Intent()
                        .putExtra("yearMonthBOGr", targetResult);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                dialog.dismiss();
                // }
            }
        });

        mBuider.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        mBuider.setView(mView);
        AlertDialog dialog = mBuider.create();
        return dialog;
    }
}
