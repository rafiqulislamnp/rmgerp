package com.example.oss.newupdate.Merchandising.PurchaseList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.oss.newupdate.Model.PurchaseOrderListModel;
import com.example.oss.newupdate.Model.PurchaseOrderModel;
import com.example.oss.newupdate.R;

import java.util.ArrayList;

/**
 * Created by Adnan on 3/17/2016.
 */
public class PurchaseListAdapter extends ArrayAdapter<PurchaseOrderListModel> {
    Context mContext;
    int mLayoutResourceId;
    private ArrayList<PurchaseOrderListModel> mData=null;

    public PurchaseListAdapter(Context context, int resource, PurchaseOrderModel data) {
        super(context, resource, data.getOrderPo());
        this.mContext=context;
        this.mLayoutResourceId=resource;
        this.mData=data.getOrderPo();
    }

    @Override
    public PurchaseOrderListModel getItem(int position)
    {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row=convertView;
        PlaceHolder holder=null;
        if(row==null)
        {
            //Create a new view
            LayoutInflater inflater = LayoutInflater.from(mContext);
            row=inflater.inflate(mLayoutResourceId,parent,false);
            holder=new PlaceHolder();
            holder.namePOView=(TextView) row.findViewById(R.id.textPOListItem);
            holder.idPOView=(TextView) row.findViewById(R.id.textPOListId);

            row.setTag(holder);
        }
        else{
            holder= (PlaceHolder) row.getTag();
        }
        //Getting the data from the data array
        PurchaseOrderListModel purchaseOrderListModel=mData.get(position);

        //Setup and reuse the same listener for each purchase_order_row

        //Setting the view to reflect the data we need to display
        holder.namePOView.setText(purchaseOrderListModel.getItem());
        holder.idPOView.setText(purchaseOrderListModel.getPOID());

        //Returning the buyer_order_row
        return row;
    }
    private static class PlaceHolder{
        TextView namePOView;
        TextView idPOView;
    }
}
