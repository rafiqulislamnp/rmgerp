package com.example.oss.newupdate.Merchandising.PurchaseList;

/**
 * Created by Adnan on 3/17/2016.
 */
public class Purchase {
    public String mNameOfPlace;
    public int mZipCode;
    public String mNameOfImage;
    public String mPopup;

    public Purchase(String startNameOfPlace, int startZipCode, String startNameOfImage, String startPopup)
    {
        this.mNameOfPlace=startNameOfPlace;
        this.mZipCode=startZipCode;
        this.mNameOfImage=startNameOfImage;
        this.mPopup=startPopup;
    }
}
