package com.example.oss.newupdate.Graph;

/**
 * Created by OSS on 9/23/2017.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.oss.newupdate.AlertDialog.BOAlertDialogFragment;
import com.example.oss.newupdate.Model.BOGraphListModel;
import com.example.oss.newupdate.Model.BOGraphModel;
import com.example.oss.newupdate.Model.BOGraphPojoResponse;
import com.example.oss.newupdate.Model.POGraphListModel;
import com.example.oss.newupdate.Model.POGraphModel;
import com.example.oss.newupdate.Model.POGraphPojoResponse;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;
import com.example.oss.newupdate.Volley.VolleySingleton;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

public class POLineChartFragment extends Fragment {

    public static final String TITLE = "Purchase Order";
    private int sendData=0;
    private POGraphPojoResponse poGraphPojoResponse;

    public static final int PO_DIALOG_FRAGMENT = 1;

    LineChart mChart;
    private Bundle linePOBundle;
    private int lineData;
    private ImageButton alertPOGrph;
    private ArrayList<POGraphListModel> poGraphListModelArrayList;
    private ProgressDialog progressDialog;

    public static POLineChartFragment newInstance() {

        return new POLineChartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        linePOBundle = getArguments();
        sendData = linePOBundle.getInt("SENTVALUE1");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        poGraphPojoResponse = new POGraphPojoResponse();
        alertPOGrph = getActivity().findViewById(R.id.grphPOLineRet);

        mChart = (LineChart) getView().findViewById(R.id.linechartPO);

        alertPOGrph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Fetch data from server [Buyer Name]
                showAlertPOGraph();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case PO_DIALOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String getResult = bundle.getString("yearMonthBOGr");
                    // makeBwBoGraphRequest(final VolleyBWBOGraphCallback volleyBWBOGraphCallback, getResult);
                    makePoGraphRequest(new VolleyPOGraphCallback() {
                        @Override
                        public void getPOGrResultVolley(POGraphModel result) {
                            poGraphListModelArrayList = result.getPo();
                            if(poGraphListModelArrayList.size()>0){

                                progressDialog = new ProgressDialog(getActivity(),
                                        R.style.AppTheme_Dark_Dialog);
                                progressDialog.setIndeterminate(true);
                                progressDialog.setMessage("Please wait...");
                                progressDialog.show();


                                showPOGraphData();
                            }

                        }
                    }, getResult);

                } else if (resultCode == Activity.RESULT_CANCELED) {

                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Fetch BO graph data from server

    private void makePoGraphRequest(final VolleyPOGraphCallback volleyPOGraphCallback, String getResult){
        //Log.d("CheckGraphData","Inside Volley 0: "+getResult);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.getPOGraphURL.concat(getResult),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     /*   Log.d("BuyerOrderResponse","My response: "+response);*/
                        POGraphModel poGraphModel = poGraphPojoResponse.parsePoGrJSON(response);
                        //Log.d("CheckGraphData","Inside Volley 1: "+poGraphModel.getPo().size());
                        volleyPOGraphCallback.getPOGrResultVolley(poGraphModel);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.d("CheckGraphData", error.toString());
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        VolleySingleton.getInstance(this.getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void showAlertPOGraph(){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        // Create and show the dialog.
        BOAlertDialogFragment boAlertDialogFragment = new BOAlertDialogFragment ("Purchase Order");

        boAlertDialogFragment.setTargetFragment(this, PO_DIALOG_FRAGMENT);
        boAlertDialogFragment.show(ft, "dialog");
    }

    private void showPOGraphData(){

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progressDialog.dismiss();

                    }
                }, 2000);


        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        int i=0;
        String[] daysInArray = new String[34];
        ArrayList<Entry> dataset1 = new ArrayList<>();

        for (POGraphListModel poGraphListModel : poGraphListModelArrayList){
            daysInArray[i] = String.valueOf(poGraphListModel.getDay());
            dataset1.add(new Entry(i,poGraphListModel.getCount()));
            //Log.d("CheckGraphData" ,"Data checking: "+ poGraphListModel.getCount());
            i++;
        }


        LineDataSet set1 = new LineDataSet(dataset1," Purchase Orders");
        set1.setFillAlpha(110);
        set1.setColor(Color.RED);
        set1.setLineWidth(3f);
        set1.setValueTextSize(10f);
        set1.setValueTextColor(Color.BLACK);


        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);


        LineData data = new LineData(dataSets);
        XAxis xAxis = mChart.getXAxis();
        xAxis.setValueFormatter(new MyXAxisValueFormatter(daysInArray));
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);

        mChart.setData(data);
    }

    public interface VolleyPOGraphCallback{
        void getPOGrResultVolley(POGraphModel result);
    }
}
