package com.example.oss.newupdate.Util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


import com.example.oss.newupdate.Profile.ProfileActivity;
import com.example.oss.newupdate.R;

/**
 * Created by OSS on 10/5/2017.
 */

public class SharedData {
    public static Boolean isLoggedIn;
    public static String userEmail;
    public static String userPassword;
    public static String boListURL = "http://103.240.250.149/romo_test/api/GetOrderDetails?";
    public static String getAllBuyerName = "http://103.240.250.149/Romo_test/api/GetBuyer";
    public static String getBWBOGraph = "http://103.240.250.149/romo_test/api/GetByBuYearOrder?p=";
    public static String getPOListURL = "http://103.240.250.149/romo_test/api/GetOrderPoDetails?";
    public static String getBOGraphURL = "http://103.240.250.149/romo_test/api/OrderByDay?p=";
    public static String getPOGraphURL = "http://103.240.250.149/romo_test/api/POGetByMonthYear?p=";
    public static String getMLCGraphURL = "http://103.240.250.149/romo_test/api/GetByMasterLc?p=";
    public static String getOrderId = "http://103.240.250.149/Romo_test/api/GetOrderId";


    public static ProgressDialog progressDialog;

    public static void showUserProfile(Activity profileActivity){
        Intent intent = new Intent(profileActivity, ProfileActivity.class);
        profileActivity.startActivity(intent);
    }

    public interface VolleyCallback{
        void getResultVolley(int result);
    }

    public static void startProgress(Activity activity, String message){
        progressDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void endProgress(final Context context, final String msg){
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progressDialog.dismiss();
                        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();


                    }
                }, 1000);
    }

    public static void endOnlyProgress(){
        progressDialog.dismiss();
    }
    public static void endOnlyProgress(int time){
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progressDialog.dismiss();

                    }
                }, time);
    }

    public static boolean hideSoftKeyboard(Activity activity) {
        final InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive()) {
            if (activity.getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        }
        return false;
    }
}
