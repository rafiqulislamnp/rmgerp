package com.example.oss.newupdate.Model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by OSS on 12/16/2017.
 */

public class BuyerNamePojoResponse {
    private BuyerNameModel buyerNameModel;
    public BuyerNamePojoResponse(){
        buyerNameModel = new BuyerNameModel();
    }
    public BuyerNameModel pojoResponseBuyerName(String response) {
        Gson gson = new GsonBuilder().create();

        buyerNameModel = gson.fromJson(response, BuyerNameModel.class);

        return buyerNameModel;
    }
}
