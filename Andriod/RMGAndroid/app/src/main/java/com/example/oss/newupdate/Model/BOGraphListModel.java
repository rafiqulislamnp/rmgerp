package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OSS on 12/18/2017.
 */

public class BOGraphListModel implements Parcelable {
    private int Month;
    private int Value;

    public int getMonth() {
        return Month;
    }

    public void setMonth(int month) {
        Month = month;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }

    protected BOGraphListModel(Parcel in) {
        Month = in.readInt();
        Value = in.readInt();
    }

    public static final Creator<BOGraphListModel> CREATOR = new Creator<BOGraphListModel>() {
        @Override
        public BOGraphListModel createFromParcel(Parcel in) {
            return new BOGraphListModel(in);
        }

        @Override
        public BOGraphListModel[] newArray(int size) {
            return new BOGraphListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Month);
        dest.writeInt(Value);
    }
}
