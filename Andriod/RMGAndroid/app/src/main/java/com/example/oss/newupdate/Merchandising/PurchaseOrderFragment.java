package com.example.oss.newupdate.Merchandising;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.oss.newupdate.Merchandising.BuyerList.BOListFragment;
import com.example.oss.newupdate.Merchandising.BuyerList.POListFragment;
import com.example.oss.newupdate.Merchandising.PurchaseList.Purchase;
import com.example.oss.newupdate.Merchandising.PurchaseList.PurchaseListAdapter;
import com.example.oss.newupdate.Model.BuyerOrderModel;
import com.example.oss.newupdate.Model.PurchaseOrderModel;
import com.example.oss.newupdate.Model.PurchaseOrderPojoResponse;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;
import com.example.oss.newupdate.Volley.VolleySingleton;


public class PurchaseOrderFragment extends Fragment {

    public static final String TITLE = "Purchase Order";
    Button buttonSearch;
    EditText editTextFrom;
    EditText editTextTo;
    private Toolbar mToolbar;

    private EditText buyerOrder;
    private EditText purchaseOrder;

    private  String boList;
    private String poList;
    private String fromList;
    private String toList;

    private PurchaseOrderPojoResponse purchaseOrderPojoResponse;

    public PurchaseOrderFragment() {
        // Required empty public constructor
    }

    public static PurchaseOrderFragment newInstance() {

        return new PurchaseOrderFragment();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_purchase_order, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        purchaseOrderPojoResponse = new PurchaseOrderPojoResponse();
        editTextFrom = (EditText) getActivity().findViewById(R.id.fromDatePurchase);
        editTextTo = (EditText) getActivity().findViewById(R.id.toDatePurchase);
        buyerOrder = (EditText) getActivity().findViewById(R.id.poListBID);
        purchaseOrder = (EditText) getActivity().findViewById(R.id.poListPID);

        buttonSearch = (Button) getActivity().findViewById(R.id.searchButtonPurchase);
        editTextFrom.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    DialogFragment datePickerFragment = new DatePickerMerchandiseFragment(editTextFrom,getContext());
                    datePickerFragment.show(getActivity().getFragmentManager(),"ANY_UNIQUE_KEY");
                    Log.d("Focus","Focus has changed.");
                }
            }
        });

        editTextTo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    DialogFragment datePickerFragment = new DatePickerMerchandiseFragment(editTextTo,getContext());
                    datePickerFragment.show(getActivity().getFragmentManager(),"ANY_UNIQUE_KEY");
                    Log.d("Focus","Focus has changed.");
                }
            }
        });

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Start progress bar
                /*final ProgressDialog progressDialog = new ProgressDialog(getActivity(),
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Searching...");
                progressDialog.show();*/
                SharedData.startProgress(getActivity(),"Searching...");

                fromList =editTextFrom.getText().toString();
                toList =editTextTo.getText().toString();
                boList = buyerOrder.getText().toString();
                poList = purchaseOrder.getText().toString();


                if((boList.isEmpty() && poList.isEmpty() && fromList.isEmpty() && toList.isEmpty()) || ( fromList.isEmpty() || toList.isEmpty() ) )

                {
                    //check trimming
                    //Show Alert message and return

                    //You have to optimize it
                    /*new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();
                                    Toast.makeText(getContext(),"Please provide required field",Toast.LENGTH_SHORT).show();


                                }
                            }, 1000);*/
                    SharedData.endProgress(getContext(),"Please provide required field");
                }

                //Start
                else{
                    Log.d("BuyerOrderResponse","Showing alert message 3");
                    if(boList.isEmpty())
                        boList = "";
                    if(poList.isEmpty())
                        poList = "";


                    makePOListRequest(new VolleyPOListCallback() {
                        @Override
                        public void getListPOResultVolley(final PurchaseOrderModel result) {
                            //Stop the progress bar

                            new android.os.Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            if(result.getOrderPo().size()>0) {

                                                POListFragment  poListFragment = new POListFragment();
                                                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                                                Bundle args = new Bundle();
                                                args.putParcelable("poListParc", result);
                                                poListFragment.setArguments(args);

                                                fragmentTransaction.replace(R.id.nameBO, poListFragment);
                                                fragmentTransaction.addToBackStack("addPoList");
                                                fragmentTransaction.commit();
                                            }
                                            else{
                                                Toast.makeText(getContext(),"Empty result",Toast.LENGTH_SHORT).show();
                                            }
                                            SharedData.endOnlyProgress();


                                        }
                                    }, 3000);


                        }
                    });
                }






               //End
            }
        });
    }

    public void addDateInEditText(String date){
        if(editTextFrom.hasFocus()){
            editTextFrom.setText(date);
        }else if(editTextTo.hasFocus()){
            editTextTo.setText(date);
        }
    }

    private void makePOListRequest(final VolleyPOListCallback volleyCallback){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.getPOListURL.concat("buyerPO="+boList+"&PoCid="+poList+"&fromDate="+fromList+"&toDate="+toList),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("BuyerOrderResponse","My response: "+response);
                        PurchaseOrderModel dataReturn = purchaseOrderPojoResponse.parsePOListJSON(response);
                        volleyCallback.getListPOResultVolley(dataReturn);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BuyerOrderResponse",error.toString());
                error.printStackTrace();
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        VolleySingleton.getInstance(this.getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public interface VolleyPOListCallback{
        void getListPOResultVolley(PurchaseOrderModel result);
    }

}

