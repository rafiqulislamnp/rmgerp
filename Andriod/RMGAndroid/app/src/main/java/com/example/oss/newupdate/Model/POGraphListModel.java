package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OSS on 12/19/2017.
 */

public class POGraphListModel implements Parcelable {
    private int Day;

    public int getDay() {
        return Day;
    }

    public void setDay(int day) {
        Day = day;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    private int Count;

    protected POGraphListModel(Parcel in) {
        Day = in.readInt();
        Count = in.readInt();
    }

    public static final Creator<POGraphListModel> CREATOR = new Creator<POGraphListModel>() {
        @Override
        public POGraphListModel createFromParcel(Parcel in) {
            return new POGraphListModel(in);
        }

        @Override
        public POGraphListModel[] newArray(int size) {
            return new POGraphListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Day);
        dest.writeInt(Count);
    }
}
