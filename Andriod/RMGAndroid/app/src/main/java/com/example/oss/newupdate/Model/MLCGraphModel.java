package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by OSS on 1/7/2018.
 */

public class MLCGraphModel implements Parcelable {
    private ArrayList<MLCGraphListModel> masterLc;

    public MLCGraphModel(){

    }

    protected MLCGraphModel(Parcel in) {
        masterLc = in.createTypedArrayList(MLCGraphListModel.CREATOR);
    }

    public static final Creator<MLCGraphModel> CREATOR = new Creator<MLCGraphModel>() {
        @Override
        public MLCGraphModel createFromParcel(Parcel in) {
            return new MLCGraphModel(in);
        }

        @Override
        public MLCGraphModel[] newArray(int size) {
            return new MLCGraphModel[size];
        }
    };

    public ArrayList<MLCGraphListModel> getMasterLc() {
        return masterLc;
    }

    public void setMasterLc(ArrayList<MLCGraphListModel> masterLc) {
        this.masterLc = masterLc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(masterLc);
    }
}
