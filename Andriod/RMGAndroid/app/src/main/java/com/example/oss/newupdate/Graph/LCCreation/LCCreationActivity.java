package com.example.oss.newupdate.Graph.LCCreation;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.example.oss.newupdate.AlertDialog.BOAlertDialogFragment;
import com.example.oss.newupdate.DashBoard.ViewPagerAdapter;
import com.example.oss.newupdate.Graph.MyXAxisValueFormatter;
import com.example.oss.newupdate.Model.MLCGraphPojoResponse;
import com.example.oss.newupdate.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Random;

public class LCCreationActivity extends AppCompatActivity {
    public static final int MLC_DIALOG_FRAGMENT = 1;
    public static final String TITLE = "MasterLC Line Chart";


    private ViewPager mViewPager;
    private Toolbar mToolbar;
    private LCViewPagerAdapter lcViewPagerAdapter;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production_status);

        mToolbar = (Toolbar) findViewById(R.id.lctoolbar);
        setSupportActionBar(mToolbar);
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        setLCViewPager();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

   /* private void showAlertMLCGraph(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Create and show the dialog.
        BOAlertDialogFragment boAlertDialogFragment = new BOAlertDialogFragment ();

        boAlertDialogFragment.setTargetFragment(this, MLC_DIALOG_FRAGMENT);
        boAlertDialogFragment.show(ft, "dialog");
    }*/

    private void setLCViewPager(){
        //Creatign LC View Pager

        Bundle bundle1 = new Bundle();

        bundle1.putInt("SENTLCVALUE1",19);
        bundle1.putInt("SENTLCVALUE2",20);
        bundle1.putInt("SENTLCVALUE3",21);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        lcViewPagerAdapter = new LCViewPagerAdapter(getSupportFragmentManager(),bundle1);
        mViewPager.setAdapter(lcViewPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.lctab);
        mTabLayout.setupWithViewPager(mViewPager);

    }
}
