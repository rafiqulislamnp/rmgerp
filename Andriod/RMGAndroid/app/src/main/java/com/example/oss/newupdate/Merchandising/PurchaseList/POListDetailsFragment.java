package com.example.oss.newupdate.Merchandising.PurchaseList;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.oss.newupdate.Model.PurchaseOrderListModel;
import com.example.oss.newupdate.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class POListDetailsFragment extends Fragment {

    private PurchaseOrderListModel purchaseOrderListModel;

    public POListDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        purchaseOrderListModel = getArguments().getParcelable("poListDetParc");
        return inflater.inflate(R.layout.fragment_polist_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d("PurchaseDetailsFrag","Purchase order Details: "+purchaseOrderListModel.getItem());

    }
}
