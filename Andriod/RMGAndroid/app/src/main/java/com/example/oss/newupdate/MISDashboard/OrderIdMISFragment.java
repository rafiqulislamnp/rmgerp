package com.example.oss.newupdate.MISDashboard;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.oss.newupdate.Model.BuyerNameModel;
import com.example.oss.newupdate.Model.OrderIdListModel;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;

import java.util.ArrayList;

import static com.example.oss.newupdate.Util.SharedData.progressDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderIdMISFragment extends Fragment {

    private ListView lv;
    private SearchView sv;
    private ArrayAdapter<OrderIdListModel> adapter;
    private ArrayList<OrderIdListModel> orderIdListModelArrayList;

    private BuyerNameModel buyerNameModel;

    public OrderIdMISFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_id_mi, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        orderIdListModelArrayList = getArguments().getParcelableArrayList("misBoListParc");

        //BUTTON,LISTVIEW,SEARCHVIEW INITIALIZATIONS
        lv=(ListView) getActivity().findViewById(R.id.listViewMis);
        sv=(SearchView) getActivity().findViewById(R.id.searchViewMis);
        // btn=(Button) mView.findViewById(R.id.dismiss);

        adapter=new ArrayAdapter<OrderIdListModel>(getActivity(), android.R.layout.simple_list_item_1);

        adapter.addAll(orderIdListModelArrayList);

        //SEARCH
        sv.setQueryHint("Search..");
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String txt) {
                // TODO Auto-generated method stub
                return false;
            }
            @Override
            public boolean onQueryTextChange(String txt) {
                // TODO Auto-generated method stub
                adapter.getFilter().filter(txt);
                return false;
            }
        });



        lv.setAdapter(adapter);
    }
}
