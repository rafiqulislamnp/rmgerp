package com.example.oss.newupdate.Notification;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.oss.newupdate.Login.LoginActivity;
import com.example.oss.newupdate.Merchandising.MerchandisingActivity;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;

//  http://www.jsonschema2pojo.org/
public class NotificationActivity extends AppCompatActivity {

    Button buttonSearch;
    EditText editTextFrom;
    EditText editTextTo;
    private Toolbar mToolbar;

    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    public static Boolean saveLogin;

    private MenuItem profileItem;
    private MenuItem logoutItem;
    Place[] myPlacesArray=new Place[]{
            new Place("Art House",78701,"art","This place is tasteful"),
            new Place("Bike Shop",78702,"bike","Cool bike"),
            new Place("Camera Fix",78703,"polaroids","This place is tasteful"),
            new Place("YET Space",78704,"radio","I love this place"),
            new Place("Secret Space Pad",78705,"rocket","Not very secret"),
            new Place("Taylor's Tailor",78706,"scissors","Android rules"),
            new Place("Camera Fix",78703,"polaroids","This place is tasteful"),
            new Place("YET Space",78704,"radio","I love this place"),
            new Place("Secret Space Pad",78705,"rocket","Not very secret"),
            new Place("Taylor's Tailor",78706,"scissors","Android rules")
    };
    private ListView mListView;
    private NotificationListAdapter mPlaceAdapter;
    private String url_link ="https://jsonplaceholder.typicode.com/users";
    //MyPojoResponse myPojoResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        //myPojoResponse = new MyPojoResponse();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        editTextFrom = (EditText) findViewById(R.id.fromDate);
        editTextTo = (EditText) findViewById(R.id.toDate);
        buttonSearch = (Button) findViewById(R.id.searchButton);
        editTextFrom.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    DialogFragment datePickerFragment = new DatePickerNotificationFragment();
                    datePickerFragment.show(getFragmentManager(),"ANY_UNIQUE_KEY");
                }
            }
        });

        editTextTo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    DialogFragment datePickerFragment = new DatePickerNotificationFragment();
                    datePickerFragment.show(getFragmentManager(),"ANY_UNIQUE_KEY");
                }
            }
        });

        mListView= (ListView) findViewById(R.id.myListView);
        mPlaceAdapter=new NotificationListAdapter(getApplicationContext(),R.layout.buyer_order_row,myPlacesArray);
        if(mListView!=null)
        {
            mListView.setAdapter(mPlaceAdapter);
        }

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // getData();
            }
        });

    }

    public void addDateInEditText(String date){
        if(editTextFrom.hasFocus()){
            editTextFrom.setText(date);
        }else if(editTextTo.hasFocus()){
            editTextTo.setText(date);
        }
    }

   /* private void getData(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url_link,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        myPojoResponse.parseJSON(response);
                       // Log.d("codedata",myPojoResponse.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("codedata",error.toString());
                error.printStackTrace();
            }
        });
        VolleySingleton.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);


    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        else if (id == R.id.action_settings) {

            return true;
        }
        /*else if(id==R.id.action_search){

            return true;
        }*/

        else if (id == R.id.action_profile) {
            SharedData.showUserProfile(this);
            return true;
        }
        else if (id == R.id.action_logout) {
            boolean saveLogin = loginPreferences.getBoolean("saveLogin", false);
            Log.d("status","Current status: "+saveLogin);
            if(!saveLogin){
                loginPrefsEditor.remove("useremail");
                loginPrefsEditor.remove("userpassword");
                loginPrefsEditor.commit();
            }
            showProgressBar();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        profileItem=menu.findItem(R.id.action_profile);
        logoutItem=menu.findItem(R.id.action_logout);

        return true;
    }

    private void showProgressBar(){
        final ProgressDialog progressDialog = new ProgressDialog(NotificationActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Logging Out...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        // onLoginFailed();
                        progressDialog.dismiss();
                        showLoginForm();

                    }
                }, 3000);
    }
    private void showLoginForm(){
        finish();
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);

    }
}
