package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OSS on 12/13/2017.
 */

public class BuyerOrderListModel implements Parcelable {



   private String ID;

   private String Item;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }

    public String getTotalPack() {
        return TotalPack;
    }

    public void setTotalPack(String totalPack) {
        TotalPack = totalPack;
    }

    public String getPC() {
        return PC;
    }

    public void setPC(String PC) {
        this.PC = PC;
    }

    public String getQuantityInDz() {
        return QuantityInDz;
    }

    public void setQuantityInDz(String quantityInDz) {
        QuantityInDz = quantityInDz;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }

    public String getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        TotalPrice = totalPrice;
    }

    private String TotalPack;

   private String PC;

   private String QuantityInDz;

   private String UnitPrice;

   private String TotalPrice;

    public BuyerOrderListModel(){
        super();
    }

    public BuyerOrderListModel(Parcel in){
        ID = in.readString();
        Item = in.readString();
        TotalPrice = in.readString();
        PC = in.readString();
        QuantityInDz = in.readString();
        UnitPrice = in.readString();
        TotalPrice = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeString(Item);
        dest.writeString(TotalPrice);
        dest.writeString(PC);
        dest.writeString(QuantityInDz);
        dest.writeString(UnitPrice);
        dest.writeString(TotalPrice);

    }
    public static final Parcelable.Creator<BuyerOrderListModel> CREATOR = new Parcelable.Creator<BuyerOrderListModel>()
    {
        public BuyerOrderListModel createFromParcel(Parcel in)
        {
            return new BuyerOrderListModel(in);
        }
        public BuyerOrderListModel[] newArray(int size)
        {
            return new BuyerOrderListModel[size];
        }
    };
}
