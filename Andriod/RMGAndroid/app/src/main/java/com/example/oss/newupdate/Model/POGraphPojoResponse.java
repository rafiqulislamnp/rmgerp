package com.example.oss.newupdate.Model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by OSS on 12/19/2017.
 */

public class POGraphPojoResponse {
   POGraphModel poGraphModel;

    public POGraphPojoResponse(){
        poGraphModel = new POGraphModel();
    }

    public POGraphModel parsePoGrJSON(String response) {
        Gson gson = new GsonBuilder().create();
        poGraphModel = gson.fromJson(response, POGraphModel.class);
        return poGraphModel;
    }
}
