package com.example.oss.newupdate.Login;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.oss.newupdate.DashBoard.DashBoardActivity;
import com.example.oss.newupdate.Model.User;
import com.example.oss.newupdate.Model.UserPojoResponse;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;
import com.example.oss.newupdate.Volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    String DEFAULT_VALUE=null;
    boolean flag=false;

    EditText _emailText;

    EditText _passwordText;

    Button _loginButton;
/*    private String login_url ="http://103.240.250.149/Romo_test/Home/Index";*/
    private String login_url ="http://103.240.250.149/romo_test/api/Login?";
    //private String login_url ="http://192.168.0.113/romo/api/Login?";


    private CheckBox saveLoginCheckBox;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    public static Boolean saveLogin;


/*    @Bind(R.id.link_signup)
    TextView _signupLink;*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        _emailText = findViewById(R.id.input_email);
        _passwordText = findViewById(R.id.input_password);
        _loginButton = findViewById(R.id.btn_login);
      /*  ButterKnife.bind(this);*/
        //Taking the id of login check box
        saveLoginCheckBox = (CheckBox)findViewById(R.id.saveLoginCheckBox);

        //Checking data saved in share preferences

        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();
        SharedData.isLoggedIn = false;  // This is shared variable. It was used to check if user is Logged in or not

        saveLogin = loginPreferences.getBoolean("saveLogin", false); //By default data will be saved false
        if(saveLogin){
            onLoginSuccess();
        }
        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
                //onLoginSuccess();
            }
        });

/*        sharedPreferences=getSharedPreferences("MyData", getApplicationContext().MODE_PRIVATE);
        if(!SharedData.isLoggedIn && saveLogin){
            SharedData.userEmail=sharedPreferences.getString("useremail",DEFAULT_VALUE);
            SharedData.userPassword=sharedPreferences.getString("userpassword",DEFAULT_VALUE);
        }
        editor = sharedPreferences.edit();*/


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

       /* if (saveLogin == true) {
            Log.d("lifecycle","Current User email: "+SharedData.userEmail);
            Log.d("lifecycle","Current User password: "+SharedData.userPassword);
            _emailText.setText(SharedData.userEmail);
            _passwordText.setText(SharedData.userPassword);
            saveLoginCheckBox.setChecked(true);
        }*/
    }

    public void login() {

        _loginButton.setEnabled(false);

        // Start Progress bar

        SharedData.startProgress(this,"Authenticating...");

        //Retrieve the value from login text field

        final String email = _emailText.getText().toString();
        final String password = _passwordText.getText().toString();


        // If checkbox is checked

        String totalConcat = email+"-"+password;

        makeLoginRequest(new SharedData.VolleyCallback() {
            @Override
            public void getResultVolley(int result) {
                Log.d("CurrentResult","Current Login result: "+ result);
                if(result==1){
                    //Successful login
                    onLoginSuccess();
                }else{
                    //Unsuccessful login
                    onLoginFailed();
                }
            }
        },totalConcat);

        //End progress
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        // onLoginFailed();
                        SharedData.endOnlyProgress();

                    }
                }, 3000);
    }

    private void makeLoginRequest(final SharedData.VolleyCallback volleyCallback, final String totalConcat){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, login_url.concat("userAndPass="+totalConcat),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int dataReturn = Integer.parseInt(response);
                        Log.d("CurrentResult","Current Login result inside: "+ response);
                        volleyCallback.getResultVolley(dataReturn);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("CurrentResponse",error.toString());
                error.printStackTrace();
            }
        })/*{@Override
        public Map < String, String > getHeaders() throws AuthFailureError {
            HashMap < String, String > headers = new HashMap < String, String > ();
            String encodedCredentials = Base64.encodeToString("passwordandlogin".getBytes(), Base64.NO_WRAP);
            //headers.put("Authorization", "Basic " + encodedCredentials);
            headers.put("Token", "1234");
            headers.put("userName",email);
            headers.put("pass",password);

            return headers;
        }
        }*/;

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

       VolleySingleton.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the GraphActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);

        if(saveLoginCheckBox.isChecked()){
            loginPrefsEditor.putBoolean("saveLogin",true);
            loginPrefsEditor.commit();
        }

        /*Intent intent=new Intent(this,GraphActivity.class);*/
        Intent intent=new Intent(this,DashBoardActivity.class);
        startActivity(intent);
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean loginValidation() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    // Callback interface




}
