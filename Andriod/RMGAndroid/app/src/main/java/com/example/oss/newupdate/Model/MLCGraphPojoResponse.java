package com.example.oss.newupdate.Model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by OSS on 1/7/2018.
 */

public class MLCGraphPojoResponse {
    MLCGraphModel mlcGraphModel;

    public MLCGraphPojoResponse(){
        mlcGraphModel = new MLCGraphModel();
    }

    public MLCGraphModel parseMLcGrJSON(String response) {
        Gson gson = new GsonBuilder().create();
        mlcGraphModel = gson.fromJson(response, MLCGraphModel.class);
        //Log.d("CurrentResult","Current result in string: "+mlcGraphModel.getMasterLc().size());
        return mlcGraphModel;
    }

}
