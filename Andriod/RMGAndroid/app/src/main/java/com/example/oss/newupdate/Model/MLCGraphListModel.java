package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OSS on 1/7/2018.
 */

public class MLCGraphListModel implements Parcelable{
    private int Day;

    protected MLCGraphListModel(Parcel in) {
        Day = in.readInt();
        Value = in.readInt();
    }

    public static final Creator<MLCGraphListModel> CREATOR = new Creator<MLCGraphListModel>() {
        @Override
        public MLCGraphListModel createFromParcel(Parcel in) {
            return new MLCGraphListModel(in);
        }

        @Override
        public MLCGraphListModel[] newArray(int size) {
            return new MLCGraphListModel[size];
        }
    };

    public int getDay() {
        return Day;
    }

    public void setDay(int day) {
        Day = day;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }

    private int Value;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Day);
        dest.writeInt(Value);
    }
}
