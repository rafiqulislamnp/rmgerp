package com.example.oss.newupdate.MISDashboard;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.oss.newupdate.Graph.GraphActivity;
import com.example.oss.newupdate.Graph.LCCreation.LCCreationActivity;
import com.example.oss.newupdate.Login.LoginActivity;
import com.example.oss.newupdate.Merchandising.BuyerList.BOListFragment;
import com.example.oss.newupdate.Merchandising.BuyerOrderFragment;
import com.example.oss.newupdate.Merchandising.MerchandisingActivity;
import com.example.oss.newupdate.Merchandising.PlayersFragment;
import com.example.oss.newupdate.Model.OrderIdListModel;
import com.example.oss.newupdate.Model.OrderIdModel;
import com.example.oss.newupdate.Model.OrderIdPojoResponse;
import com.example.oss.newupdate.Notification.NotificationActivity;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;
import com.example.oss.newupdate.Volley.VolleySingleton;

import java.util.ArrayList;

import static com.example.oss.newupdate.Merchandising.BuyerOrderFragment.Order_DIALOG_FRAGMENT;

public class MISDashActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private MenuItem profileItem;
    private MenuItem logoutItem;

    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private ArrayList<OrderIdListModel> orderIdListModelArrayList;
    private OrderIdPojoResponse orderIdPojoResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_misdash);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();
        orderIdPojoResponse = new OrderIdPojoResponse();


    }

    @Override
    protected void onResume() {
        super.onResume();

        MISDashFragment misDashFragment = new MISDashFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.nameMISDash,misDashFragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void popBackStactFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        if(fragmentManager.getBackStackEntryCount()==0){
            finish();
        }
        fragmentManager.popBackStack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {

            popBackStactFragment();
            return true;
        }

        else if (id == R.id.action_settings) {

            return true;
        }
        /*else if(id==R.id.action_search){

            return true;
        }*/

        else if (id == R.id.action_profile) {
            SharedData.showUserProfile(this);
            return true;
        }
        else if (id == R.id.action_logout) {
            boolean saveLogin = loginPreferences.getBoolean("saveLogin", false);
            if(!saveLogin){
                loginPrefsEditor.remove("useremail");
                loginPrefsEditor.remove("userpassword");
                loginPrefsEditor.commit();
            }
            showProgressBar();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        profileItem=menu.findItem(R.id.action_profile);
        logoutItem=menu.findItem(R.id.action_logout);

        return true;
    }

    private void showProgressBar(){
        final ProgressDialog progressDialog = new ProgressDialog(MISDashActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Logging Out...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        // onLoginFailed();
                        progressDialog.dismiss();
                        showLoginForm();

                    }
                }, 3000);
    }

    private void showLoginForm(){
        finish();
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    public void gridItemOnClickListener(View v, int id){
        if(id == R.drawable.management){
            /*Intent intent = new Intent(this,MerchandisingActivity.class);
            intent.putExtra("currentFrag",1);
            startActivity(intent);*/
            SharedData.startProgress(this, "Retrieving OrderId...");
            getAllOrderId();
        }
        else if(id == R.drawable.store){

        }
        else if(id == R.drawable.accounting){

        }
        else if(id == R.drawable.procurement){

        }
    }

    //Extra part for maintaining every fragment
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        // Disable going back to the GraphActivity
        popBackStactFragment();
        /*if(!SharedData.hideSoftKeyboard(this)){
            popBackStactFragment();
        }*/

        Log.d("KeyboardOpen","Keyboard is open");
       // moveTaskToBack(true);
    }

    private void getAllOrderId(){
        makeOrderIdListRequest(new VolleyMisOrderIdListCallback() {
            @Override
            public void getmisOrderListResultVolley(final OrderIdModel result) {
                //Stop the progress bar

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                if(result.getOrderID().size()>0) {
                                    orderIdListModelArrayList = result.getOrderID();

                                    OrderIdMISFragment orderIdMISFragment = new OrderIdMISFragment();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                                    Bundle args = new Bundle();
                                    args.putParcelableArrayList("misBoListParc", orderIdListModelArrayList);
                                    orderIdMISFragment.setArguments(args);

                                    fragmentTransaction.replace(R.id.nameMISDash, orderIdMISFragment);
                                    fragmentTransaction.addToBackStack("addOIdList");
                                    fragmentTransaction.commit();

                                    Log.d("CurrentMisOrderIdList","Current MIS Order Id List Model ArrayList Size: "+orderIdListModelArrayList.size());

                                }else{
                                    Toast.makeText(getApplicationContext(),"There is no order",Toast.LENGTH_SHORT).show();
                                }

                                SharedData.endOnlyProgress();

                            }
                        }, 3000);


            }
        });
    }

    private void makeOrderIdListRequest(final VolleyMisOrderIdListCallback volleyCallback){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.getOrderId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        OrderIdModel dataReturn = orderIdPojoResponse.parseOrderIdJSON(response);
                        volleyCallback.getmisOrderListResultVolley(dataReturn);
                        SharedData.endOnlyProgress(2000);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BuyerOrderResponse",error.toString());
                error.printStackTrace();
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        VolleySingleton.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public interface VolleyMisOrderIdListCallback{
        void getmisOrderListResultVolley(OrderIdModel result);
    }

}
