package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by OSS on 12/16/2017.
 */

public class BwBoGraphModel implements Parcelable {
    private ArrayList<BwBoGraphListModel> BuyerName;

    public ArrayList<BwBoGraphListModel> getBuyerName() {
        return BuyerName;
    }

    public void setBuyerName(ArrayList<BwBoGraphListModel> buyerName) {
        BuyerName = buyerName;
    }

    protected BwBoGraphModel(Parcel in) {
        BuyerName = in.createTypedArrayList(BwBoGraphListModel.CREATOR);
    }

    public BwBoGraphModel(){

    }

    public static final Creator<BwBoGraphModel> CREATOR = new Creator<BwBoGraphModel>() {
        @Override
        public BwBoGraphModel createFromParcel(Parcel in) {
            return new BwBoGraphModel(in);
        }

        @Override
        public BwBoGraphModel[] newArray(int size) {
            return new BwBoGraphModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(BuyerName);
    }
}
