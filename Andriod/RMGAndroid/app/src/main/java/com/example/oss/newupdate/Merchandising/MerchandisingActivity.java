package com.example.oss.newupdate.Merchandising;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.oss.newupdate.Login.LoginActivity;
import com.example.oss.newupdate.Merchandising.BuyerList.BOListFragment;
import com.example.oss.newupdate.Merchandising.BuyerList.POListFragment;
import com.example.oss.newupdate.Model.BuyerOrderPojoResponse;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;

public class MerchandisingActivity extends AppCompatActivity {
   /* private MerchandisePagerAdapter merchandisePagerAdapter;*/
   /* private ViewPager mViewPager;
    private TabLayout mTabLayout;*/
    private Toolbar mToolbar;
    private MenuItem profileItem;
    private MenuItem logoutItem;

    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    public static Boolean saveLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchandising);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        int getData = getIntent().getIntExtra("currentFrag",1);
        if(getData==1){
            BuyerOrderFragment buyerOrderFragment = new BuyerOrderFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.nameBO,buyerOrderFragment);
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
        else if(getData==2){
            PurchaseOrderFragment purchaseOrderFragment = new PurchaseOrderFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.nameBO,purchaseOrderFragment);
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }


        //setViewPager();
    }

    /*private void setViewPager() {

        mViewPager = (ViewPager) findViewById(R.id.pager);
        merchandisePagerAdapter = new MerchandisePagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(merchandisePagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tab);
        mTabLayout.setupWithViewPager(mViewPager);
    }*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            if(fragmentManager.getBackStackEntryCount()==0){
                finish();
            }
            fragmentManager.popBackStack();
            //Log.d("PopBackStack","Fragment count: "+fragmentManager.getBackStackEntryCount());
            return true;
        }

        else if (id == R.id.action_settings) {

            return true;
        }
        /*else if(id==R.id.action_search){

            return true;
        }*/

        else if (id == R.id.action_profile) {
            SharedData.showUserProfile(this);
            return true;
        }
        else if (id == R.id.action_logout) {
            boolean saveLogin = loginPreferences.getBoolean("saveLogin", false);
            if(!saveLogin){
                loginPrefsEditor.remove("useremail");
                loginPrefsEditor.remove("userpassword");
                loginPrefsEditor.commit();
            }
            showProgressBar();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        profileItem=menu.findItem(R.id.action_profile);
        logoutItem=menu.findItem(R.id.action_logout);

        return true;
    }

    private void showProgressBar(){
        final ProgressDialog progressDialog = new ProgressDialog(MerchandisingActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Logging Out...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        // onLoginFailed();
                        progressDialog.dismiss();
                        showLoginForm();

                    }
                }, 3000);
    }
    private void showLoginForm(){
        finish();
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
}
