package com.example.oss.newupdate.DashBoard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.oss.newupdate.DashBoard.DashBoardActivity;
import com.example.oss.newupdate.R;

/**
 * Created by OSS on 9/20/2017.
 */

public class GridAdapter extends BaseAdapter {
    Context context;
    private final String[] values;
    private final int[] images;
    View view;
    LayoutInflater layoutInflater;
    TextView textView;
    ImageView imageView;
    int[] valuesForImages ={R.drawable.search, R.drawable.searchpo,R.drawable.bar_chart,R.drawable.buyerorderlist,R.drawable.production,R.drawable.manager};
    public GridAdapter(Context context, String[] values, int[] images) {
        this.context = context;
        this.values = values;
        this.images = images;
    }

    @Override

    public int getCount() {
        return values.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if(convertView==null){
            view = new View(context);
            view = layoutInflater.inflate(R.layout.single_item,null);
            textView = (TextView) view.findViewById(R.id.textView);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            textView.setText(values[position]);
            imageView.setImageResource(images[position]);

        }
        final int id = valuesForImages[position];
        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof DashBoardActivity){
                    ((DashBoardActivity)context).gridItemOnClickListener(v,id);
                }
            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof DashBoardActivity){
                    ((DashBoardActivity)context).gridItemOnClickListener(v,id);
                }
            }
        });

        return view;
    }


}
