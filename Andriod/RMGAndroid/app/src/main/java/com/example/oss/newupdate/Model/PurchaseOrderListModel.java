package com.example.oss.newupdate.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by OSS on 12/18/2017.
 */

public class PurchaseOrderListModel implements Parcelable {
    private String Item;
    private String TotalPOValue;

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }

    public String getTotalPOValue() {
        return TotalPOValue;
    }

    public void setTotalPOValue(String totalPOValue) {
        TotalPOValue = totalPOValue;
    }

    public String getPOID() {
        return POID;
    }

    public void setPOID(String POID) {
        this.POID = POID;
    }

    private String POID;

    protected PurchaseOrderListModel(Parcel in) {
        Item = in.readString();
        TotalPOValue = in.readString();
        POID = in.readString();
    }

    public static final Creator<PurchaseOrderListModel> CREATOR = new Creator<PurchaseOrderListModel>() {
        @Override
        public PurchaseOrderListModel createFromParcel(Parcel in) {
            return new PurchaseOrderListModel(in);
        }

        @Override
        public PurchaseOrderListModel[] newArray(int size) {
            return new PurchaseOrderListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Item);
        dest.writeString(TotalPOValue);
        dest.writeString(POID);
    }
}
