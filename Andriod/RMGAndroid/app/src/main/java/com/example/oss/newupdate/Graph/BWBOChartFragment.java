package com.example.oss.newupdate.Graph;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.oss.newupdate.AlertDialog.AlertDialogFragment;
import com.example.oss.newupdate.Model.BuyerNameModel;
import com.example.oss.newupdate.Model.BuyerNamePojoResponse;
import com.example.oss.newupdate.Model.BwBoGrPojoResponse;
import com.example.oss.newupdate.Model.BwBoGraphListModel;
import com.example.oss.newupdate.Model.BwBoGraphModel;
import com.example.oss.newupdate.R;
import com.example.oss.newupdate.Util.SharedData;
import com.example.oss.newupdate.Volley.VolleySingleton;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Random;

public class BWBOChartFragment extends Fragment {
    public static final int DIALOG_FRAGMENT = 1;
    public static final String TITLE = "BuyerWiseBuyerOrder";
    BarChart barChart;
    ArrayList<String> dates;
    Random random;
    ArrayList<BarEntry> barEntries;  //Y axis data
    private Bundle barBundle;
    private int barData=0;
    private ImageButton graphRetrieve;
    private BuyerNamePojoResponse buyerNamePojoResponse;
    private BwBoGrPojoResponse bwBoGrPojoResponse;
    private ArrayList<BwBoGraphListModel> bwBoGraphListModelArrayList;
    private ProgressDialog progressDialog;


    public static BWBOChartFragment newInstance() {

        return new BWBOChartFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        barBundle = getArguments();
        barData = barBundle.getInt("SENTVALUE3");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        buyerNamePojoResponse = new BuyerNamePojoResponse();
        bwBoGrPojoResponse = new BwBoGrPojoResponse();

        barChart = (BarChart) getView().findViewById(R.id.bargraph);
        graphRetrieve = getView().findViewById(R.id.graphRetrieve);



        // Graph data retrieve on click listener
        graphRetrieve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //Fetch data from server [Buyer Name]
                makeBNListRequest(new VolleyBNListCallback() {
                    @Override
                    public void getBNListResultVolley(BuyerNameModel result) {
                        showAlertGraph(result);
                    }
                });

            }
        });

    }

    private void showAlertGraph(BuyerNameModel result){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        // Create and show the dialog.
        AlertDialogFragment alertDialogFragment = new AlertDialogFragment ("BuyerWise Buyer Order");

        Bundle args = new Bundle();
        args.putString("pickerStyle", "fancy");
        args.putParcelable("buyerNameList",result);
        alertDialogFragment.setArguments(args);
        alertDialogFragment.setTargetFragment(this, DIALOG_FRAGMENT);
        alertDialogFragment.show(ft, "dialog");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case DIALOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String getResult = bundle.getString("monthBuyerName");
                   // Log.d("CurrentStatus","CurrentLogin "+ getResult);
                   // makeBwBoGraphRequest(final VolleyBWBOGraphCallback volleyBWBOGraphCallback, getResult);

                        makeBwBoGraphRequest(new VolleyBWBOGraphCallback() {
                            @Override
                            public void getBWBOGrResultVolley(BwBoGraphModel result) {
                                bwBoGraphListModelArrayList = result.getBuyerName();
                                if(bwBoGraphListModelArrayList.size()>0){

                                    progressDialog = new ProgressDialog(getActivity(),
                                            R.style.AppTheme_Dark_Dialog);
                                    progressDialog.setIndeterminate(true);
                                    progressDialog.setMessage("Please wait...");
                                    progressDialog.show();

                                    showGraphData();
                                }

                            }
                        }, getResult);


                } else if (resultCode == Activity.RESULT_CANCELED) {

                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Fetch graph data from server

   private void makeBwBoGraphRequest(final VolleyBWBOGraphCallback volleyBWBOGraphCallback, String getResult){
       Log.d("CheckGraphData","Inside Volley 0: "+getResult);
       StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.getBWBOGraph.concat(getResult),
               new Response.Listener<String>() {
                   @Override
                   public void onResponse(String response) {
                     /*   Log.d("BuyerOrderResponse","My response: "+response);*/
                       BwBoGraphModel bwBoGraphModel = bwBoGrPojoResponse.parseBwBoGrJSON(response);
                       Log.d("CheckGraphData","Inside Volley 1: "+bwBoGraphModel.getBuyerName().size());
                       volleyBWBOGraphCallback.getBWBOGrResultVolley(bwBoGraphModel);
                   }
               }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {
               error.printStackTrace();
               Log.d("CheckGraphData", error.toString());
           }
       });

       stringRequest.setRetryPolicy(new RetryPolicy() {
           @Override
           public int getCurrentTimeout() {
               return 50000;
           }

           @Override
           public int getCurrentRetryCount() {
               return 50000;
           }

           @Override
           public void retry(VolleyError error) throws VolleyError {

           }
       });

       VolleySingleton.getInstance(this.getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }



    //Fetch all buyer name from server
    private void makeBNListRequest(final VolleyBNListCallback volleyBNCallback){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SharedData.getAllBuyerName,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     /*   Log.d("BuyerOrderResponse","My response: "+response);*/
                        BuyerNameModel buyerNameModel = buyerNamePojoResponse.pojoResponseBuyerName(response);
                        volleyBNCallback.getBNListResultVolley(buyerNameModel);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        VolleySingleton.getInstance(this.getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //Show Graph

    private void showGraphData(){

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progressDialog.dismiss();

                    }
                }, 2000);

        //Start
        Log.d("CheckGraphData","Showing graph data: "+bwBoGraphListModelArrayList.size());
        /*ArrayList<String> months = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();*/

        String[] monthsInArray = new String[16];
        //String[] valuesInArray = new String[14];


        int i=0;
        ArrayList<BarEntry> barEntries =new ArrayList<>();

        for (BwBoGraphListModel bwBoGraphListModel : bwBoGraphListModelArrayList) {
            // fruit is an element of the `fruits` array.
           /* months.add(String.valueOf(bwBoGraphListModel.getMonth()));
            values.add(String.valueOf(bwBoGraphListModel.getValue()));*/

            monthsInArray[i] = String.valueOf(bwBoGraphListModel.getMonth());
            //valuesInArray[i] = String.valueOf(bwBoGraphListModel.getValue());

            barEntries.add(new BarEntry(i,bwBoGraphListModel.getValue()));

            Log.d("CheckGraphData","Months: "+monthsInArray[i]);
            Log.d("CheckGraphData","Values: "+bwBoGraphListModel.getValue());
            i++;

        }

        BarDataSet dataSets = new BarDataSet(barEntries,"Orders");

        dataSets.setColor(Color.RED);


        //BarData barData = new BarData(dataSets,dataSets1,dataSets2);
        BarData barData = new BarData(dataSets);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new MyXAxisValueFormatter(monthsInArray));

        //End


        /*barData.setValueFormatter(new LargeValueFormatter());*/
        barChart.setData(barData);
        barChart.animateY(1500);
        barChart.setTouchEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setScaleEnabled(true);
    }



    public interface VolleyBNListCallback{
        void getBNListResultVolley(BuyerNameModel result);
    }
    public interface VolleyBWBOGraphCallback{
        void getBWBOGrResultVolley(BwBoGraphModel result);
    }
}