package com.example.oss.newupdate.DashBoard;

/**
 * Created by OSS on 9/23/2017.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.oss.newupdate.Graph.BOChartFragment;
import com.example.oss.newupdate.Graph.BWBOChartFragment;
import com.example.oss.newupdate.Graph.POLineChartFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private static int TAB_COUNT = 3;
    private final Bundle fragmentBundle1;
    public ViewPagerAdapter(FragmentManager fm, Bundle bundleFrag1) {
        super(fm);
        fragmentBundle1 = bundleFrag1;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                BWBOChartFragment BWBOChartFragment = com.example.oss.newupdate.Graph.BWBOChartFragment.newInstance();
                BWBOChartFragment.setArguments(fragmentBundle1);
                return BWBOChartFragment;
            case 1:
                BOChartFragment BOChartFragment = com.example.oss.newupdate.Graph.BOChartFragment.newInstance();
                BOChartFragment.setArguments(fragmentBundle1);
                return BOChartFragment;
            case 2:
                POLineChartFragment POLineChartFragment = com.example.oss.newupdate.Graph.POLineChartFragment.newInstance();
                POLineChartFragment.setArguments(fragmentBundle1);
                return POLineChartFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return BWBOChartFragment.TITLE;

            case 1:
                return BOChartFragment.TITLE;

            case 2:
                return POLineChartFragment.TITLE;
        }
        return super.getPageTitle(position);
    }
}
