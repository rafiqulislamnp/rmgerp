package com.example.oss.newupdate.Model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by OSS on 12/13/2017.
 */

public class BuyerOrderPojoResponse {
    /*List<BuyerOrderListModel> buyerOrder;*/
    BuyerOrderModel buyerOrderModel;
    public BuyerOrderPojoResponse(){
       // buyerOrder = new ArrayList<BuyerOrderListModel>();
        buyerOrderModel = new BuyerOrderModel();
    }

    public BuyerOrderModel parseBOListJSON(String response) {
        Gson gson = new GsonBuilder().create();
        //BuyerOrderPojoResponse myPojoResponse = gson.fromJson(response, BuyerOrderPojoResponse.class);
        //buyerOrderModelList = Arrays.asList(gson.fromJson(response, BuyerOrderListModel[].class));
        //Type collectionType = new TypeToken<Collection<BuyerOrderListModel>>(){}.getType();
        //Collection<BuyerOrderListModel> enums = gson.fromJson(response, collectionType);
        //List<BuyerOrderListModel> founderArray = gson.fromJson(response, BuyerOrderListModel);
        buyerOrderModel = gson.fromJson(response, BuyerOrderModel.class);
        //buyerOrder = buyerOrderModel.getBuyerOrder();
        //Log.d("BuyerOrderResponse", String.valueOf(" Count: "+buyerOrderModel.getBuyerOrder().size() + " And Name: "+buyerOrderModel.getBuyerOrder().get(0).getItem()));
       // buyerOrderModelList =(List<BuyerOrderListModel>) enums;

        return buyerOrderModel;
    }
}
