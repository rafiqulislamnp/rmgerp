namespace Oss.Romo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class habubulBasher : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Acc_SupplierAdjustment",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Common_SupplierFk = c.Int(nullable: false),
                        AdjustmentAmmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(maxLength: 100),
                        Active = c.Boolean(),
                        FirstCreated = c.DateTime(precision: 7, storeType: "datetime2"),
                        LastEdited = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Acc_SupplierAdjustment");
        }
    }
}
