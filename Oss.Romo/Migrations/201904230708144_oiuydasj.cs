namespace Oss.Romo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oiuydasj : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Acc_SupplierAdjustment", "FirstCreatedBy", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Acc_SupplierAdjustment", "FirstCreatedBy");
        }
    }
}
