namespace Oss.Romo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oiuydasjiuio : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Acc_SupplierAdjustment", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Acc_SupplierAdjustment", "Date");
        }
    }
}
