﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Http;
using Oss.Romo.ViewModels.User;
namespace Oss.Romo
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public const bool IsReleased = true;
        public static string FilePrefix;
        public HttpCookie requestUrl;
        public static string userName = "";

        protected void Application_Start()
        {
            if (IsReleased)
            {
                FilePrefix = "~/Romo/";
            }
            else
            {
                FilePrefix = "~/";
            }

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            string s = "";

            if (exc is HttpUnhandledException)
            {
                s = exc.Message;
            }
            Server.ClearError();
        }       
       
        //void Session_End()
        //{
        //    if (Request.Cookies["RequestUrl"] != null)
        //    {
        //        Response.Cookies.Clear();
        //    }
           
        //}

        //void Application_PostRequestHandlerExecute(Object source, EventArgs e)
        //{
        //    string session = userName;
        //    if (session == null)
        //    {
        //        HttpApplication app = (HttpApplication)source;
        //        HttpContext context = app.Context;

        //        string requestValue = FirstRequestInitialisation.Initialise(context);

        //        requestUrl = new HttpCookie("RequestUrl");
        //        requestUrl.Value = requestValue;
        //        Response.Cookies.Add(requestUrl);
        //    }
        //}

        //class FirstRequestInitialisation
        //{
        //    private static string absoluteUrl = null;

        //    // Initialise only on the first request
        //    public static string Initialise(HttpContext context)
        //    {
                
        //        if (string.IsNullOrEmpty(absoluteUrl))
        //        {
        //            Uri urid = HttpContext.Current.Request.Url;
        //            absoluteUrl = urid.AbsolutePath;
        //        }
        //        return absoluteUrl;
        //    }
        //}
    }

}
