﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Mis
{
    public class Mis_OrderStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [DefaultValue(0)]
        public string CID { get; set; }
        [MaxLength(80)]
        public string Style { get; set; }
        [DisplayName("Values")]
        [DefaultValue(0)]
        public decimal OrderValues { get; set; }
        [DefaultValue(0)]
        public decimal BOMValues { get; set; }
        [DefaultValue(0)]
        public decimal QuantityPC { get; set; }
        [DefaultValue(0)]
        public decimal CM { get; set; }
        [DefaultValue(0)]
        public decimal Cost { get; set; }
        [DefaultValue(0)]
        public decimal BEP { get; set; }
        [DefaultValue(0)]
        public decimal SMV { get; set; }
        [DefaultValue(0)]
        public decimal Exfactory { get; set; }
        [DefaultValue(0)]
        public decimal FinishQuantity { get; set; }
    }
}