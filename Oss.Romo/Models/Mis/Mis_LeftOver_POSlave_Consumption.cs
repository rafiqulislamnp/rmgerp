﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Mis
{
    public class Mis_LeftOver_POSlave_Consumption
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [DisplayName("Requisition No")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [Required(ErrorMessage = "Requisition number is required")]
        public string Requisition { get; set; }
        [DisplayName("Total Quantity")]
        [DefaultValue(0)]
        public decimal? Quantity { get; set; }
        [DisplayName("Supervisor Name")]
        public string Supervisor { get; set; }
        public bool? IsReturn { get; set; }
        public int? Mkt_POSlaveFK { get; set; }

        public int? Prod_TransitionItemInventoryFK { get; set; }

        public int? Mkt_POExtTransferFK { get; set; }

        public int Raw_InternaleTransferSlaveFk { get; set; }

        public int? Prod_Requisition_SlaveFK { get; set; }

        public int? User_DepartmentFk { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

    }
}