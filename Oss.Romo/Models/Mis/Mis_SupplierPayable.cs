﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Mis
{
    public class Mis_SupplierPayable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [DisplayName("Supplier ID")]
        [DefaultValue(0)]
        public int SupplierId { get; set; }
        [DefaultValue(0)]
        [DisplayName("Payable")]
        public decimal Payable { get; set; }
        public DateTime EntryDate { get; set; }
    }
}