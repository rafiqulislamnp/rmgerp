﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Mis
{
    public class Mis_BuyerReceivable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [DisplayName("Buyer ID")]
        [DefaultValue(0)]
        public int BuyerId { get; set; }
        [DefaultValue(0)]
        [DisplayName("Receivable")]
        public decimal Receivable { get; set; }
        public DateTime EntryDate { get; set; }
    }
}