﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Mis
{
    public class Mis_AtaGlance
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [DisplayName("SL")]
        [DefaultValue(0)]
        public int SL { get; set; }
        [MaxLength(80)]
        [DisplayName("Title")]
        public string Title { get; set; }
        [DisplayName("Values")]
        [DefaultValue(0)]
        public decimal Values { get; set; }
        public DateTime EntryDate { get; set; }
        
        
    }
}