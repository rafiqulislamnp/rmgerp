﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Services;

namespace Oss.Romo.Models.ViewModels.HRMS
{
    public class VM_District:RootModel
    {

        [DisplayName("District Name")]
        [Required(ErrorMessage = "Please Select District")]
        public string DistrictName { get; set; }

        public ICollection<VM_District> CollectionVmDistrict { get; set; }
        xOssContext db = new xOssContext();

        public void GetDistrictName()
        {
            var district = (from l in db.District
                select new VM_District()
                {
                    ID = l.ID,
                    DistrictName = l.DistrictName
                }).ToList();

            CollectionVmDistrict = district;
        }
    }
}