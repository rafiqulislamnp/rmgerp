﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using Oss.Romo.Models.Services;

namespace Oss.Romo.Models.ViewModels.HRMS
{
    
    public class VMUnit:RootModel
    {
        private xOssContext db = new xOssContext();

        [DisplayName("Unit Name")]
        [Required(ErrorMessage = "Please Enter Unit")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Unit Name should be minimum 3 characters and maximum 50 characters")]
        public string UnitName { get; set; }

        [DisplayName("Business Unit Name")]
        [Required(ErrorMessage = "Please Select Business Unit")]
        public int BusinessUnit_SL { get; set; }
        public string BusinessUnit_Name { get; set; }

        public IEnumerable<VMUnit> DataListUnit { get; set; }


        public void GetUnitList(int bUnitId)
        {
            var unitList = db.Units.Where(u => u.ID == bUnitId).ToList();
        }
        public string GetDegreeName(int id)
        {
            var s = db.EducationDegree.Find(id);
            return s.Name;
        }
    }
}