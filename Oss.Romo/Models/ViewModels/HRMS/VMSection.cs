﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Services;

namespace Oss.Romo.Models.ViewModels.HRMS
{
    public class VMSection:RootModel
    {
        [DisplayName("Section")]
        [Required(ErrorMessage = "Please Enter Section")]
        [StringLength(50, ErrorMessage = "Section Name should be minimum 3 characters and maximum 80 characters")]
        public string SectionName { get; set; }
        [DisplayName("Department")]
        public string DeptName { get; set; }
        [DisplayName("Department")]
        public int Dept_ID { get; set; }
        [ForeignKey("Dept_ID")]
        public HRMS_Department Department { get; set; }


        public IEnumerable<VMSection> DataListSection { get; set; }

    }
}