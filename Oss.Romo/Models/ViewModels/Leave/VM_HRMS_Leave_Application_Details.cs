﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Entity.HRMS;
using Oss.Romo.Models.Services;

namespace Oss.Romo.Models.ViewModels.Leave
{
    public class VM_HRMS_Leave_Application_Details
    {
        public int Leave_ApplicationId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Start_Date { get; set; }

        [DataType(DataType.Date)]
        public DateTime End_Date { get; set; }
        public int TotalDays { get; set; }
        [StringLength(200)]
        public string Comments { get; set; }
        public int LeaveStatus { get; set; }  //--In-View Dropdown----

        public ICollection<VM_HRMS_Leave_Application_Details> LeaveApplicationDetailses { get; set; }
        private xOssContext db = new xOssContext();
        public void GetLeaveApplicationDetails()
        {
            
        }
    }
}