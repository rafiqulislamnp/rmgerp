﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;
using Oss.Romo.Models.Services;
using Oss.Romo.Models.ViewModels.Attendance;
using Oss.Romo.Models.ViewModels.Reports;
using Oss.Romo.Helper;

namespace Oss.Romo.Models.ViewModels.Leave
{
    public class VMLeaveType
    {
        public int LeaveTypeId { get; set; }
        public int AssignLeaveQty { get; set; }
        public int TakenLeaveQty { get; set; }
    }
    public class VM_HRMS_Leave_Application : RootModel
    {
        private xOssContext db;
        public VM_HRMS_Leave_Application()
        {
            db = new xOssContext();
        }
        public VM_HRMS_Leave_Application VmLeaveApplication { get; set; }
        public HRMS_Leave_Application LeaveApplication { get; set; }
        
        public int ApplicationId { get; set; }
        public int LeaveApprovedBy { get; set; }
        [DisplayName("Recommended By")]
        public string RecommendedName { get; set; }
        [DisplayName("Section Incharge")]
        public string SectionInchargeName { get; set; }
        [DisplayName("APM/PM")]
        public string APMorPMName { get; set; }
        [DisplayName("HR Department")]
        public string HRDepartmentName { get; set; }
        [DisplayName("Approved By")]
        public string ApprovedByName { get; set; }
        public HRMS_Leave_Type LeaveType { get; set; }
        [DisplayName("Leave Type")]
        public string Leave_Name { get; set; }

        public HRMS_Employee Employee { get; set; }
        public HRMS_Designation Designation { get; set; }
        [DisplayName("Designation")]
        public string DesignationName { get; set; }
        public HRMS_Section Section { get; set; }
        [DisplayName("Section")]
        public string SectionName { get; set; }
        [DisplayName("Department")]
        public string DepartmentName { get; set; }

        [DisplayName("Employee ID")]
        public int EmployeeId { get; set; }
        public int PreviousLeaveId { get; set; }

        [DisplayName("Employee ID")]
        public string EmployeeIdentity { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Name { get; set; }
        //[Column(TypeName = "NVARCHAR")]
        //[StringLength(100)]
        // [ForeignKey("EmployeeId")]
        // public HRMS_Employee HrmsEmployee { get; set; }

        [DisplayName("Leave Type")]
        public int LeaveTypeId { get; set; }
        
        [DisplayName("From")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime From { get; set; }
        

        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        [DisplayName("To")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime To { get; set; }

        public string From1 { get; set; }
        public string To1 { get; set; }

        [DisplayName("Total Days")]
        public decimal Days { get; set; }
        public decimal Days1 { get; set; }
        public decimal RemainingLeaveDays { get; set; }
        //[DisplayName("Project Name")]
        //[StringLength(50)]
        //public string ProjectName { get; set; }

        //[StringLength(50)]
        //public string WorkStationName { get; set; }
        [DisplayName("Leave Purpose")]
        [StringLength(50)]
        public string Purpose { get; set; }

        [DisplayName("Stay During Leave")]
        [StringLength(50)]
        public string StayDuringLeave { get; set; }

        //[DisplayName("Mobile No")]
        //[StringLength(50)]
        //public string MobileNo { get; set; }

        //[StringLength(50)]
        //public string AltMobileNo { get; set; }
        [DisplayName("Comment")]
        [StringLength(50)]
        public string Comment { get; set; }

        [DisplayName("Joining Work After Leave")]
        [DataType(DataType.Date)]
        public DateTime JoiningWorkAfterLeave { get; set; }

        [DisplayName("Joining Date")]
        [DataType(DataType.Date)]
        public DateTime Joining_date { get; set; }

        [DisplayName("Status")]
        //[StringLength(50)]
        public int LeaveStatus { get; set; }

        //[StringLength(50)]
        //public string During_LeaveWhoTakeOver { get; set; }

        //[StringLength(50)]
        //public string Latest_Period_of_leave_Enjoyed { get; set; }
        [DisplayName("Paid Type")]
        public int LeavePaidType { get; set; }

        //[StringLength(50)]
        //public string Alt_Leave_Dates { get; set; }

        //[StringLength(50)]
        //public string Acknowledgement_Status { get; set; }
        [DisplayName("HR Department")]
        [StringLength(100)]
        public string HRDepartment { get; set; }
        //[DataType(DataType.DateTime)]
        //public DateTime HRAcknowledgeDate { get; set; }

        [DisplayName("Recommended By")]
        [StringLength(50)]
        public string Recommended { get; set; }

        [DisplayName("Section Incharge")]
        [StringLength(50)]
        public string SectionIncharge { get; set; }

        [DisplayName("A.P.M/P.M")]
        [StringLength(50)]
        public string APMorPM { get; set; }

        [DisplayName("Approved By")]
        [StringLength(50)]
        public string ApprovedBy { get; set; }

        public string Lblleavebalance { get; set; }
        [DisplayName("FM")]
        [StringLength(50)]
        public string FM { get; set; }

        [Display(Name = "ApplyDate")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime LeaveApplyDate { get; set; }

        [Display(Name = "LeaveStatus")]
        public string LeaveStatusType { get; set; }

        public List<VMLeaveType> lstLeaveType { get; set; }
        public ICollection<VM_HRMS_Leave_Application> DataList { get; set; }
        public IEnumerable<VM_HRMS_Leave_Application> DataList1 { get; set; }
        
        /// <summary>
        /// Retrieve Applied Leave Details..... 
        /// </summary>
        public void GetLeaveApplicationList()
        {
            var leave = (from t1 in db.HrmsLeaveApplications
                join em in db.Employees on t1.EmployeeId equals em.ID
                join lt in db.HrmsLeaveTypes on t1.LeaveTypeId equals lt.ID
                select new VM_HRMS_Leave_Application()
                {
                    LeaveApplication = t1,
                    Employee = em,
                    LeaveType = lt
                }).ToList();
            DataList = leave;
        }

        /// <summary>
        /// Retrieve Applied Leave Details..... 
        /// </summary>
        public VM_HRMS_Leave_Application GetLeaveApplicantInformation(int id)
        {

            var leave = (from t1 in db.Employees
                join d in db.Designations on t1.Designation_Id equals d.ID
                join s in db.Sections on t1.Section_Id equals s.ID
                where t1.ID == id
                select new VM_HRMS_Leave_Application()
                {
                    Employee = t1,
                    Designation = d,
                    Section = s
                }).FirstOrDefault();
            return leave;
        }

        /// <summary>
        /// Retrieve Applied Leave Details..... 
        /// </summary>
        public VM_HRMS_Leave_Application GetLeaveBalanceInformation(int eid,int lid)
        {
            Lblleavebalance = "This Employee no Leave Assign Yet";
            var year =Convert.ToString(DateTime.Now.Year);
            var leave = (from t1 in db.HrmsLeaveAssigns
                         where (t1.LeaveTypeId == lid && t1.EmployeeId==eid && t1.Year== year)
                select new VM_HRMS_Leave_Application()
                { 
                    Lblleavebalance ="Total Leave "+ t1.Total+" Taken "+t1.Taken+" Balance "+t1.Remaining
                }).FirstOrDefault();
            return leave;
        }
        public DataTable PersonalMonthlyAttendanceSummery(DateTime fromdate, DateTime todate, int employeeId)
        {
            DataTable table = new DataTable();
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Employee_ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("UnitName", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            table.Columns.Add("TotalOT", typeof(string));
            table.Columns.Add("BOT", typeof(string));
            table.Columns.Add("EOT", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name]," +
                                                           "u.UnitName,l.Line,s.SectionName, convert(varchar(10),a.[Date],126) as [Date],CAST(a.Intime AS time) as InTime,CAST(a.[OutTime]  AS time)as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status], " +
                                                           "PayableOverTime AS[TotalOT], " +
                                                           "(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [BOT], " +
                                                           "(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END) as [EOT] " +
                                                           " from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID " +
                                                           "join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID " +
                                                           "Where[Date] between '" + dt1 + "' and '" + dt2 + "' and e.ID =" + employeeId + " ").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].CardTitle, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].SectionName, all_rows[i].UnitName, all_rows[i].Line, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status, all_rows[i].TotalOT, all_rows[i].BOT, all_rows[i].EOT);
            }
            return table;
        }
        public void LoadEmployeeDailyAttendanceData(VM_HRMS_Leave_Application vm)
        {
            // DateDiff dd = new DateDiff();
            var a = new List<VM_HRMS_Leave_Application>();
            if (vm.LeaveApplication.EmployeeId == 0)
            {
                a = db.Database.SqlQuery<VM_HRMS_Leave_Application>(
                    "Exec [dbo].[sp_ViewDateRangeWiseLeaveInfo] @fromDate='" + vm.LeaveApplication.From +
                    "', @todate='" + vm.LeaveApplication.To + "'").ToList();
            }
            else
            {
                a = db.Database.SqlQuery<VM_HRMS_Leave_Application>(
                        "Exec [dbo].[sp_ViewDateRangeWiseSpecificLeaveInfo] @fromDate='" + vm.LeaveApplication.From +
                        "', @todate='" + vm.LeaveApplication.To + "', @EmployeeId='" + vm.LeaveApplication.EmployeeId +
                        "'")
                    .ToList();
            }
            this.DataList = a;
        }
        public VM_HRMS_Leave_Application LoadSpecificLeaveDetails(int LeaveId)
        {
            var a = new VM_HRMS_Leave_Application();
            if (true)
            {
                a = db.Database
                    .SqlQuery<VM_HRMS_Leave_Application>("Exec [dbo].[sp_ViewSpecificLeaveInfoDetails] @LeaveId='" +
                                                         LeaveId + "'").FirstOrDefault();
            }
            return a;
        }
        //ID	EmployeeId	EmployeeIdentity	Name	Designation_Id	Joining_date	DesignationName	Section_Id	SectionName	
        //LeaveTypeId	Leave_Name	Purpose	StayDuringLeave	LeaveStatus	From	To	Days	
        //Recommended	SectionInchargeName	APMorPMName	HRDepartmentName	ApprovedByName	FM	JoiningWorkAfterLeave	LeavePaidType	Comment	RecommendedName
        public VM_HRMS_Leave_Application LoadSpecificEmployeeLeaveforEdit(int? id)
        {
            var att = new VM_HRMS_Leave_Application();
            var leavelist = db.Database
                .SqlQuery<VM_HRMS_Leave_Application>("Exec [dbo].[sp_ViewSpecificLeaveInfoDetails] @LeaveId='" +
                                                     id + "'").ToList();
            att = (from t1 in leavelist
                   where t1.ID == id
                select new VM_HRMS_Leave_Application
                {
                    ID = t1.ID,
                    EmployeeId = t1.EmployeeId,
                    EmployeeIdentity = t1.EmployeeIdentity,
                    Name = t1.Name,
                    Joining_date = t1.Joining_date,
                    DesignationName = t1.DesignationName,
                    SectionName = t1.SectionName,
                    LeaveTypeId = t1.LeaveTypeId,
                    Leave_Name = t1.Leave_Name,
                    Purpose = t1.Purpose,
                    StayDuringLeave = t1.StayDuringLeave,
                    LeaveStatus = t1.LeaveStatus,
                    From = t1.From,
                    To = t1.To,
                    Days = t1.Days,
                    Recommended = t1.Recommended,
                    SectionInchargeName = t1.SectionInchargeName,
                    APMorPMName = t1.APMorPMName,
                    HRDepartmentName = t1.HRDepartmentName,
                    ApprovedByName = t1.ApprovedByName,
                    FM = t1.FM,
                    JoiningWorkAfterLeave = t1.JoiningWorkAfterLeave,
                    LeavePaidType = t1.LeavePaidType,
                    Comment = t1.Comment,
                    RecommendedName = t1.RecommendedName
                }).SingleOrDefault();

            return att;
        }
        ///// <summary>
        ///// Retrieve Applied Leave Details..... 
        ///// </summary>
        //public string GetRemainingLeaveInfo(int id)
        //{
        //    var str = "";
        //    DateTime d = new DateTime();

        //    var leave = (from t1 in db.HrmsLeaveAssigns
        //                  where t1.EmployeeId == id && t1.Year == d.Year.ToString()
        //                 select new VM_HRMS_Leave_Application()
        //        {
        //            LeaveApplication = t1,
        //            Employee = em,
        //            LeaveType = lt
        //        }).ToList();
        //    DataList = leave;
        //}

        //public string GetRemainingLeaveInfo(int id)
        //{
        //    //var str = "";
        //    //DateTime d = new DateTime();
        //    //var query =
        //    //    from t1 in db.HrmsLeaveAssigns.AsEnumerable()
        //    //    where t1.EmployeeId == id && t1.Year == d.Year.ToString()
        //    //    select t1;

        //    //var htm = "<span>You Have Remaining Leave</span>&nbsp;&nbsp;&nbsp;";
        //    //for (int i = 0; i < query.Count; i++)
        //    //{
        //    //}


        //    //DataSet ds = new DataSet();
        //        //

        //        //IEnumerable query =
        //        //    from t1 in db.HrmsLeaveAssigns.AsEnumerable()
        //        //    where t1.EmployeeId == id && t1.Year == d.Year.ToString()
        //        //    select t1;

        //        //DataTable boundTable = query.CopyToDataTable();


        //        return "";
        //}



        //public string GetRemainingLeaveInfo(string empId)
        //{
        //    List<LeaveClass> list = new List<LeaveClass>();
        //    DateTime dt = DateTime.Now;
        //    string resp = "";
        //    var ds =
        //        SqlDataAccess.SQL_ExecuteReader(
        //            "Select distinct [LeaveName],[Total],[Taken],[Remaining] from [dbo].[tblLeaveCategoryFinal] where Employee_ID='" + empId +
        //            "' and Year='" + dt.Year.ToString() + "'", out resp);
        //    var str = "";
        //    var htm = "<span>You Have Remaining Leave</span>&nbsp;&nbsp;&nbsp;";
        //    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        //Session["MaxYear"] = ds.Tables[0].Rows[0][0].ToString();
        //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //        {
        //            str += "<span class=\"label label-blue-alt\">" +
        //                   ds.Tables[0].Rows[i][0].ToString().Replace("Leave", "")
        //                   + "</span>&nbsp;<span class=\"badge badge-blue-alt\">Total" +
        //                   "</span>&nbsp;<span class=\"badge badge-blue-alt\">" + ds.Tables[0].Rows[i][1].ToString() +
        //                   "</span>&nbsp;&nbsp;<span class=\"badge badge-blue-alt\">Taken</span>&nbsp;<span class=\"badge badge-blue-alt\">" +
        //                   ds.Tables[0].Rows[i][2].ToString() +
        //                   "</span>&nbsp;&nbsp;<span class=\"badge badge-blue-alt\">Remaining</span>&nbsp;<span class=\"badge badge-blue-alt\">" +
        //                   ds.Tables[0].Rows[i][3].ToString() +
        //                   "</span>&nbsp;&nbsp;</br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        //            //str += "<span class=\"label label-blue-alt\">" +
        //            //       ds.Tables[0].Rows[i][0].ToString().Replace("Leave", "") +
        //            //       "</span>&nbsp;<span class=\"badge badge-blue-alt\">" + ds.Tables[0].Rows[i][1].ToString() +
        //            //       "</span>&nbsp;&nbsp;";
        //        }
        //        htm += str;
        //        return htm;
        //    }
        //    return "<span>No Leave assign yet!</span>";
        //}
        
        #region
        public DtPayroll LeaveSlip(int id)
        {
            xOssContext db = new xOssContext();

            DtPayroll ds = new DtPayroll();

            var LeaveData = (from t1 in db.HrmsLeaveApplications
                             join t2 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t2.ID
                             join t3 in db.Employees on t1.EmployeeId equals t3.ID
                             join t4 in db.Designations on t3.Designation_Id equals t4.ID
                             join t5 in db.Sections on t3.Section_Id equals t5.ID
                             where t1.ID == id

                             select new
                             {
                                 BanName = t3.Name_BN,
                                 EmpId = t3.ID,
                                 LeaveType = t1.LeaveTypeId,
                                 IDCard = t3.CardTitle+" "+t3.EmployeeIdentity,
                                 BanDesignation = t4.DesigNameBangla,
                                 BanSection = t5.SectionNameBangla,
                                 AssignLeave = t1.Days,
                                 FromDate = t1.From,
                                 ToDate = t1.To,
                                 JoiningDate = t1.JoiningWorkAfterLeave,

                             }).FirstOrDefault();
            CalculateLeave(LeaveData.EmpId);
            ds.DtLeaveSlip.Rows.Clear();
            int count = 0;
            ds.DtLeaveSlip.Rows.Add(
                   LeaveData.BanName,
                   LeaveData.BanDesignation,
                   LeaveData.IDCard,
                   LeaveData.BanSection,
                   LeaveData.AssignLeave,
                   LeaveData.FromDate,
                   LeaveData.ToDate,
                   LeaveData.JoiningDate,
                   0,
                   0,
                   0,
                   0,
                   0,
                   0
                  );

            foreach (var v in lstLeaveType)
            {
                if (v.LeaveTypeId==192)
                {
                    ds.DtLeaveSlip.Rows[count][8] = v.AssignLeaveQty;
                    ds.DtLeaveSlip.Rows[count][9] = v.AssignLeaveQty-v.TakenLeaveQty;
                 
                }
                else if (v.LeaveTypeId == 193)
                {
                    ds.DtLeaveSlip.Rows[count][10] = v.AssignLeaveQty;
                    ds.DtLeaveSlip.Rows[count][11] = v.AssignLeaveQty- v.TakenLeaveQty;
                }
                else if (v.LeaveTypeId == 1)
                {
                    ds.DtLeaveSlip.Rows[count][12] = v.AssignLeaveQty;
                    ds.DtLeaveSlip.Rows[count][13] = v.AssignLeaveQty- v.TakenLeaveQty;
                }
            }
            
            return ds;

        }

        #endregion
        #region
        public DtPayroll LeaveApplicationFrom(int id)
        {
            xOssContext db = new xOssContext();

            DtPayroll ds = new DtPayroll();

            var LeaveData = (from t1 in db.HrmsLeaveApplications
                             join t2 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t2.ID
                             join t3 in db.Employees on t1.EmployeeId equals t3.ID
                             join t4 in db.Designations on t3.Designation_Id equals t4.ID
                             join t5 in db.Sections on t3.Section_Id equals t5.ID
                             where t1.ID == id

                             select new
                             {
                                 BanName = t3.Name_BN,
                                 EmpId = t3.ID,
                                 LeaveType = t1.LeaveTypeId,
                                 IDCard = t3.CardTitle + " " + t3.EmployeeIdentity,
                                 BanDesignation = t4.DesigNameBangla,
                                 BanSection = t5.SectionNameBangla,
                                 AssignLeave = t1.Days,
                                 FromDate = t1.From,
                                 ToDate = t1.To,
                                 JoiningDateAfterLeave = t1.JoiningWorkAfterLeave,
                                 Purpose=t1.Purpose,
                                 StayDuringLeave=t1.StayDuringLeave,
                                 JoiningDate=t3.Joining_Date

                             }).FirstOrDefault();
            CalculateLeave(LeaveData.EmpId);
            ds.DtLeaveApplication.Rows.Clear();
            int count = 0;
            ds.DtLeaveApplication.Rows.Add(
                   LeaveData.BanName,
                   LeaveData.BanDesignation,
                   LeaveData.IDCard,
                   LeaveData.BanSection,
                   LeaveData.AssignLeave,
                   LeaveData.FromDate,
                   LeaveData.ToDate,
                   LeaveData.JoiningDateAfterLeave,
                   LeaveData.Purpose,
                   LeaveData.StayDuringLeave,
                   LeaveData.JoiningDate,
                   0,
                   0,
                   0,
                   0,
                   0,
                   0
                   );

            foreach (var v in lstLeaveType)
            {
                if (v.LeaveTypeId == 192)
                {
                    ds.DtLeaveApplication.Rows[count][11] = v.AssignLeaveQty;
                    ds.DtLeaveApplication.Rows[count][12] = v.AssignLeaveQty - v.TakenLeaveQty;

                }
                else if (v.LeaveTypeId == 193)
                {
                    ds.DtLeaveApplication.Rows[count][13] = v.AssignLeaveQty;
                    ds.DtLeaveApplication.Rows[count][14] = v.AssignLeaveQty - v.TakenLeaveQty;
                }
                else if (v.LeaveTypeId == 1)
                {
                    ds.DtLeaveApplication.Rows[count][15] = v.AssignLeaveQty;
                    ds.DtLeaveApplication.Rows[count][16] = v.AssignLeaveQty - v.TakenLeaveQty;
                }

              
            }

            return ds;

        }

        #endregion

        #region
        public void CalculateLeave( int EmId)
        {
            var allLeave = db.HrmsLeaveTypes.Where(a => a.Status == true).ToList();
            List<VMLeaveType> lstLeaveType = new List<VMLeaveType>();
            foreach (var v in allLeave)
            {
                var vData =( from t1 in db.HrmsLeaveApplications
                            where t1.EmployeeId == EmId && t1.LeaveTypeId == v.ID
                     
                            select new
                            {
                                days = t1.Days
                            }).ToList();
                VMLeaveType model = new VMLeaveType();
                model.LeaveTypeId = v.ID;
                model.AssignLeaveQty = v.Leave_Amount;
                if (vData.Any())
                {
                    model.TakenLeaveQty = (int)vData.Sum(a=>a.days);
                }
                
                
                lstLeaveType.Add(model);
            }
            
            
            this.lstLeaveType= lstLeaveType;
        }
        #endregion
        
        public void GetEmployeeDetails(int EmployeeId)
        {
            db = new xOssContext();
            var leave = (from t1 in db.Employees
                         join d in db.Designations on t1.Designation_Id equals d.ID
                         join s in db.Sections on t1.Section_Id equals s.ID
                         where t1.ID == EmployeeId
                         select new VM_HRMS_Leave_Application()
                         {
                             Employee = t1,
                             Designation = d,
                             Section = s
                         }).FirstOrDefault();
            this.Employee = leave.Employee;
            this.Designation = leave.Designation;
            this.Section = leave.Section;
        }

        public void CreateLeaveApplication(VM_HRMS_Leave_Application model)
        {
            db = new xOssContext();

            var leave = (from t1 in db.HrmsLeaveAssigns
                         where (t1.LeaveTypeId == model.LeaveTypeId && t1.EmployeeId == model.EmployeeId && t1.Year == model.To.Year.ToString())
                         select new
                         {
                             RemainingLeaveDays = t1.Remaining
                         }).FirstOrDefault();
            if (leave.RemainingLeaveDays >= model.Days1)
            {
                var count = (from t1 in db.HrmsLeaveRepositories
                             where t1.EmployeeId == model.EmployeeId && (t1.Date >= model.From && t1.Date <= model.To)
                             select t1.EmployeeId
                            ).ToList();
                if (!count.Any())
                {
                    model.Entry_Date = DateTime.Today;
                    HRMS_Leave_Application lapp = new HRMS_Leave_Application();
                    lapp.EmployeeId = model.EmployeeId;
                    lapp.LeaveTypeId = model.LeaveTypeId;
                    lapp.From = model.From;
                    lapp.To = model.To;
                    lapp.Days = model.Days;
                    lapp.Purpose = model.Purpose;
                    lapp.StayDuringLeave = model.StayDuringLeave;
                    lapp.Comment = model.Comment;
                    lapp.LeaveStatus = 1;
                    lapp.LeavePaidType = model.LeavePaidType;
                    lapp.HRDepartment = model.HRDepartment;
                    lapp.Recommended = model.Recommended;
                    lapp.SectionIncharge = model.SectionIncharge;
                    lapp.APMorPM = model.APMorPM;
                    lapp.FM = model.FM;
                    lapp.ApprovedBy = model.ApprovedBy;
                    Entry_By = model.Entry_By;
                    db.HrmsLeaveApplications.Add(lapp);
                    db.SaveChanges();
                    //var a = db.Database.ExecuteSqlCommand("EXECUTE[dbo].[sp_Manual_Leave_Application]"
                    //                                  + "@EmployeeId='" + model.LeaveApplication.EmployeeId + "', " +
                    //                                  "@LeaveTypeId ='" + model.LeaveApplication.LeaveTypeId + "', " +
                    //                                  "@From='" + dt1 + "', " +
                    //                                  "@To='" + dt2 + "', " +
                    //                                  "@Days='" + model.LeaveApplication.Days + "', " +
                    //                                  "@Purpose='" + model.LeaveApplication.Purpose + "', " +
                    //                                  "@StayDuringLeave='" + model.LeaveApplication.StayDuringLeave +
                    //                                  "', " +
                    //                                  "@Comment='" + model.LeaveApplication.Comment + "', " +
                    //                                  // "@JoiningWorkAfterLeave='"+ model.LeaveApplication.JoiningWorkAfterLeave+ "', " +
                    //                                  "@LeaveStatus=1, " +
                    //                                  "@LeavePaidType='" + model.LeaveApplication.LeavePaidType + "', " +
                    //                                  "@HRDepartment='" + model.HRDepartment + "', " +
                    //                                  "@Recommended='" + model.LeaveApplication.Recommended + "', " +
                    //                                  "@SectionIncharge='" + model.SectionIncharge + "', " +
                    //                                  "@APMorPM='" + model.APMorPM + "', " +
                    //                                  "@FM='" + model.FM + "', " +
                    //                                  "@ApprovedBy='" + model.ApprovedBy + "', " +
                    //                                  "@EntryBy=1");


                    //if (a > 0)
                    //{
                    //    Session["success_div"] = "true";
                    //    Session["success_msg"] = "Leave Application Submitted Successfully.";
                    //    return RedirectToAction("AddLeaveApplication", "HRMSLeave");
                    //}
                }
                //Session["warning_div"] = "true";
                //Session["warning_msg"] = "Employee Already Applied Leave Between these Date";
                //return RedirectToAction("AddLeaveApplication", "HRMSLeave");

            }


            
            
        }

        public VM_HRMS_Leave_Application GetLeaveApplication(int ApplicationId)
        {
            db = new xOssContext();
            
            var vData = (from t1 in db.HrmsLeaveApplications
                         join t2 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t2.ID
                         join t3 in db.Employees on t1.EmployeeId equals t3.ID
                         join t4 in db.Departments on t3.Department_Id equals t4.ID
                         join t5 in db.Sections on t3.Section_Id equals t5.ID
                         join t6 in db.Designations on t3.Designation_Id equals t6.ID
                         where t1.ID == ApplicationId
                         select new VM_HRMS_Leave_Application
                         {
                             EmployeeId = t1.EmployeeId,
                             Name = t3.Name,
                             DesignationName = t6.Name,
                             SectionName = t5.SectionName,
                             DepartmentName = t4.DeptName,
                             From=t1.From,
                             To=t1.To,
                             LeaveTypeId=t1.LeaveTypeId,
                             Comment=t1.Comment,
                             LeaveApplyDate=t1.Entry_Date,
                             Days=t1.Days,
                             Purpose=t1.Purpose,
                             StayDuringLeave=t1.StayDuringLeave
                             //LeaveApplication = t1,
                         }).FirstOrDefault();
            return vData;
        }
        
        public void GetLeaveArchieve(int EmployeeId)
        {
            db = new xOssContext();
            var vData = (from t1 in db.HrmsLeaveApplications
                         join t2 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t2.ID
                         where t1.EmployeeId==EmployeeId
                         select new VM_HRMS_Leave_Application
                         {
                             From=t1.From,
                             To=t1.To,
                             Leave_Name=t2.Leave_Name,
                             Days=t1.Days,
                             LeaveStatus=t1.LeaveStatus,
                             LeaveApplyDate=t1.Entry_Date,
                             ApplicationId=t1.ID
                         }).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    var checkArchieve = db.HrmsLeaveArchieve.Where(a => a.LeaveApplicationFk == v.ApplicationId);
                    if (checkArchieve.Any())
                    {
                        var takeOne = checkArchieve.FirstOrDefault();
                        v.From = takeOne.FromDate;
                        v.To = takeOne.ToDate;
                        v.Days = takeOne.AcceptDays;
                    }
                    
                    if (v.LeaveStatus.Equals(1))
                    {
                        v.LeaveStatusType = "Request";
                    }
                    else if (v.LeaveStatus.Equals(2))
                    {
                        v.LeaveStatusType = "Approved";
                    }
                    
                }
            }
            this.DataList = vData;
        }

        public void GetLeaveApproval(int EmployeeId)
        {
            db = new xOssContext();

            var vData1 = (from t1 in db.HrmsLinkSuperior
                          join t2 in db.HrmsLeaveApplications on t1.EmployeeId equals t2.EmployeeId
                          join t3 in db.HrmsLeaveTypes on t2.LeaveTypeId equals t3.ID
                          where t1.SuperiorId == EmployeeId && t1.Type == 2 && t2.LeaveStatus == 1
                          select new VM_HRMS_Leave_Application
                          {
                              ApplicationId=t2.ID,
                              EmployeeId=t2.EmployeeId,
                              From = t2.From,
                              To = t2.To,
                              Leave_Name = t3.Leave_Name,
                              Days = t2.Days,
                              LeaveStatus = t2.LeaveStatus,
                              LeaveApplyDate = t2.Entry_Date,
                          }).ToList();

            //var vData = (from t1 in db.HrmsLeaveApplications
            //             join t2 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t2.ID
            //             where t1.LeaveStatus==1
            //             select new VM_HRMS_Leave_Application
            //             {
            //                 From = t1.From,
            //                 To = t1.To,
            //                 Leave_Name = t2.Leave_Name,
            //                 Days = t1.Days,
            //                 LeaveStatus = t1.LeaveStatus,
            //                 LeaveApplyDate = t1.Entry_Date,

            //             }).ToList();

            if (vData1.Any())
            {
                foreach (var v in vData1)
                {
                    if (v.LeaveStatus.Equals(1))
                    {
                        v.LeaveStatusType = "Request";
                    }
                    else if (v.LeaveStatus.Equals(2))
                    {
                        v.LeaveStatusType = "Approved";
                    }

                }
            }
            this.DataList = vData1;
        }

        public void GetLeaveApprove(VM_HRMS_Leave_Application model)
        {
            db = new xOssContext();
            HRMS_Leave_Archieve LeaveArchieve = new HRMS_Leave_Archieve();
            LeaveArchieve.LeaveApplicationFk = model.ApplicationId;
            LeaveArchieve.LeaveTypeId = model.LeaveTypeId;
            LeaveArchieve.AcceptDays = (int)model.Days;
            LeaveArchieve.FromDate = model.From;
            LeaveArchieve.ToDate = model.To;
            LeaveArchieve.ApprovedBy = model.LeaveApprovedBy;

            db.HrmsLeaveArchieve.Add(LeaveArchieve);
            db.SaveChanges();
        }


    }
}

