﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_Category:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [Required(ErrorMessage ="Name is Required")]
        [StringLength(100,ErrorMessage = "Upto 100 Chracter")]
        [DisplayName("Category")]
        public string Name { get; set; }

        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        //...........................New Added Code...................................//

        //public List<Mkt_Category> Mkt_Categorys { get; set; }

    }
}