﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_OrderDeliverySchedule : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Destination")]
        [Required(ErrorMessage = "Destination is Required")]
        public int Destination { get; set; }

        [DisplayName("Order")]       
        public int? Common_TheOrderFk { get; set; }

        [DisplayName("Order")]
        public int? Mkt_BOMFk { get; set; }

        [DisplayName("Ratio")]
        public int? Ratio { get; set; }

        [DisplayName("Port No")]
        [Required(ErrorMessage = "Port is Required")]
        public string PortNo { get; set; }

        [DisplayName("Color With Size")]
        [DataType(DataType.MultilineText), Required(ErrorMessage = "Color With Size is Required")]
        public string ColorSize { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Delivery Date")]
        public DateTime Date { get; set; }

        [DisplayName("Quantity")]
        [Required(ErrorMessage = "Quantity is Required")]
        public decimal Quantity { get; set; }
    }
}