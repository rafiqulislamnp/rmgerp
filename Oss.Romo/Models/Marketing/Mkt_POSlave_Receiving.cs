﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    //Purchase Order Sleve Receiving 
    public class Mkt_POSlave_Receiving:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Challan Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DefaultValue(0)]
        [DisplayName("Total Quantity")]
        public decimal? Quantity { get; set; }

        [StringLength(150, ErrorMessage = "Upto 150 Chracter")]
        [Required(ErrorMessage = "Challan Number is Required")]
        public string Challan { get; set; }

        public bool? IsReturn { get; set; }

        public int? Mkt_POSlaveFK { get; set; }

        public int? Mkt_POExtTransferFK { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public int? User_DepartmentFk { get; set; }

        [DefaultValue(0)]
        [DisplayName("Stock Lose")]
        public decimal? StockLoss { get; set; }

        public bool IsFinishFabric { get; set; }

        public string FabricColor { get; set; }
    }
}