﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_POExtTransfer : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Raw Item")]
        public int? Raw_ItemFK { get; set; }
        
        [DisplayName("Required Quantity")]
        public decimal? TotalRequired { get; set; }

        [DisplayName("Unit")]
        public int? Common_UnitFK { get; set; }

        [DisplayName("Machine DIA")]       
        public int? Prod_MachineDIAFk { get; set; }

        [DisplayName("PO ID")]
        public int? Mkt_POFK { get; set; }

        [DisplayName("Item In Process")]
        public int?  Prod_TransitionItemInventoryFK { get; set; }
        
        [DisplayName("Item")]
        [DefaultValue(0)]
        public int? Mkt_POSlaveFK { get; set; }

    }
}