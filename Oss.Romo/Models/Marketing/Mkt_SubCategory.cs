﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_SubCategory : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Category")]
        public int Mkt_CategoryFK { get; set; }

        [Required(ErrorMessage ="Required")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DisplayName("Sub Category")]
        public string Name { get; set; }

        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        //...........................New Added Code...................................//

        //public Mkt_Category Mkt_Category { get; set; }

        //public List<Mkt_Item> Mkt_Items { get; set; }
    }
}