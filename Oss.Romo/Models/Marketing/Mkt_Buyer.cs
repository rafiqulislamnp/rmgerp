﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_Buyer : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        [DisplayName("Buyer ID")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string BuyerID { get; set; }

        public int LastEditeddBy { get; set; }

        [DisplayName("Buyer")]
        [Required(ErrorMessage = "Buyer Name Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Name { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [MaxLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string Phone { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DisplayName("Contact Person")]
        public string ContactPerson { get; set; }

        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        //public string City { get; set; }
        [DisplayName("Country")]
        public int Common_CountryFk { get; set; }

        [DisplayName("Account Head Category")]
        public int Acc_AcNameFk { get; set; }
      
        public Common_Country Common_Country { get; set; }
        [DisplayName("First Notyfy Party")]
        public string FirstNotyfyParty { get; set; }
        [DisplayName("Second Notyfy Party")]
        public string SecondNotyfyParty { get; set; }
        [DisplayName("Third Notyfy Party")]
        public string ThirdNotyfyParty { get; set; }

        public string BillTo { get; set; }

        //public List<Commercial_MasterLC> Commercial_MasterLCs { get; set; }

        //public List<Common_TheOrder> Common_TheOrders { get; set; }


    }
}