﻿using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_PO : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [MaxLength(30)]

        [DisplayName("IPO NO")]
        public string CID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime? Date { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Authorize Date")]
        public DateTime? AuthorizeDate { get; set; }

        [DisplayName(" Terms & Condition")]
        //[StringLength(1000, ErrorMessage = "Upto 1000 Chracter")]
        [DataType(DataType.MultilineText)]       
        public string Description { get; set; }

        //PurchaseOrders     = 0
        //IsStoreSettled     = 1
        //IsPaymentSettled   = 2
        [DefaultValue(0)]
        public int? Status { get; set; }
      
        [DisplayName("Supplier")]
        public int Common_SupplierFK { get; set; }

        [DisplayName("BOM")]
        [StringLength(17)]
        public string Mkt_BOMCID { get; set; }

        [DisplayName("Requisition No")]
        [DefaultValue(0)]
        public int? RequisitionFK { get; set; }

        [DisplayName("Style No")]
        public int? Mkt_BOMFK { get; set; }

        [DisplayName("Currency")]
        public int Common_CurrencyFK { get; set; }
        [DisplayName("Currency Rate")]
        public decimal CurrencyRate { get; set; }

        [DisplayName("Payment Cleared ")]
        public bool PaymentCleared { get; set; }

        [DisplayName("Approved")]
        [DefaultValue(false)]
        public bool IsAuthorize { get; set; }

        [DisplayName("Also Transfer")]
        [DefaultValue(false)]
        public bool AlsoATransfer { get; set; }

        [DefaultValue(false)]
        public bool IsComplete { get; set; }

        public string PaymentType { get; set; }
        //...........................New Added Code...................................//

        //public Common_Supplier Common_Supplier { get; set; }

        //public Mkt_BOM Mkt_BOM { get; set; }

        //public Common_Currency Common_Currency { get; set; }


        //public List<Acc_LCAlocation> Acc_LCAlocations { get; set; }
        //public List<Acc_POPH> Acc_POPHs { get; set; }

        //public List<Commercial_B2bLcSupplierPO> Commercial_B2bLcSupplierPOs { get; set; }

        //public List<Commercial_MLcSupplierPO> Commercial_MLcSupplierPOs { get; set; }

        //public List<Commercial_PIPOSupplier> Commercial_PIPOSuppliers { get; set; }

        //public List<Commercial_PISupplierPO> Commercial_PISupplierPOs { get; set; }
        //public List<Mkt_POSlave> Mkt_POSlaves { get; set; }

        //public List<Prod_PoHistory> Prod_PoHistorys { get; set; }

        //public List<Prod_PoHistoryType> Prod_PoHistoryTypes { get; set; }



        //Forget--------------------------------------------------------

        //[DefaultValue(0)]
        //[DisplayName("Paid Amount")]
        //public decimal? PaidAmount { get; set; }

        //[DisplayName("Item")]
        //public int? Raw_ItemFK { get; set; }
        //public decimal? Consumption { get; set; }

        //[DisplayName("Required Quentity")]
        //public decimal? TotalRequired { get; set; }

        //[DisplayName("Unit")]
        //public int? Common_UnitFK { get; set; }

        //[DisplayName("Item Price")]
        //[Required(ErrorMessage = "Required")]
        //public decimal Price { get; set; }

        //[DisplayName("BOM")]
        //public int? Mkt_BOMFK { get; set; }

        ////No
        //[DisplayName("Received Quentity")]
        //[DefaultValue(0)]
        //public decimal? Received { get; set; }
        ////No
        //[DisplayName("Send Quentity")]
        //[DefaultValue(0)]
        //public decimal? Delivered { get; set; }
        //No

        //No

        //[Required(ErrorMessage = "Required")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //public DateTime Date { get; set; }

    }
}