﻿using Oss.Romo.Models.Common;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_BOMStepJobs : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Item")]

        public int Raw_ItemFK { get; set; }
        public int Mkt_BOMSlaveFK { get; set; }

        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [Required(ErrorMessage = "Consumption is Required")]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal Consumption { get; set; }

       
        [Range(0.0, Double.MaxValue)]
        [DisplayName("Req. Quantity")]
        public decimal? RequiredQuantity { get; set; }


        [DisplayName("Unit")]
        [Required(ErrorMessage = "Unit is Required")]
        public int Common_UnitFK { get; set; }

        [DisplayName("Slave Type")]
        [Required(ErrorMessage = "Unit is Required")]
        public int Mkt_BOMSlaveTypeFK { get; set; }

        [DisplayName("Item Price")]
        [Required(ErrorMessage = "Item Price is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        [DisplayName("Currency")]
        [Required(ErrorMessage = "Currency is Required")]
        public int Common_CurrencyFK { get; set; }

        [DisplayName("Supplier")]
        [Required(ErrorMessage = "Supplier is Required")]
        public int Common_SupplierFK { get; set; }

        [DisplayName("BOM")]
        [Required(ErrorMessage = "BOM is Required")]
        public int? Mkt_BOMFK { get; set; }

        [DisplayName("Is This Fabrics Consumption Summary?")]
        [DefaultValue(false)]
        public bool IsFabric { get; set; }


        [DisplayName("Locked")]
        [DefaultValue(false)]
        public bool IsLocked { get; set; }


        //...........................New Added Code...................................//


        //public Raw_Item Raw_Item { get; set; }
        //public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        //public Common_Unit Common_Unit { get; set; }
        //public Common_Currency Common_Currency { get; set; }
        //public Common_Supplier Common_Supplier { get; set; }
        //public Mkt_BOM Mkt_BOM { get; set; }



    }
}