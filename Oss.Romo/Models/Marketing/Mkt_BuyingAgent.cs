﻿using System;
using System.Collections.Generic;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_BuyingAgent : BaseModel
    {

        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}