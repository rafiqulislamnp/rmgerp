﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_OrderColorAndSizeRatio: BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Color")]
        [Required(ErrorMessage = "Color is Required")]
        public string Color { get; set; }

        [DisplayName("DeliverySchedule")]
        public int? OrderDeliveryScheduleFk { get; set; }

        [DisplayName("Order")]
        public int? Common_TheOrderFk { get; set; }

        [DisplayName("Order")]
        public int? Mkt_BOMFk { get; set; }


        [DisplayName("Ratio")]
        public int? Ratio { get; set; }
        [DisplayName("Size")]
        [Required(ErrorMessage = "Size is Required")]
        public string Size { get; set; }

        [DisplayName("Quantity")]
        [Required(ErrorMessage = "Quantity is Required")]
        public decimal Quantity { get; set; }
    }
}