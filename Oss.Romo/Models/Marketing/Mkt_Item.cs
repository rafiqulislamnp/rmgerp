﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_Item:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Sub Category")]
        public int Mkt_SubCategoryFK { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [Required(ErrorMessage = "Name is Required")]
        [DisplayName("Item")]
        public string Name { get; set; }

        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        //...........................New Added Code...................................//

        //public Mkt_SubCategory Mkt_SubCategory { get; set; }

        //public List<Mkt_BOM> Mkt_BOMs { get; set; }
    }
}