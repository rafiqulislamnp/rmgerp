﻿ using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_CBSMaster : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [MaxLength(50)]
        [DisplayName("Style No")]
        public string CID { get; set; }

        [DisplayName("First Move")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FirstMove { get; set; }

        [DisplayName("Updated")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime UpdateDate { get; set; }

        [DisplayName("Reference")]
        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        public string Reference { get; set; }

        //[DisplayName("Order No")]
        //[Required(ErrorMessage = "Order Name is Required")]
        //public int? Common_TheOrderFk { get; set; }

        //[DisplayName("Buyer")]
        //public int? Mkt_BuyerFK { get; set; }

        [DisplayName("Item")]
        public int Mkt_ItemFK { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Class { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Fabrication { get; set; }

        [DisplayName("Size Range")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string SizeRange { get; set; }

        [Required(ErrorMessage = "Style is Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Style { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [DisplayName("Total Pack")]
        public int QPack { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [DisplayName("QUP")]
        [Required(ErrorMessage = "Quantity is Required")]
        public int Quantity { get; set; }
        [DefaultValue(0)]
        public decimal? Commission { get; set; }
        //[DisplayName("Unit")]
        //public int Common_UnitFK { get; set; }
        //[RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [DisplayName("Unit Price")]
        [Required(ErrorMessage = "Unit Price is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal UnitPrice { get; set; }

        //[DisplayName("Currency")]
        //public int Common_CurrencyFK { get; set; }

        [DefaultValue(false)]
        public bool IsComplete { get; set; }


        [DisplayName("Authorization")]
        [DefaultValue(false)]
        public bool IsAuthorize { get; set; }
        [DefaultValue(false)]
        public bool IsCount { get; set; }

        

        //...........................New Added Code...................................//

        //public Mkt_Item Mkt_Item { get; set; }

        //public Common_TheOrder Common_TheOrder { get; set; }

        //public List<Commercial_InvoiceSlave> Commercial_InvoiceSlaves { get; set; }

        //public List<Commercial_MasterLCBuyerPO> Commercial_MasterLCBuyerPOs { get; set; }

        //public List<Mkt_BOMSlave> Mkt_BOMSlaves { get; set; }

        //public List<Mkt_BOMStepJobs> Mkt_BOMStepJobss { get; set; }
        //public List<Mkt_PO> Mkt_POs { get; set; }
        //public List<Mkt_YarnCalculation> Mkt_YarnCalculations { get; set; }
        //public List<Mkt_YarnCotton> Mkt_YarnCottons { get; set; }
        //public List<Mkt_YarnOther> Mkt_YarnOthers { get; set; }

        //public List<Plan_OrderLine> Plan_OrderLines { get; set; }

        //public List<Prod_DailyTarget> Prod_DailyTargets { get; set; }

        //public List<Prod_DailyTargeSlave> Prod_DailyTargeSlaves { get; set; }

        //public List<Prod_MasterPlan> Prod_MasterPlans { get; set; }

        //public List<Prod_ProductionLinePlan> Prod_ProductionLinePlans { get; set; }

        //public List<Prod_Requisition> Prod_Requisitions { get; set; }        

    }
}