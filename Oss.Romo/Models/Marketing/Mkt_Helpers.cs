﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public static class Mkt_Helpers
    {
        public static string ShortDateString(DateTime Date)
        {
            string datestring = string.Empty;

            string day = Date.Day < 10 ? "0" + Date.Day : Date.Day.ToString();
            string month = Date.Month < 10 ? "0" + Date.Month : Date.Month.ToString();
            int year = Date.Year;

            datestring = day + "-" + month + "-" + year;

            return datestring;
        }

        public static string ShortDateStringSlash(DateTime Date)
        {
            string datestring = string.Empty;

            string day = Date.Day < 10 ? "0" + Date.Day : Date.Day.ToString();
            string month = Date.Month < 10 ? "0" + Date.Month : Date.Month.ToString();
            int year = Date.Year;

            datestring = day + "/" + month + "/" + year;

            return datestring;
        }
    }
}