﻿using Oss.Romo.Models.Common;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_POSlave:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Item")]
        public int? Raw_ItemFK { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal? Consumption { get; set; }
        
        [DisplayName("Required Quantity")]
        public decimal? TotalRequired { get; set; }
        
        [DisplayName("Unit")]
        public int? Common_UnitFK { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        //[Range(0.0, 8)]
        [DataType("decimal(18 ,5")]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        [DisplayName("Item Price")]
        [Required(ErrorMessage = "Required")]
        public decimal? Price { get; set; }

        [DisplayName("PO ID")]
        public int? Mkt_POFK { get; set; }

       
        public int? Mkt_BOMSlaveFK { get; set; }

        public int? RequisitionSlaveFK { get; set; }

        //[StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        //...........................New Added Code...................................//
        //public Raw_Item Raw_Item { get; set; }

        //public Common_Unit Common_Unit { get; set; }

        //public Mkt_PO Mkt_PO { get; set; }

        //public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        //public List<Mkt_POSlave_Consumption> Mkt_POSlave_Consumptions { get; set; }
        //public List<Mkt_POSlave_Receiving> Mkt_POSlave_Receivings { get; set; }


    }
}