﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_YarnType : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        
        [MaxLength(50, ErrorMessage = "Upto 50 Chracter")]
        [DisplayName("Type")]
        public string Name { get; set; }

        [MaxLength(200, ErrorMessage = "Upto 200 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}