﻿using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    // Purchase Order Slave Sending Discription
    public class Mkt_POSlave_Consumption:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [DisplayName("Requisition No")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [Required(ErrorMessage = "Requisition number is required")]
        public string Requisition { get; set; }
        [DisplayName("Total Quantity")]
        [DefaultValue(0)]
        public decimal? Quantity { get; set; }
        [DisplayName("Supervisor Name")]
        public string Supervisor { get; set; }
        public bool? IsReturn { get; set; }
        public int? Mkt_POSlaveFK { get; set; }

        public int? Prod_TransitionItemInventoryFK { get; set; }

        public int? Mkt_POExtTransferFK { get; set; }

        public int Raw_InternaleTransferSlaveFk { get; set; }

        public int? Prod_Requisition_SlaveFK { get; set; }

        public int? User_DepartmentFk { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

       // public int? Prod_TransitionItemMadeFromFK { get; set; }

        //...........................New Added Code...................................//
        //public Mkt_POSlave Mkt_POSlave { get; set; }
        //public Raw_Store Raw_Store { get; set; }
        //public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
    }
}