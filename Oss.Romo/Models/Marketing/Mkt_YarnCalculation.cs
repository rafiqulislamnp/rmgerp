﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Mkt_YarnCalculation: BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Reference No")]
        public string ReferenceNo { get; set; }

        [DisplayName("BOM")]
        [Required(ErrorMessage = "BOM is Required")]
        public int? Mkt_BOMFK { get; set; }

        [DisplayName("Yarn Type")]
        [Required(ErrorMessage = "BOM is Required")]
        public int? Mkt_YarnTypeFk { get; set; }

        [DisplayName("COMBO")]
        [MaxLength(40, ErrorMessage = "Upto 40 Chracter")]
        public string Combo { get; set; }

        [DisplayName("COLOR")]
        [MaxLength(40, ErrorMessage = "Upto 40 Chracter")]
        public string Color { get; set; }

        [DisplayName("FABRICATION/YARN")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Fabrication { get; set; }

        public decimal GSM { get; set; }
        [Required(ErrorMessage = "Item is Required")]
        [DisplayName("YARN COUNT")]
        public int? Raw_ItemFK { get; set; }        

        [DisplayName("FINISH DIA")]
        public string FinishDIA { get; set; }

        [DisplayName("QTY PCS")]
        public decimal QntyPcs { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [Required(ErrorMessage = "Consumption is Required")]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal Consumption { get; set; }

        [DisplayName("Actual Yarn Percent")]
        [Required(ErrorMessage = "Actual Yarn Percent is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal ProcessLoss { get; set; }

        [DisplayName("LYCRA")]
        public decimal Lycra { get; set; }

        [DisplayName("Supplier")]
        public int? Common_SupplierFK { get; set; }

        [DisplayName("Currency")]
        [Required(ErrorMessage = "Currency is Required")]
        public int? Common_CurrencyFK { get; set; }

        [DisplayName("Unit")]
        [Required(ErrorMessage = "Unit is Required")]
        public int? Common_UnitFK { get; set; }

        [DisplayName("Item Price")]
        [Required(ErrorMessage = "Item Price is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal Price  { get; set; }

            //...........................New Added Code...................................//
            //public Mkt_BOM Mkt_BOM { get; set; }



        }
}