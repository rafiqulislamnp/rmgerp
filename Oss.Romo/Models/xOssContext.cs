﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Shipment;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.User;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.AssetManagement;
using Oss.Romo.Models.Mis;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Shipment;

using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;
using Oss.Romo.Models.Entity.User;
using Oss.Romo.Models.ViewModels.HRMS;
using Oss.Romo.Models.Entity.Compliance;

namespace Oss.Romo.Models
{
    public class xOssContext : DbContext
    {
        //-----------------------------------------Common

        public DbSet<Common_Unit> Common_Unit { get; set; }
        public DbSet<Common_Currency> Common_Currency { get; set; }
        public DbSet<Common_Supplier> Common_Supplier { get; set; }
        public DbSet<Common_User> Common_User { get; set; }
        public DbSet<Common_Permission> Common_Permission { get; set; }
        public DbSet<Common_Method> Common_Method { get; set; }
        public DbSet<Common_Controller> Common_Controller { get; set; }
        public DbSet<Common_Roles> Common_Roles { get; set; }
        public DbSet<Common_RolesPermission> Common_RolesPermission { get; set; }
        public DbSet<Common_UserRoles> Common_UserRoles { get; set; }
        public DbSet<Common_TheOrder> Common_TheOrder { get; set; }
        public DbSet<Common_Company> Common_Company { get; set; }
        public DbSet<Prod_MachineDIA> Prod_MachineDIA { get; set; }
        
        public DbSet<Common_Country> Common_Country { get; set; }
        public DbSet<Common_CompanyBrunch> Common_CompanyBrunch { get; set; }
        public DbSet<Common_SupplierType> Common_SupplierType { get; set; }
        public DbSet<Common_BillType> Common_BillType { get; set; }
        public DbSet<Mkt_YarnType> Mkt_YarnType { get; set; }

        public DbSet<Common_Status> Common_Status { get; set; }
        //-----------------------------------------Marketing
        public DbSet<Mkt_CBS> Mkt_CBS { get; set; }
        public DbSet<Mkt_BOM> Mkt_BOM { get; set; }
        public DbSet<Mkt_BOMSlave> Mkt_BOMSlave { get; set; }
        public DbSet<Mkt_BOMSlaveFabric> Mkt_BOMSlaveFabric { get; set; }
        public DbSet<Mkt_Buyer> Mkt_Buyer { get; set; }
        public DbSet<Mkt_PO> Mkt_PO { get; set; }
        public DbSet<Mkt_Category> Mkt_Category { get; set; }
        public DbSet<Mkt_SubCategory> Mkt_SubCategory { get; set; }
        public DbSet<Mkt_Item> Mkt_Item { get; set; }
        public DbSet<Mkt_POSlave> Mkt_POSlave { get; set; }
        public DbSet<Mkt_POExtTransfer> Mkt_POExtTransfer { get; set; }
        public DbSet<Mkt_POSlave_Consumption> Mkt_POSlave_Consumption { get; set; }
        public DbSet<Mkt_POSlave_Receiving> Mkt_POSlave_Receiving { get; set; }

        public DbSet<Prod_LineChief> Prod_LineChief { get; set; }
        public DbSet<Prod_SVName> Prod_SVName { get; set; }
        public DbSet<Mkt_CBSMaster> Mkt_CBSMaster { get; set; }

        public DbSet<Mkt_BOMStepJobs> Mkt_BOMStepJobs { get; set; }
        public DbSet<Mkt_YarnCalculation> Mkt_YarnCalculation { get; set; }

        public DbSet<Mkt_OrderDeliverySchedule> Mkt_OrderDeliverySchedule { get; set; }
        public DbSet<Mkt_OrderColorAndSizeRatio> Mkt_OrderColorAndSizeRatio { get; set; }

        //-----------------------------------------Store
        public DbSet<Raw_Category> Raw_Category { get; set; }
        public DbSet<Raw_SubCategory> Raw_SubCategory { get; set; }
        public DbSet<Raw_Item> Raw_Item { get; set; }
        //public DbSet<Raw_Store> Raw_Store { get; set; }
        public DbSet<Raw_InternaleTransfer> Raw_InternaleTransfer { get; set; }
        public DbSet<Raw_InternaleTransferSlave> Raw_InternaleTransferSlave { get; set; }

        

        //-----------------------------------------Accounts
        public DbSet<Acc_SupplierPH> Acc_SupplierPH { get; set; }
        public DbSet<Acc_SupplierReceiveH> Acc_SupplierReceiveH { get; set; }
        public DbSet<Acc_POPH> Acc_POPH { get; set; }
        public DbSet<Acc_SupplierAdjustment> Acc_SupplierAdjustment { get; set; }
        public DbSet<Acc_Chart1> Acc_Chart1 { get; set; }
        public DbSet<Acc_Chart2> Acc_Chart2 { get; set; }
        public DbSet<Acc_AcName> Acc_AcName { get; set; }
        public DbSet<Acc_Journal> Acc_Journal { get; set; }
        public DbSet<Acc_JournalSlave> Acc_JournalSlave { get; set; }
        public DbSet<Acc_Ledger> Acc_Ledger { get; set; }
        public DbSet<Acc_Expenses_History> Acc_Expenses_History { get; set; }
        public DbSet<Acc_LCAlocation> Acc_LCAlocation { get; set; }
        public DbSet<Acc_SupplierPaymentType> Acc_SupplierPaymentType { get; set; }
        public DbSet<Acc_Transaction> Acc_Transaction { get; set; }
        public DbSet<Acc_Database_Table> Acc_Database_Table { get; set; }
        public DbSet<Acc_ExportRealisation> Acc_ExportRealisation { get; set; }
        public DbSet<Acc_ExportRealisationExpense> Acc_ExportRealisationExpense { get; set; }
        public DbSet<Acc_CostPool> Acc_CostPool { get; set; }
        public DbSet<Acc_Notifier> Acc_Notifier { get; set; }
        public DbSet<Acc_SupplierBill> Acc_SupplierBill { get; set; }
        public DbSet<Acc_SupplierBillSlave> Acc_SupplierBillSlave { get; set; }
        public DbSet<Acc_DollarRate> Acc_DollarRate { get; set; }


        //........................................Planning
        public DbSet<Plan_OrderLine> Plan_OrderLine { get; set; }
        public DbSet<Plan_OrderStep> Plan_OrderStep { get; set; }
        public DbSet<Plan_OrderProcessCategory> Plan_OrderProcessCategory { get; set; }
        public DbSet<Plan_OrderUpdate> Plan_OrderUpdate { get; set; }
        public DbSet<Plan_ProductionLine> Plan_ProductionLine { get; set; }
        public DbSet<Plan_ProductionLineRunning> Plan_ProductionLineRunning { get; set; }
        public DbSet<Plan_OrderProcess> Plan_OrderProcess { get; set; }
        public DbSet<Prod_DailyProductionStepSlave> Prod_DailyProductionStepSlave { get; set; }

        //.......................................Production
        public DbSet<Prod_ProductionSection> Prod_ProductionSection { get; set; }
        public DbSet<Prod_PlanReferenceProductionSectionFollowup> Prod_PlanReferenceProductionSectionFollowup { get; set; }
        public DbSet<Prod_PlanReferenceProductionSection> Prod_PlanReferenceProductionSection { get; set; }
        public DbSet<Prod_PlanReferenceProduction> Prod_PlanReferenceProduction { get; set; }
        public DbSet<Prod_PlanReference> Prod_PlanReference { get; set; }
        public DbSet<Prod_Planning> Prod_Planning { get; set; }
        public DbSet<Prod_OrderPlanning> Prod_OrderPlanning { get; set; }
        public DbSet<Prod_PlanAchievment> Prod_PlanAchievment { get; set; }
        public DbSet<Prod_Requisition> Prod_Requisition { get; set; }
        public DbSet<Prod_Requisition_Slave> Prod_Requisition_Slave { get; set; }
        public DbSet<Raw_ItemBulk> Raw_ItemBulk { get; set; }
        public DbSet<Prod_Consumption> Prod_Consumption { get; set; }
        public DbSet<Prod_TransitionItemInventory> Prod_TransitionItemInventory { get; set; }
        public DbSet<Prod_TransitionItemMadeFrom> Prod_TransitionItemMadeFrom { get; set; }
        public DbSet<Prod_Machine> Prod_Machine { get; set; }
        public DbSet<Prod_MachineType> Prod_MachineType { get; set; }
        public DbSet<Prod_MachineLine> Prod_MachineLine { get; set; }

        public DbSet<Prod_Capacity> Prod_Capacity { get; set; }
        public DbSet<Plan_OrderLayout> Plan_OrderLayout { get; set; }

        public DbSet<Prod_InputRequisition> Prod_InputRequisition { get; set; }
        public DbSet<Prod_InputRequisitionSlave> Prod_InputRequisitionSlave { get; set; }

        public DbSet<Prod_FinishItemTransfer> Prod_FinishItemTransfer { get; set; }
        public DbSet<Prod_FinishItemTransferSlave> Prod_FinishItemTransferSlave { get; set; }

        //public DbSet<Prod_OrderStatus> Prod_OrderStatus { get; set; }
        //public DbSet<Prod_PlanReference> Prod_MasterPlan { get; set; }
        //public DbSet<Prod_PlanReferenceProduction> Prod_MasterPlanSlave { get; set; }
        //public DbSet<Prod_DailyTarget> Prod_DailyTarget { get; set; }
        //public DbSet<Oss.Romo.Models.Production.Prod_DailyTargetSlave> Prod_DailyTargetSlave { get; set; }
        public DbSet<Prod_LineOfficer> Prod_LineOfficer { get; set; }
        public DbSet<Prod_SMVLayout> Prod_SMVLayout { get; set; }
        public DbSet<Prod_Reference> Prod_Reference { get; set; }
        public DbSet<Prod_PlanReferenceOrder> Prod_PlanReferenceOrder { get; set; }
        public DbSet<Prod_PlanReferenceOrderSection> Prod_PlanReferenceOrderSection { get; set; }
        public DbSet<Prod_PlanOrderSectionSlave> Prod_PlanOrderSectionSlave { get; set; }
        public DbSet<Prod_PlanReferenceRemarks> Prod_PlanReferenceRemarks { get; set; }
        //........................................User
        public DbSet<User_Menu> User_Menu { get; set; }
        public DbSet<User_Role> User_Role { get; set; }

        public DbSet<User_SubMenu> User_SubMenu { get; set; }
        public DbSet<User_MenuItem> User_MenuItem { get; set; }

        public DbSet<User_User> User_User { get; set; }
        public DbSet<User_Team> User_Team { get; set; }

     
        public DbSet<User_RoleMenuItem> User_RoleMenuItem { get; set; }
        public DbSet<User_AccessLevel> User_AccessLevel { get; set; }
        public DbSet<User_Department> User_Department { get; set; }
        public DbSet<UserAssignMenu> UserAssignMenu { get; set; }


        //.......................................Commercial
        public DbSet<Commercial_B2BPaymentInformation> Commercial_B2BPaymentInformation { get; set; }
        public DbSet<Commercial_MasterLC> Commercial_MasterLC { get; set; }
        public DbSet<Commercial_B2bLC> Commercial_B2bLC { get; set; }
        public DbSet<Commercial_PI> Commercial_PI { get; set; }
        public DbSet<Commercial__MasterLCDocs> Commercial__MasterLCDocs { get; set; }
        public DbSet<Commercial_MLcBuyerDocs> Commercial_MLcBuyerDocs { get; set; }
        public DbSet<Commercial_MLcSupplierPO> Commercial_MLcSupplierPO { get; set; }
        public DbSet<Commercial_MLcSupplierPI> Commercial_MLcSupplierPI { get; set; }
        public DbSet<Commercial_MLcSupplierB2BLc> Commercial_MLcSupplierB2BLc { get; set; }
        public DbSet<Commercial_PIPOSupplier> Commercial_PIPOSupplier { get; set; }
        public DbSet<Commercial_B2BLcSupplierPI> Commercial_B2BLcSupplierPI { get; set; }
        public DbSet<Commercial_PISupplierPO> Commercial_PISupplierPO { get; set; }
        public DbSet<Commercial_B2bLcSupplierPO> Commercial_B2bLcSupplierPO { get; set; }
        public DbSet<Commercial_MasterLCBuyerPI> Commercial_MasterLCBuyerPI { get; set; }
        public DbSet<Commercial_MasterLCBuyerPO> Commercial_MasterLCBuyerPO { get; set; }
        public DbSet<Commercial_Bank> Commercial_Bank { get; set; }
        public DbSet<Commercial_MLCRealization> Commercial_MLCRealization { get; set; }
        public DbSet<Commercial_Import> Commercial_Import { get; set; }
        public DbSet<Commercial_InvoiceSlave> Commercial_InvoiceSlave { get; set; }

        public DbSet<Commercial_BillOfExchange> Commercial_BillOfExchange { get; set; }
        public DbSet<Commercial_BillOfExchangeSlave> Commercial_BillOfExchangeSlave { get; set; }
        public DbSet<Commercial_Invoice> Commercial_Invoice { get; set; }

        public DbSet<Commercial_UD> Commercial_UD { get; set; }
        public DbSet<Commercial_LCOregin> Commercial_LCOregin { get; set; }

        public DbSet<Commercial_UDSlave> Commercial_UDSlave { get; set; }
        public DbSet<Commercial_BtBLCType> Commercial_BtBLCType { get; set; }
        public DbSet<Commercial_BTBItem> Commercial_BTBItem { get; set; }


        public DbSet<Commercial_LCType> Commercial_LCType { get; set; }
        public DbSet<Commercial_B2bLcSupplierPOSlave> Commercial_B2bLcSupplierPOSlave { get; set; }


        //...........................................Shipment

        public DbSet<Shipment_Details> Shipment_Details { get; set; }
        public DbSet<Shipment_History> Shipment_History { get; set; }
        public DbSet<Shipment_OrderDeliverdSchedule> Shipment_OrderDeliverdSchedule { get; set; }
        public DbSet<Shipment_OrderColorAndSizeRatio> Shipment_OrderColorAndSizeRatio { get; set; }
        public DbSet<Alert> Alert { get; set; }
        public DbSet<Shipment_UnloadingPort> Shipment_UnloadingPort { get; set; }
        
        public DbSet<Shipment_PortOfLoading> Shipment_PortOfLoading { get; set; }
        public DbSet<Shipment_PortOfDischarge> Shipment_PortOfDischarge { get; set; }
        public DbSet<Shipment_TermsOfShipment> Shipment_TermsOfShipment { get; set; }
        public DbSet<Shipment_CNFDelivaryChallan> Shipment_CNFDelivaryChallan { get; set; }
        public DbSet<Mkt_BuyingAgent> Mkt_BuyingAgent { get; set; }


        //-----------------MIS------------------
        public DbSet<Mis_AtaGlance> Mis_AtaGlance { get; set; }
        public DbSet<Mis_BuyerReceivable> Mis_BuyerReceivable { get; set; }
        public DbSet<Mis_SupplierPayable> Mis_SupplierPayable { get; set; }
        public DbSet<Mis_LeftOver_POSlave_Consumption> Mis_LeftOver_POSlave_Consumption { get; set; }
        public DbSet<Mis_OrderStatus> Mis_OrderStatus { get; set; }

        //-------------------Asset management------------------

        public DbSet<Asset_Category> AssetCategory { get; set; }
        public DbSet<Asset_SubCategory> AssetSubCategory { get; set; }
        public DbSet<Asset_Product> AssetProducts { get; set; }
        public DbSet<Asset_ProductBrand> AssetProductBrands { get; set; }
        public DbSet<Asset_ProductModel> AssetProductModels { get; set; }
        public DbSet<Asset_Location> AssetLocations { get; set; }
        public DbSet<Asset_VendorInfo> AssetVendorInfo { get; set; }
        public DbSet<Asset_ProductInfo> AssetProductInfo { get; set; }
        public DbSet<Asset_ProductAssign> AssetAssign { get; set; }
        public DbSet<Asset_ProductAssign_History> AssetAssignHistory { get; set; }
        public DbSet<Asset_Requisition> AssetRequisition { get; set; }
        public DbSet<Asset_Requisition_Details> AssetRequisitionDetails { get; set; }
        public DbSet<Asset_DepreciationHistory> AssetDepreciationInfo { get; set; }
        public DbSet<Asset_ReturnInfo> AssetReturnInfo { get; set; }
        public DbSet<Asset_DisposalInfo> AssetDisposalInfo { get; set; }
        public DbSet<Asset_Returned_History> AssetReturnedHistory { get; set; }
        //public DbSet<Department> Departments { get; set; }
        //public DbSet<HRMS_Employee> Employees { get; set; }
        //public DbSet<BusinessUnit> BusinessUnits { get; set; }
       // public DbSet<AssetManagement.Unit> Units { get; set; }
        //...........................................Temp


        #region  User
        public DbSet<Entity.User.User> Users { get; set; }

        public DbSet<CheckInOut> CheckInOut { get; set; }
        //  User

        #endregion

        #region Common Setting

        public DbSet<HRMS_LineNo> Lines { get; set; }
        public DbSet<HRMS_Country> Country { get; set; }
        public DbSet<HRMS_BusinessUnit> BusinessUnits { get; set; }
        public DbSet<HRMS_Designation> Designations { get; set; }
        public DbSet<HRMS_Unit> Units { get; set; }
        public DbSet<HRMS_Department> Departments { get; set; }
        public DbSet<HRMS_Section> Sections { get; set; }
        public DbSet<HRMS_NotificationUser> NotificationUsers { get; set; }
        public DbSet<SalaryGrade> SalaryGrade { get; set; }
        public DbSet<HRMS_District> District { get; set; }
        public DbSet<HRMS_PoliceStation> PoliceStation { get; set; }
        public DbSet<HRMS_PostOffice> PostOffice { get; set; }
        public DbSet<HRMS_Village> Village { get; set; }
        #endregion


        #region HRMS Context

        //Employee-----
        //   public DbSet<User> Users { get; set; }
        public DbSet<HRMS_Employee> Employees { get; set; }
        public DbSet<HRMS_Spouse> HrmsSpouses { get; set; }
        public DbSet<HRMS_Child> HrmsChildren { get; set; }
        public DbSet<HRMS_Education> HrmsEducations { get; set; }
        public DbSet<HRMS_Skill> HrmsSkills { get; set; }
        public DbSet<HRMS_Training> HrmsTrainings { get; set; }
        public DbSet<HRMS_Reference> HrmsReferences { get; set; }
        public DbSet<HRMS_Salary> HrmsSalaries { get; set; }
        public DbSet<HRMS_Salary_History> HrmsSalaryHistories { get; set; }
        public DbSet<HRMS_Experience> HrmsExperiences { get; set; }
        public DbSet<HRMS_Facility> HrmsFacilities { get; set; }
        public DbSet<HRMS_Facility_History> HrmsFacilityHistories { get; set; }
        public DbSet<HRMS_Hobby> HrmsHobbies { get; set; }
        public DbSet<HRMS_Upload_Document> HrmsUploadDocument { get; set; }
        public DbSet<HRMS_CardTitle> HrmsCardTitle { get; set; }
        public DbSet<EducationDegree> EducationDegree { get; set; }

        //Link 
        public DbSet<HRMS_LineManagerToEmployeeLink> ManagerToEmployeeLinks { get; set; }
        public DbSet<HRMS_AdminToEmployeeLink> AdminToEmployeeLinks { get; set; }
        public DbSet<HRMS_DepartmentHeadToEmployeeLink> HeadToEmployeeLinks { get; set; }
        public DbSet<HRMS_LinkSuperior> HrmsLinkSuperior { get; set; }

        #endregion

        #region Promotion
        public DbSet<HRMS_EmploymentHistory> HrmsEmploymentHistory { get; set; }
        #endregion

        #region Attendance

        public DbSet<HRMS_Daily_Attendance> HrmsDailyAttendances { get; set; }
        public DbSet<HRMS_Attendance_History> HrmsAttendanceHistory { get; set; }
        public DbSet<HRMS_Daily_Instant_Attendance> HrmsDailyInstantAttendances { get; set; }
        public DbSet<HRMS_Shift_Type> HrmsShiftTypes { get; set; }
        public DbSet<HRMS_Employee_Shift_Assign> HrmsEmployeeShiftAssigns { get; set; }
        public DbSet<HRMS_Attendance_Remarks> HrmsAttendanceRemarks { get; set; }
        public DbSet<HRMS_Shift_Rules> HrmsShiftRules { get; set; }

        #endregion

        #region Leave 

        //Leave and Holiday
        public DbSet<HRMS_Leave_Assign> HrmsLeaveAssigns { get; set; }
        public DbSet<HRMS_Leave_Repository> HrmsLeaveRepositories { get; set; }
        public DbSet<HRMS_Leave_Application> HrmsLeaveApplications { get; set; }
        public DbSet<HRMS_Leave_Archieve> HrmsLeaveArchieve { get; set; }
        //public DbSet<HRMS_Leave_Application_Details> HrmsLeaveApplicationDetails { get; set; }
        public DbSet<HRMS_Leave_Type> HrmsLeaveTypes { get; set; }
        public DbSet<HRMS_Employee_OffDay> HrmsEmployeeOffDays { get; set; }
        public DbSet<HRMS_Holiday> HrmsHolidays { get; set; }
        public DbSet<HRMS_Earn_Leave_Payment> HRMS_Earn_Leave_Payment { get; set; }

        #endregion

        #region Payroll 

        public DbSet<HRMS_EodReference> HrmsEodReferences { get; set; }
        public DbSet<HRMS_EodRecord> HrmsEodRecords { get; set; }
        public DbSet<HRMS_EodCurrentInfo> HrmsEodCurrentInfos { get; set; }
        public DbSet<HRMS_Payroll> HrmsPayrolls { get; set; }
        public DbSet<HRMS_PayrollInfo> HrmsPayrollInfos { get; set; }
        public DbSet<HRMS_Bonus> HrmsBonuses { get; set; }
        public DbSet<HRMS_BonusInfo> HrmsBonusInfos { get; set; }
        public DbSet<HRMHistoryEOD> HRMHistoryEOD { get; set; }
        public DbSet<HRMHistoryPay> HRMHistoryPay { get; set; }
        #endregion

        #region Service Benefit
        public DbSet<HRMS_Service_Benefit> HRMS_Service_Benefit { get; set; }


        #endregion

        #region TempDbSet
        public DbSet<RMS> RMS { get; set; }
        public DbSet<TAttendance> TAttendance { get; set; }
        public DbSet<TblAgeWiseEmployeeList> TblAgeWiseEmployeeList { get; set; }
        public DbSet<Temp_Attendance2> Temp_Attendance2 { get; set; }
        public DbSet<Temp_Attendance3> Temp_Attendance3 { get; set; }
        public DbSet<TempAtt> TempAtt { get; set; }
        public DbSet<tempattedit> tempattedit { get; set; }
        public DbSet<TempAttShifting> TempAttShifting { get; set; }
        public DbSet<TEmployeeLeaveAssign> TEmployeeLeaveAssign { get; set; }


        #endregion




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            SetDecimalPrecisions(modelBuilder);
            SetForeignKeys(modelBuilder);

            /*--- Below from HRMS ---*/
            //  base.OnModelCreating(modelBuilder);

             //modelBuilder.Entity<User_User>()
             //  .Property(p => p.SupperUser2).HasDefaultValue(true);

        }



        private void SetDecimalPrecisions(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Mkt_POSlave>().Property(x => x.Consumption).HasPrecision(18, 5);
            modelBuilder.Entity<Mkt_POSlave>().Property(x => x.Price).HasPrecision(18, 5);

            modelBuilder.Entity<Mkt_BOMSlave>().Property(x => x.Consumption).HasPrecision(18, 5);
            modelBuilder.Entity<Mkt_BOMSlave>().Property(x => x.Price).HasPrecision(18, 5);

            modelBuilder.Entity<Mkt_BOMSlaveFabric>().Property(x => x.Consumption).HasPrecision(18, 5);
            modelBuilder.Entity<Mkt_BOMSlaveFabric>().Property(x => x.Price).HasPrecision(18, 5);
            modelBuilder.Entity<Mkt_YarnCalculation>().Property(x => x.Price).HasPrecision(18, 5);
            modelBuilder.Entity<Mkt_BOM>().Property(x => x.UnitPrice).HasPrecision(18, 5);
            modelBuilder.Entity<Mkt_BOM>().Property(x => x.PackPrice).HasPrecision(18, 5);
            modelBuilder.Entity<Acc_ExportRealisation>().Property(x => x.DollarValue).HasPrecision(18, 4);

            modelBuilder.Entity<Prod_PlanReferenceProductionSectionFollowup>().Property(x => x.PicWeight).HasPrecision(18, 4);


            /*----HRMS ---*/
            modelBuilder.Entity<HRMS_Leave_Assign>().Property(x => x.Total).HasPrecision(18, 2);
            modelBuilder.Entity<HRMS_Leave_Assign>().Property(x => x.Taken).HasPrecision(18, 2);
            modelBuilder.Entity<HRMS_Leave_Assign>().Property(x => x.Remaining).HasPrecision(18, 2);

        }

        //public System.Data.Entity.DbSet<Oss.Romo.Models.One> One { get; set; }

            private void SetForeignKeys(DbModelBuilder modelBuilder)
            {
                modelBuilder.Properties<DateTime>()
                    .Configure(c => c.HasColumnType("datetime2"));
            
            #region Common Setting

            modelBuilder.Entity<HRMS_BusinessUnit>()
                .HasRequired<HRMS_Country>(x => x.Country)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Unit>()
                .HasRequired<HRMS_BusinessUnit>(x => x.BusinessUnit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Department>()
                .HasRequired<HRMS_Unit>(x => x.Unit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Section>()
                .HasRequired<HRMS_Department>(x => x.Department)
                .WithMany()
                .WillCascadeOnDelete(false);
            #endregion

            #region HRMS Employee Record


            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<HRMS_Country>(x => x.Country)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<HRMS_BusinessUnit>(x => x.BusinessUnit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<HRMS_Unit> (x => x.Unit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<HRMS_Department>(x => x.Department)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<HRMS_Section>(x => x.Section)
                .WithMany()
                .WillCascadeOnDelete(false);

            //foreign key from LineNo domain model
            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<HRMS_LineNo>(x => x.LineNo)
                .WithMany()
                .WillCascadeOnDelete(false);



            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PresentDistrict)
                .WithMany(t => t.PresentDistrict)
                .HasForeignKey(m => m.PresentDistrict_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PermanentDistrict)
                .WithMany(t => t.PermanentDistrict)
                .HasForeignKey(m => m.PermanentDistrict_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PresentPoliceStation)
                .WithMany(t => t.PresentPoliceStation)
                .HasForeignKey(m => m.PresentPoliceStation_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PermanentPoliceStation)
                .WithMany(t => t.PermanentPoliceStation)
                .HasForeignKey(m => m.PermanentPoliceStation_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PresentPostOffice)
                .WithMany(t => t.PresentPostOffice)
                .HasForeignKey(m => m.PresentPostOffice_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PermanentPostOffice)
                .WithMany(t => t.PermanentPostOffice)
                .HasForeignKey(m => m.PermanentPostOffice_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PresentVillage)
                .WithMany(t => t.PresentVillage)
                .HasForeignKey(m => m.PresentVillage_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PermanentVillage)
                .WithMany(t => t.PermanentVillage)
                .HasForeignKey(m => m.PermanentVillage_ID)
                .WillCascadeOnDelete(false);
            //modelBuilder.Entity<HRMS_Employee>()
            //    .HasRequired<District>(x => x.PresentDistrict)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Employee>()
            //    .HasRequired<District>(x => x.PermanentDistrict)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
               .HasRequired<HRMS_Designation>(x => x.Designation)
               .WithMany()
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Spouse>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Child>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Education>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Experience>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Training>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Skill>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Reference>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Salary>()
             .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
             .WithMany()
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Facility>()
             .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
             .WithMany()
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Hobby>()
             .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
             .WithMany()
             .WillCascadeOnDelete(false);

            #endregion

            #region HRMS EmploymentHistory

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<HRMS_Department>(x => x.Department)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<HRMS_Section>(x => x.Section)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
              .HasRequired<HRMS_Designation>(x => x.Designation)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<HRMS_LineNo>(x => x.LineNo)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                 .HasRequired<HRMS_BusinessUnit>(x => x.BusinessUnit)
                 .WithMany()
                 .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<HRMS_Unit>(x => x.Unit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasOptional<HRMS_EodRecord>(x => x.EodRecord)
                .WithMany()
                .WillCascadeOnDelete(false);

            #endregion

            #region Attendance

            modelBuilder.Entity<HRMS_Employee_Shift_Assign>()
                .HasRequired<HRMS_Shift_Type>(x => x.HRMSShiftType)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee_Shift_Assign>()
                .HasRequired<HRMS_BusinessUnit>(x => x.BusinessUnit)
                .WithMany()
                .WillCascadeOnDelete(false);

            //Relationship between HrmsEmployee to HrmsEmployeeShiftAssign

            modelBuilder.Entity<HRMS_Employee_Shift_Assign>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Daily_Attendance>()
                .HasRequired<HRMS_Shift_Type>(x => x.HrmsShiftType)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Daily_Instant_Attendance>()
                .HasRequired<HRMS_Shift_Type>(x => x.HrmsShiftType)
                .WithMany()
                .WillCascadeOnDelete(false);

            #endregion

            #region PayrollProcessing

            modelBuilder.Entity<HRMS_EodRecord>()
                .HasRequired<HRMS_EodReference>(x => x.HrmsEodReference)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EodRecord>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_PayrollInfo>()
                .HasRequired<HRMS_Payroll>(x => x.HrmsPayroll)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_PayrollInfo>()
                .HasOptional<HRMS_EodReference>(x => x.HrmsEodReference)
                .WithMany()
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_PayrollInfo>()
            //    .HasRequired<HRMS_EodCurrentInfo>(x => x.HrmsEodCurrentInfo)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_PayrollInfo>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_BonusInfo>()
                .HasRequired<HRMS_Bonus>(x => x.HrmsBonus)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_BonusInfo>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_BonusInfo>()
                .HasOptional<HRMS_EodReference>(x => x.HrmsEodReference)
                .WithMany()
                .WillCascadeOnDelete(false);

            #endregion

            #region Compliance

            modelBuilder.Entity<HRMHistoryEOD>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMHistoryEOD>()
                .HasRequired<HRMS_EodReference>(x => x.HrmsEodReference)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMHistoryPay>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMHistoryPay>()
                .HasRequired<HRMS_Payroll>(x => x.HrmsPayroll)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMHistoryPay>()
                .HasRequired<HRMS_EodReference>(x => x.HrmsEodReference)
                .WithMany()
                .WillCascadeOnDelete(false);
            #endregion

            #region Leave

            modelBuilder.Entity<HRMS_Earn_Leave_Payment>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Application>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Leave_Application_Details>()
            //    .HasRequired<HRMS_Leave_Application>(x => x.HrmsLeaveApplication)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Application>()
                .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Repository>()
                .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Repository>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Repository>()
                .HasRequired<HRMS_Leave_Application>(x => x.HrmsLeaveApplication)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Assign>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Assign>()
                .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
                .WithMany()
                .WillCascadeOnDelete(false);

            //Employee_Offday
            modelBuilder.Entity<HRMS_Employee_OffDay>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Archieve>()
                .HasRequired<HRMS_Leave_Application>(x => x.LeaveApplication)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Archieve>()
                .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Archieve>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);
            #endregion

            #region  Service Benefit

            modelBuilder.Entity<HRMS_Service_Benefit>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            #endregion



            //#region Mapping of HRMS

            //modelBuilder.Entity<BusinessUnit>()
            //    .HasRequired<Country>(x => x.Country)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<AssetManagement.Unit>()
            //    .HasRequired<BusinessUnit>(x => x.BusinessUnit)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Department>()
            //    .HasRequired<AssetManagement.Unit>(x => x.Unit)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Section>()
            //        .HasRequired<Department>(x => x.Department)
            //        .WithMany()
            //        .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Employee>()
            //        .HasRequired<Country>(x => x.Country)
            //        .WithMany()
            //        .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Employee>()
            //        .HasRequired<BusinessUnit>(x => x.BusinessUnit)
            //        .WithMany()
            //        .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Employee>()
            //    .HasRequired<AssetManagement.Unit>(x => x.Unit)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Employee>()
            //        .HasRequired<Department>(x => x.Department)
            //        .WithMany()
            //        .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Employee>()
            //        .HasRequired<Section>(x => x.Section)
            //        .WithMany()
            //        .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Employee>()
            //       .HasRequired<Designation>(x => x.Designation)
            //       .WithMany()
            //       .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Spouse>()
            //      .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //      .WithMany()
            //      .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Child>()
            //      .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //      .WithMany()
            //      .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Education>()
            //      .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //      .WithMany()
            //      .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Experience>()
            //      .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //      .WithMany()
            //      .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Training>()
            //      .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //      .WithMany()
            //      .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Skill>()
            //      .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //      .WithMany()
            //      .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Reference>()
            //      .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //      .WithMany()
            //      .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Salary>()
            //     .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //     .WithMany()
            //     .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Facility>()
            //     .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //     .WithMany()
            //     .WillCascadeOnDelete(false);

            //    modelBuilder.Entity<HRMS_Hobby>()
            //     .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //     .WithMany()
            //     .WillCascadeOnDelete(false);





            //modelBuilder.Entity<HRMS_JobCircularSetting>()
            //    .HasRequired<HRMS_JobCircular>(x => x.HRMS_JobCircular)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Recruitment>()
            //    .HasRequired<HRMS_JobCircular>(x => x.HRMS_JobCircular)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Employee_Shift_Assign>()
            //    .HasRequired<HRMS_Shift_Type>(x => x.HRMSShiftType)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Employee_Shift_Assign>()
            //    .HasRequired<BusinessUnit>(x => x.BusinessUnit)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////Relationship between HrmsEmployee to HrmsEmployeeShiftAssign

            //modelBuilder.Entity<HRMS_Employee_Shift_Assign>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Daily_Attendance>()
            //    .HasRequired<HRMS_Shift_Type>(x => x.HrmsShiftType)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Daily_Instant_Attendance>()
            //    .HasRequired<HRMS_Shift_Type>(x => x.HrmsShiftType)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Leave_Application>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Leave_Application_Details>()
            //    .HasRequired<HRMS_Leave_Application>(x => x.HrmsLeaveApplication)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////Leave_Level_Settings---EmployeeId
            //modelBuilder.Entity<HRMS_Leave_Level_Setting>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////Leave_Level_Settings----ApproveBy
            ////modelBuilder.Entity<HRMS_Leave_Level_Setting>()
            ////    .HasRequired<HRMS_Employee>(x => x.HrmsEmployeeApproveBy)
            ////    .WithMany()
            ////    .WillCascadeOnDelete(false);

            ////Leave_Level_Setting ---LeaveApprovalLevelId
            //modelBuilder.Entity<HRMS_Leave_Level_Setting>()
            //    .HasRequired<HRMS_Leave_Approval_Level_Type>(x => x.HrmsLeaveApprovalLevelTypes)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////Employee_Offday
            //modelBuilder.Entity<HRMS_Employee_OffDay>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Leave_Application>()
            //    .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Leave_Repository>()
            //    .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Leave_Repository>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Leave_Repository>()
            //    .HasRequired<HRMS_Leave_Application>(x => x.HrmsLeaveApplication)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);


            ////On Hold the Relation ship
            ////modelBuilder.Entity<HRMS_Holiday>()
            ////    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            ////    .WithMany()
            ////    .WillCascadeOnDelete(false);
            //modelBuilder.Entity<HRMS_Leave_Assign>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Leave_Assign>()
            //    .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////Payroll
            //modelBuilder.Entity<HRMS_Salary_Generate>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);


            ////HRMS_Income_Tax
            //modelBuilder.Entity<HRMS_Income_Tax>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////HRMS_Loan_Assign
            //modelBuilder.Entity<HRMS_Loan_Assign>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////HRMS_Bonus_Assign
            //modelBuilder.Entity<HRMS_Bonus_Assign>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////HRMS_Bank_Statement
            //modelBuilder.Entity<HRMS_Bank_Statement>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////HRMS_Provident_Fund
            //modelBuilder.Entity<HRMS_Provident_Fund>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////HRMS_Bank_Account
            //modelBuilder.Entity<HRMS_Bank_Account>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            ////HRMS_Salary_Generate
            //modelBuilder.Entity<HRMS_Salary_Generate>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Salary_Generate>()
            //    .HasRequired<BusinessUnit>(x => x.BusinessUnit)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //// HRMS_Salary_Grade_Assign
            //modelBuilder.Entity<HRMS_Salary_Grade_Assign>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Salary_Grade_Assign>()
            //    .HasRequired<HRMS_Salary_Grade>(x => x.HrmsSalaryGrade)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);
            //On Hold the Relation ship
            //modelBuilder.Entity<HRMS_Holiday>()
            //    .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);
//#endregion




                #region Mapping For AssetManagement


            modelBuilder.Entity<Asset_SubCategory>()
                    .HasRequired<Asset_Category>(x => x.Category)
                    .WithMany()
                    .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_Product>()
                    .HasRequired<Asset_Category>(x => x.Category)
                    .WithMany()
                    .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductBrand>()
                    .HasRequired<Asset_Product>(x => x.Product)
                    .WithMany()
                    .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductModel>()
                 .HasRequired<Asset_ProductBrand>(x => x.Brand)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductInfo>()
                 .HasRequired<Asset_Product>(x => x.Product)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductInfo>()
                 .HasRequired<Asset_ProductBrand>(x => x.Brand)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductInfo>()
                 .HasRequired<Asset_ProductModel>(x => x.Model)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductInfo>()
                 .HasRequired<Asset_VendorInfo>(x => x.Vendor)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductInfo>()
                 .HasRequired<HRMS_BusinessUnit>(x => x.BusinessUnit)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductInfo>()
                 .HasRequired<HRMS_Department>(x => x.Department)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                //modelBuilder.Entity<Asset_ProductInfo>()
                //.HasRequired<HRMS_Employee>(x => x.Employee)
                //.WithMany()
                //.WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductInfo>()
                .HasRequired<Asset_Location>(x => x.Location)
                .WithMany()
                .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign>()
                .HasRequired<Asset_Product>(x => x.Product)
                .WithMany()
                .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign>()
                 .HasRequired<Asset_ProductBrand>(x => x.Brand)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign>()
                 .HasRequired<Asset_ProductModel>(x => x.Model)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign>()
                 .HasRequired<HRMS_BusinessUnit>(x => x.BusinessUnit)
                 .WithMany()
                 .WillCascadeOnDelete(false);


                modelBuilder.Entity<Asset_ProductAssign>()
                 .HasRequired<HRMS_Department>(x => x.Department)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign>()
                .HasRequired<HRMS_Employee>(x => x.Employee)
                .WithMany()
                .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign>()
                .HasRequired<Asset_Location>(x => x.Location)
                .WithMany()
                .WillCascadeOnDelete(false);

                /* Asset Product Assign History*/
                modelBuilder.Entity<Asset_ProductAssign_History>()
                 .HasRequired<Asset_Product>(x => x.Product)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign_History>()
                 .HasRequired<Asset_ProductBrand>(x => x.Brand)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign_History>()
                 .HasRequired<Asset_ProductModel>(x => x.Model)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign_History>()
                 .HasRequired<HRMS_BusinessUnit>(x => x.BusinessUnit)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign_History>()
                 .HasRequired<HRMS_Department>(x => x.Department)
                 .WithMany()
                 .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign_History>()
                .HasRequired<HRMS_Employee>(x => x.Employee)
                .WithMany()
                .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign_History>()
                .HasRequired<Asset_Location>(x => x.Location)
                .WithMany()
                .WillCascadeOnDelete(false);

                modelBuilder.Entity<Asset_ProductAssign_History>()
                .HasRequired<HRMS_Designation>(x => x.Designation)
                .WithMany()
                .WillCascadeOnDelete(false);

                #endregion
            
        }
    }
}


