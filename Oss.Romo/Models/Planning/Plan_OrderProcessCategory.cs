﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Planning
{
    public class Plan_OrderProcessCategory : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Order Process")]
        [StringLength(50, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; } 

        [DataType(DataType.MultilineText)]
        [StringLength(101, ErrorMessage = "Upto 101 Chracter")]
        public string Description { get; set; }
    }
}