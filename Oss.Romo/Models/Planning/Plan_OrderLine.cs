﻿using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;
namespace Oss.Romo.Models.Planning
{
    public class Plan_OrderLine : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Order ID")]
        public int? Mkt_BOMFK { get; set; }
        [DisplayName("Production Line")]
        public int? Plan_ProductionLineFK { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName(" Start Date")]
        public DateTime StartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName(" Finish Date")]
        public DateTime FinishDate { get; set; }
        [DisplayName(" Supervisor Name")]
        [MaxLength(15, ErrorMessage = "Upto 15 Chracter")]
        public string  SupervisorName { get; set; }



        //...........................New Added Code...................................//

        //public Mkt_BOM Mkt_BOM { get; set; }
        //public Plan_ProductionLine Plan_ProductionLine { get; set; }




    }
}