﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Planning
{
    public class Plan_ProductionLineRunning : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        
        private DateTime? date;
        [Required]
        public DateTime Date
        {
            get
            {
                if (date == null)
                {
                    date = DateTime.Now;
                }
                return date.Value;
            }
            private set { date = value; }
        }


        public int TimeType { get; set; }
        
        public int Plan_ProductionLineFK { get; set; }
    }
}