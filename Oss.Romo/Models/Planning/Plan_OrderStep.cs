﻿using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;
namespace Oss.Romo.Models.Planning
{
    public class Plan_OrderStep : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Order ID")]
        public int? CommonTheOrderFK { get; set; }
        [DisplayName("Process Step")]
        public int? Plan_OrderProcessFK { get; set; }
       // [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        //[DisplayName("Date ")]
        //public DateTime? Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Planning Start Date ")]
        public DateTime PlannedStartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Planning Finish Date")]
        public DateTime PlannedFinishDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Actual Start Date")]
        public DateTime ActualStartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Actual Finish Date")]
        public DateTime ActualFinishDate { get; set; }
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        [DisplayName("Assign To")]
        public string AssignedTo { get; set; }
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DisplayName("Remarks")]
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }




        //...........................New Added Code...................................//
        //public Common_TheOrder Common_TheOrder { get; set; }

        //public Plan_OrderProcess Plan_OrderProcess { get; set; }

    }
}