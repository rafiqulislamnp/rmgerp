﻿using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Planning
{
    public class Plan_OrderLayout : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Order Process")]
        public int Mkt_BOMFk { get; set; }

       
        [DisplayName("Machine Type")]
        public int? Prod_MachineTypeFk { get; set; }

        public decimal SMV { get; set; }
    }
}