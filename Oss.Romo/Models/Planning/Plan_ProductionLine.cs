﻿using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Planning
{
    public class Plan_ProductionLine : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Line Name")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }

        [DisplayName("Line Number")]
        [StringLength(30, ErrorMessage = "Upto 30 Chracter")]
        public string LineNumber { get; set; }

        [DisplayName("Line Description")]
        [StringLength(200, ErrorMessage = "Upto 200 Chracter")]
        public string Description { get; set; }

        [DisplayName("Daily Capacity")]
        public int DailyCapacity { get; set; }

        [DisplayName("Production Process")]
        public int Plan_OrderProcessFK { get; set; }


        //...........................New Added Code...................................//

        //public List<Plan_OrderLine> Plan_OrderLines { get; set; }

        //public List<Prod_DailyTargeSlave> Prod_DailyTargeSlaves { get; set; }

        //public List<Prod_MasterPlanSlave> Prod_MasterPlanSlaves { get; set; }

        //public List<Prod_QualityAssurance> Prod_QualityAssurances { get; set; }


    }
}