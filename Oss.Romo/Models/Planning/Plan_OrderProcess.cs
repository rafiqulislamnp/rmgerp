﻿using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Planning
{
    public class Plan_OrderProcess : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Order Process")]
        public int Plan_OrderProcessCategoryFk { get; set; }

        [DisplayName("Step Name")]
        [StringLength(50, ErrorMessage = "Upto 20 Chracter")]
        public string StepName { get; set; }
        [DisplayName("Serial Number")]
        [StringLength(10, ErrorMessage = "Upto 10 Chracter")]
        public string SerialNo { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(101, ErrorMessage = "Upto 101 Chracter")]
        public string Description { get; set; }

        [DisplayName("Plan Start Day Interval")]
        public int? PlanStartDayInterval { get; set; }

        [DisplayName("Plan End Day Interval")]
        public int? PlanEndtDayInterval { get; set; }

        [DefaultValue(0)]
        [DisplayName("Starting Point")]
        public int StartingPoint { get; set; }

        [DefaultValue(0)]
        [DisplayName("Starting Point")]
        public int WorkingDistance { get; set; }

        //...........................New Added Code...................................//


        //public List<Plan_OrderStep> Plan_OrderSteps { get; set; }
        //public List<Prod_DailyTarget> Prod_DailyTargets { get; set; }

        //public List<Prod_DailyTargeSlave> Prod_DailyTargeSlaves { get; set; }

        //public List<Prod_MasterPlan> Prod_MasterPlans { get; set; }

        //public List<Prod_ProductionLinePlan> Prod_ProductionLinePlans { get; set; }

    }
}