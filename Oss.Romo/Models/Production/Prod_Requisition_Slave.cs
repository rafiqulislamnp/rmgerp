﻿using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_Requisition_Slave: BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        public int? Prod_RequisitionFK { get; set; }

        [DisplayName("Item")]
        //[Required(ErrorMessage = "Item is Required")]
        public int? Raw_ItemFK { get; set; }

        [DisplayName("Style No")]
        //[Required(ErrorMessage = "BOM is Required")]
        public int? Mkt_BOMFK { get; set; }

        [DisplayName("Item Name")]
        public int? Prod_TransitionItemInventoryFk { get; set; }

        [Required(ErrorMessage = "Unit is Required")]
        [DisplayName("Unit")]
        public int? Common_UnitFK { get; set; }

        [DisplayName("Price")]
        [Required(ErrorMessage = "Price is Required")]
        [DefaultValue(0)]
        public decimal? EstimatedPrice { get; set; }

        //[RegularExpression("^[1-9a-zA-Z][0-9a-zA-Z]*$", ErrorMessage = "Can't be zero and decimal point")]
        public decimal? Consumption { get; set; }

        [DisplayName("Request Quantity")]
        [Required(ErrorMessage = "Request Quantity is Required")]
        public decimal? TotalRequired { get; set; }
         
        [StringLength(100, ErrorMessage = "Upto 20 Chracter")]
        [DataType(DataType.MultilineText)]
    
        public string Description { get; set; }

        [DisplayName("Finalize")]
        [DefaultValue(false)]
        public bool IsFinalize { get; set; }


        [DisplayName("Item")]
        [DefaultValue(0)]
        public int? Mkt_POSlaveFK { get; set; }



        //...........................New Added Code...................................//

        //public Prod_Requisition Prod_Requisition { get; set; }
        //public Raw_Item Raw_Item { get; set; }
        //public Common_Unit Common_Unit { get; set; }
        //public List<Mkt_POSlave_Consumption> Mkt_POSlave_Consumptions { get; set; }
    }
}