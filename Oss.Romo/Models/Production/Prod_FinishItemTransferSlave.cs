﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_FinishItemTransferSlave : BaseModel
    {
        [DisplayName("FIT NO")]
        public int Prod_FinishItemTransferFK { get; set; }
        
        public int Prod_InputRequisitionSlaveFK { get; set; }

        [DisplayName("TransferQty")]
        public int Quantity { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
    }
}