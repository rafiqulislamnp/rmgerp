﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_InputRequisitionSlave : BaseModel
    {
        public int Prod_InputRequisitionFk { get; set; }

        [DisplayName("Style No")]
        [Required(ErrorMessage = "Style is Required")]
        public int Mkt_BOMFK { get; set; }

        public string ColorName { get; set; }

        public string Size { get; set; }
        
        [DisplayName("Request Quantity")]
        [Required(ErrorMessage = "Request Quantity is Required")]
        public decimal? TotalRequired { get; set; }

        [Required(ErrorMessage = "Unit is Required")]
        [DisplayName("Unit")]
        public int? Common_UnitFK { get; set; }

        [StringLength(100, ErrorMessage = "Upto 20 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Finalize")]
        [DefaultValue(false)]
        public bool IsFinalize { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
    }
}