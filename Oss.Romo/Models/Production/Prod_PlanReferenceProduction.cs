﻿using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_PlanReferenceProduction : BaseModel
    {

        [DisplayName("Order Process")]
        public int Plan_OrderProcessFK { get; set; }

        [DisplayName("Plan Reference")]
        public int Prod_PlanReferenceFK { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Finish Date")]
        public DateTime FinishDate { get; set; }
        [DisplayName("Extra Cutting")]
        public decimal ExtraCutting { get; set; }
        [DisplayName("SMV")]
        public decimal SMB { get; set; }
        [DisplayName("Machine Quantity")]
        public int MachineQuantity { get; set; }
        [DisplayName("Working Hour")]
        public decimal WorkingHour { get; set; }
        [DisplayName("Target Days")]
        public decimal TargetDays { get; set; }
        
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("No Of Operator")]
        [DefaultValue(0)]
        public int NoOfOpetator { get; set; }
        [DisplayName("No Of Helper")]
        [DefaultValue(0)]
        public int NoOfHelper { get; set; }
        [DisplayName("Per Hour Target")]
        [DefaultValue(0)]
        public int TargetPerHour { get; set; }




        //[DisplayName("Line Name")]
        //public int? Plan_ProductionLineFk { get; set; }


        //[StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        //[DataType(DataType.MultilineText)]
        //public string Description { get; set; }

        //[DisplayName("Colour")]
        //public string ColourName { get; set; }

        //[DisplayName("Size Name")]
        //public string SizeName { get; set; }

        //[DisplayName("Quantity")]
        //public int? SizeQty { get; set; }




        //...........................New Added Code...................................//

        //public Prod_MasterPlan Prod_MasterPlan { get; set; }

        //public Plan_ProductionLine Plan_ProductionLine { get; set; }

    }
}