﻿using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_Consumption : BaseModel
    {
        
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        public int? Raw_InternaleTransferSlaveFk { get; set; }

        [DisplayName("Quantity")]
        public int? Quantity { get; set; }
        

    }
}