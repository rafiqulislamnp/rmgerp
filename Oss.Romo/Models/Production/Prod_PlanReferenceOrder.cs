﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_PlanReferenceOrder : BaseModel
    {
        [DisplayName("Plan Reference")]
        public int Prod_ReferenceFK { get; set; }

        public int MktBomFk { get; set; }

        [Display(Name = "Port")]
        public int? OrderDeliveryScheduleFk { get; set; }

        public int? Mkt_OrderColorAndSizeRatioFk { get; set; }

        [Display(Name = "Quantity")]
        public int Quantity { get; set; }

        [DisplayName("Line Name")]
        public int Plan_ProductionLineFk { get; set; }

        [Display(Name = "CuttingPlanQuantity")]
        public int CuttingPlanQuantity { get; set; }
        
        [DisplayName("CuttingExtra")]
        public int CuttingExtra { get; set; }

        [DisplayName("GSM")]
        public decimal GSM { get; set; }

        [DisplayName("SewingPlanQuantity")]
        public int SewingPlanQuantity { get; set; }
        
        [Display(Name = "SMV")]
        public decimal SMV { get; set; }

        [DisplayName("MachineRunning")]
        public int MachineRunning { get; set; }

        [DisplayName("WorkingHour")]
        public int WorkingHour { get; set; }

        [DisplayName("Line Chief")]
        public int? Prod_LineChiefFk { get; set; }

        [DisplayName("Super Visor")]
        public int? Prod_SVNameFk { get; set; }

        [DisplayName("IronPlanQuantity")]
        public int IronPlanQuantity { get; set; }
        
        [DisplayName("PackingPlanQuantity")]
        public int PackingPlanQuantity { get; set; }
        
        public int Layer { get; set; }

        public decimal PerLayerWeight { get; set; }

        public int MarkerPiece { get; set; }

        public decimal MarkerLength { get; set; }

        public decimal MarkerWidth { get; set; }

        public int PresentOperator { get; set; }

        public int PresentHelper { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        public int SectionId { get; set; }
    }
}