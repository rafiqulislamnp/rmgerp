﻿using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_PlanReferenceProductionSectionFollowup : BaseModel
    {
        public int? Prod_PlanReferenceProductionSectionFk { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }
        [DisplayName("Quantity")]
        [Required(ErrorMessage = "Company name is required")]
        
        public int? Quantity { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Sample Count")]
        public decimal? SampleCount { get; set; }
       
        [DisplayName("Rejected Quantity")]
        public decimal? Rejected { get; set; }

        [MaxLength(200, ErrorMessage = "Upto 100 Chracter")]
        [DisplayName("Note")]
        public string Note { get; set; }

        [DisplayName("Weight")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n4}", ApplyFormatInEditMode = true)]
        public decimal? PicWeight { get; set; }

        [DisplayName("No")]
        public string CID { get; set; }
        //...........................New Added Code...................................//

        //public Prod_MasterPlan Prod_MasterPlan { get; set; }

        //public Plan_ProductionLine Plan_ProductionLine { get; set; }

    }
}