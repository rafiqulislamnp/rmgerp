﻿using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_TransitionItemInventory : BaseModel
    {
       
        [DisplayName("Style")]
        public int Mkt_BOMFk { get; set; }
        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public int Prod_PlanReferenceProductionSectionFk { get; set; }

        [DisplayName("Quantity")]
        public int? Quantity { get; set; }

        // we will be change this propert as itemFk
        [DisplayName("Item Name")]
        public string ItemName { get; set; }

        [DisplayName("Unit")]
        public int Common_UnitFK { get; set; }

        [DisplayName("Machine DIA")]
        public int? Prod_MachineDIAFk { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        

    }
}