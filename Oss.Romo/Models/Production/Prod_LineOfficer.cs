﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_LineOfficer : BaseModel
    {
        [DisplayName("Line Name")]
        public int Plan_ProductionLineFK { get; set; }

        [DisplayName("Line Chief")]
        public int Prod_LineChiefFK { get; set; }

        [DisplayName("Line Supervisor")]
        public int Prod_LineSuperVisorFk { get; set; }
        
    }
}