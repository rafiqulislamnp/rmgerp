﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_PlanAchievment : BaseModel
    {
        public int? Prod_OrderPlanningFk { get; set; }

        public int Plan_ProductionLineFk { get; set; }
        
        public int Mkt_BOMFK { get; set; }

        [DisplayName("Per Hour Capacity ")]
        public decimal HourCapacity { get; set; }

        [DisplayName("Working Hour")]
        public decimal WorkingHour { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Achiev Date")]
        public DateTime AchiveDate { get; set; }

        [DisplayName("Achiev Qty")]
        public decimal PlanAchievQty { get; set; }

        public bool IsPlanedLine { get; set; }
    }
}