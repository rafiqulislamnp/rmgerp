﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_Machine : BaseModel
    {

        [DefaultValue(0)]
        public int? FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int? LastEditeddBy { get; set; }

        [DisplayName("Machine")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Machine Type")]
        public int Prod_MachineTypeFK { get; set; }


    }
}