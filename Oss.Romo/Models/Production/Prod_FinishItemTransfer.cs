﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_FinishItemTransfer : BaseModel
    {
        [MaxLength(30)]
        [DisplayName("FIT NO")]
        public string CID { get; set; }

        [DisplayName("FIR No.")]
        public int Prod_InputRequisitionFK { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }
        
        [Required(ErrorMessage = "From Department is required"), Display(Name = "From Department")]
        public int FromUser_DeptFK { get; set; }

        [Required(ErrorMessage = "To Department is required"), Display(Name = "To Department")]
        public int ToUser_DeptFK { get; set; }

        [DefaultValue(false)]
        public bool IsAuthorize { get; set; }

        [DefaultValue(false)]
        public bool IsAcknowledge { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime AcknowledgeDate { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}