﻿using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_TransitionItemMadeFrom : BaseModel
    {
        [DisplayName("Raw Item")]
        public int? Raw_ItemFK { get; set; }
        
        public int Prod_TransitionItemInventoryFK { get; set; }

        [DisplayName("From Item")]
        public int Prod_TransitionItemInventoryFromFK { get; set; }

        // we will be change this propert as itemFk
        [DisplayName("Quantity")]
        public Decimal MakeFromQuantity { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Item")]
        [DefaultValue(0)]
        public int? Mkt_POSlaveFK { get; set; }

    }
}