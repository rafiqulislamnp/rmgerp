﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_SMVLayout : BaseModel
    {
        [DisplayName("Order")]
        public int Mkt_BOMFk { get; set; }

        [DisplayName("Total O/P")]
        public int NoOfOperator { get; set; }

        [DisplayName("Total H/P")]
        public int NoOfHelper { get; set; }

        [DisplayName("O/P SMV")]
        public decimal OperatorSMV { get; set; }

        [DisplayName("H/P SMV")]
        public decimal HelperSMV { get; set; }

        [DisplayName("Total Worker")]
        public int TotalWorker { get; set; }

        [DisplayName("SMV")]
        public decimal TotalSMV { get; set; }

        [DisplayName("Work Hour")]
        public int DailyWorkHour { get; set; }

        [DisplayName("Hour Target")]
        public int PerHourTarget { get; set; }

        [DisplayName("Efficiency")]
        public decimal Efficiency { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Description")]
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
      



    }
}