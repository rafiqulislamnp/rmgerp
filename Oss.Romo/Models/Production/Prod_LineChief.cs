﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_LineChief : BaseModel
    {
        [DisplayName("Line Chief")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }



        //...........................New Added Code...................................//

        // public List<Prod_DailyTargeSlave> Prod_DailyTargeSlaves { get; set; }

        //public List<Prod_ProductionLinePlan> Prod_ProductionLinePlans { get; set; }

    }
}