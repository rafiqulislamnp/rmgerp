﻿using System;
using System.Collections.Generic;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_PlanReferenceRemarks : BaseModel
    {
        public int Prod_ReferenceFK { get; set; }

        public int Plan_ProductionLineFk { get; set; }

        public int MktBomFk { get; set; }

        public string Remarks { get; set; }
    }
}