﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_InputRequisition : BaseModel
    {
        [DisplayName("FIR No.")]
        [StringLength(25)]
        public string RequisitionCID { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Delivary Date")]
        public DateTime? Date { get; set; }

        [Required(ErrorMessage = "Unit/Line/Department is Required"), Display(Name = "Unit/Line/Department")]
        public int FromUser_DeptFK { get; set; }

        [Required(ErrorMessage = "To Which Department is required"), Display(Name = "To Which Department")]
        public int ToUser_DeptFK { get; set; }

        [Required(ErrorMessage = "Priority is Required")]
        public string Priority { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        
        [DefaultValue(false)]
        public bool IsComplete { get; set; }

        [DefaultValue(false)]
        public bool IsAutherized { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
    }
}