﻿using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_Capacity : BaseModel
    {

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        
       
        public int Month { get; set; }
        public string Year { get; set; }

        [DisplayName("Total Working Days")]
        public int? TotalWorkingDays { get; set; }

        [DisplayName("Per Day Working Hour")]
        public int? PerDayWorkingHour { get; set; }

        [DisplayName("Total Line In Factory")]
        public int? TotalLineInFactory { get; set; }
        [NotMapped]
        public string MonthYear { get; set; }
        
    }
}