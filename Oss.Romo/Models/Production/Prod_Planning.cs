﻿using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_Planning : BaseModel
    {
        public int Prod_OrderPlanningFK { get; set; }

        [DisplayName("Style No.")]
        public int Mkt_BOMFK { get; set; }

        [DisplayName("Line"), Required(ErrorMessage = "Production line is required")]
        public int Plan_ProductionLineFk { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Finish Date")]
        public DateTime FinishDate { get; set; }
       
        [DisplayName("Per Hour Capacity ")]
        public decimal Capacity { get; set; }

        [DisplayName("Machine Qty")]
        public int MachineQuantity { get; set; }

        [DisplayName("Working Hour")]
        public decimal WorkingHour { get; set; }

        [DisplayName("Target Days")]
        public decimal TargetDays { get; set; }

        [DisplayName("Daily Target")]
        public decimal DayOutputQty { get; set; }

        [DisplayName("Total Qty")]
        public decimal TargetOutputQty { get; set; }

        [DisplayName("Remain Qty")]
        public decimal RemainQty { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public bool IsClosed { get; set; }

        public bool IsPaused { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        
        //...........................New Added Code...................................//

        //public Prod_MasterPlan Prod_MasterPlan { get; set; }

        //public Plan_ProductionLine Plan_ProductionLine { get; set; }

    }
}