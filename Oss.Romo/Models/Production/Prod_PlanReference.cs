﻿using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_PlanReference : BaseModel
    {
        [DisplayName("CID")]
        public string CID { get; set; }

        //Not needed
        [DisplayName("Order No")]
        public int Mkt_BOMFK { get; set; }

        [DisplayName("Port Name")]
        public int Mkt_OrderDeliveryScheduleFK { get; set; }

        //Not needed
        [DisplayName("Quantity")]
        public int Quantity { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }





        //[DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        //[DisplayName("Start Date")]
        //public DateTime StartDate { get; set; }

        //[DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        //[DisplayName("Finish Date")]
        //public DateTime FinishDate { get; set; }

        //[DisplayName("Line QTY")]
        //public int LineQTY { get; set; } //4

        //[DisplayName("Line Name")]
        //[MaxLength(40, ErrorMessage = "Upto 40 Chracter")]
        //public string LineName { get; set; } //A,B,G,J
        //[DisplayName("Perline")]
        //public int PerLine { get; set; } //2300
        //[DisplayName("Working Days")]
        //public int WorkingDays { get; set; }

        //[DisplayName("Remarks")]
        //[MaxLength(40, ErrorMessage = "Upto 200 Chracter")]
        //public string Remarks { get; set; } //2300



        //[DisplayName("Production Step")]
        //public int Plan_OrderProcessFK { get; set; }

        //[DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        //[DisplayName("Buyer Insp. Date")]
        //public DateTime BuyerInspDate { get; set; }





        //...........................New Added Code...................................//
        //public Mkt_BOM Mkt_BOM { get; set; }

        //public Plan_OrderProcess Plan_OrderProcess { get; set; }

        //public List<Prod_MasterPlanSlave> Prod_MasterPlanSlaves { get; set; }



    }
}