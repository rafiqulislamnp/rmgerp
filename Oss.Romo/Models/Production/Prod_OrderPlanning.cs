﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_OrderPlanning : BaseModel
    {
        [DisplayName("Style No.")]
        public int Mkt_BOMFK { get; set; }

        [DisplayName("Order Plan Qty"), Required(ErrorMessage ="Order plan quantity is required.")]
        public decimal PlanQty { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Shipment Date")]
        public DateTime ShipmentDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Sample Approved Date")]
        public DateTime SampleApprovedDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Fabric Inhouse Date")]
        public DateTime FabricInhouseDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Accessories Inhouse Date")]
        public DateTime AccessoriesInhouseDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Cutting Start Date")]
        public DateTime CuttingStartDate { get; set; }

        public bool IsClosed { get; set; }

        public bool IsPaused { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
    }
}