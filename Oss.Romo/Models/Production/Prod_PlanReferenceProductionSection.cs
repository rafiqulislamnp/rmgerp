﻿using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_PlanReferenceProductionSection : BaseModel
    {

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

       

        [DisplayName("Line Name")]
        public int? Plan_ProductionLineFk { get; set; }

        public int Prod_PlanReferenceProductionFk { get; set; }

        [DisplayName("Color")]
        public string Color { get; set; }
        [DisplayName("Line Chief")]
        public int? Prod_LineChiefFk { get; set; }
        [DisplayName("Super Visor")]
        public int? Prod_SVNameFk { get; set; }
        
        public int Layer { get; set; }

        [DisplayName("Marker Pics")]
        public int MarkerPics { get; set; }
        [DisplayName("Marker Length")]
        public int MarkerLength { get; set; }
        [DisplayName("Marker Width")]
        public int MarkerWidth { get; set; }
        public int OrderDeliveryScheduleId { get; set; }
        
        [DisplayName("Size Name")]
        public string Size { get; set; }

        [DisplayName("Plan Quantity")]
        public decimal? Quantity { get; set; }

        
      

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Start Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        [DisplayName("Finish Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FinishDate { get; set; }

        [DisplayName("Lot NO")]
        [DefaultValue(0)]
        public int LotNo { get; set; }
        [DefaultValue(0)]

        [DisplayName("Machine Running")]
        public int MachineRunning { get; set; }
        
        //...........................New Added Code...................................//

        //public Prod_MasterPlan Prod_MasterPlan { get; set; }

        //public Plan_ProductionLine Plan_ProductionLine { get; set; }

    }

    //public class Test
    //{
    //    public string Color { get; set; }
    //    public string Quantity { get; set; }
    //}
}