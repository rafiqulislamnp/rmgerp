﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_PlanReferenceOrderSection : BaseModel
    {
        public int Prod_PlanReferenceOrderFk { get; set; }

        public int Plan_ReferenceSectionFk { get; set; }
        
        public int OrderDeliveryScheduleFk { get; set; }

        public int Mkt_OrderColorAndSizeRatioFk { get; set; }

        [DisplayName("Quantity")]
        public decimal Quantity { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
    }
}