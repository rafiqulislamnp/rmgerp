﻿using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class Prod_ProductionSection : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        public int Plan_OrderProcessFK { get; set; }

        [DisplayName("Step Name")]
        [StringLength(50, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }

        [DisplayName("Serial Number")]
        [StringLength(10, ErrorMessage = "Upto 10 Chracter")]
        public string SerialNo { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(101, ErrorMessage = "Upto 101 Chracter")]
        public string Description { get; set; }

       

        //...........................New Added Code...................................//

        // public Plan_OrderProcess Plan_OrderProcess { get; set; }

        //public Mkt_BOM Mkt_BOM { get; set; }


    }

}