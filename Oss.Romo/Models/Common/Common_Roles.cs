﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Common
{
    public class Common_Roles:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Roles")]
        [StringLength(20, ErrorMessage = "upto 20 Chracter")]
        public string Name { get; set; }
        [StringLength(100, ErrorMessage = "upto 100 Chracter")]
        public string Description { get; set; }

        //...........................New Added Code...................................//

        //public List<Common_RolesPermission> Common_RolesPermissions { get; set; }

        //public List<Common_UserRoles> Common_UserRoless { get; set; }
    }
}