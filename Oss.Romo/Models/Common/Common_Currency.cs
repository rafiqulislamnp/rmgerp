﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;

namespace Oss.Romo.Models.Common
{
    public class Common_Currency : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Currency")]
        [MaxLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }

        [MaxLength(1, ErrorMessage = "Upto 1 Chracter")]
        [Required(ErrorMessage = "Symbol is Required")]
        public string Symbol { get; set; }

        [MaxLength(4, ErrorMessage = "Upto 4 Chracter")]
        public string Code { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        //...........................New Added Code...................................//

        //public List<Acc_Expenses_History> Acc_Expenses_Historys { get; set; }

        //public List<Common_TheOrder> Common_TheOrders { get; set; }

        //public List<Mkt_BOMSlave> Mkt_BOMSlaves { get; set; }

        //public List<Mkt_BOMSlaveFabric> Mkt_BOMSlaveFabrics { get; set; }

        //public List<Mkt_BOMStepJobs> Mkt_BOMStepJobss { get; set; }

        //public List<Mkt_PO> Mkt_POs { get; set; }

        //public List<Prod_PoHistory> Prod_PoHistorys { get; set; }

        //public List<Prod_PoHistoryType> Prod_PoHistoryTypes { get; set; }


    }
}