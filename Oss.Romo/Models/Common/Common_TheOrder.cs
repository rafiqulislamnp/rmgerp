﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Shipment;

namespace Oss.Romo.Models.Common
{
    public class Common_TheOrder : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DisplayName("Order ID")]
        public string CID { get; set; }


        [DisplayName("Order Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime OrderDate { get; set; }
                
        [DisplayName("Buyer")]
        public int Mkt_BuyerFK { get; set; }

        [DisplayName("Buyer PO")]
        [Required(ErrorMessage = "BuyerPO is Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string BuyerPO { get; set; }

        [DisplayName("Season/Year")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Season { get; set; }

        [DisplayName("Payment Terms")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string PaymentTerms { get; set; }

        [DisplayName("Payment Method")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string PaymentMethod { get; set; }


        [DisplayName("Currency")]
        public int Common_CurrencyFK { get; set; }

        [DisplayName("Reference")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Reference { get; set; }

        //[DisplayName("Supplier")]
        //[MaxLength(20, ErrorMessage = "Upto 20 Chracter")]
        //public string Supplier { get; set; }

        [DisplayName("Agent")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Agent { get; set; }

        [DisplayName("Inspection Agent")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string InspectionAgent { get; set; }

        [MaxLength(200, ErrorMessage = "Upto 200 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Commission")] 
        [DefaultValue(0)]    
        public decimal? Commission { get; set; }

        [DefaultValue(false)]
        public bool IsComplete { get; set; }

        //...........................New Added Code...................................//

        //public Mkt_Buyer Mkt_Buyer { get; set; }

        //public Common_Currency Common_Currency { get; set; }

        //public List<Mkt_BOM> Mkt_BOMs { get; set; }

        //public List<Plan_OrderStep> Plan_OrderSteps { get; set; }

        //public List<Prod_OrderStatus> Prod_OrderStatuss { get; set; }

        //public List<Prod_QualityAssurance> Prod_QualityAssurances { get; set; }
         //public List<Shipment_History> Shipment_Historys { get; set; }

    }
}