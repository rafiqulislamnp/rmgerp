﻿using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Common
{
    public class Common_User : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        //unique
        [Index("IX_UserName", 1, IsUnique = true)]
        [Required(ErrorMessage = "User name is required")]
        [MaxLength(10, ErrorMessage = "Upto 10 Chracter")]
        public string UserName { get; set; }

        //[Required(ErrorMessage = "Required")]
        [StringLength(40, ErrorMessage = "Minimum 6 upto 12 Chracter")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Minimum 3 upto 30 Chracter")]
        public string Name { get; set; }

        
        [StringLength(50)]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }


        [StringLength(200)]
        
        public string Photo { get; set; }

       
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

       
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }


        //...........................New Added Code...................................//

        //public List<Common_Permission> Common_Permissions { get; set; }

        //public List<Common_UserRoles> Common_UserRoless { get; set; }

        //public List<Raw_Rrequisition>Raw_Rrequisitions { get; set; }
        //public List<Raw_RrequisitionSlave> Raw_RrequisitionSlaves { get; set; }
       


    }
}