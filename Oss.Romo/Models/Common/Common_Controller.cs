﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Oss.Romo.Models.Common
{
    public class Common_Controller : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Controller Name")]
        public string Name { get; set; }

        //...........................New Added Code...................................//

        //public List<Common_Method> Common_Methods { get; set; }
    }
}