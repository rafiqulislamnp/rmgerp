﻿using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Common
{
    public class Common_Supplier : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Supplier")]
        [Required(ErrorMessage = "Supplier Name is required")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Minimum 3 upto 100 Chracter")]
        public string Name { get; set; }
        [DisplayName("CID")]
        [Required(ErrorMessage = "Supplier Name is required")]
        [StringLength(10, ErrorMessage = "Upto 10 Chracter")]
        public string CID { get; set; }
        [StringLength(100)]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        //[Required(ErrorMessage = "Mobile number is required")]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }

        [DisplayName("Country")]
        public int Common_CountryFk { get; set; }


        [DisplayName("Account Head Category")]
        public int Acc_AcNameFk { get; set; }

        [DisplayName("Supplier Type")]
        public int Common_SupplierTypeFk { get; set; }

        //...........................New Added Code...................................//

        //public Common_Country Common_Country { get; set; }

        //public List<Acc_SupplierPH> Acc_SupplierPHs { get; set; }

        //public List<Commercial_B2bLC> Commercial_B2bLCs { get; set; }

        //public List<Mkt_BOMSlave> Mkt_BOMSlaves { get; set; }

        //public List<Mkt_BOMSlaveFabric> Mkt_BOMSlaveFabrics { get; set; }

        //public List<Mkt_BOMStepJobs> Mkt_BOMStepJobss { get; set; }

        //public List<Mkt_PO> Mkt_POs { get; set; }




    }
    public class Common_SupplierType : BaseModel
    {
        [DisplayName("Supplier Type")]
        [Required(ErrorMessage = "Supplier Type is required")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Minimum 2 upto 100 Chracter")]
        public string Name { get; set; }
    }
    public class Common_BillType : BaseModel
    {
        [DisplayName("Bill Type")]
        [Required(ErrorMessage = "Bill Type is required")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Minimum 2 upto 100 Chracter")]
        public string Name { get; set; }
    }
}