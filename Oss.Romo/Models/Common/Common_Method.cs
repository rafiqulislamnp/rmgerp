﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Oss.Romo.Models.Common
{
    public class Common_Method: BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Method Name")]
        public string Name { get; set; }
        [DisplayName("Controller")]
        public int? Common_ControllerFk { get; set; }
        [StringLength(100, ErrorMessage = "upto 100 Chracter")]
        public string Description { get; set; }
        [StringLength(20, ErrorMessage = "upto 20 Chracter")]
        public string MenuName { get; set; }

        //...........................New Added Code...................................//

        //public Common_Controller Common_Controller { get; set; }

        //public List<Common_Permission> Common_Permissions { get; set; }

        //public List<Common_RolesPermission> Common_RolesPermissions { get; set; }
    }
}