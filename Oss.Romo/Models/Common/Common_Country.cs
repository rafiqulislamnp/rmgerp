﻿using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Common
{
    public class Common_Country: BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Country")]
        public string Name { get; set; }

        //...........................New Added Code...................................//

        //public List<Commercial_Bank> Commercial_Banks { get; set; }

        //public List<Common_Supplier> Common_Suppliers { get; set; }

        //public List<Mkt_Buyer> Mkt_Buyers { get; set; }
       
    }
}