﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Common
{
    public class Common_UserRoles:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        public int? Common_UserFk { get; set; }
        public int? Common_RolesFk { get; set; }

        //...........................New Added Code...................................//

        //public Common_User Common_User { get; set; }

        //public Common_Roles Common_Roles { get; set; }
    }
}