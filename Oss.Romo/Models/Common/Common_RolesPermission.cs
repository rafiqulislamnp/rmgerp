﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Common
{
    public class Common_RolesPermission:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        public int? Common_RolesFK { get; set; }
        public int? Common_MethodFK { get; set; }

        //...........................New Added Code...................................//
        //public Common_Roles Common_Roles { get; set; }
        //public Common_Method Common_Method { get; set; }
    }
}