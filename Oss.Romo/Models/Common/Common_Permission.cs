﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Oss.Romo.Models.Common
{
    public class Common_Permission: BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        public int? Common_UserFk { get; set; }
        public int? Common_MethodFk { get; set; }

        //...........................New Added Code...................................//

        //public Common_User Common_User { get; set; }

        //public Common_Method Common_Method { get; set; }
    }
}