﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace Oss.Romo.Models.AssetManagement
{
    public class SqlDataAccess
    {
        public SqlDataAccess()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static SqlConnection myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["xOssContext"].ToString());

        public static DataSet SQL_ExecuteReader(string CommandText, out string ResponseMsg)
        {
            DataSet ds = new DataSet();
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                SqlDataAdapter ad = new SqlDataAdapter(CommandText, myConnection);
                ad.Fill(ds);
                ad.Dispose();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                ResponseMsg = "Command Executed Successfully";
                return ds;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error happened when Executing Command" + ex.Message);
                //if (ex.Message.Contains("This could be because the pre-login handshake failed or the server was unable to respond back in time"))
                //{
                //    return SQL_ExecuteReader(CommandText, out ResponseMsg);
                //}
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                ResponseMsg = "Error Message: " + ex.Message + ", Stack Trace: " + ex.StackTrace;
                return ds;
            }
        }

        public static string SQL_ExecuteCommand(string CommandText)
        {
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                SqlDataAdapter myCommand = new SqlDataAdapter(CommandText, myConnection);
                DataSet ds = new DataSet();
                myCommand.Fill(ds);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Command Executed Successfully";
            }
            catch (Exception ex)
            {

                //MessageBox.Show("Error happened when Executing Command" + ex.Message);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Error. " + ex.Message;
            }
        }

        public static string SQL_ExecuteNonQuery(SqlCommand cmd, CommandType cmdType)
        {
            cmd.CommandType = cmdType;
            cmd.Connection = myConnection;
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                cmd.ExecuteNonQuery();
                return "Command Executed Successfully";
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error happened when Executing Command" + ex.Message);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Error. " + ex.Message;
            }
            finally
            {
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                myConnection.Dispose();
            }
        }

        public static DataSet SQL_ExecuteReader(SqlCommand cmd)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter ad = new SqlDataAdapter();
            cmd.Connection = myConnection;
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                ad.SelectCommand = cmd;
                ad.Fill(ds);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return ds;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error happened when Executing Command" + ex.Message);
                return null;
            }
            finally
            {
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                ad.Dispose();
                myConnection.Dispose();
            }
        }

        public static DataSet SQL_ExecuteReader(string CommandText)
        {
            DataSet ds = new DataSet();
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                SqlDataAdapter ad = new SqlDataAdapter(CommandText, myConnection);
                ad.Fill(ds);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return ds;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error happened when Executing Command" + ex.Message);
                if (ex.Message.Contains("This could be because the pre-login handshake failed or the server was unable to respond back in time"))
                {
                    return SQL_ExecuteReader(CommandText);
                }
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return ds;
            }
        }

        // Insert Data
        public static string SQL_InsertUpdate(string TableName, string Fieldname, string FieldsValue)
        {
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                SqlDataAdapter myCommand = new SqlDataAdapter("INSERT INTO " + TableName + " (" + Fieldname + ") Values (" + FieldsValue + ")", myConnection);

                DataSet ds = new DataSet();
                myCommand.Fill(ds);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                //string message = "Data Submited Successfully";
                return "Data Submited Successfully";

            }

            catch (Exception ex)
            {
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                //MessageBox.Show("Error to Insert record" + ex.Message);
                return ex.Message;
            }
        }

        public static string SQL_InsertUpdate(string TableName, string Fieldname, string FieldsValue, bool error)
        {
            if (ConnectionState.Open != myConnection.State) myConnection.Open();
            SqlDataAdapter myCommand = new SqlDataAdapter("INSERT INTO " + TableName + " (" + Fieldname + ") Values (" + FieldsValue + ")", myConnection);
            DataSet ds = new DataSet();
            myCommand.Fill(ds);
            if (ConnectionState.Closed != myConnection.State) myConnection.Close();
            return "Data Submited Successfully";
        }


        //Update existing data
        public static string SQL_Update(string TableName, string FieldNameANDvalues, string condition, string condidtionvalue)
        {
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                SqlDataAdapter myCommand = new SqlDataAdapter("UPDATE " + TableName + " SET " + FieldNameANDvalues + " WHERE " + condition + "=" + condidtionvalue, myConnection);
                DataSet ds = new DataSet();
                myCommand.Fill(ds);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Data Updated Successfully";
            }
            catch (Exception ex)
            {
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                //MessageBox.Show("Error to Update record" + ex.Message);
                return ex.Message;
            }
        }


        // get data from table
        public static DataSet SQL_Select(string Fieldname, string TableName, string condition, string condidtionvalue)
        {
            DataSet ds = new DataSet();
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                SqlDataAdapter ad = new SqlDataAdapter("SELECT " + Fieldname + " FROM " + TableName + " WHERE " + condition + "=" + condidtionvalue, myConnection);
                ad.Fill(ds);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return ds;
            }
            catch (Exception ex)
            {
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                //MessageBox.Show("Error to Select record" + ex.Message);
                return ds;
            }
        }

        public static DataSet SQL_Select(string Fieldname, string TableName)
        {
            DataSet ds = new DataSet();
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                SqlDataAdapter ad = new SqlDataAdapter("SELECT " + Fieldname + " FROM " + TableName, myConnection);
                ad.Fill(ds);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return ds;
            }
            catch (Exception ex)
            {
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                //MessageBox.Show("Error to Select record " + ex.Message);
                return ds;
            }
        }


        // delete data from table
        public static string SQL_Delete(string TableName, string searchfield, string search)
        {
            try
            {
                if (ConnectionState.Open != myConnection.State) myConnection.Open();
                SqlDataAdapter myCompany = new SqlDataAdapter("DELETE " + TableName + " Where " + searchfield + "=" + search, myConnection);
                //  myCompany.ExecuteNonQuery();
                DataSet ds = new DataSet();


                myCompany.Fill(ds);
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                string message = "Data Deleted Successfully";
                return message;

            }
            catch (Exception ex)
            {
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                // MessageBox.Show("Error to Select record" + ex.Message);
                return ex.Message;
            }
        }

        // **********************Insert Method with Transaction********************************//Written:11-4-17

        public static string SQL_ExecuteCommandWithTransaction(string commandText_1, string commandText_2)
        {
            if (ConnectionState.Open != myConnection.State) myConnection.Open();
            SqlTransaction transaction = myConnection.BeginTransaction();
            try
            {


                // SqlDataAdapter myCommand = new SqlDataAdapter(CommandText, myConnection);
                SqlCommand myCommand_1 = new SqlCommand(commandText_1, myConnection, transaction);
                SqlCommand myCommand_2 = new SqlCommand(commandText_2, myConnection, transaction);
                // DataSet ds = new DataSet();
                //myCommand.Fill(ds);
                myCommand_1.ExecuteNonQuery();
                myCommand_2.ExecuteNonQuery();
                transaction.Commit();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Command Executed Successfully";
            }
            catch (Exception ex)
            {

                //MessageBox.Show("Error happened when Executing Command" + ex.Message);
                transaction.Rollback();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Error. " + ex.Message;
            }
        }

        // **********************Insert Method with Transaction********************************//Written:11-4-17

        public static string SQL_ExecuteCommandWithTransaction(List<string> sqlCommandList)
        {
            if (ConnectionState.Open != myConnection.State) myConnection.Open();
            SqlTransaction transaction = myConnection.BeginTransaction();
            try
            {
                foreach (var commandString in sqlCommandList)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Command Executed Successfully";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Error. " + ex.Message;
            }
        }

        public static string SQL_ExecuteCommandWithTransaction(List<SqlCommand> sqlCommandList)
        {
            if (ConnectionState.Open != myConnection.State) myConnection.Open();
            SqlTransaction transaction = myConnection.BeginTransaction();
            try
            {
                foreach (var command in sqlCommandList)
                {
                    command.Connection = myConnection;
                    command.Transaction = transaction;
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Command Executed Successfully";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Error. " + ex.Message;
            }
        }

        public static string SQL_ExecuteCommandWithTransaction(List<string> sqlCommandList, List<string> updateDebitBalanceList, List<string> updateCreditBalanceList, List<string> insertDebitTransactionQuery, List<string> insertCreditTransactionQuery)
        {
            if (ConnectionState.Open != myConnection.State) myConnection.Open();
            SqlTransaction transaction = myConnection.BeginTransaction();
            try
            {
                foreach (var commandString in sqlCommandList)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                foreach (var commandString in updateDebitBalanceList)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                foreach (var commandString in updateCreditBalanceList)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                foreach (var commandString in insertDebitTransactionQuery)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                foreach (var commandString in insertCreditTransactionQuery)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Command Executed Successfully";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Error. " + ex.Message;
            }
        }
        public static string SQL_ExecuteCommandWithTransaction(List<string> sqlCommandList, List<string> updateDebitBalanceList, List<string> updateCreditBalanceList, List<string> insertDebitTransactionQuery, List<string> insertCreditTransactionQuery, List<string> sqlCostCenterList)
        {
            if (ConnectionState.Open != myConnection.State) myConnection.Open();
            SqlTransaction transaction = myConnection.BeginTransaction();
            try
            {
                foreach (var commandString in sqlCommandList)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                foreach (var commandString in updateDebitBalanceList)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                foreach (var commandString in updateCreditBalanceList)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                foreach (var commandString in insertDebitTransactionQuery)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                foreach (var commandString in insertCreditTransactionQuery)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                foreach (var commandString in sqlCostCenterList)
                {
                    SqlCommand command = new SqlCommand(commandString, myConnection, transaction);
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Command Executed Successfully";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                if (ConnectionState.Closed != myConnection.State) myConnection.Close();
                return "Error. " + ex.Message;
            }
        }

        //Newly Added, Date: 20170905

        /// <summary>
        /// Creates an SQL script that creates a table where the columns matches that of the specified DataTable.
        /// </summary>
        public static string BuildCreateTableScript(DataTable Table)
        {
            if (!IsValidDatatable(Table, ignoreZeroRows: true))
                return string.Empty;

            StringBuilder result = new StringBuilder();
            result.AppendFormat("CREATE TABLE [{1}] ({0}   ", Environment.NewLine, Table.TableName);

            bool FirstTime = true;
            foreach (DataColumn column in Table.Columns.OfType<DataColumn>())
            {
                if (FirstTime) FirstTime = false;
                else
                    result.Append("   ,");

                result.AppendFormat("[{0}] {1} {2} {3}",
                    column.ColumnName, // 0
                    GetSQLTypeAsString(column.DataType), // 1
                    column.AllowDBNull ? "NULL" : "NOT NULL", // 2
                    Environment.NewLine // 3
                );
            }
            result.AppendFormat(") ON [PRIMARY]{0}GO{0}{0}", Environment.NewLine);

            // Build an ALTER TABLE script that adds keys to a table that already exists.
            if (Table.PrimaryKey.Length > 0)
                result.Append(BuildKeysScript(Table));

            return result.ToString();
        }

        /// <summary>
        /// Builds an ALTER TABLE script that adds a primary or composite key to a table that already exists.
        /// </summary>
        private static string BuildKeysScript(DataTable Table)
        {
            // Already checked by public method CreateTable. Un-comment if making the method public
            // if (Helper.IsValidDatatable(Table, IgnoreZeroRows: true)) return string.Empty;
            if (Table.PrimaryKey.Length < 1) return string.Empty;

            StringBuilder result = new StringBuilder();

            if (Table.PrimaryKey.Length == 1)
                result.AppendFormat("ALTER TABLE {1}{0}   ADD PRIMARY KEY ({2}){0}GO{0}{0}", Environment.NewLine, Table.TableName, Table.PrimaryKey[0].ColumnName);
            else
            {
                List<string> compositeKeys = Table.PrimaryKey.OfType<DataColumn>().Select(dc => dc.ColumnName).ToList();
                string keyName = compositeKeys.Aggregate((a, b) => a + b);
                string keys = compositeKeys.Aggregate((a, b) => string.Format("{0}, {1}", a, b));
                result.AppendFormat("ALTER TABLE {1}{0}ADD CONSTRAINT pk_{3} PRIMARY KEY ({2}){0}GO{0}{0}", Environment.NewLine, Table.TableName, keys, keyName);
            }

            return result.ToString();
        }

        /// <summary>
        /// Returns the SQL data type equivalent, as a string for use in SQL script generation methods.
        /// </summary>
        private static string GetSQLTypeAsString(Type DataType)
        {
            switch (DataType.Name)
            {
                case "Boolean": return "[bit]";
                case "Char": return "[char]";
                case "SByte": return "[tinyint]";
                case "Int16": return "[smallint]";
                case "Int32": return "[int]";
                case "Int64": return "[bigint]";
                case "Byte": return "[tinyint] UNSIGNED";
                case "UInt16": return "[smallint] UNSIGNED";
                case "UInt32": return "[int] UNSIGNED";
                case "UInt64": return "[bigint] UNSIGNED";
                case "Single": return "[float]";
                case "Double": return "[double]";
                case "Decimal": return "[decimal]";
                case "DateTime": return "[datetime]";
                case "Guid": return "[uniqueidentifier]";
                case "Object": return "[variant]";
                case "String": return "[nvarchar](250)";
                default: return "[nvarchar](MAX)";
            }
        }

        /// <summary>
        /// Indicates whether a specified DataTable is null, has zero columns, or (optionally) zero rows.
        /// </summary>
        /// <param name="dataTable">DataTable to check.</param>
        /// <param name="ignoreZeroRows">When set to true, the function will return true even if the table's row count is equal to zero.</param>
        /// <returns>False if the specified DataTable null, has zero columns, or zero rows, otherwise true.</returns>
        public static bool IsValidDatatable(DataTable dataTable, bool ignoreZeroRows = false)
        {
            if (dataTable == null)
                return false;
            if (dataTable.Columns.Count == 0)
                return false;
            if (ignoreZeroRows)
                return true;
            if (dataTable.Rows.Count == 0)
                return false;

            return true;
        }

        public static string SqlBulkDataCopy(DataTable table)
        {
            try
            {
                var tblName = string.Empty;
                var connectionString = @"Data Source=OSS-PC\MSSQLSERVER14;Initial Catalog=Tally_10001;Integrated Security=True";
                // take note of SqlBulkCopyOptions.KeepIdentity, you may or may not want to use this for your situation.  

                //using (var bulkCopy = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.KeepIdentity))
                using (var bulkCopy = new SqlBulkCopy(connectionString))
                {
                    // my DataTable column names match my SQL Column names, so I simply made this loop. 
                    // However if your column names don't match, just pass in which datatable name 
                    // matches the SQL column name in Column Mappings

                    foreach (DataColumn col in table.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName.Replace(".", string.Empty));
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    tblName = table.TableName == "GROUP" ? "GROUPS" : table.TableName.Replace(".", string.Empty);
                    bulkCopy.DestinationTableName = tblName;
                    bulkCopy.WriteToServer(table);
                }
                return "Command Executed Successfully";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        // read multiple data from table
        public static List<string> SQL_DataRead(String sql)
        {
            var list = new List<string>();
            try
            {
                myConnection.Open();
                var cmd = new SqlCommand(sql, myConnection);
                SqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    int count = rd.FieldCount;
                    for (int i = 0; i < count; i++)
                    {
                        list.Add(rd[i].ToString());
                    }
                }
                myConnection.Close();
                rd.Close();
                return list;
            }
            catch
            {
                return list;
            }
        }

        // read single data from table
        public static string SQL_StringData(String sql)
        {
            string data = "";
            try
            {
                myConnection.Open();
                var cmd = new SqlCommand(sql, myConnection);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    data = reader[0].ToString();
                }
                myConnection.Close();
                reader.Close();
            }
            catch
            {
                //
            }
            return data;
        }
    }

}