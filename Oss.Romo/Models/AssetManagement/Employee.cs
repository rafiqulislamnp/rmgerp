﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.AssetManagement
{
    public class Employee
    {
        private static readonly Employee instance = new Employee();
        public static Employee Instance
        {
            get { return instance; }
        }

        [Required(ErrorMessage = "* Please Check")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "Invalid")]
        public string EmpSl { get; set; }
        public string EmployeeId { get; set; }
        public List<string> EmployeeIds { get; set; }
        public string Name { get; set; }
        public string NameBn { get; set; }
        public string SpouseName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string Dob { get; set; }
        public string Date { get; set; }
        public string QuitDate { get; set; }
        public string SpouseDob { get; set; }
        public string Gender { get; set; }
        public string Country { get; set; }
        public string Designation { get; set; }
        public string DesignationBn { get; set; }
        public string Department { get; set; }
        public string DepartmentBn { get; set; }
        public string Section { get; set; }
        public string BloodGroup { get; set; }
        public string SpouseBloodGroup { get; set; }
        public string JoiningDate { get; set; }
        public string Intime { get; set; }
        public string OutTime { get; set; }
        public string TotalTime { get; set; }
        public string Late { get; set; }
        public string Early { get; set; }
        public string Case1 { get; set; }
        public string Case2 { get; set; }
        public string Case3 { get; set; }
        public string Remarks { get; set; }
        public string ImagePath { get; set; }

        public string Religion { get; set; }
        [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [Display(Name = "Home Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        //public string PhoneNumber { get; set; }
        public string MobileNo { get; set; }
        public string AlternateMobileNo { get; set; }
        public string EmergencyContactNo { get; set; }
        public string OfficeContact { get; set; }
        public string LandPhone { get; set; }
        public string EmailId { get; set; }
        public string EmployeeType { get; set; }
        public string NidNo { get; set; }
        public string PassportNo { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string MaritalStatus { get; set; }
        public string JoiningStation { get; set; }
        public string LineManagerId { get; set; }
        public string LineManagerName { get; set; }
        public string LineManagerDept { get; set; }
        public string DepartmentHeadId { get; set; }
        public string DepartmentHeadName { get; set; }
        public string DepartmentHeadDept { get; set; }
        public string AdminId { get; set; }
        public string AdminName { get; set; }
        public string AdminDept { get; set; }
        public string Photo { get; set; }
        public string PresentStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Createddate { get; set; }
        public string EditedBy { get; set; }
        public DateTime Editeddate { get; set; }
        public string Response { get; set; }
        public bool CreateAccount { get; set; }

        public string AltContactName { get; set; }
        public string AltContactNo { get; set; }
        public string AltContactAddress { get; set; }
        public string Status { get; set; }
        public string EmpIDLeave { get; set; }

        public string EducationDetails { get; set; }
        public string EmployeeStaffType { get; set; }
        public string OverTimeEligible { get; set; }
        public string Grade { get; set; }
        public string UnitName { get; set; }





        public IEnumerable<DropDownData> GetEmplyeeIdListForUserCreate(bool withName)
        {
            var resp = "";
            var list = new List<DropDownData> { new DropDownData("", ""), new DropDownData("New User", "New User") };//{ new DropDownData("Select Employee ID", "0") }};

            var ds = SqlDataAccess.SQL_ExecuteReader("Select [EmpSl],[Employee_ID],[Name],[Email_ID],[Mobile_No] from [Employee] where [Present_Status]='Active' "+
                "and [Employee_ID] not in (select e.Employee_ID from [dbo].[AccessUsers] a inner join [dbo].[Employee] e on a.Employee_ID = e.Employee_ID) "+
                "order by [EmpSl] asc", out resp);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (withName)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        list.Add(
                            new DropDownData(
                                string.Format("Id: {0}; Name: {1}", ds.Tables[0].Rows[i]["Employee_ID"].ToString(),
                                    ds.Tables[0].Rows[i]["Name"].ToString()),
                                string.Format("{0}__{1}__{2}__{3}__{4}", ds.Tables[0].Rows[i]["EmpSl"].ToString(),
                                    ds.Tables[0].Rows[i]["Employee_ID"].ToString(),
                                    ds.Tables[0].Rows[i]["Name"].ToString(),
                                    ds.Tables[0].Rows[i]["Email_ID"].ToString(),
                                    ds.Tables[0].Rows[i]["Mobile_No"].ToString())));
                    }
                }
                else
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        list.Add(
                            new DropDownData(ds.Tables[0].Rows[i]["Employee_ID"].ToString().Trim(),
                                string.Format("{0}__{1}__{2}__{3}__{4}", ds.Tables[0].Rows[i]["EmpSl"].ToString(),
                                    ds.Tables[0].Rows[i]["Employee_ID"].ToString().Trim(),
                                    ds.Tables[0].Rows[i]["Name"].ToString().Trim(),
                                    ds.Tables[0].Rows[i]["Email_ID"].ToString(),
                                    ds.Tables[0].Rows[i]["Mobile_No"].ToString())));
                    }
                }
            }
            return list;
        }
        
        public IEnumerable<DropDownData> GetDepartmentlist()
        {
            var resp = "";
            var list = new List<DropDownData> { };// {new DropDownData("Select Department Name", "")}};
            list.Add(new DropDownData("", ""));
            var ds = SqlDataAccess.SQL_ExecuteReader("Select Distinct Department_Name from Department", out resp);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    list.Add(new DropDownData(ds.Tables[0].Rows[i][0].ToString(), ds.Tables[0].Rows[i][0].ToString()));
                }
            }
            return list;
        }

    }
}