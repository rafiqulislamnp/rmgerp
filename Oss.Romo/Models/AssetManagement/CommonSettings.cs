﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oss.Romo.Models.AssetManagement
{
    public class CommonSettings
    {
    }

    public class Country: RootModel
    {
        [DisplayName("Country Name")]
        [Required(ErrorMessage = "Please Select Country Name")]
        [Index("UX_Country", 1, IsUnique = true)]
        // [Index("UX_Country", 1, IsUnique = true)]
         [StringLength(50)]
        public string Name { get; set; }
       // public ICollection<HRMS_Employee> Employees { get; set; }
       // public ICollection<BusinessUnit> BusinessUnits { get; set; }
    }

    public class BusinessUnit : RootModel
    {
        [DisplayName("Buniness Unit Name")]
        [Required(ErrorMessage = "Please Enter Business Unit Name")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Name should be minimum 3 characters and Maximum 50 characters")]
        [Index("UX_BU", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        public string Name { get; set; }

       [DisplayName("Country Name")]
       [Required(ErrorMessage = "Please Select Country Name")]
        public int Country_ID { get; set; }
        [ForeignKey ("Country_ID")]
        public Country Country { get; set; }
       public ICollection<Unit> Units { get; set; }
       public ICollection<HRMS_Employee> Employees { get; set; }
    }


    public class Unit : RootModel
    {
        [DisplayName("Unit Name")]
        [Required(ErrorMessage = "Please Enter Unit")]
        [Index("UX_Unit", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string UnitName { get; set; }

        //[ForeignKey("CountrySL")]
        //[Required]
        //public int BuSl { get; set; }

        public int BusinessUnit_ID { get; set; }
        [ForeignKey("BusinessUnit_ID")]
        public BusinessUnit BusinessUnit { get; set; }
        //public ICollection<Department> Departments { get; set; }
        //public ICollection<HRMS_Employee> Employees { get; set; }
    }

    public class Department : RootModel
    {
        [DisplayName("Department Name")]
        [Required(ErrorMessage = "Please Enter Department")]
        [Index("UX_Dept", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string DeptName { get; set; }
      //  [Required(ErrorMessage = "Please Enter Department In Bangla")]
        public string DeptNameBangla { get; set; }

        //[ForeignKey("UnilSl")]
        public int Unit_ID { get; set; }
        [ForeignKey("Unit_ID")]
        public Unit Unit { get; set; }
        //public ICollection<Section> Sections { get; set; }
        //public ICollection<HRMS_Employee> Employees { get; set; }
    }

    public class Section : RootModel
    {
        [DisplayName("Section Name")]
        [Required(ErrorMessage = "Please Enter Section")]
        [Index("UX_Section", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string SectionName { get; set; }
       
        public int Dept_ID { get; set; }
        [ForeignKey("Dept_ID")]
        public Department Department { get; set; }
        public ICollection<HRMS_Employee> Employees { get; set; }
    }

    public class NotificationUser : RootModel
    {
        //[ForeignKey("Id")]
        [DisplayName("Employee Name :")]
        public string  Employee_Id { get; set; }
        [DisplayName()]
        public string Name { get; set; }
        [DisplayName("Email Id:")]
        [Index("Ux_Notification",IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Subject { get; set; }
        [DisplayName("Business Unit")]
        public int BusinessUnit_ID { get; set; }
        [ForeignKey("BusinessUnit_ID")]
        public BusinessUnit BusinessUnit { get; set; }

    }


    public class Designation : RootModel
    {
        [DisplayName("Designation Name")]
        [Required(ErrorMessage = "Please Enter Designation Name")]
        [Index("UX_Designation", 1, IsUnique = true)]
        [StringLength(80, MinimumLength = 3, ErrorMessage = "Designation Name should be minimum 3 characters and maximum 80 characters")]
        public string Name { get; set; }

        //[DisplayName("Designation Name Bangla")]
        //[Required(ErrorMessage = "Please Enter Designation Name Bangla")]
        //[StringLength(100, MinimumLength = 3, ErrorMessage = "Designation Name should be minimum 3 characters and maximum 100 characters")]
        public string DesigNameBangla { get; set; }
        //public int Employee_ID { get; set; }
        //[ForeignKey("Employee_ID")]
        //public  HRMS_Employee HRMS_Employee { get; set; }


    }
    public class CompanyInfo : RootModel
    {
        public string CompanyName { get; set; }
        public int CurrencySl { get; set; }
        public DateTime FinancialYear { get; set; }
        public string Address { get; set; }
        public int IsDeleted { get; set; }
    }


    public class LineManagerToEmployeeLink
    {

    }
}