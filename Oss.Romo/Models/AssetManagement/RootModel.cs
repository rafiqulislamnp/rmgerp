﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.AssetManagement
{
    public class RootModel
    {
        [Key]
        public int ID { get; set; }
        [DefaultValue(true)]
        public bool Status { get; set; }
        //[DefaultValue(DateTime.Now)]

        [DataType(DataType.Date)]
        public DateTime? Entry_Date { get; set; }
        [DataType(DataType.Date)]
        public DateTime? Update_Date { get; set; }

        public string Entry_By { get; set; }

        public string Update_By { get; set; }

        public RootModel()
        {
            Status = true;
        }
    }

    
}