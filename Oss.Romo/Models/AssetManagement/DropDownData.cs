﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.ViewModels;
using Oss.Romo.ViewModels.AssetManagement;

namespace Oss.Romo.Models.AssetManagement
{

    public class DropDownData
    {

        private xOssContext db = new xOssContext();
        public string Text { get; set; }

        public string Value { get; set; }

        public DropDownData()
        {

        }

        public DropDownData(string txt, string val)
        {
            Text = txt;
            Value = val;
        }



        //public IEnumerable<DropDownData> GetCountrylistAll()
        //{
        //    List<string> cultureList = new List<string>();
        //    CultureInfo[] getCultureInfo = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
        //    foreach (CultureInfo getculture in getCultureInfo)
        //    {
        //        RegionInfo getRegionInfo = new RegionInfo(getculture.LCID);

        //        if (!(cultureList.Contains(getRegionInfo.EnglishName)))
        //        {
        //            cultureList.Add(getRegionInfo.EnglishName);
        //        }
        //    }
        //    cultureList.Sort();
        //    var list = new List<DropDownData> { };// { new DropDownData("Select Country Name", "") }};
        //    if (cultureList.Count > 0)
        //    {
        //        for (int i = 0; i < cultureList.Count; i++)
        //        {
        //            list.Add(new DropDownData(cultureList[i], cultureList[i]));
        //        }
        //    }
        //    return list;
        //}

        //public List<object> GetCountryList()
        //{
        //    var CountryList = new List<object>();

        //    foreach (var Country in db.Country.Where(d => d.Status == true))
        //    {
        //        CountryList.Add(new { Text = Country.Name, Value = Country.ID });
        //    }
        //    return CountryList;
        //}

        public List<object> GetBusinessUnitList()
        {
            var BusinessUnitList = new List<object>();

            foreach (var businessUnit in db.BusinessUnits.Where(d => d.Status == true))
            {
                BusinessUnitList.Add(new { Text = businessUnit.Name, Value = businessUnit.ID });
            }
            return BusinessUnitList;
        }

        public List<object> GetUnitList()
        {
            var List = new List<object>();

            foreach (var list in db.Units)
            {
                List.Add(new { Text = list.UnitName, Value = list.ID });
            }
            return List;
        }

        //public List<object> GetDesignationList()
        //{
        //    var List = new List<object>();

        //    foreach (var list in db.Designations.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.Name, Value = list.ID });
        //    }
        //    return List;
        //}

        public List<object> GetDepartmentList()
        {
            var List = new List<object>();

            foreach (var list in db.Departments.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.DeptName, Value = list.ID });
            }
            return List;
        }

        //public List<object> GetSectionList()
        //{
        //    var List = new List<object>();

        //    foreach (var list in db.Sections.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.SectionName, Value = list.ID });
        //    }
        //    return List;
        //}

        //public List<object> GetActiveEmployeeList()
        //{
        //    var List = new List<object>();

        //    foreach (var list in db.Employees.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.EmployeeIdentity, Value = list.Name });
        //    }
        //    return List;
        //}

        //public List<object> GetActiveJobTitleList()
        //{
        //    var List = new List<object>();
        //    foreach (var list in db.HrmsJobCirculars.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.Job_Title, Value = string.Format("{0}__{1}", list.ID, list.Job_Identification) });
        //    }
        //    return List;
        //}

        //public List<object> GetActiveEmployeeListforLink()
        //{
        //    var List = new List<object>();
        //    foreach (var list in db.Employees.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = string.Format("{0}" + "-:-" + "{1}", list.EmployeeIdentity, list.Name), Value = list.ID });
        //    }
        //    return List;
        //}

        public List<object> GetEmployeeList()
        {
            var List = new List<object>();

            foreach (var list in db.Employees.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.EmployeeIdentity, Value = list.ID });
            }
            return List;
        }


        //public List<object> GetShiftTypeName()
        //{
        //    var List = new List<object>();

        //    foreach (var list in db.HrmsShiftTypes.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.ShiftName, Value = list.ID });
        //    }
        //    return List;
        //}

        ///// <summary>
        ///// Load Shift type for dropdown used in Shift wise Attendance Search
        ///// </summary>
        ///// <returns></returns>
        //public List<object> GetShiftTypeNamewithAll()
        //{
        //    var List = new List<object>();
        //    List.Add(new { Text = "All", Value = 100 });
        //    foreach (var list in db.HrmsShiftTypes.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.ShiftName, Value = list.ID });
        //    }
        //    return List;
        //}


        ///// <summary>
        ///// Load leave type for dropdown used in leave application form
        ///// </summary>
        ///// <returns></returns>
        //public List<object> GetLeaveTypeName()
        //{
        //    var List = new List<object>();

        //    foreach (var list in db.HrmsLeaveTypes.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.Leave_Name, Value = list.ID });
        //    }
        //    return List;
        //}

        ///// <summary>
        ///// Load leave type for dropdown used in leave application form
        ///// </summary>
        ///// <returns></returns>
        //public List<object> GetLeaveTypeNameWithAll()
        //{
        //    var List = new List<object>();
        //    List.Add(new { Text = "All", Value = 1000 });
        //    foreach (var list in db.HrmsLeaveTypes.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.Leave_Name, Value = list.ID });
        //    }
        //    return List;
        //}

        ///// <summary>
        ///// Load leave type for dropdown used in leave application form
        ///// </summary>
        ///// <returns></returns>
        //public List<object> GetLeaveTypeAssignYearList()
        //{
        //    var List = new List<object>();
        //    foreach (var list in db.HrmsLeaveAssigns.DistinctBy(x => x.Year).ToList())
        //    {
        //        List.Add(new { Text = list.Year, Value = list.Year });
        //    }
        //    return List;
        //}


        ///// <summary>
        ///// Load Leave Type for Dropdown used in Employee Leave Assign 
        ///// </summary>
        ///// <returns></returns>
        //public List<object> GetLeaveTypeNameWithAmount()
        //{
        //    var List = new List<object>();

        //    foreach (var list in db.HrmsLeaveTypes.Where(d => d.Status == true))
        //    {
        //        // List.Add(new { Text = string.Format("{0}" + "-:-" + "{1}", list.ID, list.Leave_Amount), Value = list.ID });
        //        List.Add(new { Text = list.Leave_Name, Value = string.Format("{0}" + "-:-" + "{1}", list.ID, list.Leave_Amount) });
        //    }
        //    return List;
        //}

        ///// <summary>
        ///// Load Leave Year for Dropdown used in Employee Leave Assign 
        ///// </summary>
        ///// <returns></returns>

        //public List<object> GetAssignLeaveYear()
        //{
        //    var List = new List<object>();
        //    int currentYear = DateTime.Now.Year;
        //    for (int i = currentYear; i < currentYear + 5; i++)
        //    {
        //        List.Add(new DropDownData(i.ToString(), i.ToString()));
        //    }
        //    return List;
        //}


        ///// <summary>
        ///// Load Attendance Remars Dropdown used in Employee Attendance Edit 
        ///// </summary>
        ///// <returns></returns>

        //public List<object> GetAttendanceRemarks()
        //{

        //    var List = new List<object>();
        //    foreach (var list in db.HrmsAttendanceRemarks.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.Remarks, Value = list.Remarks });
        //    }
        //    return List;
        //}

        ///// <summary>
        ///// Load All Grade Data for dropd
        ///// </summary>
        ///// <returns></returns>
        //public List<object> GetSalaryGradeAll()
        //{
        //    var List = new List<object>();
        //    List.Add(new { Text = "All", Value = 1000 });
        //    foreach (var list in db.HrmsSalaryGrades.Where(d => d.Status == true))
        //    {
        //        List.Add(new { Text = list.GradeName, Value = list.ID });
        //    }
        //    return List;
        //}

        //public List<object> GetSalaryGrade()
        //{
        //    var List = new List<object>();
        //    foreach (var list in db.HrmsSalaryGrades.Where(d => d.Status == true).OrderBy(g => g.GradeName))
        //    {
        //        List.Add(new { Text = list.GradeName, Value = list.ID });
        //    }
        //    return List;
        //}

        public List<object> GetAssetCategoryList()
        {
            var List = new List<object>();
            foreach (var list in db.AssetCategory)
            {
                List.Add(new { Text = list.CategoryName, Value = list.ID });
            }
            return List;

        }
        

        public List<object> GetAssetSubCategoryList()
        {
            var List = new List<object>();
            foreach (var list in db.AssetSubCategory)
            {
                List.Add(new { Text = list.SubCategoryName, Value = list.ID });
            }
            return List;

        }

        #region "AssetManagement Dropdown List"

        public static IList<SelectListItem> GetAssetType()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "Asset", Text = "Asset" });
            result.Add(new SelectListItem { Value = "Accessories", Text = "Accessories" });
            return result;
        }

        public static IList<SelectListItem> GetLifeTime()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "0", Text = "Select Year" });
            result.Add(new SelectListItem { Value = "1", Text = "1 year" });
            result.Add(new SelectListItem { Value = "2", Text = "2 year" });
            result.Add(new SelectListItem { Value = "3", Text = "3 year" });
            result.Add(new SelectListItem { Value = "4", Text = "4 year" });
            result.Add(new SelectListItem { Value = "5", Text = "5 year" });
            return result;
        }

        public List<object> GetAssetProductList()
        {
            var productList = new List<object>();

            foreach (var producttype in db.AssetProducts.Where(d => d.Status == true))
            {
                productList.Add(new { Text = producttype.ProductName, Value = producttype.ID });
            }
            return productList;
        }
        public List<object> GetAccessoriesProductList()
        {
            var productList = new List<object>();

            foreach (var product in db.AssetProducts.Where(d => d.IsAccessories == 1))
            {
                productList.Add(new { Text = product.ProductName, Value = product.ID });
            }
            return productList;
        }

        public List<object> GetAssetBrandList()
        {
            var brandList = new List<object>();

            foreach (var brand in db.AssetProductBrands.Where(d => d.Status == true))
            {
                brandList.Add(new { Text = brand.BrandName, Value = brand.ID });
            }
            return brandList;
        }
        public List<object> GetModelByBrand()
        {
            var modelList = new List<object>();

            foreach (var model in db.AssetProductModels.Where(d => d.Status == true))
            {
                modelList.Add(new { Text = model.ModelName, Value = model.ID });
            }
            return modelList;
        }
        
        public List<object> GetAccessoriesBrandList()
        {
            var brandList = new List<object>();

            foreach (var brand in db.AssetProductBrands.Where(d => d.IsAccessories == 1))
            {
                brandList.Add(new { Text = brand.BrandName, Value = brand.ID });
            }
            return brandList;
        }

        public List<object> GetAssetVendorList()
        {
            var vendorList = new List<object>();

            foreach (var vendor in db.AssetVendorInfo.Where(d => d.Status == true))
            {
                vendorList.Add(new { Text = vendor.VendorName, Value = vendor.ID });
            }
            return vendorList;
        }
        public List<object> GetAssetLocationList()
        {
            var locationList = new List<object>();

            foreach (var location in db.AssetLocations.Where(d => d.Status == true))
            {
                locationList.Add(new { Text = location.ProductLocation, Value = location.ID });
            }
            return locationList;
        }

        public string GetAssetSerialNo(int productID)
        {

            var maxidfromdb = "";
            var productAbbrivation = "";
            try
            {

                maxidfromdb = db.AssetProductInfo.Max(s => s.Asset_Serial_No).ToString();

            }
            catch (Exception)
            {
                //
            }
            if (maxidfromdb == "")
            {
                return "Asset-0001";
            }
            else
            {
                var maxnumber = maxidfromdb.Substring(6, 4);
                Int32 maxnumberint = Convert.ToInt32(maxnumber);
                if (maxnumberint < 9)
                {
                    maxnumberint++;
                    return "Asset-" + "000" + maxnumberint;
                }
                else if (maxnumberint < 99)
                {
                    maxnumberint++;
                    return "Asset-" + "00" + maxnumberint;
                }
                else if (maxnumberint < 999)
                {
                    maxnumberint++;
                    return "Asset-" + "0" + maxnumberint;
                }
                else
                {
                    maxnumberint++;
                    return "Asset-" + maxnumberint;
                }
            }
        }

        public List<object> GetDepartmentListForAssetReturn(int id)
        {
            var departmentList = new List<object>();
            var lstOfdepartment = (from a in db.AssetProductInfo
                                   join p in db.AssetAssign on a.ID equals p.AssetProductInfoId
                                   join d in db.Departments on a.Dept_Id equals d.ID

                                   where a.ID == id
                                   select new VM_AssetProductInfo
                                   {
                                       Dept_Id = p.Dept_Id,
                                       DeptName = d.DeptName,
                                   }).ToList();

            foreach (var dept in lstOfdepartment)
            {
                departmentList.Add(new { Text = dept.DeptName, Value = dept.Dept_Id });
            }
            return departmentList;
        }

        public List<object> GetAssetProductListForAssign()
        {
            var productList = new List<object>();

            var availableAsset = (from a in db.AssetProductInfo
                                  join p in db.AssetProducts on a.ProductId equals p.ID
                                  where a.IsAssigned != 1
                                  select new VM_AssetProductInfo
                                  {
                                      ID = p.ID,
                                      ProductId = a.ProductId,
                                      ProductName = p.ProductName

                                  }).ToList();

            var assetproductList = availableAsset.GroupBy(p => p.ProductId)
                   .Select(grp => grp.First())
                   .ToList();

            foreach (var product in assetproductList)
            {
                productList.Add(new { Text = product.ProductName, Value = product.ID });
            }
            return productList;
        }

        public List<object> GetAssetSlNoForAssign(int productId, int brand, int model)
        {
            var assetSerialNoList = new List<object>();
            foreach (var asset in db.AssetProductInfo.Where(a => a.ProductId == productId && a.BrandId == brand && a.ModelId == model))
            {
                assetSerialNoList.Add(new { Text = asset.Asset_Serial_No, Value = asset.ID });
            }
            return assetSerialNoList;
        }

        #endregion

    }
}