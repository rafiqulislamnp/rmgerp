﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;

namespace Oss.Romo.Models.AssetManagement
{
    public class Asset_ReturnInfo : RootModel
    {
        //public string Asset_Serial_No { get; set; }
        //public int ProductId { get; set; }
        //[ForeignKey("ProductId")]
        //public Asset_Product Product { get; set; }
        //public int BrandId { get; set; }
        //[ForeignKey("BrandId")]
        //public Asset_ProductBrand Brand { get; set; }
        //public int ModelId { get; set; }
        //[ForeignKey("ModelId")]
        //public Asset_ProductModel Model { get; set; }
        //[DisplayName("BusinessUnit")]
        //public int BU_Id { get; set; }
        //[ForeignKey("BU_Id")]
        //public BusinessUnit BusinessUnit { get; set; }
        //public int Dept_Id { get; set; }
        //[ForeignKey("Dept_Id")]
        //public Department Department { get; set; }
        //public string DepartmentName { get; set; }

        //public int EmployeeId { get; set; }
        //[ForeignKey("EmployeeId")]
        //public HRMS_Employee Employee { get; set; }
        //public int Designation_Id { get; set; }
        //[ForeignKey("Designation_Id")]
        //public Designation Designation { get; set; }
        //public int Location_Id { get; set; }
        //[ForeignKey("Location_Id")]
        //public Asset_Location Location { get; set; }
        //public string ToFromDeptName { get; set; }
        //public string HandoverDate { get; set; }
        //public string AssignDate { get; set; }
        //public string Price { get; set; }
        //public string AssetConfiguration { get; set; }
        //public string ReturnDate { get; set; }
        //public string Remarks { get; set; }
        //public string ActionButtonsHtmlAdmin { get; set; }
        //public string ActionButtonsHtmlUser { get; set; }


        //public List<Asset_ReturnInfo> GetProductInfoList(int Bunit, int Department, int Empl)
        //{
        //    string resp = "";
        //    List<Asset_ReturnInfo> list = new List<Asset_ReturnInfo>();
        //    string query = string.Format("select ID, ProductId, BrandId, ModelId, Asset_Serial_No from Asset_Product_Assign Where [BU_Id] = '" + Bunit + "' and [Dept_Id] = '" + Department + "' and [EmployeeId] = '" + Empl + "' and [IsReturned] != 1 ");

        //    var ds = SqlDataAccess.SQL_ExecuteReader(query, out resp);
        //    UrlHelper url = new UrlHelper(HttpContext.Current.Request.RequestContext);
        //    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //        {
        //            var productInfoList = new Asset_ReturnInfo
        //            {
        //                ID = Convert.ToInt32(ds.Tables[0].Rows[i]["ID"]),
        //                ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]),
        //                BrandId = Convert.ToInt32(ds.Tables[0].Rows[i]["BrandId"]),
        //                ModelId = Convert.ToInt32(ds.Tables[0].Rows[i]["BrandId"]),
        //                Asset_Serial_No = ds.Tables[0].Rows[i]["Ast_Product_SL_No"].ToString(),
        //                ActionButtonsHtmlUser = "<a href=\"" + url.Action("AssetReturnsToAnother", "AssetManagement", new { id = ds.Tables[0].Rows[i]["ProductId"].ToString() }) + "\" id=\"btnastrtn" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "\" class=\"btn btn-primary btn-sm no-print\" data-tooltip=\"tooltip\" title=\"Return\" data-placement=\"top\" >Return</a>"
        //                //"<a href="@Url.Action("AssetReturnsToAnother", "AssetManagement", new RouteValueDictionary(new { id = ds.Tables[0].Rows[i]["SL"].ToString()}))" class="btn btn-link btn-cstm btn-sm">Return</a>"

        //            };
        //            list.Add(productInfoList);
        //        }
        //        return list;
        //    }
        //    return list;
        //}

    }
    public class Asset_DisposalInfo : RootModel
    {


    }
    public class Asset_Returned_History : RootModel
    {
        [DisplayName("Asset_Tag_No")]
        public string Asset_Serial_No { get; set; }
        public DateTime Return_Date { get; set; }
        [DisplayName("Product")]
        public int ProductId { get; set; }

        [ForeignKey("ProductId")]
        public Asset_Product Product { get; set; }


        [DisplayName("Product")]
        public string ProductName { get; set; }
        public string Product_SL_No { get; set; }

        [DisplayName("Brand")]
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }

        [DisplayName("Model")]
        public int ModelId { get; set; }

        [ForeignKey("ModelId")]
        public Asset_ProductModel Model { get; set; }
        [DisplayName("Business Unit")]
        public int BU_Id { get; set; } //Business Unit Id as Foreign Key

        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }

        [DisplayName("Department")]
        public int Dept_Id { get; set; }

        [ForeignKey("Dept_Id")]
        public HRMS_Department Department { get; set; }
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        [DisplayName("Employee Name")]
        public HRMS_Employee Employee { get; set; }
    }


}