﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.AssetManagement
{
    public class Asset_CompanyInfo : RootModel
    {
        public int SL { get; set; }
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        public int CurrencySl { get; set; }
        public DateTime FinancialYear { get; set; }
        public string Address { get; set; }
        public int IsDeleted { get; set; }
    }

    //public class Asset_BusinessUnit : RootModel
    //{
    //    public int Unit_ID { get; set; }
    //    public string UnitName { get; set; }
    //    public int IsDeleted { get; set; }

    //}

    //public class Asset_Department : RootModel
    //{
    //    [DisplayName("Department Name")]
    //    [Required(ErrorMessage = "Please Enter Department")]
    //    [Index("UX_Dept", 1, IsUnique = true)]
    //    [Column(TypeName = "VARCHAR")]
    //    [StringLength(50)]
    //    public string DeptName { get; set; }
    //    //  [Required(ErrorMessage = "Please Enter Department In Bangla")]
    //    public string DeptNameBangla { get; set; }

    //    //[ForeignKey("UnilSl")]
    //    public int Unit_ID { get; set; }
    //    [ForeignKey("Unit_ID")]
    //    public Unit Unit { get; set; }
    //    //public ICollection<Section> Sections { get; set; }
    //    //public ICollection<HRMS_Employee> Employees { get; set; }
    //}

    //public class Asset_Employee : RootModel
    //{
    //    [Index("Ux_Employee", 1, IsUnique = true)]
    //    [Column(TypeName = "NVARCHAR")]
    //    [StringLength(50)]
    //    public string EmployeeIdentity { get; set; }
    //    public string Name { get; set; }
    //    public string Name_BN { get; set; }
    //    public string Father_Name { get; set; }
    //    public string Mother_Name { get; set; }
    //    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
    //    public DateTime DOB { get; set; }
    //    public string Gender { get; set; }
    //    //[ForeignKey("Country_Id")]
    //    public int Country_Id { get; set; }

    //    [ForeignKey("Country_Id")]
    //    public Country Country { get; set; }
    //    // public ICollection<Country> Country { get; set; }
    //    //[ForeignKey("Id")]
    //    public int Designation_Id { get; set; }
    //    [ForeignKey("Designation_Id")]
    //    public Designation Designation { get; set; }
    //    //        public ICollection<Designation> Designations { get; set; }
    //    // [ForeignKey("Id")]
    //    public int Unit_Id { get; set; }
    //    [ForeignKey("Unit_Id")]
    //    public Unit Unit { get; set; }
    //    //       public ICollection<Unit> Units { get; set; }
    //    // [ForeignKey("Id")] 
    //    public int Department_Id { get; set; }
    //    [ForeignKey("Department_Id")]
    //    public Department Department { get; set; }
    //    //        public ICollection<Department> Departments { get; set; }
    //    //[ForeignKey("Id")]
    //    public int Section_Id { get; set; }
    //    [ForeignKey("Section_Id")]
    //    public Section Section { get; set; }
    //    //public ICollection<Section> Sections { get; set; }
    //    //[ForeignKey("Id")]
    //    public int Business_Unit_Id { get; set; }
    //    [ForeignKey("Business_Unit_Id")]
    //    public BusinessUnit BusinessUnit { get; set; }
    //    // 
    //    public string Blood_Group { get; set; }
    //    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
    //    public DateTime Joining_Date { get; set; }
    //    public string Religion { get; set; }
    //    public string Mobile_No { get; set; }
    //    public string Alt_Mobile_No { get; set; }
    //    public string Alt_Person_Name { get; set; }
    //    public string Alt_Person_Contact_No { get; set; }
    //    public string Alt_Person_Address { get; set; }
    //    public string Emergency_Contact_No { get; set; }
    //    public string Office_Contact { get; set; }
    //    public string Land_Phone { get; set; }
    //    [DataType(DataType.EmailAddress)]
    //    public string Email { get; set; }
    //    public string Employement_Type { get; set; }

    //    [Index("Ux_NID", 2, IsUnique = true)]
    //    [Column(TypeName = "NVARCHAR")]
    //    [StringLength(50)]
    //    public string NID_No { get; set; }
    //    //[Index("Ux_Passport",3,IsUnique = true)]
    //    //[Column(TypeName = "NVARCHAR")]
    //    [StringLength(50)]
    //    public string Passport_No { get; set; }
    //    public string Present_Address { get; set; }
    //    public string Permanent_Address { get; set; }
    //    public string Marital_Status { get; set; }
    //    public string Photo { get; set; }
    //    public string ImagePath { get; set; }
    //    public int Present_Status { get; set; }
    //    public string Staff_Type { get; set; }
    //    public string Overtime_Eligibility { get; set; }
    //    public string Grade { get; set; }
    //    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
    //    public DateTime QuitDate { get; set; }

    //}
    public class Asset_ProductLocation
    {
        public int SL { get; set; }
        public string Product_Location { get; set; }
        public int IsDeleted { get; set; }
    }


}