﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.AssetManagement
{
    public class Asset_Requisition:RootModel
    {       
            public string Requisition_id { get; set; }
            public string Requisition_purpose { get; set; }
            public string Requisition_by { get; set; }
            public string Requisition_For { get; set; }
            public string Requisition_For_Sl { get; set; }
            public string BusinessUnitSl { get; set; }
            public string BusinessUnitName { get; set; }
            public string DepartmentName { get; set; }
            public string DepartmentSl { get; set; }
            public string Requested_Department { get; set; }
            public DateTime Requisition_date { get; set; }
            public DateTime Required_delivery_date { get; set; }
            public string TL_Approved_By { get; set; }
            public string Remarks { get; set; }
            public string IsDeleted { get; set; }
            public string IsApproved { get; set; }
            public int IsPending { get; set; }
        
    }

    public class Asset_Requisition_Details:RootModel
    {
        public int SL { get; set; }
        public string Requisition_id { get; set; }
        public string Product_type { get; set; }
        public string Product_Code { get; set; }
        public int Product_Code_SL { get; set; }
        public string Brand { get; set; }
        public int Brand_SL { get; set; }
        public string Model { get; set; }
        public int Model_SL { get; set; }
        public string Claim_Departmrnt { get; set; }
        public double Claim_Quantity { get; set; }
        public string ProductName { get; set; }
        public int Ast_Item_Model_SL { get; set; }
        public string AssetProductName { get; set; }
        public string Unit { get; set; }
        public int Unit_Id { get; set; }
        public string Description { get; set; }
        public double Request_quantity { get; set; }
        public string Require_purpose { get; set; }
        public string Requisition_by { get; set; }
        public DateTime Requisition_date { get; set; }
        public DateTime Need_Date_From { get; set; }
        public DateTime Need_Date_To { get; set; }
        public string Project_name { get; set; }
        public string Project_ID { get; set; }
        public DateTime Required_delivery_date { get; set; }
        public string TL_Approved_by { get; set; }
        public double TL_Approved_quantity { get; set; }
        public double ST_ToBeDelivery_quantity { get; set; }
        public double ST_Delivered_quantity { get; set; }
        public double ST_Pending_quantity { get; set; }
        public DateTime TL_Approved_Date_From { get; set; }
        public DateTime TL_Approved_Date_To { get; set; }
        public DateTime TL_Submitted_Date { get; set; }
        public string Approved_by { get; set; }
        public double Approved_quantity { get; set; }
        public DateTime Approved_Date_From { get; set; }
        public DateTime Approved_Date_To { get; set; }
        public DateTime Finally_Approved_Date { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string IsDeleted { get; set; }
        public string IsApproved { get; set; }
        public int IsPending { get; set; }
        public string ReqPurpose { get; set; }
        public string ReqPurposeType { get; set; }
        public string LName { get; set; }
        public string LAddress { get; set; }
        public int LGrnSL { get; set; }
        public string LGrnId { get; set; }
        public int LGrnDetailsSL { get; set; }
        public string ProductCodeChar { get; set; }
    }

}