﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.AssetManagement
{
    public class HRMS_Employee : RootModel
    {
        [Index("Ux_Employee",1,IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string EmployeeIdentity { get; set; }
        public string Name { get; set; }
        public string Name_BN { get; set; }
        public string Father_Name { get; set; }
        public string Mother_Name { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        //[ForeignKey("Country_Id")]
        public int Country_Id { get; set; }

        [ForeignKey("Country_Id")]
        public Country Country { get; set; }
        // public ICollection<Country> Countries { get; set; }
        //[ForeignKey("Id")]
        public int Designation_Id { get; set; }
        [ForeignKey("Designation_Id")]
        public Designation Designation { get; set; }
//        public ICollection<Designation> Designations { get; set; }
       // [ForeignKey("Id")]
        public int Unit_Id { get; set; }
        [ForeignKey("Unit_Id")]
        public Unit Unit { get; set; }
 //       public ICollection<Unit> Units { get; set; }
       // [ForeignKey("Id")] 
        public int Department_Id { get; set; }
        [ForeignKey("Department_Id")]
        public Department Department { get; set; }
        //        public ICollection<Department> Departments { get; set; }
        //[ForeignKey("Section_Id")]
        public int Section_IdX { get; set; }
        public Section Section { get; set; }
        //public ICollection<Section> Sections { get; set; }
       //[ForeignKey("Id")]
        public int Business_Unit_Id { get; set; }
        [ForeignKey("Business_Unit_Id")]
        public BusinessUnit BusinessUnit { get; set; }
        // 
        public string Blood_Group { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Joining_Date { get; set; }
        public string Religion { get; set; }
        public string Mobile_No { get; set; }
        public string Alt_Mobile_No { get; set; }
        public string Alt_Person_Name { get; set; }
        public string Alt_Person_Contact_No { get; set; }
        public string Alt_Person_Address { get; set; }
        public string Emergency_Contact_No { get; set; }
        public string Office_Contact { get; set; }
        public string Land_Phone { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string Employement_Type { get; set; }

        [Index("Ux_NID",2,IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string NID_No { get; set; }
        //[Index("Ux_Passport",3,IsUnique = true)]
        //[Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Passport_No { get; set; }
        public string Present_Address { get; set; }
        public string Permanent_Address { get; set; }
        public string Marital_Status { get; set; }
        public string Photo { get; set; }
        public string ImagePath { get; set; }
        public int Present_Status { get; set; }
        public string Staff_Type { get; set; }
        public string Overtime_Eligibility { get; set; }
        public string Grade { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime QuitDate { get; set; }
    }

    public class HRMS_Spouse : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }
        public string SpouseName { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime SpouseDOB { get; set; }
        public string Spouse_Contact_No { get; set; }
        public string Spouse_Blood_Group { get; set; }
    }

    public class HRMS_Child:RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public string Child_Name { get; set; }
        public string Child_Blood_Group { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Child_DOB { get; set; }
    }

    public class HRMS_Education : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public string Degree { get; set; }
        public string Institute { get; set; }
        public string GroupOrSubject { get; set; }
        public string Passing_Year { get; set; }
        public string Result { get; set; }
    }

    public class HRMS_Skill :RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public string Skill_Type { get; set; }
        public string Skill_Name { get; set; }
        public string Skill_Description { get; set; }
    }

    public class HRMS_Training : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public string Course_Name { get; set; }
        public string Institute { get; set; }
        public string Country { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Course_Start_Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Course_End_Date { get; set; }
        public string Course_Duration { get; set; }
    }

    public class HRMS_Reference : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public string Referee_Name { get; set; }
        public string Contact_No { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email_Id { get; set; }
        public string Address { get; set; }
        public string Relation { get; set; }
    }

    public class HRMS_Salary : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public char Salary_Grade { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Joining_Date { get; set; }
        public decimal Joining_Salary { get; set; }
        public string Payment_Mode { get; set; }
        public string Bank_Name { get; set; }
        [Index("Ux_Salary_Account",IsUnique = true)]
        [StringLength(60)]
        [Column(TypeName = "NVARCHAR")]
        public string Account_No { get; set; }
        public string Current_Salary { get; set; }
    }

    public class HRMS_Salary_History: RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime JoiningDate { get; set; }
        public char Salary_Grade { get; set; }
        public decimal Joining_Salary { get; set; }
        public string Payment_Mode { get; set; }
        public string Bank_Name { get; set; }
        public string Account_No { get; set; }
        public decimal Current_Salary { get; set; }
        public string Action_Name { get; set; }
    }

    public class HRMS_Experience : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public string Company_Name { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Responsibility { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Job_Start_Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Job_End_Date { get; set; }
    }

    public class HRMS_Facility : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public string Item_Name { get; set; }
        public decimal Cost_Per_Month { get; set; }
    }

    public class HRMS_Facility_History : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public string Item_Name { get; set; }
        public decimal Cost_Per_Month { get; set; }
        public string Action_Name { get; set; }
    }

    public class HRMS_Hobby : RootModel
    {
        public int Employee_Id { get; set; }
        [ForeignKey("Employee_Id")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public string Hobby_Name { get; set; }
        public string Remarks { get; set; }
    }

    /// <summary>
    /// Linking Class-----to Employee
    /// </summary>
    public class HRMS_LineManagerToEmployeeLink : RootModel
    {
        public int LineManagerId { get; set; }
        public int EmployeeId { get; set; }
    }

    public class HRMS_AdminToEmployeeLink : RootModel
    {
        public int AdminId { get; set; }
        public int EmployeeId { get; set; }
    }
    public class HRMS_DepartmentHeadToEmployeeLink : RootModel
    {
        public int DeptHeadId { get; set; }
        public int EmployeeId { get; set; }
    }
}