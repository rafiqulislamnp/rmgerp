﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;

namespace Oss.Romo.Models.AssetManagement
{
    public class Asset_ProductAssign : RootModel
    {
        [DisplayName("Asset Tag No")]

        public string Asset_Serial_No { get; set; }
        public int ProductId { get; set; }

        public int AssetProductInfoId { get; set; }

        [ForeignKey("ProductId")]
        public Asset_Product Product { get; set; }
        public string ProductName { get; set; }
        [DisplayName("Brand")]
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }
        [DisplayName("Model")]
        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        public Asset_ProductModel Model { get; set; }
        public string Product_SL_No { get; set; }
        [DisplayName("BusinessUnit")]
        public int BU_Id { get; set; }
        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }
        [DisplayName("Department")]
        public int Dept_Id { get; set; }
        [ForeignKey("Dept_Id")]
        public HRMS_Department Department { get; set; }
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee Employee { get; set; }
        public string Designation { get; set; }
        public int Location_Id { get; set; }
        [ForeignKey("Location_Id")]
        public Asset_Location Location { get; set; }
        public string ToFromDeptName { get; set; }
        //[DisplayName("Vendor")]
        //public int VendorId { get; set; }
        //[ForeignKey("VendorId")]
        //public Asset_VendorInfo Vendor { get; set; }
        public decimal Price { get; set; }
        public string PurchaseDate { get; set; }
        public double AvailableQty { get; set; }
        public string HandoverDate { get; set; }
        public string AssignDate { get; set; }
        public int IsAccessories { get; set; }
        public int IsAssigned { get; set; }
        public int IsReturned { get; set; }
        public string Remarks { get; set; }

    }

    public class Asset_ProductAssign_History : RootModel
    {
        [DisplayName("Asset Tag No")]
        public string Asset_Serial_No { get; set; }
        public int AssetProductInfoId { get; set; }
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Asset_Product Product { get; set; }
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }
        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        public Asset_ProductModel Model { get; set; }
        [DisplayName("BusinessUnit")]
        public int BU_Id { get; set; }
        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }
        public int Dept_Id { get; set; }
        [ForeignKey("Dept_Id")]
        public HRMS_Department Department { get; set; }
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee Employee { get; set; }
        public int Designation_Id { get; set; }
        [ForeignKey("Designation_Id")]
        public HRMS_Designation Designation { get; set; }
        public int Location_Id { get; set; }
        [ForeignKey("Location_Id")]
        public Asset_Location Location { get; set; }
        //public int VendorId { get; set; }
        //[ForeignKey("VendorId")]
        //public Asset_VendorInfo Vendor { get; set; }
        public string AssetAssignDate { get; set; }
        public string AssetReturnDate { get; set; }
        public int IsAssigned { get; set; }
        public int IsReturned { get; set; }
        public int IsDamaged { get; set; }
        public string AssetAction { get; set; }
        public int IsAccessories { get; set; }
        public decimal AccessoriesQty { get; set; }
        public string ToFromDeptName { get; set; }
        public string Remarks { get; set; }

    }

}


