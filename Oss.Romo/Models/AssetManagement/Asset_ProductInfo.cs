﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Entity.Common;

namespace Oss.Romo.Models.AssetManagement
{
    public class Asset_Product : RootModel
    {
        [Required(ErrorMessage = "Please Enter ProductName")]
        [Index("UX_ProductName", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string ProductName { get; set; }
        [DisplayName("Category")]
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Asset_Category Category { get; set; }

        [DisplayName("Product Abbreviation")]
        public string ProductAbbreviation { get; set; }
        public int IsAccessories { get; set; }
        public string IsDeleted { get; set; }
    }

    public class Asset_ProductBrand : RootModel
    {
        [DisplayName("Brand")]
        [Required(ErrorMessage = "Please Enter BrandName")]
        //[Index("UX_BrandName", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string BrandName { get; set; }
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Asset_Product Product { get; set; }
        public string Remark { get; set; }
        public int IsAccessories { get; set; }
        public string IsDeleted { get; set; }
    }

    public class Asset_ProductModel : RootModel
    {
        [DisplayName("Model")]
        [Required(ErrorMessage = "Please Enter ModelName")]
        //[Index("UX_ModelName", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string ModelName { get; set; }
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }
        public int IsAccessories { get; set; }
        public string IsDeleted { get; set; }
    }

    public class Asset_VendorInfo : RootModel
    {
        [DisplayName("Vendor Name")]
        [Required(ErrorMessage = "Please Select Vendor Name")]
        [Index("UX_VendorName", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string VendorName { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        [DisplayName("Cont. Person Name")]
        public string ContactPersonName { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        [DisplayName("Alt. Phone")]
        public string AlternatePhone { get; set; }
        public int IsDeleted { get; set; }
    }

    public class Asset_Location : RootModel
    {
        [DisplayName("Product Location")]
        [Required(ErrorMessage = "Please Select Location Name")]
        [Index("UX_ProductLocation", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string ProductLocation { get; set; }
        public int IsAccessories { get; set; }
        public string Block { get; set; }
        public int IsDeleted { get; set; }
    }

    //public class Asset_ProductInfo : RootModel
    //{
    //    [Required(ErrorMessage = "Please select Asset_Tag_No")]
    //    [Index("UX_Asset_Serial_No", 1, IsUnique = true)]
    //    [Column(TypeName = "VARCHAR")]
    //    [StringLength(100)]
    //    [DisplayName("Asset_Tag_No")]
    //    public string Asset_Serial_No { get; set; }

    //    public int ProductId { get; set; }
    //    [ForeignKey("ProductId")]
    //    public Asset_Product Product { get; set; }

    //    public string ProductName { get; set; }
    //    public string Product_SL_No { get; set; }

    //    [DisplayName("Brand")]
    //    public int BrandId { get; set; }
    //    [ForeignKey("BrandId")]
    //    public Asset_ProductBrand Brand { get; set; }

    //    [DisplayName("Model")]
    //    public int ModelId { get; set; }
    //    [ForeignKey("ModelId")]
    //    public Asset_ProductModel Model { get; set; }

    //    [DisplayName("Vendor")]
    //    public int VendorId { get; set; }
    //    [ForeignKey("VendorId")]
    //    public Asset_VendorInfo Vendor { get; set; }

    //    public int CategoryId { get; set; }
    //    [ForeignKey("CategoryId")]
    //    public Asset_Category Category { get; set; }


    //    [DisplayName("SubCategory")]
    //    public int SubCategoryId { get; set; }
    //    [ForeignKey("SubCategoryId")]
    //    public Asset_SubCategory SubCategory { get; set; }

    //    public int IsWarranty { get; set; }
    //    public DateTime? WarrantryDate { get; set; }
    //    public string AssetConfiguration { get; set; }
    //    public int IsAssigned { get; set; }

    //    [DisplayName("BusinessUnit")]
    //    public int BU_Id { get; set; }   //Business Unit Id as Foreign Key
    //    [ForeignKey("BU_Id")]
    //    public BusinessUnit BusinessUnit { get; set; }

    //    [DisplayName("Department")]
    //    public int Dept_Id { get; set; }
    //    [ForeignKey("Dept_Id")]
    //    public Department Department { get; set; }
    //    //public int EmployeeId { get; set; }
    //    //[ForeignKey("EmployeeId")]
    //    //public HRMS_Employee Employee { get; set; }
    //    public int Location_Id { get; set; }
    //    [ForeignKey("Location_Id")]
    //    public Asset_Location Location { get; set; }

    //    public string MasureUnit { get; set; }
    //    public DateTime? PurchaseDate { get; set; }
    //    public DateTime? ExpiredDate { get; set; }
    //    public decimal Price { get; set; }
    //    public int PurchaseQuantity { get; set; }
    //    public int Deliver { get; set; }
    //    public int IsHeadOffice { get; set; }
    //    public int Stock { get; set; }
    //    public int IsDeleted { get; set; }
    //    public int IsDamaged { get; set; }
    //    public int IsDisposed { get; set; }
    //    public string Remarks { get; set; }
    //    public int IsAccessories { get; set; }
    //    [DisplayName("Life Time")]
    //    public int LifeTime { get; set; }
    //}

    public class Asset_ProductInfo : RootModel
    {
        [Required(ErrorMessage = "Please select Asset_Tag_No")]
        [Index("UX_Asset_Serial_No", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        [DisplayName("Asset_Tag_No")]
        public string Asset_Serial_No { get; set; }
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Asset_Product Product { get; set; }
        public string ProductName { get; set; }
        public string Product_SL_No { get; set; }
        [DisplayName("Brand")]
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }
        [DisplayName("Model")]
        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        public Asset_ProductModel Model { get; set; }
        public string MasureUnit { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public decimal Price { get; set; }
        [DisplayName("Vendor")]
        public int VendorId { get; set; }
        [ForeignKey("VendorId")]
        public Asset_VendorInfo Vendor { get; set; }
        public int IsWarranty { get; set; }
        public DateTime? WarrantryDate { get; set; }
        public string AssetConfiguration { get; set; }
        public int IsAssigned { get; set; }

        [DisplayName("BusinessUnit")]
        public int BU_Id { get; set; }   //Business Unit Id as Foreign Key
        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }

        [DisplayName("Life Time")]
        public int LifeTime { get; set; }
        [DisplayName("Department")]
        public int Dept_Id { get; set; }
        [ForeignKey("Dept_Id")]
        public HRMS_Department Department { get; set; }
        //public int EmployeeId { get; set; }
        //[ForeignKey("EmployeeId")]
        //public HRMS_Employee Employee { get; set; }
        public int Location_Id { get; set; }
        [ForeignKey("Location_Id")]
        public Asset_Location Location { get; set; }
        public int PurchaseQuantity { get; set; }
        public int Deliver { get; set; }
        public int IsHeadOffice { get; set; }
        public int Stock { get; set; }
        public int IsDeleted { get; set; }
        public int IsDamaged { get; set; }
        public int IsDisposed { get; set; }
        public string Remarks { get; set; }
        public int IsAccessories { get; set; }
    }


    public class Asset_DepreciationHistory : RootModel
    {
        public int ProductIdFK { get; set; }
        public int Year { get; set; }
        public int DepreciationPercent { get; set; }
    }


    public class Asset_Category : RootModel
    {
        [DisplayName("Category")]
        [Required(ErrorMessage = "Please select Category Name")]
        [Index("UX_CategoryName", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string CategoryName { get; set; }

    }

    public class Asset_SubCategory : RootModel
    {
        [DisplayName("Sub Category")]
        [Required(ErrorMessage = "Please Select Sub Category Name")]
        [Index("UX_SubCategoryName", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string SubCategoryName { get; set; }

        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Asset_Category Category { get; set; }
    }

}