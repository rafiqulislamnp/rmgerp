﻿using Oss.Romo.Models.Entity.HRMS;
using Oss.Romo.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Entity.Compliance
{
    public class HRMHistoryEOD : RootModel
    {
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }
        public int Eod_RefFk { get; set; }
        [ForeignKey("Eod_RefFk")]
        public HRMS_EodReference HrmsEodReference { get; set; }
        public decimal ActualAmount { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}