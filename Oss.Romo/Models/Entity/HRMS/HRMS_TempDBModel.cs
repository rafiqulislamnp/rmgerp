﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Services;

namespace Oss.Romo.Models.Entity.HRMS
{
    public class HRMS_TempDBModel
    {
    }

    [Table("RMS")]
    public class RMS
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(50)]
        public string d_card { get; set; }
        [StringLength(50)]
        public string t_card { get; set; }
        [StringLength(50)]
        public string card_no { get; set; }
    }


    [Table("TAttendance")]
    public class TAttendance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int EmpId { get; set; }
        [StringLength(50)]
        public string EmployeeID { get; set; }
        [StringLength(100)]
        public string EmployeeName { get; set; }
        public DateTime AttDay { get; set; }
        [StringLength(50)]
        public string InOutStatus { get; set; }

        [StringLength(50)]
        public string Att_Type { get; set; }
    }

    [Table("TblAgeWiseEmployeeList")]
    public class TblAgeWiseEmployeeList
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int SL { get; set; }
        [StringLength(50)]
        public string EmployeeID { get; set; }
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Designation { get; set; }
        [StringLength(100)]
        public string SectionName { get; set; }

        public DateTime Joining_Date { get; set; }
        public Decimal GrossSalary { get; set; }
        public DateTime DOB { get; set; }
        
        public int Year { get; set; }
        public int Month { get; set; }
        public int Days { get; set; }
    }


    [Table("Temp_Attendance2")]
    public class Temp_Attendance2
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int EmpId { get; set; }
        [StringLength(50)]
        public string EmployeeID { get; set; }
        [StringLength(100)]
        public string EmployeeName { get; set; }
        public DateTime AttDay { get; set; }
        public DateTime InTime { get; set; }
        public DateTime OutTime { get; set; }
        public DateTime AcInTime { get; set; }
        public DateTime AcOutTime { get; set; }
        public float TotalTime { get; set; }
        [StringLength(50)]
        public string Att_Type { get; set; }
        public int ShiftTypeId { get; set; }
    }

    [Table("Temp_Attendance3")]
    public class Temp_Attendance3
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int EmpId { get; set; }
        [StringLength(50)]
        public string EmployeeID { get; set; }
        [StringLength(100)]
        public string EmployeeName { get; set; }
        public DateTime AttDay { get; set; }
        public DateTime InTime { get; set; }
        public DateTime OutTime { get; set; }
        public float TotalTime { get; set; }
        public float OverTime { get; set; }
        public int ShiftTypeId { get; set; }
    }

    [Table("TempAtt")]
    public class TempAtt
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(50)]
        public string Unit { get; set; }
        [StringLength(50)]
        public string CardTitle { get; set; }
        public int EmployeeId { get; set; }
        [StringLength(250)]
        public string Name_CardNo { get; set; }
        [StringLength(250)]
        public string Designation_JoinDate { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan InTime { get; set; }
        public TimeSpan OutTime { get; set; }
        [StringLength(20)]
        public string AttendanceStatus { get; set; }
        public int OT { get; set; }
        [StringLength(250)]
        public string inoutTotalOT { get; set; }
        public int Working_Day { get; set; }
        public int Present { get; set; }
        public int Late { get; set; }
        public int Lunch_Leave { get; set; }
        public int CL_EL_ML { get; set; }
        public int Total_Leave { get; set; }
        public int FestivalLeave_SpecialLeave { get; set; }
        public int Holiday { get; set; }
        public int Absent { get; set; }
        public int Invalid { get; set; }
        public int TotalPresent { get; set; }
        public int GrandTotal { get; set; }
        public int TotalOT { get; set; }
    }

    //[Table("TempAtt1")]
    //public class TempAtt
    //{
    //    [Key]
    //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public int ID { get; set; }
    //    [StringLength(50)]
    //    public string Unit { get; set; }
    //    [StringLength(50)]
    //    public string CardTitle { get; set; }
    //    public int EmployeeId { get; set; }
    //    [StringLength(250)]
    //    public string Name_CardNo { get; set; }
    //    [StringLength(250)]
    //    public string Designation_JoinDate { get; set; }

    //    public int Working_Day { get; set; }
    //    public int Present { get; set; }
    //    public int Late { get; set; }
    //    public int Lunch_Leave { get; set; }
    //    public int CL_EL_ML { get; set; }
    //    public int Total_Leave { get; set; }
    //    public int FestivalLeave_SpecialLeave { get; set; }
    //    public int Holiday { get; set; }
    //    public int Absent { get; set; }
    //    public int Invalid { get; set; }
    //    public int TotalPresent { get; set; }
    //    public int GrandTotal { get; set; }
    //    public int TotalOT { get; set; }

    //    [StringLength(200)]
    //    public string [1] { get; set; }
    //}

    [Table("tempattedit")]
    public class tempattedit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int eid { get; set; }
        public DateTime InTime { get; set; }
        public DateTime OutTime { get; set; }
    }

    [Table("TempAttShifting")]
    public class TempAttShifting
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public DateTime InTime { get; set; }
        public DateTime OutTime { get; set; }
        public int TotalTime { get; set; }
        public int Late { get; set; }
        public int Early { get; set; }
        public int OverTime { get; set; }
        public int AttendanceStatus { get; set; }
        [StringLength(50)]
        public string CardNo { get; set; }
        [StringLength(100)]
        public string Remarks { get; set; }
        public int ShiftTypeId { get; set; }
        public int HolidayType { get; set; }
        public int LeavePaidType { get; set; }
        public int IsFreeze { get; set; }
        public bool Status { get; set; }
        public DateTime Entry_Date { get; set; }
        public DateTime Update_Date { get; set; }
        [StringLength(50)]
        public string Entry_By { get; set; }
        [StringLength(50)]
        public string Update_By { get; set; }

        public int OverTimeDeduction { get; set; }
        public int PayableOverTime { get; set; }
    }

    [Table("TEmployeeLeaveAssign")]
    public class TEmployeeLeaveAssign
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int EmployeeId { get; set; }
        public int LeavetypeId { get; set; }
        [StringLength(50)]
        public string Year { get; set; }
        public Decimal TotalLeave { get; set; }
    }

}