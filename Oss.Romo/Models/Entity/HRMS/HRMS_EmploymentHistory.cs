﻿using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Entity.HRMS
{
    public class HRMS_EmploymentHistory : RootModel
    {
        public int EmployeeIdFK { get; set; }
        [ForeignKey("EmployeeIdFK")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string CardTitle { get; set; }
        
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string CardNo { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string PunchCardNo { get; set; }

        public int DepartmentFK { get; set; }
        [ForeignKey("DepartmentFK")]
        public HRMS_Department Department { get; set; }

        public int SectionFK { get; set; }
        [ForeignKey("SectionFK")]
        public HRMS_Section Section { get; set; }

        public int DesignationFK { get; set; }
        [ForeignKey("DesignationFK")]
        public HRMS_Designation Designation { get; set; }
        
        public int UnitFK { get; set; }
        [ForeignKey("UnitFK")]
        public HRMS_Unit Unit { get; set; }
        
        public int LineFK { get; set; }
        [ForeignKey("LineFK")]
        public HRMS_LineNo LineNo { get; set; }
        
        public int BusinessUnitFK { get; set; }
        [ForeignKey("BusinessUnitFK")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }

        public int? EodRecordFK { get; set; }
        [ForeignKey("EodRecordFK")]
        public HRMS_EodRecord EodRecord { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(20)]
        public string Grade { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Staff_Type { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "DateTime")]
        public DateTime PromotionDate { get; set; }

        public int? HistoyType { get; set; }

        public bool Current { get; set; }

    }
}