﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Oss.Romo.Models.Entity.HRMS;
using Oss.Romo.Models.Services;

namespace Oss.Romo.Models.Entity.Common
{
    public class CommonSettings
    {

    }
    [Table("HRMS_LineNo")]
    public class HRMS_LineNo: RootModel
    {
        [Index("UX_LineNo", 1, IsUnique = true)]
        [StringLength(50)]
        public string Line { get; set; }
    }
    [Table("HRMS_Country")]
    public class HRMS_Country : RootModel
    {
        [Index("UX_Country", 1, IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }
    }
    //[Table("BusinessUnits")]
    public class HRMS_BusinessUnit : RootModel
    {
        [Index("UX_BU", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Name { get; set; }
        public int Country_ID { get; set; }
        [ForeignKey("Country_ID")]
        public HRMS_Country Country { get; set; }
    }

    [Table("HRMS_Unit")]
    public class HRMS_Unit : RootModel
    {
        [Index("UX_Unit", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string UnitName { get; set; }
        public int BusinessUnit_ID { get; set; }
        [ForeignKey("BusinessUnit_ID")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }
    }
    [Table("HRMS_Department")]
    public class HRMS_Department : RootModel
    {
         [Index("UX_Dept", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string DeptName { get; set; } 
        public string DeptNameBangla { get; set; }
         
        public int Unit_ID { get; set; }
        [ForeignKey("Unit_ID")]
        public HRMS_Unit Unit { get; set; }
    }
    [Table("HRMS_Section")]
    public class HRMS_Section : RootModel
    {
        [Index("UX_Section", 1, IsUnique = true)]
        [StringLength(100)]
        public string SectionName { get; set; }
        [StringLength(100)]
        public string SectionNameBangla { get; set; }

        public int Dept_ID { get; set; }
        [ForeignKey("Dept_ID")]
        public HRMS_Department Department { get; set; }
    }
    [Table("HRMS_NotificationUser")]
    public class HRMS_NotificationUser : RootModel
    {
        
        public string Employee_Id { get; set; }
        
        public string Name { get; set; }
        
        [Index("Ux_Notification", IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Subject { get; set; }
        [DisplayName("Business Unit")]
        public int BusinessUnit_ID { get; set; }
        [ForeignKey("BusinessUnit_ID")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }

    }

    [Table("HRMS_Designation")]
    public class HRMS_Designation : RootModel
    {
        [Index("UX_Designation", 1, IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }
        public string DesigNameBangla { get; set; }
    }
    [Table("SalaryGrades")]
    public class SalaryGrade : RootModel
    {
        [Index("UX_SalaryGrade", 1, IsUnique = true)]
        [StringLength(100)]
        public string Name { get; set; }

        public decimal Grade_Amount { get; set; }
    }

    [Table("HRMS_District")]
    public class HRMS_District : RootModel
    {
        [Index("UX_DistrictName", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string DistrictName { get; set; }

        public virtual ICollection<HRMS_Employee> PresentDistrict { get; set; }
        public virtual ICollection<HRMS_Employee> PermanentDistrict { get; set; }

    }
    [Table("HRMS_PoliceStation")]
    public class HRMS_PoliceStation : RootModel
    {
       // [Index("UX_PoliceStationName", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PoliceStationName { get; set; }

        public int District_ID { get; set; }
        [ForeignKey("District_ID")]
        public HRMS_District District { get; set; }


        public virtual ICollection<HRMS_Employee> PresentPoliceStation { get; set; }
        public virtual ICollection<HRMS_Employee> PermanentPoliceStation { get; set; }
    }
    [Table("HRMS_PostOffice")]
    public class HRMS_PostOffice : RootModel
    {
       // [Index("UX_PostOfficeName", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string PostOfficeName { get; set; }

        public int PoliceStation_ID { get; set; }
        [ForeignKey("PoliceStation_ID")]
        public HRMS_PoliceStation PoliceStation { get; set; }

    
        public virtual ICollection<HRMS_Employee> PresentPostOffice { get; set; }
        public virtual ICollection<HRMS_Employee> PermanentPostOffice { get; set; }
    }
    [Table("HRMS_Village")]
    public class HRMS_Village : RootModel
    {
       // [Index("UX_VillageName", 1, IsUnique = true)]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string VillageName { get; set; }

        public int PostOffice_ID { get; set; }
        [ForeignKey("PostOffice_ID")]
        public HRMS_PostOffice PostOffice { get; set; }

        public virtual ICollection<HRMS_Employee> PresentVillage { get; set; }
        public virtual ICollection<HRMS_Employee> PermanentVillage { get; set; }
    }
}