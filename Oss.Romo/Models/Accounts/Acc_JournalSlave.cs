﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_JournalSlave : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DefaultValue(0)]
        public decimal Debit { get; set; }
        [DefaultValue(0)]
        public decimal Credit { get; set; }
        [DefaultValue(1)]
        [DisplayName("Currency Rate")]
        public decimal CurrencyRate { get; set; }
        public int CurrencyType { get; set; }

        [DisplayName("A/C Head")]
        public int? Acc_AcNameFK { get; set; }

        [DisplayName("Acc_Journal")]
        public int? Acc_JournalFK { get; set; }
    }
    enum CurrencyTypeEnum
    {
        USD = 1,
        BDT = 2
    }
}