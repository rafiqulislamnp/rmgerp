﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_Chart2 : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Chart Two")]
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string Name { get; set; }
        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        public string Description { get; set; }
        [DisplayName("Chart One")]
        public int?  Acc_Chart1FK { get; set; }
        [DefaultValue(true)]
        public bool? Editable { get; set; }

        [StringLength(3, ErrorMessage = "Upto 3 Chracter")]
        public string Code { get; set; }



        //...........................New Added Code...................................//

        //public Acc_Chart1 Acc_Chart1 { get; set; }

        //public List<Acc_AcName> Acc_AcNames { get; set; }

    }
}