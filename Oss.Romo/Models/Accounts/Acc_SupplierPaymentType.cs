﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_SupplierPaymentType : BaseModel
    {
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }
        [StringLength(200, ErrorMessage = "Upto 200 Chracter")]
        public string Description { get; set; }
    }
}