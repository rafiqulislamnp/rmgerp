﻿using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_LCAlocation:BaseModel
    {
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }

        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessage = "Upto 100 Chracter")]
        public string Description { get; set; }

        [DefaultValue(0)]
        [DisplayName("B2B L/C Value")]
        public decimal? Value { get; set; }

        [DisplayName("PO ID")]
        public int? Mkt_POFK { get; set; }

        [DisplayName("Master L/C")]
        public int? Commercial_MasterLCFK { get; set; }



        //...........................New Added Code...................................//

        //public Mkt_PO Mkt_PO { get; set; }
        //public Commercial_MasterLC Commercial_MasterLC { get; set; }



    }
}