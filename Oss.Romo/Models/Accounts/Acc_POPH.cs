﻿using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_POPH : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }

        [DefaultValue(0)]
        [DisplayName("Document Paid")]
        public decimal? Amount { get; set; }

        [DisplayName("B2B L/C No")]
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        //[Required(ErrorMessage = "B2B L/C No. is Required")]
        public string LC_D { get; set; }

        [DisplayName("PO ID")]
        public int? Mkt_POFK { get; set; }

        [DisplayName("Is check")]
        public bool Ischeque { get; set; }

        //...........................New Added Code...................................//

        //public Mkt_PO Mkt_PO { get; set; }



    }
}