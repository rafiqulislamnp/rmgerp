﻿using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_Expenses_History: BaseModel
    {

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [DisplayName("To Account")]
        public int To_Acc_NameFK { get; set; }
        [DisplayName("From Account")]
        public int From_Acc_NameFK { get; set; }
        [DisplayName("Currency")]
        public int Common_CurrencyFK { get; set; }
        [MaxLength(100)]
        public string Referance { get; set; }
        [DefaultValue(0)]
        public decimal Amount { get; set; }
        [MaxLength(100)]
        public string Note { get; set; }
        [MaxLength(100)]
        public string Comment { get; set; }
        [MaxLength(30)]
        [DisplayName("Instrument No")]
        public string InstrumentNo { get; set; }      
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DefaultValue(false)]
        public bool IsIncome { get; set; }


        //...........................New Added Code...................................//

        //public Common_Currency Common_Currency { get; set; }
    }
}