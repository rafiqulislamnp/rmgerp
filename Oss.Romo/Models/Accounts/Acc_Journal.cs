﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_Journal:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [MaxLength(250, ErrorMessage = "Upto 250 Chracter")]
        public string Title { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description{ get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date{ get; set; }

        [DefaultValue(false)]
        public bool isFinal { get; set; }


        [MaxLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string JournalCId { get; set; }
        // [Index("Acc_Journal_TableIdFk_IsUnique", 1, IsUnique = true)]
        public int? TableIdFk { get; set; }
       // [Index("Acc_Journal_TableRowIdFk_IsUnique", 2, IsUnique = true)]
        public int? TableRowIdFk { get; set; }

        [DisplayName("Cost Center")]
        public int? Acc_CostPoolFK { get; set; }

        //...........................New Added Code...................................//

        //public List<Acc_JournalSlave> Acc_JournalSlaves { get; set; }
    }
}