﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_AcName : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }


        [DisplayName("A/C Head")]
        [StringLength(150, ErrorMessage = "Upto 150 Chracter")]
        public string Name { get; set; }

        //[Index("IX_Acc_AccNameCode", 1, IsUnique = true)]
        [DisplayName("Account Code")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Code { get; set; }

        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        public string Description { get; set; }

        [DisplayName("Chart Two")]
        public int? Acc_Chart2FK { get; set; }

        [DefaultValue(0)]
        [DisplayName("Current Balance")]
        public decimal CurrentBalance { get; set; }

        [DefaultValue(true)]
        public bool? Editable { get; set; }

        [DisplayName("Cost Center")]
        public int? Acc_CostPoolFK { get; set; }


        //...........................New Added Code...................................//

        //public Acc_Chart2 Acc_Chart2 { get; set; }

        //public List<Acc_JournalSlave> Acc_JournalSlaves { get; set; }

    }

}