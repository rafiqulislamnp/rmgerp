﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_Chart1:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Chart One")]
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string Name { get; set; }

        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        public string Description { get; set; }

       //private DateTime dateTime = DateTime.Now;
        //public DateTime MyProperty { get { return dateTime == DateTime.MinValue ? DateTime.Now : dateTime; } set { dateTime = value; } }

        [StringLength(3, ErrorMessage = "Upto 3 Chracter")]
        public string Code { get; set; }

        [DisplayName("Accounts Type")]
        public int? Acc_TypeFK { get; set; }
        [DefaultValue(true)]
        public bool? Editable { get; set; }

        //...........................New Added Code...................................//

        //public List<Acc_Chart2> Acc_Chart2s { get; set; }

    }
}