﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;


namespace Oss.Romo.Models.Accounts
{
    public class Acc_SupplierAdjustment : BaseModel
    {
        public int FirstCreatedBy { get; set; }
        [StringLength(20, ErrorMessage = "Remarks must be between 0 and 20 chracter")]
        public string CID { get; set; }

        public DateTime Date { get; set; }

        [DisplayName("Supplier")]
        [Required(ErrorMessage = "Supplier Name is required")]
        public int Common_SupplierFk { get; set; }

        [DisplayName("Ammount")]
        [Required(ErrorMessage = "Adjustment Ammount is required")]
        public decimal AdjustmentAmmount { get; set; }

        [StringLength(100, ErrorMessage = "Remarks must be between 0 and 100 chracter")]
        public string Remarks { get; set; }
    }
}