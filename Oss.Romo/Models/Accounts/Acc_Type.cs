﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_Type
    {
        [Key]
        public int ID { get; set; }
        [DisplayName("Account Type")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }
        public bool DrIncrease { get; set; }
        [StringLength(2, ErrorMessage = "Upto 2 Chracter")]
        public string Code { get; set; }


        public IEnumerable<Acc_Type> GetAcc_Type()
        {
            List<Acc_Type> accList = new List<Acc_Type>();
            Acc_Type ASSETS = new Acc_Type
            {
                ID = 1,
                Name = "ASSETS",
                DrIncrease = true,
                Code = "A"
            };
            Acc_Type LIABILITIES = new Acc_Type
            {
                ID = 2,
                Name = "LIABILITIES",
                DrIncrease = false,
                Code = "L"
            };
            Acc_Type EQUITY = new Acc_Type
            {
                ID = 3,
                Name = "OWNER'S EQUITY",
                DrIncrease = false,
                Code = "O"
            };
            Acc_Type INCOME = new Acc_Type
            {
                ID = 4,
                Name = "INCOME",
                DrIncrease = false,
                Code = "I"
            };
            Acc_Type EXPENSES = new Acc_Type
            {
                ID = 5,
                Name = "EXPENSES",
                DrIncrease = true,
                Code = "E"
            };
            accList.Add(ASSETS);
            accList.Add(LIABILITIES);
            accList.Add(EQUITY);
            accList.Add(INCOME);
            accList.Add(EXPENSES);
            return accList;
        }
       
    }
} 