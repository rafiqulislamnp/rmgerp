﻿using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    //Supplier Payment History
    public class Acc_SupplierPH : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }

        [DefaultValue(0)]
        [DisplayName("Amount")]
        public decimal? Amount { get; set; }
      
        [DisplayName("Payment From")]       
        public int Acc_AcNameFK { get; set; }
        [DisplayName("Cheque or Voucher No")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string ChequeVoucherNo { get; set; }

        [DisplayName("Bank")]
        public int Commercial_BankFK { get; set; }

        [DisplayName("Supplier")]
        public int Common_SupplierFK { get; set; }

        [DefaultValue(0)]
        [DisplayName("Currency Rate")]
        public decimal? DollarRate { get; set; }

        [DisplayName("Currency Type")]
        public int Common_CurrencyFK { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(200, ErrorMessage = "Upto 200 Chracter")]
        public string Description { get; set; }
        public bool IsApproved { get; set; }


        //...........................New Added Code...................................//

        //public Commercial_Bank Commercial_Bank { get; set; }
        //public Common_Supplier Common_Supplier { get; set; }

    }
    public class Acc_SupplierReceiveH : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }

        [DefaultValue(0)]
        [DisplayName("Amount")]
        public decimal? Amount { get; set; }

        [DisplayName("Receive To")]
        public int Acc_AcNameFK { get; set; }
        [DisplayName("Cheque or Voucher No")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string ChequeVoucherNo { get; set; }

        [DisplayName("Bank")]
        public int Commercial_BankFK { get; set; }

        [DisplayName("Supplier")]
        public int Common_SupplierFK { get; set; }

        [DefaultValue(0)]
        [DisplayName("Currency Rate")]
        public decimal? DollarRate { get; set; }

        [DisplayName("Currency Type")]
        public int Common_CurrencyFK { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(200, ErrorMessage = "Upto 200 Chracter")]
        public string Description { get; set; }
        public bool IsApproved { get; set; }


        //...........................New Added Code...................................//

        //public Commercial_Bank Commercial_Bank { get; set; }
        //public Common_Supplier Common_Supplier { get; set; }

    }
}