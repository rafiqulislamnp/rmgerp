﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Accounts
{
    public class Acc_Transaction: BaseModel
    {
        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        
        [DisplayName("Type")]
        [StringLength(20, ErrorMessage = "Upto 10 Chracter")]
        public string TransactionType { get; set; }
        
        [DisplayName("Voucher No")]
        public int VoucherNo { get; set; }
        
        [DisplayName("To Account")]
        public int To_Acc_NameFK { get; set; }

        [DisplayName("From Account")]
        public int From_Acc_NameFK { get; set; }

        [DisplayName("Cost Center")]
        public int Acc_CostPoolFK { get; set; }

        [DisplayName("Cheque No")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string ChequeNo { get; set; }

        [DisplayName("Cheque Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? ChequeDate { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Amount")]
        public decimal Amount { get; set; }

        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        public string Description { get; set; }
        
        [DefaultValue(true)]
        public bool? Editable { get; set; }

        [DefaultValue(false)]
        public bool? IsFinal { get; set; }
        public int CreatedBy { get; set; }
        public int? EditedBy { get; set; }
    }

    public class Acc_Database_Table : BaseModel
    {
        [Index("Acc_Database_Table_Name_UniqueKey", 1, IsUnique = true)]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(100)]
        public string Name { get; set; }
    }
    public class Acc_CostPool : BaseModel
    {
        [Index("Acc_Database_Table_Name_UniqueKey", 1, IsUnique = true)]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(100)]
        public string Name { get; set; }
    }
    enum Acc_NotifierType
    {
        Cheque = 0,
        LcOpen = 1,
        DocPayment = 2,
        ChequeReceive = 3
    }
    public class Acc_Notifier : BaseModel
    {
        [DisplayName("Type")]
        public int Type { get; set; }
        public DateTime Date { get; set; }

        [MaxLength]
        [DisplayName("Particulars")]
        public string Particulars { get; set; }
        public string ChequeNo { get; set; }
        public decimal Amount { get; set; }
        public int TableIdFk { get; set; }
        public int RowIdFk { get; set; }
        public int FromFk { get; set; }
        [MaxLength]
        public string FromName { get; set; }
        public decimal FromBalance { get; set; }
        public int ToFk { get; set; }
        [MaxLength]
        public string ToName { get; set; }
        public decimal ToBalance { get; set; }
        public int IsApproved { get; set; }
        public bool IsSeen { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public int? ApprovedBy { get; set; }
        
    }
    public class Acc_SupplierBill : BaseModel
    {
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [DisplayName("Supplier")]
        public int Common_SupplierFk { get; set; }
        [DisplayName("Bill Type")]
        public int Common_BillTypeFk { get; set; }
        [DisplayName("Description")]
        public string Particulars { get; set; }

        [DisplayName("Approved Or Not")]
        public bool IsApproved { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public int? ApprovedBy { get; set; }
        public int CreatedBy { get; set; }
        public int? EditedBy { get; set; }
    }
    public class Acc_SupplierBillSlave : BaseModel
    {
        public int Acc_SupplierBillFk { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }
        [DisplayName("Challan/Invoice")]
        public int ChallanOrInvoiceFk { get; set; }
        [DisplayName("Challan/Invoice Note")]
        public string ChallanInvoiceNo { get; set; }
        [DisplayName("Description")]
        public string Particulars { get; set; }
        [DisplayName("Amount")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Amount Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public Decimal Amount { get; set; }
        [DisplayName("Approved Or Not")]
        public bool IsApproved { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public int? ApprovedBy { get; set; }

        [DefaultValue(0)]
        [DisplayName("Currency Rate")]
        public decimal DollarRate { get; set; }
        [DisplayName("Currency Type")]
        public int Common_CurrencyFK { get; set; }

        public int CreatedBy { get; set; }
        public int? EditedBy { get; set; }
    }
    public class Acc_DollarRate : BaseModel
    {
        [DisplayName("Dollar Rate")]
        public decimal DollarRate { get; set; }
        [DisplayName("Effected From")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime EffectDate { get; set; }
    }
}