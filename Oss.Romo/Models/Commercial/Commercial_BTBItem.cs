﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_BTBItem : BaseModel
    {
        

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        //[DisplayName("Category")]
        //public int? Mkt_CategoryFK { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [Required(ErrorMessage = "Name is Required")]
        [DisplayName("Item")]
        public string Name { get; set; }

        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }


        //...........................New Added Code...................................//

        //public List<Commercial_B2bLC> Commercial_B2bLCs { get; set; }
    }
}