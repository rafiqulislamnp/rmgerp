﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_Import : BaseModel
    {
        //[RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]

        [DisplayName("Shipment Quantity")]
        [Required(ErrorMessage = "Quantity is Required")]
        public decimal Quantity { get; set; }

        [Display(Name = "Unit")]
        public int Common_UnitFKForShipment { get; set; }

        [Display(Name ="Unit")]
        public int Common_UnitFK { get; set; }
        
        [DisplayName("Unit Quantity"), Required(ErrorMessage = "Unit quantity can not be empty.")]
        public string UnitQty { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("ETD")]
        public DateTime ETDDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("ETA")]
        public DateTime ETADate { get; set; }

        [DisplayName("Ship Mode")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Ship { get; set; }

        [DisplayName("Remarks")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Remarks { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Orginal Doc's Send Dt")]
        public DateTime? DocDate { get; set; }

        public int? Commercial_B2bLCFK { get; set; }
        
        //...........................New Added Code...................................//
        //public int? Commercial_B2bLCFK { get; set; }

        //public Commercial_B2bLC Commercial_B2bLC { get; set; }
    }
   
}