﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_PI : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("PI")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }

        [StringLength(200)]
        public string ScanedFile { get; set; }


        //...........................New Added Code...................................//

        //public List<Commercial_B2BLcSupplierPI> Commercial_B2BLcSupplierPIs { get; set; }

        //public List<Commercial_MLcSupplierPI> Commercial_MLcSupplierPIs { get; set; }

        //public List<Commercial_PIPOSupplier> Commercial_PIPOSuppliers { get; set; }

        //public List<Commercial_PISupplierPO> Commercial_PISupplierPOs { get; set; }

    }
}