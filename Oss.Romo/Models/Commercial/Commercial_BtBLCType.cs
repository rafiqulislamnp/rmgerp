﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_BtBLCType : BaseModel
    {
        [DisplayName("L/C Type")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }
        [DisplayName("Description")]
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        //...........................New Added Code...................................//

        //public List<Commercial_B2bLC> Commercial_B2bLCs { get; set; }
    }
}