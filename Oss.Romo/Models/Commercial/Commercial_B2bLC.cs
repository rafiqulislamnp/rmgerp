﻿using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_B2bLC : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("BTB NO")]
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string Name { get; set; }
        [DefaultValue(0)]
        [DisplayName("Required Quantity")]
        public decimal? RequiredQuantity { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }

        [DisplayName("BTB Value")]
        [Required(ErrorMessage = "B2B L/C Value is required")]
        public decimal Amount { get; set; }
        
        // Total value is comming from Value and Amendment value(Sum)
        //public decimal TotalValue { get; set; }
        [DefaultValue(0)]
        [DisplayName("Amendment Increase")]
        public decimal AmendmentIncrease { get; set; }
        [DefaultValue(0)]
        [DisplayName("Amendment Decrease")]
        public decimal AmendmentDecrease { get; set; }
      
        [DisplayName("Shipment")]
        public string Shipment { get; set; }        

        [DisplayName("Master L/C")]
        public int? Commercial_MasterLCFK { get; set; }
        [DisplayName("Item")]
        public int Commercial_BTBItemFK { get; set; }

        [DisplayName("UD")]
        public int Commercial_UDFK { get; set; }

        [DisplayName("Supplier")]
        public int Common_SupplierFK { get; set; }
        [DisplayName("Origin")]
        public int Commercial_LCOreginFK { get; set; }
        [DisplayName("Type")]
        public int Commercial_BtBLCTypeFK { get; set; }

        [DisplayName("Unit")]
        [DefaultValue(28)]
        public int? Common_UnitFK { get; set; }

        [DisplayName("Description")]
        [StringLength(200, ErrorMessage = "Upto 200 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Scaned File")]
        [StringLength(200)]
        public string ScanedFile { get; set; }

        [DisplayName("Account Head")]
        public int Acc_AcNameFk { get; set; }
        [DisplayName("Status")]
        [DefaultValue("Running")]
        [StringLength(10, ErrorMessage = "Upto 10 Chracter")]
        public string Status { get; set; } //Add due to requirement--FD 
        public bool IsApproved { get; set; } //Add due to requirement--FD 

        [DefaultValue(1)]
        [DisplayName("Currency Rate")]
        public decimal CurrencyRate { get; set; }
        public int CurrencyType { get; set; }

        //...........................New Added Code...................................//
        //public Commercial_MasterLC Commercial_MasterLC { get; set; }

        //public Commercial_BTBItem Commercial_BTBItem { get; set; }

        //public Commercial_UD Commercial_UD { get; set; }

        //public Common_Supplier Common_Supplier { get; set; }

        //public Commercial_LCOregin Commercial_LCOregin { get; set; }

        //public Commercial_BtBLCType Commercial_BtBLCType { get; set; }

        //public List<Commercial_B2BLcSupplierPI> Commercial_B2BLcSupplierPIs { get; set; }

        //public List<Commercial_B2bLcSupplierPO> Commercial_B2bLcSupplierPOs { get; set; }

        //public List<Commercial_B2BPaymentInformation> Commercial_B2BPaymentInformations { get; set; }

        //public List<Commercial_Import> Commercial_Imports { get; set; }

        //public List<Commercial_MLcSupplierB2BLc> Commercial_MLcSupplierB2BLcs { get; set; }


    }
}