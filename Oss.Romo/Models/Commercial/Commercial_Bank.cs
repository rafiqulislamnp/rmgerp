﻿using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_Bank:BaseModel
    {
        [DefaultValue(0)]
        public int? FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int? LastEditeddBy { get; set; }


        [DisplayName("Bank")]
        [StringLength(200, ErrorMessage = "Upto 200 Chracter")]
        public string Name { get; set; }

        [DisplayName("Address")]
        [DataType(DataType.MultilineText)]
        [StringLength(200, ErrorMessage = "Upto 200 Chracter")]
        public string Address { get; set; }

        [DataType(DataType.PhoneNumber)]
        [DisplayName("Contact No")]
        public string ContactNo { get; set; }

        [DisplayName("Country")]
        public int? Common_CountryFK { get; set; }

        [DisplayName("Swift Code")]
        [DataType(DataType.MultilineText)]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string SwiftCode { get; set; }

        [DisplayName("Lien Bank")]
        [DefaultValue(false)]
        public bool IsLien { get; set; }

        //...........................New Added Code...................................//
        //public List<Acc_SupplierPH> Acc_SupplierPHs { get; set; }

        //public Common_Country Common_Country { get; set; }

        //public List<Commercial_MasterLC> Commercial_MasterLCs { get; set; }

    
        
        
        
        
        //Forget................
        //[DisplayName("Country")]
        //[DataType(DataType.MultilineText)]
        //[StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        //public string Country { get; set; }

       
        //[DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        //[DisplayName("Date")]
        //public DateTime Date { get; set; }


    }
}