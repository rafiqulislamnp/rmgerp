﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_UDSlave : BaseModel
    {
        [DisplayName("UD")]
        public int Commercial_UDFK { get; set; }
        [DisplayName("Master L/C")]
        public int Commercial_MasterLCFK { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }


        //...........................New Added Code...................................//

        //public Commercial_UD Commercial_UD { get; set; }

        //public Commercial_MasterLC Commercial_MasterLC { get; set; }

    }
}