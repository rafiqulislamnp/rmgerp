﻿using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_InvoiceSlave : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Invoice")]
        public int Commercial_InvoiceFk { get; set; }

        [DisplayName("Deliverd Schedule")]
        public int Shipment_OrderDeliverdScheduleFK { get; set; }
        [DisplayName("Challan No")]
        public int Shipment_CNFDelivaryChallanFk { get; set; }

        [DisplayName("Order")]
        public int? MktBOMFK { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        //...........................New Added Code...................................//

        //public Commercial_Invoice Commercial_Invoice { get; set; }

        //public Mkt_BOM Mkt_BOM { get; set; }
    }
}