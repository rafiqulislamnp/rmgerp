﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.ViewModels.Reports;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_B2BLCStatementReport
    {
        public List<ReportDocType> Report { get; set; }
        public List<VmCommercial_B2bLC> BtbList { get; set; }
    }
}