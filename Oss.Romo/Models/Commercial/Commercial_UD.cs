﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_UD : BaseModel
    {
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        [Required(ErrorMessage = "B2B L/C Value is required")]
        [DisplayName("UD NO")]
        public string UdNo { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("UD Date")]
        public DateTime Date { get; set; }

        // Abailable BTB opening value set as %.
        [DisplayName("BTB Opening")]
        public decimal? BTBOpening { get; set; }

        [DisplayName("Account Head")]
        public int Acc_AcNameFk { get; set; }
        public bool IsClose { get; set; } = false;

        //...........................New Added Code...................................//


        //public List<Commercial_B2bLC> Commercial_B2bLCs { get; set; }

        //public List<Commercial_BillOfExchange> Commercial_BillOfExchanges { get; set; }

        //public List<Commercial_Invoice> Commercial_Invoices { get; set; }

        //public List<Commercial_UDSlave> Commercial_UDSlaves { get; set; }
    }
}