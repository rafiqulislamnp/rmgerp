﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_MLcSupplierPI : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }


        [DisplayName("Master L/C Supplier PI")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }

        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string Description { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }


        [DisplayName("Master L/C")]
        public int? Commercial_MasterLCFK { get; set; }
        [DisplayName("PI")]
        public int? Commercial_PIFK { get; set; }

        //...........................New Added Code...................................//

        //public Commercial_MasterLC Commercial_MasterLC { get; set; }

        //public Commercial_PI Commercial_PI { get; set; }

    }
}