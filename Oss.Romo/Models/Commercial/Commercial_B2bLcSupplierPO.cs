﻿using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_B2bLcSupplierPO : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
       

        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(200, ErrorMessage = "Upto 200 Chracter")]
        public string Description { get; set; }
        

        [DisplayName("Marketing PO")]
        public int? Mkt_POFK { get; set; }


        [DisplayName("B2B L/C")]
        public int? Commercial_B2bLCFK { get; set; }


        //...........................New Added Code...................................//

        //public Mkt_PO Mkt_PO{ get; set; }

        //public Commercial_B2bLC Commercial_B2bLC { get; set; }

    }
}