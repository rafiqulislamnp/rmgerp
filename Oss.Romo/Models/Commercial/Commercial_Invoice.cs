﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_Invoice : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DisplayName("Invoice No")]
        [Required(ErrorMessage = "Invoice number is Required")]
        public string InvoiceNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Invoice Date")]
        public DateTime InvoiceDate { get; set; }
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        [DisplayName("Erc No")]
        public string ErcNo { get; set; }
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DisplayName("Invoice Id No")]
        public string InvoiceIdNo { get; set; }

        [DisplayName("Shipped By")]
        public string ShippedPer { get; set; }
        [DisplayName("Port Of Loding ")]
        public int ShipmentPortOfLoadingFk { get; set; }
        [DisplayName("Port Of Discharge")]
        public int ShipmentPortOfDischargeFk { get; set; }
        [DisplayName("Final Destination")]
        public int CommonCountryFk { get; set; }

        [DisplayName("Country of Origin")]
        public int CommonCountryOfOriginFk { get; set; }
        [StringLength(150, ErrorMessage = "Upto 150 Chracter")]
        [DisplayName("BL No")]
        public string BLNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("BL Date")]
        public DateTime BLDate { get; set; }

        //[DisplayName("Deliverd Schedule")]
        //public string Ship_DeliverdScheduleFK { get; set; }

        // [DisplayName("Order")]
        //public int? Common_TheOrderFk { get; set; }
        
        [DisplayName("Exp No")]
        public string ExpNo { get; set; }

        [DisplayName("Exp Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpDate { get; set; }

        [DataType(DataType.MultilineText)]
        public string ShippingMarks { get; set; }

        //public string CTN { get; set; }

        [DisplayName("Total CTN N.WT")]
        public decimal NWT { get; set; }

        [DisplayName("Total CTN G.WT")]
        public decimal GWT { get; set; }

        public string CBM { get; set; }

        public int TermOfShipment { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Estimated Received Date")]
        public DateTime EstimatedReceivedDate { get; set; }

        [DisplayName("Is Increase")]
        public bool IsIncrease { get; set; }
        [DisplayName("Increase/Decrease Amount")]
        public decimal IncDecAmount { get; set; }
        [DisplayName("Description")]
        public string IncDecDescription { get; set; }
    }
}