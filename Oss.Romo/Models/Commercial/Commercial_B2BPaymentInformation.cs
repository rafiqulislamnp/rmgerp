﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_B2BPaymentInformation : BaseModel
    {
        [DisplayName("B2B L/C No")]
        public int Commercial_B2bLCFK { get; set; }

        [DisplayName("Supplier Payment")]
        public decimal B2bLCPayment { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Payment Date")]
        public DateTime Date { get; set; }

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        public bool IsApproved { get; set; }


        //...........................New Added Code...................................//

        //public Commercial_B2bLC Commercial_B2bLC { get; set; }
    }
}