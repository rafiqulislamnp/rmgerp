﻿using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_MasterLCBuyerPO : BaseModel
    {
        [DisplayName("Master L/C Buyer PO")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }

        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string Description { get; set; }
        [DisplayName("Order")]
        public int? MKTBOMFK { get; set; }
        [DisplayName("Master L/C")]
        public int? Commercial_MasterLCFK { get; set; }


        //...........................New Added Code...................................//

        //public Commercial_MasterLC Commercial_MasterLC { get; set; }

        //public Mkt_BOM Mkt_BOM { get; set; }


    }
}