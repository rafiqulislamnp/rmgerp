﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_BillOfExchangeSlave :BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }
        [DisplayName("Bill of Exchange")]
        public int Commercial_BillOfExchangeFk { get; set; }
        [DisplayName("Invoice")]
        public int Commercial_InvoiceFK { get; set; }
    }
}