﻿using Oss.Romo.Models.Common;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Commercial_B2bLcSupplierPOSlave : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
      
        
        [DisplayName("Assign Quantity")]
        public decimal? TotalRequired { get; set; }
        

        [DisplayName("PO ID")]
        public int? Mkt_POSlaveFK { get; set; }

        [DisplayName("PO ID")]
        public int? Commercial_B2bLcSupplierPOFK { get; set; }
        
    }
}