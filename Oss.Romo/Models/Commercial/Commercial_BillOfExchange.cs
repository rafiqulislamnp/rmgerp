﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_BillOfExchange : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [Required(ErrorMessage = "Bill NO is Required")]
        [DisplayName("Bill NO")]
        public string Name { get; set; }

        [DisplayName("Bill Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime BillDate { get; set; }

        [DisplayName("Courier NO")]
        public string Courier { get; set; }

        [DisplayName("Courier Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime CourierDate { get; set; }


        [Required(ErrorMessage = "Master LC is Required")]
        [DisplayName("Master LC")]
        public int Commercial_MasterLCFK { get; set; }

        [StringLength(60, ErrorMessage = "Upto 60 Chracter")]
        [DisplayName("Received Type")]
        public string ReceivedType { get; set; }

        [DisplayName("FDBC NO")]
        
        public string FDBCNO { get; set; }

        [DisplayName("FDBC Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FDBCDate { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }
    }
}
