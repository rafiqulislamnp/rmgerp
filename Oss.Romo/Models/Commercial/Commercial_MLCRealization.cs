﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_MLCRealization:BaseModel
    {
        [DefaultValue(0)]
        public int? FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int? LastEditeddBy { get; set; }


        [DisplayName("Master L/C")]
        public int? Commercial_MasterLCFK { get; set; }

        [DisplayName("Invoice No")]
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string Invoice { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Invoice Date")]
        public DateTime InvoiceDate { get; set; }

        [DisplayName("Invoice Quantity")]
        public int Quantity { get; set; }

        [DisplayName("Invoice Value")]
        public decimal Value { get; set; }

        [DisplayName("Export No")]
        public string Export { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Export Date")]
        public DateTime ExportDate { get; set; }

        [DisplayName("B/L No")]
        public string BLNO { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("B/L Date")]
        public DateTime BLDate { get; set; }

        [DisplayName("FDBC/FDBP No")]
        public string FDBPNo { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("FDBC/FDBP Date")]
        public DateTime FDBPDate { get; set; }


        [DisplayName("Courier No")]
        public string Courier { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Courier Date")]
        public DateTime CourierDate { get; set; }

        [DisplayName("Amount")]
        public decimal Amount { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }

        [DisplayName("Freight Paid")]
        public decimal Paid { get; set; }

        [DisplayName("Commision")]
        public decimal Commision { get; set; }

        [DisplayName("Insurance")]
        public string Insurance { get; set; }


        //...........................New Added Code...................................//

        //public Commercial_MasterLC Commercial_MasterLC { get; set; }
    }
}