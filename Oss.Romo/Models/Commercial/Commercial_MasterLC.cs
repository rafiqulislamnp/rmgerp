﻿using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Commercial
{
    public class Commercial_MasterLC : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Master L/C No")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Master L/C Date")]
        public DateTime Date { get; set; }

        [DisplayName("Type")]
        public int? Commercial_LCTypeFK { get; set; }
        

        [DataType(DataType.MultilineText)]
        [DisplayName("Notify Party")]
        [StringLength(150, ErrorMessage = "Upto 150 Chracter")]
        public string NotifyParty { get; set; }

        [DisplayName("Total Value")]
        public decimal?  TotalValue{ get; set; }
       
        // Abailable tolerance value set as %.
        [DisplayName("Tolerance")]
        public decimal? BuyerTolerance { get; set; }

        [DisplayName("Handling Charge")]
        public decimal? HandlingCharge { get; set; }

        //[DisplayName("Test Charge")]
        //public decimal? TestCharge { get; set; }

        //[DisplayName("Buyer Comm.")]
        //public decimal? BuyerCommision { get; set; }

        [DisplayName("Freight Charge")]
        public decimal? FreightCharge { get; set; }
        

        [DisplayName("Shipment Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ShipmentDate { get; set; }

        [DisplayName("Expiry Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpiryDate { get; set; }

        [DisplayName("Port")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Port { get; set; }

        [DisplayName("Destination")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string DestinationCountry { get; set; }
        

        [StringLength(200)]
        [DisplayName("Scaned File")]        
        public string ScanedFile { get; set; }

        [DisplayName("Applicant")]
        public int? Mkt_BuyerFK { get; set; }

        [DisplayName("Buyer Bank")]
        public int? Commercial_BuyerBankFK { get; set; }

        [DisplayName("Lien Bank")]
        public int? Commercial_LienBankFK { get; set; }
        [DefaultValue(0)]
        [DisplayName("Amendment Increase")]
        public decimal AmendmentIncrease { get; set; }
        [DefaultValue(0)]
        [DisplayName("Amendment Decrease")]
        public decimal AmendmentDecrease { get; set; }

        [DisplayName("Account Head")]
        public int Acc_AcNameFk { get; set; }


        //...........................New Added Code...................................//

        //public Mkt_Buyer Mkt_Buyer { get; set; }

        //public Commercial_Bank Commercial_Bank { get; set; }


        //public Commercial_LCType Commercial_LCType { get; set; }

        //public List<Acc_LCAlocation> Acc_LCAlocations { get; set; }

        //public List<Commercial_B2bLC> Commercial_B2bLCs { get; set; }

        //public List<Commercial_BillOfExchange> Commercial_BillOfExchanges { get; set; }

        //public List<Commercial_Invoice> Commercial_Invoices { get; set; }

        //public List<Commercial_MasterLCBuyerPI> Commercial_MasterLCBuyerPIs { get; set; }

        //public List<Commercial_MasterLCBuyerPO> Commercial_MasterLCBuyerPOs { get; set; }

        //public List<Commercial_MLcBuyerDocs> Commercial_MLcBuyerDocss { get; set; }

        //public List<Commercial_MLCRealization> Commercial_MLCRealizations { get; set; }

        //public List<Commercial_MLcSupplierB2BLc> Commercial_MLcSupplierB2BLcs { get; set; }

        //public List<Commercial_MLcSupplierPI> Commercial_MLcSupplierPIs { get; set; }

        //public List<Commercial_MLcSupplierPO> Commercial_MLcSupplierPOs { get; set; }

        //public List<Commercial_UDSlave> Commercial_UDSlaves { get; set; }
    }
}