﻿using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Collections.Generic;

namespace Oss.Romo.Models.Store
{
    public class Raw_SubCategory : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Category")]
        public int Raw_CategoryFK { get; set; }

        [DisplayName("Sub-Category")]
        [Required(ErrorMessage = "Name is Required")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Name { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

       
        [DefaultValue(true)]
        public bool IsNew { get; set; }




        //...........................New Added Code...................................//

        //public Raw_Category Raw_Category { get; set; }
        //public List<Raw_Item> Raw_Items { get; set; }

    }
}
