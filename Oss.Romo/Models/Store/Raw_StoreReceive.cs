﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Store
{
    public class Raw_StoreReceive: BaseModel
    {

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Comment")]
        public string Description { get; set; }

        [DisplayName("Receive Quantity")]
        public decimal? TotalReceive { get; set; }

    }
}