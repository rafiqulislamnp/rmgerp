﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Oss.Romo.Models.Store
{
    public class Raw_Category : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Category")]
        [Required(ErrorMessage = "Name is Required")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Name { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DefaultValue(false)]
        public bool IsInventory { get; set; }
        [DefaultValue(true)]
        public bool IsNew { get; set; }
        //...........................New Added Code...................................//
        //public List<Raw_Supplier> Raw_Suppliers { get; set; }
        //public List<Raw_SubCategory> Raw_SubCategorys { get; set; }

    }
}