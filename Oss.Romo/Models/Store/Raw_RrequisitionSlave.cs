﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Romo.Models.Common;

namespace Oss.Romo.Models.Store
{
    public class Raw_RrequisitionSlave : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        public int? Common_UserFK { get; set; }
        [Required(ErrorMessage = "Name is Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Name { get; set; }



        //...........................New Added Code...................................//
        //public Common_User Common_User { get; set; }

    }
}