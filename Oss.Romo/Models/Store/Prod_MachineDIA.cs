﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;

namespace Oss.Romo.Models.Store
{
    public class Prod_MachineDIA : BaseModel
    {

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
       
        [DisplayName("Machine DIA")]
        [Required(ErrorMessage = "Name is Required")]
        [MaxLength(100, ErrorMessage = "Upto 50 Chracter")]
        public string Name { get; set; }
        
    }
}