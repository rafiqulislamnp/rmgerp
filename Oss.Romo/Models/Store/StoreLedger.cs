﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Store
{
    public class StoreLedger
    {
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public decimal rcvqty { get; set; }
        public decimal outqty { get; set; }

    }
}