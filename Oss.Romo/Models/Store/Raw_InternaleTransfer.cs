﻿using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Raw_InternaleTransfer : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [MaxLength(30)]
        [DisplayName("ITR NO")]
        public string CID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime? Date { get; set; }
        

        [Required(ErrorMessage = "From Department is required"), Display(Name = "From Department")]
        public int? FromUser_DeptFK { get; set; }

        [Required(ErrorMessage = "To Department is required"), Display(Name = "To  Department")]
        public int? ToUser_DeptFK { get; set; }

        [DefaultValue(false)]
        public bool IsAuthorize { get; set; }

        [DefaultValue(false)]
        public bool IsAcknowledge { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime AcknowledgeDate { get; set; }
    }
}