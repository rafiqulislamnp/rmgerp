﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Romo.Models.Common;

namespace Oss.Romo.Models.Store
{
    public class Raw_Rrequisition : BaseModel
    {

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Requisition ID")]
        [StringLength(25)]
        public string RequisitionID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime? RequestDate { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Supervisor { get; set; }

        public string priority { get; set; }
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DefaultValue(false)]
        public bool IsComplete { get; set; }



        //...........................New Added Code...................................//
        //public Common_User Common_User { get; set; }
    }
}