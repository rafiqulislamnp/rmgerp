﻿using Oss.Romo.Models.Common;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Marketing
{
    public class Raw_InternaleTransferSlave : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }

        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Item")]
        public int? Raw_ItemFK { get; set; }
        

        [DisplayName("Transfer Quantity")]
        public int? Quantity { get; set; }

        [DisplayName("Unit")]
        public int? Common_UnitFK { get; set; }
        
    
        [DisplayName("PO ID")]
        public int? Raw_InternaleTransferFK { get; set; }


        public int? Mkt_BOMFK { get; set; }

        public int? Prod_TransitionItemInventoryFK { get; set; }
        [DisplayName("Item")]
        public int? RequisitionSlaveFK { get; set; }

        [DisplayName("POSlaveFK")]
        [DefaultValue(0)]
        public int? Mkt_POSlaveFK { get; set; }

    }


}