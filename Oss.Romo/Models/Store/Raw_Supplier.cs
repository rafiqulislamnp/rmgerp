﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Romo.Models.Production;

namespace Oss.Romo.Models.Store
{
    public class Raw_Supplier : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        public int? Raw_CategoryFK { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        //...........................New Added Code...................................//

        public Raw_Category Raw_Category { get; set; }

        //public List<Prod_PoHistory> Prod_PoHistorys { get; set; }

        //public List<Prod_PoHistoryType> Prod_PoHistoryTypes { get; set; }
    }
}