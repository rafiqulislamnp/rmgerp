﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;

namespace Oss.Romo.Models.Store
{
    public class Raw_ItemMoving : BaseModel
    {

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        public int? FromRaw_StoreFK { get; set; }

        public int? ToRaw_StoreFK { get; set; }

        public int? Mkt_POSlaveFK { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DefaultValue(0)]
        [DisplayName("Total Quantity")]
        public decimal? Quantity { get; set; }

        [StringLength(150, ErrorMessage = "Upto 150 Chracter")]
        [Required(ErrorMessage = "Challan Number is Required")]
        public string Challan { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        
    }
}