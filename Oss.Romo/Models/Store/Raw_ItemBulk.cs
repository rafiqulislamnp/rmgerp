﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.CoreProcess.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;

namespace Oss.Romo.Models.Store
{
    public class Raw_ItemBulk : BaseModel
    {

        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("SubCategory")]
        public int Raw_SubCategoryFK { get; set; }

        [DisplayName("Item")]
        [Required(ErrorMessage = "Name is Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }


        [DefaultValue(true)]
        public bool IsNew { get; set; }
        //...........................New Added Code...................................//
        //public Raw_SubCategory Raw_SubCategory { get; set; }
        //public List<Mkt_BOMSlave> Mkt_BOMSlaves { get; set; }
        //public List<Mkt_BOMSlaveFabric> Mkt_BOMSlaveFabrics { get; set; }
        //public List<Mkt_BOMStepJobs> Mkt_BOMStepJobss { get; set; }
        //public List<Mkt_POSlave> Mkt_POSlaves { get; set; }

        //public List<Prod_Requisition_Slave> Prod_Requisition_Slaves { get; set; }
    }
}