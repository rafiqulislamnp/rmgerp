﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Alert: BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Type")]
        public string Type { get; set; }

        [DisplayName("NO Days")]
        [Index(IsUnique = true)]
        public int? NODays { get; set; }

        [DisplayName("Color")]
        public string Color { get; set; }
    }
}