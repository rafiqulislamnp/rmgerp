﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Shipment_OrderDeliverdSchedule: BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        public int Shipment_CNFDelivaryChallanFk { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Deliverd Date")]
        public DateTime Date { get; set; }

        [DisplayName("Order")]
        public int? Common_TheOrderFk { get; set; }

        [DisplayName("Order")]
        public int? Mkt_BOMFK { get; set; }

        [DisplayName("Port")]
        public int? Mkt_OrderDeliveryFk { get; set; }

        [DisplayName("Delivered Qty")]
        [Required(ErrorMessage = "Quantity is Required")]
        public decimal DeliverdQty { get; set; }
        [DisplayName("Ctn Qty")]
        public int CtnQty { get; set; }
        [DisplayName("Pack Qty")]
        public int PackQty { get; set; }

        [DisplayName("Reference")]
        public string Reference { get; set; }
    }
}