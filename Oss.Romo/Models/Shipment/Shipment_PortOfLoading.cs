﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Shipment_PortOfLoading:BaseModel
    {
        [Required(ErrorMessage = "Port Of Loading is Required")]
        [DisplayName("Port Of Loading")]
        public string PortOfLoading { get; set; }
    }
}