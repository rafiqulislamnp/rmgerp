﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Shipment_OrderColorAndSizeRatio: BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Order")]
        public int? Common_TheOrderFk { get; set; }

        [DisplayName("Order")]
        public int? Mkt_BOMFk { get; set; }

        [DisplayName("Color & Size")]
        [Required(ErrorMessage = "Color and Size is Required")]
        public int? Mkt_OrderDeliveryFk { get; set; }

        [DisplayName("Quantity")]
        [Required(ErrorMessage = "Quantity is Required")]
        public decimal Quantity { get; set; }
    }
}