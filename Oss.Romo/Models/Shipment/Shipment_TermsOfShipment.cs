﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Shipment_TermsOfShipment : BaseModel
    {
        [Required(ErrorMessage = "Terms of Shipment is Required")]
        [DisplayName("Terms of Shipment")]
        public string Name { get; set; }
    }
}