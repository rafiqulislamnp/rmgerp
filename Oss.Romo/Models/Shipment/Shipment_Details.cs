﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Shipment_Details : BaseModel
    {
        public int Shipment_HistoryFK { get; set; }

        [DisplayName("Size Range")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string SizeRange { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [DisplayName("Total Pack")]
        public int QPack { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [DisplayName("QUP")]
        [Required(ErrorMessage = "Quantity is Required")]
        public int Quantity { get; set; }

        //...........................New Added Code...................................//

       // public Shipment_History Shipment_History { get; set; }

       

    }
}