﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Shipment_CNFDelivaryChallan:BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        [DisplayName("Challan No")]
        public string ChallanNo { get; set; }
        [DisplayName("C&F Agent")]
        public int Common_SupplierFk { get; set; }
        [DisplayName("Transport Supplier")]
        public int Transporter_Common_SupplierFk { get; set; }
        [DisplayName("Driver Name")]
        [Required]
        public string DriverName { get; set; }
        [DisplayName("Lock No")]
        [Required]
        public string LockNo { get; set; }
        [DisplayName("Driving License No")]
        [Required]
        public string DLNo { get; set; }
        [DisplayName("Truck No")]
        public string TruckNo { get; set; }
        [DisplayName("Driver Mobile")]
        [Required]
        public string Though { get; set; }
        [DisplayName("Contact Person Name")]
        [Required]
        public string Attr { get; set; }
        [DisplayName("Contact Person Mobile")]
        [Required]
        public string Mobile { get; set; }
        public string Description { get; set; }
        [Required]
        [DisplayName("Challan Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [DisplayName("In-Time")]
        public string InTime { get; set; }
        [DisplayName("Out-Time")]
        public string OutTime { get; set; }
        [Required]
        [DisplayName("Gate Pass No")]
        public string GatePassNo { get; set; }
        [Required]
        [DisplayName("Gate Pass Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime GatePassDate { get; set; }
        [DisplayName("UnLoading Port")]
        [Required]
        public int ShipmentUnloadingPortFk { get; set; }
        public string Remarks { get; set; }
    }
}