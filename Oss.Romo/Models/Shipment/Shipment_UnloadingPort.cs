﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Shipment_UnloadingPort : BaseModel
    {
        [Required(ErrorMessage = "Terms of Shipment is Required")]
        [DisplayName("Unloading Port")]
        public string Name { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }
        public string Description { get; set; }

    }
}