﻿using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Shipment_History : BaseModel
    {

        [DisplayName("Order No")]
        public int Common_TheOrderFK { get; set; }

        [DisplayName("Shipment ID")]
        public string ShipmentID { get; set; }
       
        [DisplayName("Shipper Name")]
        public string ShipperName { get; set; }

        [DisplayName("Receiver Name")]
        public string ReceiverName { get; set; }

        [DisplayName("Shipment Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DisplayName("Total Carton")]
        public string TotalPack { get; set; }

        [DisplayName("Short Note")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string DescriptionOfGoods { get; set; }

        //...........................New Added Code...................................//

       // public Common_TheOrder Common_TheOrder { get; set; }
        //public List<Shipment_Details> Shipment_Detailss { get; set; }
    }
}