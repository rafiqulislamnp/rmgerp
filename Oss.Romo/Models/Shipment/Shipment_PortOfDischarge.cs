﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Shipment
{
    public class Shipment_PortOfDischarge : BaseModel
    {
        [Required(ErrorMessage = "Port Of discharge is Required")]
        [DisplayName("Port Of Discharge")]
        public string PortOfDischarge { get; set; }
    }
}