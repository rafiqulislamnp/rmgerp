﻿
    $(document).ready(function () {
        var url = "";
        $("#dialog-alert").dialog({
            autoOpen: false,
            resizable: false,
            height: 500,
            width: 900,
            show: { effect: 'left', direction: "up" },
            modal: true,
            draggable: true,
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                "OK": function () {
                    $(this).dialog("close");

                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

        @*if ('@TempData["msg"]' != "") {
            $("#dialog-alert").dialog('open');
        }*@

        $("#dialog-edit").dialog({
            title: 'Create Bill Of Materials',
            autoOpen: false,
            resizable: false,
            height: 500,
            width: 900,
            show: { effect: 'left', direction: "up" },
            modal: true,
            draggable: true,
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
                $(this).load(url);
            }
        });

        $("#dialog-confirm").dialog({
            autoOpen: false,
            resizable: false,
            height: 170,
            width: 350,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: true,
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();

            },
            buttons: {
                "OK": function () {
                    $(this).dialog("close");
                    window.location.href = url;
                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

        $("#dialog-detail").dialog({
            title: 'View Bill Of Materials',
            autoOpen: false,
            resizable: false,
            height: 500,
            width: 900,
            show: { effect: 'left', direction: "up" },
            modal: true,
            draggable: true,
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
                $(this).load(url);
            },
            buttons: {
                "Close": function () {
                    $(this).dialog("close");
                }
            }
        });
        $("#lnkCreate").click(function (e) {
            url = $(this).attr('href');
            $("#dialog-edit").dialog('open');

            return false;
        })

        $(".lnkEdit").click(function (e) {
            url = $(this).attr('href');
            $(".ui-dialog-title").html("Update User");
            $("#dialog-edit").dialog('open');

            return false;
        })

        $(".lnkDelete").click(function (e) {
            url = $(this).attr('href');
            $("#dialog-confirm").dialog('open');

            return false;
        })
        $(".lnkDetail").click(function (e) {
            url = $(this).attr('href');
            $("#dialog-detail").dialog('open');

            return false;
        })
        $("#btncancel").click(function (e) {
            $("#dialog-edit").dialog("close");
            return false;
        })
    });