﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_Details
    {
        private xOssContext db;
        public IEnumerable<VmShipment_Details> DataList { get; set; }

        public Shipment_Details Shipment_Details { get; set; }
        public VmShipment_History VmShipment_History { get; set; }
        public Shipment_History Shipment_History { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public decimal? TotalQuantity { get; set; }
        //public string BOMStyle { get; set; }
        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_Details
                     join t2 in db.Shipment_History
                     on t1.Shipment_HistoryFK equals t2.ID
                    
                     where t1.Shipment_HistoryFK==id && t1.Active==true
                     select new VmShipment_Details
                     {
                         Shipment_Details = t1,
                         Shipment_History= t2,
                         TotalQuantity = t1.QPack * t1.Quantity
                     }).OrderByDescending(x => x.Shipment_Details.ID).AsEnumerable();
            this.DataList = v;

        }

        public void SelectSingle(string id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_Details
                     select new VmShipment_Details
                     {
                         Shipment_Details = t1,
                     }).Where(c => c.Shipment_Details.ID.ToString() == id).SingleOrDefault();
            this.Shipment_Details = v.Shipment_Details;

        }

        public int Add(int userID)
        {
            Shipment_Details.AddReady(userID);
            return Shipment_Details.Add();
        }

        public bool Edit(int userID)
        {
            return Shipment_Details.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Shipment_Details.Delete(userID);
        }

        //public void SelectSinglDetailInfo(int id)
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Shipment_Details
        //             join t2 in db.Shipment_History on t1.Shipment_HistoryFK equals t2.ID
        //             join t3 in db.Common_TheOrder on t2.Common_TheOrderFK equals t3.ID
        //             join t4 in db.Mkt_BOM on t3.ID equals t4.Common_TheOrderFk
        //             join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
        //             where t2.ID == id
        //             select new VmShipment_Details
        //             {
        //                 Shipment_Details = t1,
        //                 Shipment_History = t2,
        //                 Common_TheOrder = t3,
        //                 Mkt_BOM = t4,
        //                 Mkt_Item = t5
        //             }).FirstOrDefault();
           
        //    this.Mkt_BOM = new Mkt_BOM();
        //    this.Mkt_Item = new Mkt_Item();
        //    this.Shipment_Details = new Shipment_Details();
        //    this.Shipment_Details = v.Shipment_Details;
        //    this.Shipment_Details.ItemName = Mkt_Item.Name;
        //    this.Shipment_Details.Style = Mkt_BOM.Style;
           
        //}
    }
}