﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Shipment;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_CNFDelivaryChallanSlave
    {
        private xOssContext db;
        public Shipment_OrderDeliverdSchedule Shipment_OrderDeliverdSchedule { get; set; }
        public Shipment_CNFDelivaryChallan Shipment_CNFDelivaryChallan { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public int? BuyerId { get; set; }
        public int? BOMId { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Common_Supplier Transporter_Common_Supplier { get; set; }
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public VmShipment_CNFDelivaryChallanSlave HeaderData { get; set; }
        public VmShipment_CNFDelivaryChallanSlave HeaderData2 { get; set; }
        public IEnumerable<VmShipment_CNFDelivaryChallanSlave> DataList { get; set; }
        public IEnumerable<VmShipment_CNFDelivaryChallanSlave> InovoicedList { get; set; }
        public IEnumerable<VmShipment_CNFDelivaryChallanSlave> PendingList { get; set; }
        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Shipment_OrderDeliverdSchedule
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryFk equals t4.ID
                     where t1.Shipment_CNFDelivaryChallanFk == id
                     && t1.Active == true
                     && t2.Active == true
                     && t3.Active == true
                     && t4.Active == true

                     select new VmShipment_CNFDelivaryChallanSlave
                     {
                         Shipment_OrderDeliverdSchedule = t1,
                         Mkt_BOM = t2,
                         Common_TheOrder = t3,
                         Mkt_OrderDeliverySchedule = t4

                     }).OrderByDescending(x => x.Shipment_OrderDeliverdSchedule.ID).AsEnumerable();
            var v = (from t1 in db.Shipment_CNFDelivaryChallan
                     join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     join t3 in db.Common_Supplier on t1.Transporter_Common_SupplierFk equals t3.ID

                     select new VmShipment_CNFDelivaryChallanSlave
                     {
                         Shipment_CNFDelivaryChallan = t1,
                         Common_Supplier = t2,
                         Transporter_Common_Supplier = t3
                     }).SingleOrDefault(c => c.Shipment_CNFDelivaryChallan.ID == id);
            this.DataList = a;
            this.HeaderData = v;
        }
        public CnfDelivaryChallanTotal CnfDelivaryChallanSlaveTotalQty(int id)
        {
            InitialDataLoad(id);
            var totalPackQty = DataList.Select(x => (decimal)x.Shipment_OrderDeliverdSchedule.PackQty).DefaultIfEmpty((decimal)0).Sum();
            var deliverdQty = DataList.Select(x => (decimal)x.Shipment_OrderDeliverdSchedule.DeliverdQty).DefaultIfEmpty((decimal)0).Sum();
            return new CnfDelivaryChallanTotal { TotalPackQty = totalPackQty, TotalDeliveredQty = deliverdQty };
        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Shipment_OrderDeliverdSchedule
                     join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                     join t3 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t3.ID
                     select new VmShipment_CNFDelivaryChallanSlave
                     {
                         Shipment_OrderDeliverdSchedule = t1,
                         Mkt_Buyer = t3

                     }).SingleOrDefault(c => c.Shipment_OrderDeliverdSchedule.ID == id);
            this.Shipment_OrderDeliverdSchedule = a.Shipment_OrderDeliverdSchedule;
            this.Mkt_Buyer = a.Mkt_Buyer;
            this.BuyerId = a.Mkt_Buyer.ID;
        }

        public int Add(int userID)
        {

            Shipment_OrderDeliverdSchedule.AddReady(userID);
            return Shipment_OrderDeliverdSchedule.Add();

        }

        public bool Edit(int userID)
        {
            return Shipment_OrderDeliverdSchedule.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Shipment_OrderDeliverdSchedule.Delete(userID);
        }

        public void InitialDataLoadForWithoutInvoice(int id)
        {
            db = new xOssContext();
            var invoicedchallan = (from t5 in db.Commercial_InvoiceSlave
                     join t1 in db.Shipment_OrderDeliverdSchedule on t5.Shipment_OrderDeliverdScheduleFK equals t1.ID
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryFk equals t4.ID
                     where t1.Shipment_CNFDelivaryChallanFk == id
                     && t1.Active == true
                     && t2.Active == true
                     && t3.Active == true
                     && t4.Active == true
                     && t5.Active == true
                     select new VmShipment_CNFDelivaryChallanSlave
                     {
                         Shipment_OrderDeliverdSchedule = t1,
                         Mkt_BOM = t2,
                         Common_TheOrder = t3,
                         Mkt_OrderDeliverySchedule = t4

                     }).OrderByDescending(x => x.Shipment_OrderDeliverdSchedule.ID).ToList();

            var allchallan = (from t1 in db.Shipment_OrderDeliverdSchedule
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryFk equals t4.ID
                     where t1.Shipment_CNFDelivaryChallanFk == id
                     && t1.Active == true
                     && t2.Active == true
                     && t3.Active == true
                     && t4.Active == true

                     select new VmShipment_CNFDelivaryChallanSlave
                     {
                         Shipment_OrderDeliverdSchedule = t1,
                         Mkt_BOM = t2,
                         Common_TheOrder = t3,
                         Mkt_OrderDeliverySchedule = t4

                     }).OrderByDescending(x => x.Shipment_OrderDeliverdSchedule.ID).ToList();


          
            allchallan.RemoveAll(x => invoicedchallan.Exists(y => y.Shipment_OrderDeliverdSchedule.ID == x.Shipment_OrderDeliverdSchedule.ID));// remove list which are in invoice list (N.B: RemoveAll modifies the existing list.)


            var v = (from t1 in db.Shipment_CNFDelivaryChallan
                     join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     join t3 in db.Common_Supplier on t1.Transporter_Common_SupplierFk equals t3.ID

                     select new VmShipment_CNFDelivaryChallanSlave
                     {
                         Shipment_CNFDelivaryChallan = t1,
                         Common_Supplier = t2,
                         Transporter_Common_Supplier = t3
                     }).SingleOrDefault(c => c.Shipment_CNFDelivaryChallan.ID == id);
            this.InovoicedList = invoicedchallan;
            this.PendingList = allchallan;
            this.HeaderData2 = v;
        }
    }
    public class CnfDelivaryChallanTotal
    {
        public decimal TotalPackQty { get; set; }
        public decimal TotalDeliveredQty { get; set; }
    }
}