﻿using Oss.Romo.Models;
using Oss.Romo.Models.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_UnloadingPort
    {
        private xOssContext db;
        public Shipment_UnloadingPort Shipment_UnloadingPort { get; set; }
        public IEnumerable<VmShipment_UnloadingPort> DataList { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_UnloadingPort

                     select new VmShipment_UnloadingPort
                     {
                         Shipment_UnloadingPort = t1,

                     }).Where(x => x.Shipment_UnloadingPort.Active == true).OrderByDescending(x => x.Shipment_UnloadingPort.ID).AsEnumerable();
            this.DataList = v;

        }

        public int Add(int userID)
        {
            Shipment_UnloadingPort.AddReady(userID);
            return Shipment_UnloadingPort.Add();
        }

        public bool Edit(int userID)
        {
            return Shipment_UnloadingPort.Edit(userID);
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Shipment_UnloadingPort
                     select new VmShipment_UnloadingPort
                     {
                         Shipment_UnloadingPort = b

                     }).Where(c => c.Shipment_UnloadingPort.ID == id).SingleOrDefault();
            this.Shipment_UnloadingPort = a.Shipment_UnloadingPort;
        }
    }
}