﻿using Oss.Romo.Models;
using Oss.Romo.Models.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_PortOfDischarge
    {
        private xOssContext db;
        public Shipment_PortOfDischarge Shipment_PortOfDischarge { get; set; }
        public IEnumerable<VmShipment_PortOfDischarge> DataList { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_PortOfDischarge

                     select new VmShipment_PortOfDischarge
                     {
                         Shipment_PortOfDischarge = t1,

                     }).Where(x => x.Shipment_PortOfDischarge.Active == true).OrderByDescending(x => x.Shipment_PortOfDischarge.ID).AsEnumerable();
            this.DataList = v;

        }

        public int Add(int userID)
        {
            Shipment_PortOfDischarge.AddReady(userID);
            return Shipment_PortOfDischarge.Add();
        }

        public bool Edit(int userID)
        {
            return Shipment_PortOfDischarge.Edit(userID);
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Shipment_PortOfDischarge
                     select new VmShipment_PortOfDischarge
                     {
                         Shipment_PortOfDischarge = b

                     }).Where(c => c.Shipment_PortOfDischarge.ID == id).SingleOrDefault();
            this.Shipment_PortOfDischarge = a.Shipment_PortOfDischarge;
        }
    }
}