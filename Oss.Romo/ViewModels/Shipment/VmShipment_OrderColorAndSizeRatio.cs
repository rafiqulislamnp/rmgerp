﻿using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Shipment;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_OrderColorAndSizeRatio
    {
        private xOssContext db;
        public Shipment_OrderColorAndSizeRatio Shipment_OrderColorAndSizeRatio { get; set; }
        public Mkt_OrderColorAndSizeRatio Mkt_OrderColorAndSizeRatio { get; set; }
        public IEnumerable<VmShipment_OrderColorAndSizeRatio> DataList { get; set; }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_OrderColorAndSizeRatio
                     join t2 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderDeliveryFk equals t2.ID
                     where t1.Common_TheOrderFk == id && t2.Common_TheOrderFk == id
                     select new VmShipment_OrderColorAndSizeRatio
                     {
                         Shipment_OrderColorAndSizeRatio = t1,
                         Mkt_OrderColorAndSizeRatio = t2,

                     }).Where(x => x.Shipment_OrderColorAndSizeRatio.Active == true).OrderByDescending(x => x.Shipment_OrderColorAndSizeRatio.ID).AsEnumerable();
            this.DataList = v;
        }

        public int Add(int userID)
        {
            Shipment_OrderColorAndSizeRatio.AddReady(userID);
            return Shipment_OrderColorAndSizeRatio.Add();
        }
        public bool Edit(int userID)
        {
            return Shipment_OrderColorAndSizeRatio.Edit(userID);
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Shipment_OrderColorAndSizeRatio
                     select new VmShipment_OrderColorAndSizeRatio
                     {
                         Shipment_OrderColorAndSizeRatio = b

                     }).Where(c => c.Shipment_OrderColorAndSizeRatio.ID == id).SingleOrDefault();
            this.Shipment_OrderColorAndSizeRatio = a.Shipment_OrderColorAndSizeRatio;
        }
        public string GetOrderColorAndSizeDeliveryRequiredQty(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderColorAndSizeRatio
                     where t1.ID == id && t1.Active == true
                     select t1.Quantity).Sum();
            return v.ToString();
        }

        public string CheckDeliverdColorAndSizeQty(int id)
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_OrderColorAndSizeRatio
                     where t1.ID == id && t1.Active == true
                     select t1.Quantity).Sum();

            var v1 = (from t1 in db.Shipment_OrderColorAndSizeRatio
                     where t1.Mkt_OrderDeliveryFk == id && t1.Active == true
                     select t1.Quantity).ToList();

            decimal val = 0;
            if (v1.Count > 0)
            {
                val = Convert.ToDecimal(v1.Sum());
            }

            var result = (Convert.ToDecimal(v) - val);

            return result.ToString();
        }
        public string CheckDeliverdColorAndSizeQtyForEdit(int id, int editId)
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_OrderColorAndSizeRatio
                     where t1.ID == id && t1.Active == true
                     select t1.Quantity).Sum();

            var v1 = (from t1 in db.Shipment_OrderColorAndSizeRatio
                      where t1.Mkt_OrderDeliveryFk == id && t1.ID != editId && t1.Active == true
                      select t1.Quantity).ToList();

            decimal val = 0;
            if (v1.Count > 0)
            {
                val = Convert.ToDecimal(v1.Sum());
            }

            var result = (Convert.ToDecimal(v) - val);

            return result.ToString();
        }
        

        public DataTable GetShipmentColorAndSizeRatio(int id)
        {
            db = new xOssContext();

            DataTable dt = new DataTable();
            var data = db.Shipment_OrderColorAndSizeRatio.Distinct().ToList();

            var d = (from t1 in data
                     join t2 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderDeliveryFk equals t2.ID
                     where t1.Active == true && t1.Common_TheOrderFk == id
                     group new { t2,t1 } by new { t2.Color }
                         into Group
                     where Group.Count() > 0
                     select new
                     {
                         Group.Key.Color,
                         ColumnName = Group.GroupBy(t2 => t2.t2.Size).Select
                         (m => new { Sub = m.Key, Score = m.Sum(c => c.t1.Quantity) })
                     }).ToList();

            var sub = (from f in db.Mkt_OrderColorAndSizeRatio.AsEnumerable()
                       join t2 in db.Shipment_OrderColorAndSizeRatio on f.ID equals t2.Mkt_OrderDeliveryFk
                       where f.Active == true && f.Common_TheOrderFk == id
                       select new
                       {
                           f.Size
                       }).Distinct().ToList();

            ArrayList objDataColumn = new ArrayList();

            if (data.Count() > 0)
            {
                objDataColumn.Add("Color");
                for (int i = 0; i < sub.Count; i++)
                {
                    objDataColumn.Add(sub[i].Size);
                }
            }

            //Add column name dynamically
            for (int i = 0; i < objDataColumn.Count; i++)
            {
                dt.Columns.Add(objDataColumn[i].ToString());
            }

            //Add data into datatable
            for (int i = 0; i < d.Count; i++)
            {
                List<string> datalist = new List<string>();
                datalist.Add(d[i].Color.ToString());

                var res = d[i].ColumnName.ToList();
                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;

                    for (int j = 0; j < res.Count; j++)
                    {
                        string column = dt.Columns[m].ToString();

                        if (res[j].Sub == column)
                        {
                            datalist.Add(res[j].Score.ToString());
                            count++;
                        }
                    }

                    if (count == 0)
                    {
                        string val = "0";
                        datalist.Add(val);
                    }
                }

                dt.Rows.Add(datalist.ToArray<string>());

            }
            return dt;
        }
    }
}