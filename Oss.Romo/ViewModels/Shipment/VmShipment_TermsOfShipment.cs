﻿using Oss.Romo.Models;
using Oss.Romo.Models.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_TermsOfShipment
    {
        private xOssContext db;
        public Shipment_TermsOfShipment Shipment_TermsOfShipment { get; set; }
        public IEnumerable<VmShipment_TermsOfShipment> DataList { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_TermsOfShipment

                     select new VmShipment_TermsOfShipment
                     {
                         Shipment_TermsOfShipment = t1,

                     }).Where(x => x.Shipment_TermsOfShipment.Active == true).OrderByDescending(x => x.Shipment_TermsOfShipment.ID).AsEnumerable();
            this.DataList = v;

        }

        public int Add(int userID)
        {
            Shipment_TermsOfShipment.AddReady(userID);
            return Shipment_TermsOfShipment.Add();
        }

        public bool Edit(int userID)
        {
            return Shipment_TermsOfShipment.Edit(userID);
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Shipment_TermsOfShipment
                     select new VmShipment_TermsOfShipment
                     {
                         Shipment_TermsOfShipment = b

                     }).Where(c => c.Shipment_TermsOfShipment.ID == id).SingleOrDefault();
            this.Shipment_TermsOfShipment = a.Shipment_TermsOfShipment;
        }
    }
}