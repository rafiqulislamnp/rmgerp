﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Shipment;
using Oss.Romo.Models.User;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_CNFDelivaryChallan
    {
        private xOssContext db;
        public Shipment_UnloadingPort Shipment_UnloadingPort { get; set; }
        public Shipment_CNFDelivaryChallan Shipment_CNFDelivaryChallan { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Common_Supplier Transporter_Common_Supplier { get; set; }
        public Shipment_OrderDeliverdSchedule Shipment_OrderDeliverdSchedule { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public IEnumerable<VmShipment_CNFDelivaryChallan> DataList { get; set; }
        public IEnumerable<VmShipment_CNFDelivaryChallan> DataList1 { get; set; }
        public IEnumerable<VmShipment_CNFDelivaryChallan> DataList2 { get; set; }
        public IEnumerable<ChallanView> ChallanList { get; set; }
        public IEnumerable<ChallanView> ChallanList2 { get; set; }
        public VmShipment_CNFDelivaryChallan ChallanHeader { get; set; }
        public int? TotalPackQty { get; set; }
        public int TotalChallanItem { get; set; }
        public decimal? TotalDeliveredQty { get; set; }
        public decimal? TotalCtnQty { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public User_User User_User { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var VmShipment_CNFDelivaryChallanSlave = new VmShipment_CNFDelivaryChallanSlave();
            var a = (from t1 in db.Shipment_CNFDelivaryChallan
                     join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     join t3 in db.Common_Supplier on t1.Transporter_Common_SupplierFk equals t3.ID
                     join t4 in db.User_User on t1.FirstCreatedBy equals t4.ID
                     where t1.Active == true // && t1.FirstCreatedBy == this.User_User.ID
                     select new VmShipment_CNFDelivaryChallan
                     {
                         Shipment_CNFDelivaryChallan = t1,
                         Common_Supplier = t2,
                         Transporter_Common_Supplier = t3,
                         User_User = t4,
                         TotalPackQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                         join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                         join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                         join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                         where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                         && t1.Active == true
                                         && t2.Active == true
                                         && t3.Active == true
                                         && t4.Active == true
                                         select new
                                         {
                                             t4.PackQty
                                         }).Sum(x => x.PackQty),
                         TotalDeliveredQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                              join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                              join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                              join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                              where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                              && t1.Active == true
                                              && t2.Active == true
                                              && t3.Active == true
                                              && t4.Active == true
                                              select new
                                              {
                                                  t4.DeliverdQty
                                              }).Sum(x => x.DeliverdQty),
                         TotalCtnQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                        join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                        join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                        join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                        where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                        && t1.Active == true
                                        && t2.Active == true
                                        && t3.Active == true
                                        && t4.Active == true
                                        select new
                                        {
                                            t4.CtnQty
                                        }).Sum(x => x.CtnQty)


                     }).OrderByDescending(x => x.Shipment_CNFDelivaryChallan.ID).AsEnumerable();

            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 2)
            {
                this.DataList = a.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = a;
            }
            
        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Shipment_CNFDelivaryChallan in db.Shipment_CNFDelivaryChallan
                     select new VmShipment_CNFDelivaryChallan
                     {
                         Shipment_CNFDelivaryChallan = Shipment_CNFDelivaryChallan
                     }).Where(c => c.Shipment_CNFDelivaryChallan.ID == id).SingleOrDefault();
            this.Shipment_CNFDelivaryChallan = v.Shipment_CNFDelivaryChallan;

        }

        public int Add(int userID)
        {

            Shipment_CNFDelivaryChallan.AddReady(userID);
            return Shipment_CNFDelivaryChallan.Add();

        }

        public bool Edit(int userID)
        {
            return Shipment_CNFDelivaryChallan.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Shipment_CNFDelivaryChallan.Delete(userID);
        }

        public void CNFDelivaryChallan(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Shipment_CNFDelivaryChallan
                     join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     join t3 in db.Common_Supplier on t1.Transporter_Common_SupplierFk equals t3.ID
                     join t4 in db.Shipment_UnloadingPort on t1.ShipmentUnloadingPortFk equals t4.ID
                     where t1.ID == id &&
                           t1.Active == true &&
                           t2.Active == true &&
                           t3.Active == true &&
                           t4.Active == true
                     select new VmShipment_CNFDelivaryChallan
                     {
                         Shipment_CNFDelivaryChallan = t1,
                         Common_Supplier = t2,
                         Transporter_Common_Supplier = t3,
                         Shipment_UnloadingPort = t4
                     }).Where(x => x.Shipment_CNFDelivaryChallan.Active == true).OrderByDescending(x => x.Shipment_CNFDelivaryChallan.ID).FirstOrDefault();

            this.ChallanHeader = a;

            var b = (
                from t1 in db.Shipment_OrderDeliverdSchedule
                join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                join t4 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryFk equals t4.ID
                where t1.Shipment_CNFDelivaryChallanFk == id && t1.Active == true &&
                     t2.Active == true &&
                     t3.Active == true &&
                     t4.Active == true


                select new VmShipment_CNFDelivaryChallan
                {
                    Shipment_OrderDeliverdSchedule = t1,
                    Mkt_BOM = t2,
                    Common_TheOrder = t3,
                    Mkt_OrderDeliverySchedule = t4,
                }).ToList();
            this.DataList = b;
        }
        public void InvoicedDeliveryChallan()
        {
            db = new xOssContext();
            //var a = (from t1 in db.Commercial_InvoiceSlave
            //         join t2 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t2.ID
            //         join t3 in db.Common_Supplier on t2.Common_SupplierFk equals t3.ID
            //         join t4 in db.Common_Supplier on t2.Transporter_Common_SupplierFk equals t4.ID
            //         where t1.Active == true &&
            //               t2.Active == true &&
            //               t3.Active == true &&
            //               t4.Active == true
            //         select new VmShipment_CNFDelivaryChallan
            //         {
            //             Shipment_CNFDelivaryChallan = t2,
            //             Common_Supplier = t3,
            //             Transporter_Common_Supplier = t4
            //         }).OrderByDescending(x => x.Shipment_CNFDelivaryChallan.ID).Distinct();
            //this.DataList1 = a;

            var allChallan = (from t1 in db.Shipment_CNFDelivaryChallan
                              join t4 in db.Shipment_OrderDeliverdSchedule on t1.ID equals t4.Shipment_CNFDelivaryChallanFk
                              join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                              join t3 in db.Common_Supplier on t1.Transporter_Common_SupplierFk equals t3.ID
                              where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                              orderby t1.ID descending
                              select new
                              {
                                  ChallanName = t1.ChallanNo,
                                  ChallanDate = t1.Date,
                                  CommonSupplierName = t2.Name,
                                  TransporterSupplierName = t3.Name,
                                  ShipmentOrderDeliverdSchedule = t4.Shipment_CNFDelivaryChallanFk,
                                  TotalPackQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                  join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                  join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                  join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                  where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                  && t1.Active == true
                                                  && t2.Active == true
                                                  && t3.Active == true
                                                  && t4.Active == true
                                                  select new
                                                  {
                                                      t4.PackQty
                                                  }).Sum(x => x.PackQty),
                                  TotalDeliveredQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                       join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                       join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                       join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                       where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                       && t1.Active == true
                                                       && t2.Active == true
                                                       && t3.Active == true
                                                       && t4.Active == true
                                                       select new
                                                       {
                                                           t4.DeliverdQty
                                                       }).Sum(x => x.DeliverdQty),
                                  TotalCtnQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                 join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                 join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                 join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                 where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                 && t1.Active == true
                                                 && t2.Active == true
                                                 && t3.Active == true
                                                 && t4.Active == true
                                                 select new
                                                 {
                                                     t4.CtnQty
                                                 }).Sum(x => x.CtnQty)
                              } into grp
                              group grp by new
                              {
                                  grp.ChallanName,
                                  grp.ChallanDate,
                                  grp.CommonSupplierName,
                                  grp.TransporterSupplierName,
                                  grp.ShipmentOrderDeliverdSchedule,
                                  grp.TotalPackQty,
                                  grp.TotalDeliveredQty,
                                  grp.TotalCtnQty
                              } into g
                              select new
                              {
                                  ChallanName = g.Key.ChallanName,
                                  ChallanDate = g.Key.ChallanDate,
                                  CommonSupplierName = g.Key.CommonSupplierName,
                                  TransporterSupplierName = g.Key.TransporterSupplierName,
                                  TotalPackQty = g.Key.TotalPackQty,
                                  TotalDeliveredQty = g.Key.TotalDeliveredQty,
                                  TotalCtnQty = g.Key.TotalCtnQty,
                                  Count = g.Select(x => x.ShipmentOrderDeliverdSchedule).Count()
                              }
                             ).ToList();

            var invoicedChllan = (from t1 in db.Shipment_CNFDelivaryChallan
                                  join t4 in db.Commercial_InvoiceSlave on t1.ID equals t4.Shipment_CNFDelivaryChallanFk
                                  join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                                  join t3 in db.Common_Supplier on t1.Transporter_Common_SupplierFk equals t3.ID
                                  where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                                  orderby t1.ID descending
                                  select new
                                  {
                                      ChallanName = t1.ChallanNo,
                                      ChallanDate = t1.Date,
                                      CommonSupplierName = t2.Name,
                                      TransporterSupplierName = t3.Name,
                                      ShipmentOrderDeliverdSchedule = t4.Shipment_CNFDelivaryChallanFk,
                                      TotalPackQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                      join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                      join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                      join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                      where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                      && t1.Active == true
                                                      && t2.Active == true
                                                      && t3.Active == true
                                                      && t4.Active == true
                                                      select new
                                                      {
                                                          t4.PackQty
                                                      }).Sum(x => x.PackQty),
                                      TotalDeliveredQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                           join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                           join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                           join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                           where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                           && t1.Active == true
                                                           && t2.Active == true
                                                           && t3.Active == true
                                                           && t4.Active == true
                                                           select new
                                                           {
                                                               t4.DeliverdQty
                                                           }).Sum(x => x.DeliverdQty),
                                      TotalCtnQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                     join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                     where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                     && t1.Active == true
                                                     && t2.Active == true
                                                     && t3.Active == true
                                                     && t4.Active == true
                                                     select new
                                                     {
                                                         t4.CtnQty
                                                     }).Sum(x => x.CtnQty)
                                  } into grp
                                  group grp by new
                                  {
                                      grp.ChallanName,
                                      grp.ChallanDate,
                                      grp.CommonSupplierName,
                                      grp.TransporterSupplierName,
                                      grp.ShipmentOrderDeliverdSchedule,
                                      grp.TotalPackQty,
                                      grp.TotalDeliveredQty,
                                      grp.TotalCtnQty
                                  } into g
                                  select new
                                  {
                                      ChallanName = g.Key.ChallanName,
                                      ChallanDate = g.Key.ChallanDate,
                                      CommonSupplierName = g.Key.CommonSupplierName,
                                      TransporterSupplierName = g.Key.TransporterSupplierName,
                                      TotalPackQty = g.Key.TotalPackQty,
                                      TotalDeliveredQty = g.Key.TotalDeliveredQty,
                                      TotalCtnQty = g.Key.TotalCtnQty,
                                      Count = g.Select(x => x.ShipmentOrderDeliverdSchedule).Count()
                                  }
                              ).ToList();
            //var data = allChallan.Intersect(invoicedChllan);

            var d = (from t1 in allChallan.Intersect(invoicedChllan)
                     select new ChallanView
                     {
                         ChallanName = t1.ChallanName,
                         ChallanDate = t1.ChallanDate,
                         CommonSupplierName = t1.CommonSupplierName,
                         TransporterSupplierName = t1.TransporterSupplierName,
                         Count = t1.Count,
                         TotalCtnQty = t1.TotalCtnQty,
                         TotalDeliveredQty = t1.TotalDeliveredQty,
                         TotalPackQty = t1.TotalPackQty
                     }).ToList();
            ChallanList2 = d;
        }

        public void WithoutInvoicedDeliveryChallan()
        {
            db = new xOssContext();
            var allChallan = (from t1 in db.Shipment_CNFDelivaryChallan
                              join t4 in db.Shipment_OrderDeliverdSchedule on t1.ID equals t4.Shipment_CNFDelivaryChallanFk
                              join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                              join t3 in db.Common_Supplier on t1.Transporter_Common_SupplierFk equals t3.ID
                              where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                              orderby t1.ID descending
                              select new
                              {
                                  ChallanName = t1.ChallanNo,
                                  ChallanDate = t1.Date,
                                  CommonSupplierName = t2.Name,
                                  TransporterSupplierName = t3.Name,
                                  ShipmentOrderDeliverdSchedule = t4.Shipment_CNFDelivaryChallanFk,
                                  TotalPackQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                  join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                  join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                  join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                  where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                  && t4.Active == true
                                                      && t5.Active == true
                                                      && t6.Active == true
                                                      && t7.Active == true
                                                  select new
                                                  {
                                                      t4.PackQty
                                                  }).Sum(x => x.PackQty),
                                  TotalDeliveredQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                       join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                       join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                       join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                       where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                       && t4.Active == true
                                                      && t5.Active == true
                                                      && t6.Active == true
                                                      && t7.Active == true
                                                       select new
                                                       {
                                                           t4.DeliverdQty
                                                       }).Sum(x => x.DeliverdQty),
                                  TotalCtnQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                 join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                 join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                 join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                 where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                  && t4.Active == true
                                                      && t5.Active == true
                                                      && t6.Active == true
                                                      && t7.Active == true
                                                 select new
                                                 {
                                                     t4.CtnQty
                                                 }).Sum(x => x.CtnQty)
                              } into grp
                              group grp by new
                              {
                                  grp.ChallanName,
                                  grp.ChallanDate,
                                  grp.CommonSupplierName,
                                  grp.TransporterSupplierName,
                                  grp.ShipmentOrderDeliverdSchedule,
                                  grp.TotalPackQty,
                                  grp.TotalDeliveredQty,
                                  grp.TotalCtnQty
                              } into g
                              select new
                              {
                                  ChallanName = g.Key.ChallanName,
                                  ChallanDate = g.Key.ChallanDate,
                                  CommonSupplierName = g.Key.CommonSupplierName,
                                  TransporterSupplierName = g.Key.TransporterSupplierName,
                                  TotalPackQty = g.Key.TotalPackQty,
                                  TotalDeliveredQty = g.Key.TotalDeliveredQty,
                                  TotalCtnQty = g.Key.TotalCtnQty,
                                  Count = g.Select(x => x.ShipmentOrderDeliverdSchedule).Count(),
                                  DelivaryChallanId = g.Key.ShipmentOrderDeliverdSchedule
                              }
                              ).ToList();


            //var invoicedChllan = (from t1 in db.Commercial_InvoiceSlave
            //                      join t2 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t2.ID
            //                      join t3 in db.Common_Supplier on t2.Common_SupplierFk equals t3.ID
            //                      join t4 in db.Common_Supplier on t2.Transporter_Common_SupplierFk equals t4.ID
            //                      where t1.Active == true &&
            //                            t2.Active == true &&
            //                            t3.Active == true &&
            //                            t4.Active == true
            //                      select new VmShipment_CNFDelivaryChallan
            //                      {
            //                          Shipment_CNFDelivaryChallan = t2,
            //                          Common_Supplier = t3,
            //                          Transporter_Common_Supplier = t4
            //                      }).OrderByDescending(x => x.Shipment_CNFDelivaryChallan.ID).Distinct().ToList();

            var invoicedChllan = (from t1 in db.Shipment_CNFDelivaryChallan
                                  join t4 in db.Commercial_InvoiceSlave on t1.ID equals t4.Shipment_CNFDelivaryChallanFk
                                  join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                                  join t3 in db.Common_Supplier on t1.Transporter_Common_SupplierFk equals t3.ID
                                  where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                                  orderby t1.ID descending
                                  select new
                                  {
                                      ChallanName = t1.ChallanNo,
                                      ChallanDate = t1.Date,
                                      CommonSupplierName = t2.Name,
                                      TransporterSupplierName = t3.Name,
                                      ShipmentOrderDeliverdSchedule = t4.Shipment_CNFDelivaryChallanFk,
                                      TotalPackQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                      join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                      join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                      join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                      where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                      && t4.Active == true
                                                      && t5.Active == true
                                                      && t6.Active == true
                                                      && t7.Active == true
                                                      select new
                                                      {
                                                          t4.PackQty
                                                      }).Sum(x => x.PackQty),
                                      TotalDeliveredQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                           join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                           join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                           join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                           where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                           && t4.Active == true
                                                      && t5.Active == true
                                                      && t6.Active == true
                                                      && t7.Active == true
                                                           select new
                                                           {
                                                               t4.DeliverdQty
                                                           }).Sum(x => x.DeliverdQty),
                                      TotalCtnQty = (from t4 in db.Shipment_OrderDeliverdSchedule
                                                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                                                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                                                     join t7 in db.Mkt_OrderDeliverySchedule on t4.Mkt_OrderDeliveryFk equals t7.ID
                                                     where t4.Shipment_CNFDelivaryChallanFk == t1.ID
                                                     && t4.Active == true
                                                      && t5.Active == true
                                                      && t6.Active == true
                                                      && t7.Active == true
                                                     select new
                                                     {
                                                         t4.CtnQty
                                                     }).Sum(x => x.CtnQty)

                                  } into grp
                                  group grp by new
                                  {
                                      grp.ChallanName,
                                      grp.ChallanDate,
                                      grp.CommonSupplierName,
                                      grp.TransporterSupplierName,
                                      grp.ShipmentOrderDeliverdSchedule,
                                      grp.TotalPackQty,
                                      grp.TotalDeliveredQty,
                                      grp.TotalCtnQty
                                  } into g
                                  select new
                                  {
                                      ChallanName = g.Key.ChallanName,
                                      ChallanDate = g.Key.ChallanDate,
                                      CommonSupplierName = g.Key.CommonSupplierName,
                                      TransporterSupplierName = g.Key.TransporterSupplierName,
                                      TotalPackQty = g.Key.TotalPackQty,
                                      TotalDeliveredQty = g.Key.TotalDeliveredQty,
                                      TotalCtnQty = g.Key.TotalCtnQty,
                                      Count = g.Select(x => x.ShipmentOrderDeliverdSchedule).Count(),
                                      DelivaryChallanId = g.Key.ShipmentOrderDeliverdSchedule
                                  }
                              ).ToList();
            //  var data = allChallan.Except(invoicedChllan);




            var s = (from t1 in allChallan.Except(invoicedChllan)
                     select new ChallanView
                     {
                         ChallanName = t1.ChallanName,
                         ChallanDate = t1.ChallanDate,
                         CommonSupplierName = t1.CommonSupplierName,
                         TransporterSupplierName = t1.TransporterSupplierName,
                         TotalCtnQty = t1.TotalCtnQty,
                         TotalPackQty = t1.TotalPackQty,
                         TotalDeliveredQty = t1.TotalDeliveredQty,
                         Count = t1.Count,
                         DelivaryChallanId = t1.DelivaryChallanId,
                         InvoicedCtnQty = (from t2 in db.Shipment_OrderDeliverdSchedule
                                           join t3 in db.Commercial_InvoiceSlave on t2.ID equals t3.Shipment_OrderDeliverdScheduleFK
                                           join t4 in db.Shipment_CNFDelivaryChallan on t3.Shipment_CNFDelivaryChallanFk equals t4.ID
                                           where t4.ID == t1.DelivaryChallanId && t2.Active == true && t3.Active == true && t4.Active == true
                                           select t2.CtnQty).DefaultIfEmpty(0).Sum()
                     }).ToList();
            ChallanList = s;

            //var data2= allChallan.Except(data);
            //var data23 = allChallan.Intersect(invoicedChllan);
            //ChallanList = data;
            //var data = allChallan.Where(x =>
            //   invoicedChllan.All(y => y.Count != x.Count));
            //this.DataList2 = data;
        }
    }
}

public class ChallanView
{
    public string ChallanName { get; set; }
    public DateTime ChallanDate { get; set; }
    public string CommonSupplierName { get; set; }
    public string TransporterSupplierName { get; set; }
    public int Count { get; set; }
    public int TotalPackQty { get; set; }
    public decimal TotalDeliveredQty { get; set; }
    public int TotalCtnQty { get; set; }
    public int DelivaryChallanId { get; set; }
    public int InvoicedCtnQty { get; set; }

}
