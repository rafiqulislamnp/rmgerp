﻿using Oss.Romo.Models;
using Oss.Romo.Models.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_Alert
    {
        private xOssContext db;
        public Alert Alert { get; set; }
        public IEnumerable<VmShipment_Alert> DataList { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Alert

                     select new VmShipment_Alert
                     {
                         Alert = t1,

                     }).OrderByDescending(x => x.Alert.ID).AsEnumerable();
            this.DataList = v;
        }
        public bool Edit(int userID)
        {
            return Alert.Edit(userID);
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Alert
                     select new VmShipment_Alert
                     {
                         Alert = b

                     }).Where(c => c.Alert.ID == id).SingleOrDefault();
            this.Alert = a.Alert;
        }
    }
}