﻿using Oss.Romo.Models;
using Oss.Romo.Models.Shipment;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Commercial;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Common;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_BillOfExchange
    {
        private xOssContext db;
        public string Name { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }

        public IEnumerable<VmShipment_BillOfExchange> DataList { get; set; }
        public List<ReportDocType> BillOfExchage { get; set; }
        public List<ReportDocType> packingList { get; set; }
        public Common_Country Common_Country { get; set; }
        public Common_Country CommonCountryOfOrigin { get; set; }
        public Shipment_TermsOfShipment Shipment_TermsOfShipment { get; set; }
        public Shipment_PortOfLoading Shipment_PortOfLoading { get; set; }
        public Shipment_PortOfDischarge Shipment_PortOfDischarge { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Shipment_OrderDeliverdSchedule Shipment_OrderDeliverdSchedule { get; set; }
        public Shipment_CNFDelivaryChallan Shipment_CNFDelivaryChallan { get; set; }
        public Commercial_InvoiceSlave Commercial_InvoiceSlave { get; set; }
        public Commercial_BillOfExchangeSlave Commercial_BillOfExchangeSlave { get; set; }
        public Commercial_BillOfExchange Commercial_BillOfExchange { get; set; }
        public Commercial_Invoice Commercial_Invoice { get; set; }
        public decimal TotalInvoiceValue { get; set; }
        public string InvoiceID { get; set; }


        public void ShipmentBillOfExchange(int id)
        {
            db = new xOssContext();

            var a = (from t1 in db.Commercial_BillOfExchangeSlave
                     join t2 in db.Commercial_BillOfExchange on t1.Commercial_BillOfExchangeFk equals t2.ID
                     join t3 in db.Commercial_Invoice on t1.Commercial_InvoiceFK equals t3.ID
                     where t1.Commercial_BillOfExchangeFk == id
                     select new VmShipment_BillOfExchange
                     {
                         Commercial_BillOfExchangeSlave = t1,
                         Commercial_BillOfExchange = t2,
                         Commercial_Invoice = t3
                     }).Where(x => x.Commercial_BillOfExchangeSlave.Active == true).OrderByDescending(x => x.Commercial_BillOfExchangeSlave.ID).AsEnumerable();

            List<ReportDocType> reportDocTypeList = new List<ReportDocType>();
            foreach (VmShipment_BillOfExchange x in a)
            {
                VmCommercial_BillOfExchangeSlave vmCommercialBillOfExchangeSlave = new VmCommercial_BillOfExchangeSlave();
                ReportDocType reportDocType = new ReportDocType();
                reportDocType.Body1 = x.Commercial_Invoice.InvoiceNo;
               

                reportDocType.Body2 = vmCommercialBillOfExchangeSlave.GetInvoiceValue(x.Commercial_Invoice.ID).ToString();
                reportDocType.Body3 = "US$";
                reportDocType.Body4 = GetMasterLCNo(x.Commercial_BillOfExchange.Commercial_MasterLCFK);
                GetBuyerBankNo(x.Commercial_BillOfExchange.Commercial_MasterLCFK);
                reportDocType.Body5 = this.Name;
                reportDocType.Body9 = this.Address + " " + this.Country;
                GetLienBankNo(x.Commercial_BillOfExchange.Commercial_MasterLCFK);
                reportDocType.Body6 = this.Name;
                reportDocType.Body10 = this.Address + " " + this.Country;
                reportDocType.Body7 = GetMasterLCDate(x.Commercial_BillOfExchange.Commercial_MasterLCFK);
                reportDocType.Body8 = x.Commercial_Invoice.InvoiceDate.ToString();
                reportDocType.Body11 = x.Commercial_BillOfExchange.ReceivedType;
                reportDocType.int1 = x.Commercial_Invoice.ID;
                reportDocType.Body12 = x.Commercial_BillOfExchange.Description;
                reportDocTypeList.Add(reportDocType);

            }
            List<ReportDocType> list = new List<ReportDocType>();

            decimal TTL = 0;

            decimal totalAmount = 0;

            foreach (ReportDocType v in reportDocTypeList)
            {
                decimal body2 = 0;
                decimal.TryParse(v.Body2, out body2);
                TTL += body2;

                totalAmount = Convert.ToDecimal(v.Body2);
                v.Body2 = totalAmount.ToString("F");

                Common.VmForDropDown ntw = new Common.VmForDropDown();
                v.Body8 = v.Body8.Substring(0, v.Body8.Length - 8);
                v.Body7 = v.Body7.Substring(0, v.Body7.Length - 8);
                v.Body11 = v.Body11 + ": " + v.Body4 + ".  Date: " + v.Body7;
                v.SubBody2 = ntw.NumberToWords(TTL, Common.CurrencyType.USD) + ".";
                v.SubBody1 = v.Body3 + TTL.ToString("F");

                list.Add(v);
            }


            foreach (var r in list)
            {

                r.SubBody4 = r.Body3 + TTL.ToString("F");

            }

            #region deedcode
            //foreach (var x in list)
            //{
            //    x.SubBody1 = "US$" + TTL.ToString("F");
            //}
            //var a = (from t1 in db.Commercial_BillOfExchange
            //         join t2 in db.Commercial_BillOfExchangeSlave on t1.ID equals t2.Commercial_BillOfExchangeFk
            //         join t10 in db.Commercial_Invoice on t2.Commercial_InvoiceFK equals t10.ID
            //         join t11 in db.Commercial_InvoiceSlave on t10.ID equals t11.Commercial_InvoiceFk
            //         join t3 in db.Shipment_OrderDeliverdSchedule on t11.Shipment_OrderDeliverdScheduleFK equals t3.ID
            //         join t4 in db.Mkt_OrderDeliverySchedule on t3.Mkt_OrderDeliveryFk equals t4.ID
            //         join t5 in db.Mkt_BOM on t4.Mkt_BOMFk equals t5.ID
            //         join t6 in db.Commercial_MasterLCBuyerPO on t5.ID equals t6.MKTBOMFK
            //         join t7 in db.Commercial_MasterLC on t6.Commercial_MasterLCFK equals t7.ID
            //         join t8 in db.Commercial_Bank on t7.Commercial_BuyerBankFK equals t8.ID
            //         join t9 in db.Commercial_Bank on t7.Commercial_LienBankFK equals t9.ID
            //         where t1.Active == true && t2.Active == true && t10.Active == true && t11.Active == true && t3.Active == true && t4.Active == true && t5.Active == true
            //         && t1.ID == id

            //         select new ReportDocType
            //         {

            //             Body1 = t10.InvoiceNo,
            //             Body8 = t10.InvoiceDate.ToString(),
            //             Body2 = (t3.DeliverdQty * t5.UnitPrice).ToString(),
            //             Body3 = "US$",
            //             Body4 = t7.Name, //master lc name
            //             Body5 = t8.Name + "<br />" + t8.Address, //Buyer bank
            //             Body6 = t9.Name + "<br />" + t9.Address, // Lien bank
            //             Body7 = t7.Date.ToString(),
            //             //Body9 = t8.Address,  //Buyer bank address
            //             //Body10 = t9.Address,  // Lien bank address
            //             Body11 = t1.ReceivedType.ToString(),
            //             int1 = t2.Commercial_InvoiceFK
            //         }).OrderBy(x => x.int1).ToList();

            //List<ReportDocType> list = new List<ReportDocType>();

            //decimal TTL = 0;
            //decimal totalAmount = 0;

            //foreach (ReportDocType v in a)
            //{
            //    decimal body2 = 0;
            //    decimal.TryParse(v.Body2, out body2);
            //    TTL += body2;

            //    totalAmount = Convert.ToDecimal(v.Body2);
            //    v.Body2 = totalAmount.ToString("F");

            //    Common.VmForDropDown ntw = new Common.VmForDropDown();
            //    v.Body8 = v.Body8.Substring(0, v.Body8.Length - 8);
            //    v.SubBody2 = ntw.NumberToWords(TTL, Common.CurrencyType.USD) + ".";
            //    v.SubBody1 = v.Body3 + TTL.ToString("F");
            //    list.Add(v);

            //}
            //foreach (var x in list)
            //{
            //    x.SubBody1 = "US$" + TTL.ToString("F");
            //}
            #endregion

            this.BillOfExchage = list;
        }

        private void GetLienBankNo(int commercialMasterLcfk)
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_MasterLC
                     join t8 in db.Commercial_Bank on t1.Commercial_BuyerBankFK equals t8.ID
                     join t9 in db.Common_Country on t8.Common_CountryFK equals t9.ID
                     where t1.ID == commercialMasterLcfk
                     select new
                     {
                         Name = t8.Name,
                         Address = t8.Address,
                         Country = t9.Name
                     }).FirstOrDefault();
            this.Name = a.Name;
            this.Address = a.Address;
            this.Country = a.Country;


        }

        private void GetBuyerBankNo(int commercialMasterLcfk)
        {
            db = new xOssContext();
            var firstOrDefault = (from t1 in db.Commercial_MasterLC
                                  join t9 in db.Commercial_Bank on t1.Commercial_LienBankFK equals t9.ID
                                  join t2 in db.Common_Country on t9.Common_CountryFK equals t2.ID

                                  where t1.ID == commercialMasterLcfk
                                  select new
                                  {

                                      Name = t9.Name,
                                      Address = t9.Address,
                                      Country = t2.Name

                                  }).FirstOrDefault();


            this.Name = firstOrDefault.Name;
            this.Address = firstOrDefault.Address;
            this.Country = firstOrDefault.Country;
            //var lCDate = "";
            //if (firstOrDefault != null)
            //{
            //     lCDate = firstOrDefault.ToString();

            //}
            //return lCDate;
        }

        private string GetMasterLCDate(int commercialMasterLcfk)
        {
            db = new xOssContext();
            var lCDate = (from t1 in db.Commercial_MasterLC

                          where t1.ID == commercialMasterLcfk
                          select t1.Date).FirstOrDefault().ToString();
            return lCDate;
        }

        private string GetMasterLCNo(int mLCFk)
        {
            db = new xOssContext();
            var lCNumber = (from t1 in db.Commercial_MasterLC

                            where t1.ID == mLCFk
                            select t1.Name).FirstOrDefault();
            return lCNumber;
        }


        public void ShipmentInvoice(int id)
        {
            db = new xOssContext();
            var a = (
                from t2 in db.Commercial_InvoiceSlave
                join t1 in db.Commercial_Invoice on t2.Commercial_InvoiceFk equals t1.ID
                join t3 in db.Shipment_CNFDelivaryChallan on t2.Shipment_CNFDelivaryChallanFk equals t3.ID
                join t10 in db.Shipment_OrderDeliverdSchedule on t2.Shipment_OrderDeliverdScheduleFK equals t10.ID
                join t4 in db.Mkt_BOM on t10.Mkt_BOMFK equals t4.ID
                join t9 in db.Common_TheOrder on t4.Common_TheOrderFk equals t9.ID

                join t17 in db.Mkt_Buyer on t9.Mkt_BuyerFK equals t17.ID

                join t5 in db.Commercial_MasterLCBuyerPO on t4.ID equals t5.MKTBOMFK
                join t6 in db.Commercial_MasterLC on t5.Commercial_MasterLCFK equals t6.ID
                join t7 in db.Commercial_Bank on t6.Commercial_BuyerBankFK equals t7.ID
                join t8 in db.Commercial_Bank on t6.Commercial_LienBankFK equals t8.ID

                join t11 in db.Commercial_UDSlave on t6.ID equals t11.Commercial_MasterLCFK
                join t12 in db.Commercial_UD on t11.Commercial_UDFK equals t12.ID


                join t13 in db.Common_Country on t1.CommonCountryFk equals t13.ID
                join t14 in db.Shipment_PortOfLoading on t1.ShipmentPortOfLoadingFk equals t14.ID
                join t15 in db.Shipment_PortOfDischarge on t1.ShipmentPortOfDischargeFk equals t15.ID
                join t16 in db.Shipment_TermsOfShipment on t1.TermOfShipment equals t16.ID
                join t18 in db.Common_Country on t1.CommonCountryOfOriginFk equals t18.ID

                where t1.Active == true && t3.Active == true && t4.Active == true && t1.ID == id && t7.Active == true && t2.Active == true
                select new ReportDocType
                {
                    int1 = t2.Shipment_CNFDelivaryChallanFk,
                    HeaderLeft1 = t6.Name, //master lc name
                    HeaderLeft4 = t1.InvoiceNo,
                    HeaderLeft5 = t1.InvoiceDate.ToString(),
                    HeaderLeft6 = t6.Date.ToString(),
                    HeaderLeft7 = t12.UdNo,
                    HeaderLeft8 = t12.Date.ToString(),//UD Date
                                                      //HeaderLeft8 = System.Data.Entity.SqlServer.SqlFunctions.DateName("day", t12.Date) + "/" + System.Data.Entity.SqlServer.SqlFunctions.DateName("month", t12.Date) + "/" + System.Data.Entity.SqlServer.SqlFunctions.DateName("year", t12.Date),
                    HeaderMiddle1 = t6.NotifyParty,
                    HeaderRight1 = t1.ErcNo,
                    HeaderRight2 = t1.InvoiceIdNo,
                    HeaderRight3 = t1.ShippedPer.ToString(),
                    HeaderRight4 = t14.PortOfLoading,
                    HeaderRight5 = t15.PortOfDischarge,
                    HeaderRight6 = t13.Name,
                    HeaderRight7 = t1.BLNo,
                    HeaderRight8 = t1.BLDate.ToString(),
                    HeaderMiddle2 = t1.ExpNo,
                    HeaderMiddle3 = t1.ExpDate.ToString(),

                    HeaderLeft2 = t7.Name + "\n" + t7.Address, //Buyer bank name
                                                               /* SubBody8 = t7.Address,*/ //Buyer bank address

                    HeaderMiddle4 = t17.Name + "\n" + t17.Address, //Buyer name
                                                                   /*  SubBody9 =t17.Address , *///Buyer address
                    HeaderMiddle5 = t17.FirstNotyfyParty, //FirstNotyfyParty
                    HeaderMiddle6 = t17.SecondNotyfyParty, //SecondNotyfyParty
                    HeaderMiddle7 = t17.ThirdNotyfyParty, //ThirdNotyfyParty
                    HeaderMiddle8 = t17.BillTo, //BillTo
                    HeaderLeft3 = t8.Name + "\n" + t8.Address, //Lien bank name
                                                               /*SubBody7 = t8.Address,*///Lien bank address
                    SubBody5 = t1.NWT.ToString(),
                    SubBody6 = t1.GWT.ToString(),
                    SubBody18 = t1.IncDecDescription,
                    SubBody19 = "",
                    SubBody17 = t1.IncDecAmount.ToString(),
                    SubBody16 = t1.IsIncrease.ToString(),
                    Body1 = t18.Name,
                    Body2 = ((t10.DeliverdQty / t4.Quantity) * t4.PackPrice).ToString(), // Total Amount
                    Body3 = "$",
                    Body4 = (t4.PackPrice).ToString(),
                    Body5 = (t10.DeliverdQty / 12).ToString(), // quqntity in dozon
                    Body6 = (t4.UnitPrice * 12).ToString(),
                    Body7 = t4.UnitPrice.ToString(), //UnitPrice
                    Body8 = (t10.DeliverdQty / t4.Quantity).ToString(), //qty in pack
                    Body9 = t10.DeliverdQty.ToString(),  //Qty in pcs
                    Body10 = t9.BuyerPO + "\n" + t4.Style + "\n" + t4.Fabrication + "\n" + t2.Description,
                    Body11 = t1.ShippingMarks,
                    Body12 = t10.CtnQty.ToString(),
                    Body13 = t1.NWT.ToString(),
                    Body14 = t1.GWT.ToString(),
                    Body15 = t1.CBM.ToString(),
                    Body16 = t16.Name,
                    int2 = t2.ID,
                    int3 = t1.ID
                }).GroupBy(x => x.int2).Select(x => x.FirstOrDefault()).OrderBy(x => x.int2).ToList();


            decimal TOTALPACK = 0;
            decimal TOTALPCS = 0;
            decimal TOTALCTN = 0;
            decimal TOTALAMOUNT = 0;

            decimal TOTALNWT = 0;
            decimal TOTALGWT = 0;
            decimal TOTALCBM = 0;

            foreach (ReportDocType v in a)
            {

                decimal body2 = 0;
                decimal.TryParse(v.Body2, out body2);
                TOTALAMOUNT += body2;

                decimal body8 = 0;
                decimal.TryParse(v.Body8, out body8);
                TOTALPACK += body8;


                decimal body9 = 0;
                decimal.TryParse(v.Body9, out body9);
                TOTALPCS += body9;

                decimal body12 = 0;
                decimal.TryParse(v.Body12, out body12);
                TOTALCTN += body12;

            }
            foreach (ReportDocType r in a)
            {
                if (r.HeaderRight3 == "0")
                {
                    r.HeaderRight3 = "SEA";
                }
                else if (r.HeaderRight3 == "1")
                {
                    r.HeaderRight3 = "AIR";
                }
                else if (r.HeaderRight3 == "2")
                {
                    r.HeaderRight3 = "RAIL";
                }

                r.SubBody1 = TOTALPACK.ToString("F") + " PACK";
                r.SubBody2 = TOTALPCS.ToString() + " PCS";
                r.SubBody3 = r.Body3 + " " + TOTALAMOUNT.ToString("F");
                decimal grandtotal = 0;
                if (Convert.ToBoolean(r.SubBody16))
                {
                    r.SubBody19 = "+ " + r.Body3 + " " + Convert.ToDecimal(r.SubBody17).ToString("F");
                    grandtotal = TOTALAMOUNT + Convert.ToDecimal(r.SubBody17);
                }
                else
                {
                    r.SubBody19 = "- " + r.Body3 + " " + Convert.ToDecimal(r.SubBody17).ToString("F");
                    grandtotal = TOTALAMOUNT - Convert.ToDecimal(r.SubBody17);
                }
                r.SubBody20 = r.Body3 + " " + grandtotal.ToString("F");
                r.Body12 = TOTALCTN + " CTN";
                r.Body13 = r.Body13 + " KGS";
                r.Body14 = r.Body14 + " KGS";
                r.Body15 = r.Body15 + " CBM";

                Common.VmForDropDown ntw = new Common.VmForDropDown();
                r.HeaderRight8 = r.HeaderRight8.Substring(0, r.HeaderRight8.Length - 16);
                r.HeaderLeft6 = r.HeaderLeft6.Substring(0, r.HeaderLeft6.Length - 16);
                r.HeaderLeft5 = r.HeaderLeft5.Substring(0, r.HeaderLeft5.Length - 16);
                r.HeaderLeft8 = r.HeaderLeft8.Substring(0, r.HeaderLeft8.Length - 16);
                r.HeaderMiddle3 = r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 16);

                r.SubBody4 = ntw.NumberToWords(grandtotal, Common.CurrencyType.USD) + ".";

            }

            List<ReportDocType> list = new List<ReportDocType>();
            decimal unitprice = 0;
            decimal totalAmount = 0;
            decimal QtyPack = 0;
            foreach (var x in a)
            {
                unitprice = Convert.ToDecimal(x.Body7);
                totalAmount = Convert.ToDecimal(x.Body2);
                QtyPack = Convert.ToDecimal(x.Body8);

                x.Body7 = x.Body3 + unitprice.ToString("f4");
                x.Body2 = totalAmount.ToString("F");
                x.Body8 = QtyPack.ToString("F");
                list.Add(x);
            }



            this.BillOfExchage = list.ToList();
        }

        private void dfsd()
        {
            var v = (from t5 in db.Commercial_MasterLCBuyerPO
                     join t6 in db.Commercial_MasterLC on t5.Commercial_MasterLCFK equals t6.ID
                     join t7 in db.Commercial_Bank on t6.Commercial_BuyerBankFK equals t7.ID
                     join t8 in db.Commercial_Bank on t6.Commercial_LienBankFK equals t8.ID

                     join t11 in db.Commercial_UDSlave on t6.ID equals t11.Commercial_MasterLCFK
                     join t12 in db.Commercial_UD on t11.Commercial_UDFK equals t12.ID
                     select new
                     {

                     }).ToList();
        }

        private decimal GetQtyInPack(int delivaryChallan)
        {
            db = new xOssContext();
            var price = (from t1 in db.Shipment_OrderDeliverdSchedule
                         join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                         where t1.Active == true && t1.Shipment_CNFDelivaryChallanFk == delivaryChallan
                         select t1.DeliverdQty * t2.Quantity).ToList();

            decimal totalPrice = 0;
            if (price != null)
            {
                totalPrice = price.Sum();
            }
            return totalPrice;
        }
        private decimal GetDelQuantity(int delivaryChallan)
        {
            db = new xOssContext();
            var price = (from t1 in db.Shipment_OrderDeliverdSchedule
                         join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                         where t1.Active == true && t1.Shipment_CNFDelivaryChallanFk == delivaryChallan
                         select t1.DeliverdQty).ToList();

            decimal totalPrice = 0;
            if (price != null)
            {
                totalPrice = price.Sum();
            }
            return totalPrice;
        }
        private decimal GetTotalAmount(int delivaryChallan)
        {
            db = new xOssContext();
            var price = (from t1 in db.Shipment_OrderDeliverdSchedule
                         join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                         where t1.Active == true && t1.Shipment_CNFDelivaryChallanFk == delivaryChallan
                         select t1.DeliverdQty * t2.UnitPrice).ToList();

            decimal totalPrice = 0;
            if (price != null)
            {
                totalPrice = price.Sum();
            }
            return totalPrice;
        }
        private decimal GetTotalDozen(int delivaryChallan)
        {
            db = new xOssContext();
            var price = (from t1 in db.Shipment_OrderDeliverdSchedule
                         join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                         where t1.Active == true && t1.Shipment_CNFDelivaryChallanFk == delivaryChallan
                         select t1.DeliverdQty / 12).ToList();

            decimal totalPrice = 0;
            if (price != null)
            {
                totalPrice = price.Sum();
            }
            return totalPrice;
        }
        public void PackingList(int id)
        {
            db = new xOssContext();

            var a = (from t1 in db.Commercial_Invoice
                     join t2 in db.Commercial_InvoiceSlave on t1.ID equals t2.Commercial_InvoiceFk
                     join t10 in db.Shipment_OrderDeliverdSchedule on t2.Shipment_CNFDelivaryChallanFk equals t10.ID
                     join t3 in db.Mkt_OrderDeliverySchedule on t10.Mkt_OrderDeliveryFk equals t3.ID
                     join t4 in db.Mkt_BOM on t3.Mkt_BOMFk equals t4.ID
                     join t5 in db.Commercial_MasterLCBuyerPO on t4.ID equals t5.MKTBOMFK
                     join t6 in db.Commercial_MasterLC on t5.Commercial_MasterLCFK equals t6.ID
                     join t7 in db.Mkt_OrderColorAndSizeRatio on t3.ID equals t7.OrderDeliveryScheduleFk
                     join t8 in db.Shipment_PortOfLoading on t1.ShipmentPortOfLoadingFk equals t8.ID
                     join t9 in db.Shipment_PortOfDischarge on t1.ShipmentPortOfDischargeFk equals t9.ID
                     join t11 in db.Common_Country on t1.CommonCountryFk equals t11.ID
                     where t1.Active == true && t1.ID == id

                     select new ReportDocType
                     {
                         HeaderLeft1 = t1.InvoiceNo,
                         HeaderLeft2 = t1.InvoiceDate.ToString(),
                         HeaderLeft3 = t8.PortOfLoading,
                         HeaderLeft4 = t9.PortOfDischarge,
                         HeaderLeft5 = t11.Name,
                         HeaderLeft6 = t1.ShippedPer.ToString(),
                         HeaderLeft7 = t6.NotifyParty,
                         Body1 = t7.Color,
                         Body2 = t7.Size,
                         Body3 = t7.Ratio.ToString(),

                     }).ToList();

            foreach (ReportDocType r in a)
            {
                if (r.HeaderLeft6 == "0")
                {
                    r.HeaderLeft6 = "SEA";
                }
                else if (r.HeaderLeft6 == "1")
                {
                    r.HeaderLeft6 = "AIR";
                }
                else
                {
                    r.HeaderLeft6 = "RAIL";
                }
            }

            this.packingList = a;
        }
    }
}