﻿using Oss.Romo.Models;
using Oss.Romo.Models.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_PortOfLoading
    {
        private xOssContext db;
        public Shipment_PortOfLoading Shipment_PortOfLoading { get; set; }
        public IEnumerable<VmShipment_PortOfLoading> DataList { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_PortOfLoading

                     select new VmShipment_PortOfLoading
                     {
                         Shipment_PortOfLoading = t1,

                     }).Where(x => x.Shipment_PortOfLoading.Active == true).OrderByDescending(x => x.Shipment_PortOfLoading.ID).AsEnumerable();
            this.DataList = v;

        }

        public int Add(int userID)
        {
            Shipment_PortOfLoading.AddReady(userID);
            return Shipment_PortOfLoading.Add();
        }

        public bool Edit(int userID)
        {
            return Shipment_PortOfLoading.Edit(userID);
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Shipment_PortOfLoading
                     select new VmShipment_PortOfLoading
                     {
                         Shipment_PortOfLoading = b

                     }).Where(c => c.Shipment_PortOfLoading.ID == id).SingleOrDefault();
            this.Shipment_PortOfLoading = a.Shipment_PortOfLoading;
        }
    }
}