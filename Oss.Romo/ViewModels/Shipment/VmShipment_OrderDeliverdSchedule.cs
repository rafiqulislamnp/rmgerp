﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Shipment;
using Oss.Romo.ViewModels.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.Views.Reports;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_OrderDeliverdSchedule
    {
        private xOssContext db;
        public Shipment_OrderDeliverdSchedule Shipment_OrderDeliverdSchedule { get; set; }
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public Common_Country Common_Country { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public CommercialBillOfExchange Shipment_DocumentType { get; set; }
        public IEnumerable<VmShipment_OrderDeliverdSchedule> DataList { get; set; }
        public VmMkt_OrderDeliverySchedule VmMkt_OrderDeliverySchedule { get; set; }
        public decimal DeliverdQuantity { get; set; }
        public decimal TotalShipmentHour { get; set; }

        public string Destination { get; set; }
        public string PortNo { get; set; }
        public decimal TotalQuntity { get; set; }
        public string reference { get; set; }
        public string BackGroundColor { get; set; }
        public Prod_Capacity Prod_Capacity { get; set; }
        public VmProd_Capacity vmProd_Capacity { get; set; }
        public bool SelectChecked { get; set; }
        public bool SelectInvoiceChecked { get; set; }
        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public int Month { get; set; }
        public decimal Balance { get; set; }
        public int TotalCapacity { get; set; }
        public int Year { get; set; }
        public string Strmonth { get; set; }
        public Prod_Planning Prod_Planning { get; set; }

        public string MonthStr { get; set; }
        public string YearStr { get; set; }

        [DisplayName("Total Working Days")]
        public int? TotalWorkingDays { get; set; }

        [DisplayName("Per Day Working Hour")]
        public int? PerDayWorkingHour { get; set; }

        [DisplayName("Total Line In Factory")]
        public int? TotalLineInFactory { get; set; }
        public void ShipmentReceivedQtyDataLoad(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_OrderDeliverdSchedule
                     where t1.Common_TheOrderFk == id
                     select new VmShipment_OrderDeliverdSchedule
                     {
                         Shipment_OrderDeliverdSchedule = t1,

                     }).Where(x => x.Shipment_OrderDeliverdSchedule.Active == true).OrderByDescending(x => x.Shipment_OrderDeliverdSchedule.ID).AsEnumerable();

            List<VmShipment_OrderDeliverdSchedule> list = new List<VmShipment_OrderDeliverdSchedule>();
            foreach (var x in v)
            {
                x.Destination = GetDestination(x.Shipment_OrderDeliverdSchedule.Mkt_OrderDeliveryFk);
                x.PortNo = GetPortNo(x.Shipment_OrderDeliverdSchedule.Mkt_OrderDeliveryFk);
                // x.SelectChecked = CheckDocument(x.Shipment_OrderDeliverdSchedule.ID, x.Shipment_OrderDeliverdSchedule.Common_TheOrderFk);
                //x.SelectInvoiceChecked = CheckInvoice(x.Shipment_OrderDeliverdSchedule.ID, x.Shipment_OrderDeliverdSchedule.Common_TheOrderFk);
                list.Add(x);
            }
            this.DataList = list;
        }

        private string GetPortNo(int? id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     where t1.ID == id && t1.Active == true
                     select t1.PortNo).ToList();
            string portno = "";
            if (v.Count != 0)
            {
                portno = v[0].ToString();
            }

            return portno;
        }
        private string GetDestination(int? id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     join t2 in db.Common_Country on t1.Destination equals t2.ID
                     where t1.ID == id && t1.Active == true
                     select t2.Name).ToList();
            string destination = "";
            if (v.Count != 0)
            {
                destination = v[0].ToString();
            }

            return destination;
        }
        //private bool CheckDocument(int id, int? common_TheOrderFK)
        //{
        //    string id1 = Convert.ToString(id);
        //    db = new xOssContext();
        //    var v = (from t1 in db.Shipment_BillOfExchange
        //             where t1.Common_TheOrderFk == common_TheOrderFK && t1.Active == true
        //             select t1.Ship_DeliverdScheduleFK).ToList();
        //    bool check = false;
        //    if (v.Count > 0)
        //    {
        //        foreach (var x in v)
        //        {
        //            int[] nums = x.Split(',').Select(int.Parse).ToArray();
        //            foreach (int s in nums)
        //            {
        //                if (s == id)
        //                {
        //                    check = true;
        //                }
        //            }
        //        }
        //        // check = true;
        //    }

        //    return check;
        //}

        //private bool CheckInvoice(int id, int? common_TheOrderFK)
        //{
        //    string id1 = Convert.ToString(id);
        //    db = new xOssContext();
        //    var v = (from t1 in db.Commercial_Invoice
        //             where t1. == common_TheOrderFK && t1.Active == true
        //             select t1.Ship_DeliverdScheduleFK).ToList();
        //    bool checkInvoice = false;
        //    if (v.Count > 0)
        //    {
        //        foreach (var x in v)
        //        {
        //            int[] nums = x.Split(',').Select(int.Parse).ToArray();
        //            foreach (int s in nums)
        //            {
        //                if (s == id)
        //                {
        //                    checkInvoice = true;
        //                }
        //            }
        //        }
        //        // check = true;
        //    }

        //    return checkInvoice;
        //}
        public decimal GetDeliverdQuantity(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_OrderDeliverdSchedule
                     where t1.Mkt_OrderDeliveryFk == id && t1.Active == true
                     select t1.DeliverdQty).ToList();
            decimal deliverdQty = 0;
            if (v != null)
            {
                deliverdQty = v.Sum();
            }

            return deliverdQty;
        }
        private string GetReference(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_OrderDeliverdSchedule
                     where t1.Mkt_OrderDeliveryFk == id && t1.Active == true
                     select t1.Reference).SingleOrDefault();

            return v;
        }

        public void ShipmentInfoDataLoad(string Id = "")
        {
            db = new xOssContext();
            if (Id == "")
            {
                var v = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t3 in db.Common_Country on t1.Destination equals t3.ID
                         join t4 in db.Common_TheOrder on t1.Common_TheOrderFk equals t4.ID
                         //join t6 in db.Prod_Planning on t1.Mkt_BOMFk equals t6.Mkt_BOMFK
                         where t4.IsComplete == false
                         select new VmShipment_OrderDeliverdSchedule
                         {
                             Mkt_OrderDeliverySchedule = t1,
                             Common_Country = t3,
                             Common_TheOrder = t4,
                             //Prod_Planning = t6
                         }).Where(x => x.Mkt_OrderDeliverySchedule.Active == true).OrderByDescending(x => x.Mkt_OrderDeliverySchedule.ID).AsEnumerable();

                List<VmShipment_OrderDeliverdSchedule> list = new List<VmShipment_OrderDeliverdSchedule>();
                foreach (var x in v)
                {
                    x.DeliverdQuantity = GetDeliverdQuantity(x.Mkt_OrderDeliverySchedule.ID);
                    //x.reference = GetReference(x.Mkt_OrderDeliverySchedule.ID);
                    x.BackGroundColor = GetBackGroundColor(x.Mkt_OrderDeliverySchedule.Date);
                    list.Add(x);
                }
                this.DataList = list;
            }
            else
            {

                var v = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t3 in db.Common_Country on t1.Destination equals t3.ID
                         join t4 in db.Common_TheOrder on t1.Common_TheOrderFk equals t4.ID
                         //join t6 in db.Prod_Planning on t1.Mkt_BOMFk equals t6.Mkt_BOMFK
                         where (SqlFunctions.DateName("month", t1.Date) + " " + SqlFunctions.DateName("year", t1.Date)) == Id &&
                     t4.IsComplete == false
                         select new VmShipment_OrderDeliverdSchedule
                         {
                             Mkt_OrderDeliverySchedule = t1,
                             Common_Country = t3,
                             Common_TheOrder = t4,
                             //Prod_Planning = t6,
                             Strmonth = SqlFunctions.DateName("year", t1.Date) + " " + SqlFunctions.DateName("month", t1.Date)
                         }).Where(x => x.Mkt_OrderDeliverySchedule.Active == true).OrderByDescending(x => x.Mkt_OrderDeliverySchedule.ID).AsEnumerable();

                List<VmShipment_OrderDeliverdSchedule> list = new List<VmShipment_OrderDeliverdSchedule>();
                foreach (var x in v)
                {
                    x.DeliverdQuantity = GetDeliverdQuantity(x.Mkt_OrderDeliverySchedule.ID);
                    //x.reference = GetReference(x.Mkt_OrderDeliverySchedule.ID);
                    x.BackGroundColor = GetBackGroundColor(x.Mkt_OrderDeliverySchedule.Date);
                    list.Add(x);
                }
                this.DataList = list;
            }

        }
        public void ShipmentPlanningInfoDataLoad(string Id = "")
        {
            db = new xOssContext();
            if (Id == "")
            {
                var v = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t3 in db.Common_Country on t1.Destination equals t3.ID
                         join t4 in db.Common_TheOrder on t1.Common_TheOrderFk equals t4.ID
                         join t6 in db.Prod_Planning on t1.Mkt_BOMFk equals t6.Mkt_BOMFK
                         select new VmShipment_OrderDeliverdSchedule
                         {
                             Mkt_OrderDeliverySchedule = t1,
                             Common_Country = t3,
                             Common_TheOrder = t4,
                             Prod_Planning = t6
                         }).Where(x => x.Mkt_OrderDeliverySchedule.Active == true).OrderByDescending(x => x.Mkt_OrderDeliverySchedule.ID).AsEnumerable();

                List<VmShipment_OrderDeliverdSchedule> list = new List<VmShipment_OrderDeliverdSchedule>();
                foreach (var x in v)
                {
                    x.DeliverdQuantity = GetDeliverdQuantity(x.Mkt_OrderDeliverySchedule.ID);
                    //x.reference = GetReference(x.Mkt_OrderDeliverySchedule.ID);
                    x.BackGroundColor = GetBackGroundColor(x.Mkt_OrderDeliverySchedule.Date);
                    list.Add(x);
                }
                this.DataList = list;
            }
            else
            {

                var v = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t3 in db.Common_Country on t1.Destination equals t3.ID
                         join t4 in db.Common_TheOrder on t1.Common_TheOrderFk equals t4.ID
                         join t6 in db.Prod_Planning on t1.Mkt_BOMFk equals t6.Mkt_BOMFK
                         where (SqlFunctions.DateName("month", t1.Date) + " " + SqlFunctions.DateName("year", t1.Date)) == Id
                         select new VmShipment_OrderDeliverdSchedule
                         {
                             Mkt_OrderDeliverySchedule = t1,
                             Common_Country = t3,
                             Common_TheOrder = t4,
                             Prod_Planning = t6,
                             Strmonth = SqlFunctions.DateName("year", t1.Date) + " " + SqlFunctions.DateName("month", t1.Date)
                         }).Where(x => x.Mkt_OrderDeliverySchedule.Active == true).OrderByDescending(x => x.Mkt_OrderDeliverySchedule.ID).AsEnumerable();

                List<VmShipment_OrderDeliverdSchedule> list = new List<VmShipment_OrderDeliverdSchedule>();
                foreach (var x in v)
                {
                    x.DeliverdQuantity = GetDeliverdQuantity(x.Mkt_OrderDeliverySchedule.ID);
                    //x.reference = MonthlyShipmentTarget(x.Strmonth);
                    x.BackGroundColor = GetBackGroundColor(x.Mkt_OrderDeliverySchedule.Date);
                    list.Add(x);
                }
                this.DataList = list;
            }

        }

        private string MonthlyShipmentTarget(string strmonth)
        {
            throw new NotImplementedException();
        }

        public void ShipmentInfoDataLoad1(VmShipment_OrderDeliverdSchedule model)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_OrderDeliverySchedule
                     where t1.Active == true
                     select new VmShipment_OrderDeliverdSchedule
                     {
                         Mkt_OrderDeliverySchedule = t1

                     }).OrderBy(x => x.Mkt_OrderDeliverySchedule.Date).ToList();






            List<VmShipment_OrderDeliverdSchedule> SummeryList = new List<VmShipment_OrderDeliverdSchedule>();

            List<VmShipment_OrderDeliverdSchedule> monthlist = new List<VmShipment_OrderDeliverdSchedule>();

            monthlist = GetMonthList(model.FromDate, model.ToDate);
            foreach (var month in monthlist)
            {

                SummeryList.Add(new VmShipment_OrderDeliverdSchedule()
                {
                    FromDate = new DateTime(month.Year, month.Month, 01),
                    TotalQuntity = GetTotalQuantity(a, month),
                    TotalCapacity = GetTotalCapacity(),
                    Balance = TotalCapacity - TotalQuntity,
                    vmProd_Capacity = GetCapacityByMonth(month.Year + " " + month.Month),
                    TotalShipmentHour = GetTotalShipmentHour(month.Year + " " + month.Month)
                });

            }

            this.DataList = SummeryList;
        }

        private decimal GetTotalShipmentHour(string monthYear)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_OrderDeliverySchedule
                     join t2 in db.Prod_Planning on t1.Mkt_BOMFk equals t2.Mkt_BOMFK
                     where t1.Active == true
                     select new VmProd_Capacity
                     {
                         Mkt_OrderDeliverySchedule = t1,
                         MonthYear = t1.Date.Year + " " + t1.Date.Month,
                         Prod_Planning = t2
                     }).Where(x => x.MonthYear == monthYear).ToList();
            decimal Quantity = 0;
            decimal Capacity = 0;
            decimal Hour = 0;

            if (a != null)
            {
                foreach (VmProd_Capacity x in a)
                {
                    Quantity = x.Mkt_OrderDeliverySchedule.Quantity;
                    Capacity = x.Prod_Planning.Capacity;
                    Hour += Quantity / Capacity;
                }

            }

            return Hour;
        }

        private VmProd_Capacity GetCapacityByMonth(string fromDate)
        {
            db = new xOssContext();
            var a = (from t1 in db.Prod_Capacity
                     where t1.Active == true
                     select new VmProd_Capacity
                     {
                         Prod_Capacity = t1,
                         MonthYear = t1.Year + " " + t1.Month
                     }).FirstOrDefault(x => x.MonthYear == fromDate);

            return a;
        }


        public decimal GetTotalQuantity(List<VmShipment_OrderDeliverdSchedule> list, VmShipment_OrderDeliverdSchedule month)
        {
            var b = (from o in list
                     where o.Mkt_OrderDeliverySchedule.Date.Month == month.Month && o.Mkt_OrderDeliverySchedule.Date.Year == month.Year
                     //group o by new { o.Mkt_OrderDeliverySchedule.Quantity } into all
                     select new
                     {
                         //total=all.Sum(a=>a.Mkt_OrderDeliverySchedule.Quantity)   
                         qty = o.Mkt_OrderDeliverySchedule.Quantity
                     }).Sum(a => a.qty);

            return b;
        }
        public int GetTotalCapacity()
        {
            var b = (from o in db.Plan_ProductionLine
                     where o.Active == true
                     //group o by new { o.Mkt_OrderDeliverySchedule.Quantity } into all
                     select new
                     {
                         //total=all.Sum(a=>a.Mkt_OrderDeliverySchedule.Quantity)   
                         DailyCapacity = o.DailyCapacity
                     }).Sum(a => a.DailyCapacity);

            return b;
        }
        public List<VmShipment_OrderDeliverdSchedule> GetMonthList(DateTime fDate, DateTime tDate)
        {
            List<VmShipment_OrderDeliverdSchedule> monthlist = new List<VmShipment_OrderDeliverdSchedule>();
            do
            {
                if (fDate == tDate)
                {
                    monthlist.Add(new VmShipment_OrderDeliverdSchedule { Month = fDate.Month, Year = fDate.Year });

                }
                else
                {
                    monthlist.Add(new VmShipment_OrderDeliverdSchedule { Month = fDate.Month, Year = fDate.Year });
                    fDate = fDate.AddMonths(1);
                }
            } while (fDate <= tDate);


            return monthlist;
        }

        private string GetBackGroundColor(DateTime date)
        {
            db = new xOssContext();
            var v = db.Alert.OrderBy(x => x.NODays).ToList();

            foreach (var entry in v)
            {
                if ((date - DateTime.Today).TotalDays > -1)
                {
                    if ((date - DateTime.Today).TotalDays <= entry.NODays)
                    {
                        return entry.Color;
                    }
                }
                else
                {
                    return entry.Color;
                }
            }

            return "none";
        }

        public int Add(int userID)
        {
            Shipment_OrderDeliverdSchedule.AddReady(userID);
            return Shipment_OrderDeliverdSchedule.Add();
        }
        public bool Edit(int userID)
        {
            return Shipment_OrderDeliverdSchedule.Edit(userID);
        }

        public void SelectSingleForMktDeliverySchedule(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Mkt_OrderDeliverySchedule
                     join t1 in db.Common_Country on b.Destination equals t1.ID
                     select new VmMkt_OrderDeliverySchedule
                     {
                         Mkt_OrderDeliverySchedule = b,
                         Common_Country = t1,

                     }).Where(c => c.Mkt_OrderDeliverySchedule.ID == id).SingleOrDefault();
            this.Mkt_OrderDeliverySchedule = a.Mkt_OrderDeliverySchedule;
            this.Common_Country = a.Common_Country;

            //var a1 = (from b in db.Shipment_OrderDeliverdSchedule
            //         select new VmShipment_OrderDeliverdSchedule
            //         {
            //             Shipment_OrderDeliverdSchedule = b

            //         }).Where(c => c.Shipment_OrderDeliverdSchedule.Mkt_OrderDeliveryFk == id).SingleOrDefault();

            //if (a1 != null)
            //{
            //    this.Shipment_OrderDeliverdSchedule = a1.Shipment_OrderDeliverdSchedule;
            //}
            //else
            //{
            //    this.Shipment_OrderDeliverdSchedule = null;
            //}
        }

        public bool CheckDeliverdQty(int id)
        {
            var a1 = (from b in db.Shipment_OrderDeliverdSchedule
                      select new VmShipment_OrderDeliverdSchedule
                      {
                          Shipment_OrderDeliverdSchedule = b

                      }).Where(c => c.Shipment_OrderDeliverdSchedule.Mkt_OrderDeliveryFk == id).SingleOrDefault();

            if (a1 != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string GetOrderDeliveryRequiredQty(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     where t1.ID == id && t1.Active == true
                     select t1.Quantity).Sum();
            return v.ToString();
        }

        //public string CheckDeliverdSchedule(int id)
        //{
        //    db = new xOssContext();

        //    var v = (from t1 in db.Mkt_OrderDeliverySchedule
        //             where t1.ID == id && t1.Active == true
        //             select t1.Quantity).Sum();

        //    var v1 = (from t1 in db.Shipment_OrderDeliverdSchedule
        //              where t1.Mkt_OrderDeliveryFk == id && t1.Active == true
        //              select t1.DeliverdQty).ToList();

        //    decimal val = 0;
        //    if (v1.Count > 0)
        //    {
        //        val = Convert.ToDecimal(v1.Sum());
        //    }

        //    var result = (Convert.ToDecimal(v) - val);

        //    return result.ToString();
        //}

        //public string CheckDeliverdScheduleQtyForEdit(int id, int editId)
        //{
        //    db = new xOssContext();

        //    var v = (from t1 in db.Mkt_OrderDeliverySchedule
        //             where t1.ID == id && t1.Active == true
        //             select t1.Quantity).Sum();

        //    var v1 = (from t1 in db.Shipment_OrderDeliverdSchedule
        //              where t1.Mkt_OrderDeliveryFk == id && t1.ID != editId && t1.Active == true
        //              select t1.DeliverdQty).ToList();

        //    decimal val = 0;
        //    if (v1.Count > 0)
        //    {
        //        val = Convert.ToDecimal(v1.Sum());
        //    }

        //    var result = (Convert.ToDecimal(v) - val);

        //    return result.ToString();
        //}
    }
}