﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_History
    {
        private xOssContext db;
        public IEnumerable<VmShipment_History> DataList { get; set; }

        public Shipment_History Shipment_History { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public VmShipment_History()
        {
            //db = new xOssContext();
            InitialDataLoad();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_History
                     join t2 in db.Common_TheOrder on t1.Common_TheOrderFK equals t2.ID

                     where t1.Active==true
                     select new VmShipment_History
                     {
                         Shipment_History = t1,
                         Common_TheOrder=t2
                     }).OrderByDescending(x => x.Shipment_History.ID).AsEnumerable();
            this.DataList = v;

        }

        public void SelectSingle(int id)
        {          
            db = new xOssContext();
            var v = (from t1 in db.Shipment_History
                     select new VmShipment_History
                     {
                         Shipment_History = t1
                     }).Where(c => c.Shipment_History.ID == id).SingleOrDefault();
            this.Shipment_History = v.Shipment_History;

        }

        public int Add(int userID)
        {
            Shipment_History.AddReady(userID);
            return Shipment_History.Add();
        }

        public bool Edit(int userID)
        {
            return Shipment_History.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Shipment_History.Delete(userID);
        }

      
        public void SelectSingalJoined(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_History
                     join t2 in db.Common_TheOrder on t1.Common_TheOrderFK equals t2.ID
                     join t3 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t3.ID
                     join t4 in db.Mkt_BOM on t2.ID equals t4.Common_TheOrderFk
                     join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID

                     where t1.ID==id
                     select new VmShipment_History
                   
                     {
                         Shipment_History = t1,
                         Common_TheOrder=t2,
                         Mkt_Buyer=t3,
                         Mkt_BOM=t4,
                         Mkt_Item=t5
                     }).FirstOrDefault();
            this.Common_TheOrder = v.Common_TheOrder;
            this.Shipment_History = v.Shipment_History;
            this.Mkt_Buyer = v.Mkt_Buyer;
            this.Mkt_BOM = v.Mkt_BOM;
            this.Mkt_Item = v.Mkt_Item;
          }
    }
}