﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Shipment
{
    public class VmShipment_Invoice
    {
        private xOssContext db;
      
        public IEnumerable<VmShipment_Invoice> DataList { get; set; }
        public Shipment_PortOfLoading Shipment_PortOfLoading { get; set; }
        public Shipment_PortOfDischarge Shipment_PortOfDischarge { get; set; }
        public Common_Country Common_Country { get; set; }
        public bool SelectBillOfExchange { get; set; }
       
        public string SelectSingle(int id)
        {
            string id1 = Convert.ToString(id);
            db = new xOssContext();
            var v = (from t1 in db.Shipment_OrderDeliverdSchedule
                     where t1.ID == id && t1.Active == true
                     select t1.Common_TheOrderFk).SingleOrDefault();
            string orderID = "";
            if (v.Value != 0)
            {
                orderID = v.ToString();
            }

            return orderID;
        }

      
    }
}