﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{

    public class BuyerOrderList
    {
        public string OrderID { get; set; }
    }

    public class VmApiBuyerOrderList
    {
        private xOssContext db;
        public List<BuyerOrderList> buyerOrderID { get; set; }

        public void BuyerOrder(string name)
        {

            db = new xOssContext();
            var a = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_Buyer
                     on t1.Mkt_BuyerFK equals t2.ID
                     where t2.Name == name
                     select new BuyerOrderList
                     {
                         OrderID = t1.CID

                     }).AsEnumerable();

            this.buyerOrderID = a.ToList();



        }


    }
}