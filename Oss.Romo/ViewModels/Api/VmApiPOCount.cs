﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{
    public class PoCount
    {
        public int Day { get; set; }
        public int Count { get; set; }

    }


    public class VmApiPOCount
    {
        private xOssContext db;
        public List<PoCount> po { get; set; }

        public List<PoCount> PurchaseOrderByDay(int year, int month)
        {
           db = new xOssContext();

            var a=(from t1 in db.Mkt_PO
                   where t1.Date.Value.Year==year && t1.Date.Value.Month == month
                   select new
                   {
                       Day=t1.Date
                   }).GroupBy(c => c.Day.Value.Day).Select(g => new PoCount { Day = g.Key, Count = g.Count() }).ToList();


            int lastday = DateTime.DaysInMonth(year, month);



            for (int i = 1; i <= lastday; i++)
            {
                PoCount addday = new PoCount();
                if (a.Any(v => v.Day == i))
                {
                    var vValue = a.FirstOrDefault(v => v.Day == i);
                    addday.Day = vValue.Day;
                    addday.Count = vValue.Count;
                }
                else
                {
                    addday.Day = i;
                    addday.Count = 0;
                }
                this.po.Add(addday);
            }



            return po; 
        }


    }
}