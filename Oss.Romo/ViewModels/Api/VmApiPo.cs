﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{
    public class PurchaseOrder
    {
        public string ID { get; set; }
        public string Supplier { get; set; }
        //public string Date { get; set; }
    }

    public class VmApiPo
    {
        private xOssContext db;
        public List<PurchaseOrder> PO { get; set; }

        public void PurOrder( int month,int year)
        {
          db = new xOssContext();

            var a = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on
                     t1.Common_SupplierFK equals t2.ID
                     
                     where (t1.Date.Value.Month==month && t1.Date.Value.Year==year)
                     select new PurchaseOrder
                     {
                         ID = t1.CID,
                         Supplier = t2.Name,
                       
                     }
                   ).AsEnumerable();
            this.PO = a.ToList();
       }


   



        //................Date Wise Search................//
        public void PODate(DateTime fromDate, DateTime toDate)
        {
            db = new xOssContext();

            var a = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on
                     t1.Common_SupplierFK equals t2.ID
                     where t1.Date >= fromDate && t1.Date <= toDate
                     select new PurchaseOrder
                     {
                         ID = t1.CID,
                         Supplier = t2.Name

                     }).AsEnumerable();
            this.PO = a.ToList();
        }



    }
}