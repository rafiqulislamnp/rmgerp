﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{
    public class OrderInPo
    {
        public string Id { get; set; }
    }

    public class VmApiPoList
    {
        private xOssContext db;
        public List<OrderInPo> orderPoList { get; set; }

        public void OrderByPo(string id)
        {
            
            db = new xOssContext();
            var a = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_BOM
                     on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder
                     on t2.Common_TheOrderFk equals t3.ID
                     where t1.Active==true && t2.Active==true && t3.Active==true && t3.CID==id

                     select new OrderInPo
                     {
                         Id = t1.CID

                     }).AsEnumerable();

            this.orderPoList = a.ToList();



        }



    }
}