﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace Oss.Romo.ViewModels.Api
{
    public class OrderDetail
    {
        public string OrderId { get; set; }
        public string OrderQuantity { get; set; }
        public string DeliveryDate{ get; set; }
        public string Deviation { get; set; }
        public string Status { get; set; }
    }

    public class VmOrderSummaryDetails
    {
        private xOssContext db;
        public List<OrderDetail> orderSummary { get; set; }


        //public void OrderSummary( string name )
        //{
        //    db = new xOssContext();


        //     var a = (
        //      from t1 in db.Mkt_OrderDeliverySchedule
        //      join t2 in db.Common_TheOrder
        //      on t1.Common_TheOrderFk equals t2.ID
        //      join t3 in db.Mkt_Buyer
        //      on t2.Mkt_BuyerFK equals t3.ID
        //      where t1.Active == true && t2.Active == true && t3.Active == true && t2.IsComplete == false
        //      && t3.Name == name
        //      select new OrderDetail
        //      {
        //          OrderId = t2.CID,
        //          OrderQuantity = t1.Quantity.ToString(),
        //          DeliveryDate = t1.Date.ToString().Substring(0, 12),
        //          Deviation = "",
        //          Status = ""


        //      }).AsEnumerable();
        //     this.orderSummary = a.ToList();

        //}



        //public void OrderSummary(string name, int year, int month,string id)
        //{
        //    db = new xOssContext();
        //    //DateTime? dedate;
        //    //if (date != "")
        //    //{
        //    //    dedate = Convert.ToDateTime(date);
        //    //}

        //    var a = (
        //    from t1 in db.Mkt_OrderDeliverySchedule
        //    join t2 in db.Common_TheOrder
        //    on t1.Common_TheOrderFk equals t2.ID
        //    join t3 in db.Mkt_Buyer
        //    on t2.Mkt_BuyerFK equals t3.ID
        //    where t1.Active == true && t2.Active == true && t3.Active == true && t2.IsComplete == false
        //    && (t3.Name == name || t1.Date.Year == year || t1.Date.Month == month ||t2.CID==id)
        //    select new OrderDetail
        //    {
        //        OrderId = t2.CID,
        //        OrderQuantity = t1.Quantity.ToString(),
        //        DeliveryDate = t1.Date.ToString().Substring(0, 12),
        //        Deviation = "",
        //        Status = ""


        //    }).AsEnumerable();
        //    this.orderSummary = a.ToList();




        //}

        public void OrderSummary(string name, string year,string month, string id)
        {
            db = new xOssContext();

            int? y =null;
            int? m =null;

            if (year!="")
            {
                y = Convert.ToInt32(year);
            }
            if (month!="")
            {
                m = Convert.ToInt32(month);
            }
           if (!string.IsNullOrEmpty(name))
            {
                var a = (
                         from t1 in db.Mkt_OrderDeliverySchedule
                         join t2 in db.Common_TheOrder
                         on t1.Common_TheOrderFk equals t2.ID
                         join t3 in db.Mkt_Buyer
                         on t2.Mkt_BuyerFK equals t3.ID
                         where t1.Active == true && t2.Active == true && t3.Active == true && t2.IsComplete == false
                         && t3.Name == name
                         select new OrderDetail
                         {
                             OrderId = t2.CID,
                             OrderQuantity = t1.Quantity.ToString(),
                             DeliveryDate = t1.Date.ToString().Substring(0, 12),
                             Deviation = "",
                             Status = ""


                         }).AsEnumerable();
                this.orderSummary = a.ToList();

            }

            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
            {

           var b = (
           from t1 in db.Mkt_OrderDeliverySchedule
           join t2 in db.Common_TheOrder
           on t1.Common_TheOrderFk equals t2.ID
           join t3 in db.Mkt_Buyer
           on t2.Mkt_BuyerFK equals t3.ID
           where t1.Active == true && t2.Active == true && t3.Active == true && t2.IsComplete == false
           &&  t1.Date.Year == y && t1.Date.Month == m 
           select new OrderDetail
           {
               OrderId = t2.CID,
               OrderQuantity = t1.Quantity.ToString(),
               DeliveryDate = t1.Date.ToString().Substring(0, 12),
               Deviation = "",
               Status = ""


           }).AsEnumerable();
           this.orderSummary = b.ToList();
          }
        if (!string.IsNullOrEmpty(id))
            {
            var c = (
            from t1 in db.Mkt_OrderDeliverySchedule
            join t2 in db.Common_TheOrder
            on t1.Common_TheOrderFk equals t2.ID
            join t3 in db.Mkt_Buyer
            on t2.Mkt_BuyerFK equals t3.ID
            where t1.Active == true && t2.Active == true && t3.Active == true && t2.IsComplete == false
            &&  t2.CID == id
            select new OrderDetail
            {
                OrderId = t2.CID,
                OrderQuantity = t1.Quantity.ToString(),
                DeliveryDate = t1.Date.ToString().Substring(0, 12),
                Deviation = "",
                Status = ""


            }).AsEnumerable();
                this.orderSummary = c.ToList();
            }


            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month))
            {
                var d = (
                from t1 in db.Mkt_OrderDeliverySchedule
                join t2 in db.Common_TheOrder
                on t1.Common_TheOrderFk equals t2.ID
                join t3 in db.Mkt_Buyer
                on t2.Mkt_BuyerFK equals t3.ID
                where t1.Active == true && t2.Active == true && t3.Active == true && t2.IsComplete == false
                && t3.Name == name && t1.Date.Year == y && t1.Date.Month == m
                select new OrderDetail
                {
                    OrderId = t2.CID,
                    OrderQuantity = t1.Quantity.ToString(),
                    DeliveryDate = t1.Date.ToString().Substring(0, 12),
                    Deviation = "",
                    Status = ""


                }).AsEnumerable();
                this.orderSummary = d.ToList();
            }
            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month)&& !string.IsNullOrEmpty(id))
            {
                var e = (
                from t1 in db.Mkt_OrderDeliverySchedule
                join t2 in db.Common_TheOrder
                on t1.Common_TheOrderFk equals t2.ID
                join t3 in db.Mkt_Buyer
                on t2.Mkt_BuyerFK equals t3.ID
                where t1.Active == true && t2.Active == true && t3.Active == true && t2.IsComplete == false
                && t2.CID == id && t1.Date.Year == y && t1.Date.Month == m
                select new OrderDetail
                {
                    OrderId = t2.CID,
                    OrderQuantity = t1.Quantity.ToString(),
                    DeliveryDate = t1.Date.ToString().Substring(0, 12),
                    Deviation = "",
                    Status = ""


                }).AsEnumerable();
                this.orderSummary = e.ToList();
            }


            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(id))
            {
                var f = (
                from t1 in db.Mkt_OrderDeliverySchedule
                join t2 in db.Common_TheOrder
                on t1.Common_TheOrderFk equals t2.ID
                join t3 in db.Mkt_Buyer
                on t2.Mkt_BuyerFK equals t3.ID
                where t1.Active == true && t2.Active == true && t3.Active == true && t2.IsComplete == false
                && t3.Name == name && t2.CID == id 
                select new OrderDetail
                {
                    OrderId = t2.CID,
                    OrderQuantity = t1.Quantity.ToString(),
                    DeliveryDate = t1.Date.ToString().Substring(0, 12),
                    Deviation = "",
                    Status = ""


                }).AsEnumerable();
                this.orderSummary = f.ToList();
            }



            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(id))
            {
                var f = (
                from t1 in db.Mkt_OrderDeliverySchedule
                join t2 in db.Common_TheOrder
                on t1.Common_TheOrderFk equals t2.ID
                join t3 in db.Mkt_Buyer
                on t2.Mkt_BuyerFK equals t3.ID
                where t1.Active == true && t2.Active == true && t3.Active == true && t2.IsComplete == false
                && t3.Name == name && t1.Date.Year == y && t1.Date.Month == m && t2.CID == id
                select new OrderDetail
                {
                    OrderId = t2.CID,
                    OrderQuantity = t1.Quantity.ToString(),
                    DeliveryDate = t1.Date.ToString().Substring(0, 12),
                    Deviation = "",
                    Status = ""


                }).AsEnumerable();
                this.orderSummary = f.ToList();
            }


        }



    }
}