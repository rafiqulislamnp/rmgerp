﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{   public class GetOrder
    {
        public string ID { get; set; }
    }


    public class VmOrderId
    {

        private xOssContext db;
        public List<GetOrder> orderID { get; set; }

        public void GetOrderID()
        {
            db = new xOssContext();
            var a = (from t1 in db.Common_TheOrder
                     where t1.Active == true
                     select new GetOrder
                     {
                      ID = t1.CID

                     }).AsEnumerable();
            this.orderID = a.ToList();
        }


    }
}