﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{
    public class OrderPo
    {

        public string Item { get; set; }

        public string TotalPOValue { get; set; }
        //public string Description { get; set; }

        public int POID { get; set; }
    }


    public class VmApiOrderPo
    {
        private xOssContext db;
        public List<OrderPo> orderPo { get; set; }


        public void OrderPoWiseSearch(string Search)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO
                    on t1.Mkt_POFK equals t2.ID
                     join t3 in db.Mkt_BOM
                      on t2.Mkt_BOMFK equals t3.ID
                     join t4 in db.Raw_Item
                    on t1.Raw_ItemFK equals t4.ID
                     join t5 in db.Common_TheOrder
                      on t3.Common_TheOrderFk equals t5.ID
                     where t2.CID == Search

                     select new OrderPo
                     {
                         Item = t4.Name,
                         TotalPOValue = "",
                         POID = t2.ID

                     }).AsEnumerable();

            foreach (var i in a)
            {
                decimal? poVal = TotlaValue(i.POID);
                i.TotalPOValue = poVal.ToString();
            }
            this.orderPo = a.ToList();


        }


        public void OrderPoSearch(string orderId, string poCid, string fromDate, string toDate)
        {

            DateTime? fdate = null;
            DateTime? tdate = null;



            if (fromDate != "")
            {

                fdate = Convert.ToDateTime(fromDate);
            }
            if (toDate != "")
            {

                tdate = Convert.ToDateTime(toDate);
            }
            db = new xOssContext();
            //List<OrderPo> a;
            if (!String.IsNullOrEmpty(orderId))
            {
                var b = (from t1 in db.Mkt_POSlave
                         join t2 in db.Mkt_PO
                         on t1.Mkt_POFK equals t2.ID
                         join t3 in db.Mkt_BOM
                         on t2.Mkt_BOMFK equals t3.ID
                         join t4 in db.Raw_Item
                         on t1.Raw_ItemFK equals t4.ID
                         join t5 in db.Common_TheOrder
                         on t3.Common_TheOrderFk equals t5.ID

                         where t5.CID == orderId
                         && t5.IsComplete == false
                         && t1.Active == true
                         && t2.Active == true
                         && t3.Active == true
                         && t4.Active == true
                         && t5.Active == true
                         select new OrderPo
                         {
                             Item = t4.Name,
                             TotalPOValue = "",
                             POID = t2.ID

                         }).Distinct().ToList();

                foreach (var i in b)
                {
                    decimal? poVal = TotlaValue(i.POID);
                    i.TotalPOValue = poVal.ToString();
                }
                this.orderPo = b;
            }
            if (!String.IsNullOrEmpty(poCid))

            {
                var c = (from t1 in db.Mkt_POSlave
                         join t2 in db.Mkt_PO
                         on t1.Mkt_POFK equals t2.ID
                         join t3 in db.Mkt_BOM
                         on t2.Mkt_BOMFK equals t3.ID
                         join t4 in db.Raw_Item
                         on t1.Raw_ItemFK equals t4.ID
                         join t5 in db.Common_TheOrder
                         on t3.Common_TheOrderFk equals t5.ID

                         where t2.CID == poCid && t5.IsComplete == false
                         && t1.Active == true
                         && t2.Active == true
                         && t3.Active == true
                         && t4.Active == true
                         && t5.Active == true


                         select new OrderPo
                         {
                             Item = t4.Name,
                             TotalPOValue = "",
                             POID = t2.ID
                         }).Distinct().ToList();

                foreach (var i in c)
                {
                    decimal? poVal = TotlaValue(i.POID);
                    i.TotalPOValue = poVal.ToString();
                }
                this.orderPo = c;
            }
            if (!String.IsNullOrEmpty(orderId) && !String.IsNullOrEmpty(poCid))


            {
                var d = (from t1 in db.Mkt_POSlave
                         join t2 in db.Mkt_PO
                         on t1.Mkt_POFK equals t2.ID
                         join t3 in db.Mkt_BOM
                         on t2.Mkt_BOMFK equals t3.ID
                         join t4 in db.Raw_Item
                         on t1.Raw_ItemFK equals t4.ID
                         join t5 in db.Common_TheOrder
                         on t3.Common_TheOrderFk equals t5.ID

                         where t5.CID == orderId && t2.CID == poCid && t5.IsComplete == false
                         && t1.Active == true
                         && t2.Active == true
                         && t3.Active == true
                         && t4.Active == true
                         && t5.Active == true
                         select new OrderPo
                         {
                             Item = t4.Name,
                             TotalPOValue = "",
                             POID = t2.ID
                         }).Distinct().ToList();

                foreach (var i in d)
                {
                    decimal? poVal = TotlaValue(i.POID);
                    i.TotalPOValue = poVal.ToString();
                }
                this.orderPo = d;
            }
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))

            {
                var e = (from t1 in db.Mkt_POSlave
                         join t2 in db.Mkt_PO
                         on t1.Mkt_POFK equals t2.ID
                         join t3 in db.Mkt_BOM
                         on t2.Mkt_BOMFK equals t3.ID
                         join t4 in db.Raw_Item
                         on t1.Raw_ItemFK equals t4.ID
                         join t5 in db.Common_TheOrder
                         on t3.Common_TheOrderFk equals t5.ID

                         where t2.Date >= fdate && t2.Date <= tdate && t5.IsComplete == false
                         && t1.Active == true
                         && t2.Active == true
                         && t3.Active == true
                         && t4.Active == true
                         && t5.Active == true
                         select new OrderPo
                         {
                             Item = t4.Name,
                             TotalPOValue = "",
                             POID = t2.ID
                         }).Distinct().ToList();

                foreach (var i in e)
                {
                    decimal? poVal = TotlaValue(i.POID);
                    i.TotalPOValue = poVal.ToString();
                }
                this.orderPo = e;
            }
            if (!String.IsNullOrEmpty(orderId) && !String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))

            {
                var e = (from t1 in db.Mkt_POSlave
                         join t2 in db.Mkt_PO
                         on t1.Mkt_POFK equals t2.ID
                         join t3 in db.Mkt_BOM
                         on t2.Mkt_BOMFK equals t3.ID
                         join t4 in db.Raw_Item
                         on t1.Raw_ItemFK equals t4.ID
                         join t5 in db.Common_TheOrder
                         on t3.Common_TheOrderFk equals t5.ID

                         where t2.Date >= fdate && t2.Date <= tdate && t5.CID == orderId && t5.IsComplete == false
                         && t1.Active == true
                         && t2.Active == true
                         && t3.Active == true
                         && t4.Active == true
                         && t5.Active == true
                         select new OrderPo
                         {
                             Item = t4.Name,
                             TotalPOValue = "",
                             POID = t2.ID
                         }).Distinct().ToList();

                foreach (var i in e)
                {
                    decimal? poVal = TotlaValue(i.POID);
                    i.TotalPOValue = poVal.ToString();
                }
                this.orderPo = e;
            }


            if (!String.IsNullOrEmpty(poCid) && !String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))

            {
                var e = (from t1 in db.Mkt_POSlave
                         join t2 in db.Mkt_PO
                         on t1.Mkt_POFK equals t2.ID
                         join t3 in db.Mkt_BOM
                         on t2.Mkt_BOMFK equals t3.ID
                         join t4 in db.Raw_Item
                         on t1.Raw_ItemFK equals t4.ID
                         join t5 in db.Common_TheOrder
                         on t3.Common_TheOrderFk equals t5.ID

                         where t2.Date >= fdate && t2.Date <= tdate && t2.CID == poCid && t5.IsComplete == false
                         && t1.Active == true
                         && t2.Active == true
                         && t3.Active == true
                         && t4.Active == true
                         && t5.Active == true
                         select new OrderPo
                         {
                             Item = t4.Name,
                             TotalPOValue = "",
                             POID = t2.ID
                         }).Distinct().ToList();

                foreach (var i in e)
                {
                    decimal? poVal = TotlaValue(i.POID);
                    i.TotalPOValue = poVal.ToString();
                }
                this.orderPo = e;
            }


            if (!String.IsNullOrEmpty(poCid) && !String.IsNullOrEmpty(orderId) && !String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                var g = (from t1 in db.Mkt_POSlave
                         join t2 in db.Mkt_PO
                         on t1.Mkt_POFK equals t2.ID
                         join t3 in db.Mkt_BOM
                         on t2.Mkt_BOMFK equals t3.ID
                         join t4 in db.Raw_Item
                         on t1.Raw_ItemFK equals t4.ID
                         join t5 in db.Common_TheOrder
                         on t3.Common_TheOrderFk equals t5.ID

                         where t5.CID == orderId && t2.CID == poCid && t2.Date >= fdate && t2.Date <= tdate && t5.IsComplete == false
                         && t1.Active == true
                         && t2.Active == true
                         && t3.Active == true
                         && t4.Active == true
                         && t5.Active == true
                         select new OrderPo
                         {
                             Item = t4.Name,
                             TotalPOValue = "",
                             POID = t2.ID
                         }).Distinct().ToList();

                foreach (var i in g)
                {
                    decimal? poVal = TotlaValue(i.POID);
                    i.TotalPOValue = poVal.ToString();
                }
                this.orderPo = g;
            }
        }
        //public void OrderPoSearch(string orderId, string poCid, string fromDate, string toDate)
        //{

        //    DateTime? fdate = null;
        //    DateTime? tdate = null;



        //    if (fromDate != "")
        //    {

        //        fdate = Convert.ToDateTime(fromDate);
        //    }
        //    if (toDate != "")
        //    {

        //        tdate = Convert.ToDateTime(toDate);
        //    }
        //    db = new xOssContext();
        //    //List<OrderPo> a;
        //    if (!String.IsNullOrEmpty(orderId))
        //    {
        //        var b = (from t1 in db.Mkt_POSlave
        //                 join t2 in db.Mkt_PO
        //                 on t1.Mkt_POFK equals t2.ID
        //                 join t3 in db.Mkt_BOM
        //                 on t2.Mkt_BOMFK equals t3.ID
        //                 join t4 in db.Raw_Item
        //                 on t1.Raw_ItemFK equals t4.ID
        //                 join t5 in db.Common_TheOrder
        //                 on t3.Common_TheOrderFk equals t5.ID

        //                 where t5.CID == orderId
        //                 && t5.IsComplete == false
        //                 && t1.Active == true
        //                 && t2.Active == true
        //                 && t3.Active == true
        //                 && t4.Active == true
        //                 && t5.Active == true
        //                 select new OrderPo
        //                 {
        //                     Item = t4.Name,
        //                     TotalPOValue = "",
        //                     POID = t2.ID,
        //                     Description= t1.Description

        //                 }).ToList();

        //        foreach (var i in b)
        //        {
        //            decimal? poVal = TotlaValue(i.POID);
        //            i.TotalPOValue = poVal.ToString();
        //        }
        //        this.orderPo = b;
        //    }
        //    if (!String.IsNullOrEmpty(poCid))

        //    {
        //        var c = (from t1 in db.Mkt_POSlave
        //                 join t2 in db.Mkt_PO
        //                 on t1.Mkt_POFK equals t2.ID
        //                 join t3 in db.Mkt_BOM
        //                 on t2.Mkt_BOMFK equals t3.ID
        //                 join t4 in db.Raw_Item
        //                 on t1.Raw_ItemFK equals t4.ID
        //                 join t5 in db.Common_TheOrder
        //                 on t3.Common_TheOrderFk equals t5.ID

        //                 where t2.CID == poCid && t5.IsComplete == false
        //                 && t1.Active == true
        //                 && t2.Active == true
        //                 && t3.Active == true
        //                 && t4.Active == true
        //                 && t5.Active == true


        //                 select new OrderPo
        //                 {
        //                     Item = t4.Name,
        //                     TotalPOValue = "",
        //                     POID = t2.ID,
        //                     Description = t1.Description
        //                 }).ToList();

        //        foreach (var i in c)
        //        {
        //            decimal? poVal = TotlaValue(i.POID);
        //            i.TotalPOValue = poVal.ToString();
        //        }
        //        this.orderPo = c;
        //    }
        //    if (!String.IsNullOrEmpty(orderId) && !String.IsNullOrEmpty(poCid))


        //    {
        //        var d = (from t1 in db.Mkt_POSlave
        //                 join t2 in db.Mkt_PO
        //                 on t1.Mkt_POFK equals t2.ID
        //                 join t3 in db.Mkt_BOM
        //                 on t2.Mkt_BOMFK equals t3.ID
        //                 join t4 in db.Raw_Item
        //                 on t1.Raw_ItemFK equals t4.ID
        //                 join t5 in db.Common_TheOrder
        //                 on t3.Common_TheOrderFk equals t5.ID

        //                 where t5.CID == orderId && t2.CID == poCid && t5.IsComplete == false
        //                 && t1.Active == true
        //                 && t2.Active == true
        //                 && t3.Active == true
        //                 && t4.Active == true
        //                 && t5.Active == true
        //                 select new OrderPo
        //                 {
        //                     Item = t4.Name,
        //                     TotalPOValue = "",
        //                     POID = t2.ID,
        //                     Description = t1.Description
        //                 }).ToList();

        //        foreach (var i in d)
        //        {
        //            decimal? poVal = TotlaValue(i.POID);
        //            i.TotalPOValue = poVal.ToString();
        //        }
        //        this.orderPo = d;
        //    }
        //    if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))

        //    {
        //        var e = (from t1 in db.Mkt_POSlave
        //                 join t2 in db.Mkt_PO
        //                 on t1.Mkt_POFK equals t2.ID
        //                 join t3 in db.Mkt_BOM
        //                 on t2.Mkt_BOMFK equals t3.ID
        //                 join t4 in db.Raw_Item
        //                 on t1.Raw_ItemFK equals t4.ID
        //                 join t5 in db.Common_TheOrder
        //                 on t3.Common_TheOrderFk equals t5.ID

        //                 where t2.Date >= fdate && t2.Date <= tdate && t5.IsComplete == false
        //                 && t1.Active == true
        //                 && t2.Active == true
        //                 && t3.Active == true
        //                 && t4.Active == true
        //                 && t5.Active == true
        //                 select new OrderPo
        //                 {
        //                     Item = t4.Name,
        //                     TotalPOValue = "",
        //                     POID = t2.ID,
        //                     Description = t1.Description
        //                 }).ToList();

        //        foreach (var i in e)
        //        {
        //            decimal? poVal = TotlaValue(i.POID);
        //            i.TotalPOValue = poVal.ToString();
        //        }
        //        this.orderPo = e;
        //    }
        //    if (!String.IsNullOrEmpty(orderId) && !String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))

        //    {
        //        var e = (from t1 in db.Mkt_POSlave
        //                 join t2 in db.Mkt_PO
        //                 on t1.Mkt_POFK equals t2.ID
        //                 join t3 in db.Mkt_BOM
        //                 on t2.Mkt_BOMFK equals t3.ID
        //                 join t4 in db.Raw_Item
        //                 on t1.Raw_ItemFK equals t4.ID
        //                 join t5 in db.Common_TheOrder
        //                 on t3.Common_TheOrderFk equals t5.ID

        //                 where t2.Date >= fdate && t2.Date <= tdate && t5.CID == orderId && t5.IsComplete == false
        //                 && t1.Active == true
        //                 && t2.Active == true
        //                 && t3.Active == true
        //                 && t4.Active == true
        //                 && t5.Active == true
        //                 select new OrderPo
        //                 {
        //                     Item = t4.Name,
        //                     TotalPOValue = "",
        //                     POID = t2.ID,
        //                     Description = t1.Description
        //                 }).ToList();

        //        foreach (var i in e)
        //        {
        //            decimal? poVal = TotlaValue(i.POID);
        //            i.TotalPOValue = poVal.ToString();
        //        }
        //        this.orderPo = e;
        //    }


        //    if (!String.IsNullOrEmpty(poCid) && !String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))

        //    {
        //        var e = (from t1 in db.Mkt_POSlave
        //                 join t2 in db.Mkt_PO
        //                 on t1.Mkt_POFK equals t2.ID
        //                 join t3 in db.Mkt_BOM
        //                 on t2.Mkt_BOMFK equals t3.ID
        //                 join t4 in db.Raw_Item
        //                 on t1.Raw_ItemFK equals t4.ID
        //                 join t5 in db.Common_TheOrder
        //                 on t3.Common_TheOrderFk equals t5.ID

        //                 where t2.Date >= fdate && t2.Date <= tdate && t2.CID == poCid && t5.IsComplete == false
        //                 && t1.Active == true
        //                 && t2.Active == true
        //                 && t3.Active == true
        //                 && t4.Active == true
        //                 && t5.Active == true
        //                 select new OrderPo
        //                 {
        //                     Item = t4.Name,
        //                     TotalPOValue = "",
        //                     POID = t2.ID,
        //                     Description = t1.Description
        //                 }).ToList();

        //        foreach (var i in e)
        //        {
        //            decimal? poVal = TotlaValue(i.POID);
        //            i.TotalPOValue = poVal.ToString();
        //        }
        //        this.orderPo = e;
        //    }


        //    if (!String.IsNullOrEmpty(poCid) && !String.IsNullOrEmpty(orderId) && !String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
        //    {
        //        var g = (from t1 in db.Mkt_POSlave
        //                 join t2 in db.Mkt_PO
        //                 on t1.Mkt_POFK equals t2.ID
        //                 join t3 in db.Mkt_BOM
        //                 on t2.Mkt_BOMFK equals t3.ID
        //                 join t4 in db.Raw_Item
        //                 on t1.Raw_ItemFK equals t4.ID
        //                 join t5 in db.Common_TheOrder
        //                 on t3.Common_TheOrderFk equals t5.ID

        //                 where t5.CID == orderId && t2.CID == poCid && t2.Date >= fdate && t2.Date <= tdate && t5.IsComplete == false
        //                 && t1.Active == true
        //                 && t2.Active == true
        //                 && t3.Active == true
        //                 && t4.Active == true
        //                 && t5.Active == true
        //                 select new OrderPo
        //                 {
        //                     Item = t4.Name,
        //                     TotalPOValue = "",
        //                     POID = t2.ID,
        //                     Description = t1.Description
        //                 }).ToList();

        //        foreach (var i in g)
        //        {
        //            decimal? poVal = TotlaValue(i.POID);
        //            i.TotalPOValue = poVal.ToString();
        //        }
        //        this.orderPo = g;
        //    }
        //}

        public decimal? TotlaValue(int id)
        {
            db = new xOssContext();
            var a = (from Mkt_POSlave in db.Mkt_POSlave

                     where Mkt_POSlave.Mkt_POFK == id
                     select (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)).Sum();

            if (a == null)
            {
                a = 0;
            }
            return a;

        }

    }
}