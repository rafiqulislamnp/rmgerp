﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{
    public class userProfile
    {
        public string Name { get; set; }
        public string Uname { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

    }
   

 

    public class VmApiUser
    {
        private xOssContext db;
        public List<userProfile> userprofile { get; set; }
        //public List<userpassword> userpassword { get; set; }
        public void Profile(int id)
          
        {
            db = new xOssContext();

            var v = (from x in db.User_User
                     where x.ID==id
                     select new userProfile
                    {
                         
                         Uname = x.UserName,
                         Name = x.Name,
                         Address = x.Address,
                         Email = x.Email,
                         Mobile = x.Mobile

                     }).AsEnumerable();


            this.userprofile = v.ToList();
           
    
          
         }

       


        }
   
}