﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{
    public class BuyerName
    {
        public string Name { get; set; }
    }


    public class VmBuyer
    {
        private xOssContext db;
        public List<BuyerName> buyerName { get; set; }

        public void GetBuyerName()
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_Buyer
                     where t1.Active == true
                     select new BuyerName
                     {
                         Name = t1.Name

                     }).AsEnumerable();
            this.buyerName = a.ToList();
        }

    }
}