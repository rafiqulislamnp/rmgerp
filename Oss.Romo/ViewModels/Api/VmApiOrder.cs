﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{

    public class VmApiBuyerStyle
    {
        public string ID { get; set; }
        //public string BuyerPO { get; set; }
        public string Item { get; set; }
        public string TotalPack { get; set; }

        public string PC { get; set; }
  
        public string QuantityInDz { get; set; }
        public string UnitPrice { get; set; }
        public string TotalPrice { get; set; }
    }

    public class VmApiOrder
    {
        private xOssContext db;
        public List<VmApiBuyerStyle> buyerOrder { get; set; }
     
 

        //................Buyername,BuyerOrder,date Wise Order Search................//
        public void BuyerOtherSearch(string order,string name, string fromDate, string toDate)
        {
            DateTime? fromD = null;
            DateTime? toD = null;

            if (fromDate != "")
            {
                fromD = Convert.ToDateTime(fromDate);
            }
            if (toDate != "")
            {
                toD = Convert.ToDateTime(toDate);
            }

            db = new xOssContext();
            if(!String.IsNullOrEmpty(order))
         
            {
                var b = (from t1 in db.Mkt_BOM
                         join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                         join t3 in db.Mkt_Item on t1.Mkt_ItemFK equals t3.ID
                         join t4 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t4.ID

                         where t2.CID == order && t2.IsComplete==false
                         select new VmApiBuyerStyle
                         {
                             ID = t2.CID,
                             Item = t3.Name,
                             TotalPack = t1.QPack.ToString(),
                             PC = ((decimal)t1.QPack * (decimal)t1.Quantity).ToString(),
                             QuantityInDz = (((decimal)t1.QPack * (decimal)t1.Quantity) / 12).ToString(),
                             UnitPrice=t1.UnitPrice.ToString(),
                             TotalPrice = ((decimal)t1.QPack * (decimal)t1.Quantity * (decimal)t1.UnitPrice).ToString()
                         }).ToList();
                this.buyerOrder = b;

            }
            if (!String.IsNullOrEmpty(name))
             
            {
                var c = (from t1 in db.Mkt_BOM
                         join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                         join t3 in db.Mkt_Item on t1.Mkt_ItemFK equals t3.ID
                         join t4 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t4.ID
                         where t4.Name == name && t2.IsComplete == false


                         select new VmApiBuyerStyle
                         {
                             ID = t2.CID,
                             Item = t3.Name,
                             TotalPack = t1.QPack.ToString(),
                             PC = ((decimal)t1.QPack * (decimal)t1.Quantity).ToString(),
                             QuantityInDz = (((decimal)t1.QPack * (decimal)t1.Quantity) / 12).ToString(),
                             UnitPrice = t1.UnitPrice.ToString(),
                             TotalPrice = ((decimal)t1.QPack * (decimal)t1.Quantity * (decimal)t1.UnitPrice).ToString()
                         }).ToList();
                this.buyerOrder = c;

            }

            if(!String.IsNullOrEmpty(order) && !String.IsNullOrEmpty(name))
         
            {
                var d = (from t1 in db.Mkt_BOM
                         join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                         join t3 in db.Mkt_Item on t1.Mkt_ItemFK equals t3.ID
                         join t4 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t4.ID
                         where t2.CID == order && t4.Name == name && t2.IsComplete == false

                         select new VmApiBuyerStyle
                         {
                             ID = t2.CID,
                             Item = t3.Name,
                             TotalPack = t1.QPack.ToString(),
                             PC = ((decimal)t1.QPack * (decimal)t1.Quantity).ToString(),
                             QuantityInDz = (((decimal)t1.QPack * (decimal)t1.Quantity) / 12).ToString(),
                             UnitPrice = t1.UnitPrice.ToString(),
                             TotalPrice = ((decimal)t1.QPack * (decimal)t1.Quantity * (decimal)t1.UnitPrice).ToString()
                         }).ToList();
                this.buyerOrder = d;

            }
            if(!String.IsNullOrEmpty(fromDate)&& !String.IsNullOrEmpty(toDate))
         
            {
                var e = (from t1 in db.Mkt_BOM
                         join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                         join t3 in db.Mkt_Item on t1.Mkt_ItemFK equals t3.ID
                         join t4 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t4.ID
                         where t2.OrderDate >= fromD && t2.OrderDate <= toD && t2.IsComplete == false
                         select new VmApiBuyerStyle
                         {
                             ID = t2.CID,
                             Item = t3.Name,
                             TotalPack = t1.QPack.ToString(),
                             PC = ((decimal)t1.QPack * (decimal)t1.Quantity).ToString(),
                             QuantityInDz = (((decimal)t1.QPack * (decimal)t1.Quantity) / 12).ToString(),
                             UnitPrice = t1.UnitPrice.ToString(),
                             TotalPrice = ((decimal)t1.QPack * (decimal)t1.Quantity * (decimal)t1.UnitPrice).ToString()
                         }).ToList();

                         this.buyerOrder = e;

            }
          
            if(!String.IsNullOrEmpty(order)&& !String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
        
            {
                var h = (from t1 in db.Mkt_BOM
                         join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                         join t3 in db.Mkt_Item on t1.Mkt_ItemFK equals t3.ID
                         join t4 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t4.ID

                         where t2.CID == order && t2.OrderDate >= fromD && t2.OrderDate <= toD && t2.IsComplete == false
                         select new VmApiBuyerStyle
                         {
                             ID = t2.CID,
                             Item = t3.Name,
                             TotalPack = t1.QPack.ToString(),
                             PC = ((decimal)t1.QPack * (decimal)t1.Quantity).ToString(),
                             QuantityInDz = (((decimal)t1.QPack * (decimal)t1.Quantity) / 12).ToString(),
                             UnitPrice = t1.UnitPrice.ToString(),
                             TotalPrice = ((decimal)t1.QPack * (decimal)t1.Quantity * (decimal)t1.UnitPrice).ToString()
                         }).ToList();
                this.buyerOrder = h;

            }
            if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
           
            {
                var h = (from t1 in db.Mkt_BOM
                         join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                         join t3 in db.Mkt_Item on t1.Mkt_ItemFK equals t3.ID
                         join t4 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t4.ID
                         where t4.Name == name && t2.OrderDate >= fromD && t2.OrderDate <= toD && t2.IsComplete == false
                         select new VmApiBuyerStyle
                         {
                             ID = t2.CID,
                             Item = t3.Name,
                             TotalPack = t1.QPack.ToString(),
                             PC = ((decimal)t1.QPack * (decimal)t1.Quantity).ToString(),
                             QuantityInDz = (((decimal)t1.QPack * (decimal)t1.Quantity) / 12).ToString(),
                             UnitPrice = t1.UnitPrice.ToString(),
                             TotalPrice = ((decimal)t1.QPack * (decimal)t1.Quantity * (decimal)t1.UnitPrice).ToString()
                         }).ToList();
                this.buyerOrder = h;

            }
            if (!String.IsNullOrEmpty(order) && !String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))

            {
                var h = (from t1 in db.Mkt_BOM
                         join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                         join t3 in db.Mkt_Item on t1.Mkt_ItemFK equals t3.ID
                         join t4 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t4.ID
                         where t4.Name == name && t2.CID == order && t2.OrderDate >= fromD && t2.OrderDate <= toD && t2.IsComplete == false
                         select new VmApiBuyerStyle
                         {
                             ID = t2.CID,
                             Item = t3.Name,
                             TotalPack = t1.QPack.ToString(),
                             PC = ((decimal)t1.QPack * (decimal)t1.Quantity).ToString(),
                             QuantityInDz = (((decimal)t1.QPack * (decimal)t1.Quantity) / 12).ToString(),
                             UnitPrice = t1.UnitPrice.ToString(),
                             TotalPrice = ((decimal)t1.QPack * (decimal)t1.Quantity * (decimal)t1.UnitPrice).ToString()
                         }).ToList();
                this.buyerOrder = h;

            }



        }





    }

}