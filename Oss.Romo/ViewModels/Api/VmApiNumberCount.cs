﻿using Newtonsoft.Json;
using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{

    public class BuyerOrderMonth
    {
        public int Day { get; set; }
        public int Value { get; set; }
    

    }

    public class VmApiNumberCount
    {
        private xOssContext db;
    

        public List<BuyerOrderMonth> BuyerOrder { get; set; }
   

        //.............Count By Month..............//
        public List<BuyerOrderMonth> OrderBuyMonth( int year)
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_TheOrder
                     where t1.OrderDate.Year == year
                     select new
                     {
                         Day = t1.OrderDate,
                         OrderTotal = t1.CID
                     }).GroupBy(c => c.Day.Month).Select(g => new BuyerOrderMonth { Day= g.Key, Value = g.Count() }).ToList();


            return v;
        }


        public List<BuyerOrderMonth>OrderBuyerForMonth( int year, int month)
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_TheOrder
                     where   t1.OrderDate.Month == month && t1.OrderDate.Year == year
                     select new
                     {
                         Day = t1.OrderDate,
                         OrderTotal = t1.CID
                     }).GroupBy(c => c.Day.Day).Select(g => new BuyerOrderMonth { Day = g.Key, Value = g.Count() }).ToList();

           int lastday = DateTime.DaysInMonth(year,month);

            

            for (int i = 1; i <= lastday; i++)
            {
                BuyerOrderMonth addday = new BuyerOrderMonth();
                if (v.Any(a=>a.Day==i))
                {
                    var vValue = v.FirstOrDefault(a => a.Day == i);
                    addday.Day = vValue.Day;
                    addday.Value = vValue.Value;
                }
                else
                {
                    addday.Day = i;
                    addday.Value = 0;
                }
                this.BuyerOrder.Add(addday);
            }


            return BuyerOrder;
        }

       



   

    }

}
