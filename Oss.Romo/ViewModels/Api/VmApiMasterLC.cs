﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{
    public class MasterLC
    {
        public int Day { get; set; }
        public int Value { get; set; }

    }


    public class VmApiMasterLC
    {
        private xOssContext db;
        public List<MasterLC> masterLc { get; set; }

        public List<MasterLC> GetMasterLc(int year, int month)
        {
            db = new xOssContext();
  
            var b = (from t1 in db.Commercial_MasterLC
                     where t1.Date.Month == month && t1.Date.Year == year
                     select new
                     {
                         Date = t1.Date

                     }).GroupBy(a => a.Date.Day).Select(c => new MasterLC { Day = c.Key, Value = c.Count() }).ToList();


         

            int lastday = DateTime.DaysInMonth(year, month);



            for (int i = 1; i <= lastday; i++)
            {
                MasterLC addday = new MasterLC();
                if (b.Any(v => v.Day == i))
                {
                    var vValue = b.FirstOrDefault(v => v.Day == i);
                    addday.Day = vValue.Day;
                    addday.Value = vValue.Value;
                }
                else
                {
                    addday.Day = i;
                    addday.Value = 0;
                }
                this.masterLc.Add(addday);
            }



            return masterLc;

        }




    }
}