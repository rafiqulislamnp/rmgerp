﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Api
{

    public class BuyerCount
    {
        public int Month { get; set; }
        public int Value { get; set; }


    }
    public class VmApiBuyerOrderCount
    {
        private xOssContext db;
        public List<BuyerCount> BuyerName { get; set; }


        public List<BuyerCount> BuyerNameWiseOrder(int month, string name)
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_Buyer
                     on t1.Mkt_BuyerFK equals t2.ID
                     where t1.OrderDate.Year == month && t2.Name == name
                     select new
                     {
                         Day = t1.OrderDate
                       
                     }).GroupBy(c => c.Day.Month).Select(g => new BuyerCount { Month = g.Key, Value = g.Count() }).ToList();


            return v;


        }

    }
}