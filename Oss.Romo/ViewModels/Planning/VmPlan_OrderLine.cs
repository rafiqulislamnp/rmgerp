﻿using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Planning
{
    public class VmPlan_OrderLine
    {
        private xOssContext db;
        public Plan_OrderLine Plan_OrderLine { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Plan_ProductionLine Plan_ProductionLine { get; set; }
        public IEnumerable<VmPlan_OrderLine> DataList { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Plan_OrderLine in db.Plan_OrderLine
                     join Plan_ProductionLine in db.Plan_ProductionLine
                     on Plan_OrderLine.Plan_ProductionLineFK equals Plan_ProductionLine.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Plan_OrderLine.Mkt_BOMFK equals Mkt_BOM.ID

                     select new VmPlan_OrderLine
                     {
                         Plan_OrderLine = Plan_OrderLine,
                         Plan_ProductionLine = Plan_ProductionLine,
                         Mkt_BOM = Mkt_BOM
                     }).Where(x => x.Plan_OrderLine.Active == true).OrderByDescending(x => x.Plan_OrderLine.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from rc in db.Plan_OrderLine
                     select new VmPlan_OrderLine
                     {
                         Plan_OrderLine = rc
                     }).Where(c => c.Plan_OrderLine.ID == id).SingleOrDefault();
            this.Plan_OrderLine = v.Plan_OrderLine;

        }
        //....ForDelegateMethod...//
        public int Add(int userID)
        {
            Plan_OrderLine.AddReady(userID);
            return Plan_OrderLine.Add();
        }
        public bool Edit(int userID)
        {
            return Plan_OrderLine.Edit(userID);
        }
        public bool Dalete(int userID)
        {

            return Plan_OrderLine.Delete(userID);
        }
        //------------------------//-----------------------------//
        internal void GetBOMOrderLine(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();
            var a = (from t1 in db.Plan_OrderLine
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Plan_ProductionLine on t1.Plan_ProductionLineFK equals t3.ID
                     where t1.Active == true
                     && t1.Mkt_BOMFK == iD
                     select new VmPlan_OrderLine
                     {
                         Plan_OrderLine = t1,
                         Mkt_BOM = t2,
                         Plan_ProductionLine = t3
                     }).AsEnumerable();
            this.DataList = a;

        }
    }
}