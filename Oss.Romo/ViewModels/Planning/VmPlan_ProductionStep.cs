﻿using Oss.Romo.Models;
using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace Oss.Romo.ViewModels.Planning
{
    public class DataListPlan_OrderProcess
    {
        public int ID { get; set; }
        public string StepName { get; set; }
        public string SerialNo { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Remarks { get; set; }
        public bool? Active { get; set; }
        
    }

    public class VmPlan_OrderProcess
    {
        private xOssContext db;
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public IEnumerable<VmPlan_OrderProcess> DataList { get; set; }
        public List<DataListPlan_OrderProcess> DataListPlan_OrderProcesss { get; set; }
        public DataListPlan_OrderProcess DataListPlan_OrderProcess { get; set; }
        public Prod_DailyProductionStepSlave Prod_DailyProductionStepSlave { get; set; }
        public Plan_OrderProcessCategory Plan_OrderProcessCategory { get; set; }

        [DefaultValue(false)]
        public bool IsSelected { get; set; }
        
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }


        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from p in db.Plan_OrderProcess
                     join t2 in db.Plan_OrderProcessCategory
                     on p.Plan_OrderProcessCategoryFk equals t2.ID
                     select new VmPlan_OrderProcess
                     {
                         Plan_OrderProcess = p,
                         Plan_OrderProcessCategory = t2

                     }).Where(x => x.Plan_OrderProcess.Active == true).OrderByDescending(x => x.Plan_OrderProcess.ID).AsEnumerable();/*.ToList();*/
            this.DataList = a;
        }
       


        //public void SelectedProductionStepDataLoad()
        //{
        //    db = new xOssContext();
        //    var a = (from p in db.Plan_OrderProcess
        //             select new VmPlan_OrderProcess
        //             {
        //                 Plan_OrderProcess = p
        //             }).Where(x => x.Plan_OrderProcess.Active == true).OrderByDescending(x => x.Plan_OrderProcess.ID).AsEnumerable();/*.ToList();*/
        //    this.DataList = a;

        //   // GetSelectedStep();
        //}
        public void ProductionStepDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Plan_OrderProcess

                     select new DataListPlan_OrderProcess
                     {
                         ID = t1.ID,
                         StepName = t1.StepName,
                         SerialNo = t1.SerialNo,
                         Description = t1.Description,
                         IsSelected = this.IsSelected,
                         Active = t1.Active.Value,
                         Remarks = this.Remarks
                     }).Where(x => x.Active == true).OrderBy(x => x.ID).ToList();


            DataListPlan_OrderProcesss = a;
        }

        List<DataListPlan_OrderProcess> temoList = new List<DataListPlan_OrderProcess>();
        public List<DataListPlan_OrderProcess> GetDataListPlan_OrderProcess()
        {
            temoList = DataListPlan_OrderProcesss;
            return temoList;
        }

        internal bool GetSelectedStep(int userID)
        {
            var selectedPlan = DataListPlan_OrderProcesss.Where(x => x.IsSelected == true);

            foreach (var v in selectedPlan)
            {
               
                    Prod_DailyProductionStepSlave x = new Prod_DailyProductionStepSlave();
                    x.Plan_OrderProcessFK = v.ID;
                    x.IsSelected = v.IsSelected;
                    x.Description = v.Remarks;
                    x.PlanDate = DateTime.Now.Date;
                    x.AddReady(userID);
                    x.Add();
                
            }
            return true;
        }
      
       
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from rc in db.Plan_OrderProcess
                     select new VmPlan_OrderProcess
                     {
                         Plan_OrderProcess = rc
                     }).Where(c => c.Plan_OrderProcess.ID == id).SingleOrDefault();
            this.Plan_OrderProcess = v.Plan_OrderProcess;

        }



        


        public int Add(int userID)
        {
            Plan_OrderProcess.AddReady(userID);
            return Plan_OrderProcess.Add();

        }
        public bool Edit(int userID)
        {
            return Plan_OrderProcess.Edit(userID);
        }
        
        internal void TodaysPlanStepDataLoad()
        {
            db = new xOssContext();
            var date = DateTime.Now.Date;
            var a = (from t1 in db.Prod_DailyProductionStepSlave
                     join t2 in db.Plan_OrderProcess
                     on t1.Plan_OrderProcessFK equals t2.ID

                     select new VmPlan_OrderProcess
                     {
                         Plan_OrderProcess = t2,
                         Prod_DailyProductionStepSlave = t1
                     }).Where(x => x.Prod_DailyProductionStepSlave.Active == true &&
                                   x.Prod_DailyProductionStepSlave.PlanDate.Year == date.Year &&
                                   x.Prod_DailyProductionStepSlave.PlanDate.Month == date.Month &&
                                   x.Prod_DailyProductionStepSlave.PlanDate.Day == date.Day)
                              .OrderBy(x => x.Plan_OrderProcess.ID)
                              .AsEnumerable();

            this.DataList = a;
           
           

        }
    }
}