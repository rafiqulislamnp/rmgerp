﻿using Oss.Romo.Models;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Production;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Planning
{
    public class VmPlan_ProductionLineRunning
    {
        private xOssContext db;
        public int Prod_PlanReferenceProductionID { get; set; }
        public IEnumerable<VmPlan_ProductionLineRunning> DataList { get; set; }
        public Prod_PlanReferenceProductionSection Prod_PlanReferenceProductionSection { get; set; }
        
        public Plan_ProductionLine Plan_ProductionLine { get; set; }
        public Plan_ProductionLineRunning Plan_ProductionLineRunning { get; set; }

        public Prod_PlanReferenceProduction Prod_PlanReferenceProduction { get; set; }
        public VmProd_PlanReferenceProduction VmProd_PlanReferenceProduction { get; set; }

        public int Add(int userID)
        {
            Plan_ProductionLineRunning.AddReady(userID);
            return Plan_ProductionLineRunning.Add();
        }

        internal void InitialLoadData()
        {
            db = new xOssContext();
        //    var a = (from t1 in db.Prod_PlanReferenceProductionSection
        //             join t2 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t2.ID
        //             join t3 in db.Plan_OrderProcess on t2.Plan_OrderProcessFK equals t3.ID
        //             //join t4 in db.Plan_ProductionLineRunning on t2.ID equals t4.Plan_ProductionLineFK
        //             where t3.ID == 32
        //             && t3.Active == true
        //             select new VmPlan_ProductionLineRunning
        //             {
        //                 Plan_ProductionLine = t2,
        //                 Plan_ProductionLineRunning= db.Plan_ProductionLineRunning.Where(x => DbFunctions.TruncateTime(x.Date) == DateTime.Today && x.Active == true && x.Plan_ProductionLineFK==t2.ID).OrderByDescending(y=>y.ID).FirstOrDefault()
        //}).ToList();

            var a = (from t2 in db.Plan_ProductionLine 
                     join t3 in db.Plan_OrderProcess on t2.Plan_OrderProcessFK equals t3.ID
                     //join t4 in db.Plan_ProductionLineRunning on t2.ID equals t4.Plan_ProductionLineFK
                     where t3.ID == 32
                     && t3.Active == true
                     select new VmPlan_ProductionLineRunning
                     {
                         Plan_ProductionLine = t2,
                         Plan_ProductionLineRunning = db.Plan_ProductionLineRunning.Where(x => DbFunctions.TruncateTime(x.Date) == DateTime.Today && x.Active == true && x.Plan_ProductionLineFK == t2.ID).OrderByDescending(y => y.ID).FirstOrDefault()
                     }).ToList();

            this.DataList = a;
        }

        internal void InitialColorLoadData()
        {
            db = new xOssContext();

            //var a = db.Plan_ProductionLineRunning.Where(x => DbFunctions.TruncateTime(x.Date) == DateTime.Today && x.Active == true && x.ID==);
                
                

            //if (a.Any())
            //{
            //    this.Plan_ProductionLineRunning = a.FirstOrDefault();
            //}
            
        }
        
    }
}