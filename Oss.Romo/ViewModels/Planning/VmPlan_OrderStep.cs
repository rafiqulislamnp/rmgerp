﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Planning
{
    public class VmPlan_OrderStep
    {
        private xOssContext db;
        public VmCommon_TheOrder VmCommon_TheOrder { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Plan_OrderStep Plan_OrderStep { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public IEnumerable<VmPlan_OrderStep> DataList { get; set; }
        public IEnumerable<VmCommon_TheOrder> DataList1 { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public Plan_OrderProcessCategory Plan_OrderProcessCategory { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public List<ReportDocType> PlanReportDoc { get; set; }
        public DataTable DataTable { get; set; }
        public string CurrentRemarks { get; set; }
        //public VmPlan_OrderStep()
        //{
        //    if (Plan_OrderProcess == null)
        //    {

        //    }

        //}
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Common_Currency on t1.Common_CurrencyFK equals t2.ID
                     join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                     select new VmCommon_TheOrder
                     {
                         Common_TheOrder = t1,
                         Common_Currency = t2,
                         Mkt_Buyer = t3
                     }).Where(x => x.Common_TheOrder.Active == true).OrderByDescending(x => x.Common_TheOrder.ID).AsEnumerable();
            this.DataList1 = v;

        }
        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from rc in db.Plan_OrderStep
                     join t2 in db.Plan_OrderProcess on rc.Plan_OrderProcessFK equals t2.ID
                     join t3 in db.Plan_OrderProcessCategory on t2.Plan_OrderProcessCategoryFk equals t3.ID
                     select new VmPlan_OrderStep
                     {
                         Plan_OrderStep = rc,
                         Plan_OrderProcess = t2,
                         Plan_OrderProcessCategory = t3
                     }).SingleOrDefault(c => c.Plan_OrderStep.ID == iD);
            if (v != null) this.Plan_OrderStep = v.Plan_OrderStep;
            if (v != null) this.Plan_OrderProcess = v.Plan_OrderProcess;
            if (v != null) this.Plan_OrderProcessCategory = v.Plan_OrderProcessCategory;

        }
        public void GetPlan_OrderStep()
        {
            db = new xOssContext();
            var b = (from Plan_OrderStep in db.Plan_OrderStep
                     join Plan_OrderProcess in db.Plan_OrderProcess
                     on Plan_OrderStep.Plan_OrderProcessFK equals Plan_OrderProcess.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Plan_OrderStep.CommonTheOrderFK equals Mkt_BOM.ID

                     select new VmPlan_OrderStep
                     {
                         Plan_OrderStep = Plan_OrderStep,
                         Plan_OrderProcess = Plan_OrderProcess,
                         Mkt_BOM = Mkt_BOM
                     }).Where(x => x.Plan_OrderStep.Active == true).OrderByDescending(x => x.Plan_OrderStep.ID).AsEnumerable();
            this.DataList = b;

        }
        public void PlanOrderStepDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Plan_OrderStep in db.Plan_OrderStep
                     join Plan_OrderProcess in db.Plan_OrderProcess
                     on Plan_OrderStep.Plan_OrderProcessFK equals Plan_OrderProcess.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Plan_OrderStep.CommonTheOrderFK equals Mkt_BOM.ID
                     where Plan_OrderStep.Active == true
                     select new VmPlan_OrderStep
                     {
                         //Plan_OrderStep.PlannedStartDate = DateTime.Today,
                         Plan_OrderStep = Plan_OrderStep,
                         Plan_OrderProcess = Plan_OrderProcess,
                         Mkt_BOM = Mkt_BOM
                     }).Where(x => x.Plan_OrderStep.CommonTheOrderFK == id).OrderByDescending(x => x.Plan_OrderStep.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingleOrder(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Plan_OrderStep
                     join t2 in db.Common_TheOrder on t1.CommonTheOrderFK equals t2.ID
                     join t3 in db.Mkt_BOM on t2.ID equals t3.Common_TheOrderFk
                     join t4 in db.Mkt_Item on t3.Mkt_ItemFK equals t4.ID
                     where t2.ID == id & t3.Active == true
                     select new VmPlan_OrderStep
                     {
                         Plan_OrderStep = t1,
                         Common_TheOrder = t2,
                         Mkt_BOM = t3,
                         Mkt_Item = t4
                     }).FirstOrDefault(c => c.Common_TheOrder.ID == id);
            if (v != null)
            {
                this.Common_TheOrder = v.Common_TheOrder;
                this.Mkt_Item = v.Mkt_Item;
            }
        }
        //....ForDelegateMethod...//
        public int Add(int userID)
        {
            Plan_OrderStep.AddReady(userID);
            //Plan_OrderStep.Remarks = "sdfdsf";
            return Plan_OrderStep.Add();

        }
        public bool Edit(int userID)
        {
            return Plan_OrderStep.Edit(userID);
        }
        public bool Dalete(int userID)
        {
            return Plan_OrderStep.Delete(userID);
        }

        internal VmPlan_OrderStep SelectSingleJoined(int id)
        {
            var value = (from Plan_OrderStep in db.Plan_OrderStep
                         join Mkt_BOM in db.Mkt_BOM
                         on Plan_OrderStep.CommonTheOrderFK equals Mkt_BOM.ID
                         join Plan_OrderProcess in db.Plan_OrderProcess
                         on Plan_OrderStep.Plan_OrderProcessFK equals Plan_OrderProcess.ID
                         join Plan_OrderProcessCategory in db.Plan_OrderProcessCategory on Plan_OrderProcess.Plan_OrderProcessCategoryFk
                         equals Plan_OrderProcessCategory.ID
                         where Mkt_BOM.ID == id
                         select new VmPlan_OrderStep
                         {
                             Plan_OrderStep = Plan_OrderStep,
                             Mkt_BOM = Mkt_BOM,
                             Plan_OrderProcess = Plan_OrderProcess,
                             Plan_OrderProcessCategory = Plan_OrderProcessCategory
                         }).FirstOrDefault();
            return value;
        }

        internal void PlanReportDocLoad(int id)
        {
            db = new xOssContext();
            var v = (from plan_OrderStep in db.Plan_OrderStep
                     join plan_OrderProcess in db.Plan_OrderProcess
                     on plan_OrderStep.Plan_OrderProcessFK equals plan_OrderProcess.ID
                     join plan_OrderProcessCategory in db.Plan_OrderProcessCategory
                     on plan_OrderProcess.Plan_OrderProcessCategoryFk equals plan_OrderProcessCategory.ID
                     join common_TheOrder in db.Common_TheOrder
                     on plan_OrderStep.CommonTheOrderFK equals common_TheOrder.ID
                     join mkt_BOM in db.Mkt_BOM
                     on common_TheOrder.ID equals mkt_BOM.Common_TheOrderFk
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     join mkt_Buyer in db.Mkt_Buyer
                     on common_TheOrder.Mkt_BuyerFK equals mkt_Buyer.ID
                     join Common_Currency in db.Common_Currency
                     on common_TheOrder.Common_CurrencyFK equals Common_Currency.ID
                     join user_User in db.User_User
                     on mkt_BOM.FirstCreatedBy equals user_User.ID
                     where common_TheOrder.ID == id && plan_OrderStep.Active == true
                                                    && plan_OrderProcess.Active == true
                                                    && common_TheOrder.Active == true
                     select new ReportDocType
                     {
                         HeaderLeft1 = common_TheOrder.CID,
                         HeaderLeft2 = common_TheOrder.BuyerPO,
                         HeaderLeft3 = mkt_BOM.Fabrication,
                         HeaderLeft4 = (mkt_BOM.QPack * mkt_BOM.Quantity).ToString(),
                         HeaderRight1 = user_User.Name,
                         HeaderRight2 = common_TheOrder.OrderDate.ToString(),
                         HeaderRight3 = "Shipment Date",
                         HeaderRight4 = mkt_Item.Name,
                         Body1 = plan_OrderProcessCategory.Name,
                         Body2 = plan_OrderProcess.StepName,
                         Body3 = plan_OrderStep.AssignedTo,
                         Body4 = plan_OrderStep.PlannedStartDate.ToString().Substring(0, plan_OrderStep.PlannedStartDate.ToString().Length - 8),
                         Body5 = plan_OrderStep.PlannedFinishDate.ToString().Substring(0, plan_OrderStep.PlannedFinishDate.ToString().Length - 8),
                         Body6 = plan_OrderStep.ActualStartDate.ToString().Substring(0, plan_OrderStep.ActualStartDate.ToString().Length - 8),
                         Body7 = plan_OrderStep.ActualFinishDate.ToString().Substring(0, plan_OrderStep.ActualFinishDate.ToString().Length - 8),
                         //Body8 = plan_OrderStep.Date.ToString(),
                         Body9 = plan_OrderStep.Remarks
                     }).ToList();

            this.PlanReportDoc = v.ToList();
        }

        internal void GetPlan_OrderStep(int id)
        {
            db = new xOssContext();
            var a = (from Plan_OrderStep in db.Plan_OrderStep
                     join Plan_OrderProcess in db.Plan_OrderProcess
                     on Plan_OrderStep.Plan_OrderProcessFK equals Plan_OrderProcess.ID
                     join plan_OrderProcessCategory in db.Plan_OrderProcessCategory
                     on Plan_OrderProcess.Plan_OrderProcessCategoryFk equals plan_OrderProcessCategory.ID
                     join t1 in db.Common_TheOrder
                     on Plan_OrderStep.CommonTheOrderFK equals t1.ID
                     where Plan_OrderStep.CommonTheOrderFK == id
                     select new VmPlan_OrderStep
                     {
                         Plan_OrderStep = Plan_OrderStep,
                         Plan_OrderProcess = Plan_OrderProcess,
                         //Mkt_BOM = Mkt_BOM,
                         Plan_OrderProcessCategory = plan_OrderProcessCategory,
                         Common_TheOrder = t1
                     }).Where(x => x.Plan_OrderStep.Active == true).OrderByDescending(x => x.Plan_OrderStep.ID).AsEnumerable();

            this.DataList = a;

        }
        internal void GetPlan_OrderStepProgressBar(int id)
        {
            db = new xOssContext();
            var a = this.DataList.OrderByDescending(t => t.Plan_OrderProcess.SerialNo);
            DataTable dt = new DataTable("");

            if (a.Count() != 0)
            {
                var maxDate = (from t1 in a where t1.Plan_OrderStep.CommonTheOrderFK == id select t1.Plan_OrderStep.PlannedStartDate).
                    Union((from t1 in a where t1.Plan_OrderStep.CommonTheOrderFK == id select t1.Plan_OrderStep.PlannedFinishDate)).
                    Union((from t1 in a
                           where t1.Plan_OrderStep.CommonTheOrderFK == id &&
           t1.Plan_OrderStep.ActualStartDate > t1.Common_TheOrder.OrderDate
                           select t1.Plan_OrderStep.ActualStartDate)).
                    Union((from t1 in a
                           where t1.Plan_OrderStep.CommonTheOrderFK == id &&
           t1.Plan_OrderStep.ActualStartDate > t1.Common_TheOrder.OrderDate
                           select t1.Plan_OrderStep.ActualFinishDate)).Max(x => x.Date);

                var minDate = (from t1 in a where t1.Plan_OrderStep.CommonTheOrderFK == id select t1.Plan_OrderStep.PlannedStartDate).
                        Union((from t1 in a where t1.Plan_OrderStep.CommonTheOrderFK == id select t1.Plan_OrderStep.PlannedFinishDate)).
                        Union((from t1 in a
                               where t1.Plan_OrderStep.CommonTheOrderFK == id &&
           t1.Plan_OrderStep.ActualStartDate > t1.Common_TheOrder.OrderDate
                               select t1.Plan_OrderStep.ActualStartDate)).
                        Union((from t1 in a
                               where t1.Plan_OrderStep.CommonTheOrderFK == id &&
           t1.Plan_OrderStep.ActualStartDate > t1.Common_TheOrder.OrderDate
                               select t1.Plan_OrderStep.ActualFinishDate)).Min(x => x.Date);


                dt.Columns.Add("StepName", typeof(string));
                dt.Columns.Add("AssignedTo", typeof(string));

                for (double i = 0; i <= (maxDate - minDate).TotalDays; i++)
                {
                    dt.Columns.Add(minDate.AddDays(i).ToString().Substring(0, 10), typeof(string));
                }

                foreach (VmPlan_OrderStep vp in a)
                {
                    DataRow theRow = dt.NewRow();
                    theRow[0] = vp.Plan_OrderProcessCategory.Name + "/" + vp.Plan_OrderProcess.StepName;
                    theRow[1] = vp.Plan_OrderStep.AssignedTo;
                    int count = 0, rowCount = 0;
                    bool running = false;
                    DateTime dateTime1 = vp.Common_TheOrder.OrderDate;
                    DateTime dateTime2 = vp.Plan_OrderStep.PlannedStartDate;

                    //if (dateTime2 > dateTime1)
                    //{
                    foreach (DataColumn cl in dt.Columns)
                    {
                        if (count > 1)
                        {
                            if (cl.ColumnName == vp.Plan_OrderStep.PlannedStartDate.ToString().Substring(0, 10)
                               && cl.ColumnName == vp.Plan_OrderStep.PlannedFinishDate.ToString().Substring(0, 10))
                            {
                                theRow[count] = "#";// cl.ColumnName;
                                running = false;
                            }
                            else if (cl.ColumnName == vp.Plan_OrderStep.PlannedStartDate.ToString().Substring(0, 10))
                            {
                                theRow[count] = "#";// cl.ColumnName;
                                running = true;
                            }
                            else if (cl.ColumnName == vp.Plan_OrderStep.PlannedFinishDate.ToString().Substring(0, 10))
                            {
                                theRow[count] = "#";// cl.ColumnName;
                                running = false;
                            }
                            else if (running == true)
                            {
                                theRow[count] = "#";// cl.ColumnName;
                            }
                            else
                            {
                                theRow[count] = "";
                            }

                        }
                        count++;
                    }

                    //}                 
                    running = false;
                    count = 0;
                    rowCount = 0;
                    //if (dateTime2 > dateTime1)
                    //{
                    foreach (DataColumn cl in dt.Columns)
                    {

                        if (count > 1)
                        {
                            if (cl.ColumnName == vp.Plan_OrderStep.ActualStartDate.ToString().Substring(0, 10)
                               && cl.ColumnName == vp.Plan_OrderStep.ActualFinishDate.ToString().Substring(0, 10))
                            {
                                theRow[count] = "*";// cl.ColumnName;
                                running = false;
                            }
                            else if (cl.ColumnName == vp.Plan_OrderStep.ActualStartDate.ToString().Substring(0, 10))
                            {
                                theRow[count] = theRow[count] + "*";// cl.ColumnName;
                                running = true;
                            }
                            else if (cl.ColumnName == vp.Plan_OrderStep.ActualFinishDate.ToString().Substring(0, 10))
                            {
                                theRow[count] = theRow[count] + "*"; // cl.ColumnName;
                                running = false;
                            }
                            else if (running == true)
                            {
                                theRow[count] = theRow[count] + "*";// cl.ColumnName;
                            }
                            //else
                            //{
                            //    theRow[count] = "";
                            //}

                        }
                        count++;
                    }
                    // }

                    dt.Rows.InsertAt(theRow, rowCount++);
                }

                DataTable = dt;
            }

            //if (dt.Rows.Count == 0)
            //{

            //    GetOrderSteps(id);

            //}

        }

        private void GetOrderSteps(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Plan_OrderProcess
                     join t2 in db.Plan_OrderProcessCategory on t1.Plan_OrderProcessCategoryFk equals t2.ID
                     join t3 in db.Common_TheOrder on id equals t3.ID
                     where t1.Active == true
                     select new VmPlan_OrderStep
                     {
                         Plan_OrderProcess = t1,
                         Plan_OrderProcessCategory = t2,
                         Common_TheOrder = t3
                     }).OrderByDescending(x => x.Plan_OrderProcess.ID).AsEnumerable();
            // this.DataList = v;

            foreach (var x in v)
            {
                Plan_OrderStep plan_OrderStep = new Plan_OrderStep();
                plan_OrderStep.Plan_OrderProcessFK = x.Plan_OrderProcess.ID;
                plan_OrderStep.CommonTheOrderFK = id;
                plan_OrderStep.AssignedTo = "Not set";
                plan_OrderStep.PlannedStartDate = x.Common_TheOrder.OrderDate.AddDays(-30);
                plan_OrderStep.PlannedFinishDate = x.Common_TheOrder.OrderDate.AddDays(-30);
                plan_OrderStep.ActualStartDate = x.Common_TheOrder.OrderDate.AddDays(-30);
                plan_OrderStep.ActualFinishDate = x.Common_TheOrder.OrderDate.AddDays(-30);
                plan_OrderStep.Remarks = "Not set";
                plan_OrderStep.AddReady(0);
                plan_OrderStep.Add();

            }
            GetPlan_OrderStep(id);
            GetPlan_OrderStepProgressBar(id);
        }

        //--------------------------------Order Time&Action---------------------------//
        public void InitialDataLoadOrderAction()
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Common_Currency on t1.Common_CurrencyFK equals t2.ID
                     join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                     select new VmCommon_TheOrder
                     {
                         Common_TheOrder = t1,
                         Common_Currency = t2,
                         Mkt_Buyer = t3
                     }).Where(x => x.Common_TheOrder.Active == true).OrderByDescending(x => x.Common_TheOrder.ID).AsEnumerable();
            this.DataList1 = v;

        }

    }
}