﻿using Oss.Romo.Models;
using Oss.Romo.Models.Planning;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Planning
{
    public class VmPlan_ProductionLine
    {
        private xOssContext db;
        public Plan_ProductionLine Plan_ProductionLine { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public IEnumerable<VmPlan_ProductionLine> DataList { get; set; }


         public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from p in db.Plan_ProductionLine
                     join q in db.Plan_OrderProcess on p.Plan_OrderProcessFK equals q.ID
                     select new VmPlan_ProductionLine
                     {
                         Plan_ProductionLine = p,
                         Plan_OrderProcess = q
                     }).Where(x => x.Plan_ProductionLine.Active == true).OrderByDescending(x => x.Plan_ProductionLine.ID).AsEnumerable();/*.ToList();*/
            this.DataList = a;
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from rc in db.Plan_ProductionLine
                     select new VmPlan_ProductionLine
                     {
                         Plan_ProductionLine = rc
                     }).Where(c => c.Plan_ProductionLine.ID == id).SingleOrDefault();
            this.Plan_ProductionLine = v.Plan_ProductionLine;

        }






        public int Add(int userID)
        {
            Plan_ProductionLine.AddReady(userID);
            return Plan_ProductionLine.Add();

        }
        public bool Edit(int userID)
        {
            return Plan_ProductionLine.Edit(userID);
        }


    }
}