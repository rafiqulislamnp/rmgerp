﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Planning
{
    public class VmPlanReport
    {
        private xOssContext db;
        public List<Prod_Planning> ListProdPlanLine { get; set; }
        public Prod_OrderPlanning Prod_OrderPlanning { get; set; }
        public VmProd_Planning VmProd_Planning { get; set; }
        public List<ReportDocType> ReportDoc { get; set; }
        public List<VmPlanReport> DataList { get; set; }
        public List<VmPlanReport> DataList1 { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; }

        //public string ValueX { get; set; }
        //public DateTime ValueFrom { get; set; }
        //public DateTime ValueTo { get; set; }
        //public string ValueLabel { get; set; }

        public string BuyerName { get; set; }
        [Display(Name ="Buyer/PO")]
        public int BomFk { get; set; }
        public int Report { get; set; }
        public string Color { get; set; }


        public int ProdReferenceId { get; set; }
        public int SectionId { get; set; }
        public int BOMID { get; set; }
        public decimal OrderQuantityPC { get; set; }
        public decimal ProductionQty { get; set; }
        public decimal RemainQty { get; set; }
        public decimal OrderTotalAchiveQty { get; set; }
        public decimal OrderTotalAchiveRemain { get; set; }
        public decimal AchivementRemain { get; set; }
        public string LineName { get; set; }
        public string OrderNo { get; set; }
        public string Style { get; set; }
        public int OrderQty { get; set; }
        public string Range { get; set; }
        public int PieceQty { get; set; }
        public int PackQty { get; set; }
        public string ShipmentDateView { get; set; }
        public int ShipPackQty { get; set; }
        public int ShipOrderQty { get; set; }



        //public void GerPlanReport(DateTime FDate,DateTime TDate)
        //{
        //    db = new xOssContext();

        //    var vData = (from o in db.Prod_Planning
        //                 join p in db.Mkt_BOM on o.Mkt_BOMFK equals p.ID
        //                 join q in db.Common_TheOrder on p.Common_TheOrderFk equals q.ID
        //                 join t1 in db.Prod_OrderPlanning on o.Mkt_BOMFK equals t1.Mkt_BOMFK
        //                 join s in db.Plan_ProductionLine on o.Plan_ProductionLineFk equals s.ID
        //                 //where o.StartDate<=FDate && o.FinishDate>=TDate
        //                 select new
        //                 {
        //                     Id=o.ID,
        //                     Body1 = q.CID,
        //                     Body2 = p.Style,
        //                     Body4 = (p.QPack * p.Quantity).ToString(),
        //                     Body21=t1.PlanQty,
        //                     Body5 = t1.ShipmentDate,
        //                     Body6 = t1.SampleApprovedDate,
        //                     Body7 = t1.FabricInhouseDate,
        //                     Body8 = t1.AccessoriesInhouseDate,
        //                     Body9 = t1.CuttingStartDate,
        //                     Body10 = s.Name,
        //                     Body11 = o.MachineQuantity,
        //                     Body12 = o.StartDate,
        //                     Body13 = o.FinishDate,
        //                     Body14 = o.Capacity,
        //                     Body15 = o.WorkingHour,
        //                     Body16 = o.TargetDays,
        //                     Body17 = o.DayOutputQty,
        //                     Body18 = o.TargetOutputQty,
        //                     Body19 = o.RemainQty,
        //                     Body20 = o.Description
        //                 }).ToList();

        //    List<ReportDocType> lst = new List<ReportDocType>();

        //    if (vData.Any())
        //    {
        //        foreach (var v in vData)
        //        {
        //            ReportDocType m = new ReportDocType();
        //            m.Body1 = v.Body1;
        //            m.Body2 = v.Body2;
        //            m.Body3 = v.Body4;
        //            m.Body4 = ShortDateString(v.Body5);
        //            m.Body5 = ShortDateString(v.Body6);
        //            m.Body6 = ShortDateString(v.Body7);
        //            m.Body7 = ShortDateString(v.Body8);
        //            m.Body8 = ShortDateString(v.Body9);
        //            m.Body9 = v.Body10;
        //            m.Body10 = v.Body11.ToString();
        //            m.Body11 = ShortDateString(v.Body12);
        //            m.Body12 = ShortDateString(v.Body13);
        //            m.Body13 = v.Body14.ToString();
        //            m.Body14 = v.Body15.ToString();
        //            m.Body15 = v.Body16.ToString();
        //            m.Body16 = v.Body17.ToString();
        //            m.Body17 = v.Body18.ToString();
        //            m.Body18 = v.Body19.ToString();
        //            m.Body19 = v.Body20;
        //            lst.Add(m);
        //        }
        //    }

        //    this.ReportDoc = lst;

        //}

        public void GetPlanReport(VmPlanReport model)
        {
            db = new xOssContext();
            VmProd_Planning = new VmProd_Planning();
            bool IsFirstItem = false;
            decimal TempVariable1 = 0;
            decimal TempVariable2 = 0;
            int LineAchiveQty = 0;
            List<ReportDocType> lst = new List<ReportDocType>();
            var vOrderData = (from t1 in db.Prod_OrderPlanning
                              join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                              join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                              where t1.ShipmentDate>=model.FromDate && t1.ShipmentDate<=model.ToDate && t1.IsClosed==false
                              select new VmPlanReport {
                                  Prod_OrderPlanning = t1,
                                  OrderNo = t3.CID,
                                  Style = t2.Style,
                                  OrderQty = t2.QPack * t2.Quantity,
                              }).ToList();

            var GetLine = db.Plan_ProductionLine.ToList();
            if (vOrderData.Any())
            {
                foreach (var v in vOrderData)
                {
                    IsFirstItem = false;
                    TempVariable1 = 0;
                    TempVariable2 = 0;
                    LineAchiveQty = 0;

                    v.OrderTotalAchiveQty = VmProd_Planning.GetTotalAcheiveQty(v.Prod_OrderPlanning.Mkt_BOMFK);
                    v.ListProdPlanLine = db.Prod_Planning.Where(a=>a.Prod_OrderPlanningFK==v.Prod_OrderPlanning.ID).ToList();

                    if (v.ListProdPlanLine.Any())
                    {
                        foreach (var v1 in v.ListProdPlanLine)
                        {
                            LineAchiveQty = GetLineAchiveQty(v1.Plan_ProductionLineFk, v1.Prod_OrderPlanningFK, v1.Mkt_BOMFK);
                            v.OrderTotalAchiveRemain = 0;
                            ReportDocType m = new ReportDocType();
                            m.Body1 = v.OrderNo;
                            m.Body2 = v.Style;
                            m.Body3 = v.OrderQty.ToString();
                            m.Body4 = ((int)v.Prod_OrderPlanning.PlanQty).ToString();
                            m.Body5 = ((int)v.OrderTotalAchiveQty).ToString();
                            m.Body6 = ShortDateString(v.Prod_OrderPlanning.ShipmentDate);
                            m.Body7 = ShortDateString(v.Prod_OrderPlanning.SampleApprovedDate);
                            m.Body8 = ShortDateString(v.Prod_OrderPlanning.FabricInhouseDate);
                            m.Body9 = ShortDateString(v.Prod_OrderPlanning.AccessoriesInhouseDate);
                            m.Body10 = ShortDateString(v.Prod_OrderPlanning.CuttingStartDate);
                            m.Body11 = GetLine.FirstOrDefault(a => a.ID == v1.Plan_ProductionLineFk).Name;
                            m.Body12 = v1.MachineQuantity.ToString();
                            m.Body13 = ShortDateString(v1.StartDate);
                            m.Body14 = ShortDateString(v1.FinishDate);
                            m.Body15 = ((int)v1.Capacity).ToString();
                            m.Body16 = ((int)v1.WorkingHour).ToString();
                            m.Body17 = ((int)v1.TargetDays).ToString();
                            m.Body18 = ((int)v1.DayOutputQty).ToString();
                            m.Body19 = ((int)v1.TargetOutputQty).ToString();
                            m.Body21 = LineAchiveQty.ToString();
                            m.Body22 = ((int)(v1.TargetOutputQty - LineAchiveQty)).ToString();
                            if (IsFirstItem)
                            {
                                v1.RemainQty = TempVariable1 - v1.TargetOutputQty;
                                m.Body20 = ((int)v1.RemainQty).ToString();
                                v.OrderTotalAchiveRemain= TempVariable2 - v1.TargetOutputQty;
                                m.Body23 = ((int)v.OrderTotalAchiveRemain).ToString();
                            }
                            else
                            {
                                v1.RemainQty = v.Prod_OrderPlanning.PlanQty - v1.TargetOutputQty;
                                m.Body20 = ((int)v1.RemainQty).ToString();

                                v.OrderTotalAchiveRemain = (v.Prod_OrderPlanning.PlanQty - v.OrderTotalAchiveQty) - v1.TargetOutputQty;
                                m.Body23 = ((int)v.OrderTotalAchiveRemain).ToString();
                                IsFirstItem = true;
                            }
                            TempVariable1 = v1.RemainQty;
                            TempVariable2 = v.OrderTotalAchiveRemain;
                            lst.Add(m);
                        }
                    }
                    else
                    {
                        ReportDocType m = new ReportDocType();
                        m.Body1 = v.OrderNo;
                        m.Body2 = v.Style;
                        m.Body3 = v.OrderQty.ToString();
                        m.Body4 = v.Prod_OrderPlanning.PlanQty.ToString();
                        m.Body5 = v.OrderTotalAchiveQty.ToString();
                        m.Body6 = ShortDateString(v.Prod_OrderPlanning.ShipmentDate);
                        m.Body7 = ShortDateString(v.Prod_OrderPlanning.SampleApprovedDate);
                        m.Body8 = ShortDateString(v.Prod_OrderPlanning.FabricInhouseDate);
                        m.Body9 = ShortDateString(v.Prod_OrderPlanning.AccessoriesInhouseDate);
                        m.Body10 = ShortDateString(v.Prod_OrderPlanning.CuttingStartDate);
                        lst.Add(m);
                    }
                }
            }
            this.ReportDoc = lst;
        }
        
        private string ShortDateString(DateTime Date)
        {
            string datestring = string.Empty;

            string day = Date.Day < 10 ? "0" + Date.Day : Date.Day.ToString();
            string month = Date.Month < 10 ? "0" + Date.Month : Date.Month.ToString();
            int year = Date.Year;

            datestring = day + "-" + month + "-" + year;

            return datestring;
        }

        private int GetLineAchiveQty(int Plan_ProductionLineFk,int Prod_OrderPlanningFK, int BomId)
        {
            int TotalQty = 0;

            var vData = db.Prod_PlanAchievment.Where(a=>a.Prod_OrderPlanningFk== Prod_OrderPlanningFK && a.Plan_ProductionLineFk== Plan_ProductionLineFk && a.Mkt_BOMFK==BomId && a.IsPlanedLine==true);
            if (vData.Any())
            {
                TotalQty = (int)vData.Sum(a=>a.PlanAchievQty);
            }
            return TotalQty;
        }

        public void GetShipmentDetailsReport(VmPlanReport model)
        {
            db = new xOssContext();
            int count = 0;
            List<VmPlanReport> lstPlan = new List<VmPlanReport>();
            List<ReportDocType> lst = new List<ReportDocType>();
            
            if (model.Report == 1 && model.BomFk > 0 && model.FromDate != null && model.ToDate != null)
            {
                #region BuyerPOWiseSummary
                var vData = (from t1 in db.Common_TheOrder
                             join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                             join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                             join t4 in db.Mkt_OrderDeliverySchedule on t2.ID equals t4.Mkt_BOMFk
                             where t1.Active == true && t1.IsComplete == false && t2.Active == true && t4.Active==true
                             && t1.Mkt_BuyerFK == model.BomFk && t4.Date >= model.FromDate && t4.Date <= model.ToDate
                             group t4.Quantity by new { t1, t2, t3 } into all
                             select new VmPlanReport
                             {
                                 BuyerName = all.Key.t3.Name,
                                 OrderNo = all.Key.t1.BuyerPO,
                                 Style = all.Key.t2.Style,
                                 OrderQty = (int)all.Sum(),
                                 PieceQty = (int)all.Sum() / all.Key.t2.Quantity
                             }).ToList();

                //if (vData.Any())
                //{
                //    foreach (var v in vData)
                //    {
                //        v.OrderQty = v.PackQty * v.PieceQty;
                        
                        //totalPack += v.TotalPackQty;
                        //totalOrder += v.TotalPicQty;
                        //ReportDocType m = new ReportDocType();
                        //m.Body1 = count.ToString();
                        //m.Body2 = v.BuyerName;
                        //m.Body3 = v.BuyerPO;
                        //m.Body4 = v.Style;
                        //m.Body5 = v.TotalPackQty.ToString();
                        //m.Body6 = v.TotalPicQty.ToString();
                        //m.HeaderTop5 = totalPack.ToString();
                        //m.HeaderTop6 = totalOrder.ToString();
                        //if (v.Shipment.Any())
                        //{
                        //    m.ReportDocTypeList = new List<ReportDocType>();
                        //    foreach (var v1 in v.Shipment)
                        //    {
                        //        ReportDocType n = new ReportDocType();
                        //        n.SubBody1 = ShortDateString(v1.ShipDate);
                        //        n.SubBody2 = ((int)(v1.OrderQty / v.PackQty)).ToString();
                        //        n.SubBody3 = v1.OrderQty.ToString();
                        //        m.ReportDocTypeList.Add(n);
                        //    }
                        //}
                        //lst.Add(m);
                //    }
                //}
                //this.ReportDoc = lst;
                this.DataList1 = vData;
                #endregion
            }
            else if (model.Report == 1 && model.BomFk == 0 && model.FromDate != null && model.ToDate != null)
            {
                #region AllBuyerPOSummary
                
                var vData = (from t1 in db.Common_TheOrder
                             join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                             join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                             join t4 in db.Mkt_OrderDeliverySchedule on t2.ID equals t4.Mkt_BOMFk
                             where t1.Active == true && t1.IsComplete == false && t2.Active == true && t4.Active == true
                             && t4.Date >= model.FromDate && t4.Date <= model.ToDate
                             group t4.Quantity by new { t1, t2, t3 } into all
                             select new VmPlanReport
                             {
                                 BuyerName = all.Key.t3.Name,
                                 OrderNo = all.Key.t1.BuyerPO,
                                 Style = all.Key.t2.Style,
                                 OrderQty = (int)all.Sum(),
                                 PieceQty = (int)all.Sum() / all.Key.t2.Quantity
                             }).ToList();
                
                //var vData1 = (from t1 in db.Common_TheOrder
                //          join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                //          join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                //          where t1.Active == true && t1.IsComplete == false && t2.Active == true
                //          select new
                //          {
                //              BuyerName = t3.Name,
                //              BuyerPO = t1.BuyerPO,
                //              Style = t2.Style,
                //              PackQty = t2.Quantity,
                //              TotalPackQty = t2.QPack,
                //              TotalPicQty = t2.QPack * t2.Quantity,
                //              Shipment = (from o in db.Mkt_OrderDeliverySchedule
                //                          join p in db.Mkt_BOM on o.Mkt_BOMFk equals p.ID
                //                          where o.Common_TheOrderFk == t1.ID && o.Active == true
                //                          group o.Quantity by new { o.Date } into all
                //                          select new
                //                          {
                //                              ShipDate = all.Key.Date,
                //                              OrderQty = (int)all.Sum()
                //                          }).ToList()
                //          }).ToList();

                //if (vData.Any())
                //{
                //    foreach (var v in vData)
                //    {
                //        v.OrderQty = v.PackQty * v.PieceQty;
                //    }
                //}
                //this.ReportDoc = lst;
                this.DataList1 = vData;
                #endregion
            }
            else if (model.Report == 2 && model.BomFk > 0 && model.FromDate != null && model.ToDate != null)
            {
                #region BuyerWiseDetails

                var vData = (from t1 in db.Common_TheOrder
                             join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                             join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                             join t4 in db.Mkt_OrderDeliverySchedule on t2.ID equals t4.Mkt_BOMFk
                             where t1.Active == true && t1.IsComplete == false && t2.Active == true && t4.Active == true
                             && t1.Mkt_BuyerFK== model.BomFk && t4.Date >= model.FromDate && t4.Date <= model.ToDate
                             group t4.Quantity by new { t4.Date, t1, t2, t3 } into all
                             select new VmPlanReport
                             {
                                 BuyerName = all.Key.t3.Name,
                                 OrderNo = all.Key.t1.BuyerPO,
                                 Style = all.Key.t2.Style,
                                 PackQty = all.Key.t2.Quantity,
                                 //PieceQty = all.Key.t2.QPack,
                                 //OrderQty = all.Key.t2.Quantity * all.Key.t2.QPack,
                                 FromDate = all.Key.Date,
                                 ShipOrderQty = (int)all.Sum()
                             }).OrderBy(x=>x.FromDate).ToList();

                int totalOPack = 0;
                int totalOQty = 0;
                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        totalOPack += v.ShipOrderQty / v.PackQty;
                        totalOQty += v.ShipOrderQty;
                        v.ShipPackQty = v.ShipOrderQty / v.PackQty;
                        v.ShipmentDateView = ShortDateString(v.FromDate);
                    }
                }

                this.DataList = vData;
                
                #endregion
            }
            else if (model.Report == 2 && model.BomFk == 0 && model.FromDate != null && model.ToDate != null)
            {
                #region AllBuyerPreodicDetails

                var vData = (from t1 in db.Common_TheOrder
                             join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                             join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                             join t4 in db.Mkt_OrderDeliverySchedule on t2.ID equals t4.Mkt_BOMFk
                             where t1.Active == true && t1.IsComplete == false && t2.Active == true && t4.Active == true
                             && t4.Date >= model.FromDate && t4.Date <= model.ToDate
                             group t4.Quantity by new { t4.Date, t1, t2, t3 } into all
                             select new VmPlanReport
                             {
                                 BuyerName = all.Key.t3.Name,
                                 OrderNo = all.Key.t1.BuyerPO,
                                 Style = all.Key.t2.Style,
                                 PackQty = all.Key.t2.Quantity,
                                 //PieceQty = all.Key.t2.QPack,
                                 //OrderQty = all.Key.t2.Quantity * all.Key.t2.QPack,
                                 FromDate = all.Key.Date,
                                 ShipOrderQty = (int)all.Sum()
                             }).OrderBy(x=>x.FromDate).ToList();

                int totalOPack = 0;
                int totalOQty = 0;
                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        totalOPack += v.ShipOrderQty / v.PackQty;
                        totalOQty += v.ShipOrderQty;
                        v.ShipPackQty = v.ShipOrderQty / v.PackQty;
                        v.ShipmentDateView = ShortDateString(v.FromDate);
                    }
                }

                this.DataList = vData;
                #endregion
            }
            else if (model.Report == 3 && model.BomFk > 0 && model.FromDate != null && model.ToDate != null)
            {
                //Done
                #region BuyerWiseDetails
                var vData = (from t1 in db.Common_TheOrder
                             join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                             join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                             where t1.Active == true && t1.IsComplete == false && t2.Active == true && t1.Mkt_BuyerFK == model.BomFk
                             select new
                             {
                                 BuyerName = t3.Name,
                                 BuyerPO = t1.BuyerPO,
                                 Style = t2.Style,
                                 PackQty = t2.Quantity,
                                 TotalPackQty = t2.QPack,
                                 TotalPicQty = t2.QPack * t2.Quantity,
                                 Shipment = (from o in db.Mkt_OrderDeliverySchedule
                                             join p in db.Mkt_BOM on o.Mkt_BOMFk equals p.ID
                                             where o.Common_TheOrderFk == t1.ID && o.Active == true
                                             group o.Quantity by new { o.Date } into all
                                             select new
                                             {
                                                 ShipDate = all.Key.Date,
                                                 OrderQty = (int)all.Sum()
                                             }).ToList()
                             }).ToList();

                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        ++count;
                        
                        if (v.Shipment.Any())
                        {
                            foreach (var v1 in v.Shipment)
                            {
                                VmPlanReport m = new VmPlanReport();
                                m.BuyerName = v.BuyerName;
                                m.OrderNo = v.BuyerPO;
                                m.Style = v.Style;
                                m.PackQty = v.TotalPackQty;
                                m.OrderQty = v.TotalPicQty;
                                m.ShipmentDateView = ShortDateString(v1.ShipDate);
                                m.ShipPackQty = (int)(v1.OrderQty / v.PackQty);
                                m.ShipOrderQty = v1.OrderQty;
                                lstPlan.Add(m);
                            }
                        }
                        else
                        {
                            VmPlanReport m1 = new VmPlanReport();
                            m1.BuyerName = v.BuyerName;
                            m1.OrderNo = v.BuyerPO;
                            m1.Style = v.Style;
                            m1.PackQty = v.TotalPackQty;
                            m1.OrderQty = v.TotalPicQty;
                            lstPlan.Add(m1);
                        }
                        //ReportDocType m = new ReportDocType();
                        //m.Body1 = count.ToString();
                        //m.Body2 = v.BuyerName;
                        //m.Body3 = v.BuyerPO;
                        //m.Body4 = v.Style;
                        //m.Body5 = v.TotalPackQty.ToString();
                        //m.Body6 = v.TotalPicQty.ToString();

                        //if (v.Shipment.Any())
                        //{
                        //    m.ReportDocTypeList = new List<ReportDocType>();
                        //    foreach (var v1 in v.Shipment)
                        //    {
                        //        ReportDocType n = new ReportDocType();
                        //        n.SubBody1 = ShortDateString(v1.ShipDate);
                        //        n.SubBody2 = ((int)(v1.OrderQty / v.PackQty)).ToString();
                        //        n.SubBody3 = v1.OrderQty.ToString();
                        //        m.ReportDocTypeList.Add(n);
                        //    }
                        //}
                        //lst.Add(m);
                    }
                }

                //this.ReportDoc = lst;
                this.DataList = lstPlan;
                #endregion
            }
        }
        
        public void GetLinePlan(VmPlanReport model)
        {
            List<VmPlanReport> lstLine = new List<VmPlanReport>();
            db = new xOssContext();
            var getAllLines = db.Plan_ProductionLine.Where(a => a.LineNumber != null).OrderBy(a => a.ID).ToList();
            if (getAllLines.Any())
            {
                foreach (var v in getAllLines)
                {
                    var vPlan = (from t1 in db.Prod_Planning
                                 join t2 in db.Prod_OrderPlanning on t1.Prod_OrderPlanningFK equals t2.ID
                                 join t3 in db.Mkt_BOM on t2.Mkt_BOMFK equals t3.ID
                                 join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                                 where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                                 && t1.StartDate >= model.FromDate && t1.FinishDate <= model.ToDate && t1.Plan_ProductionLineFk==v.ID
                                 select new
                                 {
                                     BuyerPo = t4.BuyerPO,
                                     FromDate = t1.StartDate,
                                     ToDate = t1.FinishDate
                                 }).ToList();

                    if (vPlan.Any())
                    {
                        foreach (var v1 in vPlan)
                        {
                            VmPlanReport m = new VmPlanReport();
                            m.LineName = v.Name;
                            m.FromDate = v1.FromDate;
                            m.ToDate = v1.ToDate;
                            m.OrderNo = v1.BuyerPo;
                            m.Color = "blue";
                            lstLine.Add(m);
                        }
                    }
                    else
                    {
                        VmPlanReport m = new VmPlanReport();
                        m.LineName = v.Name;
                        m.FromDate = model.FromDate;
                        m.ToDate = model.ToDate;
                        m.OrderNo = "No Order";
                        m.Color = "red";
                        lstLine.Add(m);
                    }

                }
            }

            this.DataList = lstLine;
        }

        public DataTable GetLinePlan1(VmPlanReport model)
        {
            List<VmPlanReport> lstLine = new List<VmPlanReport>();
            DataTable ds = new DataTable();
            db = new xOssContext();
            ds.Columns.Add("Line/Date");

            DateTime fDate = model.FromDate;
            DateTime tDate = model.ToDate;
            int totalDay = 0;
            while (fDate<=tDate)
            {
                ++totalDay;
                int m = fDate.Month;
                int d = fDate.Day;
                //int y = fDate.Year;
                //string sDate = d + "/" + m + "/" + y;
                string sDate = d + "/" + m;

                ds.Columns.Add(sDate);
                fDate = fDate.AddDays(1);
            }
            //ds.Columns.Add("LineId",typeof(int));

            int count = 0;
            var getAllLines = db.Plan_ProductionLine.Where(a => a.LineNumber != null).OrderBy(a => a.ID).ToList();
            if (getAllLines.Any())
            {
                foreach (var v in getAllLines)
                {
                    var vPlan = (from t1 in db.Prod_Planning
                                 join t2 in db.Prod_OrderPlanning on t1.Prod_OrderPlanningFK equals t2.ID
                                 join t3 in db.Mkt_BOM on t2.Mkt_BOMFK equals t3.ID
                                 join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                                 where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                                 && t1.StartDate >= model.FromDate && t1.StartDate <= model.ToDate && t1.Plan_ProductionLineFk == v.ID
                                 //&& t1.StartDate >= model.FromDate && t1.FinishDate <= model.ToDate && t1.Plan_ProductionLineFk == v.ID
                                 select new
                                 {
                                     BomId=t1.Mkt_BOMFK,
                                     BuyerPo = t4.BuyerPO,
                                     FromDate = t1.StartDate,
                                     ToDate = t1.FinishDate
                                 }).ToList();

                    DateTime fDate1 = model.FromDate;
                    DateTime tDate1 = model.ToDate;
                    
                    ds.Rows.Add(v.Name);
                    
                    int c = 0;
                    while (fDate1 <= tDate1)
                    {
                        c++;
                        if (vPlan.Any(a => a.FromDate <= fDate1 && a.ToDate >= fDate1))
                        {
                            var data = vPlan.FirstOrDefault(a => a.FromDate <= fDate1 && a.ToDate >= fDate1);

                            string fD = data.FromDate.Day + "-" + data.FromDate.Month + "-" + data.FromDate.Year;
                            string tD = data.ToDate.Day + "-" + data.ToDate.Month + "-" + data.ToDate.Year;

                            ds.Rows[count][c] = data.BuyerPo + "@"+data.BomId + "@" + v.ID + "@" + fD + "@" + tD;
                        }
                        else
                        {
                            ds.Rows[count][c] = "Undefine";
                        }
                        fDate1 = fDate1.AddDays(1);
                    }

                    //ds.Rows[count][++c] = v.ID;

                    ++count;
                }
            }

            return ds;
        }

        public void GetOrderPlanDetails(string Id)
        {
            db = new xOssContext();
            string[] word = Id.Split('@');
            int BomId = 0;
            int LineId = 0;
            int day = 0;
            int month = 0;
            int year = 0;
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            if (word.Any())
            {
                int.TryParse(word[1],out BomId);
                int.TryParse(word[2], out LineId);
                string[] dateFWord = word[3].Split('-');
                string[] dateTWord = word[4].Split('-');

                if (dateFWord.Any())
                {
                    day = 0;
                    month = 0;
                    year = 0;
                    int.TryParse(dateFWord[0], out day);
                    int.TryParse(dateFWord[1], out month);
                    int.TryParse(dateFWord[2], out year);
                    FromDate = new DateTime(year,month,day);
                }
                if (dateTWord.Any())
                {
                    day = 0;
                    month = 0;
                    year = 0;
                    
                    int.TryParse(dateTWord[0], out day);
                    int.TryParse(dateTWord[1], out month);
                    int.TryParse(dateTWord[2], out year);
                    ToDate = new DateTime(year, month, day);
                }
            }

            if (BomId>0 && LineId>0 && FromDate !=null && ToDate !=null)
            {
                var vPlan = (from t1 in db.Prod_Planning
                             join t2 in db.Prod_OrderPlanning on t1.Prod_OrderPlanningFK equals t2.ID
                             join t3 in db.Mkt_BOM on t2.Mkt_BOMFK equals t3.ID
                             join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                             join t5 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t5.ID
                             where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                             && t1.StartDate >= FromDate && t1.FinishDate <= ToDate
                             && t2.Mkt_BOMFK == BomId && t1.Plan_ProductionLineFk == LineId
                             select new VmPlanReport
                             {
                                 LineName=t5.Name,
                                 BOMID = t1.Mkt_BOMFK,
                                 OrderNo = t4.BuyerPO,
                                 OrderQuantityPC = t2.PlanQty,
                                 FromDate = t1.StartDate,
                                 ToDate = t1.FinishDate
                             }).FirstOrDefault();
                this.LineName = vPlan.LineName;
                this.BOMID = vPlan.BOMID;
                this.OrderNo = vPlan.OrderNo;
                this.OrderQuantityPC = vPlan.OrderQuantityPC;
                //this.FromDate = vPlan.FromDate;
                //this.ToDate = vPlan.ToDate;
                this.Range = ShortDateString(vPlan.FromDate) + " To " + ShortDateString(vPlan.ToDate);
                var vQty = db.Prod_PlanAchievment.Where(a => a.AchiveDate >= FromDate && a.AchiveDate <= ToDate && a.Plan_ProductionLineFk == LineId && a.Mkt_BOMFK == BomId);
                this.ProductionQty = vQty.Any() == true ? vQty.Sum(a => a.PlanAchievQty) : 0;
                this.OrderTotalAchiveRemain = this.OrderQuantityPC - this.ProductionQty;
            }

        }
    }
}