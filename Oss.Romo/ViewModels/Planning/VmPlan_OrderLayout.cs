﻿using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Production;

namespace Oss.Romo.ViewModels.Planning
{
    public class VmPlan_OrderLayout
    {
        private xOssContext db;
        public Plan_OrderLayout Plan_OrderLayout { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Prod_MachineType Prod_MachineType { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public IEnumerable<VmPlan_OrderLayout> DataList { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Plan_OrderLayout in db.Plan_OrderLayout
                     join Prod_MachineType in db.Prod_MachineType on Plan_OrderLayout.Prod_MachineTypeFk equals Prod_MachineType.ID
                     join Mkt_BOM in db.Mkt_BOM on Plan_OrderLayout.Mkt_BOMFk equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID

                     select new VmPlan_OrderLayout
                     {
                         Plan_OrderLayout = Plan_OrderLayout,
                         Prod_MachineType = Prod_MachineType,
                         Mkt_BOM = Mkt_BOM,
                         Common_TheOrder = Common_TheOrder
                     }).Where(x => x.Plan_OrderLayout.Active == true).OrderByDescending(x => x.Plan_OrderLayout.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from rc in db.Plan_OrderLayout
                     select new VmPlan_OrderLayout
                     {
                         Plan_OrderLayout = rc
                     }).Where(c => c.Plan_OrderLayout.ID == id).SingleOrDefault();
            this.Plan_OrderLayout = v.Plan_OrderLayout;

        }
        //....ForDelegateMethod...//
        public int Add(int userID)
        {
            Plan_OrderLayout.AddReady(userID);
            return Plan_OrderLayout.Add();
        }
        public bool Edit(int userID)
        {
            return Plan_OrderLayout.Edit(userID);
        }
        public bool Dalete(int userID)
        {

            return Plan_OrderLayout.Delete(userID);
        }
       
      
    }
}