﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Planning;

namespace Oss.Romo.ViewModels.Planning
{
    public class VmPlan_TimeAndActionNotification
    {
        private xOssContext db;
        public IEnumerable<VmCommon_TimeAndAction> DataList { get; set; }
        public Plan_OrderStep Plan_OrderStep;
        public Plan_OrderProcess Plan_OrderProcess;
        public Common_TheOrder Common_TheOrder;
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var todaydate = DateTime.Today;

            var a = (from Plan_OrderStep in db.Plan_OrderStep
                     join Plan_OrderProcess in db.Plan_OrderProcess
                         on Plan_OrderStep.Plan_OrderProcessFK equals Plan_OrderProcess.ID
                     join Common_TheOrder in db.Common_TheOrder
                         on Plan_OrderStep.CommonTheOrderFK equals Common_TheOrder.ID
                     where Plan_OrderStep.PlannedStartDate >= todaydate
                           //&& Plan_OrderStep.PlannedStartDate <= Plan_OrderProcess.PlanStartDayInterval.DateAdd()
                           || Plan_OrderStep.PlannedFinishDate >= todaydate
                     //&& Plan_OrderStep.PlannedFinishDate <= Plan_OrderProcess.PlanStartDayInterval.DateAdd()
                     select new VmCommon_TimeAndAction
                     {
                         Startdate = Plan_OrderStep.PlannedStartDate >= todaydate ? true : false,
                         Enddate = Plan_OrderStep.PlannedFinishDate >= todaydate ? true : false,
                         Plan_OrderStep = Plan_OrderStep,
                         Plan_OrderProcess = Plan_OrderProcess,
                         Common_TheOrder = Common_TheOrder
                     }).Where(x => x.Common_TheOrder.Active == true && x.Plan_OrderStep.Active == true
                                   && x.Plan_OrderProcess.Active == true).OrderByDescending(x => x.Plan_OrderStep.PlannedStartDate)
                .ThenByDescending(x => x.Plan_OrderStep.PlannedFinishDate).ThenBy(x => x.Plan_OrderProcess.StepName).ToList();

           // var d = a.Where(x => x.Plan_OrderStep.PlannedStartDate <= x.Plan_OrderProcess.PlanStartDayInterval.DateAdd()
           //|| x.Plan_OrderStep.PlannedFinishDate <= x.Plan_OrderProcess.PlanEndtDayInterval.DateAdd());

            DataList = a;
        }
    }

    public static class DateFunction
    {
        public static DateTime DateAdd(this int interval)
        {
            return DateTime.Now.AddDays(interval);

        }
    }


    public class VmCommon_TimeAndAction
    {
        public bool Startdate { get; set; }
        public bool Enddate { get; set; }
        public Plan_OrderStep Plan_OrderStep { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
    }
}