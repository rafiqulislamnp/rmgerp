﻿using Oss.Romo.Models;
using Oss.Romo.Models.Planning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Planning
{
    public class VmPlan_OrderProcessCategory
    {
        private xOssContext db;
        public Plan_OrderProcessCategory Plan_OrderProcessCategory { get; set; }
        public IEnumerable<VmPlan_OrderProcessCategory> DataList { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Plan_OrderProcessCategory

                     select new VmPlan_OrderProcessCategory
                     {
                         Plan_OrderProcessCategory = t1

                     }).Where(x => x.Plan_OrderProcessCategory.Active == true).OrderBy(x => x.Plan_OrderProcessCategory.ID).AsEnumerable();
            this.DataList = v;

        }

        public int Add(int userID)
        {
            Plan_OrderProcessCategory.AddReady(userID);
            return Plan_OrderProcessCategory.Add();
        }

        public bool Edit(int userID)
        {
            return Plan_OrderProcessCategory.Edit(userID);
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Plan_OrderProcessCategory
                     select new VmPlan_OrderProcessCategory
                     {
                         Plan_OrderProcessCategory = b

                     }).Where(c => c.Plan_OrderProcessCategory.ID == id).SingleOrDefault();
            this.Plan_OrderProcessCategory = a.Plan_OrderProcessCategory;
        }
    }
}