﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Production
{
	public class VmProd_ProductionSection
	{
        private xOssContext db;
        public IEnumerable<VmProd_ProductionSection> DataList { get; set; }
        public Prod_ProductionSection Prod_ProductionSection { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmProd_ProductionSection()
        {
            db = new xOssContext();
            InitialDataLoad();
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            var v = (from t1 in db.Prod_ProductionSection
                     select new VmProd_ProductionSection
                     {
                         Prod_ProductionSection = t1
                     }).Where(x => x.Prod_ProductionSection.Active == true).OrderByDescending(x => x.Prod_ProductionSection.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from rc in db.Prod_ProductionSection
                     select new VmProd_ProductionSection
                     {
                         Prod_ProductionSection = rc
                     }).SingleOrDefault(c => c.Prod_ProductionSection.ID == id);
            if (v != null) this.Prod_ProductionSection = v.Prod_ProductionSection;
        }

        public int Add(int userID)
        {
            Prod_ProductionSection.AddReady(userID);
            return Prod_ProductionSection.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_ProductionSection.Edit(userID);
        }

        public bool Delete(int userID)
        {           
            return Prod_ProductionSection.Delete(userID);          
        }
    }
}