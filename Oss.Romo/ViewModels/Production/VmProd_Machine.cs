﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_Machine
    {
        private xOssContext db;
        public IEnumerable<VmProd_Machine> DataList { get; set; }
        public Prod_Machine Prod_Machine { get; set; }

        public Prod_MachineType Prod_MachineType { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Prod_Machine 
                     join t2 in db.Prod_MachineType
                     on t1.Prod_MachineTypeFK equals t2.ID
                     select new VmProd_Machine
                     {   Prod_Machine=t1,
                         Prod_MachineType = t2
                     }).Where(x => x.Prod_Machine.Active == true).OrderByDescending(x => x.Prod_Machine.ID).AsEnumerable();
            this.DataList = a;

        }

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Machine
                     join t2 in db.Prod_MachineType
                     on t1.Prod_MachineTypeFK equals t2.ID
                     select new VmProd_Machine
                     {
                         Prod_Machine = t1,
                         Prod_MachineType = t2
                     }).Where(c => c.Prod_Machine.ID == iD).SingleOrDefault();
            this.Prod_Machine = v.Prod_Machine;
            this.Prod_MachineType = v.Prod_MachineType;

        }

        public int Add(int userID)
        {
            Prod_Machine.AddReady(userID);

            return Prod_Machine.Add();

        }
        public bool Edit(int userID)
        {
            return Prod_Machine.Edit(userID);
        }



    }
}