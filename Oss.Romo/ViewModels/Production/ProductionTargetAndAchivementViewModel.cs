﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.Production
{
    public class ProductionTargetAndAchivementViewModel
    {
        public string LineName { get; set; }
        public int PerLineMachineQty { get; set; }
        public string OrderNo { get; set; }
        public int RunningMachine { get; set; }
        public int NumOperator { get; set; }
        public int NumHelper { get; set; }
        public int PerHourTarget { get; set; }
        public int? TotalAchivement { get; set; }
        public decimal SMV { get; set; }
        public decimal WorkingHour { get; set; }
        public string ItemName { get; set; }
        public string Remarks { get; set; }
    }
}