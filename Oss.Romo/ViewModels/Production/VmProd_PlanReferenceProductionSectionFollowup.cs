﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;

namespace Oss.Romo.ViewModels.Production
{
	public class VmProd_PlanReferenceProductionSectionFollowup
	{
        private xOssContext db;
	    public int OrderProcessID { get; set; }
        public IEnumerable<VmProd_PlanReferenceProductionSectionFollowup> DataList { get; set; }
        public Prod_PlanReferenceProductionSectionFollowup Prod_PlanReferenceProductionSectionFollowup { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public Prod_PlanReferenceProduction Prod_PlanReferenceProduction { get; set; }
        public Prod_PlanReference Prod_PlanReference { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public Prod_PlanReferenceProductionSection Prod_PlanReferenceProductionSection { get; set; }
        public Plan_ProductionLine Plan_ProductionLine { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        [DataType(DataType.MultilineText)]
        public string BuyerPO { get; set; }
        public int Count { get; set; }
        public int BOMID { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public Prod_TransitionItemInventory Prod_TransitionItemInventory { get; set; }
        public VmProd_PlanReferenceProductionSectionFollowup()
        {
            db = new xOssContext();

            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad(int id)
        {
            var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
                     join t3 in db.Prod_TransitionItemInventory on t2.ID equals t3.Prod_PlanReferenceProductionSectionFk

                    
                     select new VmProd_PlanReferenceProductionSectionFollowup
                     {
                         Prod_PlanReferenceProductionSectionFollowup = t1,
                         Prod_TransitionItemInventory = t3,
                         Prod_PlanReferenceProductionSection = t2
                     }).Where(x => x.Prod_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFk==id && x.Prod_PlanReferenceProductionSectionFollowup.Active == true).OrderByDescending(x => x.Prod_PlanReferenceProductionSectionFollowup.ID).AsEnumerable();
            this.DataList = v;

        }

	   
        public void SelectSingle(string iD)
	    {
	        int id = 0;
	        iD = this.VmControllerHelper.Decrypt(iD);
	        Int32.TryParse(iD, out id);
	        db = new xOssContext();
	        var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
	                 join t3 in db.Prod_PlanReferenceProduction on t2.Prod_PlanReferenceProductionFk equals t3.ID
	                 join t4 in db.Prod_PlanReference on t3.Prod_PlanReferenceFK equals t4.ID
                     join t5 in db.Plan_OrderProcess on t3.Plan_OrderProcessFK equals t5.ID


                     select new VmProd_PlanReferenceProductionSectionFollowup
                     {
                         Prod_PlanReferenceProductionSectionFollowup = t1,
                         Prod_PlanReferenceProductionSection = t2,
                         Prod_PlanReferenceProduction = t3,
                         Prod_PlanReference = t4,
                         Plan_OrderProcess = t5
                     }).SingleOrDefault(c => c.Prod_PlanReferenceProductionSectionFollowup.ID == id);
	        if (v != null) this.Prod_PlanReferenceProductionSectionFollowup = v.Prod_PlanReferenceProductionSectionFollowup;
            if (v != null) this.Prod_PlanReferenceProductionSection = v.Prod_PlanReferenceProductionSection;
            if (v != null) this.Prod_PlanReferenceProduction = v.Prod_PlanReferenceProduction;
            if (v != null) this.Prod_PlanReference = v.Prod_PlanReference;
            if (v != null) this.Plan_OrderProcess = v.Plan_OrderProcess;
        }
        public int? GetFollowupQuantityByID(int id)
        {
        
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     where t1.Active == true && t1.Prod_PlanReferenceProductionSectionFk == id
                     select t1.Quantity).ToList();
            int? Quantity = 0;
            if (v != null)
            {
                Quantity = v.Sum();
            }
            return Quantity;
        }

        public void GetSectionByID(int id)
        {

            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSection
                  

                     select new VmProd_PlanReferenceProductionSectionFollowup
                     {
                         Prod_PlanReferenceProductionSection = t1
                     }).SingleOrDefault(c => c.Prod_PlanReferenceProductionSection.ID == id);
            if (v != null) this.Prod_PlanReferenceProductionSection = v.Prod_PlanReferenceProductionSection;

        }
        public VmProd_PlanReferenceProductionSectionFollowup SelectSingleJoined(int id)
        {

            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSection
                join t2 in db.Prod_PlanReferenceProduction on t1.Prod_PlanReferenceProductionFk equals t2.ID
                join t3 in db.Prod_PlanReference on t2.Prod_PlanReferenceFK equals t3.ID
                join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                join t5 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t5.ID
                join t6 in db.Common_TheOrder on t4.Common_TheOrderFk equals t6.ID
                select new VmProd_PlanReferenceProductionSectionFollowup
                {
                    Prod_PlanReferenceProductionSection = t1,
                    Mkt_BOM = t4,
                    Prod_PlanReferenceProduction = t2,
                    Plan_ProductionLine = t5,
                    BuyerPO = t6.CID +"/" + t4.Style,
                   
                }).FirstOrDefault(x => x.Prod_PlanReferenceProductionSection.ID == id);
            return v;

        }

        public int Add(int userID)
        {
            Prod_PlanReferenceProductionSectionFollowup.AddReady(userID);
            return Prod_PlanReferenceProductionSectionFollowup.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_PlanReferenceProductionSectionFollowup.Edit(userID);
        }

        public bool Delete(int userID)
        {           
            return Prod_PlanReferenceProductionSectionFollowup.Delete(userID);          
        }


	    public void GetProd_PlanReferenceProductionSectionFollowuBySection(int? id)
	    {
	       var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                    join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
                    join t3 in db.Prod_TransitionItemInventory on t2.ID equals t3.Prod_PlanReferenceProductionSectionFk

                    select new VmProd_PlanReferenceProductionSectionFollowup
                     {
                        Prod_PlanReferenceProductionSectionFollowup = t1,
                        Prod_TransitionItemInventory = t3,
                        Prod_PlanReferenceProductionSection = t2
                    }).FirstOrDefault(x => x.Prod_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFk == id);
            if (v != null) this.Prod_TransitionItemInventory = v.Prod_TransitionItemInventory;

        }

        public List<ProductionTargetAndAchivementViewModel> GetProductionTargetAndAchivement()
        {
            db = new xOssContext();
            var a = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProduction on t2.Prod_PlanReferenceProductionFk equals t3.ID
                     join t4 in db.Plan_ProductionLine on t2.Plan_ProductionLineFk equals t4.ID
                     join t5 in db.Prod_PlanReference on t3.Prod_PlanReferenceFK equals t5.ID
                     join t6 in db.Mkt_BOM on t5.Mkt_BOMFK equals t6.ID
                     join t7 in db.Common_TheOrder on t6.Common_TheOrderFk equals t7.ID
                     join t8 in db.Mkt_Item on t6.Mkt_ItemFK equals t8.ID
                     where t2.Date == DateTime.Today
                     select new
                     {
                         LineName = t4.Name,
                         PerLineMachineQty = t3.MachineQuantity,
                         OrderNo = t7.BuyerPO,
                         ItemName = t8.Name,
                         RunningMachine = t2.MachineRunning,
                         NumOperator = t3.NoOfOpetator,
                         NumHelper = t3.NoOfHelper,
                         SMV = t3.SMB,
                         PerHourTarget = t3.TargetPerHour,
                         TotalAchivement = t1.Quantity,
                         WorkingHour = t3.WorkingHour,
                         Remarks = t2.Description
                     }).GroupBy(x => new
                     {
                         x.LineName,
                         x.PerLineMachineQty,
                         x.OrderNo,
                         x.ItemName,
                         x.RunningMachine,
                         x.NumOperator,
                         x.NumHelper,
                         x.PerHourTarget,
                         x.SMV,
                         x.WorkingHour,
                         x.Remarks
                     }).Select(g =>
                         new ProductionTargetAndAchivementViewModel
                         {
                             LineName = g.Key.LineName,
                             PerLineMachineQty = g.Key.PerLineMachineQty,
                             OrderNo = g.Key.OrderNo,
                             RunningMachine = g.Key.RunningMachine,
                             NumOperator = g.Key.NumOperator,
                             NumHelper = g.Key.NumHelper,
                             PerHourTarget = g.Key.PerHourTarget,
                             TotalAchivement = g.Sum(x => x.TotalAchivement),
                             SMV = g.Key.SMV,
                             ItemName = g.Key.ItemName,
                             WorkingHour = g.Key.WorkingHour,
                             Remarks = g.Key.Remarks
                         }).OrderBy(o => o.LineName);

            return a.ToList();
        }
    }
}