﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_OrderPlanning
    {
        private xOssContext db;
        public IEnumerable<VmProd_OrderPlanning> DataList { get; set; }
        public int BOMID { get; set; }
        public string BuyerPo { get; set; }
        //public VmControllerHelper VmControllerHelper { get; set; }
        public Prod_OrderPlanning Prod_OrderPlanning { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public decimal OrderQuantityPC { get; set; }
        public string ShipmentDate { get; set; }
             
        public VmProd_OrderPlanning()
        {
            db = new xOssContext();
            //VmControllerHelper = new VmControllerHelper();
        }
        
        public void InitialDataLoad()
        {
            var v = (from t1 in db.Prod_OrderPlanning
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Mkt_Item on t4.Mkt_ItemFK equals t3.ID
                     where t1.Active == true 
                     select new VmProd_OrderPlanning
                     {
                         Prod_OrderPlanning = t1,
                         Mkt_Item = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         OrderQuantityPC = t1.PlanQty //t4.QPack * t4.Quantity
                     }).Where(x => x.Prod_OrderPlanning.Active == true).OrderByDescending(x => x.Prod_OrderPlanning.ID).AsEnumerable();
            this.DataList = v;
        }

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_OrderPlanning
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Mkt_Item on t4.Mkt_ItemFK equals t3.ID
                     select new VmProd_OrderPlanning
                     {
                         Prod_OrderPlanning = t1,
                         Mkt_Item = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5
                     }).SingleOrDefault(c => c.Prod_OrderPlanning.ID == iD);
            if (v != null) this.Prod_OrderPlanning = v.Prod_OrderPlanning;
            if (v != null) this.Mkt_Item = v.Mkt_Item;
            if (v != null) this.Mkt_BOM = v.Mkt_BOM;
            if (v != null) this.Common_TheOrder = v.Common_TheOrder;
        }

        public int Add(int userID)
        {
            Prod_OrderPlanning.AddReady(userID);
            return Prod_OrderPlanning.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_OrderPlanning.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_OrderPlanning.Delete(userID);
        }
    }
}