﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_SMVLayout
    {
        private xOssContext db;
        public IEnumerable<VmProd_SMVLayout> DataList { get; set; }
        public Prod_SMVLayout Prod_SMVLayout { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_SMVLayout
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFk equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Mkt_Item on t2.Mkt_ItemFK equals t4.ID
                     where t1.Active == true && t2.Active==true && t3.Active==true && t4.Active==true
                     select new VmProd_SMVLayout
                     {
                         Prod_SMVLayout = t1,
                         Mkt_BOM = t2,
                         Common_TheOrder = t3,
                         Mkt_Item = t4
                     }).OrderByDescending(x => x.Prod_SMVLayout.ID).AsEnumerable();
            this.DataList = v;
        }
        
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_SMVLayout
                     select new VmProd_SMVLayout
                     {
                         Prod_SMVLayout = t1
                     }).Where(c => c.Prod_SMVLayout.ID == id).SingleOrDefault();
            this.Prod_SMVLayout = v.Prod_SMVLayout;
        }

        public int Add(int userID)
        {
            Prod_SMVLayout.TotalSMV = Prod_SMVLayout.OperatorSMV + Prod_SMVLayout.HelperSMV;
            Prod_SMVLayout.TotalWorker = Prod_SMVLayout.NoOfOperator + Prod_SMVLayout.NoOfHelper;
            Prod_SMVLayout.AddReady(userID);
            return Prod_SMVLayout.Add();
        }

        public bool Edit(int userID)
        {
            Prod_SMVLayout.TotalSMV = Prod_SMVLayout.OperatorSMV + Prod_SMVLayout.HelperSMV;
            Prod_SMVLayout.TotalWorker = Prod_SMVLayout.NoOfOperator + Prod_SMVLayout.NoOfHelper;
            return Prod_SMVLayout.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_SMVLayout.Delete(userID);
        }
    }
}