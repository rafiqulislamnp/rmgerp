﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Store;
using System.Data.Entity;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_PlanReferenceProductionSection
    {
        private xOssContext db;
        public int Prod_PlanReferenceProductionID { get; set; }
        public IEnumerable<VmProd_PlanReferenceProductionSection> DataList { get; set; }
        public IEnumerable<VmProd_PlanReferenceProductionSection> DataList2 { get; set; }
        public IEnumerable<VmProd_PlanReferenceProductionSection> DataList3 { get; set; }
        public Prod_PlanReferenceProductionSection Prod_PlanReferenceProductionSection { get; set; }
        public Mkt_OrderColorAndSizeRatio Mkt_OrderColorAndSizeRatio { get; set; }
        public Plan_ProductionLine Plan_ProductionLine { get; set; }
        public Prod_PlanReferenceProduction Prod_PlanReferenceProduction { get; set; }
        public VmProd_PlanReferenceProduction VmProd_PlanReferenceProduction { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public int BOMID { get; set; }
        public int OrderDeliveryScheduleId { get; set; }
        public decimal? SampleCount { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Rejected { get; set; }
        public int? Quantity { get; set; }
        public decimal? TotalQuantity { get; set; }
        public decimal? Ratio { get; set; }
        public string Size { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public Prod_LineChief Prod_LineChief { get; set; }
        public Prod_SVName Prod_SVName { get; set; }
        public DataTable DataTable { get; set; }
        public Prod_PlanReference Prod_PlanReference { get; set; }
        public Plan_ProductionLineRunning Plan_ProductionLineRunning { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public class CurrentProductionRunningProcess
        {
            public string LineName { get; set; }
            public string StartTime { get; set; }
            public int IsRunning { get; set; }
            public decimal Percent { get; set; }
            public string FinishTime { get; set; }
            public string Info { get; set; }
        }

        public List<CurrentProductionRunningProcess> CurrentProductionRunningProcessList { get; set; }

        public VmProd_PlanReferenceProductionSection()
        {
            db = new xOssContext();
            VmControllerHelper = new VmControllerHelper();
        }


        public void InitialDataLoad(int id)
        {
            var v = (from t1 in db.Prod_PlanReferenceProductionSection
                     join t2 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProduction on t1.Prod_PlanReferenceProductionFk equals t3.ID
                     join t4 in db.Prod_PlanReferenceProductionSectionFollowup on t1.ID equals t4.Prod_PlanReferenceProductionSectionFk
                     into f
                     from t4 in f.DefaultIfEmpty()
                     where t3.ID == id && t1.Active == true
                     select new VmProd_PlanReferenceProductionSection
                     {
                         Prod_PlanReferenceProductionSection = t1,
                         Plan_ProductionLine = t2,
                         Prod_PlanReferenceProduction = t3,
                         SampleCount = t4 == null ? 0 : f.Sum(x => x.SampleCount),
                        
                         Rejected = t4 == null ? 0 : f.Sum(x => x.Rejected),
                         Quantity = t4 == null ? 0 : f.Sum(x => x.Quantity)
                         
                     }).OrderByDescending(x => x.Prod_PlanReferenceProductionSection.ID).AsEnumerable();
            this.DataList = v;

        }
        public void GetSizeAndColor(int id)
        {
            var a = (from t1 in db.Prod_PlanReferenceProduction
                     join t2 in db.Prod_PlanReference on t1.Prod_PlanReferenceFK equals t2.ID
                     join t3 in db.Mkt_OrderDeliverySchedule on t2.Mkt_OrderDeliveryScheduleFK equals t3.ID
                     join t4 in db.Mkt_OrderColorAndSizeRatio on t3.ID equals t4.OrderDeliveryScheduleFk
                     where t1.ID == id
                     select new VmProd_PlanReferenceProductionSection
                     {
                         Mkt_OrderColorAndSizeRatio = t4
                     }).Where(x => x.Mkt_OrderColorAndSizeRatio.Active == true && x.Mkt_OrderColorAndSizeRatio.Active == true).OrderByDescending(x => x.Mkt_OrderColorAndSizeRatio.ID).AsEnumerable();
            this.DataList2 = a;
        }

        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from rc in db.Prod_PlanReferenceProductionSection
                     select new VmProd_PlanReferenceProductionSection
                     {
                         Prod_PlanReferenceProductionSection = rc
                     }).SingleOrDefault(c => c.Prod_PlanReferenceProductionSection.ID == id);
            if (v != null) this.Prod_PlanReferenceProductionSection = v.Prod_PlanReferenceProductionSection;
        }

        public int Add(int userID)
        {
            Prod_PlanReferenceProductionSection.AddReady(userID);
            return Prod_PlanReferenceProductionSection.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_PlanReferenceProductionSection.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_PlanReferenceProductionSection.Delete(userID);
        }

        public DataTable GetColorAndSizeDimension(int bomId, int pRefProdid)
        {
            db = new xOssContext();

            DataTable dt = new DataTable();
            var data = db.Mkt_OrderColorAndSizeRatio.Distinct().ToList().Where(x => x.Active == true);
            decimal processloss = (from t1 in db.Prod_PlanReferenceProduction
                                   where t1.ID == pRefProdid
                                   select (decimal)t1.ExtraCutting).FirstOrDefault();

            var d = (from f in data
                     where f.Active == true && f.Mkt_BOMFk == bomId
                     group f by new { f.Color } into Group
                     where Group.Count() > 0
                     select new
                     {
                         Group.Key.Color,
                         ColumnName = Group.GroupBy(f => f.Size).Select
                         (m => new
                         {
                             Sub = m.Key,
                             Score = Math.Ceiling(GetExtraCutting(m.Sum(c => c.Quantity), processloss))
                         })
                     }).ToList();

            var sub = (from f in db.Mkt_OrderColorAndSizeRatio.AsEnumerable()
                       where f.Active == true && f.Mkt_BOMFk == bomId
                       select new
                       {
                           f.Size
                       }).Distinct().ToList();

            ArrayList objDataColumn = new ArrayList();

            if (data.Count() > 0)
            {
                objDataColumn.Add("Color");
                for (int i = 0; i < sub.Count; i++)
                {
                    objDataColumn.Add(sub[i].Size);
                }

            }

            //Add column name dynamically
            for (int i = 0; i < objDataColumn.Count; i++)
            {
                dt.Columns.Add(objDataColumn[i].ToString());
            }

            //Add data into datatable
            for (int i = 0; i < d.Count; i++)
            {
                List<string> datalist = new List<string>();
                datalist.Add(d[i].Color.ToString());

                var res = d[i].ColumnName.ToList();
                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;

                    for (int j = 0; j < res.Count; j++)
                    {
                        string column = dt.Columns[m].ToString();

                        if (res[j].Sub == column)
                        {
                            datalist.Add(res[j].Score.ToString());
                            count++;
                        }
                    }

                    if (count == 0)
                    {
                        string val = "0";
                        datalist.Add(val);
                    }
                }

                dt.Rows.Add(datalist.ToArray<string>());

            }
            return dt;
        }


        public decimal GetExtraCutting(decimal quantity, decimal percent)
        {
            decimal a = ((quantity * percent) / 100);
            return a + quantity;
        }

        public IEnumerable<VmProd_PlanReferenceProductionSection> GetSizeAndRatioByColor(int pRefProdid, int bomId, string color, int orderDeliveryScheduleFk)
        {
            db = new xOssContext();

            var a = (from t2 in db.Mkt_OrderColorAndSizeRatio
                     join t3 in db.Mkt_OrderDeliverySchedule on t2.OrderDeliveryScheduleFk equals t3.ID
                     where t2.Active == true && t2.Mkt_BOMFk == bomId && t2.Color == color && t2.OrderDeliveryScheduleFk == orderDeliveryScheduleFk
                     select new VmProd_PlanReferenceProductionSection
                     {
                         Mkt_OrderColorAndSizeRatio = t2
                     }).AsEnumerable();
            decimal totalratio = Convert.ToDecimal((from t2 in db.Mkt_OrderColorAndSizeRatio
                join t3 in db.Mkt_OrderDeliverySchedule on t2.OrderDeliveryScheduleFk equals t3.ID
                where
                    t2.Active == true && t3.Active == true && t2.Mkt_BOMFk == bomId && t2.Color == color &&
                    t2.OrderDeliveryScheduleFk == orderDeliveryScheduleFk
                select new
                {
                    Ratio = t2.Ratio
                }).Sum(x => x.Ratio));
            //int[] n = new int[a.Count()]; /* n is an array of 10 integers */
            //int q = 0;
            //foreach (var x in a)
            //{
            //    n[q] = x.Mkt_OrderColorAndSizeRatio.Ratio.Value;
            //    q++;
            //}
            //var vxiu = GCD(n);

            List<VmProd_PlanReferenceProductionSection> ListVmplanReferenceProductionSection = new List<VmProd_PlanReferenceProductionSection>();
            foreach (VmProd_PlanReferenceProductionSection y in a)
            {
                y.Size = y.Mkt_OrderColorAndSizeRatio.Size;
             
                
                y.TotalQuantity = y.Mkt_OrderColorAndSizeRatio.Quantity; //Decimal.Divide((this.Prod_PlanReferenceProductionSection.MarkerPics * this.Prod_PlanReferenceProductionSection.Layer), totalratio) * y.Mkt_OrderColorAndSizeRatio.Ratio;
                                                                     // y.Quantity = Decimal.Divide(((this.Prod_PlanReferenceProductionSection.MarkerPics * this.Prod_PlanReferenceProductionSection.Layer) / totalratio) * y.Mkt_OrderColorAndSizeRatio.Ratio;
                ListVmplanReferenceProductionSection.Add(y);
            }
            return ListVmplanReferenceProductionSection;
        }

        //public ActionResult Test(List<Test> test)
        //{

        //    var x = GCD(new[] { 10, 20, 30, 40, 51 });
        //    return View();
        //}

        public int GCD(int a, int b)
        {
            return b == 0 ? a : GCD(b, a % b);
        }

        public int GCD(int[] integerSet)
        {
            return integerSet.Aggregate(GCD);
        }


        public int GetorderProcessById(int pRefProdid)
        {
            db = new xOssContext();

            var a = (from t1 in db.Plan_OrderProcess
                     join t2 in db.Prod_PlanReferenceProduction on t1.ID equals t2.Plan_OrderProcessFK
                     where t1.Active == true && t2.Active == true && t2.ID == pRefProdid
                     select
                         t2.Plan_OrderProcessFK
            ).FirstOrDefault();
            
            return a;
        }

        public decimal GetExtraById(int pRefProdid)
        {
            db = new xOssContext();

            var a = (from t1 in db.Plan_OrderProcess
                     join t2 in db.Prod_PlanReferenceProduction on t1.ID equals t2.Plan_OrderProcessFK
                     where t1.Active == true && t2.Active == true && t2.ID == pRefProdid
                     select
                         t2.ExtraCutting
            ).FirstOrDefault();

            return a;
        }

        public void SelectSinglePlanRefProdByID(int id)
        {

            db = new xOssContext();
            var v = (from rc in db.Prod_PlanReferenceProduction
                     join cc in db.Prod_PlanReference on rc.Prod_PlanReferenceFK equals cc.ID
                     select new VmProd_PlanReferenceProductionSection
                     {
                         Prod_PlanReferenceProduction = rc,
                         Prod_PlanReference = cc
                     }).SingleOrDefault(c => c.Prod_PlanReferenceProduction.ID == id);
            if (v != null) this.Prod_PlanReferenceProduction = v.Prod_PlanReferenceProduction;
            if (v != null) this.Prod_PlanReference = v.Prod_PlanReference;
        }



        public void DailyTargetDataLoad(int rid, int id)
        {
            var date = DateTime.Now.Date;
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSection
                     join t2 in db.Prod_PlanReferenceProduction on t1.Prod_PlanReferenceProductionFk equals t2.ID
                     join t4 in db.Prod_PlanReference on t2.Prod_PlanReferenceFK equals t4.ID
                     join t6 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t6.ID
                     join t7 in db.Prod_LineChief on t1.Prod_LineChiefFk equals t7.ID
                     join t8 in db.Prod_SVName on t1.Prod_SVNameFk equals t8.ID
                     join t3 in db.Plan_OrderProcess on t2.Plan_OrderProcessFK equals t3.ID
                     where t4.Mkt_BOMFK == id && t4.ID == rid
                     select new VmProd_PlanReferenceProductionSection
                     {
                         Prod_PlanReferenceProductionSection = t1,
                         Prod_PlanReferenceProduction = t2,
                         Plan_OrderProcess = t3,
                         Prod_PlanReference = t4,
                         Plan_ProductionLine = t6,
                         Prod_LineChief = t7,
                         Prod_SVName = t8

                     }).Where(x => x.Prod_PlanReferenceProductionSection.Active == true &&
                                   date >= x.Prod_PlanReferenceProductionSection.StartDate && date <= x.Prod_PlanReferenceProductionSection.FinishDate
                                   )
                       .OrderByDescending(x => x.Prod_PlanReferenceProductionSection.ID).AsEnumerable()
                       .GroupBy(m => m.Prod_PlanReferenceProductionSection.ID)
                       .Select(x => x.First());
            this.DataList = v;

            //TotalTargetSummary = v.Sum(x => x.DailyTotalTarget);
            //FillUpTotalTarget = v.Sum(x => x.Prod_DailyTarget.FillUp);
            //TotalDue = v.Sum(x => x.DueTarget);

            //if (TotalTargetSummary == FillUpTotalTarget)
            //{
            //    IsEqual = "alert-success";
            //}
            //else
            //{
            //    IsEqual = "alert-danger";
            //}
            //foreach (var item in v)
            //{
            //    this.StepName = item.Plan_ProductionStep.StepName;
            //    this.StepID = item.Plan_ProductionStep.ID;
            //    if (item.DueTarget == 0)
            //    {
            //        DailyTargetFillUp = "alert-success";
            //    }
            //    else
            //    {
            //        DailyTargetFillUp = "alert-danger";
            //    }
            //}

        }



        internal void GetProd_ProductionStep()
        {
            db = new xOssContext();
            var a = (from t1 in db.Prod_PlanReferenceProductionSection
                     join t2 in db.Plan_ProductionLine
                     on t1.Plan_ProductionLineFk equals t2.ID
                     join t3 in db.Plan_OrderProcess
                     on t2.Plan_OrderProcessFK equals t3.ID
                     where t3.ID == 32 && (t1.StartDate > DateTime.Today)

                     select new VmProd_PlanReferenceProductionSection
                     {
                         Prod_PlanReferenceProductionSection = t1,
                         Plan_ProductionLine = t2,

                     }).Where(x => x.Prod_PlanReferenceProductionSection.Active == true).OrderByDescending(x => x.Prod_PlanReferenceProductionSection.ID).AsEnumerable();

            this.DataList = a;


            var b = (from t1 in db.Prod_PlanReferenceProductionSection
                     join t2 in db.Plan_ProductionLine
                     on t1.Plan_ProductionLineFk equals t2.ID
                     join t3 in db.Plan_OrderProcess
                     on t2.Plan_OrderProcessFK equals t3.ID
                     where t3.ID == 32 && (t1.StartDate > DateTime.Today)
                     //group new { t1, t2, t3 } by t2.Name into grpList select grpList).ToList();
                     select new VmProd_PlanReferenceProductionSection
                     {
                         Prod_PlanReferenceProductionSection = t1,
                         Plan_ProductionLine = t2,

                     }).Where(x => x.Prod_PlanReferenceProductionSection.Active == true)
                     .GroupBy(x => x.Plan_ProductionLine.Name).Select(x => x.FirstOrDefault());

            this.DataList2 = b;

        }

        internal void GetProd_ProductionReferenceProgressBar()
        {
            db = new xOssContext();
            var a = this.DataList.OrderByDescending(t => t.Prod_PlanReferenceProductionSection.ID);
            DataTable dt = new DataTable("");
            if (a.Count() != 0)
            {
                var maxDate = (from t1 in a select t1.Prod_PlanReferenceProductionSection.FinishDate).Max(x => x.Date);
                var minDate = (from t1 in a select t1.Prod_PlanReferenceProductionSection.StartDate).Min(x => x.Date);


                dt.Columns.Add("Line Name", typeof(string));

                for (double i = 0; i <= (maxDate - minDate).TotalDays; i++)
                {
                    dt.Columns.Add(minDate.AddDays(i).ToString().Substring(0, 10), typeof(string));
                }

                var b = this.DataList2;

                foreach (VmProd_PlanReferenceProductionSection vp in b)
                {

                    var list = (from t1 in db.Prod_PlanReferenceProductionSection
                                join t2 in db.Plan_ProductionLine
                                on t1.Plan_ProductionLineFk equals t2.ID

                                where t1.Plan_ProductionLineFk == vp.Prod_PlanReferenceProductionSection.Plan_ProductionLineFk

                                select new VmProd_PlanReferenceProductionSection
                                {
                                    Prod_PlanReferenceProductionSection = t1,
                                    Plan_ProductionLine = t2,

                                }).Where(x => x.Prod_PlanReferenceProductionSection.Active == true)
                               .OrderByDescending(x => x.Prod_PlanReferenceProductionSection.ID).AsEnumerable();
                    this.DataList3 = list;

                    var c = this.DataList3;

                    DataRow theRow = dt.NewRow();
                    theRow[0] = vp.Plan_ProductionLine.Name;
                    int count = 0, rowCount = 0;
                    bool running = false;

                    foreach (DataColumn cl in dt.Columns)
                    {
                        if (count >= 1)
                        {
                            foreach (VmProd_PlanReferenceProductionSection vpp in c)
                            {
                                if (cl.ColumnName == vpp.Prod_PlanReferenceProductionSection.StartDate.ToString().Substring(0, 10))
                                {
                                    theRow[count] = "#";// cl.ColumnName;
                                    running = true;
                                }
                                else if (cl.ColumnName == vpp.Prod_PlanReferenceProductionSection.FinishDate.ToString().Substring(0, 10))
                                {
                                    theRow[count] = "#";// cl.ColumnName;
                                    running = false;
                                }
                                else if (running == true)
                                {
                                    theRow[count] = "#";// cl.ColumnName;
                                }
                            }

                        }
                        count++;
                    }

                    dt.Rows.InsertAt(theRow, rowCount++);
                }

                DataTable = dt;
            }
        }


        internal void GetProd_ProdunctionRunningStep()
        {
            var date = DateTime.Today.Date;
            db = new xOssContext();
            //var a = (

            //    from t1 in db.Prod_PlanReferenceProductionSection
            //    join t2 in db.Prod_PlanReferenceProduction on t1.Prod_PlanReferenceProductionFk equals t2.ID
            //    join t3 in db.Prod_PlanReference on t2.Prod_PlanReferenceFK equals t3.ID
            //    join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
            //    join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
            //    join t6 in db.Plan_OrderProcess on t2.Plan_OrderProcessFK equals t6.ID
            //    join t7 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t7.ID
            //    where t6.ID == 32

            //select new VmProd_PlanReferenceProductionSection
            //    {
            //        Prod_PlanReferenceProductionSection = t1,
            //        Plan_ProductionLine = t7,
            //        Common_TheOrder = t5,
            //        Mkt_BOM = t4
            //    }).Where(x => x.Prod_PlanReferenceProductionSection.Active == true &&
            //      date == x.Prod_PlanReferenceProductionSection.Date).AsEnumerable();


            var a = db.Plan_ProductionLine.Where(x => x.Plan_OrderProcessFK == 32);

           

            List<CurrentProductionRunningProcess> list = new List<CurrentProductionRunningProcess>();

            foreach (var x  in a)
            {
                CurrentProductionRunningProcess ab = new CurrentProductionRunningProcess();
                ab.LineName =x.Name;
                ab.StartTime = GetPlan_ProductionLineRunningByID(x.ID);
                ab.IsRunning = GetRunningStatus(x.ID);
                ab.Info = GetOrders(x.ID);
                ab.FinishTime = GetEstimationTime(x.ID);
                ab.Percent = GetPercentage(x.ID);
              
                list.Add(ab);
            }

            this.CurrentProductionRunningProcessList = list;
        }

        private Decimal GetPercentage(int iD)
        {
            int Percentage = 0;
            int TotalPercentage = 0;
            db = new xOssContext();
            var a = (from t1 in db.Prod_PlanReferenceProductionSection
                     join t2 in db.Prod_PlanReferenceProduction on t1.Prod_PlanReferenceProductionFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProductionSectionFollowup on t1.ID equals t3.Prod_PlanReferenceProductionSectionFk
                     where t1.Plan_ProductionLineFk == iD && DbFunctions.TruncateTime(t1.StartDate) == DateTime.Today
                     select new
                     {
                         //SMV = t2.SMB,
                         PlanQty = t1.Quantity,
                         DoneQty = t3.Quantity

                     }).ToList();

            if (a != null)
            {
                foreach (var val in a)
                {
                    Percentage = Convert.ToInt32(((val.DoneQty) * (100)) / val.PlanQty);
                    TotalPercentage = TotalPercentage + Percentage;
                }

            }

            return TotalPercentage;
        }

        private string GetEstimationTime(int iD)
        {
            decimal EstimationTime = 0;
            decimal estTotalTime = 0;
            db = new xOssContext();
            var a = (from t1 in db.Prod_PlanReferenceProductionSection
                     join t2 in db.Prod_PlanReferenceProduction on t1.Prod_PlanReferenceProductionFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProductionSectionFollowup on t1.ID equals t3.Prod_PlanReferenceProductionSectionFk
                     where t1.Plan_ProductionLineFk == iD && DbFunctions.TruncateTime(t1.StartDate) == DateTime.Today
                     select new
                     {
                         SMV = t2.SMB,
                         PlanQty=t1.Quantity,
                         DoneQty=t3.Quantity

                     }).ToList();

            if (a != null)
            {
                foreach(var val in a)
                {
                    EstimationTime = Convert.ToDecimal(((val.PlanQty) - (val.DoneQty)) / val.SMV);
                    estTotalTime = estTotalTime + EstimationTime;
                }             
            }

            return estTotalTime.ToString("0.00");
        }

        private string GetOrders(int id)
        {
            db = new xOssContext();   
            
            var xx = db.Plan_ProductionLineRunning.Where(x => x.Plan_ProductionLineFK == id && DbFunctions.TruncateTime(x.Date) == DateTime.Today)
            .OrderByDescending(x => x.ID).Select(x => x.Plan_ProductionLineFK).Take(1).SingleOrDefault();

            int Plan_ProductionLine = Convert.ToInt32(xx);

            var a = (from t1 in db.Plan_ProductionLine
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.ID equals t2.Plan_ProductionLineFk
                     join t3 in db.Prod_PlanReferenceProduction on t2.Prod_PlanReferenceProductionFk equals t3.ID
                     join t4 in db.Prod_PlanReference on t3.Prod_PlanReferenceFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID

                     where t1.ID == Plan_ProductionLine
                     select new
                     {
                         orderid = t6.CID + "/" + t5.Style

                     }).ToList();

            string ordernumber = "";
            if(a != null)
            {
                foreach(var val in a)
                {
                    ordernumber = ordernumber + val.orderid + ",";
                }
            }

            return ordernumber;
        }

        private int GetRunningStatus(int iD)
        {
            db = new xOssContext();
            var xx=  db.Plan_ProductionLineRunning.Where(x => x.Plan_ProductionLineFK == iD).OrderByDescending(x=>x.ID).Select(x => x.TimeType).Take(1);
            return xx.SingleOrDefault();
        } 

        public void PreviousTargetDataLoad()
        {
            var date = DateTime.Now.Date;
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSection
                     join t2 in db.Prod_PlanReferenceProduction
                         on t1.Prod_PlanReferenceProductionFk equals t2.ID
                     join t6 in db.Plan_ProductionLine
                         on t1.Plan_ProductionLineFk equals t6.ID
                     join t7 in db.Prod_LineChief
                         on t1.Prod_LineChiefFk equals t7.ID
                     join t8 in db.Prod_SVName
                         on t1.Prod_SVNameFk equals t8.ID
                     join t3 in db.Plan_OrderProcess
                         on t2.Plan_OrderProcessFK equals t3.ID
                     //where t3.ID == id
                     select new VmProd_PlanReferenceProductionSection
                     {
                         Prod_PlanReferenceProductionSection = t1,
                         Prod_PlanReferenceProduction = t2,
                         Plan_OrderProcess = t3,

                         Plan_ProductionLine = t6,
                         Prod_LineChief = t7,
                         Prod_SVName = t8

                     }).Where(x => x.Prod_PlanReferenceProductionSection.Active == true &&
                                   date < x.Prod_PlanReferenceProductionSection.FinishDate
                )
                .OrderByDescending(x => x.Prod_PlanReferenceProductionSection.ID).AsEnumerable()
                .GroupBy(m => m.Prod_PlanReferenceProductionSection.ID)
                .Select(x => x.First());
            this.DataList = v;
        }

        public void UpcomingTargetDataLoad()
        {
            var date = DateTime.Now.Date;
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSection
                     join t2 in db.Prod_PlanReferenceProduction
                         on t1.Prod_PlanReferenceProductionFk equals t2.ID
                     join t6 in db.Plan_ProductionLine
                         on t1.Plan_ProductionLineFk equals t6.ID
                     join t7 in db.Prod_LineChief
                         on t1.Prod_LineChiefFk equals t7.ID
                     join t8 in db.Prod_SVName
                         on t1.Prod_SVNameFk equals t8.ID
                     join t3 in db.Plan_OrderProcess
                         on t2.Plan_OrderProcessFK equals t3.ID
                     //where t3.ID == id
                     select new VmProd_PlanReferenceProductionSection
                     {
                         Prod_PlanReferenceProductionSection = t1,
                         Prod_PlanReferenceProduction = t2,
                         Plan_OrderProcess = t3,

                         Plan_ProductionLine = t6,
                         Prod_LineChief = t7,
                         Prod_SVName = t8

                     }).Where(x => x.Prod_PlanReferenceProductionSection.Active == true &&
                                   date < x.Prod_PlanReferenceProductionSection.StartDate
                )
                .OrderByDescending(x => x.Prod_PlanReferenceProductionSection.ID).AsEnumerable()
                .GroupBy(m => m.Prod_PlanReferenceProductionSection.ID)
                .Select(x => x.First());
            this.DataList = v;
        }

        public string GetPlan_ProductionLineRunningByID(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Plan_ProductionLineRunning
                     where DbFunctions.TruncateTime(t1.Date) == DateTime.Today && t1.Plan_ProductionLineFK == id
                     select new
                     {
                         RunningDate = t1.Date

                     }).FirstOrDefault();
            string dateTime = null;

            if (a != null)
            {
                dateTime = a.RunningDate.ToString("HH:mm:ss");
            }

            return dateTime;
        }
    }
}