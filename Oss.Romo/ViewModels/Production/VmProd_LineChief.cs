﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_LineChief
    {
        private xOssContext db;
        public IEnumerable<VmProd_LineChief> DataList { get; set; }
        public Prod_LineChief Prod_LineChief { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmProd_LineChief()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_LineChief
                     select new VmProd_LineChief
                     {
                         Prod_LineChief = t1
                     }).Where(x => x.Prod_LineChief.Active == true).OrderByDescending(x => x.Prod_LineChief.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingle(int id)
        {
          
            db = new xOssContext();
            var v = (from t1 in db.Prod_LineChief
                     select new VmProd_LineChief
                     {
                         Prod_LineChief = t1
                     }).Where(c => c.Prod_LineChief.ID == id).SingleOrDefault();
            this.Prod_LineChief = v.Prod_LineChief;

        }

        public int Add(int userID)
        {
            Prod_LineChief.AddReady(userID);
            return Prod_LineChief.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_LineChief.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_LineChief.Delete(userID);
        }

    }
}