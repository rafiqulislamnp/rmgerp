﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_InputRequisitionSlave
    {
        private xOssContext db;
        public VmProd_InputRequisitionSlave()
        {
            db = new xOssContext();
        }
        public Prod_InputRequisition Prod_InputRequisition { get; set; }
        public Prod_InputRequisitionSlave Prod_InputRequisitionSlave { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public VmProd_InputRequisition VmProd_InputRequisition { get; set; }
        public IEnumerable<VmProd_InputRequisitionSlave> DataList { get; set; }
        
        [DisplayName("Available Quantity")]
        public int AvailableQty { get; set; }
        
        public void InitialDataLoad()
        {
            var v = (from t1 in db.Prod_InputRequisitionSlave
                     join t2 in db.Prod_InputRequisition on t1.Prod_InputRequisitionFk equals t2.ID
                     select new VmProd_InputRequisitionSlave
                     {
                         Prod_InputRequisitionSlave = t1,
                         Prod_InputRequisition = t2
                     }).Where(x => x.Prod_InputRequisitionSlave.Active == true).OrderByDescending(x => x.Prod_InputRequisitionSlave.ID).AsEnumerable();
            this.DataList = v;
        }
        
        internal void GetRequisitionSlaveItem(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Prod_InputRequisitionSlave
                     join t2 in db.Prod_InputRequisition on t1.Prod_InputRequisitionFk equals t2.ID
                     join t6 in db.Mkt_BOM on t1.Mkt_BOMFK equals t6.ID
                     join t7 in db.Common_TheOrder on t6.Common_TheOrderFk equals t7.ID
                     join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
                     where t1.Active == true && t1.Prod_InputRequisitionFk == id
                     select new VmProd_InputRequisitionSlave
                     {
                         Prod_InputRequisitionSlave = t1,
                         Prod_InputRequisition = t2,
                         Common_Unit = t4,
                         Mkt_BOM = t6,
                         Common_TheOrder = t7
                     }).OrderByDescending(x => x.Prod_InputRequisitionSlave.ID).AsEnumerable();
            
            this.DataList = a;
        }

        public void SelectSingle(int Id)
        {
            db = new xOssContext();
            var v = (from rc in db.Prod_InputRequisitionSlave
                     select new VmProd_InputRequisitionSlave
                     {
                         Prod_InputRequisitionSlave = rc
                     }).Where(c => c.Prod_InputRequisitionSlave.ID == Id).SingleOrDefault();
            this.Prod_InputRequisitionSlave = v.Prod_InputRequisitionSlave;
        }

        public int Add(int userID)
        {
            Prod_InputRequisitionSlave.AddReady(userID);
            return Prod_InputRequisitionSlave.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_InputRequisitionSlave.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_InputRequisitionSlave.Delete(userID);
        }
    }
}