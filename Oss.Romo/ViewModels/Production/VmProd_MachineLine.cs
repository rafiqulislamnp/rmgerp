﻿using Oss.Romo.Models;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_MachineLine
    {
        private xOssContext db;
        public Prod_MachineLine Prod_MachineLine { get; set; }
        public Plan_ProductionLine Plan_ProductionLine { get; set; }
        public Prod_Machine Prod_Machine { get; set; }
        public IEnumerable<VmProd_MachineLine> DataList { get; set; }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Prod_MachineLine
                     join t2 in db.Prod_Machine
                     on t1.Prod_MachineFK equals t2.ID
                     join t3 in db.Plan_ProductionLine
                     on t1.Plan_ProductionLineFK equals t3.ID
                     where t1.Plan_ProductionLineFK == id
                      && t2.Active == true
                      && t3.Active == true
                     select new VmProd_MachineLine
                     {
                         Prod_MachineLine =t1,
                         Prod_Machine = t2,
                         Plan_ProductionLine = t3
                     }).Where(x => x.Prod_MachineLine.Active == true).OrderByDescending(x => x.Prod_MachineLine.ID).AsEnumerable();
            this.DataList = a;

        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_MachineLine
                     join t2 in db.Prod_Machine
                     on t1.Prod_MachineFK equals t2.ID
                     join t3 in db.Plan_ProductionLine
                     on t1.Plan_ProductionLineFK equals t3.ID
                     select new VmProd_MachineLine
                     {
                         Prod_MachineLine = t1,
                         Prod_Machine = t2,
                         Plan_ProductionLine = t3
                     }).Where(c => c.Prod_MachineLine.ID == id).SingleOrDefault();

            this.Prod_MachineLine = v.Prod_MachineLine;
            this.Prod_Machine = v.Prod_Machine;
            this.Plan_ProductionLine = v.Plan_ProductionLine;

        }
        
        public VmProd_MachineLine SelectSingleOrderLine(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Plan_ProductionLine
                     where t1.Active == true
                     select new VmProd_MachineLine
                     {
                         Plan_ProductionLine = t1

                     }).Where(x => x.Plan_ProductionLine.ID == id).FirstOrDefault();
            return v;

        }

        public int Add(int userID)
        {
            Prod_MachineLine.AddReady(userID);

            return Prod_MachineLine.Add();

        }

        public bool Edit(int userID)
        {
            return Prod_MachineLine.Edit(userID);
        }
    }
}