﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_TransitionItemMadeFrom
    {
        private xOssContext db;
        public IEnumerable<VmProd_TransitionItemMadeFrom> DataList { get; set; }
        public Prod_TransitionItemMadeFrom Prod_TransitionItemMadeFrom { get; set; }
        public Prod_TransitionItemInventory Prod_TransitionItemInventory { get; set; }
        public VmProd_TransitionItemInventory VmProd_TransitionItemInventory { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Raw_Item Raw_Item { get; set; }
        [Required(ErrorMessage = "Transfer Type Required"), Display(Name = "Transfer Type")]
        public int TransferType { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public Mkt_BOM Mkt_BOM{ get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public VmProd_TransitionItemMadeFrom()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Prod_TransitionItemMadeFrom
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     where t1.Active == true && t2.Active == true && t1.Raw_ItemFK != 1
                     select new VmProd_TransitionItemMadeFrom
                     {
                         Prod_TransitionItemMadeFrom = t1,
                         Raw_Item = t2
                     }).Where(x => x.Prod_TransitionItemMadeFrom.Prod_TransitionItemInventoryFK == id).AsEnumerable();

            var b = (from t1 in db.Prod_TransitionItemMadeFrom
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     join t0 in db.Mkt_PO on t2.Mkt_POFK equals t0.ID
                     join t4 in db.Mkt_BOM on t0.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Raw_Item on t2.Raw_ItemFK equals t3.ID
                     where t1.Active == true && t2.Active==true && t3.Active==true && t1.Mkt_POSlaveFK != 1
                     select new VmProd_TransitionItemMadeFrom
                     {
                         Prod_TransitionItemMadeFrom = t1,
                         Raw_Item = t3,
                         Mkt_POSlave = t2,
                         Common_TheOrder = t5
                     }).Where(x => x.Prod_TransitionItemMadeFrom.Prod_TransitionItemInventoryFK == id).AsEnumerable();

            var c = (from t1 in db.Prod_TransitionItemMadeFrom
                     join t2 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFromFK equals t2.ID
                     where t1.Active == true && t2.Active == true && t1.Prod_TransitionItemInventoryFromFK != 1
                     select new VmProd_TransitionItemMadeFrom
                     {
                         Prod_TransitionItemMadeFrom = t1,
                         Prod_TransitionItemInventory = t2
                     }).Where(x => x.Prod_TransitionItemMadeFrom.Prod_TransitionItemInventoryFK == id).AsEnumerable();

            //var v = a.Union(b);
            var y = a.Union(b).Union(c);
            this.DataList = y;

        }


        public void SelectSingle(int id)
        {
          
            db = new xOssContext();
            var v = (from t1 in db.Prod_TransitionItemMadeFrom                   
                     select new VmProd_TransitionItemMadeFrom
                     {
                         Prod_TransitionItemMadeFrom = t1,
                         
                     }).SingleOrDefault(c => c.Prod_TransitionItemMadeFrom.ID == id);
            this.Prod_TransitionItemMadeFrom = v.Prod_TransitionItemMadeFrom;

        }

        public int Add(int userID)
        {
            Prod_TransitionItemMadeFrom.AddReady(userID);
            return Prod_TransitionItemMadeFrom.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_TransitionItemMadeFrom.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_TransitionItemMadeFrom.Delete(userID);
        }

    }
}