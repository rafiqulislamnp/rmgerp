﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrystalDecisions.Web.HtmlReportRender;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Planning;

namespace Oss.Romo.ViewModels.Production
{
	public class VmProd_Planning
	{
        private xOssContext db;
	    public int OrderProcessID { get; set; }
        public IEnumerable<VmProd_Planning> DataList { get; set; }
        public Prod_PlanReferenceProduction Prod_PlanReferenceProduction { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public VmPlan_ProductionLine VmPlan_ProductionLine { get; set; }
        public Plan_ProductionLine Plan_ProductionLine { get; set; }
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Prod_PlanReference Prod_PlanReference { get; set; }
        public Common_Country Common_Country { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
	    public Prod_Planning Prod_Planning { get; set; }
        public Prod_OrderPlanning Prod_OrderPlanning { get; set; }
        public decimal OrderQuantityPC { get; set; }
        public decimal ProductionQty { get; set; }
        public decimal TempRemainQty { get; set; }
        public decimal OrderTotalAchive { get; set; }
        public decimal OrderTotalAchiveRemain { get; set; }
        public decimal TempAchiveRemain { get; set; }

        public bool LineAdd { get; set; }
        public int BOMID { get; set; }
        public int Prod_PlanOrderId { get; set; }
        public string BuyerPo { get; set; }
        public int Id { get; set; }
        public string LineStatus { get; set; }

        public VmProd_Planning()
        {
            db = new xOssContext();
            VmControllerHelper = new VmControllerHelper();
        }

        public void LayoutInitialDataLoad()
        {
            var v = (from t1 in db.Prod_Planning
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Mkt_Item on t4.Mkt_ItemFK equals t3.ID
                     join t6 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t6.ID
                     join t7 in db.Prod_OrderPlanning on t1.Prod_OrderPlanningFK equals t7.ID
                     //join t7 in db.Common_Country on t3.Destination equals t7.ID
                     //where t1.Mkt_BOMFK == BomId && t1.Active == true
                     select new VmProd_Planning
                     {
                         Prod_Planning = t1,
                         Mkt_Item = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         Plan_ProductionLine = t6,
                         OrderQuantityPC = t7.PlanQty //t4.QPack * t4.Quantity
                     }).OrderBy(x => x.Prod_Planning.ID).AsEnumerable();
            this.DataList = v;
        }

        public void InitialDataLoad(int Id,int BomId)
        {
            bool IsTaken = false;
            var v = (from t1 in db.Prod_Planning
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Mkt_Item on t4.Mkt_ItemFK equals t3.ID
                     join t6 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t6.ID
                     join t7 in db.Prod_OrderPlanning on t1.Prod_OrderPlanningFK equals t7.ID
                     where t1.Mkt_BOMFK == BomId && t1.Active == true && t1.Prod_OrderPlanningFK == Id
                     select new VmProd_Planning
                     {
                         Prod_Planning = t1,
                         Mkt_Item = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         Plan_ProductionLine = t6,
                         OrderQuantityPC = t7.PlanQty,
                         
                     }).OrderBy(x => x.Prod_Planning.ID).ToList();

            if (v.Any())
            {
                decimal tempVariable = 0;
                foreach (var v1 in v)
                {
                    v1.LineStatus = v1.Prod_Planning.IsPaused == false ? "Running" : "Paused";
                    if (v1.Prod_Planning.IsPaused) { }
                    v1.OrderTotalAchive = GetTotalAcheiveQty(v1.Mkt_BOM.ID);
                    var vQty= db.Prod_PlanAchievment.Where(a => a.AchiveDate >= v1.Prod_Planning.StartDate && a.AchiveDate <= v1.Prod_Planning.FinishDate && a.Plan_ProductionLineFk == v1.Prod_Planning.Plan_ProductionLineFk && a.Mkt_BOMFK==BomId);
                    v1.ProductionQty = vQty.Any() == true ? vQty.Sum(a => a.PlanAchievQty) : 0;
                    v1.TempRemainQty = v1.Prod_Planning.TargetOutputQty - v1.ProductionQty;
                    v1.OrderTotalAchiveRemain = (v1.OrderQuantityPC - v1.OrderTotalAchive);
                    
                    if (IsTaken)
                    {
                        //v1.TempAchiveRemain = tempVariable - v1.Prod_Planning.TargetOutputQty;
                        v1.TempAchiveRemain = tempVariable + v1.TempRemainQty;
                    }
                    else
                    {
                        //v1.TempAchiveRemain = v1.OrderTotalAchiveRemain - v1.Prod_Planning.TargetOutputQty;
                        v1.TempAchiveRemain = v1.TempRemainQty + tempVariable;
                        IsTaken = true;
                    }
                    tempVariable = v1.TempAchiveRemain;
                }
            }

            this.DataList = v;
        }
        
        public void GetUpdate(VmProd_Planning VmProd_Planning)
        {
            List<VmProd_Planning> updateList = new List<VmProd_Planning>();
            List<VmProd_Planning> lstBefore = new List<VmProd_Planning>();
            bool IsTaken = false;
            var GetAll = db.Prod_Planning.Where(a=>a.Mkt_BOMFK== VmProd_Planning.Prod_Planning.Mkt_BOMFK && a.Prod_OrderPlanningFK== VmProd_Planning.Prod_Planning.Prod_OrderPlanningFK && a.Active==true).OrderBy(a=>a.ID).ToList();

            if (GetAll.Any())
            {
                foreach (var v in GetAll)
                {
                    if (v.ID== VmProd_Planning.Prod_Planning.ID)
                    {
                        IsTaken = true;
                        updateList.Add(new VmProd_Planning() { Id=v.ID, TempRemainQty=v.RemainQty });
                    }
                    if (!updateList.Any(a=>a.Id==v.ID) && IsTaken)
                    {
                        updateList.Add(new VmProd_Planning() { Id = v.ID, TempRemainQty = v.RemainQty });
                    }
                    else if(!lstBefore.Any(a => a.Id == v.ID) && !IsTaken)
                    {
                        lstBefore.Add(new VmProd_Planning() { Id = v.ID, TempRemainQty = v.RemainQty });
                    }
                }
            }

            if (lstBefore.Any())
            {
                this.TempRemainQty = lstBefore.OrderByDescending(x => x.Id).FirstOrDefault().TempRemainQty;
            }
            else
            {
                this.TempRemainQty = VmProd_Planning.OrderQuantityPC;
            }

            if (updateList.Any())
            {
                foreach (var v in updateList)
                {
                    if (v.Id == VmProd_Planning.Prod_Planning.ID)
                    {
                        var GetRow = db.Prod_Planning.Where(a => a.ID == v.Id);
                        if (GetRow.Any())
                        {
                            var update = GetRow.FirstOrDefault();
                            update.StartDate = VmProd_Planning.Prod_Planning.StartDate;
                            update.FinishDate = VmProd_Planning.Prod_Planning.FinishDate;
                            update.TargetDays = (update.FinishDate - update.StartDate).Days + 1;
                            update.Capacity = VmProd_Planning.Prod_Planning.Capacity;
                            update.WorkingHour = VmProd_Planning.Prod_Planning.WorkingHour;
                            update.DayOutputQty = update.Capacity * update.WorkingHour;
                            update.TargetOutputQty = update.DayOutputQty * update.TargetDays;
                            update.RemainQty = this.TempRemainQty - update.TargetOutputQty;
                            update.Description= VmProd_Planning.Prod_Planning.Description;
                            this.TempRemainQty = update.RemainQty;
                        }
                    }
                    else
                    {
                        var GetNextRow = db.Prod_Planning.Where(a => a.ID == v.Id);
                        if (GetNextRow.Any())
                        {
                            var update = GetNextRow.FirstOrDefault();
                            update.RemainQty = this.TempRemainQty - update.TargetOutputQty;
                            this.TempRemainQty = update.RemainQty;
                        }
                    }
                }
                db.SaveChanges();
            }
        }
        
        public void GetLastBalanceQty(int BomId)
        {
            db = new xOssContext();
            var vData = (from t1 in db.Prod_Planning
                         where t1.Mkt_BOMFK==BomId
                         select new {
                             Id=t1.ID,
                             Line=t1.Plan_ProductionLineFk,
                             TargetTotalQty=t1.TargetOutputQty,
                             RemainQty=t1.RemainQty
                         }).OrderByDescending(a=>a.Id).ToList();

            if (vData.Any())
            {
                this.LineAdd = true;
                this.TempRemainQty = vData.FirstOrDefault().RemainQty;
            }
        }

        public void GetLastUpdateBalanceQty(int BomId, int id, int Pkid)
        {
            db = new xOssContext();
            var vData = (from t1 in db.Prod_Planning
                         where t1.Mkt_BOMFK == BomId
                         && t1.Prod_OrderPlanningFK==id 
                         select new
                         {
                             Id = t1.ID,
                             Line = t1.Plan_ProductionLineFk,
                             TargetTotalQty = t1.TargetOutputQty,
                             RemainQty = t1.RemainQty,
                             createdDate=t1.FirstCreated
                         }).OrderBy(a => a.Id).ToList();
            var c = vData.Where(x => x.Id == Pkid);
            var b = vData.Where(x => x.createdDate < c.FirstOrDefault().createdDate);

            if (vData.Any())
            {
                this.LineAdd = true;
                this.TempRemainQty = vData.FirstOrDefault().RemainQty;
            }
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Planning
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Mkt_Item on t4.Mkt_ItemFK equals t3.ID
                     join t6 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t6.ID
                     join t7 in db.Prod_OrderPlanning on t1.Prod_OrderPlanningFK equals t7.ID
                     select new VmProd_Planning
                     {
                         Prod_Planning = t1,
                         Mkt_Item = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         Plan_ProductionLine = t6,
                         OrderQuantityPC=t7.PlanQty
                     }).SingleOrDefault(c => c.Prod_Planning.ID == id);
            if (v != null) this.Prod_Planning = v.Prod_Planning;
            if (v != null) this.Mkt_Item = v.Mkt_Item;
            if (v != null) this.Mkt_BOM = v.Mkt_BOM;
            if (v != null) this.Common_TheOrder = v.Common_TheOrder;
            if (v != null) this.OrderQuantityPC = v.OrderQuantityPC;
        }

        public decimal GetTotalAcheiveQty(int BomId)
        {
            decimal TotalQty = 0;
            var vPlan = (from t1 in db.Prod_OrderPlanning
                         join t2 in db.Prod_PlanAchievment on t1.ID equals t2.Prod_OrderPlanningFk
                         where t1.Mkt_BOMFK == BomId && t1.Active == true && t1.IsClosed == false
                         select new { TotalQty = t2.PlanAchievQty }).ToList();

            return TotalQty = vPlan.Any() == true ? vPlan.Sum(a => a.TotalQty) : 0;
        }

        public VmProd_Planning SelectSingleJoined(int Id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_OrderPlanning
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Mkt_Item on t4.Mkt_ItemFK equals t3.ID
                     where t1.ID==Id
                     select new VmProd_Planning
                     {
                         Prod_OrderPlanning = t1,
                         Mkt_Item = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         OrderQuantityPC = t1.PlanQty
                     }).SingleOrDefault();
            v.OrderTotalAchive = GetTotalAcheiveQty(v.Prod_OrderPlanning.Mkt_BOMFK);
            return v;
        }

        public void CheckAndUpdatePlanAchivement(Prod_Planning model)
        {
            db = new xOssContext();
            var vData = db.Prod_PlanAchievment.Where(a => a.Mkt_BOMFK == model.Mkt_BOMFK && a.Prod_OrderPlanningFk == model.Prod_OrderPlanningFK && a.Plan_ProductionLineFk == model.Plan_ProductionLineFk);
            if (vData.Any())
            {
                var update = vData.FirstOrDefault();
                update.WorkingHour = model.WorkingHour;
                update.HourCapacity = model.Capacity;
                update.IsPlanedLine = true;
                update.Edit(model.FirstCreatedBy);
            }
        }
        
        public int Add(int userID)
        {
            Prod_Planning.AddReady(userID);
            return Prod_Planning.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_Planning.Edit(userID);
        }

        public bool Delete(int userID)
        {           
            return Prod_Planning.Delete(userID);          
        }

        public void LinePauseRun(int Id,bool State)
        {
            var vData = db.Prod_Planning.Where(a=>a.ID==Id);

            if (vData.Any())
            {
                var update = vData.FirstOrDefault();
                if (State)
                {
                    update.IsPaused = false;
                }
                else
                {
                    update.IsPaused = true;
                }
                db.SaveChanges();
            }

            this.Prod_Planning = vData.FirstOrDefault();
        }
    }
}