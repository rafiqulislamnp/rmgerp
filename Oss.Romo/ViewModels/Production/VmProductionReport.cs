﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProductionReport
    {
        private xOssContext db;

        public List<VmProductionReport> DataList { get; set; }
        public IEnumerable<VmProductionReport> DataList1 { get; set; }
        public List<ProductionDailyPlan> PlanDataList { get; set; }
        public List<ReportDocType> ReportDoc { get; set; }
        public Prod_Reference Prod_Reference { get; set; }
        public Prod_PlanReferenceOrder Prod_PlanReferenceOrder { get; set; }
        public Prod_PlanReferenceOrderSection Prod_PlanReferenceOrderSection { get; set; }
        public VmProd_Reference VmProd_Reference { get; set; }
        public ProductionDailyPlan ProductionDailyPlan { get; set; }

        #region Declaretion

        public string LineOutput { get; set; }
        public int ID { get; set; }

        [Display(Name = "Production Date")]
        [Required(ErrorMessage = "Production Date is Required."), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; }

        [Display(Name = "Production Date")]
        [Required(ErrorMessage = "Production Date is Required."), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate1 { get; set; }

        [Required(ErrorMessage = "ToDate is Required."), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; }

        public string DateView { get; set; }

        public string ProductionDate { get; set; }

        public int ProdReferenceId { get; set; }

        public int SectionId { get; set; }

        public int BOMID { get; set; }

        public string BuyerName { get; set; }

        public int LineId { get; set; }

        public string LineName { get; set; }

        public int LineMachineRequired { get; set; }

        public string OrderNo { get; set; }

        public string BuyerPO { get; set; }

        public string Style { get; set; }

        public string Item { get; set; }

        public int LineMachineRunning { get; set; }

        public int NoOfOPPresent { get; set; }

        public int NoOfHP { get; set; }

        public int TotalWorker { get; set; }

        public int SizeMan { get; set; }

        public int SuperVisor { get; set; }

        public int NoOfInputMan { get; set; }

        public decimal SMV { get; set; }

        public int PerHourTarget { get; set; }

        public int TotalTarget { get; set; }

        public int LineHour { get; set; }

        public int PerHourAchivement { get; set; }

        public int TotalAchivement { get; set; }

        public decimal TargetEffi { get; set; }

        public decimal AchiveEffi { get; set; }

        public int TargetBalance { get; set; }

        public string Remarks { get; set; }

        public int Quantity { get; set; }

        public int DaySewing { get; set; }

        public int LineCount { get; set; }

        public int TotalCutting { get; set; }

        public int TotalSewing { get; set; }

        public int TotalPacking { get; set; }

        public int TotalIroning { get; set; }

        public int TotalCTN { get; set; }

        public decimal CuttingRate { get; set; }

        public decimal SewingRate { get; set; }

        public decimal IroningRate { get; set; }

        public decimal PackingRate { get; set; }

        public int PackPice { get; set; }

        public string ColorName { get; set; }

        public string Size { get; set; }

        public int CuttingBalance { get; set; }

        public int SewingBalance { get; set; }

        public int IronBalance { get; set; }

        public decimal PackingBalance { get; set; }

        public int DayCutting { get; set; }

        public int DayIroning { get; set; }

        public int DayPacking { get; set; }

        public int Searchby { get; set; }

        public decimal DayCuttingWeight { get; set; }

        public int BalanceQty { get; set; }

        public decimal TotalWeight { get; set; }

        public decimal LayerWeight { get; set; }

        public decimal MarkerPic { get; set; }

        public decimal MarkerLength { get; set; }

        public decimal MarkerWidth { get; set; }

        public int ExtraQty { get; set; }

        public int ExtraRate { get; set; }

        public string Extra { get; set; }

        public decimal CuttingConsumption { get; set; }

        public decimal GSM { get; set; }

        public decimal TotalStoreReceived { get; set; }

        public decimal StoreBalance { get; set; }

        public decimal BookingConsump { get; set; }

        public decimal CuttingConsump { get; set; }

        public decimal TotalCuttingReceived { get; set; }

        public decimal TotalFabricUsed { get; set; }

        public decimal FabricBalance { get; set; }

        public decimal TotalBalance { get; set; }
        #endregion

        //public void GetTargetAchivementReport(int RefId)
        //{
        //    db = new xOssContext();
        //    this.FromDate = db.Prod_Reference.Where(a => a.ID == RefId).FirstOrDefault().FromDate;
        //    var allData = (from t1 in db.Prod_PlanReferenceOrder
        //                 join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
        //                 join t3 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t3.ID
        //                 join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
        //                 join t7 in db.Common_TheOrder on t2.Common_TheOrderFk equals t7.ID
        //                 join t9 in db.Mkt_Buyer on t7.Mkt_BuyerFK equals t9.ID
        //                 join t8 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t8.ID
        //                 where t1.Prod_ReferenceFK == RefId && t1.CuttingPlanQuantity == 0 && t1.SewingPlanQuantity != 0
        //                 group t1.SewingPlanQuantity
        //                 by new { t1.SMV, t1.WorkingHour, t1.PresentOperator, t1.PresentHelper, t1.MachineRunning, t1.Prod_ReferenceFK,
        //                     t1.Plan_ProductionLineFk, t4.ID, t4.Style, t7.BuyerPO, t9.Name } into all
        //                 select new
        //                 {
        //                     SMVInfo = db.Prod_SMVLayout.Where(a => a.Mkt_BOMFk == all.Key.ID && a.TotalSMV == all.Key.SMV).ToList(),
        //                     LineId = all.Key.Plan_ProductionLineFk,
        //                     OrderNo = all.Key.BuyerPO,
        //                     Style = all.Key.Name + "-" + all.Key.Style,
        //                     BOMId = all.Key.ID,
        //                     MachineRunning = all.Key.MachineRunning,
        //                     NoOfOptPresent=all.Key.PresentOperator,
        //                     NoOfHelperPresent = all.Key.PresentHelper,
        //                     WorkHour = all.Key.WorkingHour,
        //                     StyleSMV = all.Key.SMV,
        //                     TotalTarget = all.Sum(),
        //                 }).ToList();

        //    var allDoneData = (from t1 in db.Prod_PlanReferenceOrder
        //                       join t2 in db.Prod_PlanReferenceOrderSection on t1.ID equals t2.Prod_PlanReferenceOrderFk
        //                       join t3 in db.Mkt_OrderDeliverySchedule on t2.OrderDeliveryScheduleFk equals t3.ID
        //                       join t4 in db.Mkt_BOM on t3.Mkt_BOMFk equals t4.ID
        //                       where t1.Prod_ReferenceFK == RefId && t1.SectionId==2 && t2.Plan_ReferenceSectionFk == 2 //&& t1.SewingPlanQuantity != 0 && CuttingPlanQuantity == 0
        //                       group t2.Quantity by new { t1.Plan_ProductionLineFk, t4.ID, t1.SMV } into all
        //                       select new {
        //                           LineId = all.Key.Plan_ProductionLineFk,
        //                           BOMId = all.Key.ID,
        //                           SMV = all.Key.SMV,
        //                           TotalDoneQty = all.Sum(),
        //                       }).ToList();

        //    List<VmProductionReport> lstLine = new List<VmProductionReport>();
        //    int LineMachineQty = 0;
        //    var getAllLines = db.Plan_ProductionLine.Where(a => a.LineNumber != null).OrderBy(a => a.ID).ToList();
        //    foreach (var line in getAllLines)
        //    {
        //        if (allData.Any(a=>a.LineId==line.ID))
        //        {
        //            var GetLineInfo = allData.Where(a=>a.LineId==line.ID).ToList();

        //            LineMachineQty += GetLineInfo.FirstOrDefault().MachineRunning;
        //            foreach (var item in GetLineInfo)
        //            {
        //                int totalDoneQty = 0;
        //                if (allDoneData.Any(a => a.BOMId == item.BOMId && a.LineId == item.LineId && a.SMV==item.StyleSMV))
        //                {
        //                    totalDoneQty = (int)allDoneData.Where(a => a.BOMId == item.BOMId && a.LineId == item.LineId && a.SMV == item.StyleSMV).FirstOrDefault().TotalDoneQty;
        //                }

        //                var getRemarks= db.Prod_PlanReferenceRemarks.Where(a=>a.Prod_ReferenceFK== RefId && a.MktBomFk == item.BOMId && a.Plan_ProductionLineFk==item.LineId && a.Active==true).ToList();

        //                VmProductionReport rpt = new VmProductionReport();
        //                rpt.ProdReferenceId = RefId;
        //                rpt.BOMID = item.BOMId;
        //                rpt.LineId = line.ID;
        //                rpt.LineName = line.Name;
        //                rpt.LineMachineRequired = item.SMVInfo.Any() == true ? item.SMVInfo.FirstOrDefault().NoOfOperator : 0;
        //                rpt.OrderNo = item.OrderNo;
        //                rpt.Item = item.Style;
        //                rpt.LineMachineRunning = item.MachineRunning;
        //                rpt.NoOfOPPresent = item.NoOfOptPresent;
        //                rpt.NoOfHP = item.NoOfHelperPresent;
        //                rpt.TotalWorker = rpt.NoOfOPPresent + rpt.NoOfHP;
        //                rpt.SMV = item.StyleSMV;
        //                rpt.PerHourTarget = item.SMVInfo.Any() == true ? item.SMVInfo.FirstOrDefault().PerHourTarget : 0;
        //                rpt.TotalTarget = item.TotalTarget;
        //                rpt.LineHour = item.WorkHour;
        //                rpt.TotalAchivement = totalDoneQty;
        //                rpt.NoOfInputMan = 1;
        //                rpt.SizeMan = 1;
        //                rpt.SuperVisor = 1;
        //                rpt.PerHourAchivement = rpt.TotalAchivement / rpt.LineHour;
        //                rpt.TargetEffi = Math.Round(((rpt.SMV * rpt.TotalTarget) / (rpt.LineHour * 60 * rpt.TotalWorker)) * 100,2);
        //                rpt.AchiveEffi = Math.Round(((rpt.SMV * rpt.TotalAchivement) / (rpt.LineHour * 60 * rpt.TotalWorker)) * 100,2);
        //                rpt.TargetBalance = rpt.TotalAchivement - rpt.TotalTarget;
        //                rpt.Remarks = getRemarks.Any() == true ? getRemarks.FirstOrDefault().Remarks : string.Empty;
        //                rpt.SectionId = LineMachineQty;
        //                rpt.FromDate = FromDate;
        //                lstLine.Add(rpt);
        //            }
        //        }
        //    }

        //    this.DataList = lstLine;
        //}

        public void GetTargetAchivementReport(int RefId)
        {
            db = new xOssContext();
            this.FromDate = db.Prod_Reference.Where(a => a.ID == RefId).FirstOrDefault().FromDate;
            var allData = (from t1 in db.Prod_PlanReferenceOrder
                           join t2 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t2.ID
                           join t3 in db.Mkt_BOM on t1.MktBomFk equals t3.ID
                           join t7 in db.Common_TheOrder on t3.Common_TheOrderFk equals t7.ID
                           join t9 in db.Mkt_Buyer on t7.Mkt_BuyerFK equals t9.ID
                           where t1.Prod_ReferenceFK == RefId && t1.SectionId == 2
                           select new
                           {
                               SMVInfo = db.Prod_SMVLayout.Where(a => a.Mkt_BOMFk == t1.MktBomFk && a.TotalSMV == t1.SMV).ToList(),
                               BOMId = t1.MktBomFk,
                               LineId = t1.Plan_ProductionLineFk,
                               OrderNo = t7.BuyerPO,
                               Style = t9.Name + "-" + t3.Style,
                               MachineRunning = t1.MachineRunning,
                               NoOfOptPresent = t1.PresentOperator,
                               NoOfHelperPresent = t1.PresentHelper,
                               WorkHour = t1.WorkingHour,
                               StyleSMV = t1.SMV,
                               PerHourTarget = t1.Quantity,
                               TotalTarget = t1.SewingPlanQuantity,
                               TotalAchivement = (from a in db.Prod_PlanReferenceOrderSection
                                                  join b in db.Prod_PlanOrderSectionSlave on a.ID equals b.Prod_PlanReferenceOrderSectionFk
                                                  join c in db.Mkt_OrderDeliverySchedule on a.OrderDeliveryScheduleFk equals c.ID
                                                  where a.Prod_PlanReferenceOrderFk == t1.ID
                                                  group b.Quantity by new { a.Prod_PlanReferenceOrderFk } into all
                                                  select new
                                                  {
                                                      total = all.Sum()
                                                  }).FirstOrDefault()
                           }).ToList();

            List<VmProductionReport> lstLine = new List<VmProductionReport>();
            int LineMachineQty = 0;
            var getAllLines = db.Plan_ProductionLine.Where(a => a.LineNumber != null).OrderBy(a => a.ID).ToList();
            foreach (var line in getAllLines)
            {
                if (allData.Any(a => a.LineId == line.ID))
                {
                    var GetLineInfo = allData.Where(a => a.LineId == line.ID).ToList();

                    LineMachineQty += GetLineInfo.FirstOrDefault().MachineRunning;
                    foreach (var item in GetLineInfo)
                    {
                        var getRemarks = db.Prod_PlanReferenceRemarks.Where(a => a.Prod_ReferenceFK == RefId && a.MktBomFk == item.BOMId && a.Plan_ProductionLineFk == item.LineId && a.Active == true).ToList();

                        VmProductionReport rpt = new VmProductionReport();
                        rpt.ProdReferenceId = RefId;
                        rpt.BOMID = item.BOMId;
                        rpt.LineId = line.ID;
                        rpt.LineName = line.Name;
                        rpt.LineMachineRequired = item.SMVInfo.Any() == true ? item.SMVInfo.FirstOrDefault().NoOfOperator : 0;
                        rpt.OrderNo = item.OrderNo;
                        rpt.Item = item.Style;
                        rpt.LineMachineRunning = item.MachineRunning;
                        rpt.NoOfOPPresent = item.NoOfOptPresent;
                        rpt.NoOfHP = item.NoOfHelperPresent;
     
                        rpt.SMV = item.StyleSMV;
                        rpt.PerHourTarget = item.PerHourTarget;
                        rpt.TotalTarget = item.TotalTarget;
                        rpt.LineHour = item.WorkHour;
                        rpt.TotalAchivement = item.TotalAchivement == null ? 0 : (int)item.TotalAchivement.total;
                        rpt.NoOfInputMan = 1;
                        rpt.SizeMan = 1;
                        rpt.SuperVisor = 1;
                        rpt.TotalWorker = rpt.NoOfOPPresent + rpt.NoOfHP + rpt.SizeMan + rpt.NoOfInputMan;
                        rpt.PerHourAchivement = rpt.TotalAchivement / rpt.LineHour;
                        rpt.TargetEffi = Math.Round(((rpt.SMV * rpt.TotalTarget) / (rpt.LineHour * 60 * rpt.TotalWorker)) * 100, 2);
                        rpt.AchiveEffi = Math.Round(((rpt.SMV * rpt.TotalAchivement) / (rpt.LineHour * 60 * rpt.TotalWorker)) * 100, 2);
                        rpt.TargetBalance = rpt.TotalAchivement - rpt.TotalTarget;
                        rpt.Remarks = getRemarks.Any() == true ? getRemarks.FirstOrDefault().Remarks : string.Empty;
                        rpt.SectionId = LineMachineQty;
                        rpt.FromDate = FromDate;
                        lstLine.Add(rpt);
                    }
                }
            }

            this.DataList = lstLine;
        }

        public void GetTargetAchivementReportOne(int RefId)
        {
            db = new xOssContext();
            this.FromDate = db.Prod_Reference.Where(a => a.ID == RefId).FirstOrDefault().FromDate;
            var allData = (from t1 in db.Prod_PlanReferenceOrder
                           join t2 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t2.ID
                           join t3 in db.Mkt_BOM on t1.MktBomFk equals t3.ID
                           join t7 in db.Common_TheOrder on t3.Common_TheOrderFk equals t7.ID
                           join t9 in db.Mkt_Buyer on t7.Mkt_BuyerFK equals t9.ID
                           where t1.Prod_ReferenceFK == RefId && t1.SectionId == 2
                           select new
                           {
                               SMVInfo = db.Prod_SMVLayout.Where(a => a.Mkt_BOMFk == t1.MktBomFk && a.TotalSMV == t1.SMV).ToList(),
                               BOMId = t1.MktBomFk,
                               LineId = t1.Plan_ProductionLineFk,
                               OrderNo = t7.BuyerPO,
                               Style = t9.Name + "-" + t3.Style,
                               MachineRunning = t1.MachineRunning,
                               NoOfOptPresent = t1.PresentOperator,
                               NoOfHelperPresent = t1.PresentHelper,
                               WorkHour = t1.WorkingHour,
                               StyleSMV = t1.SMV,
                               PerHourTarget = t1.Quantity,
                               TotalTarget = t1.SewingPlanQuantity,
                               TotalAchivement = (from a in db.Prod_PlanReferenceOrderSection
                                                  join b in db.Prod_PlanOrderSectionSlave on a.ID equals b.Prod_PlanReferenceOrderSectionFk
                                                  where a.Prod_PlanReferenceOrderFk == t1.ID
                                                  group b.Quantity by new { a.Prod_PlanReferenceOrderFk } into all
                                                  select new
                                                  {
                                                      total = all.Sum()
                                                  }).FirstOrDefault()
                           }).ToList();

            List<VmProductionReport> lstLine = new List<VmProductionReport>();
            int LineMachineQty = 0;
            var getAllLines = db.Plan_ProductionLine.Where(a => a.LineNumber != null).OrderBy(a => a.ID).ToList();
            foreach (var line in getAllLines)
            {
                if (allData.Any(a => a.LineId == line.ID))
                {
                    var GetLineInfo = allData.Where(a => a.LineId == line.ID).ToList();

                    LineMachineQty += GetLineInfo.FirstOrDefault().MachineRunning;
                    foreach (var item in GetLineInfo)
                    {
                        VmProductionReport rpt = new VmProductionReport();
                        rpt.ProdReferenceId = RefId;
                        rpt.LineId = line.ID;
                        rpt.LineName = line.Name;
                        rpt.LineMachineRequired = item.SMVInfo.Any() == true ? item.SMVInfo.FirstOrDefault().NoOfOperator : 0;
                        rpt.OrderNo = item.OrderNo;
                        rpt.Item = item.Style;
                        rpt.LineMachineRunning = item.MachineRunning;
                        rpt.NoOfOPPresent = item.NoOfOptPresent;
                        rpt.NoOfHP = item.NoOfHelperPresent;
                        
                        rpt.SMV = item.StyleSMV;
                        rpt.PerHourTarget = item.PerHourTarget;
                        rpt.TotalTarget = item.TotalTarget;
                        rpt.LineHour = item.WorkHour;
                        rpt.TotalAchivement = item.TotalAchivement == null ? 0 : (int)item.TotalAchivement.total;
                        rpt.NoOfInputMan = 1;
                        rpt.SizeMan = 1;
                        rpt.SuperVisor = 1;
                        rpt.TotalWorker = rpt.NoOfOPPresent + rpt.NoOfHP + rpt.SizeMan + rpt.NoOfInputMan;
                        rpt.PerHourAchivement = rpt.TotalAchivement / rpt.LineHour;
                        rpt.TargetEffi = Math.Round(((rpt.SMV * rpt.TotalTarget) / (rpt.LineHour * 60 * rpt.TotalWorker)) * 100, 2);
                        rpt.AchiveEffi = Math.Round(((rpt.SMV * rpt.TotalAchivement) / (rpt.LineHour * 60 * rpt.TotalWorker)) * 100, 2);
                        rpt.TargetBalance = rpt.TotalAchivement - rpt.TotalTarget;
                        rpt.Remarks = string.Empty;
                        rpt.SectionId = LineMachineQty;
                        rpt.FromDate = FromDate;
                        lstLine.Add(rpt);
                    }
                }
            }


            this.DataList = lstLine;
        }

        public void SaveAchivementRemarks(VmProductionReport model)
        {
            if (model.DataList.Any())
            {
                db = new xOssContext();
                foreach (var v in model.DataList)
                {
                    var vCheck = db.Prod_PlanReferenceRemarks.Where(a => a.Prod_ReferenceFK == v.ProdReferenceId && a.Plan_ProductionLineFk == v.LineId && a.MktBomFk == v.BOMID);
                    if (vCheck.Any())
                    {
                        var update = vCheck.FirstOrDefault();
                        if (string.IsNullOrEmpty(v.Remarks))
                        {
                            update.Active = false;
                        }
                        else
                        {
                            update.Remarks = v.Remarks;
                            update.Active = true;
                        }
                        update.LastEdited = DateTime.Now;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(v.Remarks))
                        {
                            Prod_PlanReferenceRemarks Prod_PlanReferenceRemarks = new Prod_PlanReferenceRemarks();
                            Prod_PlanReferenceRemarks.Prod_ReferenceFK = v.ProdReferenceId;
                            Prod_PlanReferenceRemarks.MktBomFk = v.BOMID;
                            Prod_PlanReferenceRemarks.Plan_ProductionLineFk = v.LineId;
                            Prod_PlanReferenceRemarks.Remarks = v.Remarks;
                            Prod_PlanReferenceRemarks.Active = true;
                            Prod_PlanReferenceRemarks.FirstCreated = DateTime.Now;
                            db.Prod_PlanReferenceRemarks.Add(Prod_PlanReferenceRemarks);
                        }
                    }
                }
                db.SaveChanges();
            }
        }

        public void GetAchivementReport(List<VmProductionReport> rptList)
        {
            List<ReportDocType> lstRpt = new List<ReportDocType>();
            List<VmProductionReport> noOfLineMRe = new List<VmProductionReport>();
            List<VmProductionReport> lstLineAchievment = new List<VmProductionReport>();
            if (rptList.Any())
            {
                VmProductionReport m = new VmProductionReport();
                int LineCount = rptList.GroupBy(a=>a.LineId).Count();
                decimal counter = rptList.Count();
                foreach (var v in rptList)
                {
                    var check = noOfLineMRe.Where(a => a.LineId == v.LineId);
                    if (!check.Any())
                    {
                        VmProductionReport a = new VmProductionReport();
                        a.LineId = v.LineId;
                        noOfLineMRe.Add(a);

                        m.NoOfOPPresent += v.NoOfOPPresent;
                        m.NoOfHP += v.NoOfHP;
                        m.TotalWorker += v.TotalWorker;
                        m.LineMachineRunning += v.LineMachineRunning;
                        m.LineMachineRequired += v.LineMachineRequired;
                    }
                    var ckeckAchiev = lstLineAchievment.Where(a => a.LineId == v.LineId);
                    if (!ckeckAchiev.Any())
                    {
                        var takeAllLine = rptList.Where(a => a.LineId == v.LineId);
                        if (takeAllLine.Count() > 1)
                        {
                            decimal TotalHour = 0;
                            decimal TotalLineAcheiv = 0;
                            foreach (var v1 in takeAllLine)
                            {
                                TotalHour += v1.LineHour;
                                TotalLineAcheiv += v1.AchiveEffi * v1.LineHour;
                            }
                            VmProductionReport b = new VmProductionReport();
                            b.LineId = v.LineId;
                            b.AchiveEffi = TotalLineAcheiv / TotalHour;
                            lstLineAchievment.Add(b);
                        }
                        else
                        {
                            VmProductionReport b = new VmProductionReport();
                            b.LineId = v.LineId;
                            b.AchiveEffi = v.AchiveEffi;
                            lstLineAchievment.Add(b);
                        }
                    }
                }

                int LineMan = rptList.GroupBy(a => a.LineId).Count();
                int THour = rptList.Sum(a => a.LineHour);
                int TBalance = rptList.Sum(a => a.TargetBalance);
                int TAchivement = rptList.Sum(a => a.TotalAchivement);
                int TPerHourAchivement = rptList.Sum(a => a.PerHourAchivement);
                int TTarget = rptList.Sum(a => a.TotalTarget);
                int TPerHourTarget = rptList.Sum(a => a.PerHourTarget);
                
                decimal TSMV = rptList.Sum(a => a.SMV) / counter;

                decimal TTargetEffi = Math.Round(rptList.Sum(a => a.TargetEffi) / counter, 3);
                decimal TAchivementEffi = Math.Round(lstLineAchievment.Sum(a => a.AchiveEffi) / LineCount, 3);
                foreach (var v in rptList)
                {
                    ReportDocType rpt = new ReportDocType();
                    rpt.Body1 = v.LineName;
                    rpt.Body2 = v.LineMachineRequired.ToString();
                    rpt.Body3 = v.OrderNo.ToString();
                    rpt.Body4 = v.Item.ToString();
                    rpt.Body5 = v.LineMachineRunning.ToString();
                    rpt.Body6 = v.NoOfOPPresent.ToString();
                    rpt.Body7 = v.NoOfHP.ToString();
                    rpt.Body8 = v.TotalWorker.ToString();

                    rpt.Body9 = v.SizeMan.ToString();
                    rpt.Body10 = v.SuperVisor.ToString();
                    rpt.Body11 = v.NoOfInputMan.ToString();
                    
                    rpt.Body12 = v.SMV.ToString();
                    rpt.Body13 = v.PerHourTarget.ToString();
                    rpt.Body14 = v.TotalTarget.ToString();
                    rpt.Body15 = v.LineHour.ToString();
                    rpt.Body16 = v.PerHourAchivement.ToString();
                    rpt.Body17 = v.TotalAchivement.ToString();
                    rpt.Body18 = v.TargetEffi.ToString();
                    rpt.Body19 = v.AchiveEffi.ToString();
                    rpt.Body20 = v.TargetBalance.ToString();
                    rpt.Body21 = v.Remarks;

                    rpt.SubBody1 = m.LineMachineRequired.ToString(); //LineMachine.ToString();
                    rpt.SubBody2 = m.LineMachineRunning.ToString(); //TotalLineMachine.ToString();
                    rpt.SubBody3 = m.NoOfOPPresent.ToString(); //TOptPresent.ToString();
                    rpt.SubBody4 = m.NoOfHP.ToString(); //THelperPresent.ToString();
                    rpt.SubBody5 = m.TotalWorker.ToString();
                    //rpt.SubBody6 = 14.ToString();
                    //rpt.SubBody7 = 14.ToString();
                    //rpt.SubBody8 = 14.ToString();
                    rpt.SubBody6 = LineMan.ToString();
                    rpt.SubBody7 = LineMan.ToString();
                    rpt.SubBody8 = LineMan.ToString();
                    
                    rpt.SubBody9 = Math.Round(TSMV, 2).ToString();
                    rpt.SubBody10 = TPerHourTarget.ToString();
                    rpt.SubBody11 = TTarget.ToString();
                    rpt.SubBody12 = THour.ToString();
                    rpt.SubBody13 = TPerHourAchivement.ToString();
                    rpt.SubBody14 = TAchivement.ToString();
                    rpt.SubBody15 = TTargetEffi.ToString();
                    rpt.SubBody16 = TAchivementEffi.ToString();
                    rpt.SubBody17 = TBalance.ToString();

                    rpt.HeaderLeft1 = ShortDateString(v.FromDate);
                    lstRpt.Add(rpt);
                }
            }
            this.ReportDoc = lstRpt;
        }

        //public void GetProductionOrderSummary(int RefId)
        //{
        //    db = new xOssContext();

        //    var vDetails = (from b in db.Prod_PlanReferenceOrder
        //                    join c in db.Mkt_OrderDeliverySchedule on b.OrderDeliveryScheduleFk equals c.ID
        //                    join d in db.Mkt_BOM on c.Mkt_BOMFk equals d.ID
        //                    join e in db.Common_TheOrder on c.Common_TheOrderFk equals e.ID
        //                    join f in db.Mkt_Buyer on e.Mkt_BuyerFK equals f.ID
        //                    join g in db.Plan_ProductionLine on b.Plan_ProductionLineFk equals g.ID
        //                    where b.Prod_ReferenceFK == RefId && b.SectionId == 2
        //                    && c.Active == true && d.Active == true && e.Active == true
        //                    select new
        //                    {
        //                        BOMID = d.ID,
        //                        BuyerName = f.Name,
        //                        OrderNo = e.BuyerPO,
        //                        Item = e.BuyerPO + " " + d.Style,
        //                        Quantity = d.QPack * d.Quantity,
        //                        PackPice = d.Quantity,
        //                        SectionId = b.SectionId,
        //                        LineId = b.Plan_ProductionLineFk,
        //                        LineName = g.Name
        //                    }).ToList();

        //    var vData = (from o in vDetails
        //                 group new { o.LineId } by new { o.BOMID, o.BuyerName, o.OrderNo, o.Item, o.Quantity, o.PackPice } into all
        //                 select new
        //                 {
        //                     BOMID = all.Key.BOMID,
        //                     BuyerName = all.Key.BuyerName,
        //                     OrderNo = all.Key.OrderNo,
        //                     Item = all.Key.Item,
        //                     Quantity = all.Key.Quantity,
        //                     PackPice = all.Key.PackPice,
        //                     LineList = all.GroupBy(x => x.LineId).Select(m => new { m.Key, db.Plan_ProductionLine.FirstOrDefault(a => a.ID == m.Key).Name }).ToList()
        //                 }).ToList();

        //    List<VmProductionReport> lst = new List<VmProductionReport>();
        //    if (vData.Any())
        //    {
        //        foreach (var v in vData)
        //        {
        //            VmProductionReport model = new VmProductionReport();
        //            model.BOMID = v.BOMID;
        //            model.ProdReferenceId = RefId;
        //            model.BuyerName = v.BuyerName;
        //            model.OrderNo = v.OrderNo;
        //            model.Item = v.Item;
        //            model.Quantity = v.Quantity;
        //            if (v.LineList.Any())
        //            {
        //                model.LineCount = v.LineList.Count();
        //                foreach (var v1 in v.LineList)
        //                {
        //                    model.LineName += v1.Name + "/";
        //                }
        //            }

        //            var SewingData = GetSectionWiseDayProduction(v.BOMID, RefId, 2);
        //            if (SewingData.Any())
        //            {
        //                model.DaySewing = SewingData.Sum(a => a.Quantity);
        //                model.TotalWorker = SewingData.Sum(a => a.TotalWorker);
        //            }

        //            model.TotalCutting = GetTotalProductionQty(v.BOMID, 1);
        //            model.TotalSewing = GetTotalProductionQty(v.BOMID, 2);
        //            model.TargetBalance = model.TotalSewing - model.Quantity;
        //            model.TotalPacking = GetTotalProductionQty(v.BOMID, 4);

        //            if (model.TotalPacking > 0)
        //            {
        //                model.TotalCTN = (int)(model.TotalPacking / v.PackPice);
        //            }
        //            if (v.Quantity > 0)
        //            {
        //                model.CuttingRate = Math.Round(decimal.Multiply(decimal.Divide(model.TotalCutting, model.Quantity), 100), 2);
        //                model.SewingRate = Math.Round(decimal.Multiply(decimal.Divide(model.TotalSewing, model.Quantity), 100), 2);
        //                model.PackingRate = Math.Round(decimal.Multiply(decimal.Divide(model.TotalPacking, model.Quantity), 100), 2);
        //            }
        //            //if (model.TotalCutting > 0)
        //            //{
        //            //    model.SewingRate = Math.Round(decimal.Multiply(decimal.Divide(model.TotalSewing, model.Quantity), 100), 2);
        //            //}
        //            //if (model.TotalSewing > 0)
        //            //{
        //            //    model.PackingRate = Math.Round(decimal.Multiply(decimal.Divide(model.TotalPacking, model.Quantity), 100), 2);
        //            //}
        //            lst.Add(model);
        //        }
        //    }
        //    this.DataList1 = lst;
        //}

        private int GetLineProduction(int RefId, int BomId, int LineId)
        {
            var vData = (from o in db.Prod_PlanReferenceOrder
                         join q in db.Prod_PlanReferenceOrderSection on o.ID equals q.Prod_PlanReferenceOrderFk
                         join p in db.Prod_PlanOrderSectionSlave on q.ID equals p.Prod_PlanReferenceOrderSectionFk
                         where o.MktBomFk == BomId && q.Plan_ReferenceSectionFk == 2 && o.Plan_ProductionLineFk == LineId
                         && o.Prod_ReferenceFK == RefId
                         select new { qty = p.Quantity }).ToList();
            return vData.Any() == true ? (int)vData.Sum(a => a.qty) : 0;
        }

        public void GetProductionOrderSummary(int RefId)
        {
            db = new xOssContext();

            var vDetails = (from t1 in db.Prod_PlanReferenceOrder
                            join t2 in db.Mkt_BOM on t1.MktBomFk equals t2.ID
                            join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                            join t4 in db.Mkt_Buyer on t3.Mkt_BuyerFK equals t4.ID
                            join t5 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t5.ID
                            where t1.Prod_ReferenceFK == RefId && t1.SectionId == 2
                            group new { t1.Plan_ProductionLineFk } by new
                            {
                                BOMID = t1.MktBomFk,
                                BuyerName = t4.Name,
                                OrderNo = t3.BuyerPO,
                                Style = t2.Style,
                                QPack = t2.QPack,
                                PackPice = t2.Quantity,
                                setQty=t2.SetQuantity,
                            } into all
                            select new
                            {
                                BOMID = all.Key.BOMID,
                                BuyerName = all.Key.BuyerName,
                                OrderNo = all.Key.OrderNo,
                                Item = all.Key.OrderNo + " " + all.Key.Style,
                                Quantity = (all.Key.QPack * all.Key.PackPice)*all.Key.setQty,
                                PackPice = all.Key.PackPice,
                                LineList = all.GroupBy(x => x.Plan_ProductionLineFk).Select(m => new { m.Key, db.Plan_ProductionLine.FirstOrDefault(a => a.ID == m.Key).Name }).ToList()
                            }).ToList();



            List<VmProductionReport> lst = new List<VmProductionReport>();
            if (vDetails.Any())
            {
                foreach (var v in vDetails)
                {
                    VmProductionReport model = new VmProductionReport();
                    model.BOMID = v.BOMID;
                    model.ProdReferenceId = RefId;
                    model.BuyerName = v.BuyerName;
                    model.OrderNo = v.OrderNo;
                    model.Item = v.Item;
                    model.Quantity = v.Quantity;

                    if (v.LineList.Any())
                    {
                        model.LineCount = v.LineList.Count();
                        foreach (var v1 in v.LineList)
                        {
                            model.LineOutput += v1.Name + "(" + GetLineProduction(RefId, v.BOMID, v1.Key) + ") | ";
                            model.LineName += v1.Name + "|";
                        }
                    }
                    var SewingData = GetDayLineProductionBySectionWise(v.BOMID, RefId, 2);
                    if (SewingData.Any())
                    {
                        model.DaySewing = SewingData.Sum(a => a.Quantity);
                        model.TotalWorker = SewingData.Sum(a => a.TotalWorker);
                    }

                    model.TotalCutting = GetTotalProductionQtyBySectionWise(v.BOMID, 1);
                    model.TotalSewing = GetTotalProductionQtyBySectionWise(v.BOMID, 2);
                    model.TargetBalance = model.TotalSewing - model.Quantity;
                    model.TotalPacking = GetTotalProductionQtyBySectionWise(v.BOMID, 4);

                    if (model.TotalPacking > 0)
                    {
                        model.TotalCTN = (int)(model.TotalPacking / v.PackPice);
                    }
                    if (v.Quantity > 0)
                    {
                        model.CuttingRate = Math.Round(decimal.Multiply(decimal.Divide(model.TotalCutting, model.Quantity), 100), 2);
                        model.SewingRate = Math.Round(decimal.Multiply(decimal.Divide(model.TotalSewing, model.Quantity), 100), 2);
                        model.PackingRate = Math.Round(decimal.Multiply(decimal.Divide(model.TotalPacking, model.Quantity), 100), 2);
                    }

                    lst.Add(model);
                }
            }
            this.DataList1 = lst;
        }

        public void GetBuyerOrderProductionDetails(int BomId, int RefId)
        {
            db = new xOssContext();

            //var vData = (from t0 in db.Prod_PlanReferenceOrder
            //             join t1 in db.Prod_PlanReferenceOrderSection on t0.ID equals t1.Prod_PlanReferenceOrderFk
            //             join t2 in db.Mkt_BOM on t0.MktBomFk equals t2.ID
            //             join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
            //             join t4 in db.Mkt_Buyer on t3.Mkt_BuyerFK equals t4.ID
            //             join t5 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t5.ID
            //             where t0.MktBomFk == BomId && t0.Prod_ReferenceFK==RefId
            //             group t1.Quantity by new { BomId = t0.MktBomFk, OrderNo =t3.BuyerPO, BuyerName=t4.Name, Style=t2.Style,  ColorName=t5.Color, Size=t5.Size,Qty=t5.Quantity } into all
            //             select new
            //             {
            //                 BomId = all.Key.BomId,
            //                 BuyerName = all.Key.BuyerName,
            //                 OrderNo = all.Key.OrderNo,
            //                 Item = all.Key.Style,
            //                 ColorName = all.Key.ColorName,
            //                 Size= all.Key.Size,
            //                 OrderQty=all.Key.Qty
            //             }).ToList();
            //List<VmProductionReport> lst = new List<VmProductionReport>();
            //if (vData.Any())
            //{
            //    foreach (var v in vData)
            //    {
            //        VmProductionReport model = new VmProductionReport();
            //        model.BOMID = (int)v.BomId;
            //        model.ProdReferenceId = RefId;
            //        model.OrderNo = v.OrderNo;
            //        model.BuyerName = v.BuyerName;
            //        model.Item = v.Item;
            //        model.ColorName = v.ColorName;
            //        model.Size = v.Size;
            //        model.Quantity = (int)v.OrderQty;
            //        model.TotalCutting = GetSectionWiseColorSizeDoneQty((int)v.BomId, 1, v.ColorName, v.Size);
            //        model.DaySewing = GetSectionWiseColorSizeDayProduction((int)v.BomId, RefId, 2, v.ColorName, v.Size);
            //        model.TotalSewing = GetSectionWiseColorSizeDoneQty((int)v.BomId, 2, v.ColorName, v.Size);
            //        model.SewingBalance = model.TotalSewing - model.Quantity;
            //        lst.Add(model);
            //    }
            //}

            var vData = (from t0 in db.Common_TheOrder
                         join t1 in db.Mkt_BOM on t0.ID equals t1.Common_TheOrderFk
                         join t2 in db.Mkt_Item on t1.Mkt_ItemFK equals t2.ID
                         join t3 in db.Mkt_Buyer on t0.Mkt_BuyerFK equals t3.ID
                         join t4 in db.Mkt_OrderDeliverySchedule on t1.ID equals t4.Mkt_BOMFk
                         join t5 in db.Mkt_OrderColorAndSizeRatio on t4.ID equals t5.OrderDeliveryScheduleFk
                         where t1.ID == BomId && t1.Active == true && t2.Active == true && t4.Active == true && t5.Active == true
                         group t5 by new { t0.BuyerPO, t2.Name, t3, t4.Mkt_BOMFk, t5.Color } into all
                         select new
                         {
                             BomId = all.Key.Mkt_BOMFk,
                             BuyerName = all.Key.t3.Name,
                             OrderNo = all.Key.BuyerPO,
                             Item = all.Key.Name,
                             ColorName = all.Key.Color,
                             SizeList = all.GroupBy(x => x.Size).Select(m => new
                             {
                                 Size = m.Key
                             }).ToList()
                         }).ToList();
            List<VmProductionReport> lst = new List<VmProductionReport>();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    if (v.SizeList.Any())
                    {
                        foreach (var v1 in v.SizeList)
                        {
                            VmProductionReport model = new VmProductionReport();
                            model.BOMID = (int)v.BomId;
                            model.ProdReferenceId = RefId;
                            model.OrderNo = v.OrderNo;
                            model.BuyerName = v.BuyerName;
                            model.Item = v.Item;
                            model.ColorName = v.ColorName;
                            model.Size = v1.Size;
                            model.Quantity = GetColorSizeOrderQty((int)v.BomId, v.ColorName, v1.Size);
                            model.TotalCutting = GetSectionWiseColorSizeDoneQty((int)v.BomId, 1, v.ColorName, v1.Size);
                            model.LineName = GetSectionWiseColorSizeDayLineProduction((int)v.BomId, RefId, 2, v.ColorName, v1.Size);
                            model.DaySewing = GetSectionWiseColorSizeDayProduction((int)v.BomId, RefId, 2, v.ColorName, v1.Size);
                            model.TotalSewing = GetSectionWiseColorSizeDoneQty((int)v.BomId, 2, v.ColorName, v1.Size);
                            model.SewingBalance = model.TotalSewing - model.Quantity;
                            lst.Add(model);
                        }
                    }
                }
            }
            this.DataList = lst;
        }

        public void GetDailySewingProduction1(int RefId)
        {
            db = new xOssContext();

            if (SectionId == 1)
            {
                var VData1 = (from o in db.Prod_PlanReferenceOrder
                              join t in db.Prod_PlanReferenceOrderSection on o.ID equals t.Prod_PlanReferenceOrderFk
                              join p in db.Mkt_OrderDeliverySchedule on t.OrderDeliveryScheduleFk equals p.ID
                              join q in db.Mkt_OrderColorAndSizeRatio on t.Mkt_OrderColorAndSizeRatioFk equals q.ID
                              join r in db.Mkt_BOM on p.Mkt_BOMFk equals r.ID
                              join s in db.Common_TheOrder on p.Common_TheOrderFk equals s.ID
                              where o.Prod_ReferenceFK == RefId && p.Active == true && q.Active == true && r.Active == true && o.SectionId == SectionId
                              group q.Quantity by new { s.CID, r.Style, p.Mkt_BOMFk, q.Color, q.Size, o } into all
                              select new VmProductionReport
                              {
                                  BOMID = (int)all.Key.Mkt_BOMFk,
                                  OrderNo = all.Key.CID,
                                  Style = all.Key.Style,
                                  ColorName = all.Key.Color,
                                  Size = all.Key.Size,
                                  ExtraRate = all.Key.o.CuttingExtra,
                                  LayerWeight = all.Key.o.PerLayerWeight,
                                  MarkerPic = all.Key.o.MarkerPiece,
                                  MarkerLength = all.Key.o.MarkerLength,
                                  MarkerWidth = all.Key.o.MarkerWidth,
                                  Quantity = (int)all.Sum()
                              }).ToList();
                #region CuttingSection

                if (VData1.Any())
                {
                    foreach (var v in VData1)
                    {
                        var CQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, SectionId);

                        v.DayCutting = CQty.DayCutting;
                        v.TotalCutting = CQty.TotalCutting;
                        decimal d = Decimal.Multiply(v.Quantity, v.ExtraRate) / (decimal)100;
                        v.ExtraQty = (int)Math.Round(d);
                        v.TotalTarget = v.ExtraQty + v.Quantity;
                        if (v.LayerWeight > 0 && v.MarkerPic > 0)
                        {
                            v.DayCuttingWeight = Math.Round((v.LayerWeight / v.MarkerPic) * v.DayCutting, 2);
                            v.TotalWeight = Math.Round((v.LayerWeight / v.MarkerPic) * v.TotalCutting, 2);
                        }
                        v.BalanceQty = v.TotalTarget - v.TotalCutting;
                        if (v.MarkerPic > 0)
                        {
                            v.CuttingConsumption = (v.LayerWeight / v.MarkerPic) * 12;
                        }
                    }
                }

                this.DataList = VData1;
                #endregion
            }
            else
            {
                var VData = (from o in db.Prod_PlanReferenceOrder
                             join t in db.Prod_PlanReferenceOrderSection on o.ID equals t.Prod_PlanReferenceOrderFk
                             join p in db.Mkt_OrderDeliverySchedule on t.OrderDeliveryScheduleFk equals p.ID
                             join q in db.Mkt_OrderColorAndSizeRatio on t.Mkt_OrderColorAndSizeRatioFk equals q.ID
                             join r in db.Mkt_BOM on o.MktBomFk equals r.ID
                             join s in db.Common_TheOrder on p.Common_TheOrderFk equals s.ID
                             where o.Prod_ReferenceFK == RefId && p.Active == true && q.Active == true && r.Active == true && o.SectionId == SectionId
                             group q.Quantity by new { s.CID, r.Style, p.Mkt_BOMFk, q.Color, q.Size } into all
                             select new VmProductionReport
                             {
                                 BOMID = (int)all.Key.Mkt_BOMFk,
                                 OrderNo = all.Key.CID,
                                 Style = all.Key.Style,
                                 ColorName = all.Key.Color,
                                 Size = all.Key.Size,
                                 Quantity = (int)all.Sum()
                             }).ToList();

                #region SewingSection

                if (VData.Any())
                {
                    foreach (var v in VData)
                    {
                        if (SectionId == 2)
                        {
                            var SQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, SectionId);
                            v.DaySewing = SQty.DaySewing;
                            v.TotalSewing = SQty.TotalSewing;
                        }
                        else if (SectionId == 3)
                        {
                            var IQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, SectionId);
                            v.DayIroning = IQty.DayIroning;
                            v.TotalIroning = IQty.TotalIroning;
                        }
                        else if (SectionId == 4)
                        {
                            var PQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, SectionId);
                            v.DayPacking = PQty.DayPacking;
                            v.TotalPacking = PQty.TotalPacking;
                        }
                        else if (SectionId == 0)
                        {
                            var SQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, SectionId);
                            var IQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, SectionId);
                            var PQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, SectionId);
                            v.DaySewing = SQty.DaySewing;
                            v.TotalSewing = SQty.TotalSewing;
                            v.DayIroning = IQty.DayIroning;
                            v.TotalIroning = IQty.TotalIroning;
                            v.DayPacking = PQty.DayPacking;
                            v.TotalPacking = PQty.TotalPacking;
                        }
                    }
                }
                this.DataList = VData;
                #endregion
            }
            //else if (SectionId==3)
            //{
            //    #region IronSection

            //    if (VData.Any())
            //    {
            //        foreach (var v in VData)
            //        {
            //            var IQty = GetOrderSizeDoneQty(RefId, (int)v.BomId, v.ColorName, v.Size, SectionId);

            //            VmProductionReport m = new VmProductionReport();
            //            m.BOMID = (int)v.BomId;
            //            m.OrderNo = v.OrderNo;
            //            m.Style = v.Style;
            //            m.ColorName = v.ColorName;
            //            m.Size = v.Size;
            //            m.Quantity = (int)v.Quantity;
            //            m.DayIroning = IQty.DayIroning;
            //            m.TotalIroning = IQty.TotalIroning;

            //            if (m.Quantity > 0)
            //            {
            //                m.IroningRate = Math.Round(decimal.Multiply(decimal.Divide(m.TotalIroning, m.Quantity), 100), SectionId);
            //            }
            //            lst.Add(m);
            //        }
            //    }

            //    #endregion
            //}
            //else if (SectionId==4)
            //{
            //    #region PackingSection
            //    //var VData = (from t0 in db.Prod_PlanReferenceOrderSection
            //    //             join t1 in db.Mkt_OrderDeliverySchedule on t0.OrderDeliveryScheduleFk equals t1.ID
            //    //             join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
            //    //             join t3 in db.Mkt_BOM on t1.Mkt_BOMFk equals t3.ID
            //    //             join t4 in db.Common_TheOrder on t1.Common_TheOrderFk equals t4.ID
            //    //             join t5 in db.Prod_PlanReferenceOrder on t0.Prod_PlanReferenceOrderFk equals t5.ID
            //    //             where t5.Prod_ReferenceFK == RefId && t0.Plan_ReferenceSectionFk == SectionId
            //    //             && t1.Active == true && t2.Active == true && t4.Active == true && t5.Active == true
            //    //             group new { t0.Quantity } by new { t4.CID, t3.QPack, t3.Style, t1.Mkt_BOMFk, t2.Color, t2.Size, t2.Quantity } into all
            //    //             select new
            //    //             {
            //    //                 BomId = all.Key.Mkt_BOMFk,
            //    //                 OrderNo = all.Key.CID,
            //    //                 Style = all.Key.Style,
            //    //                 PackPice = all.Key.QPack,
            //    //                 ColorName = all.Key.Color,
            //    //                 Size = all.Key.Size,
            //    //                 Quantity = all.Key.Quantity
            //    //             }).ToList();

            //    if (VData.Any())
            //    {
            //        foreach (var v in VData)
            //        {
            //            var PQty = GetOrderSizeDoneQty(RefId, (int)v.BomId, v.ColorName, v.Size, SectionId);

            //            VmProductionReport m = new VmProductionReport();
            //            m.BOMID = (int)v.BomId;
            //            m.OrderNo = v.OrderNo;
            //            m.Style = v.Style;
            //            m.ColorName = v.ColorName;
            //            m.Size = v.Size;
            //            m.Quantity = (int)v.Quantity;
            //            m.DayPacking = PQty.DayPacking;
            //            m.TotalPacking = PQty.TotalPacking;

            //            if (m.Quantity > 0)
            //            {
            //                m.PackingRate = Math.Round(decimal.Multiply(decimal.Divide(m.TotalPacking, m.Quantity), 100), SectionId);
            //            }
            //            lst.Add(m);
            //        }
            //    }

            //    #endregion
            //}
            //else if (SectionId == 0)
            //{
            //    #region AllSection
            //    //var VData1 = (from t0 in db.Prod_PlanReferenceOrderSection
            //    //             join t1 in db.Mkt_OrderDeliverySchedule on t0.OrderDeliveryScheduleFk equals t1.ID
            //    //             join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
            //    //             join t3 in db.Mkt_BOM on t1.Mkt_BOMFk equals t3.ID
            //    //             join t4 in db.Common_TheOrder on t1.Common_TheOrderFk equals t4.ID
            //    //             join t5 in db.Prod_PlanReferenceOrder on t0.Prod_PlanReferenceOrderFk equals t5.ID
            //    //             where t5.Prod_ReferenceFK == RefId && t1.Active == true && t2.Active == true && t4.Active == true && t5.Active == true
            //    //             group new { t0.Quantity } by new { t4.CID, t3.QPack, t3.Style, t1.Mkt_BOMFk, t2.Color, t2.Size, t2.Quantity } into all
            //    //             select new
            //    //             {
            //    //                 BomId = all.Key.Mkt_BOMFk,
            //    //                 OrderNo = all.Key.CID,
            //    //                 Style = all.Key.Style,
            //    //                 PackPice = all.Key.QPack,
            //    //                 ColorName = all.Key.Color,
            //    //                 Size = all.Key.Size,
            //    //                 Quantity = all.Key.Quantity
            //    //             }).ToList();

            //    if (VData.Any())
            //    {
            //        foreach (var v in VData)
            //        {
            //            var CQty = GetOrderSizeDoneQty(RefId, (int)v.BomId, v.ColorName, v.Size, 1);
            //            var SQty = GetOrderSizeDoneQty(RefId, (int)v.BomId, v.ColorName, v.Size, 2);
            //            var IQty = GetOrderSizeDoneQty(RefId, (int)v.BomId, v.ColorName, v.Size, 3);
            //            var PQty = GetOrderSizeDoneQty(RefId, (int)v.BomId, v.ColorName, v.Size, 4);
            //            VmProductionReport m = new VmProductionReport();
            //            m.BOMID = (int)v.BomId;
            //            m.OrderNo = v.OrderNo;
            //            m.Style = v.Style;
            //            m.ColorName = v.ColorName;
            //            m.Size = v.Size;
            //            m.Quantity = (int)v.Quantity;
            //            m.DayCutting = CQty.DayCutting;
            //            m.TotalCutting = CQty.TotalCutting;

            //            m.DaySewing = SQty.DaySewing;
            //            m.TotalSewing = SQty.TotalSewing;
            //            m.DayIroning = IQty.DayIroning;
            //            m.TotalIroning = IQty.TotalIroning;
            //            m.DayPacking = PQty.DayPacking;
            //            m.TotalPacking = PQty.TotalPacking;
            //            //if (m.TotalPacking > 0)
            //            //{
            //            //    m.TotalCTN = (int)(m.TotalPacking / v.PackPice);
            //            //}
            //            if (m.Quantity > 0)
            //            {
            //                m.CuttingRate = Math.Round(decimal.Multiply(decimal.Divide(m.TotalCutting, m.Quantity), 100), 2);
            //                m.SewingRate = Math.Round(decimal.Multiply(decimal.Divide(m.TotalSewing, m.Quantity), 100), 2);
            //                m.IroningRate = Math.Round(decimal.Multiply(decimal.Divide(m.TotalIroning, m.Quantity), 100), 2);
            //                m.PackingRate = Math.Round(decimal.Multiply(decimal.Divide(m.TotalPacking, m.Quantity), 100), 2);
            //            }
            //            lst.Add(m);
            //        }
            //    }

            //    #endregion
            //}

            //this.DataList = lst;
        }

        public void GetDailySewingProduction(int RefId)
        {
            db = new xOssContext();

            var VData = (from o in db.Prod_PlanReferenceOrder
                         join t in db.Prod_PlanReferenceOrderSection on o.ID equals t.Prod_PlanReferenceOrderFk
                         join p in db.Mkt_OrderDeliverySchedule on t.OrderDeliveryScheduleFk equals p.ID
                         join q in db.Mkt_OrderColorAndSizeRatio on t.Mkt_OrderColorAndSizeRatioFk equals q.ID
                         join r in db.Mkt_BOM on o.MktBomFk equals r.ID
                         join s in db.Common_TheOrder on p.Common_TheOrderFk equals s.ID
                         where o.Prod_ReferenceFK == RefId && p.Active == true && q.Active == true && r.Active == true && o.SectionId == 2
                         group q.Quantity by new { s.CID, r.Style, p.Mkt_BOMFk, q.Color, q.Size } into all
                         select new VmProductionReport
                         {
                             BOMID = (int)all.Key.Mkt_BOMFk,
                             OrderNo = all.Key.CID,
                             Style = all.Key.Style,
                             ColorName = all.Key.Color,
                             Size = all.Key.Size,
                             Quantity = (int)all.Sum()
                         }).ToList();

            #region SewingSection

            if (VData.Any())
            {
                foreach (var v in VData)
                {
                    var CQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, 1);
                    v.DayCutting = CQty.DayCutting;
                    v.TotalCutting = CQty.TotalCutting;
                    var SQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, 2);
                    v.DaySewing = SQty.DaySewing;
                    v.TotalSewing = SQty.TotalSewing;
                    v.BalanceQty = v.Quantity - v.TotalSewing;
                }
            }
            this.DataList = VData;
            #endregion
        }

        public void GetDailyIroningProduction(int RefId)
        {
            db = new xOssContext();

            var VData = (from o in db.Prod_PlanReferenceOrder
                         join t in db.Prod_PlanReferenceOrderSection on o.ID equals t.Prod_PlanReferenceOrderFk
                         join p in db.Mkt_OrderDeliverySchedule on t.OrderDeliveryScheduleFk equals p.ID
                         join q in db.Mkt_OrderColorAndSizeRatio on t.Mkt_OrderColorAndSizeRatioFk equals q.ID
                         join r in db.Mkt_BOM on o.MktBomFk equals r.ID
                         join s in db.Common_TheOrder on p.Common_TheOrderFk equals s.ID
                         where o.Prod_ReferenceFK == RefId && p.Active == true && q.Active == true && r.Active == true && o.SectionId == 3
                         group q.Quantity by new { s.CID, r.Style, p.Mkt_BOMFk, q.Color, q.Size } into all
                         select new VmProductionReport
                         {
                             BOMID = (int)all.Key.Mkt_BOMFk,
                             OrderNo = all.Key.CID,
                             Style = all.Key.Style,
                             ColorName = all.Key.Color,
                             Size = all.Key.Size,
                             Quantity = (int)all.Sum()
                         }).ToList();

            #region IroningSection

            if (VData.Any())
            {
                foreach (var v in VData)
                {
                    var SQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, 2);
                    v.DaySewing = SQty.DaySewing;
                    v.TotalSewing = SQty.TotalSewing;
                    var IQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, 3);
                    v.DayIroning = IQty.DayIroning;
                    v.TotalIroning = IQty.TotalIroning;
                    v.BalanceQty = v.Quantity - v.TotalIroning;
                }
            }
            this.DataList = VData;
            #endregion

        }

        public void GetDailyPackingProduction(int RefId)
        {
            db = new xOssContext();

            var VData = (from o in db.Prod_PlanReferenceOrder
                         join t in db.Prod_PlanReferenceOrderSection on o.ID equals t.Prod_PlanReferenceOrderFk
                         join p in db.Mkt_OrderDeliverySchedule on t.OrderDeliveryScheduleFk equals p.ID
                         join q in db.Mkt_OrderColorAndSizeRatio on t.Mkt_OrderColorAndSizeRatioFk equals q.ID
                         join r in db.Mkt_BOM on o.MktBomFk equals r.ID
                         join s in db.Common_TheOrder on p.Common_TheOrderFk equals s.ID
                         where o.Prod_ReferenceFK == RefId && p.Active == true && q.Active == true && r.Active == true && o.SectionId == 4
                         group q.Quantity by new { s.CID, r.Style, p.Mkt_BOMFk, q.Color, q.Size } into all
                         select new VmProductionReport
                         {
                             BOMID = (int)all.Key.Mkt_BOMFk,
                             OrderNo = all.Key.CID,
                             Style = all.Key.Style,
                             ColorName = all.Key.Color,
                             Size = all.Key.Size,
                             Quantity = (int)all.Sum()
                         }).ToList();

            #region PackingSection

            if (VData.Any())
            {
                foreach (var v in VData)
                {
                    var IQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, 3);
                    v.DayIroning = IQty.DayIroning;
                    v.TotalIroning = IQty.TotalIroning;
                    var PQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, 4);
                    v.DayPacking = PQty.DayPacking;
                    v.TotalPacking = PQty.TotalPacking;
                    v.BalanceQty = v.Quantity - v.TotalPacking;
                }
            }
            this.DataList = VData;
            #endregion
        }

        public void GetDailyCuttingProduction(int RefId)
        {
            db = new xOssContext();
            
            var VData1 = (from o in db.Prod_PlanReferenceOrder
                          join t in db.Prod_PlanReferenceOrderSection on o.ID equals t.Prod_PlanReferenceOrderFk
                          join p in db.Mkt_OrderDeliverySchedule on t.OrderDeliveryScheduleFk equals p.ID
                          join q in db.Mkt_OrderColorAndSizeRatio on t.Mkt_OrderColorAndSizeRatioFk equals q.ID
                          join r in db.Mkt_BOM on p.Mkt_BOMFk equals r.ID
                          join s in db.Common_TheOrder on p.Common_TheOrderFk equals s.ID
                          where o.Prod_ReferenceFK == RefId && p.Active == true && q.Active == true && r.Active == true && o.SectionId == 1
                          group q.Quantity by new { s.CID, r.Style, p.Mkt_BOMFk, q.Color, q.Size, o } into all
                          select new VmProductionReport
                          {
                              BOMID = (int)all.Key.Mkt_BOMFk,
                              OrderNo = all.Key.CID,
                              Style = all.Key.Style,
                              ColorName = all.Key.Color,
                              Size = all.Key.Size,
                              ExtraRate = all.Key.o.CuttingExtra,
                              GSM=all.Key.o.GSM,
                              LayerWeight = all.Key.o.PerLayerWeight,
                              MarkerPic = all.Key.o.MarkerPiece,
                              MarkerLength = all.Key.o.MarkerLength,
                              MarkerWidth = all.Key.o.MarkerWidth,
                              Quantity = (int)all.Sum()
                          }).ToList();
            #region CuttingSection

            if (VData1.Any())
            {
                foreach (var v in VData1)
                {
                    var CQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, 1);

                    v.DayCutting = CQty.DayCutting;
                    v.TotalCutting = CQty.TotalCutting;
                    decimal d = Decimal.Multiply(v.Quantity, v.ExtraRate) / (decimal)100;
                    v.ExtraQty = (int)Math.Round(d);
                    v.TotalTarget = v.ExtraQty + v.Quantity;
                    if (v.LayerWeight > 0 && v.MarkerPic > 0)
                    {
                        v.DayCuttingWeight = Math.Round((v.LayerWeight / v.MarkerPic) * v.DayCutting, 2);
                        v.TotalWeight = Math.Round((v.LayerWeight / v.MarkerPic) * v.TotalCutting, 2);
                    }
                    v.BalanceQty = v.TotalTarget - v.TotalCutting;
                    if (v.MarkerPic > 0)
                    {
                        v.CuttingConsumption = (v.LayerWeight / v.MarkerPic) * 12;
                    }

                }
            }

            this.DataList = VData1;
            #endregion
        }

        public void GetOrderProduction(int BomId)
        {
            db = new xOssContext();
            int RefId = 0;
            this.SectionId = SectionId;

            var VData = (from o in db.Common_TheOrder
                         join p in db.Mkt_BOM on o.ID equals p.Common_TheOrderFk
                         join q in db.Mkt_OrderDeliverySchedule on p.ID equals q.Mkt_BOMFk
                         join r in db.Mkt_OrderColorAndSizeRatio on q.ID equals r.OrderDeliveryScheduleFk
                         where o.Active == true && p.Active == true && q.Active == true && r.Active == true && q.Mkt_BOMFk == BomId
                         group r.Quantity by new { o.CID, p.Style, q.Mkt_BOMFk, r.Color, r.Size } into all
                         select new VmProductionReport
                         {
                             BOMID = (int)all.Key.Mkt_BOMFk,
                             OrderNo = all.Key.CID,
                             Style = all.Key.Style,
                             ColorName = all.Key.Color,
                             Size = all.Key.Size,
                             Quantity = (int)all.Sum()
                         }).ToList();

            var getRef = db.Prod_Reference.Where(a => a.FromDate == DateTime.Today);
            if (getRef.Any())
            {
                RefId = getRef.FirstOrDefault().ID;
            }

            #region a
            if (VData.Any())
            {
                foreach (var v in VData)
                {
                    var CQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, 1);
                    v.DayCutting = CQty.DayCutting;
                    v.TotalCutting = CQty.TotalCutting;
                    var SQty = GetOrderSizeDoneQty(RefId, (int)v.BOMID, v.ColorName, v.Size, 2);
                    v.DaySewing = SQty.DaySewing;
                    v.TotalSewing = SQty.TotalSewing;
                    v.BalanceQty = v.Quantity - v.TotalSewing;
                }
            }
            this.DataList = VData;
            #endregion
        }

        private int GetSectionWiseColorSizeDoneQty(int BomId, int SectionId, string ColorName, string SizeName)
        {
            db = new xOssContext();
            var vData = (from o in db.Prod_PlanReferenceOrder
                         join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                         join q in db.Mkt_OrderDeliverySchedule on p.OrderDeliveryScheduleFk equals q.ID
                         join r in db.Mkt_OrderColorAndSizeRatio on p.Mkt_OrderColorAndSizeRatioFk equals r.ID
                         where q.Mkt_BOMFk == BomId && p.Plan_ReferenceSectionFk == SectionId && p.Active == true
                         && r.Color.Equals(ColorName) && r.Size.Equals(SizeName)
                         select new
                         {
                             qty = p.Quantity
                         }).ToList();
            if (vData.Any())
            {
                return (int)vData.Sum(a => a.qty);
            }
            return 0;
        }

        private int GetSectionWiseColorSizeDayProduction(int BomId, int RefId, int SectionId, string ColorName, string SizeName)
        {
            db = new xOssContext();
            var vData = (from o in db.Prod_PlanReferenceOrder
                         join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                         join q in db.Mkt_OrderDeliverySchedule on p.OrderDeliveryScheduleFk equals q.ID
                         join r in db.Mkt_OrderColorAndSizeRatio on p.Mkt_OrderColorAndSizeRatioFk equals r.ID
                         where o.Prod_ReferenceFK == RefId && q.Mkt_BOMFk == BomId
                         && p.Plan_ReferenceSectionFk == SectionId && p.Active == true
                         && r.Color.Equals(ColorName) && r.Size.Equals(SizeName)
                         select new
                         {
                             qty = p.Quantity
                         }).ToList();
            if (vData.Any())
            {
                return (int)vData.Sum(a => a.qty);
            }
            return 0;
        }

        private string GetSectionWiseColorSizeDayLineProduction(int BomId, int RefId, int SectionId, string ColorName, string SizeName)
        {
            db = new xOssContext();
            var vData = (from o in db.Prod_PlanReferenceOrder
                         join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                         join q in db.Mkt_OrderDeliverySchedule on p.OrderDeliveryScheduleFk equals q.ID
                         join r in db.Mkt_OrderColorAndSizeRatio on p.Mkt_OrderColorAndSizeRatioFk equals r.ID
                         join s in db.Plan_ProductionLine on o.Plan_ProductionLineFk equals s.ID
                         where o.Prod_ReferenceFK == RefId && q.Mkt_BOMFk == BomId
                         && p.Plan_ReferenceSectionFk == SectionId && p.Active == true
                         && r.Color.Equals(ColorName) && r.Size.Equals(SizeName)
                         select new
                         {
                             Line = s.Name
                         }).ToList();
            if (vData.Any())
            {
                return vData.FirstOrDefault().Line;
            }
            return string.Empty;
        }

        private int GetColorSizeOrderQty(int BomId, string ColorName, string SizeName)
        {
            db = new xOssContext();
            var Vdata = (from o in db.Mkt_OrderDeliverySchedule
                         join p in db.Mkt_OrderColorAndSizeRatio on o.ID equals p.OrderDeliveryScheduleFk
                         where o.Mkt_BOMFk == BomId
                         && p.Color.Equals(ColorName) && p.Size.Equals(SizeName)
                         && o.Active == true && p.Active == true
                         select new
                         {
                             Qty = p.Quantity
                         }).ToList();

            if (Vdata.Any())
            {
                return (int)Vdata.Sum(a => a.Qty);
            }

            return 0;
        }

        private VmProductionReport GetOrderSizeDoneQty(int RefId, int BomId, string Color, string Size, int SectionId)
        {
            db = new xOssContext();
            VmProductionReport model = new VmProductionReport();
            var vdata = (from t3 in db.Prod_PlanReferenceOrder
                         join t0 in db.Prod_PlanReferenceOrderSection on t3.ID equals t0.Prod_PlanReferenceOrderFk
                         join t1 in db.Mkt_OrderDeliverySchedule on t0.OrderDeliveryScheduleFk equals t1.ID
                         join t2 in db.Mkt_OrderColorAndSizeRatio on t0.Mkt_OrderColorAndSizeRatioFk equals t2.ID
                         where t0.Plan_ReferenceSectionFk == SectionId && t1.Mkt_BOMFk == BomId && t2.Color.Equals(Color) && t2.Size.Equals(Size)
                         && t2.Active==true && t1.Active==true
                         group t0.Quantity by new { t2.Color,t2.Size } into all
                         select new
                         {
                             qty = all.Sum()
                         });
            if (vdata.Any())
            {
                if (SectionId == 1)
                {
                    model.TotalCutting = vdata.Sum(a => a.qty) == null ? 0 : (int)vdata.Sum(a => a.qty);
                }
                else if (SectionId == 2)
                {
                    model.TotalSewing = vdata.Sum(a => a.qty) == null ? 0 : (int)vdata.Sum(a => a.qty);
                }
                else if (SectionId == 3)
                {
                    model.TotalIroning = vdata.Sum(a => a.qty) == null ? 0 : (int)vdata.Sum(a => a.qty);
                }
                else if (SectionId == 4)
                {
                    model.TotalPacking = vdata.Sum(a => a.qty) == null ? 0 : (int)vdata.Sum(a => a.qty);
                }

            }
            var vdata1 = (from t0 in db.Prod_PlanReferenceOrderSection
                          join t1 in db.Mkt_OrderDeliverySchedule on t0.OrderDeliveryScheduleFk equals t1.ID
                          join t2 in db.Mkt_OrderColorAndSizeRatio on t0.Mkt_OrderColorAndSizeRatioFk equals t2.ID
                          join t3 in db.Prod_PlanReferenceOrder on t0.Prod_PlanReferenceOrderFk equals t3.ID
                          where t1.Mkt_BOMFk == BomId && t3.Prod_ReferenceFK == RefId && t0.Plan_ReferenceSectionFk == SectionId
                          && t2.Color.Equals(Color) && t2.Size.Equals(Size)
                          && t2.Active == true && t1.Active == true
                          group t0.Quantity by new { t3 } into all
                          select new
                          {
                              qty = all.Sum()
                          });

            if (vdata1.Any())
            {
                if (SectionId == 1)
                {
                    model.DayCutting = vdata1.Sum(a => a.qty) == null ? 0 : (int)vdata1.Sum(a => a.qty);
                }
                else if (SectionId == 2)
                {
                    model.DaySewing = vdata1.Sum(a => a.qty) == null ? 0 : (int)vdata1.Sum(a => a.qty);
                }
                else if (SectionId == 3)
                {
                    model.DayIroning = vdata1.Sum(a => a.qty) == null ? 0 : (int)vdata1.Sum(a => a.qty);
                }
                else if (SectionId == 4)
                {
                    model.DayPacking = vdata1.Sum(a => a.qty) == null ? 0 : (int)vdata1.Sum(a => a.qty);
                }

            }
            return model;
        }
        
        private int GetTotalProductionQtyBySectionWise(int BomId, int SectionId)
        {
            db = new xOssContext();
            if (SectionId == 1)
            {
                var vData = (from o in db.Prod_PlanReferenceOrder
                             join q in db.Prod_PlanReferenceOrderSection on o.ID equals q.Prod_PlanReferenceOrderFk
                             join p in db.Mkt_OrderDeliverySchedule on q.OrderDeliveryScheduleFk equals p.ID
                             where p.Mkt_BOMFk == BomId && q.Plan_ReferenceSectionFk == SectionId
                             select new { qty = q.Quantity }).ToList();

                return vData.Any() == true ? (int)vData.Sum(a => a.qty) : 0;
            }
            else
            {
                var vData = (from o in db.Prod_PlanReferenceOrder
                             join q in db.Prod_PlanReferenceOrderSection on o.ID equals q.Prod_PlanReferenceOrderFk
                             join p in db.Prod_PlanOrderSectionSlave on q.ID equals p.Prod_PlanReferenceOrderSectionFk
                             where o.MktBomFk == BomId && q.Plan_ReferenceSectionFk == SectionId
                             select new { qty = p.Quantity }).ToList();
                return vData.Any() == true ? (int)vData.Sum(a => a.qty) : 0;
            }
        }

        private VmProductionReport GetTotalCuttingProduction(int BomId, string ColorName)
        {
            db = new xOssContext();
            VmProductionReport mod = new VmProductionReport();
            
            var VData1 = (from o in db.Prod_PlanReferenceOrder
                          join t in db.Prod_PlanReferenceOrderSection on o.ID equals t.Prod_PlanReferenceOrderFk
                          join p in db.Mkt_OrderDeliverySchedule on t.OrderDeliveryScheduleFk equals p.ID
                          join q in db.Mkt_OrderColorAndSizeRatio on t.Mkt_OrderColorAndSizeRatioFk equals q.ID
                          where p.Active == true && q.Active == true && o.SectionId == 1 && p.Mkt_BOMFk==BomId && q.Color.Equals(ColorName)
                          select new VmProductionReport
                          {
                              LayerWeight = o.PerLayerWeight,
                              MarkerPic = o.MarkerPiece,
                              MarkerLength = o.MarkerLength,
                              MarkerWidth = o.MarkerWidth,
                              Quantity = (int)t.Quantity
                          }).ToList();
            if (VData1.Any())
            {
                foreach (var v in VData1)
                {
                    if (v.LayerWeight > 0 && v.MarkerPic > 0)
                    {
                        v.TotalWeight = Math.Round((v.LayerWeight / v.MarkerPic) * v.Quantity, 2);
                    }
                }

                mod.TotalCutting = VData1.Sum(a => a.Quantity);
                mod.TotalWeight = VData1.Sum(a => a.TotalWeight);
                mod.CuttingConsumption = (mod.TotalWeight / mod.TotalCutting) * 12;
            }
            
            return mod;
        }

        private List<VmProductionReport> GetDayLineProductionBySectionWise(int BOMId, int RefId, int SectionId)
        {
            db = new xOssContext();
            if (SectionId == 1)
            {
                var vData = (from t1 in db.Prod_PlanReferenceOrder
                             join t2 in db.Prod_PlanReferenceOrderSection on t1.ID equals t2.Prod_PlanReferenceOrderFk
                             join t3 in db.Mkt_OrderDeliverySchedule on t2.OrderDeliveryScheduleFk equals t3.ID
                             join t4 in db.Mkt_BOM on t3.Mkt_BOMFk equals t4.ID
                             where t4.ID == BOMId && t1.Prod_ReferenceFK == RefId && t2.Plan_ReferenceSectionFk == SectionId
                             group t2.Quantity by new { t4.ID, t1.Plan_ProductionLineFk, t1.MachineRunning } into all
                             select new VmProductionReport
                             {
                                 LineId = all.Key.Plan_ProductionLineFk,
                                 TotalWorker = all.Key.MachineRunning,
                                 Quantity = (int)all.Sum()
                             }).ToList();

                return vData;
            }
            else
            {
                var vData = (from t1 in db.Prod_PlanReferenceOrder
                             join t2 in db.Prod_PlanReferenceOrderSection on t1.ID equals t2.Prod_PlanReferenceOrderFk
                             join t3 in db.Prod_PlanOrderSectionSlave on t2.ID equals t3.Prod_PlanReferenceOrderSectionFk
                             where t1.MktBomFk == BOMId && t1.Prod_ReferenceFK == RefId && t1.SectionId == SectionId
                             group t3.Quantity by new { t1.Plan_ProductionLineFk, t1.MachineRunning } into all
                             select new VmProductionReport
                             {
                                 LineId = all.Key.Plan_ProductionLineFk,
                                 TotalWorker = all.Key.MachineRunning,
                                 Quantity = (int)all.Sum()
                             }).ToList();

                return vData;
            }
        }

        private List<VmProductionReport> GetListOrderWisePlanQty(int BOMId, int SectionId)
        {
            db = new xOssContext();
            List<VmProductionReport> list = new List<VmProductionReport>();
            if (SectionId == 1)
            {
                var vData = (from t1 in db.Prod_PlanReferenceOrder
                             join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                             join t3 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t3.ID
                             where t1.CuttingPlanQuantity != 0 && t2.Mkt_BOMFk == BOMId
                             group t1.CuttingPlanQuantity by new { t2.Mkt_BOMFk, t3.Color, t3.Size } into all
                             select new VmProductionReport
                             {
                                 BOMID = (int)all.Key.Mkt_BOMFk,
                                 ColorName = all.Key.Color,
                                 Size = all.Key.Size,
                                 Quantity = all.Sum()
                             }).OrderBy(a => a.ColorName).ThenBy(a => a.Size).ToList();
                list = vData;
            }
            else if (SectionId == 2)
            {
                var vData = (from t1 in db.Prod_PlanReferenceOrder
                             join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                             join t3 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t3.ID
                             where t1.SewingPlanQuantity != 0 && t2.Mkt_BOMFk == BOMId
                             group t1.SewingPlanQuantity by new { t2.Mkt_BOMFk, t3.Color, t3.Size } into all
                             select new VmProductionReport
                             {
                                 BOMID = (int)all.Key.Mkt_BOMFk,
                                 ColorName = all.Key.Color,
                                 Size = all.Key.Size,
                                 Quantity = all.Sum()
                             }).OrderBy(a => a.ColorName).ThenBy(a => a.Size).ToList();
                list = vData;
            }
            else if (SectionId == 3)
            {
                var vData = (from t1 in db.Prod_PlanReferenceOrder
                             join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                             join t3 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t3.ID
                             where t1.IronPlanQuantity != 0 && t2.Mkt_BOMFk == BOMId
                             group t1.IronPlanQuantity by new { t2.Mkt_BOMFk, t3.Color, t3.Size } into all
                             select new VmProductionReport
                             {
                                 BOMID = (int)all.Key.Mkt_BOMFk,
                                 ColorName = all.Key.Color,
                                 Size = all.Key.Size,
                                 Quantity = all.Sum()
                             }).OrderBy(a => a.ColorName).ThenBy(a => a.Size).ToList();
                list = vData;
            }
            else if (SectionId == 4)
            {
                var vData = (from t1 in db.Prod_PlanReferenceOrder
                             join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                             join t3 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t3.ID
                             where t1.PackingPlanQuantity != 0 && t2.Mkt_BOMFk == BOMId
                             group t1.PackingPlanQuantity by new { t2.Mkt_BOMFk, t3.Color, t3.Size } into all
                             select new VmProductionReport
                             {
                                 BOMID = (int)all.Key.Mkt_BOMFk,
                                 ColorName = all.Key.Color,
                                 Size = all.Key.Size,
                                 Quantity = all.Sum()
                             }).OrderBy(a => a.ColorName).ThenBy(a => a.Size).ToList();
                list = vData;
            }
            return list;
        }

        private string ShortDateString(DateTime Date)
        {
            string datestring = string.Empty;

            string day = Date.Day < 10 ? "0" + Date.Day : Date.Day.ToString();
            string month = Date.Month < 10 ? "0" + Date.Month : Date.Month.ToString();
            int year = Date.Year;

            datestring = day + "-" + month + "-" + year;

            return datestring;
        }

        public DataTable GetHourlyLineProduction(int RefId)
        {
            DataTable ds = new DataTable();
            db = new xOssContext();
            var vLine = db.Plan_ProductionLine.Where(d => d.Plan_OrderProcessFK == 32).OrderBy(a => a.ID).ToList();
            ds.Columns.Add("Hour");
            foreach (var v in vLine)
            {
                ds.Columns.Add(v.Name);
            }
            ds.Columns.Add("Total");

            //Fined Line Time
            this.FromDate = db.Prod_Reference.Where(a => a.ID == RefId).FirstOrDefault().FromDate;
            this.ProductionDate = this.FromDate.ToLongDateString();
            var allData = (from t1 in db.Prod_PlanReferenceOrder
                           join t4 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t4.ID
                           join t5 in db.Mkt_BOM on t1.MktBomFk equals t5.ID
                           join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                           join t7 in db.Mkt_Buyer on t6.Mkt_BuyerFK equals t7.ID
                           where t1.Prod_ReferenceFK == RefId && t1.SectionId == 2
                           group t1.SewingPlanQuantity
                           by new { t1, t5.Style, t6.BuyerPO, t7.Name } into all
                           select new
                           {
                               LineId = all.Key.t1.Plan_ProductionLineFk,
                               OrderNo = all.Key.BuyerPO,
                               Style = all.Key.Name + "-" + all.Key.Style,
                               WorkHour = all.Key.t1.WorkingHour,
                               SMV = all.Key.t1.SMV,
                               TotalTarget = all.Key.t1.SewingPlanQuantity
                           }).ToList();
            if (allData.Any())
            {


                var LineOrder = allData.GroupBy(a => a.LineId).Select(x => new { t = x.ToList() });

                var allDoneData = (from t1 in db.Prod_PlanReferenceOrder
                                   join t2 in db.Prod_PlanReferenceOrderSection on t1.ID equals t2.Prod_PlanReferenceOrderFk
                                   join t3 in db.Prod_PlanOrderSectionSlave on t2.ID equals t3.Prod_PlanReferenceOrderSectionFk
                                   where t1.Prod_ReferenceFK == RefId && t1.SectionId == 2 && t2.Plan_ReferenceSectionFk == 2
                                   select new
                                   {
                                       LineId = t1.Plan_ProductionLineFk,
                                       EntryDate = t3.FirstCreated,
                                       TotalDoneQty = t3.Quantity
                                   }).ToList();

                var TotalHour = allData.GroupBy(a => a.LineId).Select(x => new { t = x.Sum(p => p.WorkHour) }).Max(a => a.t);
                List<VmProductionReport> lstList = new List<VmProductionReport>();
                if (LineOrder.Any())
                {
                    foreach (var v in LineOrder)
                    {
                        if (v.t.Any())
                        {
                            foreach (var v1 in v.t)
                            {
                                VmProductionReport m = new VmProductionReport();
                                m.LineName = vLine.FirstOrDefault(a => a.ID == v1.LineId).Name;
                                m.OrderNo = v1.OrderNo;
                                m.Style = v1.Style;
                                m.SMV = v1.SMV;
                                m.TotalTarget = v1.TotalTarget;
                                lstList.Add(m);
                            }
                        }
                    }
                }
                this.DataList = lstList;

                if (TotalHour > 0)
                {
                    int count = 0;
                    for (int i = 1; i <= TotalHour + 2; i++)
                    {
                        int TotalOutput = 0;
                        int LineOutput = 0;
                        DateTime fDate = this.FromDate + new TimeSpan(8 + i, 00, 00);
                        DateTime tDate = this.FromDate + new TimeSpan(8 + i, 59, 00);
                        ds.Rows.Add(fDate.TimeOfDay);
                        int c = 0;
                        foreach (var v1 in vLine)
                        {
                            c++;
                            LineOutput = 0;
                            var getProduction = allDoneData.Where(a => a.EntryDate.Value.Hour <= fDate.Hour && a.EntryDate.Value.Hour >= tDate.Hour && a.LineId == v1.ID);
                            LineOutput = getProduction.Any() == true ? (int)getProduction.Sum(a => a.TotalDoneQty) : 0;
                            TotalOutput += LineOutput;
                            ds.Rows[count][c] = LineOutput;
                        }
                        ++c;
                        ds.Rows[count][c] = TotalOutput;
                        ++count;
                    }
                }
            }
            else
            {
                this.DataList = new List<VmProductionReport>();
            }
            return ds;
        }

        public DataTable GetMonthlyBuyerProduction(VmProductionReport model)
        {
            db = new xOssContext();
            DataTable ds = new DataTable();
            ds.Columns.Add("Date/Buyer");

            var vDetails = (from t1 in db.Prod_Reference
                            join t2 in db.Prod_PlanReferenceOrder on t1.ID equals t2.Prod_ReferenceFK
                            join t3 in db.Mkt_BOM on t2.MktBomFk equals t3.ID
                            join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                            join t5 in db.Mkt_Buyer on t4.Mkt_BuyerFK equals t5.ID
                            join t6 in db.Prod_PlanReferenceOrderSection on t2.ID equals t6.Prod_PlanReferenceOrderFk
                            join t7 in db.Prod_PlanOrderSectionSlave on t6.ID equals t7.Prod_PlanReferenceOrderSectionFk
                            where t1.FromDate >= model.FromDate && t1.FromDate <= model.ToDate && t2.SectionId == 2 && t3.Active == true && t4.Active == true && t5.Active == true
                            group t7.Quantity by new { t1.FromDate, t2.MktBomFk, t4.CID } into all
                            select new
                            {
                                refDate = all.Key.FromDate,
                                bomId = all.Key.MktBomFk,
                                buyerPo = all.Key.CID,
                                qty = all.Sum()
                            }).ToList().OrderBy(a => a.bomId);

            var LineOrder = (from o in vDetails
                             group o.qty by new { o.bomId, o.buyerPo } into g
                             select new { buyerPo = g.Key.buyerPo, bomId = g.Key.bomId }).ToList().OrderBy(a => a.buyerPo);

            int totalBuyer = LineOrder.Count();
            if (totalBuyer > 0)
            {
                foreach (var v in LineOrder)
                {
                    ds.Columns.Add(v.buyerPo);
                }
            }
            ds.Columns.Add("Total");

            DateTime fDate = model.FromDate;
            DateTime tDate = model.ToDate;
            int rowCount = 0;
            while (fDate <= tDate)
            {
                string date = ShortDateString(fDate);
                ds.Rows.Add(date);
                int colCount = 1;
                int RowTotal = 0;
                foreach (var v in LineOrder)
                {
                    var getqty = vDetails.Where(a => a.bomId == v.bomId && a.refDate == fDate);
                    RowTotal += getqty.Any() == true ? (int)getqty.Sum(a => a.qty) : 0;
                    ds.Rows[rowCount][colCount] = getqty.Any() == true ? getqty.Sum(a => a.qty) : 0;
                    ++colCount;
                }
                ds.Rows[rowCount][colCount] = RowTotal;
                ++rowCount;
                fDate = fDate.AddDays(1);
            }
            return ds;
        }

        public void GetMonthlyOrderProduction(VmProductionReport model)
        {
            db = new xOssContext();
            List<VmProductionReport> lst = new List<VmProductionReport>();

            var vPreData = (from t1 in db.Prod_Reference
                            join t2 in db.Prod_PlanReferenceOrder on t1.ID equals t2.Prod_ReferenceFK
                            join t3 in db.Mkt_BOM on t2.MktBomFk equals t3.ID
                            join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                            join t5 in db.Mkt_Buyer on t4.Mkt_BuyerFK equals t5.ID
                            join t6 in db.Prod_PlanReferenceOrderSection on t2.ID equals t6.Prod_PlanReferenceOrderFk
                            join t7 in db.Prod_PlanOrderSectionSlave on t6.ID equals t7.Prod_PlanReferenceOrderSectionFk
                            where t1.FromDate < model.FromDate && t2.SectionId == 2 && t3.Active == true && t4.Active == true && t5.Active == true
                            group t7.Quantity by new { t2.MktBomFk, t4.BuyerPO, t3.Style, t5.Name, t3.Quantity, t3.QPack } into all
                            select new VmProductionReport
                            {
                                BOMID = all.Key.MktBomFk,
                                Quantity = (int)all.Sum()
                            }).ToList().OrderBy(a => a.BOMID);

            var vData = (from t1 in db.Prod_Reference
                         join t2 in db.Prod_PlanReferenceOrder on t1.ID equals t2.Prod_ReferenceFK
                         join t3 in db.Mkt_BOM on t2.MktBomFk equals t3.ID
                         join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                         join t5 in db.Mkt_Buyer on t4.Mkt_BuyerFK equals t5.ID
                         join t6 in db.Prod_PlanReferenceOrderSection on t2.ID equals t6.Prod_PlanReferenceOrderFk
                         join t7 in db.Prod_PlanOrderSectionSlave on t6.ID equals t7.Prod_PlanReferenceOrderSectionFk
                         where t1.FromDate >= model.FromDate && t1.FromDate <= model.ToDate && t2.SectionId == 2 && t3.Active == true && t4.Active == true && t5.Active == true
                         group t7.Quantity by new { t2.MktBomFk, t4.BuyerPO, t3.Style, t5.Name, t3.Quantity, t3.QPack } into all
                         select new VmProductionReport
                         {
                             BOMID = all.Key.MktBomFk,
                             OrderNo = all.Key.BuyerPO,
                             Style = all.Key.Style,
                             BuyerName = all.Key.Name,
                             PackPice = all.Key.Quantity * all.Key.QPack,
                             Quantity = (int)all.Sum()
                         }).ToList().OrderBy(a => a.BOMID);

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.TargetBalance = vPreData.Any(a => a.BOMID == v.BOMID) == true ? vPreData.Where(a => a.BOMID == v.BOMID).Sum(a => a.Quantity): 0;
                    v.TotalSewing = v.Quantity + v.TargetBalance;
                    v.SewingBalance = v.TotalSewing - v.PackPice;
                }
            }

            this.DataList = vData.ToList();
        }

        public void GetMonthlyLineProduction(VmProductionReport model)
        {
            db = new xOssContext();
            List<VmProductionReport> lst = new List<VmProductionReport>();

            //var vPreData = (from t1 in db.Prod_Reference
            //                join t2 in db.Prod_PlanReferenceOrder on t1.ID equals t2.Prod_ReferenceFK
            //                join t3 in db.Mkt_BOM on t2.MktBomFk equals t3.ID
            //                join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
            //                join t5 in db.Mkt_Buyer on t4.Mkt_BuyerFK equals t5.ID
            //                join t6 in db.Prod_PlanReferenceOrderSection on t2.ID equals t6.Prod_PlanReferenceOrderFk
            //                join t7 in db.Prod_PlanOrderSectionSlave on t6.ID equals t7.Prod_PlanReferenceOrderSectionFk
            //                where t1.FromDate < model.FromDate && t2.SectionId == 2 && t3.Active == true && t4.Active == true && t5.Active == true
            //                group t7.Quantity by new { t2.MktBomFk, t4.BuyerPO, t3.Style, t5.Name, t3.Quantity, t3.QPack } into all
            //                select new VmProductionReport
            //                {
            //                    BOMID = all.Key.MktBomFk,
            //                    Quantity = (int)all.Sum()
            //                }).ToList().OrderBy(a => a.BOMID);

            var vData = (from t1 in db.Prod_Reference
                         join t2 in db.Prod_PlanReferenceOrder on t1.ID equals t2.Prod_ReferenceFK
                         join t3 in db.Mkt_BOM on t2.MktBomFk equals t3.ID
                         join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                         join t5 in db.Mkt_Buyer on t4.Mkt_BuyerFK equals t5.ID
                         join t6 in db.Prod_PlanReferenceOrderSection on t2.ID equals t6.Prod_PlanReferenceOrderFk
                         join t7 in db.Prod_PlanOrderSectionSlave on t6.ID equals t7.Prod_PlanReferenceOrderSectionFk
                         join t8 in db.Plan_ProductionLine on t2.Plan_ProductionLineFk equals t8.ID
                         where t1.FromDate >= model.FromDate && t1.FromDate <= model.ToDate && t2.SectionId == 2 && t3.Active == true && t4.Active == true && t5.Active == true
                         group t7.Quantity by new {t1.FromDate, t2.MktBomFk, t4.BuyerPO, t3.Style, t5.Name, t3.Quantity, t3.QPack, t8 } into all
                         select new VmProductionReport
                         {
                             FromDate=all.Key.FromDate,
                             BOMID = all.Key.MktBomFk,
                             LineName = all.Key.t8.Name,
                             OrderNo = all.Key.BuyerPO,
                             Style = all.Key.Style,
                             BuyerName = all.Key.Name,
                             PackPice = all.Key.Quantity * all.Key.QPack,
                             Quantity = (int)all.Sum()
                         }).ToList().OrderBy(a => a.BOMID);

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    //v.TargetBalance = vPreData.Any(a => a.BOMID == v.BOMID) == true ? vPreData.Where(a => a.BOMID == v.BOMID).Sum(a => a.Quantity) : 0;
                    //v.TotalSewing = v.Quantity + v.TargetBalance;
                    //v.SewingBalance = v.PackPice - v.TotalSewing;
                    v.DateView = ShortDateString(v.FromDate);
                }
            }
            
            this.DataList = vData.ToList();
        }

        private int GetLineWiseColorTotal(int LineId, string Color, DateTime date)
        {
            int total = 0;
            db = new xOssContext();

            var vData = (from t0 in db.Prod_Reference
                         join t1 in db.Prod_PlanReferenceOrder on t0.ID equals t1.Prod_ReferenceFK
                         join t2 in db.Prod_PlanReferenceOrderSection on t1.ID equals t2.Prod_PlanReferenceOrderFk
                         join t3 in db.Mkt_OrderColorAndSizeRatio on t2.Mkt_OrderColorAndSizeRatioFk equals t3.ID
                         where t0.FromDate <= date && t1.SectionId == 2 && t1.Plan_ProductionLineFk == LineId && t3.Color.Equals(Color)
                         group t2.Quantity by new { t3.Color } into a
                         select new { Color = a.Key.Color, total = a.Sum() }).ToList();
            if (vData.Any())
            {
                return total = (int)vData.Sum(a => a.total);
            }
            else { return total; }
        }

        public void GetLineProduction(VmProductionReport model)
        {
            db = new xOssContext();
            List<VmProductionReport> lst = new List<VmProductionReport>();
            
            var allData = (from t0 in db.Prod_Reference
                           join t1 in db.Prod_PlanReferenceOrder on t0.ID equals t1.Prod_ReferenceFK
                           join t4 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t4.ID
                           join t5 in db.Mkt_BOM on t1.MktBomFk equals t5.ID
                           join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                           join t7 in db.Mkt_Buyer on t6.Mkt_BuyerFK equals t7.ID
                           where t0.FromDate >= model.FromDate && t0.FromDate <= model.ToDate && t1.SectionId == 2
                           group t1.Quantity
                           by new { t0, t1, t4, t5.Style, t6.BuyerPO, t7.Name } into all
                           select new
                           {
                               FromDate = all.Key.t0.FromDate,
                               LineId= all.Key.t4.ID,
                               LineName = all.Key.t4.Name,
                               OrderNo = all.Key.BuyerPO,
                               Style = all.Key.Name + "-" + all.Key.Style,
                               LineHour = all.Key.t1.WorkingHour,
                               SMV = all.Key.t1.SMV,
                               TotalTarget = all.Key.t1.Quantity * all.Key.t1.WorkingHour,
                               TotalAchivement = (from a in db.Prod_PlanReferenceOrderSection
                                                  join b in db.Prod_PlanOrderSectionSlave on a.ID equals b.Prod_PlanReferenceOrderSectionFk
                                                  join c in db.Mkt_OrderColorAndSizeRatio on a.Mkt_OrderColorAndSizeRatioFk equals c.ID
                                                  where a.Prod_PlanReferenceOrderFk == all.Key.t1.ID
                                                  group b.Quantity by new { c.Color } into a
                                                  select new
                                                  {
                                                      Color = a.Key.Color,
                                                      total = a.Sum()
                                                  }).ToList()
                           }).ToList();
            
            if (allData.Any())
            {
                foreach (var v in allData)
                {
                    if (v.TotalAchivement.Any())
                    {
                        foreach (var v1 in v.TotalAchivement)
                        {
                            VmProductionReport m1 = new VmProductionReport();
                            m1.LineName = v.LineName;
                            m1.OrderNo = v.OrderNo;
                            m1.Style = v.Style;
                            m1.ColorName = v1.Color;
                            m1.LineHour = v.LineHour;
                            m1.SMV = v.SMV;
                            m1.TotalTarget = v.TotalTarget;
                            m1.TotalAchivement = (int)v1.total;
                            m1.TotalSewing = GetLineWiseColorTotal(v.LineId,v1.Color,v.FromDate);
                            m1.DateView = ShortDateString(v.FromDate);
                            lst.Add(m1);
                        }
                    }
                    else
                    {
                        VmProductionReport m = new VmProductionReport();
                        m.LineName = v.LineName;
                        m.OrderNo = v.OrderNo;
                        m.Style = v.Style;
                        m.ColorName = string.Empty;
                        m.LineHour = v.LineHour;
                        m.SMV = v.SMV;
                        m.TotalTarget = v.TotalTarget;
                        m.TotalAchivement = 0;
                        m.DateView = ShortDateString(v.FromDate);
                        lst.Add(m);
                    }
                    
                }
            }
            this.DataList = lst;
        }

        public List<VmProductionReport> GetDailyAchievment(int RefId)
        {
            var allData = (from t1 in db.Prod_PlanReferenceOrder
                           join t2 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t2.ID
                           join t3 in db.Mkt_BOM on t1.MktBomFk equals t3.ID
                           join t7 in db.Common_TheOrder on t3.Common_TheOrderFk equals t7.ID
                           join t9 in db.Mkt_Buyer on t7.Mkt_BuyerFK equals t9.ID
                           where t1.Prod_ReferenceFK == RefId && t1.SectionId == 2
                           select new
                           {
                               SMVInfo = db.Prod_SMVLayout.Where(a => a.Mkt_BOMFk == t1.MktBomFk && a.TotalSMV == t1.SMV).ToList(),
                               BOMId = t1.MktBomFk,
                               LineId = t1.Plan_ProductionLineFk,
                               OrderNo = t7.BuyerPO,
                               Style = t9.Name + "-" + t3.Style,
                               MachineRunning = t1.MachineRunning,
                               NoOfOptPresent = t1.PresentOperator,
                               NoOfHelperPresent = t1.PresentHelper,
                               WorkHour = t1.WorkingHour,
                               StyleSMV = t1.SMV,
                               PerHourTarget = t1.Quantity,
                               TotalTarget = t1.SewingPlanQuantity,
                               TotalAchivement = (from a in db.Prod_PlanReferenceOrderSection
                                                  join b in db.Prod_PlanOrderSectionSlave on a.ID equals b.Prod_PlanReferenceOrderSectionFk
                                                  join c in db.Mkt_OrderDeliverySchedule on a.OrderDeliveryScheduleFk equals c.ID
                                                  where a.Prod_PlanReferenceOrderFk == t1.ID
                                                  group b.Quantity by new { a.Prod_PlanReferenceOrderFk } into all
                                                  select new
                                                  {
                                                      total = all.Sum()
                                                  }).FirstOrDefault()
                           }).ToList();

            List<VmProductionReport> lstLine = new List<VmProductionReport>();
            int LineMachineQty = 0;
            var getAllLines = db.Plan_ProductionLine.Where(a => a.LineNumber != null).OrderBy(a => a.ID).ToList();
            foreach (var line in getAllLines)
            {
                if (allData.Any(a => a.LineId == line.ID))
                {
                    var GetLineInfo = allData.Where(a => a.LineId == line.ID).ToList();

                    LineMachineQty += GetLineInfo.FirstOrDefault().MachineRunning;
                    foreach (var item in GetLineInfo)
                    {
                        var getRemarks = db.Prod_PlanReferenceRemarks.Where(a => a.Prod_ReferenceFK == RefId && a.MktBomFk == item.BOMId && a.Plan_ProductionLineFk == item.LineId && a.Active == true).ToList();

                        VmProductionReport rpt = new VmProductionReport();
                        rpt.ProdReferenceId = RefId;
                        rpt.BOMID = item.BOMId;
                        rpt.LineId = line.ID;
                        rpt.LineName = line.Name;
                        rpt.LineMachineRequired = item.SMVInfo.Any() == true ? item.SMVInfo.FirstOrDefault().NoOfOperator : 0;
                        rpt.OrderNo = item.OrderNo;
                        rpt.Item = item.Style;
                        rpt.LineMachineRunning = item.MachineRunning;
                        rpt.NoOfOPPresent = item.NoOfOptPresent;
                        rpt.NoOfHP = item.NoOfHelperPresent;
                        rpt.TotalWorker = rpt.NoOfOPPresent + rpt.NoOfHP;
                        rpt.SMV = item.StyleSMV;
                        rpt.PerHourTarget = item.PerHourTarget;
                        rpt.TotalTarget = item.TotalTarget;
                        rpt.LineHour = item.WorkHour;
                        rpt.TotalAchivement = item.TotalAchivement == null ? 0 : (int)item.TotalAchivement.total;
                        rpt.NoOfInputMan = 1;
                        rpt.SizeMan = 1;
                        rpt.SuperVisor = 1;
                        rpt.PerHourAchivement = rpt.TotalAchivement / rpt.LineHour;
                        rpt.TargetEffi = Math.Round(((rpt.SMV * rpt.TotalTarget) / (rpt.LineHour * 60 * rpt.TotalWorker)) * 100, 2);
                        rpt.AchiveEffi = Math.Round(((rpt.SMV * rpt.TotalAchivement) / (rpt.LineHour * 60 * rpt.TotalWorker)) * 100, 2);
                        rpt.TargetBalance = rpt.TotalAchivement - rpt.TotalTarget;
                        rpt.Remarks = getRemarks.Any() == true ? getRemarks.FirstOrDefault().Remarks : string.Empty;
                        rpt.SectionId = LineMachineQty;
                        lstLine.Add(rpt);
                    }
                }
            }
            return lstLine;
        }

        public void GetMonthlyAcheivment(VmProductionReport model)
        {
            db = new xOssContext();
            DateTime fDate = model.FromDate;
            DateTime tDate = model.ToDate;
            var vData = db.Prod_Reference.Where(a => a.FromDate >= fDate && a.FromDate <= tDate).ToList();
            List<VmProductionReport> lst = new List<VmProductionReport>();
            while (fDate <= tDate)
            {
                if (vData.Any(a => a.FromDate == fDate))
                {
                    var getData = vData.FirstOrDefault(a => a.FromDate == fDate);

                    var vA=GetDailyAchievment(getData.ID);

                    if (vA.Any())
                    {
                        foreach (var v in vA)
                        {
                            v.DateView = ShortDateString(getData.FromDate);
                            lst.Add(v);
                        }
                    }
                }
                
                fDate = fDate.AddDays(1);
            }

            this.DataList = lst;
        }

        private decimal GetTotalFabricReceivedQty(int BomId,string ColorName)
        {
            db = new xOssContext();

            var vData = (from o in db.Mkt_PO
                         join p in db.Mkt_POSlave on o.ID equals p.Mkt_POFK
                         join q in db.Mkt_POSlave_Receiving on p.ID equals q.Mkt_POSlaveFK
                         where o.Active == true && o.Mkt_BOMFK == BomId && q.IsFinishFabric == true && q.IsReturn == false && q.FabricColor.Equals(ColorName)
                         select new {
                             receivedQty=q.Quantity,
                             lossQty=q.StockLoss,
                             consumption=p.Consumption
                         }).ToList();
            if (vData.Any())
            {
                this.BookingConsump = (decimal)vData.FirstOrDefault().consumption;
                return (int)vData.Sum(a => a.receivedQty - a.lossQty);
            }
            
            //var vCon= (from o in db.Mkt_PO
            //           join p in db.Mkt_POSlave on o.ID equals p.Mkt_POFK
            //           join q in db.Mkt_POSlave_Receiving on p.ID equals q.Mkt_POSlaveFK
            //           where o.Active == true && o.Mkt_BOMFK == BomId && q.IsFinishFabric == true && q.IsReturn == false && q.FabricColor.Equals(ColorName)
            //           select q.Quantity - q.StockLoss).DefaultIfEmpty(0).Sum();

            return 0;
        }

        private decimal GetTotalRequisition(int BomId, string ColorName)
        {
            db = new xOssContext();
            var vData = (from t1 in db.Prod_Requisition
                         join t2 in db.Prod_Requisition_Slave on t1.ID equals t2.Prod_RequisitionFK
                         join t3 in db.Raw_InternaleTransferSlave on t2.ID equals t3.RequisitionSlaveFK
                         join t5 in db.Mkt_POSlave_Consumption on t3.ID equals t5.Raw_InternaleTransferSlaveFk
                         where t1.ToUser_DeptFK == 2 && t1.FromUser_DeptFK == 9 
                         && t2.Active == true && t3.Active == true && t5.IsReturn == false 
                         && t2.Description.Equals(ColorName) && t2.Mkt_BOMFK==BomId
                         select t5.Quantity).DefaultIfEmpty(0).Sum();

            return vData.Value;
        }
        
        public void GetOrderCloseReport(int BomId)
        {
            db = new xOssContext();
            var vOrder = (from t1 in db.Mkt_BOM
                          join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                          join t3 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t3.ID
                          join t4 in db.Mkt_OrderDeliverySchedule on t1.ID equals t4.Mkt_BOMFk
                          join t5 in db.Mkt_OrderColorAndSizeRatio on t4.ID equals t5.OrderDeliveryScheduleFk
                          where t1.ID == BomId && t2.Active == true && t1.Active == true && t4.Active == true && t5.Active == true
                          group t5.Quantity by new {t1.ID, t2.CID, t1.Style, t3.Name, t2.BuyerPO, t5.Color } into a
                          select new VmProductionReport
                          {
                              BOMID=a.Key.ID,
                              OrderNo = a.Key.CID,
                              Style = a.Key.Style,
                              BuyerName = a.Key.Name,
                              BuyerPO = a.Key.BuyerPO,
                              ColorName=a.Key.Color,
                              Quantity = (int)a.Sum(),
                              Extra=2+" %"
                          }).ToList();

            if (vOrder.Any())
            {
                decimal extraCutRate = (decimal)0.02;
                foreach (var v in vOrder)
                {
                    var vData = GetTotalCuttingProduction(v.BOMID,v.ColorName);
                    v.ExtraQty = (int)(v.Quantity * extraCutRate);
                    v.TotalTarget = v.Quantity + v.ExtraQty;
                    v.TotalStoreReceived = GetTotalFabricReceivedQty(v.BOMID,v.ColorName);
                    v.BookingConsump = this.BookingConsump;
                    v.TotalCuttingReceived = GetTotalRequisition(v.BOMID, v.ColorName);
                    v.StoreBalance = v.TotalStoreReceived - v.TotalCuttingReceived;
                    v.TotalCutting = vData.TotalCutting;
                    v.TotalFabricUsed = vData.TotalWeight;
                    v.CuttingConsumption = vData.CuttingConsumption;
                    v.FabricBalance = v.TotalCuttingReceived - v.TotalFabricUsed;
                    v.TotalBalance = v.StoreBalance + v.FabricBalance;
                }
            }
            this.DataList = vOrder;
        }
    }
}