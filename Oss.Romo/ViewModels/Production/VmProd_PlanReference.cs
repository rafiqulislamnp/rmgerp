﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Procurement;


namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_PlanReference
    {
        private xOssContext db;
        public IEnumerable<VmProd_PlanReference> DataList { get; set; }
        public IEnumerable<VmProd_PlanReference> DataList2 { get; set; }
        public Prod_PlanReference Prod_PlanReference { get; set; }
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public int? ActualQty { get; set; }
        public int? DoneQuantity { get; set; }
        public int? ExFactoryQty { get; set; }
        public int? ShiftQty { get; set; }
        public int? TotalPlanQty { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public Prod_PlanReferenceProductionSection Prod_PlanReferenceProductionSection { get; set; }
        public Prod_PlanReferenceProduction Prod_PlanReferenceProduction { get; set; }
        public Raw_InternaleTransferSlave Raw_InternaleTransferSlave { get; set; }
        public Raw_InternaleTransfer Raw_InternaleTransfer { get; set; }
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public Plan_ProductionLine Plan_ProductionLine { get; set; }
        public Prod_Requisition Prod_Requisition { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public int OrderProcessID { get; set; }
        public Prod_TransitionItemInventory Prod_TransitionItemInventory { get; set; }
        public Mkt_OrderColorAndSizeRatio Mkt_OrderColorAndSizeRatio { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public string OrderName { get; set; }
        public int BOMID { get; set; }
        public int OrderID { get; set; }
        public VmProd_PlanReference()
        {
         VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReference
                     join t2 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryScheduleFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Mkt_BOM on t3.ID equals t4.Common_TheOrderFk
                     join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
                     select new VmProd_PlanReference
                     {
                         Prod_PlanReference = t1,
                         Mkt_OrderDeliverySchedule = t2,
                         OrderName = t3.BuyerPO,
                         BOMID = t4.ID,
                         OrderID = t3.ID,
                         Common_TheOrder = t3,
                         Mkt_BOM = t4,
                         Mkt_Item = t5
                     }).Where(x => x.Prod_PlanReference.Active == true && x.Mkt_OrderDeliverySchedule.Active == true).OrderByDescending(x => x.Prod_PlanReference.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingleData(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReference
                     join t2 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryScheduleFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Mkt_BOM on t3.ID equals t4.Common_TheOrderFk
                     select new VmProd_PlanReference
                     {
                         Prod_PlanReference = t1,
                         Mkt_OrderDeliverySchedule = t2,
                         Common_TheOrder = t3,
                         Mkt_BOM = t4
                     }).FirstOrDefault(x => x.Prod_PlanReference.Active == true && x.Prod_PlanReference.ID == id);
            if (v != null)
            {
                this.Prod_PlanReference = v.Prod_PlanReference;
                this.Mkt_OrderDeliverySchedule = v.Mkt_OrderDeliverySchedule;
                this.Common_TheOrder = v.Common_TheOrder;
                this.Mkt_BOM = v.Mkt_BOM;
            }
        }

        public void SelectSingle(string iD)
        {
            db = new xOssContext();
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from rc in db.Prod_PlanReference
                     select new VmProd_PlanReference
                     {
                         Prod_PlanReference = rc
                     }).SingleOrDefault(c => c.Prod_PlanReference.ID == id);
            if (v != null) this.Prod_PlanReference = v.Prod_PlanReference;
        }

        public int Add(int userID)
        {
            Prod_PlanReference.AddReady(userID);
            return Prod_PlanReference.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_PlanReference.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_PlanReference.Delete(userID);
        }



        public void GetProductionFollowUpByStyle(int bomID)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReference
                     join t2 in db.Prod_PlanReferenceProduction on t1.ID equals t2.Prod_PlanReferenceFK
                     join t0 in db.Prod_PlanReferenceProductionSection on t2.ID equals t0.Prod_PlanReferenceProductionFk
                     join t3 in db.Plan_OrderProcess on t2.Plan_OrderProcessFK equals t3.ID
                     join t4 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryScheduleFK equals t4.ID
                     //join t5 in db.Mkt_BOM on t3.Mkt_BOMFK equals t5.ID
                     //join t6 in db.Mkt_OrderColorAndSizeRatio on t5.Common_TheOrderFk equals t6.Common_TheOrderFk
                     where t1.Mkt_BOMFK == bomID 
                     && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t0.Active == true
                     select new VmProd_PlanReference
                     {
                         Prod_PlanReferenceProductionSection = t0,
                         Prod_PlanReferenceProduction = t2,
                         Prod_PlanReference = t1,
                         Mkt_OrderDeliverySchedule = t4,
                         //Mkt_BOM = t5,
                         // Mkt_OrderColorAndSizeRatio = t6,
                         Plan_OrderProcess = t3

                     }).Distinct().OrderBy(x => x.Plan_OrderProcess.ID).AsEnumerable();
            List<VmProd_PlanReference> list = new List<VmProd_PlanReference>();
            foreach (VmProd_PlanReference x in v)
            {
                x.DoneQuantity = TotalDoneQuantityByReferenceProductionSection(x.Prod_PlanReferenceProductionSection.ID);
                list.Add(x);
            }

            int csdf = v.Count();

            this.DataList = list;

        }
        public void GetProductionFollowUpByStyleAndOrderProcess(int bomID, int orderProcess)
        {
            db = new xOssContext();
            List<VmProd_PlanReference> list = new List<VmProd_PlanReference>();
            if (orderProcess!= 0)
            {
                var v = (from t1 in db.Prod_PlanReference
                         join t2 in db.Prod_PlanReferenceProduction on t1.ID equals t2.Prod_PlanReferenceFK
                         join t0 in db.Prod_PlanReferenceProductionSection on t2.ID equals t0.Prod_PlanReferenceProductionFk
                         join t3 in db.Plan_OrderProcess on t2.Plan_OrderProcessFK equals t3.ID
                         join t4 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryScheduleFK equals t4.ID
                         join t5 in db.Mkt_BOM on t1.Mkt_BOMFK equals t5.ID
                         //join t6 in db.Mkt_OrderColorAndSizeRatio on t5.Common_TheOrderFk equals t6.Common_TheOrderFk
                         where t1.Mkt_BOMFK == bomID && t2.Plan_OrderProcessFK == orderProcess
                         && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                         && t0.Active == true
                         select new VmProd_PlanReference
                         {
                             Prod_PlanReferenceProductionSection = t0,
                             Prod_PlanReferenceProduction = t2,
                             Prod_PlanReference = t1,
                             Mkt_OrderDeliverySchedule = t4,
                             Mkt_BOM = t5,
                             // Mkt_OrderColorAndSizeRatio = t6,
                             Plan_OrderProcess = t3

                         }).Distinct().OrderBy(x => x.Plan_OrderProcess.ID).AsEnumerable();
               
                foreach (VmProd_PlanReference x in v)
                {
                    x.DoneQuantity = TotalDoneQuantityByReferenceProductionSection(x.Prod_PlanReferenceProductionSection.ID);
                    list.Add(x);
                }
                
            }
            else
            {
                var v = (from t1 in db.Prod_PlanReference
                         join t2 in db.Prod_PlanReferenceProduction on t1.ID equals t2.Prod_PlanReferenceFK
                         join t0 in db.Prod_PlanReferenceProductionSection on t2.ID equals t0.Prod_PlanReferenceProductionFk
                         join t3 in db.Plan_OrderProcess on t2.Plan_OrderProcessFK equals t3.ID
                         join t4 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryScheduleFK equals t4.ID
                         join t5 in db.Mkt_BOM on t1.Mkt_BOMFK equals t5.ID
                         //join t6 in db.Mkt_OrderColorAndSizeRatio on t5.Common_TheOrderFk equals t6.Common_TheOrderFk
                         where t1.Mkt_BOMFK == bomID
                         && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                         && t0.Active == true
                         select new VmProd_PlanReference
                         {
                             Prod_PlanReferenceProductionSection = t0,
                             Prod_PlanReferenceProduction = t2,
                             Prod_PlanReference = t1,
                             Mkt_OrderDeliverySchedule = t4,
                             Mkt_BOM = t5,
                             // Mkt_OrderColorAndSizeRatio = t6,
                             Plan_OrderProcess = t3

                         }).Distinct().OrderBy(x => x.Plan_OrderProcess.ID).AsEnumerable();
              
                foreach (VmProd_PlanReference x in v)
                {
                    x.DoneQuantity = TotalDoneQuantityByReferenceProductionSection(x.Prod_PlanReferenceProductionSection.ID);
                    list.Add(x);
                }
            }

            this.DataList = list;

        }

        private int? TotalDoneQuantityByReferenceProductionSection(int id)
        {
            db = new xOssContext();
            var quantity = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup 
                            where t1.Prod_PlanReferenceProductionSectionFk == id && t1.Active == true
                            select t1.Quantity).ToList();
            int? TotalQuantity = 0;
            if (quantity != null)
            {
                TotalQuantity = quantity.Sum();

            }

            return TotalQuantity;

        }


        public void GetProductionSendRequisitionByStyle(int bomID)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_InternaleTransferSlave
                     join t2 in db.Raw_InternaleTransfer on t1.Raw_InternaleTransferFK equals t2.ID
                     join t3 in db.Prod_Requisition_Slave on t1.RequisitionSlaveFK equals t3.ID
                     join t4 in db.Prod_Requisition on t3.Prod_RequisitionFK equals t4.ID
                     join t5 in db.Raw_Item on t1.Raw_ItemFK equals t5.ID
                     join t6 in db.Common_Unit on t1.Common_UnitFK equals t6.ID
                     where t1.Mkt_BOMFK == bomID && t1.Raw_ItemFK != 1
                     select new VmProd_PlanReference
                     {
                         Raw_InternaleTransferSlave = t1,
                         Raw_InternaleTransfer = t2,
                         Prod_Requisition_Slave = t3,
                         Prod_Requisition = t4,
                         Raw_Item = t5,
                         Common_Unit = t6,

                     }).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();

            var v1 = (from t1 in db.Raw_InternaleTransferSlave
                      join t2 in db.Raw_InternaleTransfer on t1.Raw_InternaleTransferFK equals t2.ID
                      join t3 in db.Prod_Requisition_Slave on t1.RequisitionSlaveFK equals t3.ID
                      join t4 in db.Prod_Requisition on t3.Prod_RequisitionFK equals t4.ID
                      join t5 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFK equals t5.ID
                      join t6 in db.Common_Unit on t1.Common_UnitFK equals t6.ID
                      where t1.Mkt_BOMFK == bomID && t1.Prod_TransitionItemInventoryFK != 1

                      select new VmProd_PlanReference
                      {
                          Raw_InternaleTransferSlave = t1,
                          Raw_InternaleTransfer = t2,
                          Prod_Requisition_Slave = t3,
                          Prod_Requisition = t4,
                          Prod_TransitionItemInventory = t5,
                          Common_Unit = t6,

                      }).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();

            var list = v.Union(v1);

            this.DataList2 = list;

        }

        public void GetSendRequisitionByStyle(int bomID)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_InternaleTransferSlave
                     join t2 in db.Raw_InternaleTransfer on t1.Raw_InternaleTransferFK equals t2.ID
                     join t3 in db.Prod_Requisition_Slave on t1.RequisitionSlaveFK equals t3.ID
                     join t4 in db.Prod_Requisition on t3.Prod_RequisitionFK equals t4.ID
                     join t5 in db.Raw_Item on t1.Raw_ItemFK equals t5.ID
                     join t6 in db.Common_Unit on t1.Common_UnitFK equals t6.ID
                     join t7 in db.Mkt_BOM on t1.Mkt_BOMFK equals t7.ID
                     where t1.Mkt_BOMFK == bomID && t1.Raw_ItemFK != 1
                     select new VmProd_PlanReference
                     {
                         Raw_InternaleTransferSlave = t1,
                         Raw_InternaleTransfer = t2,
                         Prod_Requisition_Slave = t3,
                         Prod_Requisition = t4,
                         Raw_Item = t5,
                         Common_Unit = t6,
                         Mkt_BOM = t7
                     }).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();

            var v1 = (from t1 in db.Raw_InternaleTransferSlave
                      join t2 in db.Raw_InternaleTransfer on t1.Raw_InternaleTransferFK equals t2.ID
                      join t3 in db.Prod_Requisition_Slave on t1.RequisitionSlaveFK equals t3.ID
                      join t4 in db.Prod_Requisition on t3.Prod_RequisitionFK equals t4.ID
                      join t5 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFK equals t5.ID
                      join t6 in db.Common_Unit on t1.Common_UnitFK equals t6.ID
                      join t7 in db.Mkt_BOM on t1.Mkt_BOMFK equals t7.ID
                      where t1.Mkt_BOMFK == bomID && t1.Prod_TransitionItemInventoryFK != 1

                      select new VmProd_PlanReference
                      {
                          Raw_InternaleTransferSlave = t1,
                          Raw_InternaleTransfer = t2,
                          Prod_Requisition_Slave = t3,
                          Prod_Requisition = t4,
                          Prod_TransitionItemInventory = t5,
                          Common_Unit = t6,
                          Mkt_BOM=t7

                      }).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();

            var list = v.Union(v1);

            this.DataList2 = list;

        }

        public void GetFollowUpByStyle(int bomID)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReference
                     join t2 in db.Prod_PlanReferenceProduction on t1.ID equals t2.Prod_PlanReferenceFK
                     join t0 in db.Prod_PlanReferenceProductionSection on t2.ID equals t0.Prod_PlanReferenceProductionFk
                     join t3 in db.Plan_OrderProcess on t2.Plan_OrderProcessFK equals t3.ID
                     join t4 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryScheduleFK equals t4.ID
                     join t5 in db.Mkt_BOM on t1.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                     join t7 in db.Plan_ProductionLine on t0.Plan_ProductionLineFk equals t7.ID
                     //join t6 in db.Mkt_OrderColorAndSizeRatio on t5.Common_TheOrderFk equals t6.Common_TheOrderFk
                     where t1.Mkt_BOMFK == bomID
                     && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t0.Active == true
                     && t7.Active == true
                     select new VmProd_PlanReference
                     {
                         Prod_PlanReference = t1,
                         Prod_PlanReferenceProduction = t2,
                         Prod_PlanReferenceProductionSection = t0,
                         Mkt_OrderDeliverySchedule = t4,
                         Mkt_BOM = t5,
                         Common_TheOrder=t6,
                         ExFactoryQty=0,
                         ShiftQty=0,
                         Plan_ProductionLine=t7,
                         // Mkt_OrderColorAndSizeRatio = t6,
                         Plan_OrderProcess = t3,
                         //x.TotalPlanQty += TotalQuantityByReferenceProductionSection(x.Prod_PlanReferenceProduction.ID)

        }).Distinct().OrderBy(x => x.Plan_OrderProcess.ID).AsEnumerable();
            List<VmProd_PlanReference> list = new List<VmProd_PlanReference>();
            foreach (VmProd_PlanReference x in v)
            {
                
                x.TotalPlanQty = TotalQuantityByReferenceProductionSection(x.Prod_PlanReferenceProduction.ID);
                //x.DoneQuantity = TotalDoneQuantityByReferenceProductionSection(x.Prod_PlanReferenceProductionSection.ID);
                x.DoneQuantity = TotalDoneQuantityByReferenceProductionSection(x.Plan_ProductionLine.ID);
                list.Add(x);
            }
            
            this.DataList = list;

        }

        private int? TotalQuantityByReferenceProductionSection(int id)
        {
            db = new xOssContext();
            var quantity = (from t1 in db.Prod_PlanReferenceProductionSection
                            where t1.Prod_PlanReferenceProductionFk == id && t1.Active == true
                            select t1.Quantity).ToList();
            int? TotalQuantity = 0;
            if (quantity != null)
            {
                TotalQuantity = (int)quantity.Sum();

            }

            return TotalQuantity;

        }

        
    }
}