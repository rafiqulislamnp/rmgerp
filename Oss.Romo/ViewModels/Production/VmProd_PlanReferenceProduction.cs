﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrystalDecisions.Web.HtmlReportRender;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Production
{
	public class VmProd_PlanReferenceProduction
	{
        private xOssContext db;
	    public int OrderProcessID { get; set; }
        public IEnumerable<VmProd_PlanReferenceProduction> DataList { get; set; }
        public Prod_PlanReferenceProduction Prod_PlanReferenceProduction { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
	    public int BOMID { get; set; }
        public string BuyerPo { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmProd_PlanReference VmProd_PlanReference { get; set; }
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Prod_PlanReference Prod_PlanReference { get; set; }
        public Common_Country Common_Country { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public string PortNo { get; set; }
        public VmProd_PlanReferenceProduction()
        {
            db = new xOssContext();
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad(int id)
        {
            var v = (from t1 in db.Prod_PlanReferenceProduction
                     join t2 in db.Plan_OrderProcess on t1.Plan_OrderProcessFK equals t2.ID
                     join t6 in db.Prod_PlanReference on t1.Prod_PlanReferenceFK equals t6.ID
                     
                     join t4 in db.Mkt_BOM on t6.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Mkt_Item on t4.Mkt_ItemFK equals t3.ID
                     //join t7 in db.Common_Country on t3.Destination equals t7.ID

                     where t1.Prod_PlanReferenceFK == id
                     select new VmProd_PlanReferenceProduction
                     {
                         Prod_PlanReferenceProduction = t1,
                         Plan_OrderProcess=t2,
                         Mkt_Item = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder =t5,
                         Prod_PlanReference = t6,
                        // Common_Country = t7
                     }).Where(x => x.Prod_PlanReferenceProduction.Active == true).OrderByDescending(x => x.Prod_PlanReferenceProduction.ID).AsEnumerable();
            this.DataList = v;
        }

        public void GetPlanReferenceProduction(int id)
        {
            var v = (from t1 in db.Prod_PlanReferenceProduction
                     join t2 in db.Plan_OrderProcess on t1.Plan_OrderProcessFK equals t2.ID
                     join t6 in db.Prod_PlanReference on t1.Prod_PlanReferenceFK equals t6.ID

                     join t4 in db.Mkt_BOM on t6.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Mkt_Item on t4.Mkt_ItemFK equals t3.ID
                     //join t7 in db.Common_Country on t3.Destination equals t7.ID

                     where t1.Plan_OrderProcessFK == id
                     select new VmProd_PlanReferenceProduction
                     {
                         Prod_PlanReferenceProduction = t1,
                         Plan_OrderProcess = t2,
                         Mkt_Item = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         Prod_PlanReference = t6,
                         // Common_Country = t7
                     }).Where(x => x.Prod_PlanReferenceProduction.Active == true).OrderByDescending(x => x.Prod_PlanReferenceProduction.ID).AsEnumerable();
            this.DataList = v;
        }

        public void InitialDataLoadForMasterPlanStep(int id)
        {
            var v = (from t1 in db.Prod_PlanReferenceProduction
                     join t2 in db.Prod_PlanReference on t1.Prod_PlanReferenceFK equals t2.ID
                     join t3 in db.Plan_OrderProcess on t1.Plan_OrderProcessFK equals t3.ID
                     join t5 in db.Mkt_BOM on t2.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                     
                     where t1.Plan_OrderProcessFK == id
                     select new VmProd_PlanReferenceProduction
                     {
                         Prod_PlanReferenceProduction = t1,
                         Prod_PlanReference = t2,
                         Plan_OrderProcess = t3,
                         Mkt_BOM=t5,
                         Common_TheOrder = t6,
                         BOMID = t5.ID,

                     }).Where(x => x.Prod_PlanReferenceProduction.Active == true).OrderByDescending(x => x.Prod_PlanReferenceProduction.ID).AsEnumerable();
            this.DataList = v;
        }
        
        public void SelectSingleData(int id)
        {
            var v = (from t1 in db.Prod_PlanReferenceProduction
                     join t2 in db.Plan_OrderProcess on t1.Plan_OrderProcessFK equals t2.ID
                     join t3 in db.Prod_PlanReference on t1.Prod_PlanReferenceFK equals t3.ID
                     join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t6 in db.Mkt_OrderDeliverySchedule on t3.Mkt_OrderDeliveryScheduleFK equals t6.ID
                     select new VmProd_PlanReferenceProduction
                     {
                         Prod_PlanReferenceProduction = t1,
                         Plan_OrderProcess = t2,
                         Prod_PlanReference = t3,
                         BuyerPo = t5.CID+ "/"+ t4.Style,
                         PortNo = t6.PortNo,
                         Mkt_OrderDeliverySchedule = t6
                     }).FirstOrDefault(x => x.Prod_PlanReferenceProduction.Active == true && x.Prod_PlanReferenceProduction.ID == id);
            if(v != null)
            {
                this.Prod_PlanReferenceProduction = v.Prod_PlanReferenceProduction;
                this.Plan_OrderProcess = v.Plan_OrderProcess;
                this.Prod_PlanReference = v.Prod_PlanReference;
                this.BuyerPo = v.BuyerPo;
                this.PortNo = v.PortNo;


            }         
        }
        
        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from rc in db.Prod_PlanReferenceProduction
                     select new VmProd_PlanReferenceProduction
                     {
                         Prod_PlanReferenceProduction = rc
                     }).SingleOrDefault(c => c.Prod_PlanReferenceProduction.ID == id);
            if (v != null) this.Prod_PlanReferenceProduction = v.Prod_PlanReferenceProduction;
        }

        public int Add(int userID)
        {
            Prod_PlanReferenceProduction.AddReady(userID);
            return Prod_PlanReferenceProduction.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_PlanReferenceProduction.Edit(userID);
        }

        public bool Delete(int userID)
        {           
            return Prod_PlanReferenceProduction.Delete(userID);          
        }
    }
}