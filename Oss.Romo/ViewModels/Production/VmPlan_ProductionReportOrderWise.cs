﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Production
{
    public class VmPlan_ProductionReportOrderWise
    {
        private xOssContext db;
        public string OrderNo { get; set; }
        public DateTime OrderDate { get; set; }
        public string Style { get; set; }
        public string PartyName { get; set; }
        public decimal TotalValue { get; set; }
        public decimal TotalCm { get; set; }
        public int TotalDoneQty { get; set; }
        public decimal TotalProductionCost { get; set; }
        public List<VmPlan_ProductionReportOrderWise> VmPlanProductionReportOrderWise { get; set; }
        public void InitialLoadData()
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Common_Currency on t1.Common_CurrencyFK equals t2.ID
                     join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                     join t4 in db.Mkt_BOM on t1.ID equals t4.Common_TheOrderFk
                     where t1.Active == true && t2.Active == true && t3.Active == true
                     select new
                     {
                         Common_TheOrder = t1,
                         Common_Currency = t2,
                         Mkt_Buyer = t3,
                         Mkt_BOMs = t4
                     }).Where(x => x.Common_TheOrder.Active == true).OrderByDescending(x => x.Common_TheOrder.ID).AsEnumerable();
           
            var list = new List<VmPlan_ProductionReportOrderWise>();
            foreach (var vmCommonTheOrder in v)
            {
                VmPlan_ProductionReportOrderWise planProductionReportOrderWise = new VmPlan_ProductionReportOrderWise();
                planProductionReportOrderWise.OrderNo = vmCommonTheOrder.Common_TheOrder.CID;
                planProductionReportOrderWise.PartyName = vmCommonTheOrder.Mkt_Buyer.Name;
                planProductionReportOrderWise.OrderDate = vmCommonTheOrder.Common_TheOrder.OrderDate;
                planProductionReportOrderWise.Style = vmCommonTheOrder.Mkt_BOMs.Style;
                MethodForGetBomDoneQtyValue(planProductionReportOrderWise, vmCommonTheOrder.Common_TheOrder.ID);

                var s = GetCmValue(vmCommonTheOrder.Mkt_BOMs.ID);
                planProductionReportOrderWise.TotalCm = s;
                planProductionReportOrderWise.TotalProductionCost = GetCost(0);

                list.Add(planProductionReportOrderWise);
            }
            this.VmPlanProductionReportOrderWise = list;
        }

        private void MethodForGetBomDoneQtyValue(VmPlan_ProductionReportOrderWise planProductionReportOrderWise, int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProduction on t2.Prod_PlanReferenceProductionFk equals t3.ID
                     join t4 in db.Prod_PlanReference on t3.Prod_PlanReferenceFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                     where t6.ID == id && t1.Active == true && t2.Active == true && t3.Active == true
                           && t4.Active == true && t5.Active == true && t6.Active == true
                     select new
                     {
                         OrderId = t6.CID,
                         MktPoId = t5.CID,
                         MktBomId = t5.ID,
                         Style = t5.Style,
                         Value = t5.UnitPrice * t5.QPack * t5.Quantity,
                         DoneQty = t5.UnitPrice * t1.Quantity
                     }).AsEnumerable();
            planProductionReportOrderWise.TotalValue = v.Sum(x => x.Value);
            planProductionReportOrderWise.TotalDoneQty = (int)v.Sum(x => x.DoneQty);
        }

        private decimal GetCmValue(int mktBomId)
        {
            VmOrderSummary s = new VmOrderSummary();
            var cmvalue = s.TotlaBomCost(mktBomId);
            if (cmvalue != null)
                return (decimal)cmvalue;
            else return 0;
        }

        private decimal GetCost(int i)
        {
            return 0;
        }
    }
    public class PlanProductionReportViewModel
    {

    }

}