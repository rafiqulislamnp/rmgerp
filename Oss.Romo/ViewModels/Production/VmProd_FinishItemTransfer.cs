﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_FinishItemTransfer
    {
        private xOssContext db;

        public VmProd_FinishItemTransfer()
        {
            db = new xOssContext();
        }

        public Prod_InputRequisition Prod_InputRequisition { get; set; }
        public Prod_InputRequisitionSlave Prod_InputRequisitionSlave { get; set; }
        public Prod_FinishItemTransfer Prod_FinishItemTransfer { get; set; }
        public Prod_FinishItemTransferSlave Prod_FinishItemTransferSlave { get; set; }
        public User_Department User_Department { get; set; }
        public User_Department ToUser_Department { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public VmProd_FinishItemTransferSlave VmProd_FinishItemTransferSlave { get; set; }
        public VmProd_InputRequisitionSlave VmProd_InputRequisitionSlave { get; set; }

        public IEnumerable<VmProd_FinishItemTransfer> DataList { get; set; }

        public void InitialDataLoad(int DepartmentId)
        {
            if (DepartmentId == 3)
            {
                var v = (from t1 in db.Prod_FinishItemTransfer
                         join t2 in db.Prod_InputRequisition on t1.Prod_InputRequisitionFK equals t2.ID
                         join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                         join t4 in db.User_Department on t1.FromUser_DeptFK equals t4.ID
                         where t1.Active == true
                         select new VmProd_FinishItemTransfer
                         {
                             Prod_FinishItemTransfer=t1,
                             Prod_InputRequisition = t2,
                             User_Department = t4,
                             ToUser_Department=t3
                         }).OrderByDescending(x => x.Prod_FinishItemTransfer.ID).AsEnumerable();
                this.DataList = v;
            }
            else
            {
                var v = (from t1 in db.Prod_FinishItemTransfer
                         join t2 in db.Prod_InputRequisition on t1.Prod_InputRequisitionFK equals t2.ID
                         join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                         join t4 in db.User_Department on t1.FromUser_DeptFK equals t4.ID
                         where t1.Active == true && t1.FromUser_DeptFK == DepartmentId
                         select new VmProd_FinishItemTransfer
                         {
                             Prod_FinishItemTransfer = t1,
                             Prod_InputRequisition = t2,
                             User_Department = t4,
                             ToUser_Department=t3
                         }).OrderByDescending(x => x.Prod_FinishItemTransfer.ID).AsEnumerable();
                this.DataList = v;
            }
        }

        public VmProd_FinishItemTransfer SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_FinishItemTransfer
                     join t2 in db.Prod_InputRequisition on t1.Prod_InputRequisitionFK equals t2.ID
                     join t3 in db.User_Department on t1.FromUser_DeptFK equals t3.ID
                     join t4 in db.User_Department on t1.ToUser_DeptFK equals t4.ID
                     where t1.ID == iD
                     select new VmProd_FinishItemTransfer
                     {
                         Prod_FinishItemTransfer=t1,
                         Prod_InputRequisition = t2,
                         User_Department = t3,
                         ToUser_Department = t4
                     }).FirstOrDefault();
            return v;
        }

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from rc in db.Prod_FinishItemTransfer
                     select new VmProd_FinishItemTransfer
                     {
                         Prod_FinishItemTransfer = rc
                     }).Where(c => c.Prod_FinishItemTransfer.ID == iD).SingleOrDefault();
            this.Prod_FinishItemTransfer = v.Prod_FinishItemTransfer;
        }

        public int Add(int userID)
        {
            Prod_FinishItemTransfer.AddReady(userID);
            return Prod_FinishItemTransfer.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_FinishItemTransfer.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_FinishItemTransfer.Delete(userID);
        }
    }
}