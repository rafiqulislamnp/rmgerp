﻿using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_InputRequisition
    {
        private xOssContext db;
        
        public Prod_InputRequisition Prod_InputRequisition { get; set; }
        
        public User_Department User_Department { get; set; }
        public User_Department ToUser_Department { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        
        public Prod_InputRequisitionSlave Prod_InputRequisitionSlave { get; set; }

        public VmProd_InputRequisitionSlave VmProd_InputRequisitionSlave { get; set; }

        public IEnumerable<VmProd_InputRequisition> DataList { get; set; }
      
        public VmProd_InputRequisition()
        {
            db = new xOssContext();
        }

        public void InitialDataLoad(int DepartmentId)
        {
            if (DepartmentId == 3)
            {
                var v = (from t1 in db.Prod_InputRequisition
                         join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                         where t1.Active == true
                         select new VmProd_InputRequisition
                         {
                             Prod_InputRequisition = t1,
                             ToUser_Department = t3,
                         }).OrderByDescending(x => x.Prod_InputRequisition.ID).AsEnumerable();
                this.DataList = v;
            }
            else
            {
                var v = (from t1 in db.Prod_InputRequisition
                         join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                         where t1.Active == true && t1.FromUser_DeptFK == DepartmentId
                         select new VmProd_InputRequisition
                         {
                             Prod_InputRequisition = t1,
                             ToUser_Department = t3,
                         }).OrderByDescending(x => x.Prod_InputRequisition.ID).AsEnumerable();
                this.DataList = v;
            }
        }

        public void ReceivedInitialDataLoad(int DepartmentId)
        {
            if (DepartmentId == 3)
            {
                var v = (from t1 in db.Prod_InputRequisition
                         join t3 in db.User_Department on t1.FromUser_DeptFK equals t3.ID
                         where t1.Active == true
                         select new VmProd_InputRequisition
                         {
                             Prod_InputRequisition = t1,
                             ToUser_Department = t3,
                         }).OrderByDescending(x => x.Prod_InputRequisition.ID).AsEnumerable();
                this.DataList = v;
            }
            else
            {
                var v = (from t1 in db.Prod_InputRequisition
                         join t3 in db.User_Department on t1.FromUser_DeptFK equals t3.ID
                         where t1.Active == true && t1.ToUser_DeptFK == DepartmentId
                         select new VmProd_InputRequisition
                         {
                             Prod_InputRequisition = t1,
                             ToUser_Department = t3,
                         }).OrderByDescending(x => x.Prod_InputRequisition.ID).AsEnumerable();
                this.DataList = v;
            }
        }

        public VmProd_InputRequisition SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_InputRequisition
                     join t2 in db.User_Department on t1.FromUser_DeptFK equals t2.ID
                     join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                     where t1.ID == iD
                     select new VmProd_InputRequisition
                     {
                         Prod_InputRequisition = t1,
                         User_Department = t2,
                         ToUser_Department = t3
                     }).FirstOrDefault();
            return v;
        }

        //public VmProd_Requisition SelectSingleJoined(int iD)
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Prod_Requisition
        //             join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
        //             join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //             //join t4 in db.User_Department on t1.User_DeptFK equals t4.ID
        //             where t1.ID == iD
        //             select new VmProd_Requisition
        //             {
        //                 Prod_Requisition = t1,
        //                 Mkt_BOM = t2,
        //                 Common_TheOrder = t3,
        //                 //User_Department = t4
        //             }).FirstOrDefault();
        //    return v;
        //}

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from rc in db.Prod_InputRequisition
                     select new VmProd_InputRequisition
                     {
                         Prod_InputRequisition = rc
                     }).Where(c => c.Prod_InputRequisition.ID == iD).SingleOrDefault();
            this.Prod_InputRequisition = v.Prod_InputRequisition;
        }
       
        public int Add(int userID)
        {
            Prod_InputRequisition.AddReady(userID);
            return Prod_InputRequisition.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_InputRequisition.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_InputRequisition.Delete(userID);
        }
        
        public void ChangeRequisitionStatus(int id)
        {
            VmProd_InputRequisition r = new VmProd_InputRequisition();
            r.SelectSingle(id);
            r.Prod_InputRequisition.IsComplete = !r.Prod_InputRequisition.IsComplete;
            r.Edit(0);
        }
        
        //public void GetItemForConsmtionREQ()
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Prod_InputRequisition
        //             join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
        //             where t1.IsComplete == false
        //             select new VmProd_InputRequisition
        //             {
        //                 Mkt_BOM = t2,
        //                 Prod_InputRequisition = t1
        //             }).Where(x => x.Prod_Requisition.Active == true).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();
        //    this.DataList = v;
        //}

    }
}