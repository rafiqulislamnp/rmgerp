﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_Capacity
    {
        private xOssContext db; 
        public IEnumerable<VmProd_Capacity> DataList { get; set; }
        public Prod_Capacity Prod_Capacity { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public Plan_ProductionLine Plan_ProductionLine { get; set; }
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public string MonthYear { get; set; }
        public Prod_Planning Prod_Planning { get; set; }
        public VmProd_Capacity()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Capacity
                     select new VmProd_Capacity
                     {
                         Prod_Capacity = t1
                     }).Where(x => x.Prod_Capacity.Active == true).OrderByDescending(x => x.Prod_Capacity.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingle(int id)
        {
          
            db = new xOssContext();
            var v = (from t1 in db.Prod_Capacity
                     select new VmProd_Capacity
                     {
                         Prod_Capacity = t1
                     }).Where(c => c.Prod_Capacity.ID == id).SingleOrDefault();
            this.Prod_Capacity = v.Prod_Capacity;

        }

        public int Add(int userID)
        {
            Prod_Capacity.AddReady(userID);
            return Prod_Capacity.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_Capacity.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_Capacity.Delete(userID);
        }

        public int GetTotalLineInFactory(int orderProcess)
        {
            db = new xOssContext();
            int totalLine = (from t1 in db.Plan_ProductionLine
                where t1.Active == true && t1.Plan_OrderProcessFK == orderProcess
                             select t1.LineNumber).Count();
            return totalLine;
        }
    }
}