﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_PlanReferenceOrder
    {
        private xOssContext db;

        public IEnumerable<VmProd_PlanReferenceOrder> DataList { get; set; }
        public List<ProductionDailyPlan> PlanDataList { get; set; }

        public Prod_PlanReferenceOrder Prod_PlanReferenceOrder { get; set; }
        public Prod_Reference Prod_Reference { get; set; }
        public ProductionDailyPlan ProductionDailyPlan { get; set; }
        
        [Display(Name ="Line No"),Required(ErrorMessage ="Production Line is required")]
        public int LineId { get; set; }
        public string BuyerPo { get; set; }
        public string PortNo { get; set; }
        public string OrderName { get; set; }
        [Display(Name ="Order"),Required(ErrorMessage ="Order is required")]
        public int BOMID { get; set; }
        public int Status { get; set; }
        public string LineName { get; set; }
        public string CheifSuperVisorName { get; set; }
        public int LineChiefFk { get; set; }
        public int LineSupervisorFk { get; set; }
        public int UserId { get; set; }
        public int UpdateRemainQty { get; set; }
        public bool IsProductionStart { get; set; }

        public VmProd_PlanReferenceOrder()
        {
            db = new xOssContext();
        }
        
        public void CuttingInitialDataLoad(int id)
        {
            var vData = (from t1 in db.Prod_PlanReferenceOrder
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                         join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
                         join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
                         join t7 in db.Common_Country on t2.Destination equals t7.ID
                         where t1.Prod_ReferenceFK == id
                         && t1.Active == true
                         && t1.SewingPlanQuantity == 0
                         && t1.IronPlanQuantity == 0
                         && t1.PackingPlanQuantity == 0
                         && t1.SectionId==1
                         select new ProductionDailyPlan
                         {
                             Prod_PlanReferenceOrderFk = t1.ID,
                             OrderNo = t3.CID + "/" + t4.Style,
                             Prod_ReferenceFk = id,
                             OrderDeliveryScheduleFk = t5.ID,
                             Mkt_OrderColorAndSizeRatioFk = t6.ID,
                             Port = t7.Name+"-"+t2.PortNo,
                             Color = t6.Color,
                             Size = t6.Size,
                             OrderQty = (int)t6.Quantity,
                             CuttingPlanQuantity = t1.CuttingPlanQuantity,
                             CuttingExtra = t1.CuttingExtra,
                             GSM=t1.GSM,
                             EntryDate=t2.Date,
                             Plan_ProductionLineFk = t1.Plan_ProductionLineFk,
                         }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    GetLineCheif((int)v.Plan_ProductionLineFk);
                    v.CuttingDoneQuantity = GetSectionWiseDoneQty(v.Prod_PlanReferenceOrderFk, 1);
                    //v.RemainQty = GetCuttingRemainQty(v.Prod_ReferenceFk, v.Mkt_OrderColorAndSizeRatioFk, v.OrderQty, 1);
                    v.ExtraQty= (v.CuttingExtra * v.OrderQty) / 100;
                    v.RemainQty = GetRemainPlanQty(v.Prod_ReferenceFk, v.Mkt_OrderColorAndSizeRatioFk, 1) + v.ExtraQty;
                    v.LineChiefSupervisor = this.CheifSuperVisorName;
                    v.LineTable = this.LineName;
                }
            }
            this.PlanDataList = vData;
        }

        public void CuttingFollowupDataLoad(int id)
        {
            DeleteZeroPlanOrder(id,1);
            var vData = (from t1 in db.Prod_PlanReferenceOrder
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                         join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
                         join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
                         join t7 in db.Common_Country on t2.Destination equals t7.ID
                         where t1.Prod_ReferenceFK == id
                         && t1.Active == true
                         && t1.SewingPlanQuantity == 0
                         && t1.IronPlanQuantity == 0
                         && t1.PackingPlanQuantity == 0
                         && t1.SectionId == 1
                         select new ProductionDailyPlan
                         {
                             Prod_PlanReferenceOrderFk = t1.ID,
                             OrderNo = t3.CID + "/" + t4.Style,
                             Prod_ReferenceFk = id,
                             OrderDeliveryScheduleFk = t5.ID,
                             Mkt_OrderColorAndSizeRatioFk = t6.ID,
                             Port = t7.Name + "-" + t2.PortNo,
                             Color = t6.Color,
                             Size = t6.Size,
                             OrderQty = (int)t6.Quantity,
                             CuttingPlanQuantity = t1.CuttingPlanQuantity,
                             CuttingExtra = t1.CuttingExtra,
                             LayerWeight=t1.PerLayerWeight,
                             MarkerPiece=t1.MarkerPiece,
                             EntryDate = t2.Date,
                             Plan_ProductionLineFk = t1.Plan_ProductionLineFk,
                             LineTable = db.Plan_ProductionLine.Where(a => a.ID == t1.Plan_ProductionLineFk).ToList().FirstOrDefault().Name
                         }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.CuttingDoneQuantity = GetSectionWiseDoneQty(v.Prod_PlanReferenceOrderFk, 1);
                    if (v.LayerWeight > 0 && v.MarkerPiece > 0)
                    {
                        v.CuttingDoneQuantityWeight = Math.Round((v.LayerWeight / v.MarkerPiece) * v.CuttingDoneQuantity,2);
                    }
                }
            }
            this.PlanDataList = vData;
        }

        public void SewingInitialDataLoad(int id)
        {
            var vSewingData = (from t1 in db.Prod_PlanReferenceOrder
                               join t2 in db.Mkt_BOM on t1.MktBomFk equals t2.ID
                               join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                               join t4 in db.Mkt_Item on t2.Mkt_ItemFK equals t4.ID
                               where t1.Prod_ReferenceFK == id && t1.CuttingPlanQuantity == 0 && t1.SectionId == 2
                               select new ProductionDailyPlan
                               {
                                   Prod_PlanReferenceOrderFk = t1.ID,
                                   Prod_ReferenceFk = t1.Prod_ReferenceFK,
                                   BOMFk = t1.MktBomFk,
                                   OrderNo = t3.CID + "/" + t2.Style,
                                   SewingPlanQuantity = t1.SewingPlanQuantity,
                                   WorkingHour = t1.WorkingHour,
                                   PerHourTarget = (int)t1.Quantity,
                                   MachineRunning = t1.MachineRunning,
                                   SMV = t1.SMV,
                                   Plan_ProductionLineFk = t1.Plan_ProductionLineFk,
                                   NoOfHelper = t1.PresentHelper,
                                   PresentOpt = t1.PresentOperator
                               }).ToList();
            
            if (vSewingData.Any())
            {
                foreach (var v in vSewingData)
                {
                    GetLineCheif((int)v.Plan_ProductionLineFk);
                    v.LineChiefSupervisor = this.CheifSuperVisorName;
                    v.LineTable = this.LineName;
                }
            }
            this.PlanDataList = vSewingData;
        }

        //public void SewingFollowupDataLoad(int id)
        //{
        //    DeleteZeroPlanOrder(id,2);
        //    var vData = (from t1 in db.Prod_PlanReferenceOrder
        //                        join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
        //                        join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //                        join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
        //                        join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
        //                        join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
        //                        join t7 in db.Common_Country on t2.Destination equals t7.ID
        //                        where t1.Prod_ReferenceFK == id
        //                        && t1.Active == true
        //                        && t1.CuttingPlanQuantity == 0
        //                        && t1.SewingPlanQuantity!=0
        //                        && t1.SectionId==2
        //                        select new ProductionDailyPlan
        //                        {
        //                            Prod_PlanReferenceOrderFk = t1.ID,
        //                            OrderNo = t3.CID + "/" + t4.Style,
        //                            Prod_ReferenceFk = id,
        //                            OrderDeliveryScheduleFk = (int)t1.OrderDeliveryScheduleFk,
        //                            Mkt_OrderColorAndSizeRatioFk = (int)t1.Mkt_OrderColorAndSizeRatioFk,
        //                            Port = t7.Name + "-" + t2.PortNo,
        //                            Color = t6.Color,
        //                            Size = t6.Size,
        //                            OrderQty = (int)t6.Quantity,
        //                            SewingPlanQuantity = t1.SewingPlanQuantity,
        //                            WorkingHour = t1.WorkingHour,
        //                            MachineRunning = t1.MachineRunning,
        //                            SMV = t1.SMV,
        //                            Plan_ProductionLineFk = t1.Plan_ProductionLineFk,
        //                            EntryDate = t2.Date,
        //                            LineTable = db.Plan_ProductionLine.Where(a => a.ID == t1.Plan_ProductionLineFk).ToList().FirstOrDefault().Name
        //                        }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();

        //    if (vData.Any())
        //    {
        //        foreach (var v in vData)
        //        {
        //            v.CuttingDoneQuantity = GetSectionWiseTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,1);
        //            v.SewingDoneQuantity = GetSectionWiseDoneQty(v.Prod_PlanReferenceOrderFk, 2);
        //        }
        //    }
        //    this.PlanDataList = vData;
        //}

        public void SewingFollowupDataLoad(int id)
        {
            var vData = (from t0 in db.Prod_PlanReferenceOrder
                         join t1 in db.Prod_PlanReferenceOrderSection on t0.ID equals t1.Prod_PlanReferenceOrderFk
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                         join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
                         join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
                         join t7 in db.Common_Country on t2.Destination equals t7.ID
                         join t8 in db.Plan_ProductionLine on t0.Plan_ProductionLineFk equals t8.ID
                         where t0.Prod_ReferenceFK == id && t0.CuttingPlanQuantity == 0 && t0.SewingPlanQuantity != 0 && t0.SectionId == 2
                         select new ProductionDailyPlan
                         {
                             ID=t1.ID,
                             Prod_ReferenceFk = id,
                             BOMFk=t0.MktBomFk,
                             Prod_PlanReferenceOrderFk = t0.ID,
                             OrderDeliveryScheduleFk = (int)t1.OrderDeliveryScheduleFk,
                             Mkt_OrderColorAndSizeRatioFk = (int)t1.Mkt_OrderColorAndSizeRatioFk,
                             OrderNo = t3.CID + "/" + t4.Style,
                             Port = t7.Name + "-" + t2.PortNo,
                             Color = t6.Color,
                             Size = t6.Size,
                             OrderQty = (int)t6.Quantity,
                             SewingDoneQuantity=(int)t1.Quantity,
                             LineTable = t8.Name
                         }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.CuttingDoneQuantity = GetCuttingTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk, 1);
                    v.RemainQty = GetSizeColorTotalDoneQty(v.BOMFk, v.Mkt_OrderColorAndSizeRatioFk, 2) - v.OrderQty;
                }
            }
            this.PlanDataList = vData;
        }

        //public void IronInitialDataLoad(int id)
        //{
        //    var vIronningData = (from t1 in db.Prod_PlanReferenceOrder
        //                        join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
        //                        join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //                        join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
        //                        join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
        //                        join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
        //                        join t7 in db.Common_Country on t2.Destination equals t7.ID
        //                        where t1.Prod_ReferenceFK == id
        //                        && t1.Active == true
        //                        && t1.CuttingPlanQuantity == 0
        //                        && t1.SewingPlanQuantity == 0
        //                        && t1.SectionId==3
        //                        select new ProductionDailyPlan
        //                        {
        //                            Prod_PlanReferenceOrderFk = t1.ID,
        //                            OrderNo = t3.CID + "/" + t4.Style,
        //                            Prod_ReferenceFk = id,
        //                            OrderDeliveryScheduleFk = (int)t1.OrderDeliveryScheduleFk,
        //                            Mkt_OrderColorAndSizeRatioFk = (int)t1.Mkt_OrderColorAndSizeRatioFk,
        //                            Port = t7.Name + "-" + t2.PortNo,
        //                            Color = t6.Color,
        //                            Size = t6.Size,
        //                            OrderQty = (int)t6.Quantity,
        //                            IronPlanQuantity = t1.IronPlanQuantity,
        //                            EntryDate = t2.Date
        //                        }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();

        //    if (vIronningData.Any())
        //    {
        //        foreach (var v in vIronningData)
        //        {
        //            //GetLineCheif((int)v.Plan_ProductionLineFk);
        //            v.CuttingDoneQuantity = GetSectionWiseTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk, 1);
        //            v.SewingDoneQuantity = GetSectionWiseTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,2);
        //            //v.RemainQty = GetRemainQty(v.Prod_ReferenceFk, v.Mkt_OrderColorAndSizeRatioFk, 2, 3);
        //            //v.RemainQty = v.OrderQty - GetSectionDoneQty(v.Mkt_OrderColorAndSizeRatioFk, 3);
        //            v.RemainQty= GetRemainPlanQty(v.Prod_ReferenceFk, v.Mkt_OrderColorAndSizeRatioFk, 3);
        //        }
        //    }
        //    this.PlanDataList = vIronningData;
        //}

        public void IronPlanInitialDataLoad(int id)
        {
            var vIronningData = (from t1 in db.Prod_PlanReferenceOrder
                               join t2 in db.Mkt_BOM on t1.MktBomFk equals t2.ID
                               join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                               join t4 in db.Mkt_Item on t2.Mkt_ItemFK equals t4.ID
                               where t1.Prod_ReferenceFK == id && t1.SectionId == 3
                               select new ProductionDailyPlan
                               {
                                   Prod_PlanReferenceOrderFk = t1.ID,
                                   Prod_ReferenceFk = t1.Prod_ReferenceFK,
                                   BOMFk = t1.MktBomFk,
                                   OrderNo = t3.CID + "/" + t2.Style,
                                   OrderQty=t2.QPack*t2.Quantity
                               }).ToList();
            if (vIronningData.Any())
            {
                foreach (var v in vIronningData)
                {
                    v.CuttingDoneQuantity = GetOrderTotalDoneQty(v.BOMFk, 1);
                    v.SewingDoneQuantity = GetOrderTotalDoneQty(v.BOMFk, 2);
                    v.IronDoneQuantity= GetOrderTotalDoneQty(v.BOMFk, 3);
                    v.RemainQty = v.OrderQty - v.IronDoneQuantity;
                }
            }
            this.PlanDataList = vIronningData;
        }

        public void IronFollowupDataLoad(int id)
        {
            var vData = (from t0 in db.Prod_PlanReferenceOrder
                         join t1 in db.Prod_PlanReferenceOrderSection on t0.ID equals t1.Prod_PlanReferenceOrderFk
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                         join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
                         join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
                         join t7 in db.Common_Country on t2.Destination equals t7.ID
                         where t0.Prod_ReferenceFK == id && t0.SectionId == 3
                         select new ProductionDailyPlan
                         {
                             ID = t1.ID,
                             Prod_ReferenceFk = id,
                             BOMFk=t0.MktBomFk,
                             Prod_PlanReferenceOrderFk = t0.ID,
                             OrderDeliveryScheduleFk = (int)t1.OrderDeliveryScheduleFk,
                             Mkt_OrderColorAndSizeRatioFk = (int)t1.Mkt_OrderColorAndSizeRatioFk,
                             OrderNo = t3.CID + "/" + t4.Style,
                             Port = t7.Name + "-" + t2.PortNo,
                             Color = t6.Color,
                             Size = t6.Size,
                             OrderQty = (int)t6.Quantity,
                             IronDoneQuantity = (int)t1.Quantity
                         }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.CuttingDoneQuantity = GetCuttingTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk, 1);
                    v.SewingDoneQuantity = GetSizeColorTotalDoneQty(v.BOMFk, v.Mkt_OrderColorAndSizeRatioFk, 2);
                    v.RemainQty = v.OrderQty - GetSizeColorTotalDoneQty(v.BOMFk, v.Mkt_OrderColorAndSizeRatioFk, 3);
                }
            }
            this.PlanDataList = vData;
        }

        //public void IroningFollowupDataLoad(int id)
        //{
        //    DeleteZeroPlanOrder(id, 3);
        //    var vData = (from t1 in db.Prod_PlanReferenceOrder
        //                 join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
        //                 join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //                 join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
        //                 join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
        //                 join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
        //                 join t7 in db.Common_Country on t2.Destination equals t7.ID
        //                 where t1.Prod_ReferenceFK == id
        //                 && t1.Active == true
        //                 && t1.CuttingPlanQuantity == 0
        //                 && t1.SewingPlanQuantity == 0
        //                 && t1.IronPlanQuantity != 0
        //                 && t1.SectionId == 3
        //                 select new ProductionDailyPlan
        //                 {
        //                     Prod_PlanReferenceOrderFk = t1.ID,
        //                     OrderNo = t3.CID + "/" + t4.Style,
        //                     Prod_ReferenceFk = id,
        //                     OrderDeliveryScheduleFk = (int)t1.OrderDeliveryScheduleFk,
        //                     Mkt_OrderColorAndSizeRatioFk = (int)t1.Mkt_OrderColorAndSizeRatioFk,
        //                     Port = t7.Name + "-" + t2.PortNo,
        //                     Color = t6.Color,
        //                     Size = t6.Size,
        //                     OrderQty = (int)t6.Quantity,
        //                     IronPlanQuantity = t1.IronPlanQuantity,
        //                     EntryDate = t2.Date,
        //                 }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();

        //    if (vData.Any())
        //    {
        //        foreach (var v in vData)
        //        {
        //            v.CuttingDoneQuantity = GetSectionWiseTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,1);
        //            v.SewingDoneQuantity = GetSectionWiseTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,2);
        //            v.IronDoneQuantity = GetSectionWiseDoneQty(v.Prod_PlanReferenceOrderFk, 3);
        //        }
        //    }
        //    this.PlanDataList = vData;
        //}

        //public void PackingInitialDataLoad(int id)
        //{
        //    var vIronningData = (from t1 in db.Prod_PlanReferenceOrder
        //                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
        //                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //                         join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
        //                         join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
        //                         join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
        //                         join t7 in db.Common_Country on t2.Destination equals t7.ID
        //                         where t1.Prod_ReferenceFK == id
        //                         && t1.Active == true
        //                         && t1.CuttingPlanQuantity == 0
        //                         && t1.SewingPlanQuantity == 0
        //                         && t1.IronPlanQuantity==0
        //                         && t1.SectionId==4
        //                         select new ProductionDailyPlan
        //                         {
        //                             Prod_PlanReferenceOrderFk = t1.ID,
        //                             OrderNo = t3.CID + "/" + t4.Style,
        //                             Prod_ReferenceFk = id,
        //                             OrderDeliveryScheduleFk = (int)t1.OrderDeliveryScheduleFk,
        //                             Mkt_OrderColorAndSizeRatioFk = (int)t1.Mkt_OrderColorAndSizeRatioFk,
        //                             Port = t7.Name + "-" + t2.PortNo,
        //                             Color = t6.Color,
        //                             Size = t6.Size,
        //                             OrderQty = (int)t6.Quantity,
        //                             PackingPlanQuantity = t1.PackingPlanQuantity,
        //                             EntryDate = t2.Date
        //                         }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();

        //    if (vIronningData.Any())
        //    {
        //        foreach (var v in vIronningData)
        //        {
        //            //GetLineCheif((int)v.Plan_ProductionLineFk);
        //            v.CuttingDoneQuantity = GetCuttingTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,1);
        //            v.SewingDoneQuantity = GetCuttingTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,2);
        //            v.IronDoneQuantity = GetCuttingTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,3);
        //            //v.RemainQty = GetRemainQty(v.Prod_ReferenceFk, v.Mkt_OrderColorAndSizeRatioFk, 3, 4);
        //            //v.RemainQty = v.OrderQty - GetSectionDoneQty(v.Mkt_OrderColorAndSizeRatioFk, 4);
        //            v.RemainQty = GetRemainPlanQty(v.Prod_ReferenceFk, v.Mkt_OrderColorAndSizeRatioFk, 4);
        //        }
        //    }
        //    this.PlanDataList = vIronningData;
        //}

        public void PackingInitialDataLoad(int id)
        {
            var vPackingData = (from t1 in db.Prod_PlanReferenceOrder
                                 join t2 in db.Mkt_BOM on t1.MktBomFk equals t2.ID
                                 join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                                 join t4 in db.Mkt_Item on t2.Mkt_ItemFK equals t4.ID
                                 where t1.Prod_ReferenceFK == id && t1.SectionId == 4
                                 select new ProductionDailyPlan
                                 {
                                     Prod_PlanReferenceOrderFk = t1.ID,
                                     Prod_ReferenceFk = t1.Prod_ReferenceFK,
                                     BOMFk = t1.MktBomFk,
                                     OrderNo = t3.CID + "/" + t2.Style,
                                     OrderQty = t2.QPack * t2.Quantity
                                 }).ToList();
            
            if (vPackingData.Any())
            {
                foreach (var v in vPackingData)
                {
                    v.CuttingDoneQuantity = GetOrderTotalDoneQty(v.BOMFk, 1);
                    v.SewingDoneQuantity = GetOrderTotalDoneQty(v.BOMFk, 2);
                    v.IronDoneQuantity = GetOrderTotalDoneQty(v.BOMFk, 3);
                    v.PackingDoneQuantity= GetOrderTotalDoneQty(v.BOMFk, 4);
                    v.RemainQty = v.OrderQty - v.PackingDoneQuantity;
                }
            }
            this.PlanDataList = vPackingData;
        }
        
        //public void PackingFollowupDataLoad(int id)
        //{
        //    DeleteZeroPlanOrder(id,4);
        //    var vData = (from t1 in db.Prod_PlanReferenceOrder
        //                 join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
        //                 join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //                 join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
        //                 join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
        //                 join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
        //                 join t7 in db.Common_Country on t2.Destination equals t7.ID
        //                 where t1.Prod_ReferenceFK == id
        //                 && t1.Active == true
        //                 && t1.CuttingPlanQuantity == 0
        //                 && t1.SewingPlanQuantity == 0
        //                 && t1.IronPlanQuantity == 0
        //                 && t1.PackingPlanQuantity != 0
        //                 && t1.SectionId == 4
        //                 select new ProductionDailyPlan
        //                 {
        //                     Prod_PlanReferenceOrderFk = t1.ID,
        //                     OrderNo = t3.CID + "/" + t4.Style,
        //                     Prod_ReferenceFk = id,
        //                     OrderDeliveryScheduleFk = (int)t1.OrderDeliveryScheduleFk,
        //                     Mkt_OrderColorAndSizeRatioFk = (int)t1.Mkt_OrderColorAndSizeRatioFk,
        //                     Port = t7.Name + "-" + t2.PortNo,
        //                     Color = t6.Color,
        //                     Size = t6.Size,
        //                     OrderQty = (int)t6.Quantity,
        //                     PackingPlanQuantity = t1.PackingPlanQuantity,
        //                     EntryDate = t2.Date,
        //                 }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();

        //    if (vData.Any())
        //    {
        //        foreach (var v in vData)
        //        {
        //            v.CuttingDoneQuantity = GetCuttingTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,1);
        //            v.SewingDoneQuantity = GetCuttingTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,2);
        //            v.IronDoneQuantity = GetCuttingTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk,3);
        //            v.PackingDoneQuantity = GetSectionWiseDoneQty(v.Prod_PlanReferenceOrderFk, 4);
        //        }
        //    }
        //    this.PlanDataList = vData;
        //}

        public void PackingFollowupDataLoad(int id)
        {
            var vData = (from t0 in db.Prod_PlanReferenceOrder
                         join t1 in db.Prod_PlanReferenceOrderSection on t0.ID equals t1.Prod_PlanReferenceOrderFk
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                         join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
                         join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
                         join t7 in db.Common_Country on t2.Destination equals t7.ID
                         where t0.Prod_ReferenceFK == id && t0.SectionId == 4
                         select new ProductionDailyPlan
                         {
                             ID = t1.ID,
                             Prod_ReferenceFk = id,
                             BOMFk = t0.MktBomFk,
                             Prod_PlanReferenceOrderFk = t0.ID,
                             OrderDeliveryScheduleFk = (int)t1.OrderDeliveryScheduleFk,
                             Mkt_OrderColorAndSizeRatioFk = (int)t1.Mkt_OrderColorAndSizeRatioFk,
                             OrderNo = t3.CID + "/" + t4.Style,
                             Port = t7.Name + "-" + t2.PortNo,
                             Color = t6.Color,
                             Size = t6.Size,
                             OrderQty = (int)t6.Quantity,
                             PackingDoneQuantity = (int)t1.Quantity
                         }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.CuttingDoneQuantity = GetCuttingTotalDoneQty(v.OrderDeliveryScheduleFk, v.Mkt_OrderColorAndSizeRatioFk, 1);
                    v.SewingDoneQuantity = GetSizeColorTotalDoneQty(v.BOMFk, v.Mkt_OrderColorAndSizeRatioFk, 2);
                    v.IronDoneQuantity = GetSizeColorTotalDoneQty(v.BOMFk, v.Mkt_OrderColorAndSizeRatioFk, 3);
                    v.RemainQty = v.OrderQty - GetSizeColorTotalDoneQty(v.BOMFk, v.Mkt_OrderColorAndSizeRatioFk, 4);
                }
            }
            this.PlanDataList = vData;
        }

        public int GetCuttingTotalDoneQty(int OrderDeliveryScheduleFk, int OrderColorAndSizeRatioFk, int SectionId)
        {
            int TotalQty = 0;
            var vData = db.Prod_PlanReferenceOrderSection.Where(a => a.OrderDeliveryScheduleFk == OrderDeliveryScheduleFk && a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && a.Plan_ReferenceSectionFk == SectionId).ToList();
            if (vData.Any())
            {
                TotalQty = (int)vData.Sum(a => a.Quantity);
            }

            return TotalQty;
        }

        public int GetSectionWiseDoneQty(int Id,int SectionId)
        {
            int TotalQty = 0;
            var vData = db.Prod_PlanReferenceOrderSection.Where(a => a.Prod_PlanReferenceOrderFk == Id && a.Plan_ReferenceSectionFk == SectionId).ToList();
            if (vData.Any())
            {
                TotalQty = (int)vData.Sum(a => a.Quantity);
            }

            return TotalQty;
        }

        public int GetCuttingRemainQty(int Prod_ReferenceFK, int OrderColorAndSizeRatioFk, int OrderQty, int CurrentSectionId)
        {
            int TotalPlanQty = 0;
            int TotalDoneQty = 0;
            int RemainQty = 0;
            
            var vOrderPlanData = db.Prod_PlanReferenceOrder.Where(a => a.Prod_ReferenceFK == Prod_ReferenceFK && a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk).ToList();
            if (vOrderPlanData.Any())
            {
                TotalPlanQty = (int)vOrderPlanData.Sum(a => a.CuttingPlanQuantity);
            }

            var vOrderDoneData = (from o in db.Prod_PlanReferenceOrder
                                 join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                                 where p.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && p.Plan_ReferenceSectionFk == CurrentSectionId
                                 select new
                                 {
                                     p
                                 }).ToList();
            if (vOrderPlanData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.p.Quantity);
            }

            if (TotalDoneQty > 0)
            {
                RemainQty = OrderQty - TotalDoneQty;
            }
            else if (TotalDoneQty == 0)
            {
                RemainQty = OrderQty - TotalPlanQty;
            }
            return RemainQty;
        }

        public int GetRemainQty(int Prod_ReferenceFK, int OrderColorAndSizeRatioFk, int PreviousSectionId, int CurrentSectionId)
        {
            int OriginalQty = 0;
            int TotalPlanQty = 0;
            int TotalDoneQty = 0;
            int RemainQty = 0;
            
            var vDoneCuttingData = (from o in db.Prod_PlanReferenceOrder
                                    join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                                    where p.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && p.Plan_ReferenceSectionFk == PreviousSectionId
                                    select new
                                    {
                                        p
                                    }).ToList();
            if (vDoneCuttingData.Any())
            {
                OriginalQty = (int)vDoneCuttingData.Sum(a => a.p.Quantity);
            }

            var vOrderPlanData = db.Prod_PlanReferenceOrder.Where(a => a.Prod_ReferenceFK == Prod_ReferenceFK && a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk).ToList();
            if (vOrderPlanData.Any())
            {
                if (CurrentSectionId == 1)
                {
                    TotalPlanQty = (int)vOrderPlanData.Sum(a => a.CuttingPlanQuantity);
                }
                else if (CurrentSectionId == 2)
                {
                    TotalPlanQty = (int)vOrderPlanData.Sum(a => a.SewingPlanQuantity);
                }
                else if (CurrentSectionId == 3)
                {
                    TotalPlanQty = (int)vOrderPlanData.Sum(a => a.IronPlanQuantity);
                }
                else if (CurrentSectionId == 4)
                {
                    TotalPlanQty = (int)vOrderPlanData.Sum(a => a.PackingPlanQuantity);
                }
            }

            var vOrderDoneData = (from o in db.Prod_PlanReferenceOrder
                                  join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                                  where p.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && p.Plan_ReferenceSectionFk == CurrentSectionId
                                  select new
                                  {
                                      p
                                  }).ToList();

            //var vOrderDoneData = db.Prod_PlanReferenceOrderSection.Where(a => a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && a.Plan_ReferenceSectionFk == CurrentSectionId).ToList();
            if (vOrderPlanData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.p.Quantity);
            }

            if (TotalDoneQty > 0)
            {
                RemainQty = OriginalQty - TotalDoneQty;
            }
            else if (TotalDoneQty == 0)
            {
                RemainQty = OriginalQty - TotalPlanQty;
            }
            return RemainQty;
        }

        public int GetSectionDoneQty(int OrderColorAndSizeRatioFk, int SectionId)
        {
            int TotalQty = 0;

            var vData = (from o in db.Prod_PlanReferenceOrder
                                  join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                                  where p.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && p.Plan_ReferenceSectionFk == SectionId
                                  select new
                                  {
                                      p
                                  }).ToList();
            
            //var vData = db.Prod_PlanReferenceOrderSection.Where(a => a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && a.Plan_ReferenceSectionFk == SectionId).ToList();
            if (vData.Any())
            {
                TotalQty = (int)vData.Sum(a => a.p.Quantity);
            }

            return TotalQty;
        }
        
        public int GetSewingPlanQty(int Id, int OrderColorAndSizeRatioFk)
        {
            int TotalCuttingQty = 0;
            int TotalPlanQty = 0;

            var vDoneCuttingData = db.Prod_PlanReferenceOrderSection.Where(a => a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && a.Plan_ReferenceSectionFk == 1).ToList();
            if (vDoneCuttingData.Any())
            {
                TotalCuttingQty = (int)vDoneCuttingData.Sum(a => a.Quantity);
            }


            var vOrderPlanData = db.Prod_PlanReferenceOrder.Where(a => a.Prod_ReferenceFK == Id && a.Mkt_OrderColorAndSizeRatioFk== OrderColorAndSizeRatioFk).ToList();
            if (vOrderPlanData.Any())
            {
                TotalPlanQty = (int)vOrderPlanData.Sum(a => a.SewingPlanQuantity);
            }
            
            return TotalCuttingQty - TotalPlanQty;
        }

        public int GetIronPlanQty(int Id, int OrderColorAndSizeRatioFk)
        {
            int TotalSewingQty = 0;
            int TotalPlanQty = 0;

            var vDoneData = db.Prod_PlanReferenceOrderSection.Where(a => a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && a.Plan_ReferenceSectionFk == 2).ToList();
            if (vDoneData.Any())
            {
                TotalSewingQty = (int)vDoneData.Sum(a => a.Quantity);
            }
            
            var vOrderData = db.Prod_PlanReferenceOrder.Where(a => a.Prod_ReferenceFK == Id && a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk).ToList();
            if (vOrderData.Any())
            {
                TotalPlanQty = (int)vOrderData.Sum(a => a.IronPlanQuantity);
            }

            return TotalSewingQty - TotalPlanQty;
        }

        public int GetPackingPlanQty(int Id, int OrderColorAndSizeRatioFk)
        {
            int TotalIronQty = 0;
            int TotalPlanQty = 0;

            var vDoneData = db.Prod_PlanReferenceOrderSection.Where(a => a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && a.Plan_ReferenceSectionFk == 3).ToList();
            if (vDoneData.Any())
            {
                TotalIronQty = (int)vDoneData.Sum(a => a.Quantity);
            }


            var vOrderData = db.Prod_PlanReferenceOrder.Where(a => a.Prod_ReferenceFK == Id && a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk).ToList();
            if (vOrderData.Any())
            {
                TotalPlanQty = (int)vOrderData.Sum(a => a.PackingPlanQuantity);
            }

            return TotalIronQty - TotalPlanQty;
        }
        
        public void SaveCuttingPlan(ProductionDailyPlan model)
        {
            var vGetData = db.Prod_PlanReferenceOrder.Where(a => a.ID == model.Prod_PlanReferenceOrderFk);
            if (vGetData.Any())
            {
                GetLineCheif((int)model.Plan_ProductionLineFk);
                var update = vGetData.FirstOrDefault();
                update.CuttingPlanQuantity = model.CuttingPlanQuantity;
                update.Prod_LineChiefFk = model.Prod_LineChiefFk;
                update.Prod_SVNameFk = model.Prod_SVNameFk;
                update.Plan_ProductionLineFk = (int)model.Plan_ProductionLineFk;
                update.Prod_LineChiefFk = this.LineChiefFk;
                update.Prod_SVNameFk = this.LineSupervisorFk;
                update.Edit(model.UserID);
                this.Status = 1;
            }
            else
            {
                this.Status = -1;
            }
        }

        public void SaveSewingPlan(ProductionDailyPlan model)
        {
            var vGetData = db.Prod_PlanReferenceOrder.Where(a => a.ID == model.Prod_PlanReferenceOrderFk);
            if (vGetData.Any())
            {
                //GetLineCheif((int)model.Plan_ProductionLineFk);
                var update = vGetData.FirstOrDefault();
                update.SewingPlanQuantity = model.SewingPlanQuantity;
                update.Edit(model.UserID);


                //Prod_PlanReferenceOrder Prod_PlanReferenceOrderCutting = new Prod_PlanReferenceOrder();
                //Prod_PlanReferenceOrderCutting.OrderDeliveryScheduleFk = update.OrderDeliveryScheduleFk;
                //Prod_PlanReferenceOrderCutting.Mkt_OrderColorAndSizeRatioFk = update.Mkt_OrderColorAndSizeRatioFk;
                //Prod_PlanReferenceOrderCutting.Prod_ReferenceFK = update.Prod_ReferenceFK;
                //Prod_PlanReferenceOrderCutting.Quantity = (int)update.Quantity;
                //Prod_PlanReferenceOrderCutting.CuttingPlanQuantity = update.SewingPlanQuantity;
                //Prod_PlanReferenceOrderCutting.CuttingExtra = 2;
                
                //Prod_PlanReferenceOrder.Layer = update.Layer;
                //Prod_PlanReferenceOrder.PerLayerWeight = model.Prod_PlanReferenceOrder.PerLayerWeight;
                //Prod_PlanReferenceOrder.MarkerPiece = model.Prod_PlanReferenceOrder.MarkerPiece;
                //Prod_PlanReferenceOrder.MarkerLength = model.Prod_PlanReferenceOrder.MarkerLength;
                //Prod_PlanReferenceOrder.MarkerWidth = model.Prod_PlanReferenceOrder.MarkerWidth;
                //Prod_PlanReferenceOrderCutting.SectionId = 1;
                //Prod_PlanReferenceOrderCutting.AddReady(model.UserID);
                //Prod_PlanReferenceOrderCutting.Add();

                //Prod_PlanReferenceOrder Prod_PlanReferenceOrderIron = new Prod_PlanReferenceOrder();
                //Prod_PlanReferenceOrderIron.OrderDeliveryScheduleFk = update.OrderDeliveryScheduleFk;
                //Prod_PlanReferenceOrderIron.Mkt_OrderColorAndSizeRatioFk = update.Mkt_OrderColorAndSizeRatioFk;
                //Prod_PlanReferenceOrderIron.Prod_ReferenceFK = update.Prod_ReferenceFK;
                //Prod_PlanReferenceOrderIron.Quantity = (int)update.Quantity;
                //Prod_PlanReferenceOrderIron.IronPlanQuantity = update.SewingPlanQuantity;
                //Prod_PlanReferenceOrderIron.SectionId = 3;
                //Prod_PlanReferenceOrderIron.AddReady(model.UserID);
                //Prod_PlanReferenceOrderIron.Add();

                //Prod_PlanReferenceOrder Prod_PlanReferenceOrderPack = new Prod_PlanReferenceOrder();
                //Prod_PlanReferenceOrderPack.OrderDeliveryScheduleFk = update.OrderDeliveryScheduleFk;
                //Prod_PlanReferenceOrderPack.Mkt_OrderColorAndSizeRatioFk = update.Mkt_OrderColorAndSizeRatioFk;
                //Prod_PlanReferenceOrderPack.Prod_ReferenceFK = update.Prod_ReferenceFK;
                //Prod_PlanReferenceOrderPack.Quantity = (int)update.Quantity;
                //Prod_PlanReferenceOrderPack.IronPlanQuantity = update.SewingPlanQuantity;
                //Prod_PlanReferenceOrderPack.SectionId = 4;
                //Prod_PlanReferenceOrderPack.AddReady(model.UserID);
                //Prod_PlanReferenceOrderPack.Add();

                this.Status = 1;
                //this.UpdateRemainQty = GetRemainQty(update.Prod_ReferenceFK, update.Mkt_OrderColorAndSizeRatioFk, 1, 2);
            }
            else
            {
                this.Status = -1;
            }

            

        }

        public void SaveIronPlan(ProductionDailyPlan model)
        {
            var vGetData = db.Prod_PlanReferenceOrder.Where(a => a.ID == model.Prod_PlanReferenceOrderFk);
            if (vGetData.Any())
            {
                //GetLineCheif((int)model.Plan_ProductionLineFk);
                var update = vGetData.FirstOrDefault();
                update.IronPlanQuantity = model.IronPlanQuantity;
                update.Edit(model.UserID);
                this.Status = 1;
                this.UpdateRemainQty = GetRemainQty(update.Prod_ReferenceFK, (int)update.Mkt_OrderColorAndSizeRatioFk, 2,3);  //GetIronPlanQty(update.Prod_ReferenceFK, update.Mkt_OrderColorAndSizeRatioFk);
            }
            else
            {
                this.Status = -1;
            }
        }

        public void SavePackingPlan(ProductionDailyPlan model)
        {
            var vGetData = db.Prod_PlanReferenceOrder.Where(a => a.ID == model.Prod_PlanReferenceOrderFk);
            if (vGetData.Any())
            {
                //GetLineCheif((int)model.Plan_ProductionLineFk);
                var update = vGetData.FirstOrDefault();
                update.PackingPlanQuantity = model.PackingPlanQuantity;
                update.Edit(model.UserID);
                this.Status = 1;
                this.UpdateRemainQty = GetRemainQty(update.Prod_ReferenceFK, (int)update.Mkt_OrderColorAndSizeRatioFk, 3,4);  //GetPackingPlanQty(update.Prod_ReferenceFK, update.Mkt_OrderColorAndSizeRatioFk);
            }
            else
            {
                this.Status = -1;
            }
        }

        public void GetLineCheif(int id)
        {
            if (id > 0)
            {
                this.LineName = db.Plan_ProductionLine.Where(a => a.ID == id && a.Active == true).FirstOrDefault().Name;

                var vData = (from o in db.Prod_LineOfficer
                             join p in db.Prod_LineChief on o.Prod_LineChiefFK equals p.ID
                             join q in db.Prod_SVName on o.Prod_LineSuperVisorFk equals q.ID
                             where o.Plan_ProductionLineFK == id && o.Active == true
                             select new
                             {
                                 CheifId = p.ID,
                                 CheifName = p.Name,
                                 SVID = q.ID,
                                 SVName = q.Name
                             }).ToList();
                if (vData.Any())
                {
                    var takeSingle = vData.FirstOrDefault();
                    this.LineChiefFk = takeSingle.CheifId;
                    this.CheifSuperVisorName = takeSingle.CheifName + " / ";
                    this.LineSupervisorFk = takeSingle.SVID;
                    this.CheifSuperVisorName += takeSingle.SVName;
                }
            }    
        }

        public void DeleteZeroPlanOrder(int Id, int SectionId)
        {
            try
            {
                db = new xOssContext();
                var vData = db.Prod_PlanReferenceOrder.Where(v => v.Prod_ReferenceFK == Id && v.CuttingPlanQuantity == 0 && v.SectionId==SectionId).ToList();
                if (vData.Any())
                {
                    db.Prod_PlanReferenceOrder.RemoveRange(vData);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string smg = ex.Message;
            }
        }

        public void AddOrderForCutting(VmProd_PlanReferenceOrder model)
        {
            DeleteZeroPlanOrder(model.Prod_PlanReferenceOrder.Prod_ReferenceFK,1);
            var v1 = (from t1 in db.Mkt_OrderDeliverySchedule
                      join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                      where t1.Mkt_BOMFk == model.BOMID && t1.Active == true && t2.Active==true
                      select new { t1, t2 }).ToList();

            //var getPlanRefCuttingData = db.Prod_PlanReferenceOrder.Where(a => a.Prod_ReferenceFK == model.Prod_PlanReferenceOrder.Prod_ReferenceFK && a.CuttingPlanQuantity != 0).ToList();

            if (v1.Any())
            {
                foreach (var v in v1)
                {
                    Prod_PlanReferenceOrder Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
                    Prod_PlanReferenceOrder.MktBomFk = model.BOMID;
                    Prod_PlanReferenceOrder.OrderDeliveryScheduleFk = v.t1.ID;
                    Prod_PlanReferenceOrder.Mkt_OrderColorAndSizeRatioFk = v.t2.ID;
                    Prod_PlanReferenceOrder.Prod_ReferenceFK = model.Prod_PlanReferenceOrder.Prod_ReferenceFK;
                    Prod_PlanReferenceOrder.Quantity = (int)v.t2.Quantity;
                    Prod_PlanReferenceOrder.CuttingExtra = model.Prod_PlanReferenceOrder.CuttingExtra;
                    Prod_PlanReferenceOrder.GSM = model.Prod_PlanReferenceOrder.GSM;
                    Prod_PlanReferenceOrder.Layer = model.Prod_PlanReferenceOrder.Layer;
                    Prod_PlanReferenceOrder.PerLayerWeight = model.Prod_PlanReferenceOrder.PerLayerWeight;
                    Prod_PlanReferenceOrder.MarkerPiece = model.Prod_PlanReferenceOrder.MarkerPiece;
                    Prod_PlanReferenceOrder.MarkerLength = model.Prod_PlanReferenceOrder.MarkerLength;
                    Prod_PlanReferenceOrder.MarkerWidth = model.Prod_PlanReferenceOrder.MarkerWidth;
                    Prod_PlanReferenceOrder.SectionId = 1;
                    Prod_PlanReferenceOrder.AddReady(model.UserId);
                    Prod_PlanReferenceOrder.Add();
                }
            }
        }

        //public void AddOrderForSewing(ProductionDailyPlan model)
        //{
        //    DeleteZeroPlanOrder(model.Prod_ReferenceFk,2);

        //    var vAddOrderData = (from t1 in db.Mkt_OrderDeliverySchedule
        //                         join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
        //                         where t1.Mkt_BOMFk == model.BOMFk && t1.Active == true && t2.Active == true
        //                         select new { t1, t2 }).ToList();
            
        //    var vCuttingData = (from t1 in db.Prod_PlanReferenceOrder
        //                   join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
        //                   where t2.Mkt_BOMFk == model.BOMFk && t2.Active == true && t1.CuttingPlanQuantity!=0
        //                   select new { t1, t2 }).ToList();

        //    //var getPlanRefSewingData = db.Prod_PlanReferenceOrder.Where(a=>a.Prod_ReferenceFK==RefID && a.SewingPlanQuantity!=0).ToList();

        //    if (vAddOrderData.Any())
        //    {
        //        foreach (var v in vAddOrderData)
        //        {
        //            //if (vCuttingData.Any(a=>a.t1.OrderDeliveryScheduleFk==v.t1.ID && a.t1.Mkt_OrderColorAndSizeRatioFk==v.t2.ID)) // && !getPlanRefSewingData.Any(a=> a.OrderDeliveryScheduleFk == v.t1.ID && a.Mkt_OrderColorAndSizeRatioFk == v.t2.ID))
        //            //{

        //            //}
        //            Prod_PlanReferenceOrder Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
        //            Prod_PlanReferenceOrder.OrderDeliveryScheduleFk = v.t1.ID;
        //            Prod_PlanReferenceOrder.Mkt_OrderColorAndSizeRatioFk = v.t2.ID;
        //            Prod_PlanReferenceOrder.Prod_ReferenceFK = model.Prod_ReferenceFk;
        //            Prod_PlanReferenceOrder.Quantity = (int)v.t1.Quantity;
        //            Prod_PlanReferenceOrder.SMV = model.SMV;
        //            Prod_PlanReferenceOrder.WorkingHour = model.WorkingHour;
        //            Prod_PlanReferenceOrder.MachineRunning = model.MachineRunning;
        //            Prod_PlanReferenceOrder.PresentOperator = model.PresentOpt;
        //            Prod_PlanReferenceOrder.PresentHelper = model.NoOfHelper;
        //            Prod_PlanReferenceOrder.Plan_ProductionLineFk = (int)model.Plan_ProductionLineFk;
        //            Prod_PlanReferenceOrder.Prod_LineChiefFk = model.Prod_LineChiefFk;
        //            Prod_PlanReferenceOrder.Prod_SVNameFk = model.Prod_SVNameFk;
        //            Prod_PlanReferenceOrder.SectionId = 2;
        //            Prod_PlanReferenceOrder.Active = true;

        //            Prod_PlanReferenceOrder.AddReady(model.UserID);
        //            Prod_PlanReferenceOrder.Add();
        //        }
        //    }
            
        //}
        
        public void AddOrderForSewingFollowup(ProductionDailyPlan model)
        {
            //DeleteZeroPlanOrder(model.Prod_ReferenceFk, 2);
            
            var vAddOrderData = (from t1 in db.Mkt_OrderDeliverySchedule
                                 join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                                 where t1.Mkt_BOMFk == model.BOMFk && t2.Color.Equals(model.Color) && t1.Active == true && t2.Active == true
                                 select new { t1, t2 }).ToList();
            
            if (vAddOrderData.Any())
            {
                foreach (var v in vAddOrderData)
                {
                    Prod_PlanReferenceOrderSection Prod_PlanReferenceOrderSection = new Prod_PlanReferenceOrderSection();
                    Prod_PlanReferenceOrderSection.Prod_PlanReferenceOrderFk = model.Prod_PlanReferenceOrderFk;
                    Prod_PlanReferenceOrderSection.OrderDeliveryScheduleFk = v.t1.ID;
                    Prod_PlanReferenceOrderSection.Mkt_OrderColorAndSizeRatioFk = v.t2.ID;
                    Prod_PlanReferenceOrderSection.Plan_ReferenceSectionFk = 2;
                    Prod_PlanReferenceOrderSection.Active = true;
                    Prod_PlanReferenceOrderSection.AddReady(model.UserID);
                    Prod_PlanReferenceOrderSection.Add();
                }
            }
        }

        public void AddSewingLineOrder(ProductionDailyPlan model)
        {
            var vAddOrderData = (from t1 in db.Mkt_OrderDeliverySchedule
                                 join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                                 where t1.Mkt_BOMFk == model.BOMFk && t1.Active == true && t2.Active == true
                                 select new { t1, t2 }).ToList();

            if (vAddOrderData.Any())
            {
                Prod_PlanReferenceOrder Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
                Prod_PlanReferenceOrder.Prod_ReferenceFK = model.Prod_ReferenceFk;
                Prod_PlanReferenceOrder.MktBomFk = model.BOMFk;
                Prod_PlanReferenceOrder.Quantity = model.PerHourTarget;
                Prod_PlanReferenceOrder.SewingPlanQuantity = (int)model.PerHourTarget * model.WorkingHour;
                Prod_PlanReferenceOrder.SMV = model.SMV;
                Prod_PlanReferenceOrder.WorkingHour = model.WorkingHour;
                Prod_PlanReferenceOrder.MachineRunning = model.MachineRunning;
                Prod_PlanReferenceOrder.PresentOperator = model.PresentOpt;
                Prod_PlanReferenceOrder.PresentHelper = model.NoOfHelper;
                Prod_PlanReferenceOrder.Plan_ProductionLineFk = (int)model.Plan_ProductionLineFk;
                Prod_PlanReferenceOrder.Prod_LineChiefFk = model.Prod_LineChiefFk;
                Prod_PlanReferenceOrder.Prod_SVNameFk = model.Prod_SVNameFk;
                Prod_PlanReferenceOrder.SectionId = 2;
                Prod_PlanReferenceOrder.Active = true;

                Prod_PlanReferenceOrder.AddReady(model.UserID);
                Prod_PlanReferenceOrder.Add();
            }
        }

        private DateTime GetPreviousReferenceDate(DateTime RefDate)
        {
            var prePlan = db.Prod_Reference.Where(a=>a.FromDate== RefDate);
            if (prePlan.Any())
            {
                return prePlan.FirstOrDefault().FromDate;
            }
            else
            {
                return GetPreviousReferenceDate(RefDate.AddDays(-1));
            }
        }

        public void AddPreviousSewingPlanOrder(int Id)
        {
            var vData = db.Prod_Reference.Where(a=>a.ID==Id);
            if (vData.Any())
            {
                var take = vData.FirstOrDefault();
                DateTime dt = GetPreviousReferenceDate(take.FromDate.AddDays(-1));
                var getPlan = (from t1 in db.Prod_Reference
                               join t2 in db.Prod_PlanReferenceOrder on t1.ID equals t2.Prod_ReferenceFK
                               where t1.FromDate == dt && t2.Active == true && t2.SectionId == 2
                               select new { t1,t2 }).ToList();
                if (getPlan.Any())
                {
                    foreach (var v in getPlan)
                    {
                        Prod_PlanReferenceOrder Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
                        Prod_PlanReferenceOrder.Prod_ReferenceFK = Id;
                        Prod_PlanReferenceOrder.MktBomFk = v.t2.MktBomFk;
                        Prod_PlanReferenceOrder.Quantity = v.t2.Quantity;
                        Prod_PlanReferenceOrder.SewingPlanQuantity = v.t2.SewingPlanQuantity;
                        Prod_PlanReferenceOrder.SMV = v.t2.SMV;
                        Prod_PlanReferenceOrder.WorkingHour = v.t2.WorkingHour;
                        Prod_PlanReferenceOrder.MachineRunning = v.t2.MachineRunning;
                        Prod_PlanReferenceOrder.PresentOperator = v.t2.PresentOperator;
                        Prod_PlanReferenceOrder.PresentHelper = v.t2.PresentHelper;
                        Prod_PlanReferenceOrder.Plan_ProductionLineFk = (int)v.t2.Plan_ProductionLineFk;
                        Prod_PlanReferenceOrder.Prod_LineChiefFk = v.t2.Prod_LineChiefFk;
                        Prod_PlanReferenceOrder.Prod_SVNameFk = v.t2.Prod_SVNameFk;
                        Prod_PlanReferenceOrder.SectionId = 2;
                        Prod_PlanReferenceOrder.Active = true;

                        Prod_PlanReferenceOrder.AddReady(v.t2.FirstCreatedBy);
                        Prod_PlanReferenceOrder.Add();
                    }
                }
            }
        }

        //public void AddOrderForIronning(int BomId, int RefID)
        //{
        //    DeleteZeroPlanOrder(RefID,3);
        //    var vAddOrderData = (from t1 in db.Mkt_OrderDeliverySchedule
        //                         join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
        //                         where t1.Mkt_BOMFk == BomId && t1.Active == true && t2.Active == true
        //                         select new { t1, t2 }).ToList();

        //    var vSewingData = (from t1 in db.Prod_PlanReferenceOrder
        //                        join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
        //                        where t2.Mkt_BOMFk == BomId && t2.Active == true && t1.SewingPlanQuantity != 0
        //                        select new
        //                        {
        //                            t1,
        //                            t2
        //                        }).ToList();

        //    //var getPlanRefIronData = db.Prod_PlanReferenceOrder.Where(a => a.Prod_ReferenceFK == RefID && a.IronPlanQuantity != 0).ToList();

        //    if (vAddOrderData.Any())
        //    {
        //        foreach (var v in vAddOrderData)
        //        {
        //            //if (vSewingData.Any(a => a.t1.OrderDeliveryScheduleFk == v.t1.ID && a.t1.Mkt_OrderColorAndSizeRatioFk == v.t2.ID)) //&& !getPlanRefIronData.Any(a => a.OrderDeliveryScheduleFk == v.t1.ID && a.Mkt_OrderColorAndSizeRatioFk == v.t2.ID))
        //            //{

        //            //}
        //            Prod_PlanReferenceOrder Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
        //            Prod_PlanReferenceOrder.OrderDeliveryScheduleFk = v.t1.ID;
        //            Prod_PlanReferenceOrder.Mkt_OrderColorAndSizeRatioFk = v.t2.ID;
        //            Prod_PlanReferenceOrder.Prod_ReferenceFK = RefID;
        //            Prod_PlanReferenceOrder.Quantity = (int)v.t1.Quantity;
        //            Prod_PlanReferenceOrder.SectionId = 3;
        //            Prod_PlanReferenceOrder.Active = true;

        //            Prod_PlanReferenceOrder.AddReady(1);
        //            Prod_PlanReferenceOrder.Add();
        //        }
        //    }
        //}

        public void AddOrderForIronPlan(ProductionDailyPlan model)
        {
            var vAddOrderData = (from t1 in db.Mkt_OrderDeliverySchedule
                                 join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                                 where t1.Mkt_BOMFk == model.BOMFk && t1.Active == true && t2.Active == true
                                 select new { t1, t2 }).ToList();
            
            if (vAddOrderData.Any())
            {
                Prod_PlanReferenceOrder Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
                Prod_PlanReferenceOrder.MktBomFk = model.BOMFk;
                Prod_PlanReferenceOrder.Prod_ReferenceFK = model.Prod_ReferenceFk;
                Prod_PlanReferenceOrder.SectionId = 3;
                Prod_PlanReferenceOrder.Active = true;

                Prod_PlanReferenceOrder.AddReady(model.UserID);
                Prod_PlanReferenceOrder.Add();
            }
        }

        public void AddOrderForIronFollowup(ProductionDailyPlan model)
        {
            var vAddOrderData = (from t1 in db.Mkt_OrderDeliverySchedule
                                 join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                                 where t1.Mkt_BOMFk == model.BOMFk && t2.Color.Equals(model.Color) && t1.Active == true && t2.Active == true
                                 select new { t1, t2 }).ToList();

            if (vAddOrderData.Any())
            {
                var GetPlanRefOrderId = db.Prod_PlanReferenceOrder.FirstOrDefault(a => a.MktBomFk == model.BOMFk && a.Prod_ReferenceFK == model.Prod_ReferenceFk && a.SectionId == 3);
                model.Prod_PlanReferenceOrderFk = GetPlanRefOrderId.ID;
                foreach (var v in vAddOrderData)
                {
                    Prod_PlanReferenceOrderSection Prod_PlanReferenceOrderSection = new Prod_PlanReferenceOrderSection();
                    Prod_PlanReferenceOrderSection.Prod_PlanReferenceOrderFk = model.Prod_PlanReferenceOrderFk;
                    Prod_PlanReferenceOrderSection.OrderDeliveryScheduleFk = v.t1.ID;
                    Prod_PlanReferenceOrderSection.Mkt_OrderColorAndSizeRatioFk = v.t2.ID;
                    Prod_PlanReferenceOrderSection.Plan_ReferenceSectionFk = 3;
                    Prod_PlanReferenceOrderSection.Active = true;
                    Prod_PlanReferenceOrderSection.AddReady(model.UserID);
                    Prod_PlanReferenceOrderSection.Add();
                }
            }
        }

        //public void AddOrderForPacking(int BomId, int RefID)
        //{
        //    DeleteZeroPlanOrder(RefID,4);
        //    var vAddOrderData = (from t1 in db.Mkt_OrderDeliverySchedule
        //                         join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
        //                         //join t3 in db.Prod_SMVLayout on t1.Mkt_BOMFk equals t3.Mkt_BOMFk
        //                         where t1.Mkt_BOMFk == BomId && t1.Active == true && t2.Active == true
        //                         select new
        //                         {
        //                             t1,t2 //t3
        //                         }).ToList();

        //    var vIroningData = (from t1 in db.Prod_PlanReferenceOrder
        //                       join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
        //                       where t2.Mkt_BOMFk == BomId && t2.Active == true && t1.IronPlanQuantity != 0
        //                       select new
        //                       { t1, t2 }).ToList();

        //    var getPlanRefIronData = db.Prod_PlanReferenceOrder.Where(a => a.Prod_ReferenceFK == RefID && a.PackingPlanQuantity != 0).ToList();

        //    if (vAddOrderData.Any())
        //    {
        //        foreach (var v in vAddOrderData)
        //        {
        //            //if (vIroningData.Any(a => a.t1.OrderDeliveryScheduleFk == v.t1.ID && a.t1.Mkt_OrderColorAndSizeRatioFk == v.t2.ID)) //&& !getPlanRefIronData.Any(a => a.OrderDeliveryScheduleFk == v.t1.ID && a.Mkt_OrderColorAndSizeRatioFk == v.t2.ID))
        //            //{

        //            //}
        //            Prod_PlanReferenceOrder Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
        //            Prod_PlanReferenceOrder.OrderDeliveryScheduleFk = v.t1.ID;
        //            Prod_PlanReferenceOrder.Mkt_OrderColorAndSizeRatioFk = v.t2.ID;
        //            Prod_PlanReferenceOrder.Prod_ReferenceFK = RefID;
        //            Prod_PlanReferenceOrder.Quantity = (int)v.t1.Quantity;
        //            Prod_PlanReferenceOrder.SectionId = 4;
        //            Prod_PlanReferenceOrder.Active = true;

        //            Prod_PlanReferenceOrder.AddReady(1);
        //            Prod_PlanReferenceOrder.Add();
        //        }
        //    }
        //}

        public void AddOrderForPackingPlan(ProductionDailyPlan model)
        {
            var vAddOrderData = (from t1 in db.Mkt_OrderDeliverySchedule
                                 join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                                 where t1.Mkt_BOMFk == model.BOMFk && t1.Active == true && t2.Active == true
                                 select new { t1, t2 }).ToList();

            if (vAddOrderData.Any())
            {
                Prod_PlanReferenceOrder Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
                Prod_PlanReferenceOrder.MktBomFk = model.BOMFk;
                Prod_PlanReferenceOrder.Prod_ReferenceFK = model.Prod_ReferenceFk;
                Prod_PlanReferenceOrder.SectionId = 4;
                Prod_PlanReferenceOrder.Active = true;
                Prod_PlanReferenceOrder.AddReady(model.UserID);
                Prod_PlanReferenceOrder.Add();
            }
        }

        public void AddOrderForPackingFollowup(ProductionDailyPlan model)
        {
            var vAddOrderData = (from t1 in db.Mkt_OrderDeliverySchedule
                                 join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                                 where t1.Mkt_BOMFk == model.BOMFk && t2.Color.Equals(model.Color) && t1.Active == true && t2.Active == true
                                 select new { t1, t2 }).ToList();

            if (vAddOrderData.Any())
            {
                var GetPlanRefOrderId = db.Prod_PlanReferenceOrder.FirstOrDefault(a => a.MktBomFk == model.BOMFk && a.Prod_ReferenceFK == model.Prod_ReferenceFk && a.SectionId == 4);
                model.Prod_PlanReferenceOrderFk = GetPlanRefOrderId.ID;
                foreach (var v in vAddOrderData)
                {
                    Prod_PlanReferenceOrderSection Prod_PlanReferenceOrderSection = new Prod_PlanReferenceOrderSection();
                    Prod_PlanReferenceOrderSection.Prod_PlanReferenceOrderFk = model.Prod_PlanReferenceOrderFk;
                    Prod_PlanReferenceOrderSection.OrderDeliveryScheduleFk = v.t1.ID;
                    Prod_PlanReferenceOrderSection.Mkt_OrderColorAndSizeRatioFk = v.t2.ID;
                    Prod_PlanReferenceOrderSection.Plan_ReferenceSectionFk = 4;
                    Prod_PlanReferenceOrderSection.Active = true;
                    Prod_PlanReferenceOrderSection.AddReady(model.UserID);
                    Prod_PlanReferenceOrderSection.Add();
                }
            }
        }

        public int GetRemainPlanQty(int Prod_ReferenceFK, int OrderColorAndSizeRatioFk, int SectionId)
        {
            db = new xOssContext();
            
            int TodayRemainQty = 0;
            int TotalPlanQty = 0;
            int TotalDoneQty = 0;
            int TotalOrderQty = 0;

            var takedate = db.Prod_Reference.Where(a => a.ID == Prod_ReferenceFK).FirstOrDefault();
            DateTime FDate = takedate.FromDate;
            //DateTime TDate = takedate.ToDate.AddDays(-1);
            var vOrderPlanData = (from o in db.Prod_Reference
                                  join p in db.Prod_PlanReferenceOrder on o.ID equals p.Prod_ReferenceFK
                                  where o.FromDate <= FDate
                                  && o.ToDate <= FDate
                                  && p.SectionId == SectionId
                                  && p.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk
                                  select new { p }).ToList();

            if (vOrderPlanData.Any())
            {
                if (SectionId == 1)
                {
                    TotalPlanQty = (int)vOrderPlanData.Sum(a => a.p.CuttingPlanQuantity);
                }
                else if (SectionId == 2)
                {
                    TotalPlanQty = (int)vOrderPlanData.Sum(a => a.p.SewingPlanQuantity);
                }
                else if (SectionId == 3)
                {
                    TotalPlanQty = (int)vOrderPlanData.Sum(a => a.p.IronPlanQuantity);
                }
                else if (SectionId == 4)
                {
                    TotalPlanQty = (int)vOrderPlanData.Sum(a => a.p.PackingPlanQuantity);
                }
            }

            var vOrderDoneData = (from o in db.Prod_Reference
                                  join p in db.Prod_PlanReferenceOrder on o.ID equals p.Prod_ReferenceFK
                                  join q in db.Prod_PlanReferenceOrderSection on p.ID equals q.Prod_PlanReferenceOrderFk
                                  where o.FromDate <= FDate && o.ToDate <= FDate
                                  && q.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk
                                  && q.Plan_ReferenceSectionFk == SectionId
                                  select new { q }).ToList();
            if (vOrderDoneData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.q.Quantity);
            }

            TotalOrderQty = (int)db.Mkt_OrderColorAndSizeRatio.Where(a => a.ID == OrderColorAndSizeRatioFk).FirstOrDefault().Quantity;

            if (TotalPlanQty > 0)
            {
                TodayRemainQty = (TotalOrderQty - TotalPlanQty) + (TotalPlanQty - TotalDoneQty);
            }
            else
            {
                TodayRemainQty = (int)db.Mkt_OrderColorAndSizeRatio.Where(a => a.ID == OrderColorAndSizeRatioFk).FirstOrDefault().Quantity;
            }

            return TodayRemainQty;
        }
        
        public bool ProductionProcess(int Id)
        {
            db = new xOssContext();
            var vData = db.Prod_PlanReferenceOrder.Any(a => a.Prod_ReferenceFK == Id);

            var flag =  db.Prod_Reference.Where(x => x.FromDate >= DateTime.Today && x.ToDate <= DateTime.Today && x.ID == Id).FirstOrDefault() == null ? false : true;
            if (!db.Prod_PlanReferenceOrder.Any(a => a.Prod_ReferenceFK == Id) && flag)
            {
                //this.IsProductionStart= true;
                return true;
                //Check what is planned before but not done for cutting and sewing
                // insert those left / balance as planned quantity for today
            }
            return false;
        }

        public VmProd_PlanReferenceOrder SelectSingleReference(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Reference
                     where t1.Active == true
                     && t1.ID == id
                     select new VmProd_PlanReferenceOrder
                     {
                         Prod_Reference = t1
                     }).FirstOrDefault();
            v.IsProductionStart= ProductionProcess(id);
            return v;
        }

        public void GetPlanFollowUpDetails(int Id, int sectionId)
        {
            if (sectionId == 1)
            {
                var vCuttindData = (from t1 in db.Prod_PlanReferenceOrderSection
                                    join t2 in db.Prod_PlanReferenceOrder on t1.Prod_PlanReferenceOrderFk equals t2.ID
                                    join t3 in db.Mkt_OrderDeliverySchedule on t2.OrderDeliveryScheduleFk equals t3.ID
                                    join t4 in db.Mkt_OrderColorAndSizeRatio on t2.Mkt_OrderColorAndSizeRatioFk equals t4.ID
                                    join t5 in db.Common_Country on t3.Destination equals t5.ID
                                    join t6 in db.Common_TheOrder on t3.Common_TheOrderFk equals t6.ID
                                    join t7 in db.User_User on t1.FirstCreatedBy equals t7.ID
                                    where t1.Plan_ReferenceSectionFk == sectionId && t1.Prod_PlanReferenceOrderFk == Id
                                    select new ProductionDailyPlan
                                    {
                                        OrderNo = t6.CID,
                                        Destination = t5.Name,
                                        Color = t4.Color,
                                        Size = t4.Size,
                                        OrderQty = (int)t4.Quantity,
                                        CuttingDoneQuantity = (int)t1.Quantity,
                                        EntryDate = (DateTime)t1.FirstCreated,
                                        CreateBy = t7.Name
                                    }).ToList();
                if (vCuttindData.Any())
                {
                    this.OrderName = vCuttindData.FirstOrDefault().OrderNo;
                    foreach (var v in vCuttindData)
                    {
                        v.FollowupTime = v.EntryDate.ToString("hh:mm tt");
                        v.FollowupDate = v.EntryDate.Day + "/" + v.EntryDate.Month + "/" + v.EntryDate.Year;
                    }
                }
                this.PlanDataList = vCuttindData;
            }
            else
            {
                var vData = (from t1 in db.Prod_PlanReferenceOrderSection
                             join t2 in db.Prod_PlanOrderSectionSlave on t1.ID equals t2.Prod_PlanReferenceOrderSectionFk
                             join t3 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t3.ID
                             join t4 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t4.ID
                             join t5 in db.Common_Country on t3.Destination equals t5.ID
                             join t6 in db.Common_TheOrder on t3.Common_TheOrderFk equals t6.ID
                             join t7 in db.User_User on t2.FirstCreatedBy equals t7.ID
                             where t2.Prod_PlanReferenceOrderSectionFk == Id
                             select new ProductionDailyPlan
                             {
                                 OrderNo = t6.CID,
                                 Destination = t5.Name,
                                 Color = t4.Color,
                                 Size = t4.Size,
                                 OrderQty = (int)t4.Quantity,
                                 CuttingDoneQuantity = (int)t2.Quantity,
                                 EntryDate = (DateTime)t2.FirstCreated,
                                 CreateBy = t7.Name
                             }).ToList();

                if (vData.Any())
                {
                    this.OrderName = vData.FirstOrDefault().OrderNo;
                    foreach (var v in vData)
                    {
                        v.FollowupTime = v.EntryDate.ToString("hh:mm tt");
                        v.FollowupDate = v.EntryDate.Day + "/" + v.EntryDate.Month + "/" + v.EntryDate.Year;
                    }
                }
                this.PlanDataList = vData;
            }
        }

        public int GetSizeColorTotalDoneQty(int BomId, int OrderColorAndSizeRatioFk, int SectionId)
        {
            db = new xOssContext();
            int DoneQty = 0;
            
            //var takedate = db.Prod_Reference.Where(a => a.ID == Prod_ReferenceFK).FirstOrDefault();
            //DateTime FDate = takedate.FromDate;
            var vOrderPlanData = (from o in db.Prod_Reference
                                  join p in db.Prod_PlanReferenceOrder on o.ID equals p.Prod_ReferenceFK
                                  join q in db.Prod_PlanReferenceOrderSection on p.ID equals q.Prod_PlanReferenceOrderFk
                                  where 
                                  //o.FromDate <= FDate && o.ToDate <= FDate && 
                                  p.MktBomFk==BomId &&
                                  p.SectionId == SectionId
                                  && q.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk
                                  && q.Plan_ReferenceSectionFk==SectionId
                                  select new { q.Quantity }).ToList();

            if (vOrderPlanData.Any())
            {
                DoneQty = (int)vOrderPlanData.Sum(a=>a.Quantity);
            }
            return DoneQty;
        }

        private int GetOrderTotalDoneQty(int BomId,int SectionId)
        {
            db = new xOssContext();
            int DoneQty = 0;
            if (SectionId == 1)
            {
                var vCuttindData = (from o in db.Prod_Reference
                                    join p in db.Prod_PlanReferenceOrder on o.ID equals p.Prod_ReferenceFK
                                    join q in db.Prod_PlanReferenceOrderSection on p.ID equals q.Prod_PlanReferenceOrderFk
                                    join r in db.Mkt_OrderDeliverySchedule on p.OrderDeliveryScheduleFk equals r.ID
                                    where r.Mkt_BOMFk == BomId && p.SectionId == SectionId && q.Plan_ReferenceSectionFk == SectionId
                                    select new { q.Quantity }).ToList();
                if (vCuttindData.Any())
                {
                    DoneQty = (int)vCuttindData.Sum(a => a.Quantity);
                }
            }
            else
            {
                var vOrderPlanData = (from o in db.Prod_Reference
                                      join p in db.Prod_PlanReferenceOrder on o.ID equals p.Prod_ReferenceFK
                                      join q in db.Prod_PlanReferenceOrderSection on p.ID equals q.Prod_PlanReferenceOrderFk
                                      where p.MktBomFk == BomId && p.SectionId == SectionId && q.Plan_ReferenceSectionFk == SectionId
                                      select new { q.Quantity }).ToList();
                if (vOrderPlanData.Any())
                {
                    DoneQty = (int)vOrderPlanData.Sum(a => a.Quantity);
                }
            }
            
            return DoneQty;
        }

        #region Multiple Search

        public void MultipleSearch(ProductionDailyPlan model)
        {
            if (String.IsNullOrEmpty(model.Size))
            {
                var search = (from t1 in db.Prod_PlanReferenceOrder
                              join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                              join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                              join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                              join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
                              join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
                              join t7 in db.Common_Country on t2.Destination equals t7.ID
                              where t1.Prod_ReferenceFK == model.Prod_ReferenceFk
                              && t1.Active == true
                              && t1.SewingPlanQuantity == 0
                              && t1.IronPlanQuantity == 0
                              && t1.PackingPlanQuantity == 0
                              && t1.SectionId == 1 && t6.Color.Equals(model.Color)
                              && t6.Size.Equals(model.Size)
                              && t4.ID == model.BOMFk
                              select new ProductionDailyPlan
                              {
                                  Prod_PlanReferenceOrderFk = t1.ID,
                                  OrderNo = t3.CID + Environment.NewLine + "/" + t4.Style,
                                  Prod_ReferenceFk = model.Prod_ReferenceFk,
                                  OrderDeliveryScheduleFk = t5.ID,
                                  Mkt_OrderColorAndSizeRatioFk = t6.ID,
                                  Port = t7.Name + "-" + t2.PortNo,
                                  Color = t6.Color,
                                  Size = t6.Size,
                                  OrderQty = (int)t6.Quantity,
                                  CuttingPlanQuantity = t1.CuttingPlanQuantity,
                                  CuttingExtra = t1.CuttingExtra,
                                  EntryDate = t2.Date,
                                  Plan_ProductionLineFk = t1.Plan_ProductionLineFk,
                              }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();
            }
            else if (String.IsNullOrEmpty(model.Color))
            {
                var search = (from t1 in db.Prod_PlanReferenceOrder
                              join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                              join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                              join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                              join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
                              join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
                              join t7 in db.Common_Country on t2.Destination equals t7.ID
                              where t1.Prod_ReferenceFK == model.Prod_ReferenceFk
                              && t1.Active == true
                              && t1.SewingPlanQuantity == 0
                              && t1.IronPlanQuantity == 0
                              && t1.PackingPlanQuantity == 0
                              && t1.SectionId == 1 && t6.Color.Equals(model.Color)
                              && t6.Size.Equals(model.Size)
                              && t4.ID == model.BOMFk
                              select new ProductionDailyPlan
                              {
                                  Prod_PlanReferenceOrderFk = t1.ID,
                                  OrderNo = t3.CID + Environment.NewLine + "/" + t4.Style,
                                  Prod_ReferenceFk = model.Prod_ReferenceFk,
                                  OrderDeliveryScheduleFk = t5.ID,
                                  Mkt_OrderColorAndSizeRatioFk = t6.ID,
                                  Port = t7.Name + "-" + t2.PortNo,
                                  Color = t6.Color,
                                  Size = t6.Size,
                                  OrderQty = (int)t6.Quantity,
                                  CuttingPlanQuantity = t1.CuttingPlanQuantity,
                                  CuttingExtra = t1.CuttingExtra,
                                  EntryDate = t2.Date,
                                  Plan_ProductionLineFk = t1.Plan_ProductionLineFk,
                              }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();
            }
            else if (model.BOMFk > 0)
            {
                var search = (from t1 in db.Prod_PlanReferenceOrder
                              join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                              join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                              join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                              join t5 in db.Mkt_Item on t4.Mkt_ItemFK equals t5.ID
                              join t6 in db.Mkt_OrderColorAndSizeRatio on t1.Mkt_OrderColorAndSizeRatioFk equals t6.ID
                              join t7 in db.Common_Country on t2.Destination equals t7.ID
                              where t1.Prod_ReferenceFK == model.Prod_ReferenceFk
                              && t1.Active == true
                              && t1.SewingPlanQuantity == 0
                              && t1.IronPlanQuantity == 0
                              && t1.PackingPlanQuantity == 0
                              && t1.SectionId == 1 && t6.Color.Equals(model.Color)
                              && t6.Size.Equals(model.Size)
                              && t4.ID == model.BOMFk
                              select new ProductionDailyPlan
                              {
                                  Prod_PlanReferenceOrderFk = t1.ID,
                                  OrderNo = t3.CID + Environment.NewLine + "/" + t4.Style,
                                  Prod_ReferenceFk = model.Prod_ReferenceFk,
                                  OrderDeliveryScheduleFk = t5.ID,
                                  Mkt_OrderColorAndSizeRatioFk = t6.ID,
                                  Port = t7.Name + "-" + t2.PortNo,
                                  Color = t6.Color,
                                  Size = t6.Size,
                                  OrderQty = (int)t6.Quantity,
                                  CuttingPlanQuantity = t1.CuttingPlanQuantity,
                                  CuttingExtra = t1.CuttingExtra,
                                  EntryDate = t2.Date,
                                  Plan_ProductionLineFk = t1.Plan_ProductionLineFk,
                              }).OrderBy(x => x.Color).ThenBy(x => x.Size).ToList();
            }




        }
        #endregion

        public int Add(int userID)
        {
            Prod_PlanReferenceOrder.AddReady(userID);
            return Prod_PlanReferenceOrder.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_PlanReferenceOrder.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_PlanReferenceOrder.Delete(userID);
        }
        
        /// <summary>
        /// Delete section wise single reference 
        /// all order plan and corriesponding followup data
        /// </summary>
        /// <param name="RefId"></param>
        /// <param name="SectionId"></param>
        public void GetDeletePlanFollowup(int RefId, int SectionId)
        {
            var vOrderData = db.Prod_PlanReferenceOrder.Where(a=>a.SectionId == SectionId && a.Prod_ReferenceFK==RefId).ToList();
            if (vOrderData.Any()) {
                foreach (var b in vOrderData)
                {
                    var vSectionData = db.Prod_PlanReferenceOrderSection.Where(x => x.Prod_PlanReferenceOrderFk == b.ID);
                    if (vSectionData.Any())
                    {
                        db.Prod_PlanReferenceOrderSection.RemoveRange(vSectionData);
                    }
                    var getOrder = db.Prod_PlanReferenceOrder.Where(e => e.ID == b.ID);
                    if (getOrder.Any())
                    {
                        var delete = getOrder.FirstOrDefault();
                        db.Prod_PlanReferenceOrder.Remove(delete);
                    }
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Delete all Followup data which are not planed or 
        /// Create plan and followup than update plan qty zero(0)
        /// </summary>
        public void GetDeleteFollowupWithoutPlan()
        {
            var vData = db.Prod_PlanReferenceOrderSection.GroupBy(a=>a.Prod_PlanReferenceOrderFk).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    var vCheck = db.Prod_PlanReferenceOrder.Where(a => a.ID == v.Key);
                    if (!vCheck.Any())
                    {
                        var delete = db.Prod_PlanReferenceOrderSection.Where(a => a.Prod_PlanReferenceOrderFk==v.Key);
                        if (delete.Any())
                        {
                            db.Prod_PlanReferenceOrderSection.RemoveRange(delete);
                        }
                    }
                }
                db.SaveChanges();
            }

        }

        public void GetDeletePlanOrderWithoutPlan(int PlanOrderId)
        {
            var vData = db.Prod_PlanReferenceOrder.Where(a=>a.ID==PlanOrderId);
            if (vData.Any())
            {
                bool IsCanDelete = true;

                var takeFirst = vData.FirstOrDefault();
                this.ProductionDailyPlan = new ProductionDailyPlan();
                this.ProductionDailyPlan.Prod_ReferenceFk = takeFirst.Prod_ReferenceFK;
                var checkPlanSection = db.Prod_PlanReferenceOrderSection.Where(a=>a.Prod_PlanReferenceOrderFk==takeFirst.ID).ToList();
                if (checkPlanSection.Any())
                {
                    foreach (var v in checkPlanSection)
                    {
                        var vSlave = db.Prod_PlanOrderSectionSlave.Where(a => a.Prod_PlanReferenceOrderSectionFk == v.ID);
                        if (vSlave.Any())
                        {
                            IsCanDelete = false;
                            break;
                        }
                    }
                }
                if (IsCanDelete)
                {
                    var checkPlanSection1 = db.Prod_PlanReferenceOrderSection.Where(a => a.Prod_PlanReferenceOrderFk == takeFirst.ID).ToList();
                    if (checkPlanSection1.Any())
                    {
                        db.Prod_PlanReferenceOrderSection.RemoveRange(checkPlanSection1);
                        db.Prod_PlanReferenceOrder.Remove(takeFirst);
                    }
                    else
                    {
                        db.Prod_PlanReferenceOrder.Remove(takeFirst);
                    }
                }
                db.SaveChanges();
            }
        }

        public void UpdateSewingOrderPlan(ProductionDailyPlan model)
        {
            model.SewingPlanQuantity = model.WorkingHour * model.PerHourTarget;
            var getPlanData = db.Prod_PlanReferenceOrder.Where(a => a.Plan_ProductionLineFk == model.Plan_ProductionLineFk && a.MktBomFk == model.BOMFk && a.SMV == model.SMV && a.ID == model.Prod_PlanReferenceOrderFk);
            if (getPlanData.Any())
            {
                var update = getPlanData.FirstOrDefault();
                update.WorkingHour = model.WorkingHour;
                update.PresentOperator = model.PresentOpt;
                update.PresentHelper = model.NoOfHelper;
                update.MachineRunning = model.MachineRunning;
                update.SewingPlanQuantity = model.SewingPlanQuantity;
                db.SaveChanges();
            }
        }
        
        public void SelectSingleSewingPlan(int OId)
        {
            var getPlanData = (from t1 in db.Prod_PlanReferenceOrder
                               join t2 in db.Mkt_BOM on t1.MktBomFk equals t2.ID
                               join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                               join t4 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t4.ID
                               where t1.ID == OId
                               select new ProductionDailyPlan
                               {
                                   Prod_PlanReferenceOrderFk = t1.ID,
                                   Prod_ReferenceFk = t1.Prod_ReferenceFK,
                                   Plan_ProductionLineFk=t1.Plan_ProductionLineFk,
                                   OrderNo = t3.CID + "/" + t2.Style,
                                   BOMFk = t1.MktBomFk,
                                   SMV = t1.SMV,
                                   PresentOpt = t1.PresentOperator,
                                   NoOfHelper = t1.PresentHelper,
                                   MachineRunning = t1.MachineRunning,
                                   PerHourTarget = t1.Quantity,
                                   WorkingHour = t1.WorkingHour,
                                   Line = t4.Name
                               }).FirstOrDefault();

            this.ProductionDailyPlan = getPlanData;
        }

        #region
        public void AddOrderPlanForSewing(ProductionDailyPlan model)
        {
            DeleteZeroPlanOrder(model.Prod_ReferenceFk, 2);

            var vAddOrderData = (from t1 in db.Prod_PlanReferenceOrder
                                 join t2 in  db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                                 join t3 in db.Mkt_BOM on t2.Mkt_BOMFk equals t3.ID
                                 where t2.Mkt_BOMFk == model.BOMFk && t1.SMV==model.SMV && t1.Active == true
                                 select new { t1, t2, t3 }).ToList();
            
            if (vAddOrderData.Any())
            {
                foreach (var v in vAddOrderData)
                {
                    Prod_PlanReferenceOrder Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
                    Prod_PlanReferenceOrder.MachineRunning = model.MachineRunning;
                    Prod_PlanReferenceOrder.SMV = model.SMV;
                    Prod_PlanReferenceOrder.WorkingHour = model.WorkingHour;
                    Prod_PlanReferenceOrder.PresentOperator = model.PresentOpt;
                    Prod_PlanReferenceOrder.PresentHelper = model.NoOfHelper;
                    Prod_PlanReferenceOrder.Prod_LineChiefFk = model.Prod_LineChiefFk;
                    Prod_PlanReferenceOrder.Add();
                }
            }

        }
        #endregion
    }
}
public class ProductionDailyPlan
{
    public string Code { get; set; }

    public int ID { get; set; }
    
    [Display(Name = "OrderNo"),Required(ErrorMessage ="Order no is required")]
    public int BOMFk { get; set; }

    public int OrderDeliveryScheduleFk { get; set; }

    public int Mkt_OrderColorAndSizeRatioFk { get; set; }

    public int Prod_PlanReferenceOrderFk { get; set; }

    public int Prod_ReferenceFk { get; set; }

    [Display(Name = "Port")]
    public string Port { get; set; }

    [Display(Name = "OrderNo")]
    public string OrderNo { get; set; }
    
    [DisplayName("Line Name"), Required(ErrorMessage ="Production Line is required")]
    public int? Plan_ProductionLineFk { get; set; }

    [DisplayName("Color")]
    public string Color { get; set; }

    [DisplayName("Size Name")]
    public string Size { get; set; }

    public int OrderQty { get; set; }

    public int RemainQty { get; set; }

    [DisplayName("Line Chief")]
    public int? Prod_LineChiefFk { get; set; }

    [DisplayName("Super Visor")]
    public int? Prod_SVNameFk { get; set; }

    [Display(Name = "Line Chief")]
    public string LineChiefSupervisor { get; set; }

    [Display(Name = "Supervisor")]
    public string SuperVisor { get; set; }

    public string Line { get; set; }

    public string Destination { get; set; }

    public string Remarks { get; set; }

    [Display(Name = "CuttingPlanQuantity")]
    public int CuttingPlanQuantity { get; set; }

    [DisplayName("CuttingDoneQuantity")]
    public int CuttingDoneQuantity { get; set; }

    [DisplayName("Extra Cutting")]
    public int CuttingExtra { get; set; }

    [DisplayName("GSM")]
    public decimal GSM { get; set; }

    [DisplayName("SewingPlanQuantity")]
    public int SewingPlanQuantity { get; set; }

    [DisplayName("SewingDoneQuantity")]
    public int SewingDoneQuantity { get; set; }

    [DisplayName("Machine Running")]
    public int MachineRunning { get; set; }

    [DisplayName("Working Hour")]
    public int WorkingHour { get; set; }

    public decimal SMV { get; set; }

    public string LineTable { get; set; }

    [DisplayName("IronPlanQuantity")]
    public int IronPlanQuantity { get; set; }

    [DisplayName("IronDoneQuantity")]
    public int IronDoneQuantity { get; set; }

    [DisplayName("PackingPlanQuantity")]
    public int PackingPlanQuantity { get; set; }

    [DisplayName("PackingDoneQuantity")]
    public int PackingDoneQuantity { get; set; }

    public int UserID { get; set; }

    public string FollowupTime { get; set; }

    public string FollowupDate { get; set; }

    [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
    public DateTime EntryDate { get; set; }

    public string CreateBy { get; set; }

    public decimal CuttingDoneQuantityWeight { get; set; }

    public decimal LayerWeight { get; set; }

    public int MarkerPiece { get; set; }

    [Display(Name ="Present Opt")]
    public int PresentOpt { get; set; }

    [Display(Name = "No of helper")]
    public int NoOfHelper { get; set; }

    public int ExtraQty { get; set; }

    [Display(Name = "Hour Terget")]
    public int PerHourTarget { get; set; }
}