﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_Requisition_Slave
    {
        private xOssContext db;
        public VmGeneral_POSlave VmGeneral_POSlave { get; set; }

        public VmStore_InternalTransferSlave VmStore_InternalTransferSlave { get; set; }
        public Prod_Requisition Prod_Requisition { get; set; }
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public Mkt_POSlave_Consumption Mkt_POSlave_Consumption { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }

        public Prod_TransitionItemInventory Prod_TransitionItemInventory { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public VmProd_Requisition VmProd_Requisition { get; set; }
        public IEnumerable<VmProd_Requisition_Slave> DataList { get; set; }
        public IEnumerable<VmProd_Requisition_Slave> DataOrderList { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Mkt_OrderColorAndSizeRatio Mkt_OrderColorAndSizeRatio { get; set; }
        public int UserDepartmentId { get; set; }
        public decimal? PreviousDelivery { get; set; }
        public decimal? TotalStock { get; set; }
        public decimal? Delivery { get; set; }
        public int RawSubCatID { get; set; }
        public int RawCatID { get; set; }

        public string FabricColor { get; set; }

        public decimal? PreviousDeliveryReturn { get; set; }
        [DisplayName("Available Quantity")]
        public int AvailableQty { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public VmProd_Requisition_Slave()
        {
            db = new xOssContext();
            InitialDataLoad();
        }

        public void InitialDataLoad()
        {
            var v = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                     select new VmProd_Requisition_Slave
                     {
                         Prod_Requisition_Slave = t1,
                         Prod_Requisition = t2
                     }).Where(x => x.Prod_Requisition_Slave.Active == true).OrderByDescending(x => x.Prod_Requisition_Slave.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var reqSlave = db.Prod_Requisition_Slave.Find(iD);

            if (reqSlave.Mkt_POSlaveFK == 1 && reqSlave.Mkt_BOMFK != 1 && reqSlave.Raw_ItemFK != 1)
            {
                var v1 = (from t1 in db.Prod_Requisition_Slave
                          join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                          join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                          join t4 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t4.ID
                          join t5 in db.Raw_Category on t4.Raw_CategoryFK equals t5.ID
                          join t6 in db.Mkt_BOM on t1.Mkt_BOMFK equals t6.ID
                          join t7 in db.Common_TheOrder on t6.Common_TheOrderFk equals t7.ID
                          where t1.Active == true && t2.Active == true // && t10.Active == true && t3.Active == true
                          select new VmProd_Requisition_Slave
                          {
                              Prod_Requisition_Slave = t1,
                              Prod_Requisition = t2,
                              Raw_Item = t3,
                              Raw_SubCategory = t4,
                              Raw_Category = t5,
                              Mkt_BOM = t6,
                              Common_TheOrder = t7
                          }).Where(c => c.Prod_Requisition_Slave.ID == iD).SingleOrDefault();
                this.Prod_Requisition_Slave = v1.Prod_Requisition_Slave;
                this.Prod_Requisition = v1.Prod_Requisition;
                this.Raw_Item = v1.Raw_Item;
                this.Raw_SubCategory = v1.Raw_SubCategory;
                this.Raw_Category = v1.Raw_Category;
                this.Mkt_BOM = v1.Mkt_BOM;
                this.Common_TheOrder = v1.Common_TheOrder;
                this.RawCatID = v1.Raw_Category.ID;
                this.RawSubCatID = v1.Raw_SubCategory.ID;
            }

            if (reqSlave.Mkt_POSlaveFK == 1 && reqSlave.Mkt_BOMFK == 1 && reqSlave.Raw_ItemFK != 1)
            {
                var v1 = (from t1 in db.Prod_Requisition_Slave
                          join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                          join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                          join t4 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t4.ID
                          join t5 in db.Raw_Category on t4.Raw_CategoryFK equals t5.ID                          
                          where t1.Active == true && t2.Active == true // && t10.Active == true && t3.Active == true
                          select new VmProd_Requisition_Slave
                          {
                              Prod_Requisition_Slave = t1,
                              Prod_Requisition = t2,
                              Raw_Item = t3,
                              Raw_SubCategory = t4,
                              Raw_Category = t5                             
                          }).Where(c => c.Prod_Requisition_Slave.ID == iD).SingleOrDefault();
                this.Prod_Requisition_Slave = v1.Prod_Requisition_Slave;
                this.Prod_Requisition = v1.Prod_Requisition;
                this.Raw_Item = v1.Raw_Item;
                this.Raw_SubCategory = v1.Raw_SubCategory;
                this.Raw_Category = v1.Raw_Category;                
                this.RawCatID = v1.Raw_Category.ID;
                this.RawSubCatID = v1.Raw_SubCategory.ID;
            }
            if (reqSlave.Mkt_POSlaveFK != 1 && reqSlave.Mkt_BOMFK != 1 && reqSlave.Raw_ItemFK == 1)
            {
                var v1 = (from t1 in db.Prod_Requisition_Slave
                          join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                          join t3 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t3.ID
                          join t4 in db.Raw_Item on t3.Raw_ItemFK equals t4.ID
                          join t5 in db.Raw_SubCategory on t4.Raw_SubCategoryFK equals t5.ID
                          join t6 in db.Raw_Category on t5.Raw_CategoryFK equals t6.ID
                          where t1.Active == true && t2.Active == true // && t10.Active == true && t3.Active == true
                          select new VmProd_Requisition_Slave
                          {
                              Prod_Requisition_Slave = t1,
                              Prod_Requisition = t2,
                              Mkt_POSlave = t3,
                              Raw_Item = t4,
                              Raw_SubCategory = t5,
                              Raw_Category = t6
                          }).Where(c => c.Prod_Requisition_Slave.ID == iD).SingleOrDefault();
                this.Prod_Requisition_Slave = v1.Prod_Requisition_Slave;
                this.Prod_Requisition = v1.Prod_Requisition;
                this.Mkt_POSlave = v1.Mkt_POSlave;
                this.Raw_Item = v1.Raw_Item;
                this.Raw_SubCategory = v1.Raw_SubCategory;
                this.Raw_Category = v1.Raw_Category;
                this.RawCatID = v1.Raw_Category.ID;
                this.RawSubCatID = v1.Raw_SubCategory.ID;
            }
          
        }


        //internal void GetRequisitionSlaveOrderItem(int id)
        //{
        //    db = new xOssContext();

        //    var a = (from t1 in db.Prod_Requisition_Slave
        //             join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
        //             join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
        //             where t1.Active == true && t1.Prod_RequisitionFK == id

        //             select new VmProd_Requisition_Slave
        //             {

        //                 Prod_Requisition_Slave = t1,
        //                 Prod_Requisition = t2,
        //                 Common_Unit = t3,
        //                 Raw_Item = from o in db.Mkt_BOMSlave
        //                            where o.Mkt_BOMFK== t1.Mkt_BOMFK && o.Mk==t1.Raw_ItemFK
        //                            join p in db.Raw_Item



        //             }).OrderByDescending(x => x.Prod_Requisition_Slave.ID).AsEnumerable();
        //    this.DataList = a;

        //}

        internal void GetRequisitionSlaveItem(int id)
        {
            db = new xOssContext();

            var a = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                     join t10 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t10.ID
                     join t5 in db.Mkt_PO on t10.Mkt_POFK equals t5.ID
                     join t6 in db.Mkt_BOM on t5.Mkt_BOMFK equals t6.ID
                     join t7 in db.Common_TheOrder on t6.Common_TheOrderFk equals t7.ID
                     join t3 in db.Raw_Item on t10.Raw_ItemFK equals t3.ID
                     join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
                     where t1.Active == true && t1.Prod_RequisitionFK == id && t1.Mkt_POSlaveFK != 1
                     select new VmProd_Requisition_Slave
                     {

                         Prod_Requisition_Slave = t1,
                         Prod_Requisition = t2,
                         Raw_Item = t3,
                         Mkt_POSlave = t10,
                         Common_Unit = t4,
                         Mkt_BOM = t6,
                         Common_TheOrder = t7
                     }).OrderByDescending(x => x.Prod_Requisition_Slave.ID).AsEnumerable();

            var b = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                     join t3 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFk equals t3.ID
                     join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
                     where t1.Active == true && t1.Prod_RequisitionFK == id
                     && t1.Prod_TransitionItemInventoryFk != 1
                     select new VmProd_Requisition_Slave
                     {

                         Prod_Requisition_Slave = t1,
                         Prod_Requisition = t2,
                         Prod_TransitionItemInventory = t3,
                         Common_Unit = t4

                     }).OrderByDescending(x => x.Prod_Requisition_Slave.ID).AsEnumerable();

            var c = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                     join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                     join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
                     join t5 in db.Mkt_BOM on t1.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                     where t1.Active == true && t2.Active == true && t3.Active == true && t1.Prod_RequisitionFK == id && t1.Raw_ItemFK != 1
                     select new VmProd_Requisition_Slave
                     {
                         Prod_Requisition_Slave = t1,
                         Prod_Requisition = t2,
                         Raw_Item = t3,
                         Common_Unit = t4,
                         Mkt_BOM = t5,
                         Common_TheOrder = t6
                     }).OrderByDescending(x => x.Prod_Requisition_Slave.ID).AsEnumerable();
            var all = a.Union(b).Union(c);
            this.DataList = all;
            //db = new xOssContext();

            //var a = (from t1 in db.Prod_Requisition_Slave
            //         join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
            //         join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
            //         join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
            //         where t1.Active == true && t1.Prod_RequisitionFK == id

            //         select new VmProd_Requisition_Slave
            //         {

            //             Prod_Requisition_Slave = t1,
            //             Prod_Requisition = t2,
            //             Raw_Item = t3,
            //             Common_Unit = t4,

            //         }).OrderByDescending(x => x.Prod_Requisition_Slave.ID).AsEnumerable();
            //this.DataList = a;

        }

        internal void GetSuggestedFromBOM(int id)
        {
            db = new xOssContext();

            var data1 = (from t1 in db.Prod_Requisition_Slave
                         join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                         join t3 in db.Mkt_BOM on t2.Mkt_BOMFK equals t3.ID
                         join t4 in db.Mkt_BOMSlave on t3.ID equals t4.Mkt_BOMFK

                         where t2.Mkt_BOMFK == t4.Mkt_BOMFK
                         select new
                         {
                             OrderQ = t3.Quantity * t4.Consumption,
                             Raw_ItemFK = t4.Raw_ItemFK,
                             Consumption = t4.Consumption,
                             Common_UnitFK = t4.Common_UnitFK,
                             Prod_RequisitionFK = id
                         }).AsEnumerable();
            foreach (var b in data1)
            {
                Prod_Requisition_Slave x = new Prod_Requisition_Slave();
                x.Prod_RequisitionFK = b.Prod_RequisitionFK;
                x.Raw_ItemFK = b.Raw_ItemFK;
                x.Consumption = b.Consumption;
                x.TotalRequired = b.Consumption * b.OrderQ;
                x.Common_UnitFK = b.Common_UnitFK;
                x.AddReady(0);
                x.Add();
            }
        }

        public int Add(int userID)
        {
            Prod_Requisition_Slave.AddReady(userID);
            return Prod_Requisition_Slave.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_Requisition_Slave.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_Requisition_Slave.Delete(userID);
        }

        public int GetRawItemID(int id)
        {
            var rs = (from t1 in db.Prod_Requisition_Slave
                      where t1.ID == id
                      select (t1.Raw_ItemFK)).FirstOrDefault();
            return rs.Value;
        }
        public int GetCommonUintID(int id)
        {
            var rs = (from t1 in db.Prod_Requisition_Slave
                      where t1.ID == id
                      select (t1.Common_UnitFK)).FirstOrDefault();
            return rs.Value;
        }
    }
}