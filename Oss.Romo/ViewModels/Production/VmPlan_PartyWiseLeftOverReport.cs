﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models;

namespace Oss.Romo.ViewModels.Production
{
    public class VmPlan_PartyWiseLeftOver
    {
        private xOssContext db;
        public string SupplierName { get; set; }
        public DateTime OrderDate { get; set; }
        public string CID { get; set; }
        public string Perticulers { get; set; }
        public decimal ReceiveQty { get; set; }
        public decimal RequiredQty { get; set; }
        public decimal SendQty { get; set; }
        public List<VmPlan_PartyWiseLeftOver> VmPlanPartyWiseLeftOver { get; set; }
        public void InitialLoadData()
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     where t1.Active == true && t1.IsAuthorize && t2.Active == true
                     select new
                     {
                         Mkt_PO = t1,
                         Common_Supplier = t2
                     }).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();
            var list = new List<VmPlan_PartyWiseLeftOver>();

            foreach (var vmCommonTheOrder in v)
            {
                VmPlan_PartyWiseLeftOver planProductionReportOrderWise = new VmPlan_PartyWiseLeftOver();
                planProductionReportOrderWise.CID = vmCommonTheOrder.Mkt_PO.CID;
                planProductionReportOrderWise.SupplierName = vmCommonTheOrder.Common_Supplier.Name;

                MethodForGetReqireSendReceiveQty(planProductionReportOrderWise, vmCommonTheOrder.Mkt_PO.ID);
                list.Add(planProductionReportOrderWise);
            }
            this.VmPlanPartyWiseLeftOver = list;
        }

        private void MethodForGetReqireSendReceiveQty(VmPlan_PartyWiseLeftOver planPartyWiseLeftOver, int mktPoId)
        {
            db = new xOssContext();
            var poslave = (from t1 in db.Mkt_POSlave
                           join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                           join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                           where t1.Active == true && t2.Active == true && t1.Mkt_POFK == mktPoId
                           select new
                           {
                               Mkt_POSlave = t1,
                               Raw_Item = t2,
                               Common_Unit = t3
                           }).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            var perticulars = "";
            foreach (var items in poslave)
            {
                if (items.Mkt_POSlave.TotalRequired != null)
                    planPartyWiseLeftOver.RequiredQty = (decimal)items.Mkt_POSlave.TotalRequired;
                planPartyWiseLeftOver.ReceiveQty = GetReceiveQtyByPoSlave(items.Mkt_POSlave.ID);
                planPartyWiseLeftOver.SendQty = GetSendQtyByPoSlave(items.Mkt_POSlave.ID);
                perticulars += items.Raw_Item.Name + "(Req: " + planPartyWiseLeftOver.RequiredQty + ", Rec: " + planPartyWiseLeftOver.ReceiveQty + ", Send: " + planPartyWiseLeftOver.SendQty + ", Left: " + GetPerticulers(planPartyWiseLeftOver.RequiredQty, planPartyWiseLeftOver.ReceiveQty, planPartyWiseLeftOver.SendQty).ToString("F") + " " + items.Common_Unit.Name + ")\n";
                planPartyWiseLeftOver.Perticulers = perticulars;
            }
        }

        private decimal GetPerticulers(decimal requiredQty, decimal receiveQty, decimal sendQty)
        {
            var s = requiredQty - receiveQty;
            try
            {
                var t = sendQty / requiredQty;
                if (sendQty == 0)
                    return s;
                else
                    return s * t;
            }
            catch (Exception e)
            {
                return s;
            }
        }

        private decimal GetReceiveQtyByPoSlave(int mktPoSlaveId)
        {
            db = new xOssContext();
            var poslave = (from t1 in db.Mkt_POSlave_Receiving
                           where t1.Active == true && t1.Mkt_POSlaveFK == mktPoSlaveId && t1.IsReturn == false
                           select new
                           {
                               Mkt_POSlave_Receiving = t1
                           }).Sum(x => x.Mkt_POSlave_Receiving.Quantity);
            if (poslave != null)
                return (decimal)poslave;
            else return 0;
        }
        private decimal GetSendQtyByPoSlave(int mktPoSlaveId)
        {
            db = new xOssContext();
            var poslave = (from t1 in db.Mkt_POExtTransfer
                           where t1.Active == true && t1.Mkt_POSlaveFK == mktPoSlaveId
                           select new
                           {
                               Mkt_POExtTransfer = t1
                           }).Sum(x => x.Mkt_POExtTransfer.TotalRequired);
            if (poslave != null)
                return (decimal)poslave;
            else return 0;
        }
    }
}