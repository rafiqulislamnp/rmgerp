﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_Reference
    {
        private xOssContext db;
        public IEnumerable<VmProd_Reference> DataList { get; set; }
        public Prod_Reference Prod_Reference { get; set; }

        public VmProd_Reference()
        {
            db = new xOssContext();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Reference
                     select new VmProd_Reference
                     {
                         Prod_Reference = t1
                     }).Where(x => x.Prod_Reference.Active == true).OrderByDescending(x => x.Prod_Reference.FromDate).AsEnumerable();
            this.DataList = v;
        }

        public void SelectSingleData(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Reference
                     select new VmProd_Reference
                     {
                         Prod_Reference = t1
                     }).FirstOrDefault(x => x.Prod_Reference.Active == true && x.Prod_Reference.ID == id);
            if (v != null)
            {
                this.Prod_Reference = v.Prod_Reference;
            }
        }

        public int Add(int userID)
        {
            Prod_Reference.AddReady(userID);
            return Prod_Reference.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_Reference.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_Reference.Delete(userID);
        }
    }
}