﻿using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrystalDecisions.Web.HtmlReportRender;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.User;
using System.ComponentModel.DataAnnotations;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_Requisition
    {
        private xOssContext db;

        [Required(ErrorMessage = "Requisition Type is required"), Display(Name = "Requisition Type")]
        public int InternalValue { get; set; }

        public Prod_Requisition Prod_Requisition { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public User_Department User_Department { get; set; }
        public User_Department ToUser_Department { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public VmProd_Requisition_Slave VmProd_Requisition_Slave { get; set; }
        public IEnumerable<VmProd_Requisition> DataList { get; set; }
      
        public VmProd_Requisition()
        {
            db = new xOssContext();
            //InitialDataLoad(false);

        }

        public void InitialDataLoad(int DepartmentId)
        {
            if (DepartmentId == 3)
            {
                var v = (from t1 in db.Prod_Requisition
                         join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                         where t1.Active == true && t1.Internal == true
                         select new VmProd_Requisition
                         {
                             Prod_Requisition = t1,
                             ToUser_Department = t3,
                         }).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();
                this.DataList = v;
            }
            else
            {
                var v = (from t1 in db.Prod_Requisition
                         join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                         where t1.Active == true && t1.Internal == true && t1.FromUser_DeptFK == DepartmentId
                         select new VmProd_Requisition
                         {
                             Prod_Requisition = t1,
                             ToUser_Department = t3,
                         }).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();
                this.DataList = v;
            }
        }
        
        public void InitialDataLoadComplete()
        {
            var v = (from t1 in db.Prod_Requisition
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     where t1.IsComplete == true
                     select new VmProd_Requisition
                     {
                         Mkt_BOM = t2,
                         Prod_Requisition = t1
                     }).Where(x => x.Prod_Requisition.Active == true).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();
            this.DataList = v;

        }

        public VmProd_Requisition SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Requisition
                     join t2 in db.User_Department on t1.FromUser_DeptFK equals t2.ID
                     join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                     where t1.ID == iD
                     select new VmProd_Requisition
                     {
                         Prod_Requisition = t1,
                         User_Department = t2,
                         ToUser_Department = t3
                     }).FirstOrDefault();
            return v;
        }

        //public VmProd_Requisition SelectSingleJoined(int iD)
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Prod_Requisition
        //             join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
        //             join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //             //join t4 in db.User_Department on t1.User_DeptFK equals t4.ID
        //             where t1.ID == iD
        //             select new VmProd_Requisition
        //             {
        //                 Prod_Requisition = t1,
        //                 Mkt_BOM = t2,
        //                 Common_TheOrder = t3,
        //                 //User_Department = t4
        //             }).FirstOrDefault();
        //    return v;
        //}

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from rc in db.Prod_Requisition
                     select new VmProd_Requisition
                     {
                         Prod_Requisition = rc
                     }).Where(c => c.Prod_Requisition.ID == iD).SingleOrDefault();
            this.Prod_Requisition = v.Prod_Requisition;

        }
       
        public int Add(int userID)
        {
            Prod_Requisition.AddReady(userID);
            return Prod_Requisition.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_Requisition.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_Requisition.Delete(userID);
        }
        
        public void ChangeRequisitionStatus(int id)
        {
            VmProd_Requisition r = new VmProd_Requisition();
            r.SelectSingle(id);
            r.Prod_Requisition.IsComplete = !r.Prod_Requisition.IsComplete;
            r.Edit(0);
        }

        public void InitialSearchLoad(bool flag, string SearchString)
        {
            var v = (from t1 in db.Prod_Requisition
                             join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                             where t1.IsComplete == flag
                             select new VmProd_Requisition
                             {
                                 Mkt_BOM = t2,
                                 Prod_Requisition = t1
                             }).Where(x => x.Prod_Requisition.Active == true && x.Prod_Requisition.RequisitionCID.Contains(SearchString)).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();
            this.DataList = v;

        }

        public void GetItemForConsmtionREQ()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Requisition
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     where t1.IsComplete == false
                     select new VmProd_Requisition
                     {
                         Mkt_BOM = t2,
                         Prod_Requisition = t1
                     }).Where(x => x.Prod_Requisition.Active == true).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();
            this.DataList = v;
        }
        
    }
}