﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_MonthlyOrderMc
    {
        private xOssContext db;
        public List<MonthlyOrderMcView> DataList { get; set; }
        public Prod_PlanReferenceProduction Prod_PlanReferenceProduction { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Prod_PlanReference Prod_PlanReference { get; set; }
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
      
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public void IndialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Planning

                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t3 in db.Mkt_Item on t4.Mkt_ItemFK equals t3.ID
                     //join t7 in db.Common_Country on t3.Destination equals t7.ID

                     select new MonthlyOrderMcView
                     {
                         Prod_Planning = t1,

                         Mkt_Item = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         OrderQuantityPC = t4.QPack * t4.Quantity
                     }).Where(x => x.Prod_Planning.Active == true).OrderByDescending(x => x.Prod_Planning.ID).AsEnumerable();
           this.DataList = v.ToList();

        }
    }

    public class MonthlyOrderMcView
    {
        public Prod_Planning Prod_Planning { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
      
        public decimal OrderQuantityPC { get; set; }
    }
}