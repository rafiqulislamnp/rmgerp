﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_FinishItemTransferSlave
    {
        private xOssContext db;

        public VmProd_FinishItemTransferSlave()
        {
            db = new xOssContext();
        }

        public Prod_InputRequisition Prod_InputRequisition { get; set; }
        public Prod_InputRequisitionSlave Prod_InputRequisitionSlave { get; set; }
        public Prod_FinishItemTransfer Prod_FinishItemTransfer { get; set; }
        public Prod_FinishItemTransferSlave Prod_FinishItemTransferSlave { get; set; }
        
        public Common_TheOrder Common_TheOrder { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }

        public int DepartmentId{ get; set; }

        public int AvailableQty { get; set; }

        public int RemainQty { get; set; }

        public IEnumerable<VmProd_FinishItemTransferSlave> DataList { get; set; }

        //public void GetRequisitionToId(int ReqId)
        //{
        //    db = new xOssContext();
        //    var vData=db.Prod_InputRequisition.Where(a=>a.)
        //}

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_FinishItemTransfer
                     join t2 in db.Prod_InputRequisition on t1.Prod_InputRequisitionFK equals t2.ID
                     join t3 in db.Prod_FinishItemTransferSlave on t1.ID equals t3.Prod_FinishItemTransferFK
                     join t4 in db.Prod_InputRequisitionSlave on t3.Prod_InputRequisitionSlaveFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Unit on t4.Common_UnitFK equals t7.ID
                     where t3.Active == true && t1.ID == id
                     select new VmProd_FinishItemTransferSlave
                     {
                         Prod_FinishItemTransfer=t1,
                         Prod_InputRequisition = t2,
                         Prod_FinishItemTransferSlave = t3,
                         Prod_InputRequisitionSlave=t4,
                         Mkt_BOM = t5,
                         Common_TheOrder = t6,
                         Common_Unit = t7
                     }).OrderByDescending(x => x.Prod_FinishItemTransferSlave.ID).AsEnumerable();
            this.DataList = v;
        }

        internal void GetFinishItemTransferSlaveItem(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_FinishItemTransfer
                     join t2 in db.Prod_InputRequisition on t1.Prod_InputRequisitionFK equals t2.ID
                     join t3 in db.Prod_FinishItemTransferSlave on t1.ID equals t3.Prod_FinishItemTransferFK
                     join t4 in db.Prod_InputRequisitionSlave on t3.Prod_InputRequisitionSlaveFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Unit on t4.Common_UnitFK equals t7.ID
                     where t3.Active == true && t1.ID == id
                     select new VmProd_FinishItemTransferSlave
                     {
                         Prod_FinishItemTransfer = t1,
                         Prod_InputRequisition = t2,
                         Prod_FinishItemTransferSlave = t3,
                         Prod_InputRequisitionSlave = t4,
                         Mkt_BOM = t5,
                         Common_TheOrder = t6,
                         Common_Unit = t7
                     }).OrderByDescending(x => x.Prod_FinishItemTransferSlave.ID).AsEnumerable();
            this.DataList = v;
        }

        public int Add(int userID)
        {
            Prod_FinishItemTransferSlave.AddReady(userID);
            return Prod_FinishItemTransferSlave.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_FinishItemTransferSlave.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_FinishItemTransferSlave.Delete(userID);
        }
    }
}