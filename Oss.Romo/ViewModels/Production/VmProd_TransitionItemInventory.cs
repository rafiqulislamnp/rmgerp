﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_TransitionItemInventory
    {
        private xOssContext db;
        public IEnumerable<VmProd_TransitionItemInventory> DataList { get; set; }
        public Prod_TransitionItemInventory Prod_TransitionItemInventory { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Prod_MachineDIA Prod_MachineDIA { get; set; }

        public VmControllerHelper VmControllerHelper { get; set; }
        public VmProd_TransitionItemInventory()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_TransitionItemInventory
                join t2 in db.Mkt_BOM  on t1.Mkt_BOMFk equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
                     join t5 in db.Prod_MachineDIA on t1.Prod_MachineDIAFk equals t5.ID

                     select new VmProd_TransitionItemInventory
                     {
                         Prod_TransitionItemInventory = t1,
                         Mkt_BOM = t2,
                         Common_TheOrder = t3,
                         Common_Unit = t4,
                         Prod_MachineDIA = t5
                     }).Where(x => x.Prod_TransitionItemInventory.Active == true).OrderByDescending(x => x.Prod_TransitionItemInventory.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingle(int? id)
        {
          
            db = new xOssContext();
            var v = (from t1 in db.Prod_TransitionItemInventory
                     select new VmProd_TransitionItemInventory
                     {
                         Prod_TransitionItemInventory = t1
                     }).SingleOrDefault(c => c.Prod_TransitionItemInventory.ID == id);
            this.Prod_TransitionItemInventory = v.Prod_TransitionItemInventory;

        }

        public int Add(int userID)
        {
            Prod_TransitionItemInventory.AddReady(userID);
            return Prod_TransitionItemInventory.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_TransitionItemInventory.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_TransitionItemInventory.Delete(userID);
        }

    }
}