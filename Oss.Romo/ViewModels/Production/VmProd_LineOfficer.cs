﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Planning;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_LineOfficer
    {
        private xOssContext db;
        public IEnumerable<VmProd_LineOfficer> DataList { get; set; }
        public Prod_LineOfficer Prod_LineOfficer { get; set; }
        public Prod_LineChief Prod_LineChief { get; set; }
        public Prod_SVName Prod_SVName { get; set; }
        public Plan_ProductionLine Plan_ProductionLine { get; set; }

        public void InitialDataLoad(int Id)
        {
            db = new xOssContext();

            var vData= (from o in db.Prod_LineOfficer
                          join p in db.Plan_ProductionLine on o.Plan_ProductionLineFK equals p.ID
                          join q in db.Prod_LineChief on o.Prod_LineChiefFK equals q.ID
                          join r in db.Prod_SVName on o.Prod_LineSuperVisorFk equals r.ID
                          where o.Prod_LineChiefFK == Id && o.Active == true
                          select new VmProd_LineOfficer
                          {
                              Plan_ProductionLine=p,
                              Prod_LineChief=q,
                              Prod_SVName=r,
                              Prod_LineOfficer=o
                          }).AsEnumerable();
            
            this.DataList = vData;
        }
        
        public void SelectSingle(int id)
        {

            db = new xOssContext();
            var v = (from t1 in db.Prod_LineOfficer
                     select new VmProd_LineOfficer
                     {
                         Prod_LineOfficer = t1
                     }).Where(c => c.Prod_LineOfficer.ID == id).SingleOrDefault();
            this.Prod_LineOfficer = v.Prod_LineOfficer;

        }

        public VmProd_LineOfficer SelectSingleChiefLine(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_LineChief
                     where t1.Active == true
                     && t1.ID==id
                     select new VmProd_LineOfficer
                     {
                         Prod_LineChief = t1

                     }).FirstOrDefault();
            return v;
        }
       
        public int Add(int userID)
        {
            Prod_LineOfficer.AddReady(userID);
            return Prod_LineOfficer.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_LineOfficer.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_LineOfficer.Delete(userID);
        }

        public void DeleteAll(int Id,int userId)
        {

            db = new xOssContext();
            var getData = (from o in db.Prod_LineOfficer
                          where o.ID==Id
                          select new
                          { o }).ToList();

            if (getData.Any())
            {
                int LineID = getData.FirstOrDefault().o.Plan_ProductionLineFK;

                var deleteItem = db.Prod_LineOfficer.Where(a=>a.Plan_ProductionLineFK==LineID && a.Active==true);
                if (deleteItem.Any())
                {
                    foreach (var v in deleteItem)
                    {
                        Prod_LineOfficer b = new Prod_LineOfficer();
                        b.ID = v.ID;
                        b.Delete(userId);
                    }
                }
            }
        }
    }
}