﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_MachineType
    {
        private xOssContext db;
        public IEnumerable<VmProd_MachineType> DataList { get; set; }

        public Prod_MachineType Prod_MachineType { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Prod_MachineType
                     select new VmProd_MachineType
                     {
                         Prod_MachineType = t1
                     }).Where(x => x.Prod_MachineType.Active == true).OrderByDescending(x => x.Prod_MachineType.ID).AsEnumerable();
            this.DataList = a;

        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_MachineType

                     select new VmProd_MachineType
                     {
                         Prod_MachineType = t1
                     }).Where(c => c.Prod_MachineType.ID == id).SingleOrDefault();
            this.Prod_MachineType = v.Prod_MachineType;

        }

        public int Add(int userID)
        {
            Prod_MachineType.AddReady(userID);
       
            return Prod_MachineType.Add();

        }
        public bool Edit(int userID)
        {
            return Prod_MachineType.Edit(userID);
        }
    }
}