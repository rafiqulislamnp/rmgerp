﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_Consumption
    {
        private xOssContext db;
        public IEnumerable<VmProd_Consumption> DataList { get; set; }
        public Prod_Consumption Prod_Consumption { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmProd_Consumption()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Consumption
                     select new VmProd_Consumption
                     {
                         Prod_Consumption = t1
                     }).Where(x => x.Prod_Consumption.Active == true).OrderByDescending(x => x.Prod_Consumption.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingle(int id)
        {
          
            db = new xOssContext();
            var v = (from t1 in db.Prod_Consumption
                     select new VmProd_Consumption
                     {
                         Prod_Consumption = t1
                     }).Where(c => c.Prod_Consumption.ID == id).SingleOrDefault();
            this.Prod_Consumption = v.Prod_Consumption;

        }

        public int Add(int userID)
        {
            Prod_Consumption.AddReady(userID);
            return Prod_Consumption.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_Consumption.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_Consumption.Delete(userID);
        }

    }
}