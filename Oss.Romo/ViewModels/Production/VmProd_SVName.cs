﻿using Oss.Romo.Models;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_SVName
    {
        private xOssContext db;
        public IEnumerable<VmProd_SVName> DataList { get; set; }
        public Prod_SVName Prod_SVName { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmProd_SVName()
        {
            db = new xOssContext();
            InitialDataLoad();
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            var v = (from t1 in db.Prod_SVName
                     select new VmProd_SVName
                     {
                         Prod_SVName = t1
                     }).Where(x => x.Prod_SVName.Active == true).OrderByDescending(x => x.Prod_SVName.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from rc in db.Prod_SVName
                     select new VmProd_SVName
                     {
                         Prod_SVName = rc
                     }).Where(c => c.Prod_SVName.ID == id).SingleOrDefault();
            this.Prod_SVName = v.Prod_SVName;

        }

        public int Add(int userID)
        {
            Prod_SVName.AddReady(userID);
            return Prod_SVName.Add();
        }

        public bool Edit(int userID)
        {
            return Prod_SVName.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Prod_SVName.Delete(userID);
        }
    }
}