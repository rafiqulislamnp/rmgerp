﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Production
{
    public class VmProd_PlanReferenceOrderSection
    {
        private xOssContext db;
        
        public ProductionDailyPlan ProductionDailyPlan { get; set; }
        public IEnumerable<ProductionDailyPlan> PlanDataList { get; set; }
        //public int LineChiefId { get; set; }
        //public int SupervisorId { get; set; }
        public int TotalQty { get; set; }
        public decimal WeightQty { get; set; }

        public VmProd_PlanReferenceOrderSection()
        {
            db = new xOssContext();
        }
        
        public void SaveCuttingSection(ProductionDailyPlan model)
        {
            var getRefOrder = db.Prod_PlanReferenceOrder.Where(a=>a.ID==model.Prod_PlanReferenceOrderFk).FirstOrDefault();

            Prod_PlanReferenceOrderSection Prod_PlanReferenceOrderSection = new Prod_PlanReferenceOrderSection();
            Prod_PlanReferenceOrderSection.Prod_PlanReferenceOrderFk = model.Prod_PlanReferenceOrderFk;
            Prod_PlanReferenceOrderSection.Quantity = model.CuttingDoneQuantity;
            Prod_PlanReferenceOrderSection.Plan_ReferenceSectionFk = 1;
            Prod_PlanReferenceOrderSection.OrderDeliveryScheduleFk = (int)getRefOrder.OrderDeliveryScheduleFk;
            Prod_PlanReferenceOrderSection.Mkt_OrderColorAndSizeRatioFk = (int)getRefOrder.Mkt_OrderColorAndSizeRatioFk;
            Prod_PlanReferenceOrderSection.FirstCreatedBy = model.UserID;
            Prod_PlanReferenceOrderSection.AddReady(model.UserID);
            Prod_PlanReferenceOrderSection.Add();
            decimal perPieceWeight = 0;

            if (getRefOrder.PerLayerWeight>0 && getRefOrder.MarkerPiece>0)
            {
                perPieceWeight = getRefOrder.PerLayerWeight / getRefOrder.MarkerPiece;
            }

            this.WeightQty = Math.Round(GetSectionWiseTotalDoneQty((int)getRefOrder.OrderDeliveryScheduleFk, (int)getRefOrder.Mkt_OrderColorAndSizeRatioFk, 1) * perPieceWeight, 2);
            var getAll = db.Prod_PlanReferenceOrderSection.Where(a => a.Prod_PlanReferenceOrderFk == model.Prod_PlanReferenceOrderFk && a.Plan_ReferenceSectionFk == 1);
            if (getAll.Any())
            {
                this.TotalQty = (int)getAll.Sum(a => a.Quantity);
                
            }
        }

        public void SaveSewingSection(ProductionDailyPlan model)
        {
            var getRefOrderSection = db.Prod_PlanReferenceOrderSection.Where(a => a.ID == model.ID).FirstOrDefault();
            
            Prod_PlanOrderSectionSlave Prod_PlanOrderSectionSlave = new Prod_PlanOrderSectionSlave();
            Prod_PlanOrderSectionSlave.Prod_PlanReferenceOrderSectionFk = model.ID;
            Prod_PlanOrderSectionSlave.Quantity = model.SewingDoneQuantity;
            Prod_PlanOrderSectionSlave.FirstCreatedBy = model.UserID;
            Prod_PlanOrderSectionSlave.AddReady(model.UserID);
            Prod_PlanOrderSectionSlave.Add();

            var TotalFollowupQty = db.Prod_PlanOrderSectionSlave.Where(a => a.Prod_PlanReferenceOrderSectionFk == model.ID).Sum(a => a.Quantity);

            getRefOrderSection.Quantity = TotalFollowupQty == null ? 0 : TotalFollowupQty;
            getRefOrderSection.Edit(model.UserID);

            var ProdRefOrderInfo = db.Prod_PlanReferenceOrderSection.Where(a => a.ID == model.ID).Join(db.Prod_PlanReferenceOrder, a => a.Prod_PlanReferenceOrderFk, b => b.ID, (a, b) => new { RefId = b.Prod_ReferenceFK, BomFk=b.MktBomFk }).FirstOrDefault();

            model.Prod_ReferenceFk = ProdRefOrderInfo.RefId;
            model.BOMFk = ProdRefOrderInfo.BomFk;
            
            UpdatePlanAchivement(model);
            this.TotalQty= TotalFollowupQty == null ? 0 : (int)TotalFollowupQty;
        }

        public void SaveIroningSection(ProductionDailyPlan model)
        {
            var getRefOrderSection = db.Prod_PlanReferenceOrderSection.Where(a => a.ID == model.ID).FirstOrDefault();

            Prod_PlanOrderSectionSlave Prod_PlanOrderSectionSlave = new Prod_PlanOrderSectionSlave();
            Prod_PlanOrderSectionSlave.Prod_PlanReferenceOrderSectionFk = model.ID;
            Prod_PlanOrderSectionSlave.Quantity = model.IronDoneQuantity;
            Prod_PlanOrderSectionSlave.FirstCreatedBy = model.UserID;
            Prod_PlanOrderSectionSlave.AddReady(model.UserID);
            Prod_PlanOrderSectionSlave.Add();

            var TotalFollowupQty = db.Prod_PlanOrderSectionSlave.Where(a => a.Prod_PlanReferenceOrderSectionFk == model.ID).Sum(a => a.Quantity);

            getRefOrderSection.Quantity = TotalFollowupQty == null ? 0 : TotalFollowupQty;
            getRefOrderSection.Edit(model.UserID);
            
            this.TotalQty = TotalFollowupQty == null ? 0 : (int)TotalFollowupQty;
            
            //var getRefOrder = db.Prod_PlanReferenceOrder.Where(a => a.ID == model.Prod_PlanReferenceOrderFk).FirstOrDefault();

            //Prod_PlanReferenceOrderSection Prod_PlanReferenceOrderSection = new Prod_PlanReferenceOrderSection();
            //Prod_PlanReferenceOrderSection.Prod_PlanReferenceOrderFk = model.Prod_PlanReferenceOrderFk;
            //Prod_PlanReferenceOrderSection.Quantity = model.IronDoneQuantity;
            //Prod_PlanReferenceOrderSection.Plan_ReferenceSectionFk = 3;
            //Prod_PlanReferenceOrderSection.OrderDeliveryScheduleFk = (int)getRefOrder.OrderDeliveryScheduleFk;
            //Prod_PlanReferenceOrderSection.Mkt_OrderColorAndSizeRatioFk = (int)getRefOrder.Mkt_OrderColorAndSizeRatioFk;
            //Prod_PlanReferenceOrderSection.FirstCreatedBy = model.UserID;
            //Prod_PlanReferenceOrderSection.AddReady(model.UserID);
            //Prod_PlanReferenceOrderSection.Add();

            //var getAll = db.Prod_PlanReferenceOrderSection.Where(a => a.Prod_PlanReferenceOrderFk == model.Prod_PlanReferenceOrderFk && a.Plan_ReferenceSectionFk==3);
            //if (getAll.Any())
            //{
            //    this.TotalQty = (int)getAll.Sum(a => a.Quantity);
            //}
        }

        public void SavePackingSection(ProductionDailyPlan model)
        {
            var getRefOrderSection = db.Prod_PlanReferenceOrderSection.Where(a => a.ID == model.ID).FirstOrDefault();

            Prod_PlanOrderSectionSlave Prod_PlanOrderSectionSlave = new Prod_PlanOrderSectionSlave();
            Prod_PlanOrderSectionSlave.Prod_PlanReferenceOrderSectionFk = model.ID;
            Prod_PlanOrderSectionSlave.Quantity = model.PackingDoneQuantity;
            Prod_PlanOrderSectionSlave.FirstCreatedBy = model.UserID;
            Prod_PlanOrderSectionSlave.AddReady(model.UserID);
            Prod_PlanOrderSectionSlave.Add();

            var TotalFollowupQty = db.Prod_PlanOrderSectionSlave.Where(a => a.Prod_PlanReferenceOrderSectionFk == model.ID).Sum(a => a.Quantity);

            getRefOrderSection.Quantity = TotalFollowupQty == null ? 0 : TotalFollowupQty;
            getRefOrderSection.Edit(model.UserID);

            this.TotalQty = TotalFollowupQty == null ? 0 : (int)TotalFollowupQty;
        }

        public int GetSectionWiseTotalDoneQty(int OrderDeliveryScheduleFk, int OrderColorAndSizeRatioFk, int SectionId)
        {
            int TotalQty = 0;
            var vData = db.Prod_PlanReferenceOrderSection.Where(a => a.OrderDeliveryScheduleFk == OrderDeliveryScheduleFk && a.Mkt_OrderColorAndSizeRatioFk == OrderColorAndSizeRatioFk && a.Plan_ReferenceSectionFk == SectionId).ToList();
            if (vData.Any())
            {
                TotalQty = (int)vData.Sum(a => a.Quantity);
            }

            return TotalQty;
        }

        public void UpdatePlanAchivement(ProductionDailyPlan model)
        {
            var vData = (from t1 in db.Prod_Reference
                         join t2 in db.Prod_PlanReferenceOrder on t1.ID equals t2.Prod_ReferenceFK
                         join t3 in db.Prod_PlanReferenceOrderSection on t2.ID equals t3.Prod_PlanReferenceOrderFk
                         where
                         t2.SectionId == 2 
                         && t1.ID == model.Prod_ReferenceFk  
                         && t3.Plan_ReferenceSectionFk == 2
                         && t2.MktBomFk==model.BOMFk
                         group t3.Quantity by new { t2.Plan_ProductionLineFk, t2.MktBomFk, t1.FromDate } into a
                         select new
                         {
                             BomId = a.Key.MktBomFk,
                             LineId = a.Key.Plan_ProductionLineFk,
                             Date = a.Key.FromDate,
                             qty = a.Sum()
                         }).ToList();
            
            if (vData.Any())
            {
                var vPlanData = (from t1 in db.Prod_OrderPlanning
                                 join t2 in db.Prod_Planning on t1.ID equals t2.Prod_OrderPlanningFK
                                 where t2.Mkt_BOMFK == model.BOMFk && t2.IsClosed==false
                                 select new { t2 }).ToList();

                if (vPlanData.Any())
                {
                    var vAchivment = db.Prod_PlanAchievment.Where(a => a.Mkt_BOMFK == model.BOMFk).ToList();

                    foreach (var v in vData)
                    {
                        var vCheck = vPlanData.Where(a => a.t2.StartDate <= v.Date && a.t2.FinishDate >= v.Date && a.t2.Plan_ProductionLineFk == v.LineId);
                        if (vCheck.Any())
                        {
                            var takePlan = vCheck.FirstOrDefault();
                            #region OrderPlanWithAchiveFound
                            if (vAchivment.Any(a => a.AchiveDate == v.Date && a.Mkt_BOMFK == v.BomId && a.Plan_ProductionLineFk == v.LineId && a.Prod_OrderPlanningFk == takePlan.t2.Prod_OrderPlanningFK))
                            {
                                var getAchive = db.Prod_PlanAchievment.Where(a => a.AchiveDate == v.Date && a.Mkt_BOMFK == v.BomId && a.Plan_ProductionLineFk == v.LineId && a.Prod_OrderPlanningFk == takePlan.t2.Prod_OrderPlanningFK).FirstOrDefault();
                                getAchive.Prod_OrderPlanningFk = takePlan.t2.Prod_OrderPlanningFK;
                                getAchive.HourCapacity = takePlan.t2.Capacity;
                                getAchive.WorkingHour = takePlan.t2.WorkingHour;
                                getAchive.PlanAchievQty = v.qty;
                                getAchive.IsPlanedLine = true;
                            }
                            else
                            {
                                Prod_PlanAchievment add = new Prod_PlanAchievment();
                                add.Prod_OrderPlanningFk = takePlan.t2.Prod_OrderPlanningFK;
                                add.HourCapacity = takePlan.t2.Capacity;
                                add.WorkingHour = takePlan.t2.WorkingHour;
                                add.AchiveDate = v.Date;
                                add.Plan_ProductionLineFk = v.LineId;
                                add.Mkt_BOMFK = v.BomId;
                                add.PlanAchievQty = v.qty;
                                add.IsPlanedLine = true;
                                db.Prod_PlanAchievment.Add(add);
                            }
                            #endregion
                        }
                        else
                        {
                            var getPlanOrder = vPlanData.FirstOrDefault();
                            if (vAchivment.Any(a => a.AchiveDate == v.Date && a.Mkt_BOMFK == v.BomId && a.Plan_ProductionLineFk == v.LineId && a.Prod_OrderPlanningFk == getPlanOrder.t2.Prod_OrderPlanningFK))
                            {
                                var getAchive = db.Prod_PlanAchievment.Where(a => a.AchiveDate == v.Date && a.Mkt_BOMFK == v.BomId && a.Plan_ProductionLineFk == v.LineId && a.Prod_OrderPlanningFk == getPlanOrder.t2.Prod_OrderPlanningFK).FirstOrDefault();
                                getAchive.Prod_OrderPlanningFk = getPlanOrder.t2.Prod_OrderPlanningFK;
                                getAchive.HourCapacity = 0;
                                getAchive.WorkingHour = 0;
                                getAchive.PlanAchievQty = v.qty;
                                getAchive.IsPlanedLine = false;
                            }
                            else
                            {
                                Prod_PlanAchievment add = new Prod_PlanAchievment();
                                add.Prod_OrderPlanningFk = getPlanOrder.t2.Prod_OrderPlanningFK;
                                add.HourCapacity = 0;
                                add.WorkingHour = 0;
                                add.AchiveDate = v.Date;
                                add.Plan_ProductionLineFk = v.LineId;
                                add.Mkt_BOMFK = v.BomId;
                                add.PlanAchievQty = v.qty;
                                add.IsPlanedLine = false;
                                db.Prod_PlanAchievment.Add(add);
                            }
                        }
                    }
                }
                

                db.SaveChanges();
            }

        }

        public void DeleteZeroFollowup(int RefId, int SectionId)
        {
            db = new xOssContext();
            var vData = (from t1 in db.Prod_PlanReferenceOrder
                         join t2 in db.Prod_PlanReferenceOrderSection on t1.ID equals t2.Prod_PlanReferenceOrderFk
                         where t2.Quantity == 0 && t2.Plan_ReferenceSectionFk == SectionId && t1.Prod_ReferenceFK == RefId
                         select new { t1, t2 }).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    var getSlave = db.Prod_PlanOrderSectionSlave.Where(a => a.Prod_PlanReferenceOrderSectionFk == v.t2.ID).ToList();
                    if (getSlave.Any())
                    {
                        db.Prod_PlanOrderSectionSlave.RemoveRange(getSlave);
                    }

                    var deleteRow = db.Prod_PlanReferenceOrderSection.FirstOrDefault(a => a.ID == v.t2.ID);
                    db.Prod_PlanReferenceOrderSection.Remove(deleteRow);
                }
                db.SaveChanges();
            }
        }
    }
}