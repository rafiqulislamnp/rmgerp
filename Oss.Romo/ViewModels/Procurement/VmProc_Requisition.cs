﻿using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrystalDecisions.Web.HtmlReportRender;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.ViewModels.Store;
using System.ComponentModel.DataAnnotations;

namespace Oss.Romo.ViewModels.Procurement
{
    public class VmProc_Requisition
    {
        private xOssContext db;
        public string SearchString { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Prod_Requisition Prod_Requisition { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public User_Department User_Department { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public VmMkt_BOMSlave VmMkt_BOMSlave { get; set; }
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public VmProd_Requisition_Slave VmProd_Requisition_Slave { get; set; }
        public Raw_InternaleTransferSlave Raw_InternaleTransferSlave { get; set; }
        public User_Department FromUser_Department { get; set; }
        public User_Department ToUser_Department { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public string Header { get; set; }
        public bool IsFromProduction { get; set; }
        public string ButtonName { get; set; }
        public decimal? TotalEstimatedPrice { get; set; }
        public IEnumerable<VmProc_Requisition> DataList { get; set; }
        public VmProc_Requisition()
        {
            db = new xOssContext();
            //InitialDataLoad(false);

        }
        public void InitialDataLoad()
        {
            var v = (from t1 in db.Prod_Requisition
                     join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                     where t1.Active == true
                     select new VmProc_Requisition
                     {
                         Prod_Requisition = t1,
                         ToUser_Department = t3,
                         TotalEstimatedPrice = (from x in db.Prod_Requisition_Slave
                                                where x.Prod_RequisitionFK == t1.ID && x.Active == true
                                                select (x.TotalRequired * x.EstimatedPrice)).DefaultIfEmpty(0).Sum()
                     }).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();           
            this.DataList = v;

        }
        public VmProc_Requisition SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Requisition
                     join t2 in db.User_Department on t1.FromUser_DeptFK equals t2.ID
                     join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                     where t1.ID == iD
                     select new VmProc_Requisition
                     {
                         Prod_Requisition = t1,
                         FromUser_Department = t2,
                         ToUser_Department = t3
                     }).FirstOrDefault();
            return v;
        }
        public decimal? TotlaPrice(int id)
        {
            db = new xOssContext();
            var a = (from Prod_Requisition_Slave in db.Prod_Requisition_Slave
                     where Prod_Requisition_Slave.Prod_RequisitionFK == id && Prod_Requisition_Slave.Active == true
                     select (Prod_Requisition_Slave.TotalRequired * Prod_Requisition_Slave.EstimatedPrice)).Sum();
            if (a == null)
            {
                a = 0;
            }
            return a;

        }
        public void InitialDataLoadForSpecificDepartmentRS(int deptid)
        {
            var v = (from t1 in db.Prod_Requisition

                     join t2 in db.User_Department on t1.FromUser_DeptFK equals t2.ID
                     //join t3 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     where t1.FromUser_DeptFK == deptid
                     select new VmProc_Requisition
                     {
                         Prod_Requisition = t1,
                         User_Department = t2

                     }).Where(x => x.Prod_Requisition.Active == true).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();
            List<VmProc_Requisition> list = new List<VmProc_Requisition>();
            foreach (VmProc_Requisition x in v)
            {
                x.TotalEstimatedPrice = TotlaPrice(x.Prod_Requisition.ID);
                list.Add(x);
            }
            this.DataList = list;

        }

        public void InitialDataLoadForSpecificDepartmentRR(int deptid)
        {
            var v = (from t1 in db.Prod_Requisition

                     join t2 in db.User_Department on t1.ToUser_DeptFK equals t2.ID
                     //join t3 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     where t1.ToUser_DeptFK == deptid
                     select new VmProc_Requisition
                     {
                         Prod_Requisition = t1,
                         User_Department = t2

                     }).Where(x => x.Prod_Requisition.Active == true).OrderByDescending(x => x.Prod_Requisition.ID).AsEnumerable();
            List<VmProc_Requisition> list = new List<VmProc_Requisition>();
            foreach (VmProc_Requisition x in v)
            {
                x.TotalEstimatedPrice = TotlaPrice(x.Prod_Requisition.ID);
                list.Add(x);
            }
            this.DataList = list;
        }
    }



    public class VmProc_Purchase
    {
        private xOssContext db;
        public string SearchString { get; set; }
        public Prod_Requisition Prod_Requisition { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public User_Department User_Department { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public User_User User_User { get; set; }
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public VmProd_Requisition_Slave VmProd_Requisition_Slave { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public decimal? Total_Value { get; set; }
        public string Header { get; set; }
        public string ButtonName { get; set; }
        public IEnumerable<VmProc_Purchase> DataList { get; set; }
        public IEnumerable<VmMktPO_Slave> DataList2 { get; set; }
        public VmProc_Purchase()
        {
            db = new xOssContext();
            //InitialDataLoad(false);

        }
        public void InitialDataLoad()
        {
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                     join t4 in db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                     join t5 in db.Prod_Requisition on t1.RequisitionFK equals t5.ID
                     select new VmProc_Purchase
                     {
                         Mkt_PO = t1,
                         Common_Supplier = t2,
                         User_User = t3,
                         Common_Currency = t4,
                         Prod_Requisition = t5
                     }).Where(x => x.Mkt_PO.Active == true && x.Mkt_PO.RequisitionFK != 0).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();
            List<VmProc_Purchase> list = new List<VmProc_Purchase>();
            foreach (VmProc_Purchase x in v)
            {
                x.Total_Value = TotlaValue(x.Mkt_PO.ID);
                list.Add(x);
            }
            this.DataList = list;
        }
        public decimal? TotlaValue(int id)
        {
            db = new xOssContext();
            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     where Mkt_POSlave.Mkt_POFK == id && Mkt_POSlave.Active == true
                     select (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)).Sum();
            if (a == null)
            {
                a = 0;
            }
            return a;

        }

        public void InsertRequisitionItemIntoMkt_PO(int reqid, int poid)
        {

            var r = db.Prod_Requisition_Slave.Where(x => x.Prod_RequisitionFK == reqid && x.Active == true).ToList();
            foreach (var prodRequisitionSlave in r)
            {
                Mkt_POSlave Mkt_POSlave = new Mkt_POSlave();
                Mkt_POSlave.Raw_ItemFK = prodRequisitionSlave.Raw_ItemFK;
                Mkt_POSlave.Consumption = 0;
                Mkt_POSlave.TotalRequired = prodRequisitionSlave.TotalRequired;
                Mkt_POSlave.Common_UnitFK = prodRequisitionSlave.Common_UnitFK;
                Mkt_POSlave.Price = 0;
                Mkt_POSlave.Mkt_POFK = poid;
                Mkt_POSlave.Active = true;
                Mkt_POSlave.FirstCreated = DateTime.Now;
                Mkt_POSlave.FirstCreatedBy = 0;
                Mkt_POSlave.Add();
            }
        }


    }

}