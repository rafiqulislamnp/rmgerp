﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Common
{
    public class VmCommon_Method
    {
        private xOssContext db;
        public Common_Method Common_Method { get; set; }
        public Common_Controller Common_Controller { get; set; }
        public IEnumerable<VmCommon_Method> DataList { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Common_Method in db.Common_Method
                     join Common_Controller in db.Common_Controller
                     on Common_Method.Common_ControllerFk equals Common_Controller.ID
                     select new VmCommon_Method
                     {
                         Common_Method = Common_Method,
                         Common_Controller= Common_Controller
                     }).Where(x => x.Common_Method.Active == true).OrderByDescending(x => x.Common_Method.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Common_Method in db.Common_Method
                     join Common_Controller in db.Common_Controller
                     on Common_Method.Common_ControllerFk equals Common_Controller.ID
                     select new VmCommon_Method
                     {
                         Common_Method = Common_Method,
                         Common_Controller = Common_Controller
                     }).Where(c => c.Common_Method.ID == id).SingleOrDefault();
            this.Common_Method = v.Common_Method;

        }

        public int Add(int userID)
        {

            Common_Method.AddReady(userID);
            return Common_Method.Add();

        }

        public bool Edit(int userID)
        {
            return Common_Method.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Common_Method.Delete(userID);
        }



    }
}
