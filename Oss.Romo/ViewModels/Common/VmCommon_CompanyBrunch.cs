﻿using Oss.Romo.Models;
using Oss.Romo.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Common;

namespace Oss.Romo.ViewModels.Common
{
    public class VmCommon_CompanyBrunch
    {
        private xOssContext db;        
        public Common_CompanyBrunch Common_CompanyBrunch { get; set; }
        public IEnumerable<VmCommon_CompanyBrunch> DataList { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Common_CompanyBrunch in db.Common_CompanyBrunch
                     where Common_CompanyBrunch.Active == true

                     select new VmCommon_CompanyBrunch
                     {
                         Common_CompanyBrunch = Common_CompanyBrunch
                     }).OrderByDescending(x => x.Common_CompanyBrunch.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from Common_CompanyBrunch in db.Common_CompanyBrunch

                     select new VmCommon_CompanyBrunch
                     {
                         Common_CompanyBrunch = Common_CompanyBrunch
                     }).Where(c => c.Common_CompanyBrunch.ID == iD).SingleOrDefault();
            this.Common_CompanyBrunch = v.Common_CompanyBrunch;

        }



        public int Add(int userID)
        {
            Common_CompanyBrunch.AddReady(userID);
            return Common_CompanyBrunch.Add();

        }
        public bool Edit(int userID)
        {
            return Common_CompanyBrunch.Edit(userID);
        }
        public bool Delete(int userID)
        {
            return Common_CompanyBrunch.Delete(userID);
        }
    }
}