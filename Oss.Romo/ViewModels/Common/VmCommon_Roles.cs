﻿
















using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Common
{
    public class VmCommon_Roles
    {
        private xOssContext db;
        public Common_Roles Common_Roles { get; set; }
        public IEnumerable<VmCommon_Roles> DataList { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Common_Roles in db.Common_Roles
                     select new VmCommon_Roles
                     {
                         Common_Roles = Common_Roles
                     }).Where(x => x.Common_Roles.Active == true).OrderByDescending(x => x.Common_Roles.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Common_Roles in db.Common_Roles
                     select new VmCommon_Roles
                     {
                         Common_Roles = Common_Roles
                     }).Where(c => c.Common_Roles.ID == id).SingleOrDefault();
            this.Common_Roles = v.Common_Roles;

        }




        public int Add(int userID)
        {

            Common_Roles.AddReady(userID);
            return Common_Roles.Add();

        }

        public bool Edit(int userID)
        {
            return Common_Roles.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Common_Roles.Delete(userID);
        }



    }
}