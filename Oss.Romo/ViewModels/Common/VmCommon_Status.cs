﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Common
{
    public class VmCommon_Status
    {
        private xOssContext db;
        public IEnumerable<VmCommon_Status> DataList { get; set; }

        public Common_Status Common_Status { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }

        public VmCommon_Status()
        {
            //db = new xOssContext();
            InitialDataLoad();
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_Status
                     select new VmCommon_Status
                     {
                         Common_Status = t1
                     }).Where(x => x.Common_Status.Active == true).OrderByDescending(x => x.Common_Status.ID).AsEnumerable();
            this.DataList = v;
        }


        public void SelectSingle(int Id)
        {
            
            db = new xOssContext();
            var v = (from rc in db.Common_Status
                     select new VmCommon_Status
                     {
                         Common_Status = rc
                     }).Where(c => c.Common_Status.ID == Id).SingleOrDefault();
            this.Common_Status = v.Common_Status;

        }

        public int Add(int userID)
        {
            Common_Status.AddReady(userID);
            return Common_Status.Add();
        }

        public bool Edit(int userID)
        {
            return Common_Status.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Common_Status.Delete(userID);
        }
    }
}