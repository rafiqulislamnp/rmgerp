﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Marketing;

namespace Oss.Romo.ViewModels.Common
{
    public class VmMkt_YarnType
    {
        private xOssContext db;
        public IEnumerable<VmMkt_YarnType> DataList { get; set; }
        public Mkt_YarnType Mkt_YarnType { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }

      
        public VmMkt_YarnType()
        {
            db = new xOssContext();
            
        }

        public void InitialDataLoad()
        {
            var v = (from t1 in db.Mkt_YarnType
                    
                     select new VmMkt_YarnType
                     {
                         Mkt_YarnType = t1,
                       
                     }).Where(x => x.Mkt_YarnType.Active == true).OrderByDescending(x => x.Mkt_YarnType.ID).OrderBy(x => x.Mkt_YarnType.Name).AsEnumerable();
            this.DataList = v;
        }


        public void SelectSingle(int id)
        {
         
            db = new xOssContext();
            var v = (from rc in db.Mkt_YarnType
                   
                     select new VmMkt_YarnType
                     {
                        
                         Mkt_YarnType = rc,
                      
                     }).SingleOrDefault(c => c.Mkt_YarnType.ID == id);
            this.Mkt_YarnType = v.Mkt_YarnType;
           
        }

        public int Add(int userID)
        {
            Mkt_YarnType.AddReady(userID);
            return Mkt_YarnType.Add();
        }

        public bool Edit(int userID)
        {
            return Mkt_YarnType.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_YarnType.Delete(userID);
        }
    }
}