﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Oss.Romo.ViewModels.User;
namespace Oss.Romo.ViewModels.Common
{
    public class VmControllerHelper
    {
        public string Encrypt(string text)
        {
            //byte[] key = new byte[8] { 1, 2, 3, 4, 5, 6, 7, 8 };
            //byte[] iv = new byte[8] { 1, 2, 3, 4, 5, 6, 7, 8 };
            //SymmetricAlgorithm algorithm = DES.Create();
            //ICryptoTransform transform = algorithm.CreateEncryptor(key, iv);
            //byte[] inputbuffer = Encoding.Unicode.GetBytes(text);
            //byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
            //string x = Convert.ToBase64String(outputBuffer);
            //x = x.Replace("/","OOO");

            //return x;
            return text;

        }

        public string Decrypt(string text)
        {
            //text = text.Replace("OOO", "/");

            //byte[] key = new byte[8] { 1, 2, 3, 4, 5, 6, 7, 8 };
            //byte[] iv = new byte[8] { 1, 2, 3, 4, 5, 6, 7, 8 };
            //SymmetricAlgorithm algorithm = DES.Create();
            //ICryptoTransform transform = algorithm.CreateDecryptor(key, iv);
            //byte[] inputbuffer = Convert.FromBase64String(text);
            //byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
            //return Encoding.Unicode.GetString(outputBuffer);
            return text;
        }

        public VmUser_User GetCurrentUser()
        {
            if (HttpContext.Current.Session["One"] != null)
            {
                VmUser_User vmUser_User = (VmUser_User)HttpContext.Current.Session["One"];
                return vmUser_User;
            }
           else
            {
                VmUser_User vmUser_User = new VmUser_User();
                vmUser_User.SelectSingle((1).ToString());
                return vmUser_User;
            }
        }
    }
}