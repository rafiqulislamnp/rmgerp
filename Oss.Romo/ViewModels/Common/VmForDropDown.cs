﻿using Oss.Romo.Models;
using Oss.Romo.Models.User;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.ViewModels.User;
using System.EnterpriseServices;
using System.Globalization;
using System.Web.UI.WebControls;
using CrystalDecisions.Web.HtmlReportRender;
using Oss.Romo.Models.Accounts;
using Oss.Romo.ViewModels.Marketing;

namespace Oss.Romo.ViewModels.Common
{

    public class DDL
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public int UserID { get; set; }
    }


    public enum CurrencyType
    {
        USD,
        BDT
    }
    public class InWordFunction
    {
        public static String ConvertToWords(String numb)
        {
            String val, wholeNo = numb;
            String andStr = "", pointStr = "";
            String endStr = "Only";
            try
            {
                int decimalPlace = numb.IndexOf(".", StringComparison.Ordinal);
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    var points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and"; // just to separate whole numbers from points/cents  
                        endStr = "Paisa " + endStr; //Cents  
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch
            {
                return "Invalid number......";
            }
            return val;
        }
        private static String ConvertWholeNumber(String number)
        {
            string word = "";
            try
            {
                bool isDone = false; //test if already translated
                double dblAmt = (Convert.ToDouble(number));
                //if ((dblAmt > 0) && number.StartsWith("0"))
                if (dblAmt > 0)
                {
                    //test for zero or digit zero in a nuemric

                    int numDigits = number.Length;
                    int pos = 0; //store digit grouping
                    String place = ""; //digit grouping name:hundres,thousand,etc...
                    switch (numDigits)
                    {
                        case 1: //ones' range

                            word = Ones(number);
                            isDone = true;
                            break;
                        case 2: //tens' range
                            word = Tens(number);
                            isDone = true;
                            break;
                        case 3: //hundreds' range
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4: //thousands' range
                        case 5:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 6: //Lakh' range
                        case 7:
                            pos = (numDigits % 6) + 1;
                            place = " Lakh ";
                            break;
                        case 8: //Lakh's range
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                        case 16:
                            pos = (numDigits % 8) + 1;
                            place = " crore ";
                            break;
                        //add extra case options for anything above Billion...
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {
                        //if transalation is not done, continue...(Recursion comes in now!!)
                        if (number.Substring(0, pos) != "0" && number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(number.Substring(0, pos)) + place +
                                       ConvertWholeNumber(number.Substring(pos));
                            }
                            catch
                            {
                                //ignore
                            }
                        }
                        else
                        {
                            word = ConvertWholeNumber(number.Substring(0, pos)) +
                                   ConvertWholeNumber(number.Substring(pos));
                        }

                        //check for trailing zeros
                        //if (beginsZero) word = " and " + word.Trim();
                    }
                    //ignore digit grouping names
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch
            {
                return "Invalid number.....";
            }
            return word.Trim();
        }

        private static String Tens(String number)
        {
            int numberiInt32 = Convert.ToInt32(number);
            String name = null;
            switch (numberiInt32)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Forty ";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (numberiInt32 > 0)
                    {
                        name = Tens(number.Substring(0, 1) + "0") + " " + Ones(number.Substring(1));
                    }
                    break;
            }
            return name;
        }
        private static String Ones(String number)
        {
            int numberiInt32 = Convert.ToInt32(number);
            String name = "";
            switch (numberiInt32)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }

        private static String ConvertDecimals(String number)
        {
            String cd = "";
            foreach (char t in number)
            {
                var digit = t.ToString();
                string engOne;
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = Ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }
        public static String Comma(decimal amount)
        {
            string result;
            string amt;

            amt = amount.ToString(CultureInfo.InvariantCulture);
            int aaa = amount.ToString(CultureInfo.InvariantCulture).IndexOf(".", 0, StringComparison.Ordinal);
            var amtPaisa = amount.ToString(CultureInfo.InvariantCulture).Substring(aaa + 1);

            if (amt == amtPaisa)
            {
                amtPaisa = "";
            }
            else
            {
                amt = amount.ToString(CultureInfo.InvariantCulture).Substring(0, amount.ToString(CultureInfo.InvariantCulture).IndexOf(".", 0, StringComparison.Ordinal));
                amt = (amt.Replace(",", ""));
            }
            switch (amt.Length)
            {
                case 9:
                    if (amtPaisa == "")
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 2) + "," +
                                 amt.Substring(4, 2) + "," + amt.Substring(6, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 2) + "," +
                                 amt.Substring(4, 2) + "," + amt.Substring(6, 3) + "." +
                                 amtPaisa;
                    }
                    break;
                case 8:
                    if (amtPaisa == "")
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 2) + "," +
                                 amt.Substring(3, 2) + "," + amt.Substring(5, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 2) + "," +
                                 amt.Substring(3, 2) + "," + amt.Substring(5, 3) + "." +
                                 amtPaisa;
                    }
                    break;
                case 7:
                    if (amtPaisa == "")
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 2) + "," +
                                 amt.Substring(4, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 2) + "," +
                                 amt.Substring(4, 3) + "." + amtPaisa;
                    }
                    break;
                case 6:
                    if (amtPaisa == "")
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 2) + "," +
                                 amt.Substring(3, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 2) + "," +
                                 amt.Substring(3, 3) + "." + amtPaisa;
                    }
                    break;
                case 5:
                    if (amtPaisa == "")
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 3) + "." +
                                 amtPaisa;
                    }
                    break;
                case 4:
                    if (amtPaisa == "")
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 3) + "." +
                                 amtPaisa;
                    }
                    break;
                default:
                    if (amtPaisa == "")
                    {
                        result = amt;
                    }
                    else
                    {
                        result = amt + "." + amtPaisa;
                    }
                    break;
            }
            return result;
        }
    }
    public class SelectListItems
    {
        public string Text { get; set; }
        public decimal Value { get; set; }
    }
    public class SelectListItems2
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
    public class VmForDropDown
    {
        private xOssContext db;
        public List<object> DDownData { get; set; }
        public SelectList DDData { get; set; }
        public User_User User_User { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }

        public VmForDropDown()
        {
            VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            User_User = vMUser.User_User;
        }

        public string NumberToWords(decimal d, CurrencyType ct)
        {
            string s = d.ToString("F");
            string s1 = "";
            string s2 = "";
            int n1 = 0;
            int n2 = 0;
            if (s.Contains("."))
            {
                s1 = s.Substring(0, s.IndexOf("."));
                s2 = s.Replace(s1 + ".", "");
            }
            else
            {
                s1 = s;
                s2 = "00";
            }
            int.TryParse(s1, out n1);
            int.TryParse(s2, out n2);
            s1 = NumberToWords(n1);
            s2 = NumberToWords(n2);
            if (ct == CurrencyType.USD)
            {
                s = " U.S Dollar " + s1 + " & Cents " + s2 + " Only";
            }
            else if (ct == CurrencyType.BDT)
            {
                s = s1 + " Taka  & " + s2 + " Paisa Only";
            }
            return s;
        }
        public String NumberToComma(decimal amount)
        {
            string result = "";
            string amt = "";
            string amt_paisa = "";

            amt = amount.ToString();
            int aaa = amount.ToString().IndexOf(".", 0);
            amt_paisa = amount.ToString().Substring(aaa + 1);

            if (amt == amt_paisa)
            {
                amt_paisa = "";
            }
            else
            {
                amt = amount.ToString().Substring(0, amount.ToString().IndexOf(".", 0));
                amt = (amt.Replace(",", "")).ToString();
            }
            switch (amt.Length)
            {
                case 9:
                    if (amt_paisa == "")
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 2) + "," +
                                 amt.Substring(4, 2) + "," + amt.Substring(6, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 2) + "," +
                                 amt.Substring(4, 2) + "," + amt.Substring(6, 3) + "." +
                                 amt_paisa;
                    }
                    break;
                case 8:
                    if (amt_paisa == "")
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 2) + "," +
                                 amt.Substring(3, 2) + "," + amt.Substring(5, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 2) + "," +
                                 amt.Substring(3, 2) + "," + amt.Substring(5, 3) + "." +
                                 amt_paisa;
                    }
                    break;
                case 7:
                    if (amt_paisa == "")
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 2) + "," +
                                 amt.Substring(4, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 2) + "," +
                                 amt.Substring(4, 3) + "." + amt_paisa;
                    }
                    break;
                case 6:
                    if (amt_paisa == "")
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 2) + "," +
                                 amt.Substring(3, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 2) + "," +
                                 amt.Substring(3, 3) + "." + amt_paisa;
                    }
                    break;
                case 5:
                    if (amt_paisa == "")
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 2) + "," + amt.Substring(2, 3) + "." +
                                 amt_paisa;
                    }
                    break;
                case 4:
                    if (amt_paisa == "")
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 3);
                    }
                    else
                    {
                        result = amt.Substring(0, 1) + "," + amt.Substring(1, 3) + "." +
                                 amt_paisa;
                    }
                    break;
                default:
                    if (amt_paisa == "")
                    {
                        result = amt;
                    }
                    else
                    {
                        result = amt + "." + amt_paisa;
                    }
                    break;
            }
            return result;
        }
        private string NumberToWords(int number)
        {
            if (number == 0)
                return "Zero";

            if (number < 0)
                return "Minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[]
                {
                    "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven",
                    "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"
                };
                var tensMap = new[]
                    {"Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        //words += "-" + unitsMap[number % 10];
                        words += unitsMap[number % 10];
                }
            }

            return words;
        }

        public List<object> GetAcc_SupplierPaymentForDropDown()
        {
            db = new xOssContext();
            var payList = new List<object>();

            foreach (var p in db.Acc_SupplierPaymentType.Where(x => x.Active == true))
            {
                payList.Add(new { Text = p.Name, Value = p.ID });
            }
            return payList;
        }

        public List<object> GetProductionLineForDropDown()
        {
            db = new xOssContext();
            var lineList = new List<object>();

            foreach (var line in db.Plan_ProductionLine.Where(x => x.Active == true))
            {
                lineList.Add(new { Text = line.Name, Value = line.ID });
            }
            return lineList;
        }
        public List<object> GetProductionStepForDropDownReport()
        {
            db = new xOssContext();
            var lineList = new List<object>();

            foreach (var line in db.Plan_OrderProcess.Where(x => x.Active == true))
            {
                lineList.Add(new { Text = line.StepName, Value = line.StepName });
            }
            return lineList;
        }
        public List<object> GetProdMachineDIA()
        {
            db = new xOssContext();
            var diaList = new List<object>();

            foreach (var dia in db.Prod_MachineDIA.Where(x => x.Active == true))
            {
                diaList.Add(new { Text = dia.Name, Value = dia.ID });
            }
            return diaList;
        }
        public List<object> GetShipmentOrderDeliverdScheduleByBOM()
        {
            db = new xOssContext();
            var list = new List<object>();
            var items = (from t1 in db.Shipment_OrderDeliverdSchedule
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryFk equals t2.ID
                         join t3 in db.Common_Country on t2.Destination equals t3.ID
                         where t1.Active == true

                         select new
                         {
                             Port = t3.Name + " | " + t2.PortNo + "(" + t1.Date + ") Quantity - " + t1.DeliverdQty,
                             ID = t1.ID,

                         }).OrderByDescending(x => x.ID);
            foreach (var item in items)
            {
                list.Add(new { Text = item.Port, Value = item.ID });
            }
            return list;
        }
        public List<object> GetShipmentOrderDeliverdScheduleByBom(int challanId)
        {
            db = new xOssContext();
            var list = new List<object>();
            var v = (from t1 in db.Shipment_OrderDeliverdSchedule
                     join t2 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t2.ID
                     where t1.Shipment_CNFDelivaryChallanFk == challanId
                     select new
                     {
                         Text = t1.DeliverdQty + " | " + SqlFunctions.DateName("day", t1.Date) + "/" + SqlFunctions.DateName("month", t1.Date) + "/" + SqlFunctions.DateName("year", t1.Date) + "",
                         Value = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                list.Add(new { Text = item.Text, Value = item.Value });
            }
            return list;
        }
        public List<object> GetProductionSVNameForDropDown()
        {
            db = new xOssContext();
            var avList = new List<object>();

            foreach (var sv in db.Prod_SVName.Where(x => x.Active == true))
            {
                avList.Add(new { Text = sv.Name, Value = sv.ID });
            }
            return avList;
        }

        public List<object> GetProductionChiefForDropDown()
        {
            db = new xOssContext();
            var chiefList = new List<object>();

            foreach (var chief in db.Prod_LineChief.Where(x => x.Active == true))
            {
                chiefList.Add(new { Text = chief.Name, Value = chief.ID });
            }
            return chiefList;
        }

        public List<object> GetProductionStepForDropDown()
        {
            db = new xOssContext();
            var StepList = new List<object>();

            foreach (var step in db.Plan_OrderProcess.Where(x => x.Active == true))
            {
                StepList.Add(new { Text = step.StepName, Value = step.ID });
            }
            return StepList;
        }

        public List<object> GetUserRoleForDropDown()
        {
            db = new xOssContext();
            var userRoleList = new List<object>();

            foreach (var userRole in db.User_Role.Where(x => x.Active == true))
            {
                userRoleList.Add(new { Text = userRole.Name, Value = userRole.ID });
            }
            return userRoleList;
        }



        public List<object> GetTempRaw_CategoryForEdit(int? id)
        {
            db = new xOssContext();
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }

        public List<object> GetTempRaw_CategoryForEdit()
        {
            db = new xOssContext();
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(s => s.Active == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }

        public List<object> GetProductionLine()
        {
            db = new xOssContext();
            var LineList = new List<object>();

            foreach (var plan in db.Plan_ProductionLine.Where(
                x =>
                    x.Active == true))
            {
                LineList.Add(new { Text = plan.Name, Value = plan.ID });
            }
            return LineList;
        }

        internal List<object> GetUnitForDropDown()
        {
            db = new xOssContext();
            var unitList = new List<object>();

            foreach (var unit in db.Common_Unit.Where(d => d.Active == true))
            {
                unitList.Add(new { Text = unit.Name, Value = unit.ID });
            }
            return unitList;
        }

        public List<object> GetBuyerForDropDown()
        {
            db = new xOssContext();
            var BuyerList = new List<object>();
            BuyerList.Add(new { Text = "All Buyer", Value = 0 });
            foreach (var buyer in db.Mkt_Buyer.Where(d => d.Active == true && d.ID != 1))
            {
                BuyerList.Add(new { Text = buyer.Name, Value = buyer.ID });
            }
            return BuyerList;
        }
        public List<object> GetUserMenuListForDropDown()
        {
            db = new xOssContext();
            var userMenuList = new List<object>();

            foreach (var userMenu in db.User_Menu.Where(d => d.Active == true))
            {
                userMenuList.Add(new { Text = userMenu.Name, Value = userMenu.ID });
            }
            return userMenuList;
        }
        public List<object> GetUserSubMenuForDropDown()
        {
            db = new xOssContext();
            var userSubMenuList = new List<object>();

            foreach (var userSubMenu in db.User_SubMenu.Where(d => d.Active == true))
            {
                userSubMenuList.Add(new { Text = userSubMenu.Name, Value = userSubMenu.ID });
            }
            return userSubMenuList;
        }
        public List<object> GetCurrencyForDropDown()
        {
            db = new xOssContext();
            var CurrencyList = new List<object>();

            foreach (var buyer in db.Common_Currency.Where(d => d.Active == true))
            {
                CurrencyList.Add(new { Text = buyer.Name, Value = buyer.ID });
            }
            return CurrencyList;
        }

        public List<object> GetInvoiceForDropDown(int id)
        {
            db = new xOssContext();
            var InvoiceList = new List<object>();
            var invoice = (from t1 in db.Commercial_Invoice
                           join t2 in db.Commercial_InvoiceSlave on t1.ID equals t2.Commercial_InvoiceFk
                           //join t3 in db.Shipment_CNFDelivaryChallan on t2.Shipment_CNFDelivaryChallanFk equals t3.ID
                           //join t5 in db.Commercial_MasterLCBuyerPO on t2.MktBOMFK equals t5.MKTBOMFK


                           where t1.Active == true
                           //&& t5.Commercial_MasterLCFK == id
                           select new
                           {
                               ID = t1.ID,
                               Name = t1.InvoiceNo
                           }).Distinct().ToList();


            foreach (var v in invoice)
            {
                InvoiceList.Add(new { Text = v.Name, Value = v.ID });
            }
            return InvoiceList;
        }

        public List<object> GetMktOrderedItemForDropDown()
        {
            db = new xOssContext();
            var ItemList = new List<object>();
            var item = (from t1 in db.Mkt_Item
                        join t2 in db.Mkt_BOM
                            on t1.ID equals t2.Mkt_ItemFK
                        join t3 in db.Mkt_PO
                            on t2.ID equals t3.Mkt_BOMFK
                        where t1.Active == true
                              && t3.IsAuthorize == true
                        select new
                        {
                            ID = t2.Mkt_ItemFK,
                            Name = t1.Name
                        }).Distinct().ToList();

            foreach (var x in item)
            {
                ItemList.Add(new { Text = x.Name, Value = x.ID });
            }
            return ItemList;
        }

        public List<object> GetMktItemForDropDown()
        {
            db = new xOssContext();
            var ItemList = new List<object>();

            foreach (var item in db.Mkt_Item.Where(d => d.Active == true))
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }

        public List<object> GetMktSubCategoryForDropDown()
        {
            db = new xOssContext();
            var sCategoryList = new List<object>();

            foreach (var sCategory in db.Mkt_SubCategory.Where(x => x.Active == true))
            {
                sCategoryList.Add(new { Text = sCategory.Name, Value = sCategory.ID });
            }
            return sCategoryList;
        }

        public List<object> GetMktCategoryForDropDown()
        {
            db = new xOssContext();
            var categoryList = new List<object>();

            foreach (var category in db.Mkt_Category.Where(x => x.Active == true))
            {
                categoryList.Add(new { Text = category.Name, Value = category.ID });
            }
            return categoryList;
        }

        int? sabCatID;
        int? catID;
        int? mktSabCatID;
        int? mktCatID;

        public List<object> GetTempMktSubCategoryForEdit(string sCid)
        {
            int id = 0;
            sCid = this.VmControllerHelper.Decrypt(sCid);
            Int32.TryParse(sCid, out id);
            db = new xOssContext();
            var scl = new List<object>();

            mktSabCatID = (from t3 in db.Mkt_Item where t3.ID == id select t3.Mkt_SubCategoryFK).FirstOrDefault();
            mktCatID = (
                from t2 in db.Mkt_SubCategory
                where t2.ID == mktSabCatID
                select t2.Mkt_CategoryFK

            ).FirstOrDefault();

            var x = (from t1 in db.Mkt_SubCategory
                     where t1.Mkt_CategoryFK == mktCatID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
            ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }

        public List<object> GetTempMktItemForEdit(string id)
        {

            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();
            var scl = new List<object>();


            mktSabCatID = (from t3 in db.Mkt_Item where t3.ID == iD select t3.Mkt_SubCategoryFK).FirstOrDefault();

            mktCatID = (
                from t2 in db.Mkt_SubCategory
                where t2.ID == mktSabCatID
                select t2.Mkt_CategoryFK

            ).FirstOrDefault();
            var x = (from t1 in db.Mkt_Item
                     where t1.Mkt_SubCategoryFK == mktSabCatID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
            ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }



        public List<object> GetRawItemForDropDown()
        {
            db = new xOssContext();
            var rawItemList = new List<object>();

            foreach (var raw_Item in db.Raw_Item.Where(d => d.Active == true))
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });
            }
            return rawItemList;
        }
        public List<object> GetMkrYarnType()
        {
            db = new xOssContext();
            var yarnTypeList = new List<object>();

            foreach (var yarnType in db.Mkt_YarnType.Where(d => d.Active == true))
            {
                yarnTypeList.Add(new { Text = yarnType.Name, Value = yarnType.ID });
            }
            return yarnTypeList;
        }
        public List<object> GetSupplierForDropDown()
        {
            db = new xOssContext();
            var supplierList = new List<object>();

            foreach (var supplier in db.Common_Supplier.Where(d => d.Active == true).OrderBy(x => x.Name))
            {
                supplierList.Add(new { Text = supplier.Name, Value = supplier.ID });
            }
            return supplierList;
        }

        public List<object> GetSupplierForCreatePO()
        {
            db = new xOssContext();
            var supplierList = new List<object>();

            foreach (var supplier in db.Common_Supplier.Where(d => d.Active == true && d.ID != 1).OrderBy(x => x.Name))
            {
                supplierList.Add(new { Text = supplier.Name, Value = supplier.ID });
            }
            return supplierList;
        }

        public List<object> GetRaw_CategoryForDropDown()
        {
            db = new xOssContext();
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true && x.IsNew == true && x.ID != 1020))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }
        public List<object> GetAllRawCategory()
        {
            db = new xOssContext();
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true && x.IsNew == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }


        public List<object> GetAllRaw_CategoryForDropDown()
        {
            db = new xOssContext();
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true && x.IsNew == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }

        public List<object> GetRaw_SubCategoryForDropDown()
        {
            db = new xOssContext();
            var raw_subCategoryList = new List<object>();

            foreach (var raw_subCategory in db.Raw_SubCategory.Where(x => x.Active == true && x.IsNew == true))
            {
                raw_subCategoryList.Add(new { Text = raw_subCategory.Name, Value = raw_subCategory.ID });
            }
            return raw_subCategoryList;
        }
        public List<object> GetRawSubCategoryByCategory()
        {
            db = new xOssContext();
            var raw_subCategoryList = new List<object>();

            foreach (var raw_subCategory in db.Raw_SubCategory.Where(x => x.Active == true && x.Raw_CategoryFK == 1020 && x.ID != 2044))
            {
                raw_subCategoryList.Add(new { Text = raw_subCategory.Name, Value = raw_subCategory.ID });
            }
            return raw_subCategoryList;
        }
        public List<object> GetBOMForDropDown()
        {
            db = new xOssContext();
            var Mkt_BOMList = new List<object>();

            foreach (var bom in db.Mkt_BOM.Where(d => d.Active == true))
            {
                Mkt_BOMList.Add(new { Text = bom.CID, Value = bom.ID });
            }
            return Mkt_BOMList;
        }

        public List<object> GetTempRawSubCategoryForEdit(int? id)
        {
            db = new xOssContext();
            var scl = new List<object>();

            sabCatID = (from t3 in db.Raw_Item where t3.ID == id select t3.Raw_SubCategoryFK).FirstOrDefault();
            catID = (
                from t2 in db.Raw_SubCategory
                where t2.ID == sabCatID
                select t2.Raw_CategoryFK

            ).FirstOrDefault();

            var x = (from t1 in db.Raw_SubCategory
                     where t1.Raw_CategoryFK == catID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
            ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }

        public List<object> GetTempRaw_ItemForEdit(int? id)
        {
            db = new xOssContext();
            var rawItemList = new List<object>();
            var x = (from t1 in db.Raw_Item
                     where t1.Raw_SubCategoryFK ==
                           (from t2 in db.Raw_Item where t2.ID == id select t2.Raw_SubCategoryFK).FirstOrDefault()
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
            ).ToList();


            foreach (var raw_Item in x)
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });

            }
            return rawItemList;
        }

        public List<object> GetMktPODropDown()
        {
            db = new xOssContext();
            var POList = new List<object>();

            foreach (var po in db.Mkt_PO.Where(d => d.Active == true))
            {
                POList.Add(new { Text = po.CID, Value = po.ID });
            }
            return POList;
        }

        public List<object> GetCommonOrderDropDown()
        {
            db = new xOssContext();
            var CommonOrderList = new List<object>();

            foreach (var order in db.Common_TheOrder.Where(d => d.Active == true))
            {
                CommonOrderList.Add(new { Text = order.CID, Value = order.ID });
            }
            return CommonOrderList;
        }

        //public List<object> GetMasterLCDocsDropDown()
        //{
        //    db = new xOssContext();
        //    var MasterLCDocsList = new List<object>();

        //    foreach (var Commercial__MasterLCDocs in db.Commercial__MasterLCDocs.Where(d => d.Active == true))
        //    {
        //        MasterLCDocsList.Add(new { Text = Commercial__MasterLCDocs.Name, Value = Commercial__MasterLCDocs.ID });
        //    }
        //    return MasterLCDocsList;
        //}

        public List<object> GetDropDownRawItem()
        {
            db = new xOssContext();
            var rawItemList = new List<object>();

            foreach (var raw_Item in db.Raw_Item.Where(d => d.Active == true))
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });
            }
            return rawItemList;
        }

        public List<object> GetBropDownBOMCID()
        {
            db = new xOssContext();
            var List = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM
                         on t1.ID equals t2.Common_TheOrderFk
                     where t2.Active == true
                     select new
                     {
                         CID = t1.CID + "/" + t2.Style,
                         ID = t2.ID
                     }).OrderByDescending(x => x.ID);

            foreach (var cid in v)
            {
                List.Add(new { Text = cid.CID, Value = cid.ID });
            }
            return List;
        }

        //public List<object> GetDropDownBOM()
        //{
        //    db = new xOssContext();
        //    var List = new List<object>();
        //    var v = (from t1 in db.Mkt_BOMSlave
        //             join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
        //             join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //             join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
        //             where t2.Active == true
        //             select new
        //             {
        //                 CID = t4.Name + "(" + t3.CID + "/" + t2.Style + ")",
        //                 ID = t1.ID
        //             }).OrderByDescending(x => x.ID);

        //    foreach (var cid in v)
        //    {
        //        List.Add(new { Text = cid.CID, Value = cid.ID });
        //    }
        //    return List;
        //}
        //public List<object> GetPOSlavesByBOM()
        //{
        //    db = new xOssContext();
        //    var List = new List<object>();
        //    var v = (from t1 in db.Mkt_POSlave
        //        join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
        //             join t2 in db.Mkt_BOM on t1.m equals t2.ID
        //        join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //        join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
        //        where t2.Active == true
        //        select new
        //        {
        //            CID = t4.Name + "(" + t3.CID + "/" + t2.Style + ")",
        //            ID = t1.ID
        //        }).OrderByDescending(x => x.ID);

        //    foreach (var cid in v)
        //    {
        //        List.Add(new { Text = cid.CID, Value = cid.ID });
        //    }
        //    return List;
        //}
        public List<object> GetDropDownTransitionItemInventory()
        {
            db = new xOssContext();
            var List = new List<object>();
            var v = (from t1 in db.Prod_TransitionItemInventory
                     where t1.Active == true
                     select new
                     {
                         Name = t1.ItemName,
                         ID = t1.ID
                     }).OrderByDescending(x => x.ID);

            foreach (var item in v)
            {
                List.Add(new { Text = item.Name, Value = item.ID });
            }
            return List;
        }

        public List<object> GetRaw_ItemDropDownForProductionEdit(int? id)
        {
            db = new xOssContext();
            var rawItemList = new List<object>();
            var requisitaionNo = from t1 in db.Prod_Requisition
                                 where t1.ID == id
                                 select t1.Mkt_BOMFK;
            ;
            var item = (from t1 in db.Mkt_BOMSlave
                        join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                        where requisitaionNo.Contains(t1.Mkt_BOMFK)
                        select new
                        {
                            Name = t2.Name + " | " + t1.Description,
                            ID = t1.ID
                        });

            foreach (var v in item)
            {
                rawItemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return rawItemList;
        }

        public List<object> GetRaw_ItemDropDownForEdit(int? id)
        {
            db = new xOssContext();
            var rawItemList = new List<object>();
            var x = (from t1 in db.Raw_Item
                     where t1.IsNew == true && t1.Raw_SubCategoryFK == (from t2 in db.Raw_Item where t2.ID == id select t2.Raw_SubCategoryFK).FirstOrDefault()
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
                   ).ToList();


            foreach (var raw_Item in x)
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });

            }
            return rawItemList;
        }

        public List<object> GetRawSubCategoryDropDownForEdit(string rawSCId)
        {
            int id = 0;
            rawSCId = this.VmControllerHelper.Decrypt(rawSCId);
            Int32.TryParse(rawSCId, out id);
            db = new xOssContext();
            var scl = new List<object>();

            sabCatID = (from t3 in db.Raw_Item where t3.IsNew == true && t3.ID == id select t3.Raw_SubCategoryFK).FirstOrDefault();
            catID = (
                from t2 in db.Raw_SubCategory
                where t2.IsNew == true && t2.ID == sabCatID
                select t2.Raw_CategoryFK

            ).FirstOrDefault();

            var x = (from t1 in db.Raw_SubCategory
                     where t1.IsNew == true && t1.Raw_CategoryFK == catID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
            ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }


        public List<object> GetRaw_CategoryDropDownForEdit(int? id)
        {
            db = new xOssContext();
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true && x.IsNew == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }

        public List<object> GetPlan_OrderProcessCategory()
        {
            db = new xOssContext();
            var planCategoryList = new List<object>();

            foreach (var planCategory in db.Plan_OrderProcessCategory.Where(x => x.Active == true))
            {
                planCategoryList.Add(new { Text = planCategory.Name, Value = planCategory.ID });
            }
            return planCategoryList;
        }

        public List<object> GetRaw_ItemDropDownEdit(int? id)
        {
            db = new xOssContext();
            var rawItemList = new List<object>();
            var x = (from t1 in db.Raw_Item
                     where t1.Raw_SubCategoryFK ==
                           (from t2 in db.Raw_Item where t2.ID == id select t2.Raw_SubCategoryFK).FirstOrDefault()
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
            ).ToList();


            foreach (var raw_Item in x)
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });

            }
            return rawItemList;
        }

        public List<object> GetDropDownAuthorizeBOMCID()
        {
            //var xx = GetInCompletOrderIDs();
            db = new xOssContext();
            var List = new List<object>();
            var tempv = (from t1 in db.Common_TheOrder
                         join t2 in db.Mkt_BOM
                             on t1.ID equals t2.Common_TheOrderFk
                         //join t4 in db.Mkt_PO
                         //on t2.ID equals t4.Mkt_BOMFK
                         //join t3 in db.User_User
                         //on t1.FirstCreatedBy equals t3.ID
                         where t1.Active == true
                               && t2.Active == true // && t4.IsAuthorize == true
                                                    //&& xx.Contains(t1.ID)
                         select new DDL
                         {
                             Name = t1.CID + "/" + t2.Style,
                             ID = t2.ID,
                             //UserID = t3.ID

                         }).OrderByDescending(x => x.ID).ToList();

            List<DDL> v = new List<DDL>();
            if (User_User.User_AccessLevelFK == 1 && User_User.User_DepartmentFK == 8)
            {

                v = tempv.Where(x => x.UserID == this.User_User.ID).ToList();
            }
            else
            {
                v = tempv.ToList();
            }

            foreach (var cid in v)
            {
                List.Add(new { Text = cid.Name, Value = cid.ID });
            }
            return List;
        }

        internal List<object> GetBTBItemDropDown()
        {
            db = new xOssContext();
            var btbItemList = new List<object>();

            foreach (var btb_Item in db.Commercial_BTBItem.Where(d => d.Active == true))
            {
                btbItemList.Add(new { Text = btb_Item.Name, Value = btb_Item.ID });
            }
            return btbItemList;
        }

        public List<object> GetDropDownBOMCID()
        {
            var xx = GetInCompletOrderIDs();
            db = new xOssContext();
            var List = new List<object>();
            var tempv = (from t1 in db.Common_TheOrder
                         join t2 in db.Mkt_BOM
                             on t1.ID equals t2.Common_TheOrderFk

                         join t3 in db.User_User
                        on t1.FirstCreatedBy equals t3.ID
                         where t1.Active == true
                               && t2.Active == true
                               && t1.IsComplete == false
                               && xx.Contains(t1.ID)
                         select new DDL
                         {
                             Name = t1.CID + "/" + t2.Style,
                             ID = t2.ID,
                             UserID = t3.ID

                         }).OrderByDescending(x => x.ID).ToList();

            List<DDL> v = new List<DDL>();
            if (User_User.User_AccessLevelFK == 1 && User_User.User_DepartmentFK == 8)
            {
                v = tempv.Where(x => x.UserID == this.User_User.ID).ToList();
            }
            else
            {
                v = tempv.ToList();
            }

            foreach (var cid in v)
            {
                List.Add(new { Text = cid.Name, Value = cid.ID });
            }
            return List;
        }

        private List<int> GetInCompletOrderIDs()
        {
            db = new xOssContext();
            List<int> x = new List<int>();
            using (var ctx = new xOssContext())
            {
                var a = ctx.Common_TheOrder.SqlQuery("EXEC  dbo.GETINCOMPLETEORDER").ToList();
                foreach (var l in a)
                {
                    x.Add(l.ID);
                }
            }
            return x;
        }
        public List<object> GetStyleItemByBomPoSlave(int id)
        {
            db = new xOssContext();
            var itemList = new List<object>();

            var item = (from t1 in db.Mkt_POSlave
                        join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                        join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                        where t1.Active == true && t2.Mkt_BOMFK == id
                        select new
                        {
                            Name = t3.Name + " | " + t1.Description,
                            ID = t1.ID
                        });

            foreach (var v in item)
            {
                itemList.Add(new { Text = v.Name, Value = v.ID});
            }

            return itemList;
        }
        public List<object> GetOrderIdDropDown()
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                     join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                     where t1.Active == true && t2.Active == true && t1.IsComplete == false
                     select new DDL
                     {
                         Name = t1.CID + "/" + t2.Style,
                         ID = t2.ID,
                         UserID = t3.ID

                     }).OrderByDescending(x => x.ID).ToList();
            foreach (var order in v)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }
        public List<object> GetOrderIdDropDownasdf(int? id)
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                     join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                     where t2.Mkt_BOMFK == id
                     select new
                     {
                         Name = t3.Name + " | " + t1.Description,
                         ID = t1.ID
                     });
            foreach (var order in v)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetAllOrderAndStyle()
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                     where t1.Active == true && t2.Active == true && t1.IsComplete==false
                     select new DDL
                     {
                         Name = t1.CID + "/" + t2.Style,
                         ID = t2.ID
                     }).OrderByDescending(x => x.ID).ToList();
            foreach (var order in v)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetAllOrderByDeliverdSchedule(int id)
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                     join t3 in db.Mkt_OrderDeliverySchedule on t2.ID equals t3.Mkt_BOMFk

                     where t1.Active == true && t3.ID == id
                           && t2.Active == true

                     select new DDL
                     {
                         Name = t1.CID + "/" + t2.Style,
                         ID = t2.ID,


                     }).OrderByDescending(x => x.ID).ToList();
            foreach (var order in v)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }
        public List<object> GetStyleByOrderDropDown(int id)
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM
                     on t1.ID equals t2.Common_TheOrderFk

                     join t3 in db.User_User
                     on t1.FirstCreatedBy equals t3.ID
                     where t1.Active == true
                     && t2.Active == true
                     && t2.Common_TheOrderFk == id
                     select new DDL
                     {
                         Name = t1.CID + "/" + t2.Style,
                         ID = t2.ID,
                         UserID = t3.ID

                     }).OrderByDescending(x => x.ID).ToList();
            foreach (var order in v)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetOrderByBuyerDropDown()
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                     //join t5 in db.Commercial_MasterLC on t1.Mkt_BuyerFK equals t5.Mkt_BuyerFK
                     //join t4 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t4.ID
                     join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                     where t1.Active == true && t2.Active == true //&& t5.Mkt_BuyerFK == t1.Mkt_BuyerFK
                     select new DDL
                     {
                         Name = t1.CID + "/" + t2.Style,
                         ID = t2.ID,
                         UserID = t3.ID
                     }).OrderByDescending(x => x.ID).ToList();
            foreach (var order in v)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetOrderByBuyer(int id)
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM
                     on t1.ID equals t2.Common_TheOrderFk
                     join t3 in db.User_User
                     on t1.FirstCreatedBy equals t3.ID
                     where !(from x in db.Commercial_MasterLCBuyerPO join y in db.Commercial_MasterLC on x.Commercial_MasterLCFK equals y.ID where x.Active == true && y.Active == true select x.MKTBOMFK)
                          .Contains(t2.ID)
                      && t1.Mkt_BuyerFK == id
                      && t1.Active == true
                      && t1.IsComplete == false
                      && t2.Active == true


                     select new DDL
                     {
                         Name = t1.CID + "/" + t2.Style,
                         ID = t2.ID,
                         UserID = t3.ID

                     }).OrderByDescending(x => x.ID).ToList();
            foreach (var order in v)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetOrdersContainsMLC(int id)
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM
                         on t1.ID equals t2.Common_TheOrderFk

                     join t5 in db.Commercial_MasterLCBuyerPO
                         on t2.ID equals t5.MKTBOMFK

                     join t4 in db.Commercial_MasterLC
                         on t5.Commercial_MasterLCFK equals t4.ID

                     join t3 in db.User_User
                         on t1.FirstCreatedBy equals t3.ID
                     where t1.Active == true
                           && t2.Active == true
                           && t4.ID == id
                     //&& t5.Mkt_BuyerFK == t1.Mkt_BuyerFK

                     select new DDL
                     {
                         Name = t1.CID + "/" + t2.Style,
                         ID = t2.ID,
                         UserID = t3.ID

                     }).OrderByDescending(x => x.ID).ToList();
            foreach (var order in v)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        internal List<object> GetCountryForDropDown()
        {
            db = new xOssContext();
            var countryList = new List<object>();

            foreach (var country in db.Common_Country.Where(d => d.Active == true))
            {
                countryList.Add(new { Text = country.Name, Value = country.ID });
            }
            return countryList;
        }
        internal List<object> GetAccountSupplierHeadForDropDown()
        {
            db = new xOssContext();
            var accChart2List = new List<object>();

            foreach (var accChart2 in db.Acc_Chart2.Where(d => d.Active == true && (d.ID == 176)))
            {
                accChart2List.Add(new { Text = accChart2.Name, Value = accChart2.ID });
            }
            return accChart2List;
        }
        internal List<object> GetCommonSupplierTypeDropDown()
        {
            db = new xOssContext();
            var accChart2List = new List<object>();

            foreach (var accChart2 in db.Common_SupplierType.Where(d => d.Active == true))
            {
                accChart2List.Add(new { Text = accChart2.Name, Value = accChart2.ID });
            }
            return accChart2List;
        }
        internal List<object> GetAccountBuyerHeadForDropDown()
        {
            db = new xOssContext();
            var accChart2List = new List<object>();

            foreach (var accChart2 in db.Acc_Chart2.Where(d => d.Active == true && (d.ID == 175)))
            {
                accChart2List.Add(new { Text = accChart2.Name, Value = accChart2.ID });
            }
            return accChart2List;
        }
        internal List<object> GetAccountPartyChart2HeadForDropDown()
        {
            db = new xOssContext();
            var accChart2List = new List<object>();

            foreach (var accChart2 in db.Acc_Chart2.Where(d => d.Active == true && (d.ID == 74 || d.ID == 2 || d.ID == 3)))
            {
                accChart2List.Add(new { Text = accChart2.Name, Value = accChart2.ID });
            }
            return accChart2List;
        }

        public List<object> GetRequisitionDropDownCID()
        {
            db = new xOssContext();
            var List = new List<object>();
            var v = (from t1 in db.Prod_Requisition
                     join t2 in db.Mkt_BOM
                         on t1.Mkt_BOMFK equals t2.ID
                     where t2.Active == true
                     select new
                     {
                         CID = t2.CID + "/Req/" + t1.ID,
                         ID = t2.ID
                     }).OrderByDescending(x => x.ID);

            foreach (var cid in v)
            {
                List.Add(new { Text = cid.CID, Value = cid.ID });
            }
            return List;
        }
        //..........DropDown For Accounting........//AccountType
        //public List<object> GetAcc_Type()
        //{
        //    db = new xOssContext();
        //    var Acc_TypeList = new List<object>();

        //    foreach (var Acc_Type in db.Acc_Type.Where(x => x.Active == true))
        //    {
        //        Acc_TypeList.Add(new { Text = Acc_Type.Name, Value = Acc_Type.ID });
        //    }
        //    return Acc_TypeList;
        //}

        public List<object> GetAcc_Type()
        {
            var acc_Type = new List<object>();
            Acc_Type x = new Acc_Type();
            var listOfAcc_Type = x.GetAcc_Type();
            foreach (var list in listOfAcc_Type)
            {
                acc_Type.Add(new { Text = list.Name, Value = list.ID });
            }
            return acc_Type;
        }

        //public List<object> GetAcc_Type()
        //{
        //    var AccountTypeList = new List<object>();

        //    //Array Name =Enum.GetNames(typeof(AccountType));
        //    //Array Value = Enum.GetValues(typeof(AccountType));

        //    foreach (AccountType  at in Enum.GetValues(typeof(AccountType)))
        //    {
        //        ListItem item = new ListItem(Enum.GetName(typeof(AccountType),at),at.ToString());
        //        AccountTypeList.Add(new { Text = item.Text, Value = item.Value });
        //    }
        //    return AccountTypeList;
        //}
        public List<object> GetUser_TeamForDropDown()
        {
            db = new xOssContext();
            var User_TeamList = new List<object>();

            foreach (var User_Team in db.User_Team.Where(x => x.Active == true))
            {
                User_TeamList.Add(new { Text = User_Team.Name, Value = User_Team.ID });
            }
            return User_TeamList;
        }

        public List<object> GetUser_DepartmentForDropDown()
        {
            db = new xOssContext();
            var User_DepartmentList = new List<object>();

            foreach (var User_Department in db.User_Department.Where(x => x.Active == true))
            {
                User_DepartmentList.Add(new { Text = User_Department.Name, Value = User_Department.ID });
            }
            return User_DepartmentList;
        }

        public List<object> GetUser_AccessLevelForDropDown()
        {
            db = new xOssContext();
            var User_AccessLevelList = new List<object>();

            foreach (var User_AccessLevel in db.User_AccessLevel.Where(x => x.Active == true))
            {
                User_AccessLevelList.Add(new { Text = User_AccessLevel.Name, Value = User_AccessLevel.ID });
            }
            return User_AccessLevelList;
        }

        public List<object> GetAcc_TypeDropDownForEdit(int? id)
        {
            db = new xOssContext();
            var Acc_TypeList = new List<object>();
            Acc_Type x = new Acc_Type();
            foreach (var Acc_Type in x.GetAcc_Type())
            {
                Acc_TypeList.Add(new { Text = Acc_Type.Name, Value = Acc_Type.ID });
            }
            return Acc_TypeList;
        }

        public List<object> GetAcc_Chart1()
        {
            db = new xOssContext();
            var Acc_Chart1List = new List<object>();

            foreach (var Acc_Chart1 in db.Acc_Chart1.Where(x => x.Active == true))
            {
                Acc_Chart1List.Add(new { Text = Acc_Chart1.Name, Value = Acc_Chart1.ID });
            }
            return Acc_Chart1List;
        }

        int? TypeID;
        int? Chart1ID;
        int? Acc_Chart2ID;

        public List<object> GetChart1ForEdit(int? id)
        {
            db = new xOssContext();
            var scl = new List<object>();

            Chart1ID = (from Acc_Chart2 in db.Acc_Chart2
                        where Acc_Chart2.ID == id
                        select Acc_Chart2.Acc_Chart1FK
            ).FirstOrDefault();

            TypeID = (from Acc_Chart1 in db.Acc_Chart1
                      where Acc_Chart1.ID == Chart1ID
                      select Acc_Chart1.Acc_TypeFK
            ).FirstOrDefault();

            var x = (from Acc_Chart1 in db.Acc_Chart1

                     where Acc_Chart1.Acc_TypeFK == TypeID && Acc_Chart1.Active == true
                     select new
                     {
                         ID = Acc_Chart1.ID,
                         Name = Acc_Chart1.Name
                     }
            ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }

            return scl;
        }

        public List<object> GetAcc_Chart2()
        {
            db = new xOssContext();
            var Acc_Chart2List = new List<object>();

            foreach (var Acc_Chart2 in db.Acc_Chart2.Where(x => x.Active == true))
            {
                Acc_Chart2List.Add(new { Text = Acc_Chart2.Name, Value = Acc_Chart2.ID });
            }
            return Acc_Chart2List;
        }

        public List<object> GetAcc_CostPool()
        {
            db = new xOssContext();
            var Acc_CostPoolList = new List<object>();

            foreach (var Acc_CostPool in db.Acc_CostPool.Where(x => x.Active == true))
            {
                Acc_CostPoolList.Add(new { Text = Acc_CostPool.Name, Value = Acc_CostPool.ID });
            }
            return Acc_CostPoolList;
        }

        public List<object> GetAcc_Chart2DropDownEdit(int? id)
        {
            db = new xOssContext();
            var acc_Chart2List = new List<object>();


            var x = (from t1 in db.Acc_Chart2
                     where t1.Acc_Chart1FK ==
                    (from t2 in db.Acc_AcName where t2.ID == id && t2.Active == true select t2.Acc_Chart2FK).FirstOrDefault()
                     where t1.Active == true
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
            ).ToList();
            foreach (var chart2 in x)
            {
                acc_Chart2List.Add(new { Text = chart2.Name, Value = chart2.ID });
            }

            return acc_Chart2List;


        }

        public List<object> Acc_Chart2DropDownEdit(int? id)
        {
            db = new xOssContext();
            var acc_Chart2List = new List<object>();

            Chart1ID = (from Acc_Chart2 in db.Acc_Chart2
                        where Acc_Chart2.ID == id
                        select Acc_Chart2.Acc_Chart1FK
            ).FirstOrDefault();

            //TypeID = (from Acc_Chart1 in db.Acc_Chart1
            //          where Acc_Chart1.ID == Chart1ID
            //          select Acc_Chart1.Acc_TypeFK
            //         ).FirstOrDefault();
            var x = (from t1 in db.Acc_Chart2
                     where t1.Acc_Chart1FK == Chart1ID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
            ).ToList();
            foreach (var chart2 in x)
            {
                acc_Chart2List.Add(new { Text = chart2.Name, Value = chart2.ID });
            }

            return acc_Chart2List;


        }

        public List<object> GetAcc_AcName()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                 select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable()
                         on t1.Acc_Chart2FK equals t2.ID
                     join t3 in db.Acc_Chart1.AsEnumerable()
                         on t2.Acc_Chart1FK equals t3.ID
                     join t4 in listOfAcc_Type
                         on t3.Acc_TypeFK equals t4.ID
                     where t1.Active == true && t2.Active == true
                     && t3.Active == true
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t4.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }
        public List<object> GetAllAcc_AcNameFrom()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                 select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable()
                         on t1.Acc_Chart2FK equals t2.ID
                     join t3 in db.Acc_Chart1.AsEnumerable()
                         on t2.Acc_Chart1FK equals t3.ID
                     join t4 in listOfAcc_Type
                         on t3.Acc_TypeFK equals t4.ID
                     where t4.ID == 1 || t4.ID == 4 && t1.Active == true && t2.Active == true
                           && t3.Active == true
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t4.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }
        public List<object> GetAllAcc_AcNameTo()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                 select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable()
                         on t1.Acc_Chart2FK equals t2.ID
                     join t3 in db.Acc_Chart1.AsEnumerable()
                         on t2.Acc_Chart1FK equals t3.ID
                     join t4 in listOfAcc_Type
                         on t3.Acc_TypeFK equals t4.ID
                     where (t4.ID == 2 || t4.ID == 3 || t4.ID == 5) && t1.Active == true && t2.Active == true
                           && t3.Active == true
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t4.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }

        public List<object> GetAcc_AcNameDropDownEdit(int? id)
        {
            db = new xOssContext();
            var acc_AcNameList = new List<object>();
            var x = (from t1 in db.Acc_AcName
                     where t1.Acc_Chart2FK == (from t2 in db.Acc_AcName where t2.ID == id && t2.Active == true select t2.Acc_Chart2FK).FirstOrDefault()
                           && t1.Active == true
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name

                     }
            ).ToList();


            foreach (var acc_AcName in x)
            {
                acc_AcNameList.Add(new { Text = acc_AcName.Name, Value = acc_AcName.ID });

            }
            return acc_AcNameList;
        }

        //..........For Commercial.............
        public List<object> GetCommon_TheOrderForDropDown()
        {
            db = new xOssContext();
            var commonTheOrderList = new List<object>();
            foreach (var dorder in db.Common_TheOrder.Where(d => d.Active == true))
            {
                commonTheOrderList.Add(new { Text = dorder.CID, Value = dorder.ID });
            }
            return commonTheOrderList;
        }

        public List<object> GetMkt_BuyerForDropDown()
        {
            db = new xOssContext();
            var Mkt_BuyerList = new List<object>();

            foreach (var Mkt_Buyer in db.Mkt_Buyer.Where(d => d.Active == true))
            {
                Mkt_BuyerList.Add(new { Text = Mkt_Buyer.Name, Value = Mkt_Buyer.ID });
            }
            return Mkt_BuyerList;
        }

        public List<object> GetCommercial_BankForDropDown()
        {
            db = new xOssContext();
            var Commercial_BankList = new List<object>();

            foreach (var Commercial_Bank in db.Commercial_Bank.Where(d => d.Active == true && d.IsLien == false))
            {
                Commercial_BankList.Add(new { Text = Commercial_Bank.Name, Value = Commercial_Bank.ID });
            }
            return Commercial_BankList;
        }

        public List<object> GetLienBankForDropDown()
        {
            db = new xOssContext();
            var Commercial_BankList = new List<object>();

            foreach (var Commercial_Bank in db.Commercial_Bank.Where(d => d.Active == true && d.IsLien == true))
            {
                Commercial_BankList.Add(new { Text = Commercial_Bank.Name, Value = Commercial_Bank.ID });
            }
            return Commercial_BankList;
        }

        public List<object> GetCommon_SupplierForDropDown()
        {
            db = new xOssContext();
            var Common_SupplierList = new List<object>();

            foreach (var Common_Supplier in db.Common_Supplier.Where(d => d.Active == true).OrderBy(d => d.Name))
            {
                Common_SupplierList.Add(new { Text = Common_Supplier.Name, Value = Common_Supplier.ID });
            }
            return Common_SupplierList;
        }

        public List<object> GetCommercial_MasterLCForDropDown()
        {
            db = new xOssContext();
            var Commercial_MasterLCList = new List<object>();

            foreach (var Commercial_MasterLC in db.Commercial_MasterLC.Where(d => d.Active == true))
            {
                Commercial_MasterLCList.Add(new { Text = Commercial_MasterLC.Name, Value = Commercial_MasterLC.ID });
            }
            return Commercial_MasterLCList;
        }

        public List<object> GetMasterLCWithoutUD()
        {
            db = new xOssContext();
            var Commercial_MasterLCList = new List<object>();
            var v = (from t1 in db.Commercial_MasterLC
                     where t1.Active == true
                           && !(from x in db.Commercial_UDSlave where x.Active == true select x.Commercial_MasterLCFK)
                               .Contains(t1.ID)
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     });

            foreach (var z in v)
            {
                Commercial_MasterLCList.Add(new { Text = z.Name, Value = z.ID });
            }
            return Commercial_MasterLCList;
        }

        public List<object> GetMasterLCWithoutUD1()
        {
            db = new xOssContext();
            var Commercial_MasterLCList = new List<object>();
            var v = (from t1 in db.Commercial_MasterLC
                     where t1.Active == true
                           && (from x in db.Commercial_UDSlave where x.Active == true select x.Commercial_MasterLCFK)
                               .Contains(t1.ID)
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     });
            foreach (var z in v)
            {
                Commercial_MasterLCList.Add(new { Text = z.Name, Value = z.ID });
            }
            return Commercial_MasterLCList;
        }

        public List<object> GetCommercial_PIForDropDown()
        {
            db = new xOssContext();
            var Commercial_PIList = new List<object>();

            foreach (var Commercial_PI in db.Commercial_PI.Where(d => d.Active == true))
            {
                Commercial_PIList.Add(new { Text = Commercial_PI.Name, Value = Commercial_PI.ID });
            }
            return Commercial_PIList;
        }

        public List<object> GetMkt_POForDropDown()
        {
            db = new xOssContext();
            var Mkt_POList = new List<object>();

            foreach (var Mkt_PO in db.Mkt_PO.Where(d => d.Active == true))
            {
                Mkt_POList.Add(new { Text = Mkt_PO.CID, Value = Mkt_PO.ID });
            }
            return Mkt_POList;
        }

        public List<object> GetMktPOBySupplier(int id)
        {
            db = new xOssContext();
            var pOList = new List<object>();
            var v = (from t1 in db.Mkt_PO
                     where t1.Common_SupplierFK == id && t1.Active == true && t1.IsComplete == false
                     //     &&
                     //!(from t2 in db.Commercial_B2bLcSupplierPO where t2.Active == true select t2.Mkt_POFK)
                     //    .Contains(t1.ID)
                     select new
                     {
                         ID = t1.ID,
                         CID = t1.CID
                     }).ToList();

            foreach (var x in v)
            {
                pOList.Add(new { Text = x.CID, Value = x.ID });
            }
            return pOList;
        }

        public List<object> GetMkt_POForDropDownSpecificSupplier(int supplierID)
        {
            db = new xOssContext();
            var Mkt_POList = new List<object>();

            foreach (var Mkt_PO in db.Mkt_PO.Where(d => d.Active == true && d.Common_SupplierFK == supplierID))
            {
                Mkt_POList.Add(new { Text = Mkt_PO.CID, Value = Mkt_PO.ID });
            }
            return Mkt_POList;
        }

        public List<object> GetCommercial_B2bLCForDropDown()
        {
            db = new xOssContext();
            var Commercial_B2bLCList = new List<object>();

            foreach (var Commercial_B2bLC in db.Commercial_B2bLC.Where(d => d.Active == true))
            {
                Commercial_B2bLCList.Add(new { Text = Commercial_B2bLC.Name, Value = Commercial_B2bLC.ID });
            }
            return Commercial_B2bLCList;
        }

        public List<object> GetAcc_AcNameForEdit(int? id)
        {
            db = new xOssContext();
            var scl = new List<object>();
            Acc_Chart2ID = (
                from Acc_AcName in db.Acc_AcName
                where Acc_AcName.ID == id
                select Acc_AcName.Acc_Chart2FK

            ).FirstOrDefault();


            Chart1ID = (
                from Acc_Chart2 in db.Acc_Chart2
                where Acc_Chart2.ID == Acc_Chart2ID
                select Acc_Chart2.Acc_Chart1FK

            ).FirstOrDefault();


            TypeID = (
                from Acc_Chart1 in db.Acc_Chart1
                where Acc_Chart1.ID == Chart1ID
                select Acc_Chart1.Acc_TypeFK

            ).FirstOrDefault();

            var x = (from Acc_Chart1 in db.Acc_Chart1
                     where Acc_Chart1.Active == true

                     where Acc_Chart1.Acc_TypeFK == TypeID
                     select new
                     {
                         ID = Acc_Chart1.ID,
                         Name = Acc_Chart1.Name
                     }
            ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }

            return scl;
        }

        //public List<object> GetProd_StatusForDropDown()
        //{
        //    db = new xOssContext();
        //    var Prod_Status = new List<object>();

        //    foreach (var ProdStatus in db.Prod_Status.Where(x => x.Active == true))
        //    {
        //        Prod_Status.Add(new { Text = ProdStatus.Name, Value = ProdStatus.ID });
        //    }
        //    return Prod_Status;
        //}

        public List<object> GetB2BLCForDropDown()
        {
            db = new xOssContext();
            var b2bLcList = new List<object>();

            foreach (var b2b in db.Commercial_B2bLC.Where(d => d.Active == true))
            {
                b2bLcList.Add(new { Text = b2b.Name, Value = b2b.Name });
            }
            return b2bLcList;
        }

        public List<object> GetCommercial_MasterLCBuyerPIDropDown()
        {
            db = new xOssContext();
            var MasterLCBuyerPI = new List<object>();

            foreach (var Commercial_MasterLCBuyerPI in db.Commercial_MasterLCBuyerPI.Where(d => d.Active == true))
            {
                MasterLCBuyerPI.Add(new
                {
                    Text = Commercial_MasterLCBuyerPI.Name,
                    Value = Commercial_MasterLCBuyerPI.ID
                });
            }
            return MasterLCBuyerPI;
        }

        public List<object> GetStoreExternalReqIdDropDown()
        {
            db = new xOssContext();
            var StoreReqList = new List<object>();

            foreach (var req in db.Prod_Requisition.Where(x => x.Active == true && x.Internal == false && x.IsComplete == false))
            {
                StoreReqList.Add(new { Text = req.RequisitionCID, Value = req.ID });
            }
            return StoreReqList;
        }

        public List<object> GetAllStoreReqIdDropDown()
        {
            db = new xOssContext();
            var StoreReqList = new List<object>();

            foreach (var req in db.Prod_Requisition.Where(x => x.Active == true && x.Internal == false))
            {
                StoreReqList.Add(new { Text = req.RequisitionCID, Value = req.ID });
            }
            return StoreReqList;
        }

        public List<object> GetStoreReqIdTrue()
        {
            db = new xOssContext();
            var StoreReqList = new List<object>();

            foreach (var req in db.Prod_Requisition.Where(x => x.Active == true && x.Internal == true && x.IsAutherized == true).OrderByDescending(x => x.ID))
            {
                StoreReqList.Add(new { Text = req.RequisitionCID, Value = req.ID });
            }
            return StoreReqList;
        }

        public List<object> GetStoreRequsitionSlave()
        {
            db = new xOssContext();
            var StoreReqList = new List<object>();

            foreach (var req in db.Prod_Requisition.Where(x => x.Active == true && x.Internal == true))
            {
                StoreReqList.Add(new { Text = req.RequisitionCID, Value = req.ID });
            }
            return StoreReqList;
        }

        public List<object> GetStoreReqItemDropDown()
        {
            db = new xOssContext();
            var StoreReqItemList = new List<object>();
            var v = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Raw_Item
                         on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit
                         on t1.Common_UnitFK equals t3.ID
                     where t1.Active == true
                     select new
                     {
                         Text = t2.Name,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                StoreReqItemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return StoreReqItemList;
        }

        public List<object> GetStoreReqItemQuantityDropDown()
        {
            db = new xOssContext();
            var StoreReqItemList = new List<object>();

            foreach (var item in db.Prod_Requisition_Slave.Where(x => x.Active == true))
            {
                StoreReqItemList.Add(new { Text = item.TotalRequired, Value = item.ID });
            }
            return StoreReqItemList;
        }

        public List<object> GetMasterLcTypeDropDown()
        {
            db = new xOssContext();
            var MasterLcTypeList = new List<object>();

            foreach (var item in db.Commercial_LCType.Where(x => x.Active == true))
            {
                MasterLcTypeList.Add(new { Text = item.Name, Value = item.ID });
            }
            return MasterLcTypeList;
        }

        public List<object> GetBtBLCTypeDropDown()
        {
            db = new xOssContext();
            var BtBTypeList = new List<object>();

            foreach (var item in db.Commercial_BtBLCType.Where(x => x.Active == true))
            {
                BtBTypeList.Add(new { Text = item.Name, Value = item.ID });
            }
            return BtBTypeList;
        }

        public List<object> GetLCOreginDropDown()
        {
            db = new xOssContext();
            var LCOreginList = new List<object>();

            foreach (var item in db.Commercial_LCOregin.Where(x => x.Active == true))
            {
                LCOreginList.Add(new { Text = item.Name, Value = item.ID });
            }
            return LCOreginList;
        }

        internal List<object> GetMasterLCByUD(int id)
        {
            db = new xOssContext();
            var LCList = new List<object>();
            var item = (from t1 in db.Commercial_MasterLC
                        join t2 in db.Commercial_UDSlave
                            on t1.ID equals t2.Commercial_MasterLCFK
                        where t2.Commercial_UDFK == id
                        select new
                        {
                            ID = t1.ID,
                            Name = t1.Name
                        });

            foreach (var v in item)
            {
                LCList.Add(new { Text = v.Name, Value = v.ID });
            }
            return LCList;
        }

        public List<object> GetUDDropDown()
        {
            db = new xOssContext();
            var udList = new List<object>();

            foreach (var item in db.Commercial_UD.Where(x => x.Active == true && x.IsClose == false && x.ID != 1))
            {
                udList.Add(new { Text = item.UdNo, Value = item.ID });
            }
            return udList;
        }

        public List<object> GetRaw_StoreDropDown()
        {
            db = new xOssContext();
            var stNList = new List<object>();

            foreach (var item in db.User_Department.Where(x => x.Active == true))
            {
                stNList.Add(new { Text = item.Name, Value = item.ID });
            }
            return stNList;
        }

        public List<object> GetPlanOrderProcessCategory()
        {
            db = new xOssContext();
            var catList = new List<object>();

            foreach (var item in db.Plan_OrderProcessCategory.Where(x => x.Active == true))
            {
                catList.Add(new { Text = item.Name, Value = item.ID });
            }
            return catList;
        }
        public List<object> GetPlanOrderProcessCategoryEdit(int id)
        {
            db = new xOssContext();
            var catList = new List<object>();

            foreach (var item in db.Plan_OrderProcessCategory.Where(x => x.Active == true))
            {
                catList.Add(new { Text = item.Name, Value = item.ID });
            }
            return catList;
        }
        public List<object> GetPlanOrderProcess()
        {
            db = new xOssContext();
            var catList = new List<object>();

            foreach (var item in db.Plan_OrderProcess.Where(x => x.Active == true))
            {
                catList.Add(new { Text = item.StepName, Value = item.ID });
            }
            return catList;
        }
        public List<object> GetDepartment()
        {
            db = new xOssContext();
            var deptList = new List<object>();

            foreach (var item in db.User_Department.Where(x => x.Active == true))
            {
                deptList.Add(new { Text = item.Name, Value = item.ID });
            }
            return deptList;
        }

        internal List<object> GetItemsForSpecificStyle(int id)
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var requisitaionNo = from t1 in db.Prod_Requisition
                                 where t1.ID == id
                                 select t1.Mkt_BOMFK;

            var item = (from t1 in db.Mkt_BOMSlave
                        join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                        where requisitaionNo.Contains(t1.Mkt_BOMFK)
                        select new
                        {
                            Name = t2.Name + " | " + t1.Description,
                            ID = t1.ID
                        });

            foreach (var v in item)
            {
                itemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return itemList;
        }
        internal List<object> GetPriority()
        {
            var result = new List<object>();
            result.Add(new SelectListItem { Value = "High", Text = "High" });
            result.Add(new SelectListItem { Value = "Medium", Text = "Medium" });
            result.Add(new SelectListItem { Value = "Low", Text = "Low" });
            return result;
        }
        public List<object> GetChartType()
        {
            var result = new List<object>();
            result.Add(new SelectListItem { Value = "1", Text = "Chart 2" });
            result.Add(new SelectListItem { Value = "2", Text = "Chart 1" });
            return result;
        }
        public List<object> GetRequisitionList()
        {
            db = new xOssContext();
            var reqList = new List<object>();

            foreach (var item in db.Prod_Requisition.Where(x => x.Active == true && x.IsAutherized))
            {
                reqList.Add(new { Text = item.RequisitionCID, Value = item.ID });
            }
            return reqList;
        }
        public List<object> GetAcc_AcNameBankDr()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable() on t1.Acc_Chart2FK equals t2.ID
                     where (t2.ID == 56 || t2.ID == 57 || t2.ID == 58 || t2.ID == 59 || t2.ID == 60 || t2.ID == 61)
                           && t1.Active == true && t2.Active == true
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t2.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }
        public List<object> GetAcc_AcNameCashDr()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable() on t1.Acc_Chart2FK equals t2.ID
                     where (t2.ID == 62 || t2.ID == 64) && t1.Active == true && t2.Active == true
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t2.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }
        public List<object> GetAcc_AcNameLoanDr()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable() on t1.Acc_Chart2FK equals t2.ID
                     where t2.ID == 128 || t2.ID == 129 && t1.Active == true && t2.Active == true
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t2.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }

        public List<object> GetAcc_AcNameCr()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                 select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable() on t1.Acc_Chart2FK equals t2.ID
                     join t3 in db.Acc_Chart1.AsEnumerable() on t2.Acc_Chart1FK equals t3.ID
                     join t4 in listOfAcc_Type on t3.Acc_TypeFK equals t4.ID
                     where t1.Active == true && t2.Active == true
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t4.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }
        public List<object> GetAcc_AcNameFundTransfer()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                 select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable() on t1.Acc_Chart2FK equals t2.ID
                     join t3 in db.Acc_Chart1.AsEnumerable() on t2.Acc_Chart1FK equals t3.ID
                     join t4 in listOfAcc_Type on t3.Acc_TypeFK equals t4.ID
                     where t1.Active == true && t2.Active == true && t3.ID == 30
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t4.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }
        public List<object> GetAcc_AcNameJournalEntry()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                 select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable() on t1.Acc_Chart2FK equals t2.ID
                     join t3 in db.Acc_Chart1.AsEnumerable() on t2.Acc_Chart1FK equals t3.ID
                     join t4 in listOfAcc_Type on t3.Acc_TypeFK equals t4.ID
                     where t1.Active == true && t2.Active == true
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t4.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }
        public List<object> GetAcc_AcNameExportRealisationExpName()
        {
            db = new xOssContext();
            var Acc_AcNameList = new List<object>();
            var v = (from t1 in db.Acc_AcName.AsEnumerable()
                     join t2 in db.Acc_Chart2.AsEnumerable() on t1.Acc_Chart2FK equals t2.ID
                     where (t2.ID == 203 || t2.ID == 56 || t2.ID == 57 || t2.ID == 58 || t2.ID == 60 || t2.ID == 70 || t2.ID == 204)
                     && t1.Active == true && t2.Active == true
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID,
                         Category = t2.Name
                     });

            foreach (var x in v)
            {
                Acc_AcNameList.Add(new { Text = x.Text, Value = x.Value, Category = x.Category });
            }
            return Acc_AcNameList;
        }
        public List<object> GetItemsForMkt_POSlaveEditFromRequisition(int id)
        {

            db = new xOssContext();
            var items = new List<object>();
            var item = (from t1 in db.Raw_Item
                        join t2 in db.Prod_Requisition_Slave
                            on t1.ID equals t2.Raw_ItemFK
                        where t2.ID == id && t2.Active == true && t2.IsFinalize == false
                        select new
                        {
                            ID = t2.ID,
                            Name = t1.Name
                        });

            foreach (var v in item)
            {
                items.Add(new { Text = v.Name, Value = v.ID });
            }
            return items;
        }

        public List<object> GetFilterType()
        {
            List<object> FilterType = new List<object>();
            FilterType.Add(new { Text = "B2B L/C Date", Value = 1 });
            FilterType.Add(new { Text = "Master L/C Date", Value = 2 });
            FilterType.Add(new { Text = "ETD Date", Value = 3 });
            FilterType.Add(new { Text = "ETA Date", Value = 4 });

            return FilterType;

        }

        public List<object> GetLineBank()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Commercial_Bank
                     where t1.Active == true
                     select new
                     {
                         Text = t1.Name,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;

        }
        public List<object> GetShipment_UnloadingPort()
        {
            db = new xOssContext();
            var upList = new List<object>();
            var v = (from t1 in db.Shipment_UnloadingPort
                     where t1.Active == true
                     select new
                     {
                         Text = t1.Name,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                upList.Add(new { Text = item.Text, Value = item.ID });
            }
            return upList;

        }
        public List<object> GetUD()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Commercial_UD
                     where t1.Active == true
                     select new
                     {
                         Text = t1.UdNo,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;

        }
        public List<object> GetSupplerBillType()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Common_BillType
                     where t1.Active == true
                     select new
                     {
                         Text = t1.Name,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;

        }
        public List<object> GetSupplerCnfTransport()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Common_Supplier
                     where t1.Active == true && t1.Common_SupplierTypeFk != 1
                     select new
                     {
                         Text = t1.Name,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;

        }
        public List<object> GetSupplerCnfTransportById(int id)
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Common_Supplier
                     where t1.Active == true && t1.Common_SupplierTypeFk == id
                     select new
                     {
                         Text = t1.Name,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;

        }

        public List<object> GetInvoiceList()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Commercial_Invoice
                     where t1.Active == true
                     select new
                     {
                         Text = t1.InvoiceNo,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;

        }
        public List<object> GetChallanList()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Shipment_CNFDelivaryChallan
                     where t1.Active == true
                     select new
                     {
                         Text = t1.ChallanNo,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;

        }

        public List<object> GetExpenseBillList()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Acc_AcName
                     join t2 in db.Acc_Chart2 on t1.Acc_Chart2FK equals t2.ID
                     join t3 in db.Acc_Chart1 on t2.Acc_Chart1FK equals t3.ID
                     where t1.Active == true && t3.Acc_TypeFK == 5
                     select new
                     {
                         Text = t1.Name,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;

        }
        public List<object> GetTransferFilterType()
        {
            List<object> FilterType = new List<object>();
            FilterType.Add(new { Text = "Raw Item", Value = 1 });
            FilterType.Add(new { Text = "Item In Order", Value = 2 });
            FilterType.Add(new { Text = "Item In Process", Value = 3 });

            return FilterType;

        }

        public List<object> GetItemForSupplierPoEditDropDown(int id)
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_POSlave on t1.ID equals t2.Mkt_POFK
                     join t3 in db.Raw_Item on t2.Raw_ItemFK equals t3.ID
                     where t1.ID == id && t1.Active == true && t2.Active == true && t3.Active == true
                     select new
                     {
                         Text = t3.Name + " | " + t2.Description,
                         ID = t2.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;
        }

        public List<object> GetDelivaryPort(int id)
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     join t2 in db.Common_Country on t1.Destination equals t2.ID
                     where t1.Mkt_BOMFk == id && t1.Active == true && t2.Active == true
                     select new
                     {
                         Text = t2.Name + " | " + t1.PortNo,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;
        }

        public List<object> GetProductionStep()
        {
            db = new xOssContext();
            var lineList = new List<object>();
            var v = (from t1 in db.Plan_OrderProcess
                     join t2 in db.Plan_OrderProcessCategory on t1.Plan_OrderProcessCategoryFk equals t2.ID
                     where t1.Plan_OrderProcessCategoryFk == 6 && t1.Active == true && t2.Active == true
                     select new
                     {
                         Text = t1.StepName,
                         ID = t1.ID
                     }).ToList();
            foreach (var line in v)
            {
                lineList.Add(new { Text = line.Text, Value = line.ID });
            }
            return lineList;
        }

        public List<object> GetColorByPlanReferenceProduction(int id, int orderDeliveryScheduleId)
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Mkt_OrderColorAndSizeRatio
                     join t2 in db.Prod_PlanReference on t1.OrderDeliveryScheduleFk equals t2.Mkt_OrderDeliveryScheduleFK


                     where t1.Mkt_BOMFk == id && t1.Active == true && t2.Mkt_OrderDeliveryScheduleFK == orderDeliveryScheduleId
                     select new
                     {
                         Text = t1.Color,
                         ID = t1.Color
                     }).GroupBy(x => x.Text).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Key, Value = item.Key });
            }
            return itemList;
        }
        public List<object> GetSizeByPlanReferenceProduction(int id, int orderDeliveryScheduleId)
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Mkt_OrderColorAndSizeRatio
                     join t2 in db.Prod_PlanReference on t1.OrderDeliveryScheduleFk equals t2.Mkt_OrderDeliveryScheduleFK


                     where t1.Mkt_BOMFk == id && t1.Active == true && t2.Mkt_OrderDeliveryScheduleFK == orderDeliveryScheduleId
                     select new
                     {
                         Text = t1.Size,
                         ID = t1.Size
                     }).GroupBy(x => x.Text).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Key, Value = item.Key });
            }
            return itemList;
        }
        public List<object> GetStatusDropDown()
        {
            db = new xOssContext();
            var statusList = new List<object>();

            foreach (var status in db.Common_Status.Where(x => x.Active == true))
            {
                statusList.Add(new { Text = status.Name, Value = status.ID });
            }
            return statusList;
        }

        public List<object> GetStoreType()
        {
            List<object> StoreType = new List<object>();
            StoreType.Add(new { Text = "External", Value = 0 });
            StoreType.Add(new { Text = "Internal", Value = 1 });

            return StoreType;
        }

        public List<object> GetInternalStore()
        {
            db = new xOssContext();
            var internalStoreList = new List<object>();

            foreach (var internalStore in db.User_Department.Where(x => x.Active == true && x.IsStore == true))
            {
                internalStoreList.Add(new { Text = internalStore.Name, Value = internalStore.ID });
            }
            return internalStoreList;
        }

        internal List<object> GetItemsForStyle(int BOMId)
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var item = (from t1 in db.Mkt_BOMSlave
                        join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                        where t1.Mkt_BOMFK == BOMId && t1.Active == true
                        select new
                        {
                            Name = t2.Name + " | " + t1.Description,
                            ID = t2.ID
                        });

            foreach (var v in item)
            {
                itemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return itemList;
        }
        internal List<object> GetItemsByBOM(int BOMId)
        {
            db = new xOssContext();
            var itemList = new List<object>();

            var item = (from t1 in db.Mkt_BOMSlave
                        join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                        where t1.Mkt_BOMFK == BOMId && t1.Active == true
                        select new
                        {
                            Name = t2.Name + " | " + t1.Description,
                            ID = t1.ID
                        });

            foreach (var v in item)
            {
                itemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return itemList;
        }
        public List<object> GetTransitionItemsWithoutInReqSlave()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Prod_TransitionItemInventory
                     where t1.Active == true &&
                     !(from x in db.Prod_Requisition_Slave where x.Active == true select x.Prod_TransitionItemInventoryFk)
                               .Contains(t1.ID)
                     select new
                     {
                         Text = t1.ItemName,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;
        }

        public List<object> GetTransitionItems()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Prod_TransitionItemInventory
                     where t1.Active == true
                     select new
                     {
                         Text = t1.ItemName,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;
        }
        public List<object> GetTransitionItemsWithoutItSelf(int id)
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Prod_TransitionItemInventory
                     where t1.ID != id
                     where t1.Active == true
                     select new
                     {
                         Text = t1.ItemName,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;
        }
        public List<object> GetCommercial_BankForExportRealisationDropDown()
        {
            db = new xOssContext();
            var Commercial_BankList = new List<object>();

            foreach (var Commercial_Bank in db.Commercial_Bank.Where(d => d.Active == true && d.IsLien == true))
            {
                Commercial_BankList.Add(new { Text = Commercial_Bank.Name, Value = Commercial_Bank.ID });
            }
            return Commercial_BankList;
        }

        public List<object> GetPortOfLoadingDropDown()
        {
            db = new xOssContext();
            var portOfLoadingList = new List<object>();

            foreach (var port in db.Shipment_PortOfLoading.Where(x => x.Active == true))
            {
                portOfLoadingList.Add(new { Text = port.PortOfLoading, Value = port.ID });
            }
            return portOfLoadingList;
        }

        public List<object> GetPortOfDischargeDropDown()
        {
            db = new xOssContext();
            var portOfDischargeList = new List<object>();

            foreach (var discharge in db.Shipment_PortOfDischarge.Where(x => x.Active == true))
            {
                portOfDischargeList.Add(new { Text = discharge.PortOfDischarge, Value = discharge.ID });
            }
            return portOfDischargeList;
        }

        public List<object> GetShippedByDropdown()
        {
            List<object> shipped = new List<object>();
            shipped.Add(new { Text = "SEA", Value = "SEA" });
            shipped.Add(new { Text = "AIR", Value = "AIR" });
            shipped.Add(new { Text = "Rail", Value = "Rail" });
            shipped.Add(new { Text = "Road", Value = "Road" });
            shipped.Add(new { Text = "Curier", Value = "Curier" });
            return shipped;
        }
        public List<object> GetShippedWay()
        {
            List<object> shipped = new List<object>();
            shipped.Add(new { Text = "SEA", Value = "1" });
            shipped.Add(new { Text = "AIR", Value = "2" });
            shipped.Add(new { Text = "Rail", Value = "3" });
            shipped.Add(new { Text = "Road", Value = "4" });
            shipped.Add(new { Text = "Curier", Value = "5" });
            return shipped;
        }
        public List<object> GetTermsOfShipmentDropDown()
        {
            db = new xOssContext();
            var termsList = new List<object>();

            foreach (var shipment in db.Shipment_TermsOfShipment.Where(x => x.Active == true))
            {
                termsList.Add(new { Text = shipment.Name, Value = shipment.ID });
            }
            return termsList;
        }

        public List<object> GetShipmentBillOfExchageType()
        {
            List<object> shipped = new List<object>();
            shipped.Add(new { Text = "Value received and drawn under Contract No", Value = "Value received and drawn under Contract No" });
            shipped.Add(new { Text = "Value received and drawn under Export L/C No", Value = "Value received and drawn under Export L/C No" });
            return shipped;
        }

        public List<object> GetMasterLCForBillOfExchangeDropdown(string id)
        {
            int id1 = Convert.ToInt32(id);
            db = new xOssContext();
            var LineList = new List<object>();
            var v = (from t1 in db.Mkt_BOM
                     join t2 in db.Commercial_MasterLCBuyerPO on t1.ID equals t2.MKTBOMFK
                     join t3 in db.Commercial_MasterLC on t2.Commercial_MasterLCFK equals t3.ID
                     where t1.Common_TheOrderFk == id1
                     where t1.Active == true && t2.Active == true && t3.Active == true
                     select new
                     {
                         Text = t3.Name,
                         ID = t3.ID
                     }).ToList();
            foreach (var x in v)
            {
                LineList.Add(new { Text = x.Text, Value = x.ID });
            }
            return LineList;
        }

        public List<object> GetProductionLineByOrderProcess(int id)
        {
            db = new xOssContext();
            var LineList = new List<object>();
            var v = (from t1 in db.Plan_ProductionLine
                     join t2 in db.Plan_OrderProcess on t1.Plan_OrderProcessFK equals t2.ID
                     where t2.ID == id
                     where t1.Active == true && t2.Active == true
                     select new
                     {
                         Text = t1.Name,
                         ID = t1.ID
                     }).ToList();
            foreach (var x in v)
            {
                LineList.Add(new { Text = x.Text, Value = x.ID });
            }
            return LineList;
        }

        public List<object> Prod_MachineTypeDropDown()
        {
            db = new xOssContext();
            var Prod_MachineTypeList = new List<object>();

            foreach (var item in db.Prod_MachineType.Where(x => x.Active == true))
            {
                Prod_MachineTypeList.Add(new { Text = item.Name, Value = item.ID });
            }
            return Prod_MachineTypeList;
        }

        public List<object> GetProd_MachineDropDown()
        {
            db = new xOssContext();
            var MachineList = new List<object>();

            foreach (var t1 in db.Prod_Machine.Where(d => d.Active == true))
            {
                MachineList.Add(new { Text = t1.Name, Value = t1.ID });
            }
            return MachineList;
        }
        public List<object> GetMasterLCByUDNo()
        {
            db = new xOssContext();
            var LCList = new List<object>();
            var item = (from t1 in db.Commercial_MasterLC
                        join t2 in db.Commercial_UDSlave
                            on t1.ID equals t2.Commercial_MasterLCFK
                        select new
                        {
                            ID = t1.ID,
                            Name = t1.Name
                        });

            foreach (var v in item)
            {
                LCList.Add(new { Text = v.Name, Value = v.ID });
            }
            return LCList;
        }
        public List<object> GetBillOfExchangeByMasterLC()
        {
            db = new xOssContext();
            var LCList = new List<object>();
            var item = (from t1 in db.Commercial_BillOfExchange
                        select new
                        {
                            ID = t1.ID,
                            Name = t1.Name
                        });
            foreach (var v in item)
            {
                LCList.Add(new { Text = v.Name, Value = v.ID });
            }
            return LCList;
        }

        public List<object> GetOrderDeliverySchedule(int id)
        {
            db = new xOssContext();
            var OrderDeliveryList = new List<object>();
            foreach (var t1 in db.Mkt_OrderDeliverySchedule.Where(d => d.Active == true && d.Common_TheOrderFk == id))
            {
                OrderDeliveryList.Add(new { Text = t1.PortNo, Value = t1.ID });
            }
            return OrderDeliveryList;
        }

        public List<object> GetShipmentMode()
        {
            List<object> mode = new List<object>();
            mode.Add(new { Text = "AIR", Value = "AIR" });
            mode.Add(new { Text = "SEA", Value = "SEA" });
            mode.Add(new { Text = "ROAD", Value = "ROAD" });
            mode.Add(new { Text = "CURIER", Value = "CURIER" });

            return mode;

        }

        public List<object> GetBTBStatus()
        {
            List<object> status = new List<object>();
            status.Add(new { Text = "Running", Value = "Running" });
            status.Add(new { Text = "Pending", Value = "Pending" });
            status.Add(new { Text = "In-House", Value = "In-House" });
            return status;
        }
        public List<object> GetMonths()
        {
            List<object> months = new List<object>();
            months.Add(new { Text = "January", Value = 1 });
            months.Add(new { Text = "February", Value = 2 });
            months.Add(new { Text = "March", Value = 3 });
            months.Add(new { Text = "April", Value = 4 });
            months.Add(new { Text = "May", Value = 5 });
            months.Add(new { Text = "June", Value = 6 });
            months.Add(new { Text = "July", Value = 7 });
            months.Add(new { Text = "August", Value = 8 });
            months.Add(new { Text = "September", Value = 9 });
            months.Add(new { Text = "October", Value = 10 });
            months.Add(new { Text = "November", Value = 11 });
            months.Add(new { Text = "December", Value = 12 });
            return months;
        }
        public List<object> GetYears()
        {
            List<object> status = new List<object>();
            status.Add(new { Text = "2018", Value = "2018" });
            status.Add(new { Text = "2019", Value = "2019" });
            status.Add(new { Text = "2020", Value = "2020" });
            return status;
        }
        public List<object> GetCnfTransportSuppler()
        {
            db = new xOssContext();
            var itemList = new List<object>();
            var v = (from t1 in db.Common_Supplier
                     where t1.Active == true && t1.Common_SupplierTypeFk == 3
                     select new
                     {
                         Text = t1.Name,
                         ID = t1.ID
                     }).ToList();
            foreach (var item in v)
            {
                itemList.Add(new { Text = item.Text, Value = item.ID });
            }
            return itemList;

        }
        public List<object> GetOrderDeliverdScheduleByBOM(int bomid)
        {
            db = new xOssContext();
            var list = new List<object>();
            var items = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t3 in db.Common_Country on t1.Destination equals t3.ID
                         where t1.Mkt_BOMFk == bomid && t1.Active == true

                         select new
                         {
                             Port = t3.Name + " | " + t1.PortNo + "(" + t1.Date + ") Quantity - " + t1.Quantity,
                             ID = t1.ID,

                         }).OrderByDescending(x => x.ID);
            foreach (var item in items)
            {
                list.Add(new { Text = item.Port, Value = item.ID });
            }
            return list;
        }

        public List<object> GetProd_LineDropDown()
        {
            db = new xOssContext();
            var LineList = new List<object>();

            foreach (var t1 in db.Plan_ProductionLine.Where(d => d.Plan_OrderProcessFK == 32))
            {
                LineList.Add(new { Text = t1.Name, Value = t1.ID });
            }
            return LineList;
        }

        public List<object> GetProd_SVDropDown()
        {
            db = new xOssContext();
            var SVList = new List<object>();

            foreach (var t1 in db.Prod_SVName.Where(d => d.Active == true))
            {
                SVList.Add(new { Text = t1.Name, Value = t1.ID });
            }
            return SVList;
        }

        //not need
        public List<object> GetSMVOrderIdDropDown()
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var v = (from t0 in db.Prod_SMVLayout
                     join t2 in db.Mkt_BOM on t0.Mkt_BOMFk equals t2.ID
                     join t1 in db.Common_TheOrder on t2.Common_TheOrderFk equals t1.ID
                     //t1 in db.Common_TheOrder join t2 in db.Mkt_BOM on t1.ID equals t2.Common_TheOrderFk
                     join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                     where t1.Active == true && t2.Active == true
                     select new DDL
                     {
                         Name = t1.CID + "/" + t2.Style,
                         ID = t2.ID,
                         UserID = t3.ID

                     }).OrderByDescending(x => x.ID).ToList();
            foreach (var order in v)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        //not need
        public List<object> GetOrderDeliverdScheduleOrderIdDropDown()
        {
            db = new xOssContext();
            var list = new List<object>();
            var vData = (from t1 in db.Mkt_OrderColorAndSizeRatio
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Mkt_BOM on t1.Mkt_BOMFk equals t3.ID
                         join t4 in db.Common_TheOrder on t1.Common_TheOrderFk equals t4.ID
                         where t1.Active == true && t2.Active == true
                         group t1.Mkt_BOMFk by new { t1.Mkt_BOMFk, t3.Style, t4.CID } into all
                         select new
                         {
                             ID = all.Key.Mkt_BOMFk,
                             Name = all.Key.CID + "/" + all.Key.Style,

                         }).OrderByDescending(x => x.ID).ToList();
            foreach (var item in vData)
            {
                list.Add(new { Text = item.Name, Value = item.ID });
            }
            return list;
        }

        public List<object> GetCuttingOrderIdDropDown()
        {
            db = new xOssContext();
            var list = new List<object>();
            var vData = (from t1 in db.Mkt_OrderColorAndSizeRatio
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Mkt_BOM on t1.Mkt_BOMFk equals t3.ID
                         join t4 in db.Common_TheOrder on t1.Common_TheOrderFk equals t4.ID
                         where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true && t4.IsComplete == false
                         group t1.Mkt_BOMFk by new { t1.Mkt_BOMFk, t3.Style, t4.CID } into all
                         select new
                         {
                             ID = all.Key.Mkt_BOMFk,
                             Name = all.Key.CID + "/" + all.Key.Style,

                         }).OrderByDescending(x => x.ID).ToList();
            foreach (var item in vData)
            {
                list.Add(new { Text = item.Name, Value = item.ID });
            }
            return list;

        }

        public List<object> GetLayoutOrderIdDropDown()
        {
            db = new xOssContext();
            var list = new List<object>();
            var vData = (from t1 in db.Mkt_OrderColorAndSizeRatio
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Mkt_BOM on t1.Mkt_BOMFk equals t3.ID
                         join t4 in db.Common_TheOrder on t1.Common_TheOrderFk equals t4.ID
                         where t1.Active == true && t2.Active == true
                         group t1.Mkt_BOMFk by new { t1.Mkt_BOMFk, t3.Style, t4.CID } into all
                         select new
                         {
                             ID = all.Key.Mkt_BOMFk,
                             Name = all.Key.CID + "/" + all.Key.Style,

                         }).OrderByDescending(x => x.ID).ToList();
            foreach (var item in vData)
            {
                list.Add(new { Text = item.Name, Value = item.ID });
            }
            return list;

        }

        public List<object> GetSewingOrderIdDropDown()
        {
            db = new xOssContext();
            var OrderList = new List<object>();

            var vData = (from t1 in db.Prod_SMVLayout
                         join t2 in db.Mkt_BOM on t1.Mkt_BOMFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         where t3.Active == true && t3.IsComplete == false
                         select new
                         {
                             ID = t1.ID,
                             Name = "[SMV-" + t1.TotalSMV + "]" + t3.CID + "/" + t2.Style,
                         }).OrderByDescending(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetIroningOrderIdDropDown()
        {
            db = new xOssContext();
            var OrderList = new List<object>();

            var vData = (from t1 in db.Prod_PlanReferenceOrderSection
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                         join t5 in db.Prod_SMVLayout on t4.ID equals t5.Mkt_BOMFk
                         where t1.Plan_ReferenceSectionFk == 2
                         group t2.Mkt_BOMFk by new { t2.Mkt_BOMFk, t3.CID, t4.Style } into all
                         select new
                         {
                             ID = all.Key.Mkt_BOMFk,
                             Name = all.Key.CID + "/" + all.Key.Style,
                         }).OrderByDescending(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetPackingOrderIdDropDown()
        {
            db = new xOssContext();
            var OrderList = new List<object>();

            var vData = (from t1 in db.Prod_PlanReferenceOrderSection
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         join t4 in db.Mkt_BOM on t2.Mkt_BOMFk equals t4.ID
                         join t5 in db.Prod_SMVLayout on t4.ID equals t5.Mkt_BOMFk
                         where t1.Plan_ReferenceSectionFk == 3
                         group t2.Mkt_BOMFk by new { t2.Mkt_BOMFk, t3.CID, t4.Style } into all
                         select new
                         {
                             ID = all.Key.Mkt_BOMFk,
                             Name = all.Key.CID + "/" + all.Key.Style,
                         }).OrderByDescending(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetProd_TableDropDown()
        {
            db = new xOssContext();
            var LineList = new List<object>();

            foreach (var t1 in db.Plan_ProductionLine.Where(d => d.Plan_OrderProcessFK == 30))
            {
                LineList.Add(new { Text = t1.Name, Value = t1.ID });
            }
            return LineList;
        }

        public List<object> GetProd_AllLineDropDown()
        {
            db = new xOssContext();
            var LineList = new List<object>();

            foreach (var t1 in db.Plan_ProductionLine.Where(d => d.Plan_OrderProcessFK == 32 || d.Plan_OrderProcessFK == 30))
            {
                LineList.Add(new { Text = t1.Name, Value = t1.ID });
            }
            return LineList;
        }

        public List<object> GetPlanOrderDropDown(int id, int sectionId)
        {
            db = new xOssContext();
            var LineList = new List<object>();
            var serch = (from t1 in db.Prod_PlanReferenceOrder
                         join t2 in db.Mkt_OrderDeliverySchedule
                         on t1.OrderDeliveryScheduleFk equals t2.ID
                         join t3 in db.Mkt_BOM on t2.Mkt_BOMFk equals t3.ID
                         join t4 in db.Common_TheOrder on t2.Common_TheOrderFk equals t4.ID
                         where t1.Prod_ReferenceFK == id && t1.SectionId == sectionId
                         group t1.Quantity by new
                         {
                             t4.CID,
                             t3.Style,
                             t3.ID
                         } into all
                         select new
                         {
                             ID = all.Key.ID,
                             Name = all.Key.CID + "/" + all.Key.Style
                         }).OrderBy(x => x.ID);

            foreach (var t1 in serch)
            {
                LineList.Add(new { Text = t1.Name, Value = t1.ID });
            }
            return LineList;
        }

        public List<object> GetSewingPlanOrderIdDropDown(int id)
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var vData = (from t1 in db.Prod_PlanReferenceOrder
                         join t2 in db.Mkt_BOM on t1.MktBomFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         join t4 in db.Plan_ProductionLine on t1.Plan_ProductionLineFk equals t4.ID
                         where t3.Active == true && t3.IsComplete == false && t2.Active == true && t1.Prod_ReferenceFK == id && t1.SectionId == 2
                         group t1.SMV by new { t1.SMV, t3, t2,t1,t4.Name } into a
                         select new
                         {
                             ID = a.Key.t2.ID+"_"+a.Key.t1.ID,
                             Name = a.Key.Name +"|"+"[SMV-" + a.Key.SMV + "]" + a.Key.t3.CID + "/" + a.Key.t2.Style
                         }).OrderBy(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        //public List<object> GetSewingPlanOrderIdDropDown(int id)
        //{
        //    db = new xOssContext();
        //    var OrderList = new List<object>();
        //    var vData = (from t1 in db.Prod_PlanReferenceOrder
        //                 join t4 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t4.ID
        //                 join t2 in db.Mkt_BOM on t4.Mkt_BOMFk equals t2.ID
        //                 join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
        //                 where t3.Active == true && t3.IsComplete == false && t4.Active==true && t1.Prod_ReferenceFK==id && t1.SectionId==2
        //                 group t1.SMV by new {t1.SMV, t3,t2 } into a
        //                 select new
        //                 {
        //                     ID = a.Key.t2.ID,
        //                     Name = "[SMV-" + a.Key.SMV + "]" + a.Key.t3.CID + "/" + a.Key.t2.Style
        //                 }).OrderBy(a=>a.ID).ToList();

        //    foreach (var order in vData)
        //    {
        //        OrderList.Add(new { Text = order.Name, Value = order.ID });
        //    }
        //    return OrderList;
        //}

        public List<object> GetIronPlanOrderIdDropDown(int id)
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var vData = (from t1 in db.Prod_PlanReferenceOrder
                         join t2 in db.Mkt_BOM on t1.MktBomFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         where t3.Active == true && t3.IsComplete == false && t2.Active == true && t1.Prod_ReferenceFK == id && t1.SectionId == 3
                         group t1 by new { t3, t2 } into a
                         select new
                         {
                             ID = a.Key.t2.ID,
                             Name = a.Key.t3.CID + "/" + a.Key.t2.Style
                         }).OrderBy(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetPackingPlanOrderIdDropDown(int id)
        {
            db = new xOssContext();
            var OrderList = new List<object>();
            var vData = (from t1 in db.Prod_PlanReferenceOrder
                         join t2 in db.Mkt_BOM on t1.MktBomFk equals t2.ID
                         join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                         where t3.Active == true && t3.IsComplete == false && t2.Active == true && t1.Prod_ReferenceFK == id && t1.SectionId == 4
                         group t1 by new { t3, t2 } into a
                         select new
                         {
                             ID = a.Key.t2.ID,
                             Name = a.Key.t3.CID + "/" + a.Key.t2.Style
                         }).OrderBy(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> GetLeaveTypeName()
        {
            db = new xOssContext();
            var List = new List<object>();
            foreach (var list in db.HrmsLeaveTypes.Where(d => d.Status == true))
            {
                List.Add(new { Text = list.Leave_Name, Value = list.ID });
            }

            return List;
        }

        public List<object> GetEmployee()
        {
            db = new xOssContext();
            var empList = new List<object>();
            foreach (var t1 in db.Employees.Where(d => d.Present_Status == 1))
            {
                empList.Add(new { Text = t1.EmployeeIdentity + " " + t1.Name, Value = t1.ID });
            }
            return empList;
        }

        public List<object> GetReportTypeList()
        {
            var ReportType = new List<object>();
            ReportType.Add(new { Text = "Periodic Summary Report", Value = "1" });
            ReportType.Add(new { Text = "Periodic Details Report", Value = "2" });
            //ReportType.Add(new { Text = "BuyerPO Wise Details Report", Value = "2" });
            //ReportType.Add(new { Text = "Buyer Wise Details Report", Value = "3" });

            return ReportType;
        }

        public List<object> GetProductionReport()
        {
            var ReportType = new List<object>();
            ReportType.Add(new { Text = "Monthly Report-Buyer", Value = "1" });
            ReportType.Add(new { Text = "Monthly Report-Order", Value = "2" });
            ReportType.Add(new { Text = "Monthly Line-Order", Value = "3" });
            ReportType.Add(new { Text = "Monthly Achievement", Value = "4" });
            return ReportType;
        }

        public List<object> GetCommon_TheOrderRunningForDropDown()
        {
            db = new xOssContext();
            var commonTheOrderList = new List<object>();
            foreach (var dorder in db.Common_TheOrder.Where(d => d.Active == true && d.IsComplete==false))
            {
                commonTheOrderList.Add(new { Text = dorder.CID, Value = dorder.ID });
            }
            return commonTheOrderList;
        }

        public List<object> GetInputRequisitionList()
        {
            db = new xOssContext();
            var reqList = new List<object>();

            foreach (var item in db.Prod_InputRequisition.Where(x => x.Active == true && x.IsAutherized==true))
            {
                reqList.Add(new { Text = item.RequisitionCID, Value = item.ID });
            }
            return reqList;
        }

        public List<object> GetAllInputRequisitionById(int RequsitionId)
        {
            db = new xOssContext();
            var reqList = new List<object>();
            var vData = db.Prod_InputRequisitionSlave.Where(x => x.Prod_InputRequisitionFk == RequsitionId);
            foreach (var item in vData)
            {
                reqList.Add(new { Text = item.ColorName+"-"+item.Size, Value = item.ID });
            }
            return reqList;
        }

        public List<object> GetOrderStyleColor(int BomId)
        {
            db = new xOssContext();
            var list = new List<object>();

            var vData = (from a in db.Mkt_OrderDeliverySchedule
                         join b in db.Mkt_OrderColorAndSizeRatio on a.ID equals b.OrderDeliveryScheduleFk
                         where a.Active == true && b.Active == true && a.Mkt_BOMFk == BomId
                         group b.Quantity by new { b.Color } into all
                         select new
                         {
                             ID = all.Key.Color,
                             Name = all.Key.Color
                         }).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    list.Add(new { Text = v.Name, Value = v.ID });
                }
            }
            return list;
        }

        public List<object> GetPOPaymentType()
        {
            var result = new List<object>();
            result.Add(new SelectListItem { Value = "Cash", Text = "Cash" });
            result.Add(new SelectListItem { Value = "L/C", Text = "L/C" });
            return result;
        }
    }

}