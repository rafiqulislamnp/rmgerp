﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Common
{
	public class VmCommon_Currency
	{
        private xOssContext db;
        public IEnumerable<VmCommon_Currency> DataList { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmCommon_Currency()
        {
            db = new xOssContext();
            InitialDataLoad();
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            var v = (from t1 in db.Common_Currency
                     select new VmCommon_Currency
                     {
                         Common_Currency = t1
                     }).Where(x => x.Common_Currency.Active == true).OrderByDescending(x => x.Common_Currency.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from rc in db.Common_Currency
                     select new VmCommon_Currency
                     {
                         Common_Currency = rc
                     }).Where(c => c.Common_Currency.ID == id).SingleOrDefault();
            this.Common_Currency = v.Common_Currency;

        }

        public int Add(int userID)
        {
            Common_Currency.AddReady(userID);
            return Common_Currency.Add();
        }

        public bool Edit(int userID)
        {
            return Common_Currency.Edit(userID);
        }

        public bool Delete(int userID)
        {           
            return Common_Currency.Delete(userID);          
        }
    }
}