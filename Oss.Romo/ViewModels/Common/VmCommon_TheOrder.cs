﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Reports;
using Oss.Romo.ViewModels.Shipment;
using Oss.Romo.ViewModels.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.ViewModels.Commercial;

namespace Oss.Romo.ViewModels.Common
{
    public class VmCommon_TheOrder
    {
        public string SearchString { get; set; }
        public string Header { get; set; }
        public string ButtonName { get; set; }
        public decimal TotalOrderValue { get; set; }
        public bool IsComplete { get; set; }
        private xOssContext db;
        public IEnumerable<VmCommon_TheOrder> DataList { get; set; }
        public IEnumerable<SizeList> PackingList { get; set; }
        public List<object> DDownData { get; set; }

        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }

        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public VmMkt_PO VmMkt_PO { get; set; }


        public Raw_Item Raw_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public User_User User_User { get; set; }

        public Mkt_SubCategory Mkt_SubCategory { get; set; }
        public Mkt_Category Mkt_Category { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }

        //public Prod_DailyTarget Prod_DailyTarget { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }

        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public List<ReportDocType> OrderReportDoc { get; set; }
        //public List<ReportDocType> OrderReport { get; set; }
        public Plan_OrderProcessCategory Plan_OrderProcessCategory { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }


        public VmMkt_OrderDeliverySchedule VmMkt_OrderDeliverySchedule { get; set; }
        public VmMkt_OrderColorAndSizeRatio VmMkt_OrderColorAndSizeRatio { get; set; }

        public VmShipment_OrderDeliverdSchedule VmShipment_OrderDeliverdSchedule { get; set; }
        public VmShipment_OrderColorAndSizeRatio VmShipment_OrderColorAndSizeRatio { get; set; }
        public VmCommercial_BillOfExchange VmCommercial_BillOfExchange { get; set; }
        public VmCommercial_BillOfExchangeSlave VmCommercial_BillOfExchangeSlave { get; set; }
        public VmShipment_Invoice VmShipment_Invoice { get; set; }
        public string ErrorMessage { get; set; }

        public VmCommon_TheOrder()
        {
            db = new xOssContext();
            VmControllerHelper = new VmControllerHelper();

        }

        public void OrderCloss(string id)
        {
            int Id = 0;
            int.TryParse(id,out Id);

            db = new xOssContext();
            var vData = (from t1 in db.Mkt_BOM
                         join t2 in db.Mkt_PO on t1.ID equals t2.Mkt_BOMFK
                         where t1.Common_TheOrderFk == Id
                         select new
                         {
                             t1
                         }).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    VmMkt_PO VmMkt_PO = new VmMkt_PO();
                    var getPo = db.Mkt_PO.Where(a=>a.Mkt_BOMFK==v.t1.ID);
                    if (getPo.Any())
                    {
                        foreach (var v1 in getPo)
                        {
                            v1.IsComplete = true;
                        }
                    }
                }
            }

            db.SaveChangesAsync();
            VmCommon_TheOrder o = new VmCommon_TheOrder();
            o.SelectSingle(id);
            o.Common_TheOrder.IsComplete = !o.Common_TheOrder.IsComplete;
            o.Edit(0);
        }

        public void ChangeOrderStatus(string id)
        {

            VmCommon_TheOrder o = new VmCommon_TheOrder();
            o.SelectSingle(id);
            o.Common_TheOrder.IsComplete = !o.Common_TheOrder.IsComplete;
            o.Edit(0);
        }
        public void InitialDataLoad(bool flag)
        {

            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Common_Currency on t1.Common_CurrencyFK equals t2.ID
                     join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                     join t4 in db.User_User on t1.FirstCreatedBy equals t4.ID

                     where t1.IsComplete == flag
                     select new VmCommon_TheOrder
                     {
                         Common_TheOrder = t1,
                         Common_Currency = t2,
                         Mkt_Buyer = t3,
                         User_User = t4
                     }).Where(x => x.Common_TheOrder.Active == true).OrderByDescending(x => x.Common_TheOrder.ID).AsEnumerable();

            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }



        }
        public void InitialSearchLoad(bool flag, string SearchString)
        {
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Common_Currency on t1.Common_CurrencyFK equals t2.ID
                     join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                     join t4 in db.User_User on t1.LastEditeddBy equals t4.ID
                     where t1.IsComplete == flag

                     select new VmCommon_TheOrder
                     {
                         Common_TheOrder = t1,
                         Common_Currency = t2,
                         Mkt_Buyer = t3,
                         User_User = t4

                     }).Where(x => x.Common_TheOrder.Active == true && x.Common_TheOrder.CID.Contains(SearchString)).OrderByDescending(x => x.Common_TheOrder.ID).AsEnumerable();
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }

        }
        public VmCommon_TheOrder SelectSingleJoined(string tiD)
        {
            int iD = 0;
            tiD = this.VmControllerHelper.Decrypt(tiD);
            Int32.TryParse(tiD, out iD);
            db = new xOssContext();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Common_Currency on t1.Common_CurrencyFK equals t2.ID
                     join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                     where t1.ID == iD
                     select new VmCommon_TheOrder
                     {
                         Common_TheOrder = t1,
                         Common_Currency = t2,
                         Mkt_Buyer = t3
                     }).FirstOrDefault();
            return v;
        }
        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from rc in db.Common_TheOrder
                     select new VmCommon_TheOrder
                     {
                         Common_TheOrder = rc
                     }).Where(c => c.Common_TheOrder.ID == id).SingleOrDefault();
            this.Common_TheOrder = v.Common_TheOrder;

        }

        internal bool CreateCID(int userID, int orderID)
        {
            Common_TheOrder.ID = orderID;
            Common_TheOrder.CID = "PO/" + @DateTime.Now.Year.ToString().Substring(2, 2) + "/" + (10 + Common_TheOrder.Mkt_BuyerFK).ToString() + "/" + (10 + orderID).ToString();
            return Common_TheOrder.Edit(userID);
        }
        public int Add(int userID)
        {
            Common_TheOrder.AddReady(userID);
            return Common_TheOrder.Add();
            //return CreateCID(userID,Common_TheOrder.Add());
        }

        public bool Edit(int userID)
        {
            return Common_TheOrder.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Common_TheOrder.Delete(userID);
        }
        //.................Report For Common The Order............//
        internal void POInventoryReportDocLoad(int id)
        {
            db = new xOssContext();

            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                    join Common_Supplier in db.Common_Supplier
                    on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     where Mkt_PO.ID == id

                     select new ReportDocType
                     {
                         HeaderLeft1 = Common_TheOrder.CID + "/" + Mkt_BOM.Style,
                         HeaderLeft2 = Mkt_BOM.FirstMove.ToString(),
                         HeaderLeft3 = Mkt_BOM.UpdateDate.ToString(),
                         HeaderLeft4 = Mkt_BOM.Reference.ToString(),
                         HeaderLeft5 = Mkt_BOM.Class.ToString(),
                         HeaderLeft6 = Mkt_BOM.Fabrication,
                         HeaderLeft7 = Mkt_BOM.SizeRange,
                         HeaderLeft8 = Mkt_BOM.Style,
                        
                         HeaderMiddle1 = Mkt_Buyer.Name,
                         HeaderMiddle2 = Common_TheOrder.BuyerPO,
                         HeaderMiddle3 = Common_TheOrder.OrderDate.ToString(),
                         HeaderMiddle4 = Common_TheOrder.Season,
                         HeaderMiddle5 = Mkt_Item.Name,
                         HeaderMiddle6 = Common_Supplier.Name,

                         HeaderRight1 = Mkt_BOM.QPack.ToString(),
                         HeaderRight2 = Mkt_BOM.Quantity.ToString(),
                         HeaderRight3 = Mkt_BOM.UnitPrice.ToString(),
                         HeaderRight4 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString()),
                         HeaderRight5 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
                         HeaderRight6 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
                         HeaderRight7 = Mkt_PO.CID,
                         Body1 = Raw_Item.Name,
                         Body2 = Mkt_POSlave.TotalRequired.ToString(),
                         Body3 = Mkt_POSlave.ID.ToString(),
                         Body8 = Common_Unit.Name

                     }).ToList();


            foreach (ReportDocType r in a)
            {
                int poSlaveID = 0;
                decimal balance = 0;
                decimal body2 = 0;
                decimal.TryParse(r.Body2, out body2);
                int.TryParse(r.Body3, out poSlaveID);
                decimal recived, returned;
                recived = GetTotalReceive(poSlaveID);
                returned = GetTotalReturn(poSlaveID);
                r.Body4 = recived.ToString("F");
                r.Body5 = returned.ToString("F");
                r.Body6 = (recived - returned).ToString("F");
                decimal body6 = 0;
                decimal.TryParse(r.Body6, out body6);
                balance = body2 - body6;
                r.Body10 = balance.ToString("F");


                r.HeaderLeft2 = r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
                r.HeaderLeft3 = r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
                r.HeaderMiddle3 = r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);
            }

            this.OrderReportDoc = a.ToList();

        }




        internal void OrderInventoryReportDocLoad(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();

            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join t1 in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals t1.ID


                     where Common_TheOrder.ID == iD && Mkt_PO.Active == true
                     && Mkt_POSlave.Active == true


                     select new ReportDocType
                     {
                         HeaderLeft1 = Common_TheOrder.CID + "/" + Mkt_BOM.Style,
                         HeaderLeft2 = Mkt_BOM.FirstMove.ToString(),
                         HeaderLeft3 = Mkt_BOM.UpdateDate.ToString(),
                         HeaderLeft4 = Mkt_BOM.Reference.ToString(),
                         HeaderLeft5 = Mkt_BOM.Class.ToString(),
                         HeaderLeft6 = Mkt_BOM.Fabrication,
                         HeaderLeft7 = Mkt_BOM.SizeRange,
                         HeaderLeft8 = Mkt_BOM.Style,
                         HeaderMiddle1 = Mkt_Buyer.Name,
                         HeaderMiddle2 = Common_TheOrder.BuyerPO,
                         HeaderMiddle3 = Common_TheOrder.OrderDate.ToString(),
                         HeaderMiddle4 = Common_TheOrder.Season,
                         HeaderMiddle5 = Mkt_Item.Name,

                         HeaderRight1 = Mkt_BOM.QPack.ToString(),
                         HeaderRight2 = Mkt_BOM.Quantity.ToString(),
                         HeaderRight3 = Mkt_BOM.UnitPrice.ToString(),
                         HeaderRight4 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString()),
                         HeaderRight5 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
                         HeaderRight6 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
                         Body1 = Mkt_PO.CID,
                         Body2 = Raw_Item.Name,
                         Body3 = Mkt_POSlave.TotalRequired.ToString(),
                         Body7 = Mkt_POSlave.ID.ToString(),
                         Body8 = Common_Unit.Name,
                         Body9 = t1.Name,
                         Body11 = Mkt_POSlave.Description
                     }).ToList();

            foreach (ReportDocType r in a)
            {
                int poSlaveID = 0;
                decimal balance = 0;
                decimal body3 = 0;
                decimal.TryParse(r.Body3, out body3);
                int.TryParse(r.Body7, out poSlaveID);
                decimal recived, returned;
                recived = GetTotalReceive(poSlaveID);
                returned = GetTotalReturn(poSlaveID);
                r.Body4 = recived.ToString("F");
                r.Body5 = returned.ToString("F");
                r.Body6 = (recived - returned).ToString("F");

                decimal body6 = 0;
                decimal.TryParse(r.Body6, out body6);
                balance = body3 - body6;
                r.Body10 = balance.ToString("F");

                r.HeaderLeft2 = r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
                r.HeaderLeft3 = r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
                r.HeaderMiddle3 = r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);


            }

            this.OrderReportDoc = a.ToList();
        }
        //-------------------------------Closing Raw inventory--------------------------//
        internal void RawInventoryReport(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();
            VmMkt_PO Vpo = new VmMkt_PO();
            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join t1 in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals t1.ID


                     where Common_TheOrder.ID == iD && Mkt_PO.Active == true
                     && Mkt_POSlave.Active == true

                     select new ReportDocType
                     {
                         HeaderLeft1 = Mkt_BOM.Class.ToString(),
                         HeaderLeft2 = Common_TheOrder.BuyerPO + " / " + Mkt_BOM.Style,
                         HeaderLeft3 = Mkt_Buyer.Name,
                         HeaderLeft4 = Mkt_Item.Name,
                         HeaderLeft5 = (((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString()) + " PCS",
                         Body1 = Mkt_BOM.Fabrication,
                         Body2 = Raw_Item.Name,
                         Body3 = Mkt_POSlave.TotalRequired.ToString(),
                         Body7 = Mkt_POSlave.ID.ToString(),
                         Body8 = Common_Unit.Name,
                         Body11 = Mkt_POSlave.Description
                     }).ToList();

            foreach (ReportDocType r in a)
            {
                int poSlaveID = 0;
                decimal balance = 0;
                decimal body3 = 0;
                decimal.TryParse(r.Body3, out body3);
                int.TryParse(r.Body7, out poSlaveID);
                decimal recived, returned;
                recived = GetTotalReceive(poSlaveID);
                returned = GetTotalReturn(poSlaveID);
                r.Body4 = recived.ToString("F");
                r.Body5 = returned.ToString("F");
                r.Body6 = (recived - returned).ToString("F");
                decimal body6 = 0;
                decimal.TryParse(r.Body6, out body6);
                balance = body3 - body6;
                r.Body10 = balance.ToString("F");
                r.Body12 = Vpo.GetPreviousDelivery(poSlaveID).ToString();
                r.Body13 = ((recived - returned) - Vpo.GetPreviousDelivery(poSlaveID)).ToString();

            }

            this.OrderReportDoc = a.ToList();
        }

        internal void OrderInProgressInventoryReportDocLoad(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();

            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join t1 in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals t1.ID


                     where Common_TheOrder.ID == iD && Mkt_PO.Active == true
                     && Mkt_POSlave.Active == true


                     select new ReportDocType
                     {
                         HeaderLeft1 = Common_TheOrder.CID + "/" + Mkt_BOM.Style,
                         HeaderLeft2 = Mkt_BOM.FirstMove.ToString(),
                         HeaderLeft3 = Mkt_BOM.UpdateDate.ToString(),
                         HeaderLeft4 = Mkt_BOM.Reference.ToString(),
                         HeaderLeft5 = Mkt_BOM.Class.ToString(),
                         HeaderLeft6 = Mkt_BOM.Fabrication,
                         HeaderLeft7 = Mkt_BOM.SizeRange,
                         HeaderLeft8 = Mkt_BOM.Style,
                         HeaderMiddle1 = Mkt_Buyer.Name,
                         HeaderMiddle2 = Common_TheOrder.BuyerPO,
                         HeaderMiddle3 = Common_TheOrder.OrderDate.ToString(),
                         HeaderMiddle4 = Common_TheOrder.Season,
                         HeaderMiddle5 = Mkt_Item.Name,

                         HeaderRight1 = Mkt_BOM.QPack.ToString(),
                         HeaderRight2 = Mkt_BOM.Quantity.ToString(),
                         HeaderRight3 = Mkt_BOM.UnitPrice.ToString(),
                         HeaderRight4 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString()),
                         HeaderRight5 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
                         HeaderRight6 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
                         Body1 = Raw_Item.Name,
                         Body2 = Raw_Item.Description,
                         Body3 = Mkt_POSlave.TotalRequired.ToString(),
                         Body7 = Mkt_POSlave.ID.ToString(),
                         Body8 = Common_Unit.Name,
                         Body9 = t1.Name,
                         Body11 = Mkt_POSlave.Description
                     }).ToList();

            foreach (ReportDocType r in a)
            {
                int poSlaveID = 0;
                decimal balance = 0;
                decimal body3 = 0;
                decimal.TryParse(r.Body3, out body3);
                int.TryParse(r.Body7, out poSlaveID);
                decimal recived, returned,stockout;
                recived = GetTotalReceive(poSlaveID);
                //returned = GetTotalReturn(poSlaveID);
                stockout = GetTotalStockOut(poSlaveID);
                r.Body4 = recived.ToString("F");
                // r.Body5 = returned.ToString("F");
                r.Body5 = stockout.ToString("F");
                r.Body6 = (recived - stockout).ToString("F");

                decimal body4 = 0;
                decimal.TryParse(r.Body4, out body4);
                balance = body3 - body4;
                r.Body10 = balance.ToString("F");

                r.HeaderLeft2 = r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
                r.HeaderLeft3 = r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
                r.HeaderMiddle3 = r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);


            }

            this.OrderReportDoc = a.ToList();
        }
        //-------------------------------Closing Order Leftover Inventory--------------------------//
        internal void OrderLeftOverInventoryReport(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();
            VmMkt_PO Vpo = new VmMkt_PO();

            var a = (from mktPoSlave in db.Mkt_POSlave
                     join mktPo in db.Mkt_PO on mktPoSlave.Mkt_POFK equals mktPo.ID
                     join mktBom in db.Mkt_BOM on mktPo.Mkt_BOMFK equals mktBom.ID
                     join commonTheOrder in db.Common_TheOrder on mktBom.Common_TheOrderFk equals commonTheOrder.ID
                     join mktBuyer in db.Mkt_Buyer on commonTheOrder.Mkt_BuyerFK equals mktBuyer.ID
                     join mktItem in db.Mkt_Item on mktBom.Mkt_ItemFK equals mktItem.ID
                     join rawItem in db.Raw_Item on mktPoSlave.Raw_ItemFK equals rawItem.ID
                     join commonUnit in db.Common_Unit on mktPoSlave.Common_UnitFK equals commonUnit.ID
                     join commonSupplier in db.Common_Supplier on mktPo.Common_SupplierFK equals commonSupplier.ID
                     where commonTheOrder.ID == iD && mktPo.Active == true
                     && mktPoSlave.Active == true && commonTheOrder.IsComplete==true

                     select new ReportDocType
                     {
                         HeaderLeft1 = commonTheOrder.CID + "/" + mktBom.Style,
                         HeaderLeft2 = mktBom.FirstMove.ToString(),
                         HeaderLeft3 = mktBom.UpdateDate.ToString(),
                         HeaderLeft4 = mktBom.Reference.ToString(),
                         HeaderLeft5 = mktBom.Class.ToString(),
                         HeaderLeft6 = mktBom.Fabrication,
                         HeaderLeft7 = mktBom.SizeRange,
                         HeaderLeft8 = mktBom.Style,
                         HeaderMiddle1 = mktBuyer.Name,
                         HeaderMiddle2 = commonTheOrder.BuyerPO,
                         HeaderMiddle3 = commonTheOrder.OrderDate.ToString(),
                         HeaderMiddle4 = commonTheOrder.Season,
                         HeaderMiddle5 = mktItem.Name,

                         HeaderRight1 = mktBom.QPack.ToString(),
                         HeaderRight2 = mktBom.Quantity.ToString(),
                         HeaderRight3 = mktBom.UnitPrice.ToString(),
                         HeaderRight4 = ((mktBom.QPack * mktBom.Quantity).ToString()),
                         HeaderRight5 = ((mktBom.QPack * mktBom.Quantity) / 12).ToString(),
                         HeaderRight6 = ((mktBom.QPack * mktBom.Quantity) * mktBom.UnitPrice).ToString(),


                         Body1 = rawItem.Name,
                         Body2 = mktPoSlave.Description,
                         Body3 = commonSupplier.Name,
                         Body4 = mktPoSlave.TotalRequired.ToString(),
                         Body7 = mktPoSlave.ID.ToString(),
                     }).ToList();

            foreach (var r in a)
            {
                var poSlaveId = Convert.ToInt32(r.Body7);


                decimal totalRequired = 0;
                decimal.TryParse(r.Body4, out totalRequired);
                
                decimal recived, consumption;
                recived = GetTotalReceive(poSlaveId);
                consumption = GetPreviousDelivery(poSlaveId);
                var due = totalRequired - recived;
                r.Body5 = recived.ToString("F");
                r.Body6 = due.ToString();
                r.Body7 = consumption.ToString();
                r.Body8 = (recived - consumption).ToString();
                if (r.Body2 != null)
                {
                    r.Body1 = r.Body1 + " / " + r.Body2;
                }
                r.HeaderLeft2 = r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
                r.HeaderLeft3 = r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
                r.HeaderMiddle3 = r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);
                
            }

            this.OrderReportDoc = a.ToList();
        }
        internal decimal GetTotalReceive(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POSlaveFK == id
                     && t1.IsReturn == false
                     select t1.Quantity).Sum().GetValueOrDefault();
            return x;

        }
        internal decimal GetTotalStockOut(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Consumption
                     where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                     select (t1.Quantity)).Sum().GetValueOrDefault();
            return v;

        }
        internal decimal GetTotalReturn(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POSlaveFK == id
                     && t1.IsReturn == true
                     select t1.Quantity).Sum().GetValueOrDefault();
            return x;
        }
        internal decimal GetTotalConsumption(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Consumption
                     join t2 in db.Mkt_POExtTransfer on t1.Mkt_POExtTransferFK equals t2.ID
                     where t2.Mkt_POSlaveFK == id && t1.IsReturn == false
                     select (t1.Quantity)).Sum().GetValueOrDefault();
            return v;
        }
        internal decimal GetPreviousDelivery(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Consumption
                     where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                     select (t1.Quantity)).Sum().GetValueOrDefault();
            return v;
        }
        internal decimal GetPreviousDeliveryReturn(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Consumption
                     where t1.Mkt_POSlaveFK == id && t1.IsReturn == true
                     select (t1.Quantity)).Sum().GetValueOrDefault();
            return v;
        }

        internal void OrderInventoryBalanceReport(string id)
        {

            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();

            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID


                     where Mkt_BOM.ID == iD && Mkt_PO.Active == true
                     && Mkt_POSlave.Active == true


                     select new ReportDocType
                     {
                         HeaderLeft1 = Common_TheOrder.CID + "/" + Mkt_BOM.Style,
                         HeaderLeft2 = Mkt_BOM.FirstMove.ToString(),
                         HeaderLeft3 = Mkt_BOM.UpdateDate.ToString(),
                         HeaderLeft4 = Mkt_BOM.Reference.ToString(),
                         HeaderLeft5 = Mkt_BOM.Class.ToString(),
                         HeaderLeft6 = Mkt_BOM.Fabrication,
                         HeaderLeft7 = Mkt_BOM.SizeRange,
                         HeaderLeft8 = Mkt_BOM.Style,
                         HeaderMiddle1 = Mkt_Buyer.Name,
                         HeaderMiddle2 = Common_TheOrder.BuyerPO,
                         HeaderMiddle3 = Common_TheOrder.OrderDate.ToString(),
                         HeaderMiddle4 = Common_TheOrder.Season,
                         HeaderMiddle5 = Mkt_Item.Name,

                         HeaderRight1 = Mkt_BOM.QPack.ToString(),
                         HeaderRight2 = Mkt_BOM.Quantity.ToString(),
                         HeaderRight3 = Mkt_BOM.UnitPrice.ToString(),
                         HeaderRight4 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString()),
                         HeaderRight5 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
                         HeaderRight6 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
                         Body1 = Mkt_POSlave.Description,
                         Body2 = Raw_Item.Name,
                         Body3 = Mkt_POSlave.TotalRequired.ToString(),
                         Body7 = Mkt_POSlave.ID.ToString(),
                         Body8 = Common_Unit.Name
                     }).ToList();
            VmMkt_PO = new VmMkt_PO();
            foreach (ReportDocType r in a)
            {
                int poSlaveID = 0;
                decimal balance = 0;
                decimal body3 = 0;
                decimal body6 = 0;
                decimal body11 = 0;
                decimal body12 = 0;
                decimal.TryParse(r.Body3, out body3);
                decimal.TryParse(r.Body6, out body6);
                decimal.TryParse(r.Body11, out body11);
                decimal.TryParse(r.Body12, out body12);
                int.TryParse(r.Body7, out poSlaveID);

                decimal recived, returned, deliver, deliverReturned;
                recived = GetTotalReceive(poSlaveID);
                returned = GetTotalReturn(poSlaveID);
                deliver = GetPreviousDelivery(poSlaveID);
                deliverReturned = GetPreviousDeliveryReturn(poSlaveID);
                r.Body4 = (recived - returned).ToString("F");
                r.Body5 = (deliver - deliverReturned).ToString("F");
                //r.Body5 = returned.ToString("F");
                r.Body11 = deliver.ToString("F");
                r.Body12 = deliverReturned.ToString("F");
                r.Body6 = (body3 - recived).ToString("F");

                balance = (recived + deliverReturned) - (returned + deliver);
                r.Body10 = balance.ToString("F");

                r.HeaderLeft2 = r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
                r.HeaderLeft3 = r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
                r.HeaderMiddle3 = r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);


            }

            this.OrderReportDoc = a.ToList();
        }
        public void SelectSingleBOM(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from rc in db.Mkt_BOM
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = rc
                     }).Where(c => c.Mkt_BOM.ID == id).SingleOrDefault();
            this.Mkt_BOM = v.Mkt_BOM;

        }

        //...........Marchandier Order View............//
        internal void MarchandierOrderView(int id)
        {
            db = new xOssContext();

            var a = (from Mkt_BOM in db.Mkt_BOM
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join User_User in db.User_User
                     on Common_TheOrder.FirstCreatedBy equals User_User.ID
                     //join User_Role in db.User_Role
                     //on User_User.User_RoleFK equals User_Role.ID
                     where User_User.ID == id
                     &&
                     Common_TheOrder.Active == true
                     && Mkt_BOM.Active == true



                     select new ReportDocType
                     {

                         Body1 = Mkt_Buyer.Name + "/" + Common_TheOrder.BuyerPO,
                         Body2 = Mkt_BOM.Fabrication,
                         Body3 = Mkt_Item.Name,
                         Body4 = Mkt_BOM.QPack.ToString(),
                         Body5 = Mkt_BOM.Quantity.ToString(),
                         Body6 = Mkt_BOM.UnitPrice.ToString(),
                         Body7 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString()),
                         //Body8 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
                         Body9 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
                         int1 = User_User.ID,
                         int2 = User_User.User_AccessLevelFK.Value,
                         int3 = User_User.User_DepartmentFK.Value,
                         CustomObject = User_User

              }).ToList();

            foreach (ReportDocType r in a)

            {



                //r.Body12 = deliverReturned.ToString("F");
                r.Body6 = r.Body6.Substring(0, r.Body6.Length - 3);
                r.Body9 = r.Body9.Substring(0, r.Body9.Length - 3);

                //r.HeaderMiddle3 = r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);
                //int userID = 0;
                //i/nt.TryParse(r.Body10,out userID);
                this.User_User = r.CustomObject;
            }

            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.OrderReportDoc = a.Where(x => x.int1 == this.User_User.ID).ToList();
            }
            else
            {
                this.OrderReportDoc = a.ToList();
            }
            //this.OrderReportDoc = a.ToList();
            //this.OrderReportDoc = a;
        }


        //................Intial/In Process Report...............//
        //internal void IntailInProcessReport(int id)
        //{
        //    db = new xOssContext();
        //    int sl = 0;
        //    var a = (from Mkt_BOM in db.Mkt_BOM
        //             join Common_TheOrder in db.Common_TheOrder
        //             on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
        //             join Mkt_Buyer in db.Mkt_Buyer
        //             on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
        //             join Mkt_Item in db.Mkt_Item
        //             on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
        //             join Prod_DailyTarget in db.Prod_DailyTarget
        //             on Mkt_BOM.ID equals Prod_DailyTarget.MKTBOMFK
        //             join Pro in db.Plan_OrderProcess
        //             on Prod_DailyTarget.Plan_OrderProcessFk equals Pro.ID
        //             //join Mkt_BOMSlave in db.Mkt_BOMSlave
        //             //on Mkt_BOM.ID  equals Mkt_BOMSlave.Mkt_BOMFK

        //             //join Common_Unit in db.Common_Unit
        //             //on Mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID

        //             where Mkt_BOM.ID == id
        //             &&
        //             Common_TheOrder.Active == true
        //             && Mkt_BOM.Active == true
        //             && Pro.StepName == "PACKING & CTN"
        //             select new ReportDocType
        //             {
        //                 HeaderLeft1 = Mkt_Buyer.Name,
        //                 HeaderLeft2 = Common_TheOrder.CID + "/" + Mkt_BOM.Style,
        //                 HeaderLeft3 = Prod_DailyTarget.CtnFillUp.ToString(),
        //                 Body1 = Mkt_BOM.Fabrication + "\n" + Mkt_BOM.Description,
        //                 //Body2 = Mkt_Item.Name,
        //                 //Body3 = Common_Unit.Name,
        //                 Body2= Prod_DailyTarget.Color +"\n"+ Prod_DailyTarget.Size,
        //                 Body5 = (Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString(),
        //                 Body6 = (Prod_DailyTarget.PackFillUp).ToString()


        //             }).ToList();
        //   int ShortQTY = 0,ExcessQTY = 0, QTY = 0;

        //    foreach (ReportDocType r in a)
        //    {
        //        int body5 = 0;
        //        int.TryParse(r.Body5, out body5);

        //        int body6 = 0;
        //        int.TryParse(r.Body6, out body6);
        //        QTY = body5 - body6;
        //        //ExcessQTY = body6 - body5;

             
        //        if (QTY >= 0)
        //        {
        //            ShortQTY = QTY;
        //            ExcessQTY = 0;
        //        }
        //        else if(QTY < 0)
        //        {
        //            ExcessQTY = QTY;
        //            ShortQTY = 0;
        //        }
        //        string x = "0";
        //        string b = ShortQTY.ToString();               
        //        string c = x + ExcessQTY.ToString();
        //        c = c.Replace(x + "-", "");
        //        b = b.Replace(x + "+", "");
          
        //        r.Body7 = b;
        //        r.Body8 = c;
        //        r.Body10 = (++sl).ToString();

        //    }
        //    this.OrderReportDoc = a.ToList();

        //}

        //....................Mock Order Inventory..............//

        internal void OrderReport()
        {
            db = new xOssContext();
            int sl = 0;
            var a = (from Mkt_BOM in db.Mkt_BOM
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                 
                     where Common_TheOrder.Active == true
                
              
                     select new ReportDocType
                     {
                        HeaderLeft1 = "",
                        HeaderLeft2 = "",
                        HeaderLeft3 = "",
                        Body1 = "",
                        Body2 = "",
                 
                     }).ToList();


            this.OrderReportDoc = a;
         

        }


        //------SummaryReport-----
        public void OrderSummaryReport()
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Common_Currency on t1.Common_CurrencyFK equals t2.ID
                     join t3 in db.Mkt_Buyer on t1.Mkt_BuyerFK equals t3.ID
                     join t4 in db.User_User on t1.FirstCreatedBy equals t4.ID
                     join t5 in db.Mkt_BOM on t1.ID equals t5.Common_TheOrderFk
                     where t1.Active == true &&
                     t5.Active == true
                     select new VmCommon_TheOrder
                     {
                         Common_TheOrder = t1,
                         Common_Currency = t2,
                         Mkt_Buyer = t3,
                         User_User = t4,
                         Mkt_BOM = t5,
                         TotalOrderValue = t5.QPack * t5.Quantity * t5.UnitPrice
                     }).OrderByDescending(x => x.Common_TheOrder.ID).AsEnumerable();

            this.DataList = v;
        }
    }
}
