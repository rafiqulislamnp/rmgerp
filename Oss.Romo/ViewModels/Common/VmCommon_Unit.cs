﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Oss.Romo.Models.Common;
using Oss.Romo.Models;
namespace Oss.Romo.ViewModels.Common
{
    public class VmCommon_Unit
    {
        private xOssContext db;
        public IEnumerable<VmCommon_Unit> DataList { get; set; }

        public Common_Unit Common_Unit { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }

        public VmCommon_Unit()
        {
            //db = new xOssContext();
            InitialDataLoad();
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_Unit
                     select new VmCommon_Unit
                     {
                         Common_Unit = t1
                     }).Where(x => x.Common_Unit.Active == true).OrderByDescending(x => x.Common_Unit.ID).AsEnumerable();
            this.DataList = v;
        }

        
        public void SelectSingle(string uiD)
        {
            int id = 0;
            uiD = this.VmControllerHelper.Decrypt(uiD);
            Int32.TryParse(uiD, out id);
            db = new xOssContext();
            var v = (from rc in db.Common_Unit
                     select new VmCommon_Unit
                     {
                         Common_Unit = rc
                     }).Where(c => c.Common_Unit.ID == id).SingleOrDefault();
            this.Common_Unit = v.Common_Unit;

        }

        public int Add(int userID)
        {
            Common_Unit.AddReady(userID);
            return Common_Unit.Add();
        }

        public bool Edit(int userID)
        {
            return Common_Unit.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Common_Unit.Delete(userID);
        }


        //create 
        // delete
        //edit

    }
    
}