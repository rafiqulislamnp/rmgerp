﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Common
{
    public class BOMShortDetails
    {
        public int? BOMID { get; set; }
        public decimal? TOTAL { get; set; }
        public decimal? TotalCM { get; set; }
        public decimal? CMperDZ { get; set; }
    }
    public class VmShortPO
    {
        public int ID { get; set; }
        public string CID { get; set; }
        public string SupplierName { get; set; }
        [DisplayName("Po Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? PoDate { get; set; }
        public decimal Value { get; set; }
        public bool IsAuthorize { get; set; }
    }

    public class VmOrderDeliverySchedule
    {
        public string Destination { get; set; }
        public string PortNo { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Delivery Date")]
        public DateTime Date { get; set; }
        public decimal Quantity { get; set; }

    }
    public class VmOrderSummary
    {
        private xOssContext db;
        public decimal? TotalPoOfStyle { get; set; }
        public List<VmOrderDeliverySchedule> VmOrderDeliverySchedule { get; set; } 
        public List<BOMShortDetails> BOMShortDetails { get; set; }
        public decimal QuantityInPc { get; set; }
        public decimal QuantityInDz { get; set; }
        public decimal TotalPrice { get; set; }



        public decimal? Commission { get; set; }

        public decimal TotalFOB { get; set; }

        public decimal TotalQDZ { get; set; }
        public decimal ItemPrice { get; set; }
        public string SearchString { get; set; }
        public string Header { get; set; }
        public string ButtonName { get; set; }

        public IEnumerable<VmOrderSummary> DataList { get; set; }
        public List<object> DDownData { get; set; }

        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }

        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public VmMkt_PO VmMkt_PO { get; set; }
        public Raw_Item Raw_Item { get; set; }

        public User_User User_User { get; set; }
        public Mkt_SubCategory Mkt_SubCategory { get; set; }
        public Mkt_Category Mkt_Category { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public List<ReportDocType> OrderReportDoc { get; set; }

        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmOrderSummary()
        {
            db = new xOssContext();
            VmControllerHelper = new VmControllerHelper();

        }

        public void InitialDataLoaForOrderSummary(bool flag)
        {
            db = new xOssContext();
            var v = (from mktBOM in db.Mkt_BOM
                     join commonTheOrder in db.Common_TheOrder on mktBOM.Common_TheOrderFk equals commonTheOrder.ID
                     join commonCurrency in db.Common_Currency on commonTheOrder.Common_CurrencyFK equals commonCurrency.ID
                     join mktItem in db.Mkt_Item on mktBOM.Mkt_ItemFK equals mktItem.ID                    
                     join userUser in db.User_User on commonTheOrder.FirstCreatedBy equals userUser.ID
                     where commonTheOrder.IsComplete == flag && mktBOM.Active == true && commonTheOrder.Active == true && mktBOM.ID != 1
                     select new VmOrderSummary
                     {
                         Mkt_BOM = mktBOM,
                         Common_TheOrder = commonTheOrder,
                         Common_Currency = commonCurrency,
                         Mkt_Item = mktItem,                        
                         User_User = userUser,
                         QuantityInPc = mktBOM.QPack * mktBOM.Quantity,
                         QuantityInDz = (mktBOM.QPack * mktBOM.Quantity) / 12,
                         TotalPrice = mktBOM.QPack * mktBOM.Quantity * mktBOM.UnitPrice,
                         Commission = (((((mktBOM.QPack * mktBOM.Quantity) * mktBOM.UnitPrice) * commonTheOrder.Commission)) / 100),
                         TotalFOB = ((mktBOM.QPack * mktBOM.Quantity) * mktBOM.UnitPrice),
                         TotalPoOfStyle = ((from t1 in db.Mkt_POSlave join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID where t1.Active == true && t2.Active == true && t2.Mkt_BOMFK == mktBOM.ID select t1.TotalRequired * t1.Price).DefaultIfEmpty(0).Sum()),
                         BOMShortDetails = (from x in db.Mkt_BOM
                                            where x.Active == true && x.ID == mktBOM.ID && x.ID > 1
                                            select new BOMShortDetails
                                            {
                                                TOTAL = (from t1 in db.Mkt_BOMSlave where t1.Mkt_BOMFK == x.ID && t1.Active == true select t1.RequiredQuantity * t1.Price).DefaultIfEmpty(0).Sum(),
                                                TotalCM = ((x.QPack * x.Quantity) * x.UnitPrice) - ((from t1 in db.Mkt_BOMSlave where t1.Mkt_BOMFK == x.ID && t1.Active == true select t1.RequiredQuantity * t1.Price).DefaultIfEmpty(0).Sum() + (((((x.QPack * x.Quantity) * x.UnitPrice) * commonTheOrder.Commission)) / 100)),
                                                CMperDZ = (((x.QPack * x.Quantity) * x.UnitPrice) - ((from t1 in db.Mkt_BOMSlave where t1.Mkt_BOMFK == x.ID && t1.Active == true select t1.RequiredQuantity * t1.Price).DefaultIfEmpty(0).Sum() + (((((x.QPack * x.Quantity) * x.UnitPrice) * commonTheOrder.Commission)) / 100))) / ((x.QPack * x.Quantity) / 12)
                                            }).ToList(),
                         VmOrderDeliverySchedule = (from t1 in db.Mkt_OrderDeliverySchedule join t2 in db.Common_Country on t1.Destination equals t2.ID where t1.Mkt_BOMFk == mktBOM.ID && t1.Active == true select new VmOrderDeliverySchedule { Destination = t2.Name, PortNo = t1.PortNo, Date = t1.Date, Quantity = t1.Quantity }).ToList()
                     }).OrderByDescending(x => x.Common_TheOrder.ID).ToList();          
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 1)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }
        }

        private List<BOMShortDetails> GetBOMTotalCM(int bomID)
        {
            List<BOMShortDetails> bOMShortDetails = new List<BOMShortDetails>();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_POSlave
                     on t1.ID equals t2.Mkt_POFK
                     where t1.Mkt_BOMFK == bomID
                     && t1.Active == true && t2.Active == true
                     select new BOMShortDetails
                     {
                         BOMID = t1.Mkt_BOMFK,
                         TOTAL = 0

                     }).Distinct();

            foreach (BOMShortDetails x in v)
            {
                BOMShortDetails y = new BOMShortDetails();
                y.BOMID = x.BOMID;
                y.TOTAL = TotlaBomCost(x.BOMID.Value);
                //y.TotalCM = y.TOTAL TTTCM = FOB -(Total + Com)
                bOMShortDetails.Add(y);
            }
            return bOMShortDetails;
        }

        private List<BOMShortDetails> GetBOMShortDetails(int bomID)
        {
            List<BOMShortDetails> bomShortDetails = new List<BOMShortDetails>();

            var v = (from Mkt_BOM in db.Mkt_BOM
                     join Common_TheOrder in db.Common_TheOrder on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     where Mkt_BOM.ID == bomID
                     && Mkt_BOM.Active == true && Common_TheOrder.Active == true && Mkt_BOM.ID != 1
                     select new BOMShortDetails
                     {
                         BOMID = Mkt_BOM.ID,
                         TOTAL = (from t1 in db.Mkt_BOMSlave where t1.Mkt_BOMFK == bomID && t1.Active == true select t1.RequiredQuantity * t1.Price).DefaultIfEmpty(0).Sum(),
                         TotalCM = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice) - ((from t1 in db.Mkt_BOMSlave where t1.Mkt_BOMFK == bomID && t1.Active == true select t1.RequiredQuantity * t1.Price).DefaultIfEmpty(0).Sum() + (((((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice) * Common_TheOrder.Commission)) / 100)),
                         CMperDZ = (((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice) - ((from t1 in db.Mkt_BOMSlave where t1.Mkt_BOMFK == bomID && t1.Active == true select t1.RequiredQuantity * t1.Price).DefaultIfEmpty(0).Sum() + (((((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice) * Common_TheOrder.Commission)) / 100)) / ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12)),

                         //Fob = (Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice,
                         //TotalCom = (((((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice) * Common_TheOrder.Commission)) / 100),
                         //TotalDz = (Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12,
                         //total = (from t1 in db.Mkt_BOMSlave where t1.Mkt_BOMFK == bomID  && t1.Active == true  select t1.RequiredQuantity * t1.Price).DefaultIfEmpty(0).Sum()


                     }).Distinct().ToList();

            //foreach (var x in v)
            //{
            //    if (x.TotalDz != 0)
            //    {
            //        BOMShortDetails y = new BOMShortDetails();
            //        y.BOMID = x.BOMID;
            //        y.TOTAL = TotlaBomCost(x.BOMID);
            //        y.TotalCM = x.Fob - (y.TOTAL + x.TotalCom);


            //        y.CMperDZ = y.TotalCM / x.TotalDz;
            //        bomShortDetails.Add(y);
            //    }
            //}
            return v;
        }



        //private List<BOMShortDetails> GetBOMShortDetails(int bomID)
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Mkt_BOMSlave
        //             join t2 in db.Mkt_BOM
        //             on t1.Mkt_BOMFK equals t2.ID
        //             where t1.Mkt_BOMFK == bomID
        //             //join t3 in db.Mkt_Item
        //             //on t1.Raw_ItemFK equals t3.ID
        //             select new BOMShortDetails
        //             {
        //                 ID = t2.ID,
        //                 //Name = t3.Name,
        //                 Value = 0
        //             });

        //    foreach (BOMShortDetails x in v)
        //    {
        //        x.Value += GetTTL(x.ID);
        //    }
        //    return v.ToList();
        //}

        //public decimal? GetTTL(int BomID)
        //{

        //    db = new xOssContext();
        //    var v = (from t1 in db.Mkt_BOMSlave
        //             where t1.Mkt_BOMFK == BomID
        //             select (t1.Price * t1.RequiredQuantity)).Sum();
        //    return v;


        //}
        public decimal TotalPoValueOfStyle { get; set; }
        public decimal? GetShortPO(int bomID)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     where t1.Mkt_BOMFK == bomID
                     && t1.Active == true
                     select new VmOrderSummary
                     {
                         Mkt_PO = t1,
                         Mkt_BOM = t2
                     }).Distinct();
            decimal? TotalPoValueOfStyle = 0;
            foreach (var x in v)
            {
                VmMkt_PO vmMktPO = new VmMkt_PO();
                TotalPoValueOfStyle += vmMktPO.TotlaValue(x.Mkt_PO.ID);
            }
            return TotalPoValueOfStyle;
        }

        public List<VmOrderDeliverySchedule> GetOrderDeliverySchedule(int bomId)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     join t2 in db.Common_Country on t1.Destination equals t2.ID

                     where t1.Mkt_BOMFk == bomId && t1.Active == true

                     select new VmOrderDeliverySchedule
                     {
                         Destination = t2.Name,
                         PortNo = t1.PortNo,
                         Date = t1.Date,
                         Quantity = t1.Quantity
                     }).ToList();


            return v;

        }
        public decimal? GetPOValue(int ID)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     where t1.Mkt_POFK == ID
                     && t1.Active == true
                     select (t1.Price * t1.TotalRequired)).Sum();
            return v;

        }
        public decimal? TotlaBomCost(int bomID)
        {
            db = new xOssContext();
            return (from t1 in db.Mkt_BOMSlave

                    where t1.Mkt_BOMFK == bomID
                    && t1.Active == true
                    select t1.RequiredQuantity * t1.Price).DefaultIfEmpty(0).Sum();

        }


    }
}