﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Common
{
    public class VmCommon_Supplier
    {
        private xOssContext db;
        public IEnumerable<VmCommon_Supplier> DataList { get; set; }
        public Common_Country Common_Country { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Common_SupplierType Common_SupplierType { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }

        [DisplayName("Chart 2 Account   ")]
        public int Acc_Chart2IDFk { get; set; }
        public VmCommon_Supplier()
        {
            db = new xOssContext();
            InitialDataLoad();
            VmControllerHelper = new VmControllerHelper();

        }

        public void InitialDataLoad()
        {
            var v = (from t1 in db.Common_Supplier
                     join t2 in db.Common_Country on t1.Common_CountryFk equals t2.ID
                     join t3 in db.Common_SupplierType on t1.Common_SupplierTypeFk equals t3.ID
                     select new VmCommon_Supplier
                     {
                         Common_Supplier = t1,
                         Common_Country = t2,
                         Common_SupplierType = t3
                     }).Where(x => x.Common_Supplier.Active == true).OrderByDescending(x => x.Common_Supplier.ID).OrderBy(x => x.Common_Supplier.Name).AsEnumerable();
            this.DataList = v;
        }


        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from rc in db.Common_Supplier
                     join t2 in db.Common_Country on rc.Common_CountryFk equals t2.ID
                     join t3 in db.Common_SupplierType on rc.Common_SupplierTypeFk equals t3.ID
                     select new VmCommon_Supplier
                     {
                         Common_Country = t2,
                         Common_Supplier = rc,
                         Common_SupplierType = t3
                     }).Where(c => c.Common_Supplier.ID == id).SingleOrDefault();
            this.Common_Supplier = v.Common_Supplier;
            this.Common_Country = v.Common_Country;
        }

        public int Add(int userID)
        {
            Common_Supplier.AddReady(userID);
            return Common_Supplier.Add();
        }

        public bool Edit(int userID)
        {
            return Common_Supplier.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Common_Supplier.Delete(userID);
        }
    }
}