﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Common
{
    public class VmCommon_Permission
    {
        private xOssContext db;
        public IEnumerable<VmCommon_Permission> DataList { get; set; }

        public Common_Method Common_Method { get; set; }
        public Common_Controller Common_Controller { get; set; }
        public Common_User Common_User { get; set; }
        public Common_Permission Common_Permission { get; set; }
        public VmCommon_Permission()
        {
            //db = new xOssContext();
            InitialDataLoad();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_Permission
                     from t2 in db.Common_User
                     where t1.Common_UserFk==t2.ID
                     from t3 in db.Common_Controller
                     from t4 in db.Common_Method
                     where t4.Common_ControllerFk==t3.ID
                     where t1.Common_MethodFk==t4.ID
                     select new VmCommon_Permission
                     {
                         Common_Permission = t1
                     }).Where(x => x.Common_Permission.Active == true).OrderByDescending(x => x.Common_Permission.ID).AsEnumerable();
            this.DataList = v;

        }


        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from rc in db.Common_Permission
                     select new VmCommon_Permission
                     {
                         Common_Permission = rc
                     }).Where(c => c.Common_Permission.ID == iD).SingleOrDefault();
            this.Common_Permission = v.Common_Permission;

        }

        public int Add(int userID)
        {
            Common_Permission.AddReady(userID);
            return Common_Permission.Add();
        }

        public bool Edit(int userID)
        {
            return Common_Permission.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Common_Permission.Delete(userID);
        }
    }
}