﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.ComponentModel;
using System.IO;

namespace Oss.Romo.ViewModels.Common
{
    public class VmCommon_User
    {
        private xOssContext db;
        public IEnumerable<VmCommon_User> DataList { get; set; }
        
        public string LblError { get; set; }
        public Common_UserRoles Common_UserRoles { get; set; }
        public Common_Method Common_Method { get; set; }
        public Common_Roles Common_Roles { get; set; }
        public Common_RolesPermission Common_RolesPermission { get; set; }
        
        [Required(ErrorMessage = "Required")]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "Minimum 6 upto 12 Chracter")]
        [DisplayName("Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Required")]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "Minimum 6 upto 12 Chracter")]
        [DisplayName ("Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public HttpPostedFileBase Photo { get; set; }

        public Common_User Common_User { get; set; }
 

        public void InitialDataLoad()
        {
            db = new xOssContext();

            var v = (from Common_User in db.Common_User
                     select new VmCommon_User
                     {
                         Common_User = Common_User
                     }).Where(x => x.Common_User.Active == true).OrderByDescending(x => x.Common_User.ID).AsEnumerable();
            this.DataList = v;

        }
        public void UserDataLoad()
        {
            db = new xOssContext();

            var v = (from user in db.Common_User
                     join ur in db.Common_UserRoles
                     on user.ID equals ur.Common_UserFk
                     join r in db.Common_Roles
                     on ur.Common_RolesFk equals r.ID
                     join permission in db.Common_RolesPermission
                      on r.ID equals permission.Common_RolesFK
                     join method in db.Common_Method
                    on permission.Common_MethodFK equals method.ID
                     select new VmCommon_User
                     {
                         Common_UserRoles = ur,
                         Common_Roles = r,
                         Common_User = user,
                         Common_RolesPermission = permission,
                         Common_Method = method
                     }).Where(x => x.Common_User.Active == true).OrderByDescending(x => x.Common_User.ID).AsEnumerable();
            this.DataList = v;

        }
        //public void SelectSingleProfile()
        //{
        //    db = new xOssContext();
        //    var b = (from Common_User in db.Common_User
        //             select new VmCommon_User
        //             {


        //             }).Single();
        //    this.Common_User =


        //}




        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from rc in db.Common_User
                     select new VmCommon_User
                     {
                         Common_User = rc
                     }).SingleOrDefault(c => c.Common_User.ID == id);
            if (v != null) this.Common_User = v.Common_User;
        }
        private string GetHashedPassword(string password)
        {
            char[] array = password.ToCharArray();
            Array.Reverse(array);
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(array);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }           
            return sb.ToString();
        }
        public int Add(int userID, string imagePath)
        {
      
            Common_User.AddReady(0);
            Common_User.Password = GetHashedPassword(this.Password);
            Upload(imagePath);
            return Common_User.Add();
        }
        public bool EditPassword(int userID)
        {
           
            Common_User.Password = GetHashedPassword(this.Password);
            return Common_User.Edit(userID);
        }
        public bool Edit(int userID)
        {
           
            return Common_User.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Common_User.Delete(userID);
        }
        public void Upload(string imagePath)
        {
            string exten = Path.GetFileName(Photo.FileName);
            string fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length- exten.IndexOf("."));
            string filePath = Path.Combine(imagePath + fName);
            Photo.SaveAs(filePath);
            Common_User.Photo = fName;

            //int w, h;
            //w = h = 40;
            //var orginal = System.Drawing.Image.FromFile(filePath);
            //if (orginal.Width > orginal.Height)
            //{
            //    h = (int)(40 * (double)orginal.Height / orginal.Width);
            //}
            //else
            //{
            //    w = (int)(40 * (double)orginal.Width / orginal.Height);
            //}


            //System.Drawing.Image thumb = orginal.GetThumbnailImage(w, h, null, IntPtr.Zero);

            //thumb.Save(filePath);



        }
    }
}