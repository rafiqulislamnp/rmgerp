﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using CrystalDecisions.Web.HtmlReportRender;

using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Planning;
using Oss.Romo.ViewModels.Store;

namespace Oss.Romo.ViewModels.Home
{
    public class SummaryReportAdminBoard
    {
        public decimal MasterLCValue { get; set; }

        public decimal ExportValue { get; set; }

        public decimal BOMValue { get; set; }

        public decimal POIssuedValue { get; set; }

        public decimal BTBLimitValue { get; set; }

        public decimal BTBIssuedValue { get; set; }

        public decimal BTBAvailableValue { get; set; }

        public decimal BOMRestValue { get; set; }

        public decimal PORestValue { get; set; }

        public decimal POPending { get; set; }

        public string MasterLCNo { get; set; }

        [Required, Display(Name = "From Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Required, Display(Name = "To Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
    }
    public class VmMis_AdminBoard
    {
        private xOssContext db;
        public Plan_OrderStep Plan_OrderStep { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public int Buyer { get; set; }
        public int Order { get; set; }
        public Plan_OrderProcess Plan_OrderProcess { get; set; }
        public Plan_OrderProcessCategory Plan_OrderProcessCategory { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public SummaryReportAdminBoard SummaryReportAdminBoard { get; set; }
        public decimal RealisedAmount { get; set; }
        public decimal TotalLcTtAmount { get; set; }
        public decimal TotalLcBtbLcPoAmount { get; set; }
        public VmStoreInventory VmStoreInventory { get; set; }
        public List<SummaryReportAdminBoard> BTBList { get; set; }
        public List<CollectionDueReceivableAmount> CollectionDueReceivableAmount { get; set; }
        public List<BusinessCashFlow> BusinessCashFlow { get; set; }
        public List<FactoryOverview> FactoryOverview { get; set; }
        public VmAcc_AcName VmAcc_AcName { get; set; }
        public List<ProductionStatus> ProductionStatus { get; set; }
        public List<BuyerInfoChart> BuyerInfoChartList { get; set; }
        public List<SupplierInfoChart> SupplierInfoChartList { get; set; }
        [Required, Display(Name = "From Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        public IEnumerable<VmMis_AdminBoard> DataList { get; set; }
        public List<VmMis_AdminBoard> MisAdminBoardList { get; set; }
        [Required, Display(Name = "To Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public int TotalOrderQuantity { get; set; }
        public int? TotalProductionQuantity { get; set; }
        public DataTable DataTable { get; set; }
        public void GetSummaryReport()
        {
            db = new xOssContext();
            SummaryReportAdminBoard model = new SummaryReportAdminBoard();
            decimal BTBIssuedValue = decimal.Zero;
            decimal BOMValue = decimal.Zero;
            decimal POIssuedValue = decimal.Zero;
            decimal MasterLCValue = decimal.Zero;
            decimal BTBLimitValue = decimal.Zero;
            decimal ExportValue = decimal.Zero;


            decimal HandlingCharge = decimal.Zero;
            decimal FreightCharge = decimal.Zero;
            decimal GarmentTest = decimal.Zero;
            decimal BuyerCharge = decimal.Zero;
            decimal TotalCharge = decimal.Zero;

            #region ExecuteQuery
            var vData = (from ud in db.Commercial_UD
                         where ud.Active == true
                         group new { ud } by new { ud.ID } into groupUd
                         select new
                         {
                             MasterLC = (from udslave in db.Commercial_UDSlave
                                         join mlc in db.Commercial_MasterLC on udslave.Commercial_MasterLCFK equals mlc.ID
                                         where
                                         udslave.Commercial_UDFK == groupUd.Key.ID &&
                                         udslave.Active == true &&
                                         mlc.Active == true
                                         group mlc by new { mlc.ID } into groupMLC
                                         select new
                                         {
                                             TotalMLc = groupMLC.Sum(a => a.TotalValue),
                                             HandlingCharge = groupMLC.Sum(a => a.HandlingCharge),
                                             FreightCharge = groupMLC.Sum(a => a.FreightCharge),

                                             //TotalBTBLimit = allmlc.Sum(a => (a.mlc.TotalValue * a.BTBOpening) / 100),

                                             TotalBOM = (from mlcbuyerpo in db.Commercial_MasterLCBuyerPO
                                                         join bom in db.Mkt_BOM on mlcbuyerpo.MKTBOMFK equals bom.ID
                                                         join order in db.Common_TheOrder on bom.Common_TheOrderFk equals order.ID
                                                         where
                                                         order.Active == true &&
                                                         bom.Active == true &&
                                                         mlcbuyerpo.Active == true &&
                                                         mlcbuyerpo.Commercial_MasterLCFK == groupMLC.Key.ID
                                                         group new { bom.QPack, bom.Quantity, bom.UnitPrice, order } by new { bom.ID } into allPO
                                                         select new
                                                         {
                                                             TotalPOValue = allPO.Sum(a => (a.QPack * a.Quantity * a.UnitPrice)),

                                                             BOMValue = (from o in db.Mkt_BOMSlave
                                                                         where o.Active == true && o.Mkt_BOMFK == allPO.Key.ID
                                                                         select new { o }).ToList().Sum(b => (b.o.RequiredQuantity * b.o.Price)),

                                                             TotalPOIssued = (from mktpo in db.Mkt_PO
                                                                              join poslave in db.Mkt_POSlave on mktpo.ID equals poslave.Mkt_POFK
                                                                              join currency in db.Common_Currency on mktpo.Common_CurrencyFK equals currency.ID
                                                                              where
                                                                              poslave.Active == true &&
                                                                              mktpo.Mkt_BOMFK == allPO.Key.ID &&
                                                                              mktpo.Active == true
                                                                              select new { poslave }).ToList().Sum(a => a.poslave.TotalRequired * a.poslave.Price),

                                                             BuyerCommission = allPO.Sum(a => (a.QPack * a.Quantity * a.UnitPrice) / 100 * a.order.Commission)
                                                         }).ToList(),
                                         }).ToList(),

                             BTBIssuedValue = (from b2b in db.Commercial_B2bLC
                                               where
                                               b2b.Active == true &&
                                               b2b.Commercial_UDFK == groupUd.Key.ID
                                               group b2b.Amount by new { b2b.ID } into allMlc
                                               select new
                                               {
                                                   total = allMlc.Sum()
                                               }).ToList()
                         }).ToList();

            if (vData.Any())
            {
                foreach (var udItem in vData)
                {

                    if (udItem.MasterLC.Any())
                    {
                        foreach (var lcItem in udItem.MasterLC)
                        {
                            MasterLCValue += lcItem.TotalMLc == null ? 0 : (decimal)lcItem.TotalMLc;
                            HandlingCharge += lcItem.HandlingCharge == null ? 0 : (decimal)lcItem.HandlingCharge;
                            FreightCharge += lcItem.FreightCharge == null ? 0 : (decimal)lcItem.FreightCharge;

                            if (lcItem.TotalBOM.Any())
                            {
                                foreach (var bom in lcItem.TotalBOM)
                                {
                                    ExportValue += bom.TotalPOValue;
                                    BOMValue += bom.BOMValue == null ? 0 : (decimal)bom.BOMValue;
                                    POIssuedValue += bom.TotalPOIssued == null ? 0 : (decimal)bom.TotalPOIssued;
                                    BuyerCharge += bom.BuyerCommission == null ? 0 : (decimal)bom.BuyerCommission;
                                }
                            }

                        }
                    }

                    if (udItem.BTBIssuedValue.Any())
                    {
                        foreach (var b2bItem in udItem.BTBIssuedValue)
                        {
                            BTBIssuedValue += b2bItem.total == null ? 0 : (decimal)b2bItem.total;
                        }
                    }

                }
            }

            #endregion

            TotalCharge = HandlingCharge + FreightCharge + BuyerCharge;

            decimal ActualPO = ExportValue - TotalCharge;
            BTBLimitValue = (ActualPO * 75) / 100;

            //model.MasterLCValue = "$ " + Math.Round(MasterLCValue).ToString("#,##0");
            //model.BOMValue = "$ " + Math.Round(BOMValue).ToString("#,##0");
            //model.ExportValue = "$ " + Math.Round(ActualPO).ToString("#,##0");

            //model.POIssuedValue = "$ " + Math.Round(POIssuedValue).ToString("#,##0");
            //model.POPending = "$ " + Math.Round(BOMValue - POIssuedValue).ToString("#,##0");

            //model.BTBLimitValue = "$ " + Math.Round(BTBLimitValue).ToString("#,##0");
            //model.BTBIssuedValue = "$ " + Math.Round(BTBIssuedValue).ToString("#,##0");

            //model.BTBAvailableValue = "$ " + Math.Round(BTBLimitValue - BTBIssuedValue).ToString("#,##0");
            //model.BOMRestValue = "$ " + Math.Round(BOMValue - BTBIssuedValue).ToString("#,##0");
            //model.PORestValue = "$ " + Math.Round(POIssuedValue - BTBIssuedValue).ToString("#,##0");
            model.MasterLCValue = MasterLCValue;
            model.BOMValue = BOMValue;
            model.ExportValue = ActualPO;

            model.POIssuedValue = POIssuedValue;
            model.POPending = BOMValue - POIssuedValue;

            model.BTBLimitValue = BTBLimitValue;
            model.BTBIssuedValue = BTBIssuedValue;

            model.BTBAvailableValue = BTBLimitValue - BTBIssuedValue;
            model.BOMRestValue = BOMValue - BTBIssuedValue;
            model.PORestValue = POIssuedValue - BTBIssuedValue;
            this.SummaryReportAdminBoard = model;
        }
        public void GetRealisedAmountLoad()
        {
            try
            {
                db = new xOssContext();
                var amount = db.Acc_ExportRealisation.AsEnumerable().Sum(o => o.RealisedAmount);
                this.RealisedAmount = amount;
            }
            catch (Exception e)
            {
                //
            }
        }
        public void GetTotalLcTtAmount()
        {
            try
            {
                db = new xOssContext();
                var amount = (from t1 in db.Commercial_B2bLC where t1.Commercial_BtBLCTypeFK == 3 select t1.Amount).Sum();
                this.TotalLcTtAmount = amount;
            }
            catch (Exception e)
            {
                //
            }
        }
        public void GetTotalLcBtbLcPoAmount()
        {
            try
            {
                db = new xOssContext();
                var amount = (from t1 in db.Commercial_B2bLcSupplierPOSlave
                              join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                              select (decimal)(t1.TotalRequired * t2.Price)).Sum();
                this.TotalLcBtbLcPoAmount = amount;
            }
            catch (Exception e)
            {
                //
            }
        }
        public void GetCollectionDueReceivableAmountLast12Months(DateTime date)
        {
            try
            {
                List<CollectionDueReceivableAmount> listBtb = new List<CollectionDueReceivableAmount>();
                db = new xOssContext();
                for (int i = 12; i >= 1; i--)
                {
                    var newdate = date.AddMonths(-i);
                    var firstDayOfMonth = new DateTime(newdate.Year, newdate.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    Acc_Type t = new Acc_Type();
                    IEnumerable<Acc_Type> listOfAcc_Type = from tbl in t.GetAcc_Type()
                                                           select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

                    VmAcc_IncomeStatement vmAcc_IncomeStatement = new VmAcc_IncomeStatement();
                    vmAcc_IncomeStatement.FromDate = firstDayOfMonth;
                    vmAcc_IncomeStatement.ToDate = lastDayOfMonth;

                    var TotalIncomeFromJournal = vmAcc_IncomeStatement.TotalIncomeFromJournal(listOfAcc_Type);
                    var TotalReceivableForMis = vmAcc_IncomeStatement.TotalReceivableForMis(listOfAcc_Type);
                    //var TotalExpenseFromJournal = vmAcc_IncomeStatement.TotalExpenseFromJournal(listOfAcc_Type);
                    //var TotalExpenseFromAcc_TransactionAndExpHistory = vmAcc_IncomeStatement.TotalExpenseFromAcc_TransactionAndExpHistory(listOfAcc_Type);


                    var joinqueryIncome = TotalIncomeFromJournal.VmAcc_IncomeStatementReport.Sum(c => c.Balance);
                    var receivable = TotalReceivableForMis.VmAcc_IncomeStatementReport.Sum(c => c.Balance);

                    CollectionDueReceivableAmount collectionDueReceivableAmount = new CollectionDueReceivableAmount();
                    collectionDueReceivableAmount.MonthName = newdate.ToString("MMM") + "-" + newdate.ToString("yy");

                    if (i == 12)
                    {
                        collectionDueReceivableAmount.Income = TotalIncomeFromJournal.OpeingBalance + joinqueryIncome;
                        collectionDueReceivableAmount.Receivable = TotalReceivableForMis.OpeingBalance + receivable;
                    }
                    else
                    {
                        collectionDueReceivableAmount.Income = joinqueryIncome;
                        collectionDueReceivableAmount.Receivable = receivable;
                    }



                    listBtb.Add(collectionDueReceivableAmount);
                }
                this.CollectionDueReceivableAmount = listBtb;
            }
            catch (Exception e)
            {
                //
            }
        }
        public void GetCollectionDueReceivableAmountLastMonths(DateTime date)
        {
            try
            {
                List<CollectionDueReceivableAmount> listBtb = new List<CollectionDueReceivableAmount>();
                db = new xOssContext();

                var firstDayOfMonth = new DateTime(1990, date.Month, 1);
                var lastDayOfMonth = date;
                Acc_Type t = new Acc_Type();
                IEnumerable<Acc_Type> listOfAcc_Type = from tbl in t.GetAcc_Type()
                                                       select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

                VmAcc_IncomeStatement vmAcc_IncomeStatement = new VmAcc_IncomeStatement();
                vmAcc_IncomeStatement.FromDate = firstDayOfMonth;
                vmAcc_IncomeStatement.ToDate = lastDayOfMonth;

                var TotalIncomeFromJournal = vmAcc_IncomeStatement.TotalIncomeFromJournal(listOfAcc_Type);
                var TotalReceivableForMis = vmAcc_IncomeStatement.TotalReceivableForMis(listOfAcc_Type);
                //var TotalExpenseFromJournal = vmAcc_IncomeStatement.TotalExpenseFromJournal(listOfAcc_Type);
                //var TotalExpenseFromAcc_TransactionAndExpHistory = vmAcc_IncomeStatement.TotalExpenseFromAcc_TransactionAndExpHistory(listOfAcc_Type);


                var joinqueryIncome = TotalIncomeFromJournal.VmAcc_IncomeStatementReport.Sum(c => c.Balance);
                var receivable = TotalReceivableForMis.VmAcc_IncomeStatementReport.Sum(c => c.Balance);

                CollectionDueReceivableAmount collectionDueReceivableAmount = new CollectionDueReceivableAmount();
                collectionDueReceivableAmount.MonthName = date.ToString("MMM") + "-" + date.ToString("yy");

                collectionDueReceivableAmount.Income = TotalIncomeFromJournal.OpeingBalance + joinqueryIncome;
                collectionDueReceivableAmount.Receivable = TotalReceivableForMis.OpeingBalance + receivable;

                listBtb.Add(collectionDueReceivableAmount);

                this.CollectionDueReceivableAmount = listBtb;
            }
            catch (Exception e)
            {
                //
            }
        }
        public void BuyerChart()
        {



            db = new xOssContext();

            var buyerList = (from t3 in db.Mkt_Buyer
                             where t3.Active == true && t3.ID != 1
                             select new
                             {
                                 BuyerName = t3.BuyerID,
                                 Acc_AcNameFk = t3.Acc_AcNameFk,
                             }).OrderBy(x => x.BuyerName).ToList();
            List<BuyerInfoChart> buyerInfoChartList = new List<BuyerInfoChart>();

            foreach (var x in buyerList)
            {
                BuyerInfoChart buyerInfoChart = new BuyerInfoChart();
                buyerInfoChart.BuyerName = x.BuyerName;
                buyerInfoChart.Balance = GetBuyerBalance(x.Acc_AcNameFk);
                buyerInfoChartList.Add(buyerInfoChart);
            }
            this.BuyerInfoChartList = buyerInfoChartList;
        }

        private decimal GetBuyerBalance(int accAcNameFk)
        {
            db = new xOssContext();
            var totalDebit = (from t1 in db.Acc_JournalSlave
                              join t2 in db.Mkt_Buyer on t1.Acc_AcNameFK equals t2.Acc_AcNameFk
                              where t2.Acc_AcNameFk == accAcNameFk && t1.Active == true
                              select t1.Debit).ToList();// (t1.Debit == null ? 0 : 

            var totalCredit = (from t1 in db.Acc_JournalSlave
                               join t2 in db.Mkt_Buyer on t1.Acc_AcNameFK equals t2.Acc_AcNameFk
                               where t2.Acc_AcNameFk == accAcNameFk && t1.Active == true
                               select t1.Credit).ToList(); //(t1.Credit == null ? 0 : 

            decimal TotalBalance = 0;

            if (totalDebit != null && totalCredit != null)
            {
                TotalBalance = totalDebit.Sum() - totalCredit.Sum();
            }
            return TotalBalance;
        }



        public void BuyerChartDebtors()
        {



            db = new xOssContext();

            var buyerList = (from t3 in db.Mkt_Buyer
                             where t3.Active == true && t3.ID != 1
                             select new
                             {
                                 BuyerName = t3.BuyerID,
                                 Acc_AcNameFk = t3.Acc_AcNameFk,
                             }).OrderBy(x => x.BuyerName).ToList();
            List<BuyerInfoChart> buyerInfoChartList = new List<BuyerInfoChart>();

            foreach (var x in buyerList)
            {
                BuyerInfoChart buyerInfoChart = new BuyerInfoChart();
                buyerInfoChart.BuyerName = x.BuyerName;
                buyerInfoChart.Balance = GetBuyerBalanceDebtors(x.Acc_AcNameFk);
                buyerInfoChartList.Add(buyerInfoChart);
            }
            this.BuyerInfoChartList = buyerInfoChartList;
        }
        public void SupplierPayble()
        {
            db = new xOssContext();
            var supplierList = (from t1 in db.Common_Supplier
                                where t1.Active == true && t1.ID != 1
                                select new
                                {
                                    SupplierName = t1.CID,
                                    Acc_AcNameFk = t1.Acc_AcNameFk,
                                }).OrderBy(x => x.SupplierName).ToList();
            List<SupplierInfoChart> supplierInfoChartList = new List<SupplierInfoChart>();

            foreach (var x in supplierList)
            {
                SupplierInfoChart supplierInfoChart = new SupplierInfoChart();
                supplierInfoChart.SupplierName = x.SupplierName;
                supplierInfoChart.Balance = GetBuyerBalanceDebtors(x.Acc_AcNameFk);
                supplierInfoChartList.Add(supplierInfoChart);
            }
            this.SupplierInfoChartList = supplierInfoChartList;
        }
        private decimal GetBuyerBalanceDebtors(int accAcNameFk)
        {
            db = new xOssContext();
            var totalDebit = (from t1 in db.Acc_JournalSlave
                              join t2 in db.Common_Supplier on t1.Acc_AcNameFK equals t2.Acc_AcNameFk
                              where t2.Acc_AcNameFk == accAcNameFk && t1.Active == true
                              select t1.Debit).ToList();// (t1.Debit == null ? 0 : 

            var totalCredit = (from t1 in db.Acc_JournalSlave
                               join t2 in db.Common_Supplier on t1.Acc_AcNameFK equals t2.Acc_AcNameFk
                               where t2.Acc_AcNameFk == accAcNameFk && t1.Active == true
                               select t1.Credit).ToList(); //(t1.Credit == null ? 0 : 

            decimal TotalBalance = 0;

            if (totalDebit != null && totalCredit != null)
            {
                TotalBalance = totalCredit.Sum() - totalDebit.Sum();
            }
            return TotalBalance;
        }


        public void GetBusinessCashFlow(DateTime date)
        {
            db = new xOssContext();
            try
            {
                List<BusinessCashFlow> listBcf = new List<BusinessCashFlow>();
                db = new xOssContext();
                for (int i = 12; i >= 1; i--)
                {
                    var newdate = date.AddMonths(-i);
                    var firstDayOfMonth = new DateTime(newdate.Year, newdate.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    Acc_Type t = new Acc_Type();
                    IEnumerable<Acc_Type> listOfAccType = from tbl in t.GetAcc_Type()
                                                          select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
                    var ofAccType = listOfAccType as Acc_Type[] ?? listOfAccType.ToArray();
                    VmAcc_IncomeStatement vmAcc_IncomeStatement = new VmAcc_IncomeStatement();
                    vmAcc_IncomeStatement.FromDate = firstDayOfMonth;
                    vmAcc_IncomeStatement.ToDate = lastDayOfMonth;
                    VmMis_AdminBoard vmMis_AdminBoard = new VmMis_AdminBoard();
                    var TotalIncomeFromJournal = vmAcc_IncomeStatement.TotalIncomeFromJournal(ofAccType);
                    var TotalReceivableForMis = vmAcc_IncomeStatement.TotalReceivableForMis(ofAccType);
                    var TotalPayableForMis = vmAcc_IncomeStatement.TotalPayableForMis(ofAccType);


                    var TotalExpenseFromJournal = vmAcc_IncomeStatement.TotalExpenseFromJournal(ofAccType);
                    var TotalExpenseFromAcc_TransactionAndExpHistory = vmAcc_IncomeStatement.TotalExpenseFromAcc_TransactionAndExpHistory(ofAccType);
                    var Expense = TotalExpenseFromJournal.VmAcc_IncomeStatementReport.Sum(c => c.Balance);
                    var ExpenseTransactionAndExpHistory = TotalExpenseFromAcc_TransactionAndExpHistory.VmAcc_IncomeStatementReport.Sum(c => c.Balance);


                    var joinqueryIncome = TotalIncomeFromJournal.VmAcc_IncomeStatementReport.Sum(c => c.Balance);
                    var receivable = TotalReceivableForMis.VmAcc_IncomeStatementReport.Sum(c => c.Balance);
                    var Payable = TotalPayableForMis.VmAcc_IncomeStatementReport.Sum(c => c.Balance);
                    decimal totalInvoiceAmount = vmMis_AdminBoard.GetAllInvoiceAmount();

                    BusinessCashFlow businessCashFlow = new BusinessCashFlow();
                    businessCashFlow.MonthName = newdate.ToString("MMM") + "-" + newdate.ToString("yy");
                    businessCashFlow.InvoiceAmount = totalInvoiceAmount;
                    if (i == 12)
                    {
                        businessCashFlow.PredictedCashInFlow = TotalReceivableForMis.OpeingBalance + receivable;
                        businessCashFlow.ActualCashInFlow = TotalIncomeFromJournal.OpeingBalance + joinqueryIncome;

                        businessCashFlow.PedictedCashOutFlow = TotalPayableForMis.OpeingBalance + Payable;
                        businessCashFlow.ActualCashOutFlow = TotalExpenseFromJournal.OpeingBalance +
                                                             TotalExpenseFromAcc_TransactionAndExpHistory.OpeingBalance +
                                                             Expense + ExpenseTransactionAndExpHistory;
                    }
                    else
                    {

                        businessCashFlow.PredictedCashInFlow = receivable;
                        businessCashFlow.ActualCashInFlow = joinqueryIncome;

                        businessCashFlow.PedictedCashOutFlow = Payable;
                        businessCashFlow.ActualCashOutFlow = Expense + ExpenseTransactionAndExpHistory;
                    }
                    listBcf.Add(businessCashFlow);
                }

                this.BusinessCashFlow = listBcf;
            }
            catch (Exception e)
            {
                //
            }
        }

        private decimal GetAllInvoiceAmount()
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_InvoiceSlave
                     join t2 in db.Commercial_Invoice on t1.Commercial_InvoiceFk equals t2.ID
                     join t3 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t3.ID
                     join t4 in db.Mkt_BOM on t1.MktBOMFK equals t4.ID
                     join t5 in db.Shipment_OrderDeliverdSchedule on t3.ID equals t5.Shipment_CNFDelivaryChallanFk
                     where t1.Active == true && t2.Active == true && (t2.InvoiceDate >= FromDate && t2.InvoiceDate <= ToDate)
                     select t5.DeliverdQty * t4.UnitPrice).ToList();
            decimal deliverdQty = 0;
            if (v != null)
            {
                deliverdQty = v.Sum();
            }
            return deliverdQty;
        }
        private decimal GetAllOrderDeliverySchedule()
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     where t1.Active == true && (t1.Date >= FromDate && t1.Date <= ToDate)
                     select t1.Quantity).ToList();
            decimal deliverdQty = 0;
            if (v != null)
            {
                deliverdQty = v.Sum();
            }
            return deliverdQty;
        }

        private decimal GetAllCapacityQuantity()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Planning
                     join t2 in db.Mkt_OrderDeliverySchedule on t1.Mkt_BOMFK equals t2.Mkt_BOMFk
                     where t1.Active == true && (t2.Date >= FromDate && t2.Date <= ToDate)
                     select t1.Capacity).ToList();
            decimal totalCapacity = 0;
            if (v != null)
            {
                totalCapacity = v.Sum();
            }
            return totalCapacity;
        }
        private decimal? GetAllSewingQuantity()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProduction on t2.Prod_PlanReferenceProductionFk equals t3.ID
                     join t4 in db.Prod_PlanReference on t3.Prod_PlanReferenceFK equals t4.ID
                     where t1.Active == true && t3.Plan_OrderProcessFK == 32 && (t1.Date >= FromDate && t1.Date <= ToDate)
                     select t1.Quantity).ToList();
            decimal? totalFollowup = 0;
            if (v != null)
            {
                totalFollowup = v.Sum();
            }
            return totalFollowup;
        }

        private decimal? GetAllCuttingQuantity()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProduction on t2.Prod_PlanReferenceProductionFk equals t3.ID
                     join t4 in db.Prod_PlanReference on t3.Prod_PlanReferenceFK equals t4.ID
                     where t1.Active == true && t3.Plan_OrderProcessFK == 30 && (t1.Date >= FromDate && t1.Date <= ToDate)
                     select t1.Quantity).ToList();
            decimal? totalFollowup = 0;
            if (v != null)
            {
                totalFollowup = v.Sum();
            }
            return totalFollowup;
        }

        private decimal? GetAllIronQuantity()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProduction on t2.Prod_PlanReferenceProductionFk equals t3.ID
                     join t4 in db.Prod_PlanReference on t3.Prod_PlanReferenceFK equals t4.ID
                     where t1.Active == true && t3.Plan_OrderProcessFK == 33 && (t1.Date >= FromDate && t1.Date <= ToDate)
                     select t1.Quantity).ToList();
            decimal? totalFollowup = 0;
            if (v != null)
            {
                totalFollowup = v.Sum();
            }
            return totalFollowup;
        }

        private decimal? GetAllPackingQuantity()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProduction on t2.Prod_PlanReferenceProductionFk equals t3.ID
                     join t4 in db.Prod_PlanReference on t3.Prod_PlanReferenceFK equals t4.ID
                     where t1.Active == true && t3.Plan_OrderProcessFK == 34 && (t1.Date >= FromDate && t1.Date <= ToDate)
                     select t1.Quantity).ToList();
            decimal? totalFollowup = 0;
            if (v != null)
            {
                totalFollowup = v.Sum();
            }
            return totalFollowup;
        }
        public void GetProductionStatus(DateTime date)
        {
            db = new xOssContext();
            try
            {
                List<ProductionStatus> listPs = new List<ProductionStatus>();
                db = new xOssContext();
                for (int i = 12; i >= 1; i--)
                {
                    var newdate = date.AddMonths(-i);
                    var firstDayOfMonth = new DateTime(newdate.Year, newdate.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    Acc_Type t = new Acc_Type();
                    IEnumerable<Acc_Type> listOfAcc_Type = from tbl in t.GetAcc_Type()
                                                           select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

                    VmMis_AdminBoard vmMis_AdminBoard = new VmMis_AdminBoard();
                    vmMis_AdminBoard.FromDate = firstDayOfMonth;
                    vmMis_AdminBoard.ToDate = lastDayOfMonth;


                    decimal? totalSewingQuantity = vmMis_AdminBoard.GetAllSewingQuantity();
                    decimal? totalCuttinQuantity = vmMis_AdminBoard.GetAllCuttingQuantity();
                    decimal? totalIronQuantity = vmMis_AdminBoard.GetAllIronQuantity();
                    decimal? totalPackingQuantity = vmMis_AdminBoard.GetAllPackingQuantity();
                    //var TotalReceivableForMis = vmAcc_IncomeStatement.TotalReceivableForMis(listOfAcc_Type);
                    //var TotalExpenseFromJournal = vmAcc_IncomeStatement.TotalExpenseFromJournal(listOfAcc_Type);
                    //var TotalExpenseFromAcc_TransactionAndExpHistory = vmAcc_IncomeStatement.TotalExpenseFromAcc_TransactionAndExpHistory(listOfAcc_Type);


                    //var joinqueryIncome = TotalIncomeFromJournal.Sum(c => c.Balance);
                    // var receivable = TotalReceivableForMis.VmAcc_IncomeStatementReport.Sum(c => c.Balance);

                    ProductionStatus productionStatus = new ProductionStatus();
                    productionStatus.MonthName = newdate.ToString("MMM") + "-" + newdate.ToString("yy");

                    //if (i == 12)
                    //{
                    //    businessCashFlow.PredictedCashInFlow = TotalIncomeFromJournal.OpeingBalance + joinqueryIncome;
                    //   // collectionDueReceivableAmount.Receivable = TotalReceivableForMis.OpeingBalance + receivable;
                    //}
                    //else
                    //{
                    productionStatus.Cutting = totalCuttinQuantity;
                    productionStatus.Sewing = totalSewingQuantity;
                    productionStatus.Iron = totalIronQuantity;
                    productionStatus.Packing = totalPackingQuantity;
                    // }
                    listPs.Add(productionStatus);
                }
                this.ProductionStatus = listPs;
            }
            catch (Exception e)
            {
                //
            }
        }

        public void GetFactoryOverview(DateTime date)
        {
            db = new xOssContext();
            try
            {
                List<FactoryOverview> listFov = new List<FactoryOverview>();
                db = new xOssContext();
                for (int i = 12; i >= 1; i--)
                {
                    var newdate = date.AddMonths(-i);
                    var firstDayOfMonth = new DateTime(newdate.Year, newdate.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    Acc_Type t = new Acc_Type();
                    IEnumerable<Acc_Type> listOfAcc_Type = from tbl in t.GetAcc_Type()
                                                           select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

                    VmMis_AdminBoard vmMis_AdminBoard = new VmMis_AdminBoard();
                    vmMis_AdminBoard.FromDate = firstDayOfMonth;
                    vmMis_AdminBoard.ToDate = lastDayOfMonth;

                    decimal totalOrderDeliverySchedule = vmMis_AdminBoard.GetAllOrderDeliverySchedule();
                    decimal? totalSewingQuantity = vmMis_AdminBoard.GetAllSewingQuantity();
                    var totalCapacityQuantity = vmMis_AdminBoard.GetAllCapacityQuantity();
                    //var TotalExpenseFromJournal = vmAcc_IncomeStatement.TotalExpenseFromJournal(listOfAcc_Type);
                    //var TotalExpenseFromAcc_TransactionAndExpHistory = vmAcc_IncomeStatement.TotalExpenseFromAcc_TransactionAndExpHistory(listOfAcc_Type);


                    //var joinqueryIncome = TotalIncomeFromJournal.Sum(c => c.Balance);
                    // var receivable = TotalReceivableForMis.VmAcc_IncomeStatementReport.Sum(c => c.Balance);

                    FactoryOverview factoryOverview = new FactoryOverview();
                    factoryOverview.MonthName = newdate.ToString("MMM") + "-" + newdate.ToString("yy");

                    //if (i == 12)
                    //{
                    //    businessCashFlow.PredictedCashInFlow = TotalIncomeFromJournal.OpeingBalance + joinqueryIncome;
                    //   // collectionDueReceivableAmount.Receivable = TotalReceivableForMis.OpeingBalance + receivable;
                    //}
                    //else
                    //{
                    factoryOverview.Planed = totalOrderDeliverySchedule;
                    factoryOverview.Actual = totalSewingQuantity;
                    factoryOverview.Capacity = totalCapacityQuantity;
                    // }
                    listFov.Add(factoryOverview);
                }
                this.FactoryOverview = listFov;
            }
            catch (Exception e)
            {
                //
            }

        }


        internal void GetPlan_OrderStep(int id)
        {
            db = new xOssContext();
            var a = (from Plan_OrderStep in db.Plan_OrderStep
                     join Plan_OrderProcess in db.Plan_OrderProcess
                     on Plan_OrderStep.Plan_OrderProcessFK equals Plan_OrderProcess.ID
                     join plan_OrderProcessCategory in db.Plan_OrderProcessCategory
                     on Plan_OrderProcess.Plan_OrderProcessCategoryFk equals plan_OrderProcessCategory.ID
                     join t1 in db.Common_TheOrder
                     on Plan_OrderStep.CommonTheOrderFK equals t1.ID
                     where Plan_OrderStep.CommonTheOrderFK == id
                     select new VmMis_AdminBoard
                     {
                         Plan_OrderStep = Plan_OrderStep,
                         Plan_OrderProcess = Plan_OrderProcess,
                         //Mkt_BOM = Mkt_BOM,
                         Plan_OrderProcessCategory = plan_OrderProcessCategory,
                         Common_TheOrder = t1
                     }).Where(x => x.Plan_OrderStep.Active == true).OrderByDescending(x => x.Plan_OrderStep.ID).AsEnumerable();

            this.DataList = a;

        }
        internal void GetAllOrder(int id)
        {
            db = new xOssContext();
            var list = new List<VmMis_AdminBoard>();
            if (id != 0)
            {
                list = (from t1 in db.Mkt_BOM
                     join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID

                     where t1.Active == true && t1.ID != 1
                     && t2.Mkt_BuyerFK == id
                     select new VmMis_AdminBoard
                     {
                         Mkt_BOM = t1,
                         Common_TheOrder = t2
                     }).ToList();
            }
            else
            {
                list = (from t1 in db.Mkt_BOM
                     join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                     where t1.Active == true && t1.ID != 1

                     select new VmMis_AdminBoard
                     {
                         Mkt_BOM = t1,
                         Common_TheOrder = t2
                     }).ToList();
            }

            this.MisAdminBoardList = list;

        }
        public void GetPlan_OrderStepProgres()
        {
            db = new xOssContext();
            var a = this.MisAdminBoardList.OrderBy(t => t.Mkt_BOM.ID);
            //List<VmMis_AdminBoard> list =  new List<VmMis_AdminBoard>();




            DataTable dt = new DataTable("");

            if (a.Count() != 0)
            {
                var orderProcess = (from t1 in db.Plan_OrderProcess
                                    where t1.Active == true && t1.Plan_OrderProcessCategoryFk == 6
                                    select new VmMis_AdminBoard
                                    {
                                        Plan_OrderProcess = t1
                                    }).OrderBy(x => x.Plan_OrderProcess.ID).ToList();


                //foreach (VmMis_AdminBoard xx in a)
                //{
                //    xx.TotalOrderQuantity = xx.Mkt_BOM.QPack * xx.Mkt_BOM.Quantity;
                //    xx.TotalProductionQuantity = GetAllProductionQuantity(xx.Mkt_BOM.ID);
                //    list.Add(xx);
                //}


                dt.Columns.Add("Style No", typeof(string));
                foreach (var x in orderProcess)
                {
                    dt.Columns.Add(x.Plan_OrderProcess.StepName, typeof(string));
                }

                foreach (VmMis_AdminBoard o in a)
                {
                    DataRow theRow = dt.NewRow();
                    theRow[0] = o.Common_TheOrder.ID + "₴" + o.Common_TheOrder.BuyerPO;
                   
                    int count = 0, rowCount = 0;
                    bool running = false;

                    //if (dateTime2 > dateTime1)
                    //{
                    foreach (DataColumn cl in dt.Columns)
                    {
                        string column = cl.ColumnName;
                        if (column != "Style No")
                        {
                            decimal productionQuantity = Convert.ToDecimal(GetAllProductionQuantity(o.Mkt_BOM.ID, column));
                            decimal orderQuantity = Convert.ToDecimal(o.Mkt_BOM.Quantity * o.Mkt_BOM.QPack);
                            decimal PendingQuantity = Convert.ToDecimal((orderQuantity - productionQuantity));

                            decimal percent = PendingQuantity / orderQuantity * 100;

                            if (count > 0)
                            {
                                if (productionQuantity >= orderQuantity)
                                {

                                    theRow[count] = "# " + PendingQuantity + " (" + Math.Round(percent) + "%)";
                                    // "#-Order: " + orderQuantity + " Done: " + productionQuantity;// cl.ColumnName;
                                }
                                else if ((productionQuantity >= (orderQuantity / 100) * 90) && (productionQuantity < (orderQuantity / 100) * 100))
                                {

                                    theRow[count] = "$ " + PendingQuantity + " (" + Math.Round(percent) + "%)";// "$-Order: " + orderQuantity + " Done: " + productionQuantity;

                                }
                                else if (productionQuantity < (orderQuantity / 100) * 90)
                                {

                                    theRow[count] = "* " + PendingQuantity + " (" + Math.Round(percent) + "%)";// "*-Order: " + orderQuantity + " Done: " + productionQuantity;// cl.ColumnName;
                                }
                            }
                        }

                        count++;
                    }

                    dt.Rows.InsertAt(theRow, rowCount++);
                }

                this.DataTable = dt;
            }
        }

        private int? GetAllProductionQuantity(int id, string stepName)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_PlanReferenceProductionSectionFollowup
                     join t2 in db.Prod_PlanReferenceProductionSection on t1.Prod_PlanReferenceProductionSectionFk equals t2.ID
                     join t3 in db.Prod_PlanReferenceProduction on t2.Prod_PlanReferenceProductionFk equals t3.ID
                     join t4 in db.Prod_PlanReference on t3.Prod_PlanReferenceFK equals t4.ID
                     join t5 in db.Plan_OrderProcess on t3.Plan_OrderProcessFK equals t5.ID
                     where t1.Active == true && t5.StepName == stepName && t4.Mkt_BOMFK == id
                     select t1.Quantity).ToList();
            int? totalFollowup = 0;
            if (v != null)
            {
                totalFollowup = v.Sum();
            }
            return totalFollowup;
        }


    }

    public class CollectionDueReceivableAmount
    {
        public string MonthName { get; set; }
        public decimal Income { get; set; }
        public decimal Receivable { get; set; }
    }
    public class BuyerInfoChart
    {
        public string BuyerName { get; set; }
        public decimal Balance { get; set; }
    }
    public class SupplierInfoChart
    {
        public string SupplierName { get; set; }
        public decimal Balance { get; set; }
    }

    public class BusinessCashFlow
    {
        public string MonthName { get; set; }
        public decimal PredictedCashInFlow { get; set; }
        public decimal ActualCashInFlow { get; set; }
        public decimal PedictedCashOutFlow { get; set; }
        public decimal ActualCashOutFlow { get; set; }
        public decimal InvoiceAmount { get; set; }
    }
    public class FactoryOverview
    {
        public string MonthName { get; set; }
        public decimal Planed { get; set; }
        public decimal? Actual { get; set; }

        public decimal? Capacity { get; set; }


    }
    public class ProductionStatus
    {
        public string MonthName { get; set; }
        public decimal? Cutting { get; set; }
        public decimal? Sewing { get; set; }
        public decimal? Iron { get; set; }
        public decimal? Packing { get; set; }

    }

}