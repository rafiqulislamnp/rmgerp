﻿using Oss.Romo.Models;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.User
{

    public class MenuDetailsViewModel
    {
        public Int64 Id { get; set; }
        public string ModuleName { get; set; }
        public Int64 ModuleId { get; set; }
        public string MenuType { get; set; }
        public string MenuName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public bool IsMenu { get; set; }
        public int MenuSl { get; set; }
        private xOssContext db = new xOssContext();

        //public List<MenuDetailsViewModel> GetAllUserMenuDetails()
        //{
        //    //var menu = (from t1 in db.User_MenuItem
        //    //            join t2 in db.User_SubMenu on t1.User_SubMenuFk equals t2.ID
        //    //            join t3 in db.User_Menu on t2.User_MenuFk equals t3.ID
        //    //            join t4 in db.User_RoleMenuItem on t1.ID equals t4.User_MenuItemFk
        //    //            select new MenuDetailsViewModel
        //    //            {
        //    //                //Id = t1.Id,
        //    //                //ModuleName = t2.ModuleName,
        //    //                //ModuleId = t2.Id,
        //    //                //MenuType = t1.MenuType,
        //    //                //MenuName = t1.MenuName,
        //    //                //ControllerName = t1.ControllerName,
        //    //                //ActionName = t1.ActionName,
        //    //                //IsMenu = t1.IsMenu,
        //    //                //MenuSl = t1.MenuSl,
        //    //            }).OrderByDescending(x => x.Id).ToList();
        //    return menu;
        //}
    }
    public class VmRoleDetails
    {
        
        public int MenuID { get; set; }
        public int RoleID { get; set; }
        public int ID { get; set; }
        public int UserSubMenuID { get; set; }
        public string Top { get; set; }
        public int TopId { get; set; }
        public string Menu { get; set; }
        public string Description { get; set; }
        public string Item { get; set; }
        public string Method { get; set; }
        public bool IsSelected { get; set; }
        public int? Priority { get; set; }
        
    }
    public class VmUser_Role
    {

        private xOssContext db;
        public IEnumerable<VmUser_Role> DataList { get; set; }
        public List<VmRoleDetails> DataListVmRoleDetails { get; set; }
        public User_Role User_Role { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmUser_Role()
        {
            db = new xOssContext();
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            var v = (from t1 in db.User_Role
                     select new VmUser_Role
                     {
                         User_Role = t1
                     }).Where(x => x.User_Role.Active == true).OrderByDescending(x => x.User_Role.ID).AsEnumerable();
            this.DataList = v;
        }
        public List<VmUser_Role> GetUserRoles()
        {
            VmUser_Role vmUserRole = new VmUser_Role();
            var v = (from t1 in db.User_Role
                     select new VmUser_Role
                     {
                         User_Role = t1
                     }).Where(x => x.User_Role.Active == true).OrderByDescending(x => x.User_Role.ID).ToList();

            return v;
          
        }



        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from ur in db.User_Role
                     select new VmUser_Role
                     {
                         User_Role = ur
                     }).Where(c => c.User_Role.ID == id).SingleOrDefault();
            this.User_Role = v.User_Role;

        }

        public int Add(int userID)
        {
            User_Role.AddReady(userID);
            return User_Role.Add();
        }

        public bool Edit(int userID)
        {
            return User_Role.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return User_Role.Delete(userID);
        }
        public VmUser_Role SelectSingleRole(string id)
        {
            int iD = 0;
            id = this.VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();
            var v = (from user_Role in db.User_Role
                     select new VmUser_Role
                     {
                         User_Role = user_Role

                     }).Where(c => c.User_Role.ID == iD).FirstOrDefault();
            return v;
        }
       
        public void GetRoleDetailsById(string id)

        {
            int iD = 0;
            id = this.VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();
            var v = (
                 from t0 in db.User_Menu
                 join t1 in db.User_SubMenu
                 on t0.ID equals t1.User_MenuFk
                 join t2 in db.User_MenuItem
                 on t1.ID equals t2.User_SubMenuFk
                 join t3 in db.User_RoleMenuItem
                 on t2.ID equals t3.User_MenuItemFk

                 where t3.User_RoleFK == iD
                 select new VmRoleDetails
                 {
                     MenuID = t3.User_MenuItemFk,
                     RoleID = iD,
                     TopId = t0.ID,
                     Top = t0.Name,
                     Menu = t1.Name,
                     Description = t2.Description,
                     Item = t2.Name,
                     IsSelected = t3.IsAllowed,
                     Priority = t2.Priority,
                     ID = t3.ID,
                     UserSubMenuID = t1.ID,
                 }).OrderBy(x => x.Priority).OrderBy(x=>x.UserSubMenuID).ToList();

            DataListVmRoleDetails = v;



            if (DataListVmRoleDetails.Count == 0)
            {
                CreateNewRole(id);
            }
        }

        private void CreateNewRole(string id)
        {
            int rId = 0;
            Int32.TryParse(id, out rId);
            db = new xOssContext();
            object[] parameter = { new SqlParameter("@id", rId) };
            db.Database.ExecuteSqlCommand("EXEC dbo.SP_CreateNewRole @id", parameter);
            string s = rId.ToString();
            s = this.VmControllerHelper.Encrypt(s);
            GetRoleDetailsById(s);
        }

        internal bool DefineRoleMenu(int userID)
        {
            foreach (var v in DataListVmRoleDetails)
            {
                User_RoleMenuItem x = new User_RoleMenuItem();
                x.ID = v.ID;
                x.User_RoleFK = v.RoleID;
                x.User_MenuItemFk = v.MenuID;
                x.IsAllowed = v.IsSelected;
                x.Edit(userID);
            }
            return true;
        }


    }
}