﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using Oss.Romo.Models;

namespace Oss.Romo.ViewModels.MIS
{
    public class VmMis_OrderStatus
    {
        public int ID { get; set; }
        
        public string CID { get; set; }
        public string Style { get; set; }
        public decimal OrderValues { get; set; }
        public decimal BOMValues { get; set; }
        public decimal QuantityPC { get; set; }
        public decimal CM { get; set; }
        public decimal Cost { get; set; }
        public decimal BEP { get; set; }
        public decimal SMV { get; set; }
        public decimal Exfactory { get; set; } 
        public decimal FinishQuantity { get; set; }


        public IEnumerable<VmMis_OrderStatus> OrderStatusDataList { get; set; }

        private xOssContext db = new xOssContext();


        public void GetOrderStatus()
        {
            var orderStatus = (from t1 in db.Mis_OrderStatus
                               
                select new VmMis_OrderStatus
                {
                    CID = t1.CID,
                    Style = t1.Style,
                    OrderValues = t1.OrderValues,
                    QuantityPC = t1.QuantityPC,
                    CM = t1.CM,
                    Cost = t1.Cost,
                    BEP = t1.BEP,
                    SMV = t1.SMV
                }).ToList();
            OrderStatusDataList = orderStatus;

        }
    }
}