﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models;

namespace Oss.Romo.ViewModels.MIS
{
    public class Vm_BuyerReceivable
    {
        [DisplayName("ID")]
        public int ID { get; set; }
        [DisplayName("Buyer ID")]
        [DefaultValue(0)]
        public int BuyerId { get; set; }

        [DisplayName("Buyer ID")]
        public string BuyerShortName { get; set; }

        [DisplayName("Buyer Name")]
        public string BuyerName { get; set; }


        [DefaultValue(0)]
        [DisplayName("Receivable")]
        public decimal Receivable { get; set; }
        public DateTime EntryDate { get; set; }

        public IEnumerable<Vm_BuyerReceivable> BuyerReceivableDataList { get; set; }

        private xOssContext db = new xOssContext();


        public void GetBuyerReceivableList()
        {
            var receivableInfo = (from t1 in db.Mis_BuyerReceivable
                join t2 in db.Mkt_Buyer on t1.BuyerId equals t2.ID
                select new Vm_BuyerReceivable()
                {
                    BuyerId = t1.BuyerId,
                    BuyerShortName = t2.BuyerID,
                    BuyerName = t2.Name,
                    Receivable = t1.Receivable
                }).ToList();
            BuyerReceivableDataList = receivableInfo;

        }
    }
}