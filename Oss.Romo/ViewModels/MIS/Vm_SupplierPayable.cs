﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using Oss.Romo.Models;

namespace Oss.Romo.ViewModels.MIS
{
    public class Vm_SupplierPayable
    {
        [DisplayName("ID")]
        public int ID { get; set; }
        [DisplayName("Supplier ID")]
        [DefaultValue(0)]
        public int SupplierId { get; set; }

        [DisplayName("Supplier Name")]
        public string SupplierName { get; set; }

        [DisplayName("CID")]
        public string CID { get; set; }

        [DefaultValue(0)]
        [DisplayName("Payable")]
        public decimal Payable { get; set; }
        public DateTime EntryDate { get; set; }


        public IEnumerable<Vm_SupplierPayable> SupplierPayableDataList { get; set; }

        private xOssContext db = new xOssContext();


        public void GetSupplierPayableList()
        {
            var payableInfo = (from t1 in db.Mis_SupplierPayable
                               join t2 in db.Common_Supplier on t1.SupplierId equals t2.ID
                select new Vm_SupplierPayable()
                {
                    SupplierId = t1.SupplierId,
                    SupplierName = t2.Name,
                    CID = t2.CID,
                    Payable = t1.Payable
                }).ToList();
            SupplierPayableDataList = payableInfo;

        }
    }
}