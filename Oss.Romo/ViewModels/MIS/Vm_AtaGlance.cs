﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models;

namespace Oss.Romo.ViewModels.MIS
{
    public class Vm_AtaGlance
    {
        [DisplayName("ID")]
        public int ID { get; set; }
        [DisplayName("SL")]
        [DefaultValue(0)]
        public int SL { get; set; }
        [MaxLength(80)]
        [DisplayName("Title")]
        public string Title { get; set; }
        [DisplayName("Values")]
        [DefaultValue(0)]
        public decimal Values { get; set; }
        public DateTime EntryDate { get; set; }

        public IEnumerable<Vm_AtaGlance> AtaGlanceDataList { get; set; }
        
        private xOssContext db = new xOssContext();


        public void GetAtaGlanceList()
        {
            var ataglanceInfo = (from t1 in db.Mis_AtaGlance
                                 select new Vm_AtaGlance()
                                 {
                                     SL = t1.SL,
                                     Title = t1.Title,
                                     Values = t1.Values
                                 }).ToList();
            AtaGlanceDataList = ataglanceInfo;

        }
        //public void GetAtaGlanceErq()
        //{
        //    var ataglanceInfo2 = (from t1 in db.Mis_AtaGlance
        //                          select new Vm_AtaGlance()
        //                          {
        //                              SL = t1.SL,
        //                              Title = t1.Title,
        //                              Values = t1.Values
        //                          }).Where(x => x.SL >= 17 && x.SL <= 20).ToList();
        //    AtaGlanceDataList2 = ataglanceInfo2;
        //}
        //public void GetAtaGlanceFdr()
        //{
        //    var ataglanceInfo2 = (from t1 in db.Mis_AtaGlance
        //                          select new Vm_AtaGlance()
        //                          {
        //                              SL = t1.SL,
        //                              Title = t1.Title,
        //                              Values = t1.Values
        //                          }).Where(x => x.SL >= 21 && x.SL <= 22).ToList();
        //    AtaGlanceDataList2 = ataglanceInfo2;
        //}
        //public void GetAtaGlancePc()
        //{
        //    var ataglanceInfo2 = (from t1 in db.Mis_AtaGlance
        //                          select new Vm_AtaGlance()
        //                          {
        //                              SL = t1.SL,
        //                              Title = t1.Title,
        //                              Values = t1.Values
        //                          }).Where(x => x.SL >= 23 && x.SL <= 24).ToList();
        //    AtaGlanceDataList2 = ataglanceInfo2;
        //}
        //public void GetAtaGlanceOd()
        //{
        //    var ataglanceInfo2 = (from t1 in db.Mis_AtaGlance
        //                          select new Vm_AtaGlance()
        //                          {
        //                              SL = t1.SL,
        //                              Title = t1.Title,
        //                              Values = t1.Values
        //                          }).Where(x => x.SL == 25).ToList();
        //    AtaGlanceDataList2 = ataglanceInfo2;
        //}
        //public void GetAtaGlanceTl()
        //{
        //    var ataglanceInfo2 = (from t1 in db.Mis_AtaGlance
        //                          select new Vm_AtaGlance()
        //                          {
        //                              SL = t1.SL,
        //                              Title = t1.Title,
        //                              Values = t1.Values
        //                          }).Where(x => x.SL >= 26 && x.SL <= 27).ToList();
        //    AtaGlanceDataList2 = ataglanceInfo2;
        //}

    }
}