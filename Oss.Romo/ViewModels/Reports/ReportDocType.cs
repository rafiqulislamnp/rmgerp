﻿using Oss.Romo.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Reports
{
    public class ReportDocType
    {
       //HeaderTop---------------------------------
        public string HeaderTop1 { get { return ""; } }
        public string HeaderTop2 { get; set; }
        public string HeaderTop3 { get; set; }
        public string HeaderTop4 { get { return "hello"; } }
        public string HeaderTop5 { get; set; }
        public string HeaderTop6 { get; set; }
        public string HeaderTop7 { get; set; }
        public string HeaderTop8 { get; set; }

        //HeaderLeft---------------------------------
        public string HeaderLeft1 { get; set; }
        public string HeaderLeft2 { get; set; }
        public string HeaderLeft3 { get; set; }
        public string HeaderLeft4 { get; set; }
        public string HeaderLeft5 { get; set; }
        public string HeaderLeft6 { get; set; }
        public string HeaderLeft7 { get; set; }
        public string HeaderLeft8 { get; set; }

        //HeaderMiddle----------------
        public string HeaderMiddle1 { get; set; }
        public string HeaderMiddle2 { get; set; }
        public string HeaderMiddle3 { get; set; }
        public string HeaderMiddle4 { get; set; }
        public string HeaderMiddle5 { get; set; }
        public string HeaderMiddle6 { get; set; }
        public string HeaderMiddle7 { get; set; }
        public string HeaderMiddle8 { get; set; }


        //Header Right ------------------------
        public string HeaderRight1 { get; set; }
        public string HeaderRight2 { get; set; }
        public string HeaderRight3 { get; set; }
        public string HeaderRight4 { get; set; }
        public string HeaderRight5 { get; set; }
        public string HeaderRight6 { get; set; }
        public string HeaderRight7 { get; set; }
        public string HeaderRight8 { get; set; }


        //Body----------------------------------------
        public string Body1 { get; set; }
        public string Body2 { get; set; }
        public string Body3 { get; set; }
        public string Body4 { get; set; }
        public string Body5 { get; set; }
        public string Body6 { get; set; }
        public string Body7 { get; set; }
        public string Body8 { get; set; }
        public string Body9 { get; set; }
        public string Body10 { get; set; }
        public string Body11 { get; set; }
        public string Body12 { get; set; }
        public string Body13 { get; set; }
        public string Body14 { get; set; }
        public string Body15 { get; set; }
        public string Body16 { get; set; }
        public string Body17 { get; set; }
        public string Body18 { get; set; }
        public string Body19 { get; set; }
        public string Body20 { get; set; }
        public string Body21 { get; set; }
        public string Body22 { get; set; }
        public string Body23 { get; set; }
        public string Body24 { get; set; }

        //public decimal? Body16 { get; set; }
        //public decimal? Body17 { get; set; }

        //SubBody ---------------------------
        public string SubBody1 { get; set; }
        public string SubBody2 { get; set; }
        public string SubBody3 { get; set; }
        public string SubBody4 { get; set; }
        public string SubBody5 { get; set; }
        public string SubBody6 { get; set; }
        public string SubBody7 { get; set; }
        public string SubBody8 { get; set; }
        public string SubBody9 { get; set; }
        public string SubBody10 { get; set; }
        public string SubBody11 { get; set; }
        public string SubBody12 { get; set; }
        public string SubBody13 { get; set; }
        public string SubBody14 { get; set; }
        public string SubBody15 { get; set; }
        public string SubBody16 { get; set; }
        public string SubBody17 { get; set; }
        public string SubBody18 { get; set; }
        public string SubBody19 { get; set; }
        public string SubBody20 { get; set; }


        //Footer ---------------------------------
        public string Footer1 { get; set; }
        public string Footer2 { get; set; }
        public string Footer3 { get; set; }
        public string Footer4 { get; set; }
        public string Footer5 { get; set; }
        public string Footer6 { get; set; }
        public string Footer7 { get; set; }
        public string Footer8 { get; set; }
        //Hidden -------------------------------------
        public int int1 { get; set; }
        public int int2 { get; set; }
        public int int3 { get; set; }



        public decimal Q1 { get; set; }
        public decimal Q2 { get; set; }
        public decimal Q3 { get; set; }
        public decimal Q4 { get; set; }
        public decimal Q5 { get; set; }
        public decimal Q6 { get; set; }
        public decimal Q7 { get; set; }
        public decimal Q8 { get; set; }
        public decimal Q9 { get; set; }
        public decimal Q10 { get; set; }


        public User_User CustomObject { get; set; }
        public List<ReportDocType> ReportDocTypeList { get; set; }

    }
}