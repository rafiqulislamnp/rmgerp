﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Common;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.ViewModels.User;

namespace Oss.Romo.ViewModels.MisDashboard
{
    public class VmNotifier
    {
        private xOssContext db;
        public int TotalNotifyCounter { get; set; }
        public int NotifyCounter { get; set; }
        public string NotifyName { get; set; }
        public string NotifyLink { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
        public Acc_Notifier Acc_Notifier { get; set; }
        public VmCommercial_UD VmCommercial_UD { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public IEnumerable<VmNotifier> DataList { get; set; }
        public IEnumerable<VmNotifier> DataListLcOpen { get; set; }
        public IEnumerable<VmNotifier> DataListDocPayment { get; set; }
        public IEnumerable<PartyPaymentLedgerList> Common_SupplierList { get; set; }
        public IEnumerable<PartyPaymentLedgerList> Common_SupplierListPaymnet { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            // Cheque
            var a = (from Acc_Notifier in db.Acc_Notifier
                     where Acc_Notifier.Active == true && Acc_Notifier.IsSeen == false && (Acc_Notifier.Type == 0 || Acc_Notifier.Type == 3)
                     select new VmNotifier
                     {
                         Acc_Notifier = Acc_Notifier
                     }).OrderByDescending(x => x.Acc_Notifier.Date).AsEnumerable();
            this.DataList = a;
            //L/C Open
            var b = (from Acc_Notifier in db.Acc_Notifier
                     where Acc_Notifier.Active == true && Acc_Notifier.IsSeen == false && Acc_Notifier.Type == 1
                     select new VmNotifier
                     {
                         Acc_Notifier = Acc_Notifier
                     }).OrderByDescending(x => x.Acc_Notifier.Date).AsEnumerable();
            this.DataListLcOpen = b;
            //L/C Payment
            var c = (from Acc_Notifier in db.Acc_Notifier
                     where Acc_Notifier.Active == true && Acc_Notifier.IsSeen == false && Acc_Notifier.Type == 2
                     select new VmNotifier
                     {
                         Acc_Notifier = Acc_Notifier
                     }).OrderByDescending(x => x.Acc_Notifier.Date).AsEnumerable();
            this.DataListDocPayment = c;

            //var supplierlist = db.Common_Supplier.Where(x => x.Active == true).ToList();

            //List<PartyPaymentLedgerList> supplitsOpening = new List<PartyPaymentLedgerList>();
            //List<PartyPaymentLedgerList> supplistPayment = new List<PartyPaymentLedgerList>();

            //foreach (var commonSupplier in supplierlist)
            //{
            //    PartyPaymentLedgerList partyOpeningLedgerList =
            //        new PartyPaymentLedgerList
            //        {
            //            Common_Supplier = commonSupplier,
            //            Common_SupplierOpeningBalance = PartyOpeningLedger(commonSupplier)
            //        };
            //    supplitsOpening.Add(partyOpeningLedgerList);

            //    PartyPaymentLedgerList partyPaymentLedgerList =
            //        new PartyPaymentLedgerList
            //        {
            //            Common_Supplier = commonSupplier,
            //            Common_SupplierOpeningBalance = PartyPaymentLedger(commonSupplier)
            //        };
            //    supplistPayment.Add(partyPaymentLedgerList);
            //}

            //this.Common_SupplierList = supplitsOpening;
            //this.Common_SupplierListPaymnet = supplistPayment;
        }
        public void MirrorDataLoad()
        {
            db = new xOssContext();
            var a = (from Acc_Notifier in db.Acc_Notifier
                     where Acc_Notifier.Active == true && (Acc_Notifier.Type == 0 || Acc_Notifier.Type == 3)
                     select new VmNotifier
                     {
                         Acc_Notifier = Acc_Notifier
                     }).OrderByDescending(x => x.Acc_Notifier.Date).AsEnumerable();
            this.DataList = a;

            var b = (from Acc_Notifier in db.Acc_Notifier
                     where Acc_Notifier.Active == true && Acc_Notifier.Type == 1
                     select new VmNotifier
                     {
                         Acc_Notifier = Acc_Notifier
                     }).OrderByDescending(x => x.Acc_Notifier.Date).AsEnumerable();
            this.DataListLcOpen = b;

            var c = (from Acc_Notifier in db.Acc_Notifier
                     where Acc_Notifier.Active == true && Acc_Notifier.Type == 2
                     select new VmNotifier
                     {
                         Acc_Notifier = Acc_Notifier
                     }).OrderByDescending(x => x.Acc_Notifier.Date).AsEnumerable();
            this.DataListDocPayment = c;

        }
        public void NotifyCount()
        {
            List<VmNotifier> vmNotifiersList = new List<VmNotifier>();
            VmNotifier vmNotifier = new VmNotifier();

            db = new xOssContext();
            var a = db.Acc_Notifier.Count(x => x.Active == true && x.IsSeen == false && (x.Type == 0 || x.Type == 3));

            if (a > 0)
            {
                vmNotifier = new VmNotifier();
                vmNotifier.NotifyCounter = a;
                vmNotifier.NotifyLink = "cheque";

                //vmNotifier.NotifyLink = "~/../Notifier?type=cheque";
                vmNotifier.NotifyName = a + " Cheque signing pending.";
                vmNotifiersList.Add(vmNotifier);
            }


            var b = db.Acc_Notifier.Count(x => x.Active == true && x.IsSeen == false && (x.Type == 1));
            if (b > 0)
            {
                vmNotifier = new VmNotifier();
                vmNotifier.NotifyCounter = b;
                vmNotifier.NotifyLink = "lcopen";
                //vmNotifier.NotifyLink = "~/../Notifier?type=lcopen";

                vmNotifier.NotifyName = b + " L/C Opening pending.";
                vmNotifiersList.Add(vmNotifier);
            }

            var c = db.Acc_Notifier.Count(x => x.Active == true && x.IsSeen == false && (x.Type == 2));
            if (c > 0)
            {
                vmNotifier = new VmNotifier();
                vmNotifier.NotifyCounter = c;
                vmNotifier.NotifyLink = "lcpayment";
                //vmNotifier.NotifyLink = "~/../Notifier?type=lcpayment";

                vmNotifier.NotifyName = c + " L/C Doc Payment pending.";
                vmNotifiersList.Add(vmNotifier);
            }


            this.DataList = vmNotifiersList;
            this.TotalNotifyCounter = a + b + c;
        }
        public decimal PartyOpeningLedger(Common_Supplier Common_Supplier)
        {
            VmAccounting vmAccounting = new VmAccounting();
            vmAccounting.FromDate = new DateTime(2001, 1, 1);
            vmAccounting.ToDate = DateTime.Today;
            vmAccounting.GetSupplierLedgerLCOpening(Common_Supplier.ID);
            if (vmAccounting.Transaction.Count() > 0)
            {
                try
                {
                    var frombalance = vmAccounting.Transaction.Any() ? vmAccounting.Transaction[vmAccounting.Transaction.Count() - 1].Balance : 0;
                    return frombalance;
                }
                catch (Exception e)
                {
                    return 0;
                }

            }
            else
            {
                return 0;
            }
        }
        public decimal PartyPaymentLedger(Common_Supplier Common_Supplier)
        {
            VmAccounting vmAccounting = new VmAccounting();
            vmAccounting.FromDate = new DateTime(2001, 1, 1);
            vmAccounting.ToDate = DateTime.Today;
            vmAccounting.GetSupplierPaymentLedger(Common_Supplier.ID);
            if (vmAccounting.Transaction.Count() > 0)
            {
                try
                {
                    var frombalance = vmAccounting.Transaction.Any() ? vmAccounting.Transaction[vmAccounting.Transaction.Count() - 1].Balance : 0;
                    return frombalance;
                }
                catch (Exception e)
                {
                    return 0;
                }

            }
            else
            {
                return 0;
            }
        }
    }

    public class PartyPaymentLedgerList
    {
        public Common_Supplier Common_Supplier { get; set; }
        public decimal Common_SupplierOpeningBalance { get; set; }
    }

    public class VmNotifierApproval
    {
        private xOssContext db;
        private VmAcc_ChequeIntegration VmAcc_ChequeIntegration;
        private VmAcc_IntegrationJournal VmAcc_IntegrationJournal;
        private VmAcc_Transaction VmAcc_Transaction;
        public void NotifierBtbApproved(int id, VmUser_User user)
        {
            db = new xOssContext();
            Acc_Notifier accNotifier = db.Acc_Notifier.Find(id);
            accNotifier.ApprovedBy = user.User_User.ID;
            accNotifier.ApprovedDate = DateTime.Now;
            accNotifier.IsApproved = 1;

            this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
            {
                UserId = user.User_User.ID,
                Acc_Notifier = accNotifier
            };
            // update status
            this.VmAcc_ChequeIntegration.EditCheque(user.User_User.ID);
            var commercialB2bLc = db.Commercial_B2bLC.Find(accNotifier.RowIdFk);

            commercialB2bLc.IsApproved = true;
            db.Entry(commercialB2bLc).State = EntityState.Modified;
            var s = db.SaveChanges();


            // journal integration
            //Notifier Integration Get Closing Balance
            var commercial_UD = db.Commercial_UD.Find(commercialB2bLc.Commercial_UDFK);
            var common_Supplier = db.Common_Supplier.Find(commercialB2bLc.Common_SupplierFK);

            //Notifier Integration Get Closing Balance

            try
            {
                this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                {
                    UserId = user.User_User.ID,
                    Date = commercialB2bLc.Date,
                    Title = "BTB Foreign L/C",
                    CurrencyRate = commercialB2bLc.CurrencyRate,
                    CurrencyType = commercialB2bLc.CurrencyType,
                    FromAccNo = commercial_UD.Acc_AcNameFk,
                    ToAccNo = commercialB2bLc.Acc_AcNameFk,
                    Amount = (decimal)(commercialB2bLc.Amount),
                    Descriptions = "UD: " + commercial_UD.UdNo + " Payable to BTB Foreign L/C : " + commercialB2bLc.Name + ". Party Name: " + common_Supplier.Name,
                    TableIdFk = VmAcc_Database_Table.TableName(commercialB2bLc.GetType().Name),
                    TableRowIdFk = commercialB2bLc.ID,
                };
                var journalstatus = this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable();
            }
            catch (Exception e)
            {

            }
        }

        public void NotifierDocApproved(int id, VmUser_User user)
        {
            db = new xOssContext();
            Acc_Notifier accNotifier = db.Acc_Notifier.Find(id);
            accNotifier.ApprovedBy = user.User_User.ID;
            accNotifier.ApprovedDate = DateTime.Now;
            accNotifier.IsApproved = 1;

            this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
            {
                UserId = user.User_User.ID,
                Acc_Notifier = accNotifier
            };
            // update status
            VmAcc_ChequeIntegration.EditCheque(user.User_User.ID);

            var bPaymentInformation = db.Commercial_B2BPaymentInformation.Find(accNotifier.RowIdFk);

            bPaymentInformation.IsApproved = true;
            db.Entry(bPaymentInformation).State = EntityState.Modified;
            var s = db.SaveChanges();

            //Notifier Integration Get Closing Balance

            var commercial_B2bLC = db.Commercial_B2bLC.Find(bPaymentInformation.Commercial_B2bLCFK);
            var common_Supplier = db.Common_Supplier.Find(commercial_B2bLC.Common_SupplierFK);
            //Notifier Integration Get Closing Balance

            // journal integration
            try
            {
                this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                {
                    UserId = user.User_User.ID,
                    Date = bPaymentInformation.Date,
                    Title = "BTB L/C Payment",
                    CurrencyRate = commercial_B2bLC.CurrencyRate,
                    CurrencyType = commercial_B2bLC.CurrencyType,
                    FromAccNo = commercial_B2bLC.Acc_AcNameFk,
                    ToAccNo = common_Supplier.Acc_AcNameFk,
                    Amount = (decimal)(bPaymentInformation.B2bLCPayment),
                    Descriptions = "BTB L/C Payment " + commercial_B2bLC.Name + " to " + common_Supplier.Name,
                    TableIdFk = VmAcc_Database_Table.TableName(bPaymentInformation.GetType().Name),
                    TableRowIdFk = bPaymentInformation.ID,
                };
                var journalstatus = this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable();
            }
            catch (Exception e)
            {

            }
        }
        public void NotifierFinalized(int id, VmUser_User user)
        {
            db = new xOssContext();
            Acc_Notifier accNotifier = db.Acc_Notifier.Find(id);
            accNotifier.ApprovedBy = user.User_User.ID;
            accNotifier.ApprovedDate = DateTime.Now;
            accNotifier.IsApproved = 1;

            this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
            {
                UserId = user.User_User.ID,
                Acc_Notifier = accNotifier
            };
            // update status
            this.VmAcc_ChequeIntegration.EditCheque(user.User_User.ID);

            if (accNotifier.TableIdFk == 14)
            {
                // to retrieve transaction data(payment/received)
                this.VmAcc_Transaction = new VmAcc_Transaction();
                this.VmAcc_Transaction.SelectSingleAcc_Transaction(accNotifier.RowIdFk);

                var checkno = "";
                if (!string.IsNullOrEmpty(VmAcc_Transaction.Acc_Transaction.ChequeNo))
                {
                    checkno = ", Cheque No. :" + VmAcc_Transaction.Acc_Transaction.ChequeNo;
                }
                var description = "";
                if (!string.IsNullOrEmpty(VmAcc_Transaction.Acc_Transaction.Description))
                {
                    description = ", Des :" + VmAcc_Transaction.Acc_Transaction.Description;
                }
                var checkdate = "";
                if (VmAcc_Transaction.Acc_Transaction.ChequeDate != null)
                {
                    checkdate = ", Cheque Date :" + VmAcc_Transaction.Acc_Transaction.ChequeDate.ToString().Substring(0, 10);
                }
                // to initialize transaction data in journal model
                this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                {
                    UserId = user.User_User.ID,
                    Date = this.VmAcc_Transaction.Acc_Transaction.Date,
                    CostPool = this.VmAcc_Transaction.Acc_Transaction.Acc_CostPoolFK,
                    Title = this.VmAcc_Transaction.Acc_Transaction.TransactionType,
                    FromAccNo = this.VmAcc_Transaction.Acc_Transaction.From_Acc_NameFK,
                    ToAccNo = this.VmAcc_Transaction.Acc_Transaction.To_Acc_NameFK,
                    Amount = this.VmAcc_Transaction.Acc_Transaction.Amount,
                    Descriptions = "Voucher no: " + VmAcc_Transaction.Acc_Transaction.VoucherNo + checkno + checkdate + description,
                    TableIdFk = VmAcc_Database_Table.TableName("Acc_Transaction"),
                    TableRowIdFk = this.VmAcc_Transaction.Acc_Transaction.ID,
                };

                // call integration and receive boolean. if true data add in journal successfully else not added
                var journalstatus = this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable();
                if (!string.IsNullOrEmpty(journalstatus))
                {
                    VmAcc_Transaction o = new VmAcc_Transaction();
                    o.ChangeFinaliseStatus(accNotifier.RowIdFk);
                }
            }
            else if (accNotifier.TableIdFk == 13)
            {
                VmAcc_SupplierPH vmAcc_SupplierPH = new VmAcc_SupplierPH();
                var acc_SupplierPH = db.Acc_SupplierPH.Find(accNotifier.RowIdFk);

                acc_SupplierPH.IsApproved = true;
                db.Entry(acc_SupplierPH).State = EntityState.Modified;
                var s = db.SaveChanges();

                vmAcc_SupplierPH.Acc_SupplierPH = acc_SupplierPH;
                var common_Supplier = db.Common_Supplier.Find(acc_SupplierPH.Common_SupplierFK);
                var accountname = db.Acc_AcName.Find(acc_SupplierPH.Acc_AcNameFK);
                // journal integration
                try
                {
                    var chqno = "";
                    if (!string.IsNullOrEmpty(vmAcc_SupplierPH.Acc_SupplierPH.ChequeVoucherNo))
                        chqno = ", Cheque No: " + vmAcc_SupplierPH.Acc_SupplierPH.ChequeVoucherNo;
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = vmAcc_SupplierPH.Acc_SupplierPH.Date,
                        Title = "L/C Other Payment",
                        CurrencyType = vmAcc_SupplierPH.Acc_SupplierPH.Common_CurrencyFK,
                        CurrencyRate = (decimal)vmAcc_SupplierPH.Acc_SupplierPH.DollarRate,
                        FromAccNo = vmAcc_SupplierPH.Acc_SupplierPH.Acc_AcNameFK,
                        ToAccNo = common_Supplier.Acc_AcNameFk,
                        Amount = (decimal)(vmAcc_SupplierPH.Acc_SupplierPH.Amount),
                        Descriptions = "BTB L/C Other Payment from " + accountname.Name + " to " + common_Supplier.Name + ", Des: " + vmAcc_SupplierPH.Acc_SupplierPH.Description + chqno,
                        TableIdFk = VmAcc_Database_Table.TableName(vmAcc_SupplierPH.Acc_SupplierPH.GetType().Name),
                        TableRowIdFk = acc_SupplierPH.ID,
                    };
                    var journalstatus = this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable();
                }
                catch (Exception e)
                {

                }
            }
        }
        public void NotifierChequeReceiveFinalized(int id, VmUser_User user)
        {
            db = new xOssContext();
            Acc_Notifier accNotifier = db.Acc_Notifier.Find(id);
            accNotifier.ApprovedBy = user.User_User.ID;
            accNotifier.ApprovedDate = DateTime.Now;
            accNotifier.IsApproved = 1;

            this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
            {
                UserId = user.User_User.ID,
                Acc_Notifier = accNotifier
            };
            // update status
            this.VmAcc_ChequeIntegration.EditCheque(user.User_User.ID);

            VmAcc_SupplierReceiveH vmAcc_SupplierReceiveH = new VmAcc_SupplierReceiveH();
            var acc_SupplierReceiveH = db.Acc_SupplierReceiveH.Find(accNotifier.RowIdFk);

            acc_SupplierReceiveH.IsApproved = true;
            db.Entry(acc_SupplierReceiveH).State = EntityState.Modified;
            var s = db.SaveChanges();

            vmAcc_SupplierReceiveH.Acc_SupplierReceiveH = acc_SupplierReceiveH;
            var common_Supplier = db.Common_Supplier.Find(acc_SupplierReceiveH.Common_SupplierFK);
            var accountname = db.Acc_AcName.Find(acc_SupplierReceiveH.Acc_AcNameFK);
            // journal integration
            try
            {
                var chqno = "";
                if (!string.IsNullOrEmpty(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.ChequeVoucherNo))
                    chqno = ", Cheque No: " + vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.ChequeVoucherNo;
                this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                {
                    UserId = user.User_User.ID,
                    Date = vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Date,
                    Title = "L/C Cash Receive",
                    CurrencyType = vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Common_CurrencyFK,
                    CurrencyRate = (decimal)vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.DollarRate,
                    ToAccNo = vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Acc_AcNameFK,
                    FromAccNo = common_Supplier.Acc_AcNameFk,
                    Amount = (decimal)(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Amount),
                    Descriptions = "BTB L/C Cash Receive to " + accountname.Name + " from " + common_Supplier.Name + ", Des: " + vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Description + chqno,
                    TableIdFk = VmAcc_Database_Table.TableName(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.GetType().Name),
                    TableRowIdFk = acc_SupplierReceiveH.ID,
                };
                var journalstatus = this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable();
            }
            catch (Exception e)
            {

            }
        }
    }
}