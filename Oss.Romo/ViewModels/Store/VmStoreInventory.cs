﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Store
{
    public class StockOutQuantity
    {
        public decimal? StockOut { get; set; }
        public string Requition { get; set; }
    }
    public class CategoryDetails
    {
        public int ID { get; set; }
        public string Name { get; set; }

    }
    public class ItemDetails
    {
        public string Name { get; set; }
        public int ID { get; set; }

    }
    public class ItemQuantity
    {
        public decimal? Quantity { get; set; }

        public decimal? Price { get; set; }
        public string Challan { get; set; }
        public decimal? StockOut { get; set; }

        public string PO { get; set; }
        public string ID { get; set; }
        public decimal? TotalStockInQuantity { get; set; }
    }



    public class VmStoreInventory
    {

        private xOssContext db;
        public IEnumerable<VmStoreInventory> DataList { get; set; }
        public IEnumerable<VmStoreInventory> ClosingInventory { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public List<ItemDetails> ItemDetails { get; set; }
        public List<ItemQuantity> ItemQty { get; set; }
        public List<CategoryDetails> CategoryDetails { get; set; }

        public List<VmStoreInventory> Inventory { get; set; }

        public List<StockOutQuantity> StockOutQuantitys { get; set; }

        public List<ReportDocType> ItemInventoryReport { get; set; }

        public decimal? TotalStockIn { get; set; }
        [DisplayName("Total Price")]
        public decimal? TotalPrice { get; set; }
        [DisplayName("Total Quantity ")]
        public decimal? TotalRequired { get; set; }

        public decimal? TotalStockOut { get; set; }

        public decimal? TotalConsumption { get; set; }

        public decimal? ProductionStore { get; set; }

        public decimal? CentralStore { get; set; }

        public decimal? TotalStore { get; set; }

        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_POSlave_Consumption Mkt_POSlave_Consumption { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_POSlave_Receiving Mkt_POSlave_Receiving { get; set; }

        public Mkt_PO Mkt_PO { get; set; }
        [DisplayName("Value")]
        public decimal? CategoryTotalPrice { get; set; }
        [DisplayName("Unit Price")]
        [DisplayFormat(DataFormatString = "{0:0.0000}")]
        public decimal? CaunitPrice { get; set; }

        [DisplayName("Value")]
        public decimal? SubCategoryTotalPrice { get; set; }
        [DisplayName("Value")]
        public decimal? ItemTotalPrice { get; set; }

        [DisplayName("Quantity")]
        public decimal? CategoryQuantity { get; set; }
        [DisplayName("Stock Out")]
        public decimal? CategoryStockOutQuantity { get; set; }
        [DisplayName("Quantity")]
        public decimal? SubCategoryQuantity { get; set; }
        [DisplayName("Quantity")]
        public decimal? ItemQuantity { get; set; }
        [DisplayName("Stock Out")]
        public decimal? SubCategoryStockOutQuantity { get; set; }
        [DisplayName("Stock Out")]
        public decimal? ItemStockOutQuantity { get; set; }

        [DisplayName("PO Issued")]
        public decimal? PoIssued { get; set; }
        [DisplayName("Store Received")]
        public decimal? StoreReceived { get; set; }
        [DisplayName("Supplier Due")]
        public decimal? SupplierDue { get; set; }
        [DisplayName("Consumption")]
        public decimal? Consumption { get; set; }
        [DisplayName("Store Balance")]
        public decimal? StoreBalance { get; set; }
      

        public string Challan { get; set; }

        public string POID { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_Item
                     join t2 in db.Raw_SubCategory
                     on t1.Raw_SubCategoryFK equals t2.ID
                     join t3 in db.Raw_Category
                     on t2.Raw_CategoryFK equals t3.ID
                     select new VmStoreInventory
                     {
                         Raw_Item = t1,
                         Raw_SubCategory = t2,
                         Raw_Category = t3
                     }).Where(x => x.Raw_Item.Active == true).OrderBy(x => x.Raw_Item.ID).AsEnumerable();
            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in v)
            {

                x.TotalStockIn = x.GetTotalReceive(x.Raw_Item.ID);
                x.TotalPrice = x.GetTotalPrice(x.Raw_Item.ID);
                x.TotalRequired = x.GetTotalRequired(x.Raw_Item.ID);
                x.TotalStockOut = x.GetTotalOut(x.Raw_Item.ID);


                list.Add(x);

            }
            this.DataList = list;
        }


        public List<CategoryStockQty> CategoryWiseStock()
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_Item
                     join t2 in db.Raw_SubCategory
                     on t1.Raw_SubCategoryFK equals t2.ID
                     join t3 in db.Raw_Category
                     on t2.Raw_CategoryFK equals t3.ID
                     select new VmStoreInventory
                     {
                         Raw_Item = t1,
                         Raw_SubCategory = t2,
                         Raw_Category = t3
                     }).Where(x => x.Raw_Item.Active == true).OrderBy(x => x.Raw_Item.ID).AsEnumerable();
            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in v)
            {
                x.TotalStockIn = x.GetTotalReceive(x.Raw_Item.ID);
                x.TotalPrice = x.GetTotalPrice(x.Raw_Item.ID);
                x.TotalRequired = x.GetTotalRequired(x.Raw_Item.ID);
                x.TotalStockOut = x.GetTotalOut(x.Raw_Item.ID);
                list.Add(x);

            }
            var groupedCustomerList = list.GroupBy(u => u.Raw_Category.ID).Select(
                 g => new CategoryStockQty
                 {
                     Name = g.First().Raw_Category.Name,
                     Balance = (decimal) g.Sum(s => s.TotalStockIn - s.TotalStockOut),
                 }
                ).ToList();






            //var v = (from t1 in db.Raw_Category
                    
            //         select new VmStoreInventory
            //         {
            //             Raw_Category = t1
            //         }).Where(x => x.Raw_Category.Active == true).OrderBy(x => x.Raw_Category.ID).AsEnumerable();
            //List<VmStoreInventory> list = new List<VmStoreInventory>();
            //foreach (VmStoreInventory x in v)
            //{
            //    x.TotalStockIn = x.GetTotalReceive(x.Raw_Category.ID);
            //    x.TotalPrice = x.GetTotalPrice(x.Raw_Category.ID);
            //    x.TotalRequired = x.GetTotalRequired(x.Raw_Item.ID);
            //    x.TotalStockOut = x.GetTotalOut(x.Raw_Item.ID);
            //    list.Add(x);

            //}
            //var groupedCustomerList = list.GroupBy(u => u.Raw_Category.ID).Select(
            //     g => new CategoryStockQty
            //     {
            //         Name = g.First().Raw_Category.Name,
            //         Balance = (decimal)g.Sum(s => s.TotalStockIn - s.TotalStockOut),
            //     }
            //    ).ToList();
            return groupedCustomerList;
        }



        internal decimal GetTotalPrice(int id)
        {
            db = new xOssContext();
            var x = (from t3 in db.Mkt_POSlave_Receiving
                     join t1 in db.Mkt_POSlave
                     on t3.Mkt_POSlaveFK equals t1.ID
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     where t1.Raw_ItemFK == id
                     && t3.IsReturn == false && t3.Active == true && t1.Active == true
                     select t1.Price).Sum().GetValueOrDefault();
            return x;

        }

        //................................Item Inventory.................//

        public VmStoreInventory SelectJoinedItem(int id)
        {

            db = new xOssContext();
            var v = (from t1 in db.Raw_Category
                     join t2 in db.Raw_SubCategory
                     on t1.ID equals t2.Raw_CategoryFK
                     join t3 in db.Raw_Item
                     on t2.ID equals t3.Raw_SubCategoryFK
                     where t3.ID == id
                     select new VmStoreInventory
                     {
                         Raw_Category = t1,
                         Raw_SubCategory = t2,
                         Raw_Item = t3
                     }).FirstOrDefault();
            return v;
        }

        public void SelectSingleItem(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_Item
                     where t1.ID == id

                     select new VmStoreInventory
                     {
                         Raw_Item = t1

                     }).ToList();

            foreach (var x in v)
            {

                x.ItemQty = GetItemStockInQty(x.Raw_Item.ID);


            }

            this.DataList = v;

        }

        public List<ItemQuantity> GetItemStockInQty(int id)
        {
            db = new xOssContext();
            var v = (from t3 in db.Mkt_POSlave_Receiving
                     join t1 in db.Mkt_POSlave
                     on t3.Mkt_POSlaveFK equals t1.ID
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t4 in db.Mkt_PO
                     on t1.Mkt_POFK equals t4.ID

                     where t2.ID == id && t3.Active == true && t1.Active == true

                     select new ItemQuantity
                     {
                         Quantity = t3.Quantity,
                         Price = t1.Price,
                         Challan = t3.Challan,
                         PO = t4.CID,
                         ID = t4.ID + "x" + t4.Common_SupplierFK + "y" + t4.Mkt_BOMFK,



                     }).ToList();



            return v;

        }



        public List<StockOutQuantity> GetItemStockOutQty(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_POSlave_Consumption
                     on t1.ID equals t2.Mkt_POSlaveFK
                     join t3 in db.Raw_Item
                     on t1.Raw_ItemFK equals t3.ID

                     where t3.ID == id

                     select new StockOutQuantity
                     {
                         StockOut = t2.Quantity,
                         Requition = t2.Requisition

                     }).Distinct().ToList();



            return v;

        }


        //................................SubCategory Inventory.................//







        public void SelectSingleSubCategory(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_SubCategory
                     where t1.ID == id

                     select new VmStoreInventory
                     {
                         Raw_SubCategory = t1,


                     }).ToList();

            foreach (var x in v)
            {
                x.ItemDetails = GetItem(x.Raw_SubCategory.ID);

            }

            this.DataList = v;

        }



        public List<ItemDetails> GetItem(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_SubCategory
                     join t2 in db.Raw_Item
                     on t1.ID equals t2.Raw_SubCategoryFK

                     where t1.ID == id
                     && t1.Active == true
                     && t2.Active == true

                     select new ItemDetails
                     {
                         Name = t2.Name,
                         ID = t2.ID

                     }).Distinct().ToList();
            return v;

        }

        //................................Category Inventory.................//


        public void SelectSingleCategory(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Raw_Category
                     where t1.ID == id

                     select new VmStoreInventory
                     {
                         Raw_Category = t1,
                     }).ToList();

            foreach (var x in a)
            {
                x.CategoryDetails = GetSubcategory(x.Raw_Category.ID);
            }


            this.DataList = a;

        }


        public List<CategoryDetails> GetSubcategory(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_Category
                     join t2 in db.Raw_SubCategory
                     on t1.ID equals t2.Raw_CategoryFK

                     where t1.ID == id
                     && t1.Active == true
                     && t2.Active == true

                     select new CategoryDetails
                     {
                         ID = t2.ID,
                         Name = t2.Name

                     }).ToList();
            return v;

        }





        //public void ItemInventory()
        //{
        //    db = new xOssContext();
        //    var comQuantity = (from t1 in db.Mkt_POSlave
        //                       join t4 in db.Mkt_POSlave_Consumption
        //                        on t1.ID equals t4.Mkt_POSlaveFK
        //                       where t1.TotalRequired != 0 && t4.Quantity != 0 && t1.Active == true &&
        //                        t4.Active == true && t4.IsReturn == false
        //                       group new { t1, t4 } by t1.Raw_ItemFK into Mkt_POSlave
        //                       select new Inventory
        //                       {
        //                           Name = Mkt_POSlave.Key.ToString(),
        //                           TotalRequired = 0,
        //                           TotalReceived = 0,
        //                           TotalConsumption = (decimal)Mkt_POSlave.Sum(y => y.t4.Quantity),
        //                       }).Union(from t1 in db.Mkt_POSlave
        //                                join t4 in db.Mkt_POSlave_Receiving
        //                                 on t1.ID equals t4.Mkt_POSlaveFK
        //                                where t1.TotalRequired != 0 && t4.Quantity != 0 && t1.Active == true &&
        //                                 t4.Active == true && t4.IsReturn == false
        //                                group new { t1, t4 } by t1.Raw_ItemFK into Mkt_POSlave
        //                                select new Inventory
        //                                {
        //                                    Name = Mkt_POSlave.Key.ToString(),
        //                                    TotalRequired = 0,
        //                                    TotalReceived = (decimal)Mkt_POSlave.Sum(y => y.t4.Quantity),
        //                                    TotalConsumption = 0,
        //                                }).Union(from t1 in db.Mkt_POSlave
        //                                         where t1.TotalRequired != 0 && t1.Active == true
        //                                         group new { t1 } by t1.Raw_ItemFK into Mkt_POSlave
        //                                         select new Inventory
        //                                         {
        //                                             Name = Mkt_POSlave.Key.ToString(),
        //                                             TotalRequired = (decimal)Mkt_POSlave.Sum(y => y.t1.TotalRequired),
        //                                             TotalReceived = 0,
        //                                             TotalConsumption = 0,
        //                                         });

        //    var t=from u in comQuantity join t4 in db.Mkt_POSlave_Consumption
        //    on t1.ID equals t4.Mkt_POSlaveFK 
        //    var s = comQuantity.ToList();
        //}


        public void ItemInventory()
        {
            db = new xOssContext();
            var a = (from t1 in db.Raw_Item


                     select new VmStoreInventory
                     {
                         Raw_Item = t1


                     }).ToList();


            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in a)
            {
                x.TotalRequired = x.GetTotalRequired(x.Raw_Item.ID);
                x.TotalStockIn = x.GetTotalReceive(x.Raw_Item.ID);
                x.TotalStockOut = x.GetTotalOut(x.Raw_Item.ID);
                x.TotalConsumption = (decimal)00.0;
                x.ProductionStore = (x.TotalStockOut - x.TotalConsumption);
                x.CentralStore = (x.TotalStockIn - x.TotalStockOut);
                x.TotalStore = (x.ProductionStore + x.CentralStore);


                list.Add(x);
            }

            this.DataList = list;

        }

        public void ItemLeftOverInventory()
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_BOM
                     join t2 in db.Common_TheOrder
                     on t1.Common_TheOrderFk equals t2.ID
                     join t3 in db.Mkt_PO
                     on t1.ID equals t3.Mkt_BOMFK
                     join t4 in db.Mkt_POSlave
                     on t3.ID equals t4.Mkt_POFK
                     join t5 in db.Raw_Item
                     on t4.Raw_ItemFK equals t5.ID

                     where t2.IsComplete == true
                     select new VmStoreInventory
                     {
                         Raw_Item = t5


                     }).Distinct().ToList();


            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in a)
            {
                x.TotalRequired = x.GetTotalRequiredLeftover(x.Raw_Item.ID);
                x.TotalStockIn = x.GetTotalReceiveLeftover(x.Raw_Item.ID);
                x.TotalStockOut = x.GetTotalOutLeftover(x.Raw_Item.ID);
                x.TotalConsumption = (decimal)00.0;
                x.ProductionStore = (x.TotalStockOut - x.TotalConsumption);
                x.CentralStore = (x.TotalStockIn - x.TotalStockOut);
                x.TotalStore = (x.ProductionStore + x.CentralStore);


                list.Add(x);
            }

            this.DataList = list;

        }



        internal decimal GetTotalRequired(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Raw_Item
                     join t2 in db.Mkt_POSlave
                     on t1.ID equals t2.Raw_ItemFK
                     join t3 in db.Mkt_PO
                     on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM
                     on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder
                     on t4.Common_TheOrderFk equals t5.ID
                     where t2.Raw_ItemFK == id && t1.Active == true && t2.Active == true && t5.IsComplete == false
                     select t2.TotalRequired).Sum().GetValueOrDefault();

            return x;
        }

        internal decimal GetTotalRequiredLeftover(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Raw_Item
                     join t2 in db.Mkt_POSlave
                     on t1.ID equals t2.Raw_ItemFK
                     join t3 in db.Mkt_PO
                     on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM
                     on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder
                     on t4.Common_TheOrderFk equals t5.ID
                     where t2.Raw_ItemFK == id && t1.Active == true && t2.Active == true && t5.IsComplete == true
                     select t2.TotalRequired).Sum().GetValueOrDefault();

            return x;
        }

        internal decimal GetTotalReceive(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Raw_Item
                     join t2 in db.Mkt_POSlave
                     on t1.ID equals t2.Raw_ItemFK
                     join t6 in db.Mkt_POSlave_Receiving
                     on t2.ID equals t6.Mkt_POSlaveFK
                     join t3 in db.Mkt_PO
                     on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM
                     on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder
                     on t4.Common_TheOrderFk equals t5.ID
                     where t2.Raw_ItemFK == id
                     && t6.IsReturn == false && t3.Active == true && t1.Active == true && t5.IsComplete == false
                     select t6.Quantity).Sum().GetValueOrDefault();
            return x;

        }
         internal decimal GetTotalReceivebyCategory(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave_Receiving
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     join t3 in db.Raw_Item on t2.Raw_ItemFK equals t3.ID
                     join t4 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t4.ID
                     join t5 in db.Mkt_PO on t2.Mkt_POFK equals t5.ID
                     join t6 in db.Mkt_BOM on t5.Mkt_BOMFK equals t6.ID
                     join t7 in db.Common_TheOrder on t6.Common_TheOrderFk equals t7.ID
                     where t4.Raw_CategoryFK == id
                     && t1.IsReturn == false && t3.Active == true && t1.Active == true
                     select t1.Quantity).Sum().GetValueOrDefault();


            //from t1 in db.Raw_Item
            //     join t2 in db.Mkt_POSlave
            //     on t1.ID equals t2.Raw_ItemFK
            //     join t6 in db.Mkt_POSlave_Receiving
            //     on t2.ID equals t6.Mkt_POSlaveFK
            //     join t3 in db.Mkt_PO
            //     on t2.Mkt_POFK equals t3.ID
            //     join t4 in db.Mkt_BOM
            //     on t3.Mkt_BOMFK equals t4.ID
            //     join t5 in db.Common_TheOrder
            //     on t4.Common_TheOrderFk equals t5.ID
            //     where t2.Raw_ItemFK == id
            //     && t6.IsReturn == false && t3.Active == true && t1.Active == true && t5.IsComplete == false
            //     select t6.Quantity).Sum().GetValueOrDefault();
            return x;

        }

        internal decimal GetTotalReceiveLeftover(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Raw_Item
                     join t2 in db.Mkt_POSlave
                     on t1.ID equals t2.Raw_ItemFK
                     join t6 in db.Mkt_POSlave_Receiving
                     on t2.ID equals t6.Mkt_POSlaveFK
                     join t3 in db.Mkt_PO
                     on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM
                     on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder
                     on t4.Common_TheOrderFk equals t5.ID
                     where t2.Raw_ItemFK == id
                     && t6.IsReturn == false && t3.Active == true && t1.Active == true && t5.IsComplete == true
                     select t6.Quantity).Sum().GetValueOrDefault();
            return x;

        }

        internal decimal GetTotalOut(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Raw_Item
                     join t2 in db.Mkt_POSlave
                     on t1.ID equals t2.Raw_ItemFK
                     join t6 in db.Mkt_POSlave_Consumption
                     on t2.ID equals t6.Mkt_POSlaveFK
                     join t3 in db.Mkt_PO
                     on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM
                     on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder
                     on t4.Common_TheOrderFk equals t5.ID
                     where t2.Raw_ItemFK == id
                     && t6.IsReturn == false && t3.Active == true && t1.Active == true && t5.IsComplete == false
                     select t6.Quantity).Sum().GetValueOrDefault();

            return x;
        }


        internal decimal GetTotalOutLeftover(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Raw_Item
                     join t2 in db.Mkt_POSlave
                     on t1.ID equals t2.Raw_ItemFK
                     join t6 in db.Mkt_POSlave_Consumption
                     on t2.ID equals t6.Mkt_POSlaveFK
                     join t3 in db.Mkt_PO
                     on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM
                     on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder
                     on t4.Common_TheOrderFk equals t5.ID
                     where t2.Raw_ItemFK == id
                     && t6.IsReturn == false && t3.Active == true && t1.Active == true && t5.IsComplete == true
                     select t6.Quantity).Sum().GetValueOrDefault();

            return x;
        }
        //........................Category Wise Inventory....................//
        public void CategoryWiseInventory()
        {

            db = new xOssContext();
            var a = (from t1 in db.Raw_Category

                     select new VmStoreInventory
                     {
                         Raw_Category = t1
                     }).Where(x => x.Raw_Category.Active == true).OrderBy(x => x.Raw_Category.ID).AsEnumerable();


            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in a)
            {
                x.CategoryTotalPrice = x.CategoryWiseTotalPrice(x.Raw_Category.ID);
                x.CategoryQuantity = x.CategoryWiseTotalReceivedQty(x.Raw_Category.ID);
                x.CategoryStockOutQuantity = x.CategoryWiseTotalStockOutQty(x.Raw_Category.ID);


                //if (x.CategoryTotalPrice !=0 && x.CategoryQuantity != 0)
                //{
                //    x.CaunitPrice = (x.CategoryQuantity / x.CategoryTotalPrice);
                //}
                //else
                //{
                //    x.CaunitPrice = 0;
                //}
                list.Add(x);

            }
            this.DataList = list;
        }

        public void GetClosingInventorys()
        {

            db = new xOssContext();
            var a = (from t1 in db.Raw_Category
                     where t1.Active == true && t1.IsInventory
                     select new VmStoreInventory
                     {
                         Raw_Category = t1
                     }).OrderBy(x => x.Raw_Category.Name).AsEnumerable();


            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in a)
            {
                x.PoIssued = x.CategoryWiseTotalPoIssuedQty(x.Raw_Category.ID);
                x.StoreReceived = x.CategoryWiseTotalClosedReceivedQty(x.Raw_Category.ID);
                x.SupplierDue = x.PoIssued - x.StoreReceived;
                x.Consumption = x.CategoryWiseTotalClosedStockOutQty(x.Raw_Category.ID);
                x.StoreBalance = x.StoreReceived - x.Consumption;
               
                list.Add(x);

            }
            this.ClosingInventory = list;
        }


        public void CategoryWiseInventoryById(int Id)
        {

            db = new xOssContext();
            var a = (from t1 in db.Raw_Category
                     where t1.ID == Id
                     select new VmStoreInventory
                     {
                         Raw_Category = t1
                     }).Where(x => x.Raw_Category.Active == true).OrderBy(x => x.Raw_Category.ID).AsEnumerable();


            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in a)
            {
                x.CategoryTotalPrice = x.CategoryWiseTotalPrice(x.Raw_Category.ID);
                x.CategoryQuantity = x.CategoryWiseTotalReceivedQty(x.Raw_Category.ID);

                list.Add(x);

            }
            this.DataList = list;
        }

        internal decimal CategoryWiseTotalPrice(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory
                     on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category
                     on t3.Raw_CategoryFK equals t4.ID
                     where t4.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     select t1.Price).Sum().GetValueOrDefault();

            return x;



        }


        internal decimal CategoryWiseTotalReceivedQty(int id)
        {
            db = new xOssContext();
            var x = (from
                     t5 in db.Mkt_POSlave_Receiving
                     join t1 in db.Mkt_POSlave
                     on t5.Mkt_POSlaveFK equals t1.ID
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory
                     on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category
                     on t3.Raw_CategoryFK equals t4.ID
                     where t4.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t5.Active == true && t5.IsReturn == false

                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;



        }
        #region Colsing Order
        internal decimal CategoryWiseTotalPoIssuedQty(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave
                     join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                     where t4.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t0.Active == true && t0.IsComplete == true

                     select t1.TotalRequired).Sum().GetValueOrDefault();

            return x;
            
        }
        internal decimal SubCategoryWiseTotalPoIssuedQty(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave
                     join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                     //join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                     where t3.ID == id && t1.Active == true && t2.Active == true && t3.Active == true// && t4.Active == true
                     && t0.Active == true && t0.IsComplete == true

                     select t1.TotalRequired).Sum().GetValueOrDefault();

            return x;

        }
        internal decimal ItemWiseTotalClosedPoIssuedQty(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave
                     join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                     //join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                     where t2.ID == id && t1.Active == true && t2.Active == true && t3.Active == true// && t4.Active == true
                     && t0.Active == true && t0.IsComplete == true

                     select t1.TotalRequired).Sum().GetValueOrDefault();

            return x;

        }
        internal decimal CategoryWiseTotalClosedReceivedQty(int id)
        {
            db = new xOssContext();
            var x = (from t5 in db.Mkt_POSlave_Receiving
                     join t1 in db.Mkt_POSlave on t5.Mkt_POSlaveFK equals t1.ID
                     join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                     where t4.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t5.Active == true && t5.IsReturn == false && t0.Active == true && t0.IsComplete == true

                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;



        }
        internal decimal SubCategoryWiseTotalClosedReceivedQty(int id)
        {
            db = new xOssContext();
            var x = (from t5 in db.Mkt_POSlave_Receiving
                     join t1 in db.Mkt_POSlave on t5.Mkt_POSlaveFK equals t1.ID
                     join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                     //join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                     where t3.ID == id && t1.Active == true && t2.Active == true && t3.Active == true// && t4.Active == true
                     && t5.Active == true && t5.IsReturn == false && t0.Active == true && t0.IsComplete == true

                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;



        }

        internal decimal ItemWiseTotalClosedReceivedQty(int id)
        {
            db = new xOssContext();
            var x = (from t5 in db.Mkt_POSlave_Receiving
                     join t1 in db.Mkt_POSlave on t5.Mkt_POSlaveFK equals t1.ID
                     join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                     //join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                     where t2.ID == id && t1.Active == true && t2.Active == true && t3.Active == true// && t4.Active == true
                     && t5.Active == true && t5.IsReturn == false && t0.Active == true && t0.IsComplete == true

                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;
            
        }

        internal decimal CategoryWiseTotalClosedStockOutQty(int id)
        {
            db = new xOssContext();
            var x = (from t5 in db.Mkt_POSlave_Consumption
                join t1 in db.Mkt_POSlave on t5.Mkt_POSlaveFK equals t1.ID
                join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
                join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                where t4.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                      && t5.Active == true && t5.IsReturn == false && t0.Active == true && t0.IsComplete == true

                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;
        }
        internal decimal SubCategoryWiseTotalClosedStockOutQty(int id)
        {
            db = new xOssContext();
            var x = (from t5 in db.Mkt_POSlave_Consumption
                     join t1 in db.Mkt_POSlave on t5.Mkt_POSlaveFK equals t1.ID
                     join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                     //join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                     where t3.ID == id && t1.Active == true && t2.Active == true && t3.Active == true// && t4.Active == true
                           && t5.Active == true && t5.IsReturn == false && t0.Active == true && t0.IsComplete == true

                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;
        }

        internal decimal ItemWiseTotalClosedStockOutQty(int id)
        {
            db = new xOssContext();
            var x = (from t5 in db.Mkt_POSlave_Consumption
                     join t1 in db.Mkt_POSlave on t5.Mkt_POSlaveFK equals t1.ID
                     join t0 in db.Mkt_PO on t1.Mkt_POFK equals t0.ID
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                     //join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                     where t2.ID == id && t1.Active == true && t2.Active == true && t3.Active == true// && t4.Active == true
                           && t5.Active == true && t5.IsReturn == false && t0.Active == true && t0.IsComplete == true

                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;
        }
        #endregion




        internal decimal CategoryWiseTotalStockOutQty(int id)
        {
            db = new xOssContext();
            var x = (from
                     t5 in db.Mkt_POSlave_Consumption
                     join t1 in db.Mkt_POSlave
                     on t5.Mkt_POSlaveFK equals t1.ID
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory
                     on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category
                     on t3.Raw_CategoryFK equals t4.ID
                     where t4.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t5.Active == true && t5.IsReturn == false

                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;



        }

        //..........................SubCategory Wise Inventory.............//

        public VmStoreInventory SelectSingleCategoryInventory(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_Category
                     where t1.ID == id

                     select new VmStoreInventory
                     {
                         Raw_Category = t1,

                     }).FirstOrDefault();


            return v;

        }




        public void SubCategoryWiseInventory(int id)
        {

            db = new xOssContext();
            var a = (from t1 in db.Raw_SubCategory
                     join t2 in db.Raw_Category
                     on t1.Raw_CategoryFK equals t2.ID
                     where t1.Raw_CategoryFK == id
                     select new VmStoreInventory
                     {
                         Raw_SubCategory = t1
                     }).Where(x => x.Raw_SubCategory.Active == true).OrderBy(x => x.Raw_SubCategory.ID).AsEnumerable();


            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in a)
            {
                x.SubCategoryTotalPrice = x.SubCategoryWiseTotalPrice(x.Raw_SubCategory.ID);
                x.SubCategoryQuantity = x.SubCategoryWiseTotalQty(x.Raw_SubCategory.ID);
                x.SubCategoryStockOutQuantity = x.SubCategoryWiseTotalStockOutQty(x.Raw_SubCategory.ID);

                list.Add(x);

            }

            var Caid = (from t1 in db.Raw_SubCategory

                        where t1.Raw_CategoryFK == id
                        select t1.Raw_CategoryFK

                   ).FirstOrDefault();


            this.DataList = list;

            this.CategoryTotalPrice = CategoryWiseTotalPrice(Caid);
            this.CategoryQuantity = CategoryWiseTotalReceivedQty(Caid);


        }

        public void SubCategoryWiseClosingInventory(int id)
        {

            db = new xOssContext();
            var a = (from t1 in db.Raw_SubCategory
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID
                     where t1.Raw_CategoryFK == id && t2.IsInventory && t1.Active == true
                     select new VmStoreInventory
                     {
                         Raw_SubCategory = t1,
                         Raw_Category = t2
                     }).OrderBy(x => x.Raw_SubCategory.Name).AsEnumerable();


            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in a)
            {
                x.PoIssued = x.SubCategoryWiseTotalPoIssuedQty(x.Raw_SubCategory.ID);
                x.StoreReceived = x.SubCategoryWiseTotalClosedReceivedQty(x.Raw_SubCategory.ID);
                x.SupplierDue = x.PoIssued - x.StoreReceived;
                x.Consumption = x.SubCategoryWiseTotalClosedStockOutQty(x.Raw_SubCategory.ID);
                x.StoreBalance = x.StoreReceived - x.Consumption;

                list.Add(x);

            }
            this.ClosingInventory = list;
        }



        internal decimal SubCategoryWiseTotalPrice(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory
                     on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category
                     on t3.Raw_CategoryFK equals t4.ID
                     where t3.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     select t1.Price).Sum().GetValueOrDefault();

            return x;



        }



        internal decimal SubCategoryWiseTotalQty(int id)
        {
            db = new xOssContext();
            var x = (from t5 in db.Mkt_POSlave_Receiving
                     join t1 in db.Mkt_POSlave
                     on t5.Mkt_POSlaveFK equals t1.ID
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory
                     on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category
                     on t3.Raw_CategoryFK equals t4.ID
                     where t3.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t5.Active == true && t5.IsReturn == false
                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;



        }


        internal decimal SubCategoryWiseTotalStockOutQty(int id)
        {
            db = new xOssContext();
            var x = (from t5 in db.Mkt_POSlave_Consumption
                     join t1 in db.Mkt_POSlave
                     on t5.Mkt_POSlaveFK equals t1.ID
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory
                     on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category
                     on t3.Raw_CategoryFK equals t4.ID
                     where t3.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t5.Active == true && t5.IsReturn == false
                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;



        }

        //..........................item  Wise Inventory.............//

        public VmStoreInventory SelectJoinedRaw_SubCategory(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_Category
                     join t2 in db.Raw_SubCategory
                     on t1.ID equals t2.Raw_CategoryFK

                     where t2.ID == id
                     select new VmStoreInventory
                     {
                         Raw_Category = t1,
                         Raw_SubCategory = t2,

                     }).FirstOrDefault();


            return v;
        }


        public void ItemWiseInventory(int id)
        {

            db = new xOssContext();
            var a = (from t3 in db.Raw_Item
                     join t1 in db.Raw_SubCategory
                     on t3.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category
                     on t1.Raw_CategoryFK equals t2.ID
                     where t3.Raw_SubCategoryFK == id
                     select new VmStoreInventory
                     {
                         Raw_Item = t3
                     }).Where(x => x.Raw_Item.Active == true).OrderBy(x => x.Raw_Item.ID).AsEnumerable();


            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in a)
            {
                x.ItemTotalPrice = x.ItemWiseTotalPrice(x.Raw_Item.ID);
                x.ItemQuantity = x.ItemWiseTotalQty(x.Raw_Item.ID);
                x.ItemStockOutQuantity = x.ItemWiseTotalStockOutQty(x.Raw_Item.ID);
                list.Add(x);

            }





            var SubCaid = (from t1 in db.Raw_Item

                           where t1.Raw_SubCategoryFK == id
                           select t1.Raw_SubCategoryFK

             ).FirstOrDefault();


            this.SubCategoryTotalPrice = SubCategoryWiseTotalPrice(SubCaid);
            this.SubCategoryQuantity = SubCategoryWiseTotalQty(SubCaid);

            this.DataList = list;

        }

        public void ItemWiseClosingInventory(int id)
        {

            db = new xOssContext();
            var a = (from t3 in db.Raw_Item
                     join t1 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID
                     where t3.Raw_SubCategoryFK == id
                     select new VmStoreInventory
                     {
                         Raw_Item = t3
                     }).Where(x => x.Raw_Item.Active == true).OrderBy(x => x.Raw_Item.ID).AsEnumerable();

            List<VmStoreInventory> list = new List<VmStoreInventory>();
            foreach (VmStoreInventory x in a)
            {
                x.PoIssued = x.ItemWiseTotalClosedPoIssuedQty(x.Raw_Item.ID);
                x.StoreReceived = x.ItemWiseTotalClosedReceivedQty(x.Raw_Item.ID);
                x.SupplierDue = x.PoIssued - x.StoreReceived;
                x.Consumption = x.ItemWiseTotalClosedStockOutQty(x.Raw_Item.ID);
                x.StoreBalance = x.StoreReceived - x.Consumption;

                list.Add(x);
            }
            this.ClosingInventory = list;
        }


        internal decimal ItemWiseTotalPrice(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory
                     on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category
                     on t3.Raw_CategoryFK equals t4.ID
                     where t2.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     select t1.Price).Sum().GetValueOrDefault();

            return x;



        }


        internal decimal ItemWiseTotalQty(int id)
        {
            db = new xOssContext();
            var x = (from
                     t5 in db.Mkt_POSlave_Receiving
                     join t1 in db.Mkt_POSlave
                     on t5.Mkt_POSlaveFK equals t1.ID
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory
                     on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category
                     on t3.Raw_CategoryFK equals t4.ID
                     where t2.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t5.Active == true && t5.IsReturn == false
                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;



        }


        internal decimal ItemWiseTotalStockOutQty(int id)
        {
            db = new xOssContext();
            var x = (from
                     t5 in db.Mkt_POSlave_Consumption
                     join t1 in db.Mkt_POSlave
                     on t5.Mkt_POSlaveFK equals t1.ID
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory
                     on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category
                     on t3.Raw_CategoryFK equals t4.ID
                     where t2.ID == id && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                     && t5.Active == true && t5.IsReturn == false
                     select t5.Quantity).Sum().GetValueOrDefault();

            return x;



        }


        //..................Item Details...............//
        public VmStoreInventory SelectSingleRaw_Item(int id)
        {
            db = new xOssContext();
            var v = (from t3 in db.Raw_Item
                     join t1 in db.Raw_SubCategory
                     on t3.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category
                     on t1.Raw_CategoryFK equals t2.ID
                     where t3.ID == id

                     select new VmStoreInventory
                     {
                         Raw_Item = t3,
                         Raw_Category = t2,
                         Raw_SubCategory = t1
                     }).FirstOrDefault();


            return v;
        }

        public void ItemWiseDetailsInventory(int id)
        {
            List<VmStoreInventory> list = new List<VmStoreInventory>();
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Mkt_POSlave_Receiving
                     on t1.ID equals t3.Mkt_POSlaveFK
                     join t4 in db.Mkt_POSlave_Consumption
                     on t1.ID equals t4.Mkt_POSlaveFK
                     join t5 in db.Mkt_PO
                     on t1.Mkt_POFK equals t5.ID
                     join t6 in db.Common_Supplier
                     on t5.Common_SupplierFK equals t6.ID
                     join t7 in db.Mkt_BOM
                     on t5.Mkt_BOMFK equals t7.ID
                     where t1.Raw_ItemFK == id
                     && t1.Active == true
                     && t2.Active == true
                     && t3.Active == true
                     && t4.Active == true
                     && t5.Active == true
                     && t6.Active == true
                     && t7.Active == true
                     && t3.Quantity != 0
                     && t4.Quantity != 0

                     select new VmStoreInventory
                     {
                         Mkt_POSlave_Receiving = t3,
                         Mkt_PO = t5,
                         Mkt_POSlave_Consumption = t4
                     }).ToList();

            var Itid = (from t1 in db.Raw_Item
                        where t1.ID == id
                        select t1.ID).FirstOrDefault();



            this.TotalPrice = ItemWiseTotalPrice(Itid);
            this.TotalRequired = ItemWiseTotalQty(Itid);



            var DrQty = (from t1 in db.Mkt_POSlave
                         join t2 in db.Raw_Item
                         on t1.Raw_ItemFK equals t2.ID
                         join t3 in db.Mkt_POSlave_Receiving
                         on t1.ID equals t3.Mkt_POSlaveFK
                         join t4 in db.Mkt_POSlave_Consumption
                         on t1.ID equals t4.Mkt_POSlaveFK
                         //join t5 in db.Mkt_PO
                         //on t1.Mkt_POFK equals t5.ID
                         //join t6 in db.Common_Supplier
                         //on t5.Common_SupplierFK equals t6.ID
                         //join t7 in db.Mkt_BOM
                         //on t5.Mkt_BOMFK equals t7.ID
                         where t1.Raw_ItemFK == id && t1.Active == true
                         && t2.Active == true
                         && t3.Active == true
                         && t4.Active == true
                         //&& t5.Active == true
                         //&& t6.Active == true
                         //&& t7.Active == true 
                         && t3.IsReturn == false && t4.IsReturn == true
                         select t3.Quantity).DefaultIfEmpty(0).Sum();


            var CrQty = (from t1 in db.Mkt_POSlave
                         join t2 in db.Raw_Item
                         on t1.Raw_ItemFK equals t2.ID
                         join t3 in db.Mkt_POSlave_Receiving
                         on t1.ID equals t3.Mkt_POSlaveFK
                         join t4 in db.Mkt_POSlave_Consumption
                         on t1.ID equals t4.Mkt_POSlaveFK
                         //join t5 in db.Mkt_PO
                         //on t1.Mkt_POFK equals t5.ID
                         //join t6 in db.Common_Supplier
                         //on t5.Common_SupplierFK equals t6.ID
                         //join t7 in db.Mkt_BOM
                         //on t5.Mkt_BOMFK equals t7.ID
                         where t1.Raw_ItemFK == id && t1.Active == true
                         && t2.Active == true
                         && t3.Active == true
                         && t4.Active == true
                         //&& t5.Active == true
                         //&& t6.Active == true
                         //&& t7.Active == true 
                         && t3.IsReturn == false && t4.IsReturn == true
                         select t4.Quantity).DefaultIfEmpty(0).Sum();

            this.DataList = a;

        }








    }


    public class CategoryStockQty
    {
        public  string Name { get; set; }
        public  decimal Balance { get; set; }
    }



}