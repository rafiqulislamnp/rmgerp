﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Store;
using Oss.Romo.Models;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Store
{

    public class VmRaw_Category
    {
      
        private xOssContext db;
        public Raw_Category Raw_Category { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public IEnumerable<VmRaw_Category> DataList { get; set; }

     
     
        public VmRaw_Category()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from rc in db.Raw_Category
                     where rc.Active == true && rc.IsNew==true
                     select new VmRaw_Category
                     {
                         Raw_Category = rc
                     }).Where(x=>x.Raw_Category.Active==true).OrderByDescending(x => x.Raw_Category.ID).AsEnumerable();
            this.DataList = v;

        }

        public void LoadDataFromDatabase()
        {
            db = new xOssContext();

            var v = (from rc in db.Raw_Category
                     where rc.IsNew == true
                     select new VmRaw_Category
                     {
                         Raw_Category = rc
                     }).AsEnumerable();


            this.DataList = v;

        }
        //This  Code for edit
        public void SelectSingle(string rawCid)
        {
            int id = 0;
            rawCid = this.VmControllerHelper.Decrypt(rawCid);
            Int32.TryParse(rawCid, out id);
            db = new xOssContext();
            var v = (from rc in db.Raw_Category
                     select new VmRaw_Category
                     {
                         Raw_Category = rc
                     }).Where(c=>c.Raw_Category.ID == id).SingleOrDefault();
            this.Raw_Category = v.Raw_Category;

        }


        //public void SelectSingleCategory(int id)
        //{
        //    db = new xOssContext();
        //    var v = (from rc in db.Raw_Category
        //             select new VmRaw_Category
        //             {
        //                 Raw_Category = rc
        //             }).Where(c => c.Raw_Category.ID == id).SingleOrDefault();
        //    this.Raw_Category = v.Raw_Category;

        //}


  

    
        //Create 

        public int Add(int userID)
        {
            Raw_Category.AddReady(userID);
            return Raw_Category.Add();
        }
        //Update
        public bool Edit(int userID)
        {
            return Raw_Category.Edit(userID);
        }
        //Delete
        public bool Delete(int userID)
        {
            return Raw_Category.Delete(userID);
        }
    }
}