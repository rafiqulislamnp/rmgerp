﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Store
{
    public class VmMkt_POExtTransfer
    {
        private xOssContext db;
        public IEnumerable<VmMkt_POExtTransfer> DataList { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public VmMktPO_Slave VmMktPO_Slave { get; set; }
        public Prod_TransitionItemInventory Prod_TransitionItemInventory { get; set; }
        public Mkt_POExtTransfer Mkt_POExtTransfer { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_YarnCalculation Mkt_YarnCalculation { get; set; }
        public Mkt_YarnType Mkt_YarnType { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Prod_MachineDIA Prod_MachineDIA { get; set; }
        public string RequiredQty { get; set; }

        [Required(ErrorMessage ="Transfer Type Required"), Display(Name = "Transfer Type")]
        public int TransferType { get; set; }
        public string StyleName { get; set; }
        public string ReturnController { get; set; }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POExtTransfer
                     join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                     join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                     join t6 in db.Mkt_PO on t1.Mkt_POFK equals t6.ID
                     select new VmMkt_POExtTransfer
                     {
                         Mkt_POExtTransfer = t1,
                         Common_Unit = t2,
                         Raw_Item = t3,
                     }).FirstOrDefault(V => V.Mkt_POExtTransfer.ID == id);
            this.Mkt_POExtTransfer = a.Mkt_POExtTransfer;
            this.Common_Unit = a.Common_Unit;
            this.Raw_Item = a.Raw_Item;
        }

        public int Add(int userID)
        {
            Mkt_POExtTransfer.FirstCreatedBy = userID;
            Mkt_POExtTransfer.AddReady(userID);
            return Mkt_POExtTransfer.Add();
        }

        public bool Edit(int userID)
        {
            Mkt_POExtTransfer.LastEditeddBy = userID;
            return Mkt_POExtTransfer.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_POExtTransfer.Delete(userID);
        }

        public void InitialDataLoad_POExtTransfer(int id)
        {
            db = new xOssContext();

            var a = (from t1 in db.Mkt_POExtTransfer
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     where t1.Active == true && t1.Raw_ItemFK != 1
                     select new VmMkt_POExtTransfer
                     {
                         Mkt_POExtTransfer = t1,
                         Raw_Item = t2,
                         Common_Unit = t3
                     }).Where(x => x.Mkt_POExtTransfer.Mkt_POFK == id).AsEnumerable();

            var b = (from t1 in db.Mkt_POExtTransfer
                     join t3 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t3.ID
                     join t2 in db.Raw_Item on t3.Raw_ItemFK equals t2.ID
                     join t4 in db.Mkt_PO on t3.Mkt_POFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Unit on t1.Common_UnitFK equals t7.ID
                     join t0 in db.Mkt_BOMSlave on t3.Mkt_BOMSlaveFK equals t0.ID
                     join t8 in db.Mkt_YarnCalculation on t0.Mkt_YarnCalculationFK equals t8.ID
                     join t9 in db.Mkt_YarnType on t8.Mkt_YarnTypeFk equals t9.ID
                     join t10 in db.Prod_MachineDIA on t1.Prod_MachineDIAFk equals t10.ID

                     where t1.Active == true && t1.Mkt_POSlaveFK != 1
                     select new VmMkt_POExtTransfer
                     {
                         Mkt_POExtTransfer = t1,
                         Raw_Item = t2,
                         Mkt_POSlave = t3,
                         Mkt_BOM = t5,
                         Common_TheOrder = t6,
                         Common_Unit = t7,
                         Mkt_YarnCalculation = t8,
                         Mkt_YarnType = t9,
                         Prod_MachineDIA = t10
                     }).Where(x => x.Mkt_POExtTransfer.Mkt_POFK == id).ToList();

           

            //var c = (from t1 in db.Mkt_POExtTransfer              
            //         join t2 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFK equals t2.ID
            //         join t3 in db.Mkt_BOM on t2.Mkt_BOMFk equals t3.ID
            //         join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
            //         join t5 in db.Common_Unit on t1.Common_UnitFK equals t5.ID
            //         where t1.Active == true && t1.Prod_TransitionItemInventoryFK !=1
            //         select new VmMkt_POExtTransfer
            //         {
            //             Mkt_POExtTransfer = t1,
            //             Prod_TransitionItemInventory=t2,
            //             Mkt_BOM = t3,
            //             Common_TheOrder = t4,
            //             Common_Unit = t5
            //         }).Where(x => x.Mkt_POExtTransfer.Mkt_POFK == id).AsEnumerable();

            var v =a.Union(b);
            var y = a.Union(b);//.Union(c);
            this.DataList = y;
        }
    }
}