﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Store
{
    public class VmMktPOSlave_Consumption
    {
        private xOssContext db;
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public Mkt_POSlave_Consumption Mkt_POSlave_Consumption { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public decimal? Delivery { get; set; }
        public decimal? PreviousDelivery { get; set; }
        public IEnumerable<VmMktPOSlave_Consumption> DataList { get; set; }
        public IEnumerable<VmMktPO_Slave> DataList2 { get; set; }
        public decimal? TotalReturn { get; set; }
        public string Requisition { get; private set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime RequisitionDate { get; private set; }
        public string Supervisor { get; private set; }
        //------------------Delivery on Requisition--------------------//
        public void InitialDataLoadDelivery()
        {
            db = new xOssContext();
            var BOM = (from t1 in db.Mkt_POSlave_Consumption
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                     where t1.Active == true &&  t1.IsReturn == false
                     select new VmMktPOSlave_Consumption
                     {
                         Requisition = t1.Requisition,
                         RequisitionDate = t1.Date,
                         Supervisor = t1.Supervisor,
                         Mkt_BOM = t4
                     }).Distinct().AsEnumerable();
            var REQ = (from t1 in db.Mkt_POSlave_Consumption
                       join t2 in db.Prod_Requisition_Slave on t1.Prod_Requisition_SlaveFK equals t2.ID
                       join t3 in db.Prod_Requisition on t2.Prod_RequisitionFK equals t3.ID
                       join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                       where t1.Active == true && t1.IsReturn == false
                       select new VmMktPOSlave_Consumption
                       {
                           Requisition = t1.Requisition,
                           RequisitionDate = t1.Date,
                           Supervisor = t1.Supervisor,
                           Mkt_BOM = t4
                       }).Distinct().AsEnumerable();
            this.DataList = BOM.Union(REQ);

        }      
        public VmMktPOSlave_Consumption SelectSingleJoined(string requisition)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Consumption
                     //join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     //join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                     //join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                     //join t5 in db.Mkt_BOMSlave on t4.ID equals t5.Mkt_BOMFK
                     where t1.Requisition== requisition
                     select new VmMktPOSlave_Consumption
                     {
                         Requisition = t1.Requisition,
                         RequisitionDate = t1.Date,
                         Supervisor = t1.Supervisor,
                         //Mkt_BOM = t4,                       
                     }).FirstOrDefault();
            return v;
        }
        //public VmMktPOSlave_Consumption SelectSingleJoinedREQ(string requisition)
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Mkt_POSlave_Consumption
        //             join t2 in db.Prod_Requisition_Slave on t1.Prod_Requisition_SlaveFK equals t2.ID
        //             join t3 in db.Prod_Requisition on t2.Prod_RequisitionFK equals t3.ID
        //             join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
        //             join t5 in db.Mkt_BOMSlave on t4.ID equals t5.Mkt_BOMFK
        //             where t1.Requisition == requisition
        //             select new VmMktPOSlave_Consumption
        //             {
        //                 Requisition = t1.Requisition,
        //                 RequisitionDate = t1.Date,
        //                 Supervisor = t1.Supervisor,
        //                 Mkt_BOM = t4,
        //             }).FirstOrDefault();
        //    return v;
        //}
        internal void GetDeliveryItem(string requisition)
        {

            db = new xOssContext();
            var PO = (from t1 in db.Mkt_POSlave_Consumption
                      join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID                                  
                      join t6 in db.Raw_Item on t2.Raw_ItemFK equals t6.ID
                      where t1.Requisition == requisition && t1.IsReturn == false
                     select new VmMktPOSlave_Consumption
                     {
                         Mkt_POSlave_Consumption = t1,
                         Mkt_POSlave = t2,
                         Raw_Item = t6
                     }).OrderByDescending(x => x.Mkt_POSlave_Consumption.ID).AsEnumerable();
            var REQ = (from t1 in db.Mkt_POSlave_Consumption
                       join t2 in db.Prod_Requisition_Slave on t1.Prod_Requisition_SlaveFK equals t2.ID                                          
                       join t6 in db.Raw_Item on t2.Raw_ItemFK equals t6.ID
                       where t1.Requisition == requisition && t1.IsReturn == false
                     select new VmMktPOSlave_Consumption
                     {
                         Mkt_POSlave_Consumption = t1,
                         Prod_Requisition_Slave = t2,
                         Raw_Item = t6
                     }).OrderByDescending(x => x.Mkt_POSlave_Consumption.ID).AsEnumerable();

            this.DataList = PO.Union(REQ);
        }
        internal void GetDeliveryItemREQ(string requisition)
        {

            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave_Consumption
                     join t2 in db.Prod_Requisition_Slave on t1.Prod_Requisition_SlaveFK equals t2.ID
                     join t3 in db.Prod_Requisition on t2.Prod_RequisitionFK equals t3.ID
                     join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Mkt_BOMSlave on t4.ID equals t5.Mkt_BOMFK
                     join t6 in db.Raw_Item on t5.Raw_ItemFK equals t6.ID
                     where t1.Requisition == requisition && t1.IsReturn == false
                     select new VmMktPOSlave_Consumption
                     {
                         Mkt_POSlave_Consumption = t1,
                         Prod_Requisition_Slave = t2,
                         Raw_Item = t6
                     }).OrderByDescending(x => x.Mkt_POSlave_Consumption.ID).AsEnumerable();
            this.DataList = a;
        }
        internal void GetDeliveryReturnItem(string requisition)
        {

            db = new xOssContext();
            db = new xOssContext();
            var PO = (from t1 in db.Mkt_POSlave_Consumption
                      join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                      join t6 in db.Raw_Item on t2.Raw_ItemFK equals t6.ID
                      where t1.Requisition == requisition && t1.IsReturn == true
                      select new VmMktPOSlave_Consumption
                      {
                          Mkt_POSlave_Consumption = t1,
                          Mkt_POSlave = t2,
                          Raw_Item = t6
                      }).OrderByDescending(x => x.Mkt_POSlave_Consumption.ID).AsEnumerable();
            var REQ = (from t1 in db.Mkt_POSlave_Consumption
                       join t2 in db.Prod_Requisition_Slave on t1.Prod_Requisition_SlaveFK equals t2.ID
                       join t6 in db.Raw_Item on t2.Raw_ItemFK equals t6.ID
                       where t1.Requisition == requisition && t1.IsReturn == true
                       select new VmMktPOSlave_Consumption
                       {
                           Mkt_POSlave_Consumption = t1,
                           Prod_Requisition_Slave = t2,
                           Raw_Item = t6
                       }).OrderByDescending(x => x.Mkt_POSlave_Consumption.ID).AsEnumerable();

            this.DataList = PO.Union(REQ);

        }
        //------------------Return From Production--------------------//
        public void InitialDataLoadDeliveryReturn()
        {
            db = new xOssContext();
            var BOM = (from t1 in db.Mkt_POSlave_Consumption
                       join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                       join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                       join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                       where t1.Active == true && t1.IsReturn == true
                       select new VmMktPOSlave_Consumption
                       {
                           Requisition = t1.Requisition,
                           RequisitionDate = t1.Date,
                           Supervisor = t1.Supervisor,
                           Mkt_BOM = t4
                       }).Distinct().AsEnumerable();
            var REQ = (from t1 in db.Mkt_POSlave_Consumption
                       join t2 in db.Prod_Requisition_Slave on t1.Prod_Requisition_SlaveFK equals t2.ID
                       join t3 in db.Prod_Requisition on t2.Prod_RequisitionFK equals t3.ID
                       join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                       where t1.Active == true && t1.IsReturn == true
                       select new VmMktPOSlave_Consumption
                       {
                           Requisition = t1.Requisition,
                           RequisitionDate = t1.Date,
                           Supervisor = t1.Supervisor,
                           Mkt_BOM = t4
                       }).Distinct().AsEnumerable();
            this.DataList = BOM.Union(REQ);

        }
        public void SelectSingle(int? id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave_Consumption
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM on t3.Mkt_BOMCID equals t4.CID
                     where t1.Active == true
                     select new VmMktPOSlave_Consumption
                     {
                         Mkt_POSlave_Consumption = t1,
                         Mkt_POSlave = t2,
                         Mkt_PO = t3,
                         Mkt_BOM = t4
                     }).Where(c => c.Mkt_PO.ID == id).SingleOrDefault();
            this.Mkt_BOM = a.Mkt_BOM;
            this.Mkt_PO = a.Mkt_PO;
            this.Mkt_POSlave = a.Mkt_POSlave;
            this.Mkt_POSlave_Consumption = a.Mkt_POSlave_Consumption;

            InitialDataLoadDelivery();
        }

        //----------Mkt_POSlave--------/////

        public void InitialDataLoad_PoSlave(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID

                     select new VmMktPO_Slave
                     {
                         Mkt_POSlave = t1,
                         Raw_Item = t2,
                         Common_Unit = t3

                     }).Where(x => x.Mkt_POSlave_Consumption.Mkt_POSlaveFK == id).AsEnumerable();
            List<VmMktPO_Slave> tempVmMktPO_Slave = new List<VmMktPO_Slave>();
            foreach (VmMktPO_Slave x in v)
            {
                x.PreviousDelivery = GetPreviousDelivery(x.Mkt_POSlave.ID);
                tempVmMktPO_Slave.Add(x);

            }
            this.DataList2 = tempVmMktPO_Slave;
        }

        public decimal? GetPreviousDelivery(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POSlaveFK == id
                     select (t1.Quantity)).Sum();
            return v;
        }

        //---------POSlave_Consumption--------/////////////
        public int AddPOSlave_Delivery(int userID)
        {

            Mkt_POSlave_Consumption.AddReady(userID);
            return Mkt_POSlave_Consumption.Add();
        }
        public void VmPOSlave_DeliveryCreate(int userID, IEnumerable<VmMktPO_Slave> DataList2)
        {

            foreach (VmMktPO_Slave x in DataList2)
            {
                Mkt_POSlave_Consumption.Quantity = x.Receiving;
                Mkt_POSlave_Consumption.Mkt_POSlaveFK = x.Mkt_POSlave.ID;
                Mkt_POSlave_Consumption.Date = this.Mkt_POSlave_Consumption.Date;
                AddPOSlave_Delivery(userID);
                //if (Mkt_POSlave.TotalRequired >= (Mkt_POSlave_Receiving.Quantity + x.PreviousReceived))
                //{

                //}

            }
        }


        public bool Edit(int userID)
        {
            return Mkt_POSlave_Consumption.Edit(userID);
        }

        public int Add(int userID)
        {

            Mkt_POSlave_Consumption.AddReady(userID);
            return Mkt_POSlave_Consumption.Add();
        }
      
        //Delete
        public bool Delate(int userID)
        {
            return Mkt_POSlave_Consumption.Delete(userID);
        }
    }
}