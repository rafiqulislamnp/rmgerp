﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Store
{
    public class VmStore_InterTransfer
    {
        private xOssContext db;
    
        public IEnumerable<VmStore_InterTransfer> DataList { get; set; }
        public User_Department FromUser_Department { get; set; }
        public User_Department ToUser_Department { get; set; }
     
        public Raw_InternaleTransfer Raw_InternaleTransfer { get; set;}


 


        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_InternaleTransfer
                     join t2 in db.User_Department on t1.FromUser_DeptFK equals t2.ID
                     join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                     select new VmStore_InterTransfer
                     {
                         Raw_InternaleTransfer = t1,
                         FromUser_Department = t2,
                         ToUser_Department = t3
                     }).Where(x => x.Raw_InternaleTransfer.Active == true).OrderByDescending(x => x.Raw_InternaleTransfer.ID).AsEnumerable();

       
            this.DataList = v;

        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_InternaleTransfer
                     join t2 in db.User_Department on t1.FromUser_DeptFK equals t2.ID
                     join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID
                     select new VmStore_InterTransfer
                     {
                         Raw_InternaleTransfer =t1,
                         FromUser_Department = t2,
                         ToUser_Department = t3

                     }).SingleOrDefault(c => c.Raw_InternaleTransfer.ID == id);
            this.Raw_InternaleTransfer = v.Raw_InternaleTransfer;
            this.FromUser_Department = v.FromUser_Department;
            this.ToUser_Department = v.ToUser_Department;

        }


        public int Add(int userID)
        {
            Raw_InternaleTransfer.AddReady(userID);
            int xcx = Raw_InternaleTransfer.Add();
            return xcx;
        }

        public bool Edit(int userID)
        {
            return Raw_InternaleTransfer.Edit(userID);
        }

        public bool StoreInterTransferAcknowledgement(int userID)
        {
            return Raw_InternaleTransfer.Edit(userID);
        }
    }
}