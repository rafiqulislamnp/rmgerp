﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Store;
using Oss.Romo.Models;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Store
{

    public class VmProd_MachineDIA
    {
      
        private xOssContext db;
        public Prod_MachineDIA Prod_MachineDIA { get; set; }
     
        public VmControllerHelper VmControllerHelper { get; set; }
        public IEnumerable<VmProd_MachineDIA> DataList { get; set; }

     
     
        public VmProd_MachineDIA()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from rc in db.Prod_MachineDIA
                     where rc.Active == true
                     select new VmProd_MachineDIA
                     {
                         Prod_MachineDIA = rc
                     }).Where(x=>x.Prod_MachineDIA.Active==true).OrderByDescending(x => x.Prod_MachineDIA.ID).AsEnumerable();
            this.DataList = v;

        }

        public void LoadDataFromDatabase()
        {
            db = new xOssContext();
            var v = (from rc in db.Prod_MachineDIA                   
                     select new VmProd_MachineDIA
                     {
                         Prod_MachineDIA = rc
                     }).AsEnumerable();
            this.DataList = v;

        }
        //This  Code for edit
        public void SelectSingle(int id)
        {          
            db = new xOssContext();
            var v = (from rc in db.Prod_MachineDIA
                     select new VmProd_MachineDIA
                     {
                         Prod_MachineDIA = rc
                     }).Where(c=>c.Prod_MachineDIA.ID == id).SingleOrDefault();
            this.Prod_MachineDIA = v.Prod_MachineDIA;

        }
        
        //Create 

        public int Add(int userID)
        {
            Prod_MachineDIA.AddReady(userID);
            return Prod_MachineDIA.Add();
        }
        //Update
        public bool Edit(int userID)
        {
            return Prod_MachineDIA.Edit(userID);
        }
        //Delete
        public bool Delete(int userID)
        {
            return Prod_MachineDIA.Delete(userID);
        }
    }
}