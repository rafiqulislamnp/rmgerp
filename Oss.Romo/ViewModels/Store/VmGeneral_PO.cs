﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models.User;

namespace Oss.Romo.ViewModels.Store
{
    public class VmGeneral_PO
    {
        private xOssContext db;
        [Display(Name = "T & C Title")]
        public int StatusId { get; set; }
        public IEnumerable<VmMkt_PO> DataList { get; set; }

        public Common_Currency Common_Currency { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public VmGeneral_POSlave VmGeneral_POSlave { get; set; }
        public VmMkt_PO VmMkt_PO { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public User_User User_User { get; set; }
        public int DepartmentID { get; set; }
        public List<ReportDocType> OrderReportDoc { get; set; }

        public VmGeneral_PO()
        {
            db = new xOssContext();
            

        }

        public void InitialDataLoad()
        {
            var v = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder on t2.Common_TheOrderFk equals t6.ID
                     join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
                     where Mkt_PO.Active == true
                     select new VmMkt_PO
                     {
                         Mkt_PO = Mkt_PO,
                         Common_Supplier= Common_Supplier,
                         Mkt_BOM = t2,
                         Common_TheOrder = t6,
                         User_User = User_User,
                         Total_Value = (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired).Sum() == 0?0: (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired).Sum()
                     }).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();
            if (this.User_User.User_DepartmentFK != 3)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);

            }
            else
            {

                this.DataList = v;

                //var bb = list.ToList();
            }
            //this.DataList = v;

        }

        public decimal? TotlaPrice(int id)
        {
            db = new xOssContext();
            var a = (from Mkt_POSlave in db.Mkt_POSlave
                where Mkt_POSlave.Mkt_POFK == id && Mkt_POSlave.Active == true
                select (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)).Sum();
            if (a == null)
            {
                a = 0;
            }
            return a;

        }
        public void InitialDataLoadGeneral()
        {
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     where t1.Mkt_BOMFK == null
                     select new VmMkt_PO
                     {
                         Mkt_PO = t1,
                         Common_Supplier = t2
                     }).Where(x => x.Mkt_PO.Active == true).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();
            this.DataList = v;

        }

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from rc in db.Mkt_PO
                     select new VmMkt_PO
                     {
                         Mkt_PO = rc
                     }).Where(c => c.Mkt_PO.ID == iD).SingleOrDefault();
            this.Mkt_PO = v.Mkt_PO;

        }

        public VmMkt_PO SelectSingleJoined(int iD)
        {
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in db.Common_Currency on t1.Common_CurrencyFK equals t3.ID
                     select new VmMkt_PO
                     {
                         Mkt_PO = t1,
                         Common_Supplier = t2,
                         Common_Currency = t3
                     }).FirstOrDefault();
            return v;
        }

        public int Add(int userID)
        {
            Mkt_PO.AddReady(userID);
            return Mkt_PO.Add();
        }

        public bool Edit(int userID)
        {
            return Mkt_PO.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_PO.Delete(userID);
        }

        internal void POInventoryReportDocLoad(int id)
        {
            db = new xOssContext();

            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Mkt_PO in db.Mkt_PO on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Raw_Item in db.Raw_Item on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID

                     where Mkt_PO.ID == id

                     select new ReportDocType
                     {
                         HeaderLeft1 = Mkt_PO.CID,
                         HeaderLeft2=Mkt_PO.Date.ToString(),
                         Body1 = Raw_Item.Name,
                         Body2 = Mkt_POSlave.TotalRequired.ToString(),
                         Body3 = Mkt_POSlave.ID.ToString(),
                         Body8 = Common_Unit.Name

                     }).ToList();
            foreach (ReportDocType r in a)
            {
                int poSlaveID = 0;
                decimal balance = 0;
                decimal body2 = 0;
                decimal.TryParse(r.Body2, out body2);
                int.TryParse(r.Body3, out poSlaveID);
                decimal recived, returned;
                recived = GetTotalReceive(poSlaveID);
                returned = GetTotalReturn(poSlaveID);
                r.Body4 = recived.ToString("F");
                r.Body5 = returned.ToString("F");
                r.Body6 = (recived - returned).ToString("F");
                decimal body6 = 0;
                decimal.TryParse(r.Body6, out body6);
                balance = body2 - body6;
                r.Body10 = balance.ToString("F");
                r.HeaderLeft2 = r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
            }

            this.OrderReportDoc = a.ToList();
        }


        internal decimal GetTotalReceive(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POSlaveFK == id
                     && t1.IsReturn == false
                     select t1.Quantity).Sum().GetValueOrDefault();
            return x;

        }

        internal decimal GetTotalReturn(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POSlaveFK == id
                     && t1.IsReturn == true
                     select t1.Quantity).Sum().GetValueOrDefault();
            return x;
        }
    }
}