﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Store
{
    public class VmMktPO_Slave
    {
        public decimal MaxAllowed { get; set; }
        private xOssContext db;
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Prod_Requisition Prod_Requisition { get; set; }
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public VmProd_Requisition_Slave VmProd_Requisition_Slave { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Mkt_POSlave_Receiving Mkt_POSlave_Receiving { get; set; }
        public User_Department User_Department { get; set; }
        public Mkt_POSlave_Consumption Mkt_POSlave_Consumption { get; set; }
        public IEnumerable<VmMktPO_Slave> DataList { get; set; }
        public IEnumerable<VmProd_Requisition_Slave> DataList1 { get; set; }
        public string StoreName { get; set; }
        public Mkt_POExtTransfer Mkt_POExtTransfer { get; set; }
        public int RawSubCatID { get; set; }
        public int RawCatID { get; set; }
        public Raw_InternaleTransfer Raw_InternaleTransfer { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        //[Range (0.01, 5.00)]
        public decimal? Receiving { get; set; }
        public decimal? PreviousReceived { get; set; }
        public decimal? Delivery { get; set; }
        public decimal? PreviousDelivery { get; set; }
        public decimal? LineTotal { get; set; }
        public decimal? Required { get; set; }
        public decimal? PreviousReturned { get; set; }
        public decimal? TotalStock { get; set; }
        public decimal? CurrentStock { get; set; }
        public decimal? StockLoss { get; set; }
        public decimal? PreviousLoss { get; set; }
        public decimal? CurrentDelivery { get; set; }
        public decimal? PreviousDeliveryReturn { get; set; }
        public string FabricColor { get; set; }
        public bool IsFinishFabric { get; set; }
        
        public Prod_TransitionItemInventory Prod_TransitionItemInventory { get; set; }

        public IEnumerable<VmMktPO_Slave> DataList2 { get; set; }
        
        public void InitialDataLoad_PoSlave()
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     from t2 in db.Mkt_PO
                     where t1.Mkt_POFK == t2.ID
                     //from t3 in db.Mkt_POSlave_Receiving
                     //where t3.Mkt_POSlaveFK == t1.ID
                     where Mkt_POSlave.Active == true
                     select new VmMktPO_Slave
                     {
                         Mkt_POSlave = t1,
                         Mkt_PO = t2
                         //Mkt_POSlave_Receiving = t3
                     }).Where(x => x.Mkt_POSlave.Active == true).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            this.DataList2 = v;

        }
        public int AddMkt_POSlave(int userID)
        {
            Mkt_POSlave.AddReady(userID);
            return Mkt_POSlave.Add();
        }
        internal void GetPOSlaveWithoutOrder(int poID)
        {
            db = new xOssContext();
           
            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID

                     join t1 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t1.ID
                     join t2 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t2.ID

                     where Mkt_POSlave.Active == true
                     && Mkt_POSlave.Mkt_POFK == poID
                     select new VmMktPO_Slave
                     {
                         Common_Currency = t2,
                         Mkt_POSlave = Mkt_POSlave,
                         Common_Unit = Common_Unit,
                         Mkt_PO = Mkt_PO,
                         Raw_Item = Raw_Item,
                         LineTotal = (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)
                     }).Where(x => x.Mkt_POSlave.Active == true).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            this.Mkt_POSlave = new Mkt_POSlave();
            this.DataList = a;

        }
        internal void GetPOSlave(int poID,int supplierid , int bomID)
        {
            db = new xOssContext();
            var check = (from mkt_POSlave in db.Mkt_POSlave
                         where mkt_POSlave.Mkt_POFK == poID
                         select (1)
                          ).Count();

            var check2 = (from t1 in db.Mkt_BOMSlave
                          join t2 in db.Mkt_BOM
                          on t1.Mkt_BOMFK equals t2.ID
                          where t1.Mkt_BOMFK == bomID && t1.Common_SupplierFK == supplierid                   
                          where !(from tt in db.Mkt_POSlave join po in db.Mkt_PO on tt.Mkt_POFK equals po.ID where tt.Mkt_POFK == poID && po.Mkt_BOMFK == bomID && po.Common_SupplierFK == supplierid select tt.Mkt_BOMSlaveFK).Contains(t1.ID)
                          select t1.ID).Count();

            //.Except((from t3 in db.Mkt_POSlave where t3.Mkt_POFK == poID select t3.Mkt_BOMSlaveFK.Value))

            if (check == 0)
            {
                GetSuggestedPOFromMktPo(poID, bomID);
            }
            if (check2 > 0)
            {
                GetSuggestedPOFromMktPo(poID, bomID);
            }



            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                   
                     join t1 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t1.ID
                     join t2 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t2.ID

                     where Mkt_POSlave.Active == true
                     && Mkt_POSlave.Mkt_POFK == poID
                     select new VmMktPO_Slave
                     {
                         Common_Currency = t2,
                         Mkt_POSlave = Mkt_POSlave,
                         Common_Unit = Common_Unit,
                         Mkt_PO = Mkt_PO,
                         Raw_Item = Raw_Item,
                         LineTotal = (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)
                     }).Where(x => x.Mkt_POSlave.Active == true).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            this.Mkt_POSlave = new Mkt_POSlave();
            this.DataList = a;
            
        }

        internal void GetPODetails(int poID)
        {
            db = new xOssContext();
           
            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID

                     join t1 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t1.ID
                     join t2 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t2.ID

                     where Mkt_POSlave.Active == true
                     && Mkt_POSlave.Mkt_POFK == poID
                     select new VmMktPO_Slave
                     {
                         Common_Currency = t2,
                         Mkt_POSlave = Mkt_POSlave,
                         Common_Unit = Common_Unit,
                         Mkt_PO = Mkt_PO,
                         Raw_Item = Raw_Item,
                         LineTotal = (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)
                     }).Where(x => x.Mkt_POSlave.Active == true).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            this.Mkt_POSlave = new Mkt_POSlave();
            this.DataList = a;

        }
        internal void GetPOStepJobs(int poID, int supplierid, int bomID)
        {
            db = new xOssContext();
            var check = (from mkt_POSlave in db.Mkt_POSlave
                         where mkt_POSlave.Mkt_POFK == poID
                         select (1)
                          ).Count();

            var check2 = (from t1 in db.Mkt_BOMStepJobs
                          join t2 in db.Mkt_BOM
                          on t1.Mkt_BOMFK equals t2.ID
                          where t1.Mkt_BOMFK == bomID && t1.Common_SupplierFK == supplierid
                          where !(from tt in db.Mkt_POSlave join po in db.Mkt_PO on tt.Mkt_POFK equals po.ID where tt.Mkt_POFK == poID && po.Mkt_BOMFK == bomID && po.Common_SupplierFK == supplierid select tt.Mkt_BOMSlaveFK).Contains(t1.ID)
                          select t1.ID).Count();

            //.Except((from t3 in db.Mkt_POSlave where t3.Mkt_POFK == poID select t3.Mkt_BOMSlaveFK.Value))

            if (check == 0)
            {
                GetSuggestedPOFromMktBobStepJobs(poID, bomID);
            }
            if (check2 > 0)
            {
                GetSuggestedPOFromMktBobStepJobs(poID, bomID);
            }



            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID

                     join t1 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t1.ID
                     join t2 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t2.ID

                     where Mkt_POSlave.Active == true
                     && Mkt_POSlave.Mkt_POFK == poID
                     select new VmMktPO_Slave
                     {
                         Common_Currency = t2,
                         Mkt_POSlave = Mkt_POSlave,
                         Common_Unit = Common_Unit,
                         Mkt_PO = Mkt_PO,
                         Raw_Item = Raw_Item,
                         LineTotal = (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)
                     }).Where(x => x.Mkt_POSlave.Active == true).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            this.Mkt_POSlave = new Mkt_POSlave();
            this.DataList = a;

        }
        internal void GetPOSlaveGEN(int id)
        {
            db = new xOssContext();
            var check = (from t1 in db.Prod_Requisition_Slave
                         where t1.Prod_RequisitionFK == id
                         select (1)
                          ).Count();


            if (check == 0)
            {
                GetSuggestedPOFromREQ(id);
            }
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                     join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                     where t1.Active == true
                     && t1.Mkt_POFK == id
                     select new VmMktPO_Slave
                     {
                         Mkt_POSlave = t1,
                         Common_Unit = t2,
                         Raw_Item = t3,
                         LineTotal = t1.TotalRequired * t1.Price
                     }).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            this.Mkt_POSlave = new Mkt_POSlave();
            this.DataList = a;
        }

        internal void GetPOSlaveByPO(int id)
        {
            db = new xOssContext();
            
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                     join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                     join t4 in db.Prod_Requisition_Slave on t1.RequisitionSlaveFK equals t4.ID
                     join t5 in db.Prod_Requisition on t4.Prod_RequisitionFK equals t5.ID

                     where t1.Active == true
                     && t1.Mkt_POFK == id
                     select new VmMktPO_Slave
                     {
                         Mkt_POSlave = t1,
                         Common_Unit = t2,
                         Raw_Item = t3,
                         Prod_Requisition = t5,
                         LineTotal = t1.TotalRequired * t1.Price,
                         
                     }).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            this.Mkt_POSlave = new Mkt_POSlave();
            this.DataList = a;
        }
        internal void GetSuggestedPOFromREQ(int id)
        {

            db = new xOssContext();
            var data1 = (from t1 in db.Prod_Requisition_Slave
                         join t2 in db.Prod_Requisition
                        on t1.Prod_RequisitionFK equals t2.ID 
                        join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID                                             
                         where
                         t2.ID == id                                              
                         && t1.Active == true
                         select new
                         {
                             Raw_ItemFK=t1.Raw_ItemFK,
                             Prod_RequisitionFK = id,
                             RequiredQuantity = t1.TotalRequired,
                             Description = t1.Description,
                         }).AsEnumerable();
           
            foreach (var b in data1)
            {
                Mkt_POSlave x = new Mkt_POSlave();
                x.Raw_ItemFK = b.Raw_ItemFK;
                x.Description = b.Description;
                x.TotalRequired = b.RequiredQuantity;
                //x.Common_UnitFK = b.Common_UnitFK;
                //x.Price = b.Price;
                x.Mkt_POFK = b.Prod_RequisitionFK;
                x.AddReady(0);
                x.Add();
            }
           
        }
        internal void GetSuggestedPOFromMktPo(int id,int bomID)
        {
            VmMkt_BOM vmMkt_BOM = new VmMkt_BOM();
            string yarnValue = vmMkt_BOM.GetYarnValue(bomID.ToString());
            decimal yarnVal = Convert.ToDecimal(yarnValue);
            string lycraValue = vmMkt_BOM.GetLycraValue(bomID.ToString());
            decimal lycraVal = Convert.ToDecimal(lycraValue);
            string yarnConsumption = vmMkt_BOM.GetAverageConsumption(bomID.ToString());
            decimal YarnCon = Convert.ToDecimal(yarnConsumption);

            var data1 = (from mkt_BOMSlave in db.Mkt_BOMSlave
                         join mkt_BOM in db.Mkt_BOM
                        on mkt_BOMSlave.Mkt_BOMFK equals mkt_BOM.ID
                         join mkt_PO in db.Mkt_PO
                         on mkt_BOM.ID equals mkt_PO.Mkt_BOMFK
                         join cc in db.Common_Currency
                         on mkt_PO.Common_CurrencyFK equals cc.ID
                         join ss in db.Common_Supplier
                         on mkt_PO.Common_SupplierFK equals ss.ID
                         where
                         mkt_PO.ID == id
                         && mkt_BOMSlave.Common_SupplierFK == mkt_PO.Common_SupplierFK
                        && !(from tt in db.Mkt_POSlave where tt.Active == true select tt.Mkt_BOMSlaveFK).Contains(mkt_BOMSlave.ID)
                      // && !(from x in db.Mkt_PO where x.Active ==true select x.Common_SupplierFK).Contains(ss.ID)
                         && mkt_BOMSlave.Active == true
                         select new
                         {
                             //OrderQ = mkt_BOM.QPack * mkt_BOM.Quantity,
                             Common_Currency= cc,
                             Raw_ItemFK = mkt_BOMSlave.Raw_ItemFK,
                             Consumption = mkt_BOMSlave.Consumption,// mkt_BOMSlave.Raw_ItemFK == 1045 ? YarnCon : (mkt_BOMSlave.Raw_ItemFK == 5331 ? YarnCon : mkt_BOMSlave.Consumption),
                             //RequiredQ = mkt_BOM.QPack * mkt_BOM.Quantity * mkt_BOMSlave.Consumption,
                             Common_UnitFK = mkt_BOMSlave.Common_UnitFK,
                             Price = mkt_BOMSlave.Price,
                             Mkt_POFK = id,
                             RequiredQuantity = mkt_BOMSlave.RequiredQuantity,// mkt_BOMSlave.Raw_ItemFK == 1045 ? yarnVal : (mkt_BOMSlave.Raw_ItemFK == 5331 ? lycraVal : mkt_BOMSlave.RequiredQuantity),
                             Description = mkt_BOMSlave.Description,

                             Mkt_BOMSlaveFK = mkt_BOMSlave.ID

                         }).AsEnumerable();
            //this.DataList = data;
            var data2 = (from mkt_BOMSlave in db.Mkt_BOMSlave

                         join mkt_BOM in db.Mkt_BOM
                        on mkt_BOMSlave.Mkt_BOMFK equals mkt_BOM.ID
                         join mkt_PO in db.Mkt_PO
                         on mkt_BOM.ID equals mkt_PO.Mkt_BOMFK
                         join mkt_BOMSlaveFabric in db.Mkt_BOMSlaveFabric
                         on mkt_BOMSlave.ID equals mkt_BOMSlaveFabric.Mkt_BOMSlaveFK
                         join cc in db.Common_Currency
                         on mkt_PO.Common_CurrencyFK equals cc.ID
                         where mkt_PO.ID == id
                         && mkt_BOMSlaveFabric.Common_SupplierFK == mkt_PO.Common_SupplierFK
                         && mkt_BOMSlaveFabric.Active == true
                         select new
                         {
                             //OrderQ = mkt_BOM.Quantity * mkt_BOM.QPack,
                             Raw_ItemFK = mkt_BOMSlaveFabric.Raw_ItemFK,
                             Common_Currency= cc,
                             Consumption = mkt_BOMSlaveFabric.Consumption,// mkt_BOMSlave.Raw_ItemFK == 1045 ? YarnCon : (mkt_BOMSlave.Raw_ItemFK == 5331 ? YarnCon : mkt_BOMSlaveFabric.Consumption),
                             //RequiredQ = mkt_BOM.QPack * mkt_BOM.Quantity * mkt_BOMSlaveFabric.Consumption,
                             Common_UnitFK = mkt_BOMSlaveFabric.Common_UnitFK,
                             Price = mkt_BOMSlaveFabric.Price,
                             Mkt_POFK = id,
                             RequiredQuantity = mkt_BOMSlaveFabric.RequiredQuantity,// mkt_BOMSlave.Raw_ItemFK == 1045 ? yarnVal : (mkt_BOMSlave.Raw_ItemFK == 5331 ? lycraVal : mkt_BOMSlaveFabric.RequiredQuantity),
                             Description = mkt_BOMSlaveFabric.Description,
                             Mkt_BOMSlaveFK = mkt_BOMSlave.ID
                         }).AsEnumerable();           
            foreach (var b in data1)
            {
                Mkt_POSlave x = new Mkt_POSlave();
                x.Raw_ItemFK = b.Raw_ItemFK;
                x.Description = b.Description;
                x.Consumption = b.Consumption;
                x.TotalRequired = b.RequiredQuantity;
                x.Common_UnitFK = b.Common_UnitFK;
                x.Price = b.Price;
                x.Mkt_POFK = b.Mkt_POFK;

                x.Mkt_BOMSlaveFK = b.Mkt_BOMSlaveFK;

                x.AddReady(0);
                x.Add();
            }
            foreach (var b in data2)
            {
                Mkt_POSlave x = new Mkt_POSlave();
                x.Raw_ItemFK = b.Raw_ItemFK;
                x.Description = b.Description;
                x.Consumption = b.Consumption;
                x.TotalRequired = b.RequiredQuantity;
                x.Common_UnitFK = b.Common_UnitFK;
                x.Price = b.Price;
                x.Mkt_POFK = b.Mkt_POFK;

                x.Mkt_BOMSlaveFK = b.Mkt_BOMSlaveFK;

                x.AddReady(0);
                x.Add();
            }
        }
        internal void GetSuggestedPOFromMktBobStepJobs(int id, int bomID)
        {
            VmMkt_BOM vmMkt_BOM = new VmMkt_BOM();
            string yarnValue = vmMkt_BOM.GetYarnValue(bomID.ToString());
            decimal yarnVal = Convert.ToDecimal(yarnValue);
            string lycraValue = vmMkt_BOM.GetLycraValue(bomID.ToString());
            decimal lycraVal = Convert.ToDecimal(lycraValue);
            string yarnConsumption = vmMkt_BOM.GetAverageConsumption(bomID.ToString());
            decimal YarnCon = Convert.ToDecimal(yarnConsumption);

            var data1 = (from mkt_BOMStepJobs in db.Mkt_BOMStepJobs
                         join mkt_BOM in db.Mkt_BOM
                        on mkt_BOMStepJobs.Mkt_BOMFK equals mkt_BOM.ID
                         join mkt_PO in db.Mkt_PO
                         on mkt_BOM.ID equals mkt_PO.Mkt_BOMFK
                         join cc in db.Common_Currency
                         on mkt_PO.Common_CurrencyFK equals cc.ID
                         join ss in db.Common_Supplier
                         on mkt_PO.Common_SupplierFK equals ss.ID
                         where
                         mkt_PO.ID == id
                         && mkt_BOMStepJobs.Common_SupplierFK == mkt_PO.Common_SupplierFK
                        && !(from tt in db.Mkt_POSlave where tt.Active == true select tt.Mkt_BOMSlaveFK).Contains(mkt_BOMStepJobs.ID)
                         // && !(from x in db.Mkt_PO where x.Active ==true select x.Common_SupplierFK).Contains(ss.ID)
                         && mkt_BOMStepJobs.Active == true
                         select new
                         {
                             //OrderQ = mkt_BOM.QPack * mkt_BOM.Quantity,
                             Common_Currency = cc,
                             Raw_ItemFK = mkt_BOMStepJobs.Raw_ItemFK,
                             Consumption = mkt_BOMStepJobs.Consumption,// mkt_BOMStepJobs.Raw_ItemFK == 1045 ? YarnCon : (mkt_BOMStepJobs.Raw_ItemFK == 5331 ? YarnCon : mkt_BOMStepJobs.Consumption),
                             //RequiredQ = mkt_BOM.QPack * mkt_BOM.Quantity * mkt_BOMSlave.Consumption,
                             Common_UnitFK = mkt_BOMStepJobs.Common_UnitFK,
                             Price = mkt_BOMStepJobs.Price,
                             Mkt_POFK = id,
                             RequiredQuantity = mkt_BOMStepJobs.RequiredQuantity,// mkt_BOMStepJobs.Raw_ItemFK == 1045 ? yarnVal : (mkt_BOMStepJobs.Raw_ItemFK == 5331 ? lycraVal : mkt_BOMStepJobs.RequiredQuantity),
                             Description = mkt_BOMStepJobs.Description,

                             Mkt_BOMSlaveFK = mkt_BOMStepJobs.ID

                         }).AsEnumerable();
            //this.DataList = data;
            var data2 = (from mkt_BOMStepJobs in db.Mkt_BOMStepJobs

                         join mkt_BOM in db.Mkt_BOM
                        on mkt_BOMStepJobs.Mkt_BOMFK equals mkt_BOM.ID
                         join mkt_PO in db.Mkt_PO
                         on mkt_BOM.ID equals mkt_PO.Mkt_BOMFK
                         join mkt_BOMSlaveFabric in db.Mkt_BOMSlaveFabric
                         on mkt_BOMStepJobs.ID equals mkt_BOMSlaveFabric.Mkt_BOMSlaveFK
                         join cc in db.Common_Currency
                         on mkt_PO.Common_CurrencyFK equals cc.ID
                         where mkt_PO.ID == id
                         && mkt_BOMSlaveFabric.Common_SupplierFK == mkt_PO.Common_SupplierFK
                         && mkt_BOMSlaveFabric.Active == true
                         select new
                         {
                             // OrderQ = mkt_BOM.Quantity * mkt_BOM.QPack,
                             Raw_ItemFK = mkt_BOMSlaveFabric.Raw_ItemFK,
                             Common_Currency = cc,
                             Consumption = mkt_BOMSlaveFabric.Consumption,// mkt_BOMStepJobs.Raw_ItemFK == 1045 ? YarnCon : (mkt_BOMStepJobs.Raw_ItemFK == 5331 ? YarnCon : mkt_BOMSlaveFabric.Consumption),
                             // RequiredQ = mkt_BOM.QPack * mkt_BOM.Quantity * mkt_BOMSlaveFabric.Consumption,
                             Common_UnitFK = mkt_BOMSlaveFabric.Common_UnitFK,
                             Price = mkt_BOMSlaveFabric.Price,
                             Mkt_POFK = id,
                             RequiredQuantity = mkt_BOMSlaveFabric.RequiredQuantity,// mkt_BOMStepJobs.Raw_ItemFK == 1045 ? yarnVal : (mkt_BOMStepJobs.Raw_ItemFK == 5331 ? lycraVal : mkt_BOMSlaveFabric.RequiredQuantity),
                             Description = mkt_BOMSlaveFabric.Description,

                             Mkt_BOMSlaveFK = mkt_BOMStepJobs.ID

                         }).AsEnumerable();


            foreach (var b in data1)
            {
                Mkt_POSlave x = new Mkt_POSlave();
                x.Raw_ItemFK = b.Raw_ItemFK;
                x.Description = b.Description;
                x.Consumption = b.Consumption;
                x.TotalRequired = b.RequiredQuantity;
                x.Common_UnitFK = b.Common_UnitFK;
                x.Price = b.Price;
                x.Mkt_POFK = b.Mkt_POFK;
                //set x.Mkt_BOMSlaveFK from step jobs table.
                x.Mkt_BOMSlaveFK = b.Mkt_BOMSlaveFK;

                x.AddReady(0);
                x.Add();
            }
            foreach (var b in data2)
            {
                Mkt_POSlave x = new Mkt_POSlave();
                x.Raw_ItemFK = b.Raw_ItemFK;
                x.Description = b.Description;
                x.Consumption = b.Consumption;
                x.TotalRequired = b.RequiredQuantity;
                x.Common_UnitFK = b.Common_UnitFK;
                x.Price = b.Price;
                x.Mkt_POFK = b.Mkt_POFK;
                // set x.Mkt_BOMSlaveFK from step jobs table.
                x.Mkt_BOMSlaveFK = b.Mkt_BOMSlaveFK;

                x.AddReady(0);
                x.Add();
            }
        }

        public int Add(int userID)
        {            
            Mkt_POSlave.AddReady(userID);
            return Mkt_POSlave.Add();

        }
        public bool Edit(int userID)
        {
            return Mkt_POSlave.Edit(userID);
        }

        public bool Dalete(int userID)
        {
            return Mkt_POSlave.Delete(userID);
        }
        public bool DaleteBulk(int userID, int poID)
        {
            db = new xOssContext();
            var v = from t1 in db.Mkt_POSlave where t1.Mkt_POFK == poID select t1.ID;

            foreach(int i in v)
            {
                this.Mkt_POSlave = new Mkt_POSlave();
                this.Mkt_POSlave.ID = i;
                this.Mkt_POSlave.Delete(userID);

            }
            return false;
        }
        public void SelectSingleSlaveOfCommercial(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Common_Unit
                     on t1.Common_UnitFK equals t2.ID
                     join t3 in db.Raw_Item
                     on t1.Raw_ItemFK equals t3.ID
                     // join t4 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t4.ID
                     // join t5 in db.Raw_Category on t4.Raw_CategoryFK equals t5.ID
                     join t6 in db.Mkt_PO
                     on t1.Mkt_POFK equals t6.ID
                     select new VmMktPO_Slave
                     {

                         Mkt_POSlave = t1,
                         Common_Unit = t2,
                         Raw_Item = t3,
                         //Raw_SubCategory = t4,
                         //Raw_Category = t5,
                         Mkt_PO = t6,
                     }).Where(V => V.Mkt_POSlave.ID == id).FirstOrDefault();



            //this.RawCatID = a.Raw_Category.ID;
            //this.RawSubCatID = a.Raw_SubCategory.ID;
            if (a != null)
            {
                this.Mkt_POSlave = a.Mkt_POSlave;
            }
        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Common_Unit
                     on t1.Common_UnitFK equals t2.ID
                     join t3 in db.Raw_Item
                     on t1.Raw_ItemFK equals t3.ID
                     //join t4 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t4.ID
                     //join t5 in db.Raw_Category on t4.Raw_CategoryFK equals t5.ID
                     join t6 in db.Mkt_PO
                     on t1.Mkt_POFK equals t6.ID
                     select new VmMktPO_Slave
                     {

                         Mkt_POSlave = t1,
                         Common_Unit = t2,
                         Raw_Item = t3,
                         //Raw_SubCategory = t4,
                         //Raw_Category = t5,
                         Mkt_PO = t6,
                     }).Where(V => V.Mkt_POSlave.Mkt_BOMSlaveFK == id).FirstOrDefault();



            //this.RawCatID = a.Raw_Category.ID;
            //this.RawSubCatID = a.Raw_SubCategory.ID;
            if (a != null)
            {
                this.Mkt_POSlave = a.Mkt_POSlave;
            }
        }

        internal void GetPOSlaveFromRequisition(int poID)
        {
            db = new xOssContext();

            var a = (from t1 in db.Mkt_POSlave
                join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
                where t1.Mkt_POFK == poID 
                      && t1.Active == true
                     select new VmMktPO_Slave
                {
                    Mkt_POSlave = t1,
                    Common_Unit = t2,
                    Raw_Item = t3,
                    Mkt_PO = t4,
                    LineTotal = (t1.TotalRequired * t1.Price)
                }).Where(x => x.Mkt_POSlave.Active == true).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            this.Mkt_POSlave = new Mkt_POSlave();
            this.DataList2 = a;

        }
        public void SelectSingleForPOSlaveRequisition(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave
                join t2 in db.Common_Unit
                    on t1.Common_UnitFK equals t2.ID
                join t3 in db.Raw_Item
                    on t1.Raw_ItemFK equals t3.ID
                //join t4 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t4.ID
                //join t5 in db.Raw_Category on t4.Raw_CategoryFK equals t5.ID
                join t6 in db.Mkt_PO
                    on t1.Mkt_POFK equals t6.ID
                select new VmMktPO_Slave
                {

                    Mkt_POSlave = t1,
                    Common_Unit = t2,
                    Raw_Item = t3,
                    //Raw_SubCategory = t4,
                    //Raw_Category = t5,
                    Mkt_PO = t6,
                }).Where(V => V.Mkt_POSlave.ID == id).FirstOrDefault();



            //this.RawCatID = a.Raw_Category.ID;
            //this.RawSubCatID = a.Raw_SubCategory.ID;
            if (a != null)
            {
                this.Mkt_POSlave = a.Mkt_POSlave;
            }
        }

    }

    public class Vm_Mkt_POJournalIntegration
    {
        public int Id { get; set; }
        public int Acc_AcNameFk { get; set; }
        public int CurrencyType { get; set; }
        public string Name { get; set; }
        public string Cid { get; set; }
        public decimal CurrencyRate { get; set; }
        public decimal? Total { get; set; }
        public decimal? TotalQty { get; set; }
    }
}