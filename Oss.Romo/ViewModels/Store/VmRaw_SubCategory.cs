﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Store
{

   
    public class VmRaw_SubCategory
    {
        private xOssContext db;
        public IEnumerable<VmRaw_SubCategory> DataList { get; set; }


        public Raw_Item Raw_Item { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public int RawCategoryId { get; set; }
  

        public VmRaw_SubCategory()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_Category
                     from t2 in db.Raw_SubCategory
                         //where t2.ID == t3.Raw_CategoryFK
                     where t2.Raw_CategoryFK == t1.ID && t1.IsNew == true && t2.IsNew==true
                    
                     select new VmRaw_SubCategory
                     {
                         Raw_Category = t1,
                         Raw_SubCategory = t2
                     }).Where(x => x.Raw_SubCategory.Active == true).OrderByDescending(x => x.Raw_SubCategory.ID).AsEnumerable();
            this.DataList = v;
           
        }
        public void LoadDataFromDatabase()
        {
            db = new xOssContext();

            var v = (from t2 in db.Raw_SubCategory
                     select new VmRaw_SubCategory
                     {
                         Raw_SubCategory = t2
                     }).AsEnumerable();


            this.DataList = v;

        }


        public void SelectSingle(string rsCid)
        {
            int id = 0;
            rsCid = this.VmControllerHelper.Decrypt(rsCid);
            Int32.TryParse(rsCid, out id);
            db = new xOssContext();
            var v = (
                     from t1 in db.Raw_SubCategory
                     join t2 in db.Raw_Category
                     on t1.Raw_CategoryFK equals t2.ID

                     select new VmRaw_SubCategory
                     {
                         Raw_SubCategory = t1,
                         Raw_Category = t2

                     }).Where(c => c.Raw_SubCategory.ID == id).SingleOrDefault();
            this.Raw_SubCategory = v.Raw_SubCategory;          
            this.Raw_Category = v.Raw_Category;
            ///this.RawCategoryId = v.Raw_Category.ID;
    }

        //public void SelectSingleSubCategory(int id)
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Raw_SubCategory
        //             select new VmRaw_SubCategory
        //             {
        //                 Raw_SubCategory = t1
        //             }).Where(c => c.Raw_SubCategory.ID == id).SingleOrDefault();
        //    this.Raw_SubCategory = v.Raw_SubCategory;

        //}
  

        //Create
        public int Add(int userID)
        {
            Raw_SubCategory.AddReady(userID);
         
            return Raw_SubCategory.Add();
        }
        //Update
        public bool Edit(int userID)
        {
            return Raw_SubCategory.Edit(userID);
        }
        //Delete
        public bool Delete(int userID)
        {
            return Raw_SubCategory.Delete(userID);
        }
    }
   
}