﻿using Oss.Romo.Models;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Store
{
   



    public class VmRaw_Item
    {
        private xOssContext db;
        public IEnumerable<VmRaw_Item> DataList { get; set; }
        public Raw_Item Raw_Item{ get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }





        public VmRaw_Item()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_Item
                     from t2 in db.Raw_SubCategory
                     where t1.Raw_SubCategoryFK == t2.ID
                     from t3 in db.Raw_Category
                     where t2.Raw_CategoryFK == t3.ID && t1.IsNew==true && t2.IsNew==true &&t3.IsNew==true
                     select new VmRaw_Item
                     {
                         Raw_Item = t1,
                         Raw_SubCategory = t2,
                         Raw_Category=t3
                     }).Where(x => x.Raw_Item.Active == true).OrderByDescending(x => x.Raw_Item.ID).AsEnumerable();
            this.DataList = v;

        }
        public int RawCatID { get; set; }
        public void SelectSingle(string rawIid)
        {
            int id = 0;
            rawIid = this.VmControllerHelper.Decrypt(rawIid);
            Int32.TryParse(rawIid, out id);
            db = new xOssContext();
            var a = (from b in db.Raw_Item
                     join c in db.Raw_SubCategory
                     on b.Raw_SubCategoryFK equals c.ID
                     join d in db.Raw_Category 
                     on c.Raw_CategoryFK equals d.ID
                     select new VmRaw_Item
                     {
                         Raw_Category = d,
                         Raw_SubCategory = c,
                         Raw_Item = b
                     }).Where(c => c.Raw_Item.ID == id).SingleOrDefault();
            this.Raw_Item = a.Raw_Item;
            //this.Raw_SubCategory = a.Raw_SubCategory;
            //this.Raw_Category = a.Raw_Category;
            this.RawCatID = a.Raw_Category.ID;

        }

        //public void SelectSingleItem(int id)
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Raw_Item
        //             select new VmRaw_Item
        //             {
        //                 Raw_Item = t1
        //             }).Where(c => c.Raw_Item.ID == id).SingleOrDefault();
        //    this.Raw_Item = v.Raw_Item;

        //}

       //Add
        public int Add(int userID)
        {
            
            Raw_Item.AddReady(userID);
            return Raw_Item.Add();
        }
        //Edit
        public bool Edit(int userID)
        {
            return Raw_Item.Edit(userID);
        }
        //Delete
        public bool Delate(int userID)
        {
            return Raw_Item.Delete(userID);
        }
    }
}
