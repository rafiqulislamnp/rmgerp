﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Web;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.Models.User;

namespace Oss.Romo.ViewModels.Store
{
    public class VmStore_InternalTransferSlave
    {
        private xOssContext db;
        public IEnumerable<VmStore_InternalTransferSlave> DataList { get; set; }
        public IEnumerable<VmStore_InternalTransferSlave> DataList2 { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Prod_Requisition Prod_Requisition { get; set; }
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public VmMktPO_Slave VmMktPO_Slave { get; set; }
        public Mkt_POSlave_Consumption Mkt_POSlave_Consumption { get; set; }
        public Raw_InternaleTransfer Raw_InternaleTransfer { get; set; }
        public Prod_Consumption Prod_Consumption { get; set; }
        public Prod_TransitionItemInventory Prod_TransitionItemInventory { get; set; }
        public Raw_InternaleTransferSlave Raw_InternaleTransferSlave { get; set; }
        public int RawSubCatID { get; set; }
        public int RequsitionId { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public string RequiredQty { get; set; }
        public string EstimatedPrice { get; set; }
        public decimal? LineTotal { get; set; }
        public User_Department FromUser_Department { get; set; }
        public User_Department ToUser_Department { get; set; }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Raw_InternaleTransferSlave
                         //join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                         //join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                     join t4 in db.Prod_Requisition_Slave on t1.RequisitionSlaveFK equals t4.ID
                     //join t5 in db.Prod_Requisition on t4.Prod_RequisitionFK equals t5.ID
                     where t1.Active == true
                     && t1.ID == id
                     select new VmStore_InternalTransferSlave
                     {

                         Raw_InternaleTransferSlave = t1,
                         //Common_Unit = t2,
                         //Raw_Item = t3,
                         RequiredQty = t4.TotalRequired.ToString()
                         //Prod_Requisition=t5

                     }).FirstOrDefault();

            this.Raw_InternaleTransferSlave = a.Raw_InternaleTransferSlave;
            this.RequiredQty = a.RequiredQty;
            //this.Prod_Requisition_Slave = a.Prod_Requisition_Slave;
            //this.Prod_Requisition = a.Prod_Requisition;
            //this.Raw_Item = a.Raw_Item;
            //this.Common_Unit = a.Common_Unit;
        }

        public int Add(int userID)
        {
            Raw_InternaleTransferSlave.AddReady(userID);
            return Raw_InternaleTransferSlave.Add();
        }

        public bool Edit(int userID)
        {
            return Raw_InternaleTransferSlave.Edit(userID);
        }

        public void GetRequsitionID(int Id)
        {
            db = new xOssContext();
            var v = (from o in db.Prod_Requisition_Slave

                     where o.ID == Id && o.Active == true
                     select new
                     {
                         reqid = o.Prod_RequisitionFK
                     }).FirstOrDefault();

            this.RequsitionId = (int)v.reqid;
        }

        public VmStore_InternalTransferSlave SelectSingleJoinedInternaleTransfer(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_InternaleTransfer
                     join t2 in db.User_Department on t1.FromUser_DeptFK equals t2.ID
                     join t3 in db.User_Department on t1.ToUser_DeptFK equals t3.ID

                     select new VmStore_InternalTransferSlave
                     {
                         Raw_InternaleTransfer = t1,
                         FromUser_Department = t2,
                         ToUser_Department = t3

                     }).Where(x => x.Raw_InternaleTransfer.ID == id).FirstOrDefault();
            return v;

        }

        internal void InitialDataLoadForInternaleTransferSlave(int id)
        {
            db = new xOssContext();

            var a = (from t1 in db.Raw_InternaleTransferSlave
                     join t3 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t3.ID
                     join t4 in db.Raw_Item on t3.Raw_ItemFK equals t4.ID
                     join t5 in db.Common_Unit on t1.Common_UnitFK equals t5.ID
                     join t6 in db.Mkt_PO on t3.Mkt_POFK equals t6.ID
                     join t7 in db.Mkt_BOM on t6.Mkt_BOMFK equals t7.ID
                     join t8 in db.Common_TheOrder on t7.Common_TheOrderFk equals t8.ID
                     where t1.Active == true
                     && t1.Raw_InternaleTransferFK == id && t1.Mkt_POSlaveFK != 1
                     select new VmStore_InternalTransferSlave
                     {
                         Raw_InternaleTransferSlave = t1,
                         Raw_Item = t4,
                         Mkt_POSlave = t3,
                         Common_Unit = t5,
                         Mkt_BOM = t7,
                         Common_TheOrder = t8

                     }).OrderByDescending(x => x.Raw_InternaleTransferSlave.ID).AsEnumerable();

            var b = (from t1 in db.Raw_InternaleTransferSlave
                     join t3 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFK equals t3.ID
                     where t1.Active == true
                     && t1.Raw_InternaleTransferFK == id && t1.Prod_TransitionItemInventoryFK != 1
                     select new VmStore_InternalTransferSlave
                     {
                         Raw_InternaleTransferSlave = t1,
                         Prod_TransitionItemInventory = t3
                     }).OrderByDescending(x => x.Raw_InternaleTransferSlave.ID).AsEnumerable();
            var c = a.Union(b);
            this.DataList = c;
        }

        public void GetRaw_InternaleTransferSlave()
        {
            db = new xOssContext();
            var v = (from t1 in db.Raw_InternaleTransferSlave
                     join t2 in db.Raw_InternaleTransfer on t1.Raw_InternaleTransferFK equals t2.ID
                     join t3 in db.Prod_Requisition_Slave on t1.RequisitionSlaveFK equals t3.ID
                     join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                     //join t5 in db.Common_Unit on t1.Common_UnitFK equals  t5.ID
                     join t6 in db.Prod_Requisition on t3.Prod_RequisitionFK equals t6.ID
                     where t1.Active == true && t1.Raw_ItemFK != 1
                     select new VmStore_InternalTransferSlave
                     {
                         Raw_InternaleTransferSlave = t1,
                         Raw_InternaleTransfer = t2,
                         Prod_Requisition_Slave = t3,
                         Raw_Item = t4,
                         Prod_Requisition = t6
                     }).OrderByDescending(x => x.Raw_InternaleTransferSlave.ID).AsEnumerable();

            var v1 = (from t1 in db.Raw_InternaleTransferSlave
                      join t2 in db.Raw_InternaleTransfer on t1.Raw_InternaleTransferFK equals t2.ID
                      join t3 in db.Prod_Requisition_Slave on t1.RequisitionSlaveFK equals t3.ID
                      join t4 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFK equals t4.ID
                      //join t5 in db.Common_Unit on t1.Common_UnitFK equals t5.ID
                      join t6 in db.Prod_Requisition on t3.Prod_RequisitionFK equals t6.ID
                      where t1.Active == true && t1.Prod_TransitionItemInventoryFK != 1
                      select new VmStore_InternalTransferSlave
                      {
                          Raw_InternaleTransferSlave = t1,
                          Raw_InternaleTransfer = t2,
                          Prod_Requisition_Slave = t3,
                          Prod_TransitionItemInventory = t4,
                          Prod_Requisition = t6
                      }).OrderByDescending(x => x.Raw_InternaleTransferSlave.ID).AsEnumerable();
            var list = v.Union(v1);
            this.DataList = list;
        }

        public void GetProd_Consumption()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Consumption
                         //join t2 in db.Raw_InternaleTransferSlave
                         //on t1.Raw_InternaleTransferSlaveFk equals t2.ID


                     select new VmStore_InternalTransferSlave
                     {
                         Prod_Consumption = t1,
                         //Raw_InternaleTransferSlave = t2

                     }).Where(x => x.Prod_Consumption.Active == true).OrderByDescending(x => x.Prod_Consumption.ID).AsEnumerable();

            this.DataList2 = v;
        }

        public void UpdateConsumption(int id)
        {
            db  = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave_Consumption
                        
                   
                     where t1.Active == true
                     && t1.Raw_InternaleTransferSlaveFk == id
                     select new VmStore_InternalTransferSlave
                     {
                         Mkt_POSlave_Consumption = t1
                     }).FirstOrDefault();
            if (a != null)
            {
                this.Mkt_POSlave_Consumption = a.Mkt_POSlave_Consumption;
            }

         
        }

        public void UpdateConsumptionExt(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave_Consumption


                     where t1.Active == true
                     && t1.Mkt_POExtTransferFK == id
                     select new VmStore_InternalTransferSlave
                     {
                         Mkt_POSlave_Consumption = t1
                     }).FirstOrDefault();

            this.Mkt_POSlave_Consumption = a.Mkt_POSlave_Consumption;
        }
    }
}