﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Store
{
    public class VmInternalStore
    {
        private xOssContext db;
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_Item Mkt_Item { get; set; }

        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Prod_Requisition Prod_Requisition { get; set; }

        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public User_Department User_Department { get; set; }

        public IEnumerable<VmInternalStore> DataList { get; set; }

        public void InitialDataLoadForInternalStore ()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Requisition
                     join t2 in db.User_Department
                     on t1.FromUser_DeptFK equals t2.ID
                     where t1.Internal == true
                     select new VmInternalStore
                     {
                         Prod_Requisition = t1,
                         User_Department = t2,


                     }).AsEnumerable();
            this.DataList = v;

        }




        public void InitialDataLoadForExternalStore()
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Requisition
                     join t2 in db.User_Department
                     on t1.FromUser_DeptFK equals t2.ID
                     where t1.Internal ==false
                     select new VmInternalStore
                     {
                         Prod_Requisition = t1,
                         User_Department = t2,
                       

                     }).AsEnumerable();
            this.DataList = v;

        }




        public void InternalStoreReceive( int  id)
        {
            db = new xOssContext();





        }


        public void ExternalStoreReceive(int id)
        {
            db = new xOssContext();
            var a=(from t1 in db.Prod_Requisition_Slave       
                   join t2 in db.Prod_Requisition
                   on t1.Prod_RequisitionFK equals t2.ID
                   where t1.Active == true && t2.Active==true && t2.Internal==false

                   select new VmInternalStore
                   {
                      Prod_Requisition_Slave =t1,
                      Prod_Requisition = t2
                   }).Where(x => x.Prod_Requisition.ID == id).AsEnumerable();






        }




    }
}