﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Store
{
    public class VmPOSlave_Receiving
    {
        private xOssContext db;
        public Mkt_POSlave_Receiving Mkt_POSlave_Receiving { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public IEnumerable<VmPOSlave_Receiving> DataList { get; set; }
        public VmMkt_PO VmMkt_PO { get; set; }

        public decimal? TotalReturn { get; set; }
        public string Challan { get; private set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ChallanDate { get; private set; }

        public void InitialDataLoad_PoSlaveReceiving()
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Receiving
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     //join t6 in db.Raw_Item on t2.Raw_ItemFK equals t6.ID
                     join t7 in db.Common_Supplier on t3.Common_SupplierFK equals t7.ID
                     where t1.Active == true && t1.IsReturn == false
                     select new VmPOSlave_Receiving
                     {
                         Challan = t1.Challan,
                         ChallanDate = t1.Date,
                         Mkt_PO = t3,
                         Common_TheOrder = t5,
                         Common_Supplier = t7
                     }).Distinct().AsEnumerable();

            this.DataList = v;


            //    List<VmPOSlave_Receiving> tempVmMktPO_SlaveReceiving = new List<VmPOSlave_Receiving>();
            //    var grop = from b in tempVmMktPO_SlaveReceiving
            //               group b by b.Mkt_POSlave_Receiving into g
            //               select new Group<string,Mkt_POSlave_Receiving> { Key = g.Key, Values = g };
            //    this.DataList = grop;
            //}
            //public class Group<T, K>
            //{
            //    public K Key;
            //    public IEnumerable<T> Values;
        }
        public VmPOSlave_Receiving SelectSingleJoined(string challan)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Receiving
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t6 in db.Raw_Item on t2.Raw_ItemFK equals t6.ID
                     join t7 in db.Common_Supplier on t3.Common_SupplierFK equals t7.ID
                     where t1.Challan==challan
                     select new VmPOSlave_Receiving
                     {
                         Challan = t1.Challan,
                         ChallanDate = t1.Date,
                         Mkt_PO = t3,
                         Common_TheOrder = t5,
                         Common_Supplier = t7
                     }).FirstOrDefault();
            return v;

        

        }
        internal void GetReceivedChallanItem(string challan)
        {

            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave_Receiving
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID                   
                     join t6 in db.Raw_Item on t2.Raw_ItemFK equals t6.ID
                     where t1.Challan== challan && t1.IsReturn == false
                     select new VmPOSlave_Receiving
                     {
                         Mkt_POSlave_Receiving = t1,
                         Mkt_POSlave = t2,                       
                         Raw_Item = t6                        
                     }).OrderByDescending(x => x.Mkt_POSlave_Receiving.ID).AsEnumerable();
            this.DataList = a;

        }
        internal void GetReturnChallanItem(string challan)
        {

            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave_Receiving
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     join t6 in db.Raw_Item on t2.Raw_ItemFK equals t6.ID
                     where t1.Challan == challan&& t1.IsReturn==true
                     select new VmPOSlave_Receiving
                     {
                         Mkt_POSlave_Receiving = t1,
                         Mkt_POSlave = t2,
                         Raw_Item = t6
                     }).OrderByDescending(x => x.Mkt_POSlave_Receiving.ID).AsEnumerable();
            this.DataList = a;

        }

        public void InitialDataLoad_PoSlaveReturn()
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Receiving
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Mkt_BOM on t3.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     //join t6 in db.Raw_Item on t2.Raw_ItemFK equals t6.ID
                     join t7 in db.Common_Supplier on t3.Common_SupplierFK equals t7.ID
                     where t1.Active == true && t1.IsReturn == true
                     select new VmPOSlave_Receiving
                     {
                         Challan = t1.Challan,
                         ChallanDate = t1.Date,
                         Mkt_PO = t3,
                         Common_TheOrder = t5,
                         Common_Supplier = t7
                     }).Distinct().AsEnumerable();
            this.DataList = v;
           
        }     
        public int Add(int userID)
        {
            Mkt_POSlave_Receiving.AddReady(userID);
            return Mkt_POSlave_Receiving.Add();
        }
       

    }
}