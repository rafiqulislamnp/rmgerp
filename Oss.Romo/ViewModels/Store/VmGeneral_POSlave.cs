﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Production;

namespace Oss.Romo.ViewModels.Store
{
    public class VmGeneral_POSlave
    {
        private xOssContext db;
        public IEnumerable<VmGeneral_POSlave> DataList { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Prod_Requisition Prod_Requisition { get; set; }
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public VmMktPO_Slave VmMktPO_Slave { get; set; }
   
        public int RawSubCatID { get; set; }
        public int RawCatID { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public string RequiredQty { get; set; }
        public string EstimatedPrice { get; set; }
     
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Common_Unit
                     on t1.Common_UnitFK equals t2.ID
                     join t3 in db.Raw_Item
                     on t1.Raw_ItemFK equals t3.ID
                     join t4 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t4.ID
                     join t5 in db.Raw_Category on t4.Raw_CategoryFK equals t5.ID
                     join t6 in db.Mkt_PO
                     on t1.Mkt_POFK equals t6.ID
                     select new VmMktPO_Slave
                     {

                         Mkt_POSlave = t1,
                         Common_Unit = t2,
                         Raw_Item = t3,
                         Raw_SubCategory = t4,
                         Raw_Category = t5,
                         Mkt_PO = t6,
                     }).Where(V => V.Mkt_POSlave.ID == id).FirstOrDefault();



            this.RawCatID = a.Raw_Category.ID;
            this.RawSubCatID = a.Raw_SubCategory.ID;
            this.Mkt_POSlave = a.Mkt_POSlave;
        }

        public int Add(int userID)
        {
            Mkt_POSlave.AddReady(userID);
            return Mkt_POSlave.Add();
        }

        public bool Edit(int userID)
        {
            return Mkt_POSlave.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_POSlave.Delete(userID);
        }

    }
}