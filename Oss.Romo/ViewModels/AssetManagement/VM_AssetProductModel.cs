﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetProductModel:RootModel
    {
        private xOssContext db = new xOssContext();

        [DisplayName("Model")]
        [Required(ErrorMessage = "Please Enter Model  Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Model Name should be minimum 2 characters and Maximum 100 characters")]
        public string ModelName { get; set; }
        [DisplayName("Brand")]
        [Required(ErrorMessage = "Please Select Brand Name")]
        public string BrandName { get; set; }
        [DisplayName("Brand")]
        public int BrandId { get; set; }
        [DisplayName("Product Type")]
        public int ProductId { get; set; }
        [DisplayName("Product Type")]
        [Required(ErrorMessage = "Please Select Product Name")]
        public string ProductName { get; set; }
        public int IsAccessories { get; set; }

        public IEnumerable<VM_AssetProductModel> DataList { get; set; }
    }
}