﻿using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetProductInfo : RootModel
    {
        private xOssContext db = new xOssContext();

        [DisplayName("Asset Tag No")]
        public string Asset_Serial_No { get; set; }
        [DisplayName("Product")]
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Asset_Product Product { get; set; }

        [DisplayName("Product")]
        public string ProductName { get; set; }
        public string Product_SL_No { get; set; }
        [DisplayName("Brand")]
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }
        [DisplayName("Brand")]
        public string BrandName { get; set; }

        [DisplayName("Model")]
        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        [DisplayName("Model")]
        public string ModelName { get; set; }

        public Asset_ProductModel Model { get; set; }
        public string MasureUnit { get; set; }
        [DisplayName("Purchase Date")]
        public DateTime? PurchaseDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public decimal Price { get; set; }
        [DisplayName("Vendor")]
        public int VendorId { get; set; }
        [ForeignKey("VendorId")]
        public Asset_VendorInfo Vendor { get; set; }
        [DisplayName("Vendor")]
        public string VendorName { get; set; }            

        public int IsWarranty { get; set; }

        [DisplayName("Warrantry Date")]
        public DateTime? WarrantryDate { get; set; }

        [DisplayName("Configuration")]
        public string AssetConfiguration { get; set; }
        public int IsAssigned { get; set; }
        [DisplayName("Business Unit")]
        public int BU_Id { get; set; }   //Business Unit Id as Foreign Key
        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }
        public string BusinessName { get; set; }

        [DisplayName("Unit")]
        public int Unit_Id { get; set; }
        [DisplayName("Unit")]
        public string Unit_Name { get; set; }
        [DisplayName("Department")]
        public int Dept_Id { get; set; }
        [ForeignKey("Dept_Id")]
        [DisplayName("Department")]
        public string DeptName { get; set; }

        public HRMS_Department Department { get; set; }
        [DisplayName("Employee")]
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee Employee { get; set; }

        [DisplayName("Deptmenatal Location")]
        public int Location_Id { get; set; }
        [ForeignKey("Location_Id")]
        public Asset_Location Location { get; set; }
        [DisplayName("Assign Date")]
        public DateTime? AssignDate { get; set; }

        public int PurchaseQuantity { get; set; }
        public int Deliver { get; set; }
        public int IsHeadOffice { get; set; }
        public int Stock { get; set; }
        public int IsDeleted { get; set; }
        public int IsDamaged { get; set; }
        public int IsDisposed { get; set; }
        public string Remarks { get; set; }
        public int IsAccessories { get; set; }
        [DisplayName("Life Time")]
        public int LifeTime { get; set; }
        public ICollection<VM_AssetProductInfo> listasset { get; set; }
        public List<DepreciationHistory> listDepriciatiion { get; set; }
        public string listDepriciatiionc { get; set; }

        public void LoadAllAssetList()
        {
            var assetList = (from t1 in db.AssetProductInfo
                             join p in db.AssetProducts on t1.ProductId equals p.ID
                             join b in db.AssetProductBrands on t1.BrandId equals b.ID
                             join m in db.AssetProductModels on t1.ModelId equals m.ID
                             join d in db.Departments on t1.Dept_Id equals d.ID
                             join v in db.AssetVendorInfo on t1.VendorId equals v.ID

                             select new VM_AssetProductInfo()
                             {
                                 ID = t1.ID,
                                 ProductId=t1.ProductId,
                                 Asset_Serial_No=t1.Asset_Serial_No,
                                 ProductName = p.ProductName,
                                 BrandName = b.BrandName,
                                 Price=t1.Price,
                                 PurchaseDate=t1.PurchaseDate,
                                 ModelName = m.ModelName,
                                 VendorName = v.VendorName,
                                 DeptName = d.DeptName,
                                 Status = t1.Status,
                             }).OrderByDescending(x => x.ID).ToList();
            listasset = assetList;

        }

        public void LoadAvailableAssetList()
        {
            var assetList = (from t1 in db.AssetProductInfo
                             join p in db.AssetProducts on t1.ProductId equals p.ID
                             join b in db.AssetProductBrands on t1.BrandId equals b.ID
                             join m in db.AssetProductModels on t1.ModelId equals m.ID
                             join d in db.Departments on t1.Dept_Id equals d.ID
                             join v in db.AssetVendorInfo on t1.VendorId equals v.ID
                             where t1.IsAssigned!=1 && t1.IsDamaged!=1 &&t1.IsDisposed!=1
                             select new VM_AssetProductInfo()
                             {
                                 ID = t1.ID,
                                 ProductId = t1.ProductId,
                                 Asset_Serial_No = t1.Asset_Serial_No,
                                 ProductName = p.ProductName,
                                 BrandName = b.BrandName,
                                 Price = t1.Price,
                                 PurchaseDate = t1.PurchaseDate,
                                 ModelName = m.ModelName,
                                 VendorName = v.VendorName,
                                 DeptName = d.DeptName,
                                 Status = t1.Status,
                             }).ToList();
            listasset = assetList;

        }

        //Not completed
        public void LoadDisposedAssetList()
        {
            var disposedAssetList = (from t1 in db.AssetProductInfo
                                     join ap in db.AssetAssign on t1.ID equals ap.AssetProductInfoId
                                     join p in db.AssetProducts on t1.ProductId equals p.ID
                                     join b in db.AssetProductBrands on t1.BrandId equals b.ID
                                     join m in db.AssetProductModels on t1.ModelId equals m.ID
                                     join d in db.Departments on t1.Dept_Id equals d.ID
                                     join emp in db.Employees on ap.EmployeeId equals emp.ID
                                     where t1.IsDisposed == 1
                                     select new VM_AssetProductInfo()
                                     {
                                         ID = t1.ID,
                                         ProductId = t1.ProductId,
                                         Asset_Serial_No = t1.Asset_Serial_No,
                                         ProductName = p.ProductName,
                                         BrandName = b.BrandName,
                                         ModelName = m.ModelName,
                                         Price = t1.Price,
                                         //AssignDate = ,
                                         //AssetReturnDate = ap.ass,
                                         DeptName = d.DeptName,
                                         //EmpName = emp.Name,
                                         Remarks = t1.Remarks

                                     }).ToList();
            listasset = disposedAssetList;
        }

    }
    public class DepreciationHistory
    {
        public int year { get; set; }

        public int prtnge { get; set; }
    }


}