﻿using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetProductAssign : RootModel
    {
        private xOssContext db = new xOssContext();

        [DisplayName("Asset Tag No")]
        public string Asset_Serial_No { get; set; }
        public int AssetProductInfoId { get; set; }

        [DisplayName("Product")]
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Asset_Product Product { get; set; }

        [DisplayName("Product Type")]
        public string ProductName { get; set; }
        [DisplayName("Brand")]
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }

        [DisplayName("Brand")]
        public string BrandName { get; set; }

        [DisplayName("Model")]
        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        public Asset_ProductModel Model { get; set; }
        [DisplayName("Model")]
        public string ModelName { get; set; }

        public string Product_SL_No { get; set; }
        [DisplayName("BusinessUnit")]
        public int BU_Id { get; set; }
        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }
        [DisplayName("Department")]
        public int Dept_Id { get; set; }
        [ForeignKey("Dept_Id")]
        [DisplayName("Department")]
        public string DeptName { get; set; }

        [DisplayName("Employee")]
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]

        [DisplayName("Employee")]
        public string EmpName { get; set; }

        public HRMS_Employee Employee { get; set; }
        public string Designation { get; set; }
        [DisplayName("Location")]
        public int Location_Id { get; set; }
        [ForeignKey("Location_Id")]
        public Asset_Location Location { get; set; }
        public string ToFromDeptName { get; set; }
        [DisplayName("Vendor")]
        public int VendorId { get; set; }
        [ForeignKey("VendorId")]
        public Asset_VendorInfo Vendor { get; set; }
        public decimal Price { get; set; }
        public string PurchaseDate { get; set; }
        public double AvailableQty { get; set; }
        public string HandoverDate { get; set; }
        public string AssignDate { get; set; }
        public int IsAccessories { get; set; }
        public int IsAssigned { get; set; }
        public int IsReturned { get; set; }
        [DisplayName("Return Date")]
        public string AssetReturnDate { get; set; }
        public string Remarks { get; set; }

        public ICollection<VM_AssetProductAssign> listasset { get; set; }

        public void LoadAssignedAssetList()
        {
            var assignedAssetList = (from t1 in db.AssetAssign
                                     join p in db.AssetProducts on t1.ProductId equals p.ID
                                     join b in db.AssetProductBrands on t1.BrandId equals b.ID
                                     join m in db.AssetProductModels on t1.ModelId equals m.ID
                                     join d in db.Departments on t1.Dept_Id equals d.ID
                                     join emp in db.Employees on t1.EmployeeId equals emp.ID
                                     where t1.IsAssigned == 1 && t1.IsReturned == 0
                                     select new VM_AssetProductAssign()
                                     {
                                         ID = t1.ID,
                                         ProductId = t1.ProductId,
                                         Asset_Serial_No = t1.Asset_Serial_No,
                                         ProductName = p.ProductName,
                                         BrandName = b.BrandName,
                                         ModelName = m.ModelName,
                                         AssignDate = t1.AssignDate,
                                         DeptName = d.DeptName,
                                         EmpName = emp.Name
                                     }).ToList();
            listasset = assignedAssetList;

        }

        public VM_AssetProductAssign GetAssignedAssetInfo(int id)
        {

            VM_AssetProductAssign result = new VM_AssetProductAssign();
            var assignedAssetList = (from t1 in db.AssetAssign
                                     join p in db.AssetProducts on t1.ProductId equals p.ID
                                     join b in db.AssetProductBrands on t1.BrandId equals b.ID
                                     join m in db.AssetProductModels on t1.ModelId equals m.ID
                                     join d in db.Departments on t1.Dept_Id equals d.ID
                                     join emp in db.Employees on t1.EmployeeId equals emp.ID
                                     where t1.AssetProductInfoId == id
                                     select new VM_AssetProductAssign()
                                     {
                                         ID = t1.ID,
                                         ProductId = t1.ProductId,
                                         AssetProductInfoId = t1.AssetProductInfoId,
                                         Asset_Serial_No = t1.Asset_Serial_No,
                                         ProductName = p.ProductName,
                                         BrandId = t1.BrandId,
                                         BrandName = b.BrandName,
                                         ModelId = t1.ModelId,
                                         ModelName = m.ModelName,
                                         AssignDate = t1.AssignDate,
                                         Dept_Id = t1.Dept_Id,
                                         DeptName = d.DeptName,
                                         Location_Id = t1.Location_Id,
                                         EmployeeId = t1.EmployeeId,
                                         BU_Id = t1.BU_Id,
                                         ToFromDeptName = t1.ToFromDeptName,
                                         EmpName = emp.Name,
                                     }).FirstOrDefault();
            return assignedAssetList;
        }


        public string ReturnAsset(VM_AssetProductAssign assetReturnsModel, int returnType)
        {
            var str = "Problem";
            List<string> queryList = new List<string>();
            try
            {
                //For Normal Asset Return 
                if (returnType.Equals(0))
                {
                    var assetProduct = db.AssetProductInfo.FirstOrDefault(b => b.Asset_Serial_No == assetReturnsModel.Asset_Serial_No);
                    if (assetProduct != null)
                    {
                        assetProduct.Stock = 1;
                        assetProduct.Deliver = 0;
                        assetProduct.IsAssigned = 0;
                        db.SaveChanges();
                    }

                    var assignHistory = db.AssetAssignHistory.FirstOrDefault(b => b.Asset_Serial_No == assetReturnsModel.Asset_Serial_No);
                    if (assignHistory != null)
                    {
                        assignHistory.IsAssigned = 0;
                        assignHistory.IsReturned = 1;
                        assignHistory.AssetReturnDate = assetReturnsModel.AssetReturnDate;
                        db.SaveChanges();
                    }

                    var assetAssign = db.AssetAssign.FirstOrDefault(b => b.Asset_Serial_No == assetReturnsModel.Asset_Serial_No);
                    if (assetAssign != null)
                    {
                        assetAssign.IsAssigned = 0;
                        assetAssign.IsReturned = 1;
                        //assetAssign.re = assetReturnsModel.AssetReturnDate;
                        db.SaveChanges();
                    }

                    Asset_ProductAssign_History assetAssignHisotry = new Asset_ProductAssign_History
                    {
                        Asset_Serial_No = assetReturnsModel.Asset_Serial_No,
                        AssetProductInfoId = assetReturnsModel.AssetProductInfoId,
                        ProductId = assetReturnsModel.ProductId,
                        BrandId = assetReturnsModel.BrandId,
                        ModelId = assetReturnsModel.ModelId,
                        BU_Id = assetReturnsModel.BU_Id,
                        Dept_Id = assetReturnsModel.Dept_Id,
                        EmployeeId = assetReturnsModel.EmployeeId,
                        Designation_Id = 1,
                        Location_Id = assetReturnsModel.Location_Id,
                        AssetAction = "Return",
                        IsReturned = 0,
                        IsAccessories = 0,
                        IsAssigned = 0,
                        //Entry_By = Session["EmpID"].ToString(),
                        Entry_Date = DateTime.Now,
                        AssetReturnDate = assetReturnsModel.AssetReturnDate,
                        ToFromDeptName = assetReturnsModel.ToFromDeptName
                    };
                    db.AssetAssignHistory.Add(assetAssignHisotry);
                    db.SaveChanges();
                    return str = "Data Updated Successfully";
                }

                //For Damaged Asset Return 
                else if (returnType.Equals(1))
                {
                    var asProduct = db.AssetProductInfo.FirstOrDefault(b => b.Asset_Serial_No == assetReturnsModel.Asset_Serial_No);

                    if (asProduct != null)
                    {
                        asProduct.BU_Id = assetReturnsModel.BU_Id;
                        asProduct.Dept_Id = assetReturnsModel.Dept_Id;
                        asProduct.Location_Id = assetReturnsModel.Location_Id;
                        asProduct.Remarks = assetReturnsModel.Remarks;
                        asProduct.IsDamaged = 1;
                        asProduct.Stock = 1;
                        asProduct.Deliver = 0;
                        asProduct.IsAssigned = 0;
                        db.SaveChanges();
                    }

                    var assign = db.AssetAssign.FirstOrDefault(b => b.Asset_Serial_No == assetReturnsModel.Asset_Serial_No);
                    if (assign != null)
                    {
                        assign.IsReturned = 1;
                        assign.IsAssigned = 0;
                        db.SaveChanges();
                    }

                    var assignHistory = db.AssetAssignHistory.FirstOrDefault(b => b.Asset_Serial_No == assetReturnsModel.Asset_Serial_No);
                    if (assignHistory != null)
                    {
                        assignHistory.IsReturned = 1;
                        assignHistory.IsDamaged = 1;
                        assignHistory.IsAssigned = 0;
                        assignHistory.AssetReturnDate = assetReturnsModel.AssetReturnDate;
                        db.SaveChanges();
                    }

                    Asset_ProductAssign_History assetAssignHisotry = new Asset_ProductAssign_History
                    {
                        Asset_Serial_No = assetReturnsModel.Asset_Serial_No,
                        AssetProductInfoId = assetReturnsModel.AssetProductInfoId,
                        ProductId = assetReturnsModel.ProductId,
                        BrandId = assetReturnsModel.BrandId,
                        ModelId = assetReturnsModel.ModelId,
                        BU_Id = assetReturnsModel.BU_Id,
                        Dept_Id = assetReturnsModel.Dept_Id,
                        EmployeeId = assetReturnsModel.EmployeeId,
                        Designation_Id = 1,
                        Location_Id = assetReturnsModel.Location_Id,
                        AssetAction = "Return",
                        IsAccessories = 0,
                        IsAssigned = 0,
                        IsReturned = 1,
                        IsDamaged = 1,
                        //Entry_By = Session["EmpID"].ToString(),
                        Entry_Date = DateTime.Now,
                        AssetReturnDate = assetReturnsModel.AssetReturnDate,
                        ToFromDeptName = assetReturnsModel.ToFromDeptName
                    };
                    db.AssetAssignHistory.Add(assetAssignHisotry);
                    db.SaveChanges();
                    return str = "Data Updated Successfully";
                }
                //For Asset Return For Maintenance 
                else
                {
                    return null;

                }

            }

            catch (Exception ex)
            {
                return str = "Problem. Error is - " + ex.Message;
            }
        }


        public void LoadDamagedAssetList()
        {
            var damagedAssetList = (from t1 in db.AssetAssignHistory
                                    join ap in db.AssetProductInfo on t1.AssetProductInfoId equals ap.ID
                                    join p in db.AssetProducts on t1.ProductId equals p.ID
                                    join b in db.AssetProductBrands on t1.BrandId equals b.ID
                                    join m in db.AssetProductModels on t1.ModelId equals m.ID
                                    join d in db.Departments on t1.Dept_Id equals d.ID
                                    join emp in db.Employees on t1.EmployeeId equals emp.ID
                                    where t1.IsDamaged == 1
                                    select new VM_AssetProductAssign()
                                    {
                                        ID = t1.ID,
                                        AssetProductInfoId = ap.ID,
                                        ProductId = t1.ProductId,
                                        Asset_Serial_No = t1.Asset_Serial_No,
                                        ProductName = p.ProductName,
                                        BrandName = b.BrandName,
                                        ModelName = m.ModelName,
                                        AssetReturnDate = t1.AssetReturnDate,
                                        DeptName = d.DeptName,
                                        EmpName = emp.Name,
                                        Remarks = t1.Remarks

                                    }).ToList();
            listasset = damagedAssetList;
        }
    }
}