﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models.AssetManagement;
using Oss.Romo.Models;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetSubCategory : RootModel
    {
        private xOssContext db = new xOssContext();

        [DisplayName("Category")]
        public int CategoryId { get; set; }
        [DisplayName("Category")]
        public string CategoryName { get; set; }

        [DisplayName("Sub Category")]
        [Required]
        public string SubCategoryName { get; set; }
        public Asset_Category Category { get; set; }

        public IEnumerable<VM_AssetSubCategory> DataListAssetSubCategory { get; set; }

        public IEnumerable<VM_AssetSubCategory> DataListAssetSubCategoryInfo { get; set; }

        public void LoadAssetSubCategory()
        {
            var subCategoryList = (from t1 in db.AssetSubCategory
                                   join c in db.AssetCategory on t1.CategoryId equals c.ID

                                   select new VM_AssetSubCategory
                                   {
                                       ID = t1.ID,
                                       CategoryName = c.CategoryName,
                                       SubCategoryName = t1.SubCategoryName,
                                       Status = t1.Status,
                                   }).ToList();
            this.DataListAssetSubCategory = subCategoryList;
        }
        public VM_AssetSubCategory LoadSpecificAssetSubCategory(int id)
        {
            var subCategoryList = (from t1 in db.AssetSubCategory
                                   where t1.ID == id
                                   select new VM_AssetSubCategory
                                   {
                                       ID = t1.ID,
                                       SubCategoryName = t1.SubCategoryName,
                                       Status = t1.Status,
                                   }).FirstOrDefault();
            return subCategoryList;
        }



    }
}
