﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetReturnedHistory : RootModel
    {
        private xOssContext db = new xOssContext();

        [DisplayName("Return From Business Unit")]
        public int BU_Id { get; set; } //Business Unit Id as Foreign Key

        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }

        [DisplayName("Return To Department")]
        public int Dept_Id { get; set; }
        [ForeignKey("Dept_Id")]
        public HRMS_Department Department { get; set; }

        [DisplayName("Return To Employee")]
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee Employee { get; set; }
        [DisplayName("Asset Tag No")]
        public string Asset_Serial_No { get; set; }
        [DisplayName("Product")]
        public int ProductId { get; set; }

        [ForeignKey("ProductId")]
        public Asset_Product Product { get; set; }
        public string ProductName { get; set; }
        public string Product_SL_No { get; set; }

        [DisplayName("Brand")]
        public int BrandId { get; set; }
        public int Location_Id { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }

        [DisplayName("Model")]
        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        public Asset_ProductModel Model { get; set; }
        public DateTime AssignDate { get; set; }
        public IEnumerable<VM_AssetReturnedHistory> DataListAssetProductReturnInfo { get; set; }

        public void LoadAssetProductReturnList()
        {
            var a = (from t1 in db.AssetReturnedHistory
                     select new VM_AssetReturnedHistory()
                     {
                         ID = t1.ID,
                         ProductName = t1.ProductName,
                         BrandId = t1.BrandId,
                         ModelId = t1.ModelId,
                         Asset_Serial_No = t1.Asset_Serial_No,
                     }).AsEnumerable();
            this.DataListAssetProductReturnInfo = a;

        }

        public string DisposeAssetProduct(int id)
        {
            var str = "Problem";
            var query = "UPDATE [Asset_ProductInfo] SET IsDisposed = 1, [Entry_date] = GETDATE() WHERE [ID]=" + id;

            try
            {
                str =SqlDataAccess.SQL_ExecuteCommand(query);
                return str == "Command Executed Successfully" ? "Data Deleted Successfully" : str;
            }
            catch (Exception ex)
            {
                return str = "Problem. Error is - " + ex.Message;
            }
        }

    }
}