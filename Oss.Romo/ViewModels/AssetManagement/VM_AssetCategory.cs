﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetCategory : RootModel
    {
        private xOssContext db = new xOssContext();

        [DisplayName("Category")]
        [Required]
        public string CategoryName { get; set; }

        public List<VM_AssetCategory> DataListAssetCategory { get; set; }

        public void LoadAssetCategory()
        {
            var a = (from t1 in db.AssetCategory
                     select new VM_AssetCategory()
                     {
                         ID = t1.ID,
                         CategoryName = t1.CategoryName,
                         Status = t1.Status,
                     }).ToList();
            this.DataListAssetCategory = a;
        }
        public VM_AssetCategory LoadSpecificAssetCategory(int id)
        {
            var a = (from t1 in db.AssetCategory
                     where t1.ID == id
                     select new VM_AssetCategory()
                     {
                         ID = t1.ID,
                         CategoryName = t1.CategoryName,
                         Status = t1.Status,
                     }).FirstOrDefault();
            return a;
        }



    }
}