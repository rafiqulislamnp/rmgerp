﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;


namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetLocation : RootModel
    {
        private xOssContext db = new xOssContext();
        [DisplayName("Product Location")]
        [Required(ErrorMessage = "Please Select Location Name")]
        public string ProductLocation { get; set; }
        public int IsAccessories { get; set; }
        public string Block { get; set; }
        public int IsDeleted { get; set; }

        public IEnumerable<VM_AssetLocation> DataListAssetLocation { get; set; }



        public void LoadAssetProductLocationInfo()
        {
            var location = (from t1 in db.AssetLocations
                            select new VM_AssetLocation()
                            {
                                ID = t1.ID,
                                ProductLocation = t1.ProductLocation,
                                IsAccessories = t1.IsAccessories,
                                Block = t1.Block,
                            }).AsEnumerable();
            this.DataListAssetLocation = location;
        }

        public VM_AssetLocation LoadAssetProductLocationInfo(int id)
        {
            var a = (from t1 in db.AssetLocations
                     where t1.ID == id
                     select new VM_AssetLocation()
                     {
                         ID = t1.ID,
                         ProductLocation = t1.ProductLocation,
                         IsAccessories = t1.IsAccessories,
                         Block = t1.Block,
                         Entry_Date = t1.Entry_Date,
                         Entry_By = t1.Entry_By

                     }).FirstOrDefault();
            return a;
        }

        public void LoadAssetProductInfo()
        {
            var location = (from t1 in db.AssetLocations
                            select new VM_AssetLocation()
                            {
                                ID = t1.ID,
                                ProductLocation = t1.ProductLocation,
                                IsAccessories = t1.IsAccessories,
                                Block = t1.Block,
                            }).AsEnumerable();
            this.DataListAssetLocation = location;
        }




    }
}