﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;


namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetDepreciationHistory :RootModel
    {

        private xOssContext db = new xOssContext();

        [DisplayName("Asset_Tag_No")]
        public string Asset_Serial_No { get; set; }
        [DisplayName("Product")]
        public int ProductId { get; set; }

        [ForeignKey("ProductId")]
        public Asset_Product Product { get; set; }
        [DisplayName("Current Price")]
        public decimal Price { get; set; }
        public decimal Depreciation_Percent { get; set; }

        public DateTime Depreciation_Date { get; set; }

        [DisplayName("Product")]
        public string ProductName { get; set; }
        public string Product_SL_No { get; set; }

        [DisplayName("Brand")]
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }

        [DisplayName("Model")]
        public int ModelId { get; set; }

        [ForeignKey("ModelId")]
        public Asset_ProductModel Model { get; set; }
        [DisplayName("Business Unit")]
        public int BU_Id { get; set; } //Business Unit Id as Foreign Key

        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }

        [DisplayName("Department")]
        public int Dept_Id { get; set; }

        [ForeignKey("Dept_Id")]
        public HRMS_Department Department { get; set; }
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        [DisplayName("Employee Name")]
        public HRMS_Employee Employee { get; set; }

    }
}