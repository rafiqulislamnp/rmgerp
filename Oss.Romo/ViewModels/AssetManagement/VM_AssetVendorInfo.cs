﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetVendorInfo:RootModel

    {
        private xOssContext db = new xOssContext();
        [DisplayName("Vendor Name")]
         [Required(ErrorMessage = "Please Select Vendor Name")]
         public string VendorName { get; set; }
         public string Code { get; set; }
         public string Address { get; set; }
         [DisplayName("Cont. Person Name")]
         public string ContactPersonName { get; set; }
         public string Designation { get; set; }
         public string Email { get; set; }
         public string Phone { get; set; }
        [DisplayName("Alt. Phone")]
         public string AlternatePhone { get; set; }
         public int IsDeleted { get; set; }

    public List<VM_AssetVendorInfo> DataListAssetVendorInfo { get; set; }
   


        public void LoadAssetVendorInfo()
        {
            var a = (from t1 in db.AssetVendorInfo
                select new VM_AssetVendorInfo
                {
                    ID = t1.ID,
                    VendorName = t1.VendorName,
                    Address = t1.Address,
                    ContactPersonName =t1.ContactPersonName,
                    Designation =t1.Designation,
                    Email =t1.Email,
                    Phone =t1.Phone,
                    Code=t1.Code,
                    AlternatePhone = t1.AlternatePhone,

                }).ToList();
            this.DataListAssetVendorInfo = a;
        }
        public VM_AssetVendorInfo LoadSpecificAssetVendorInfo(int id)
    {
        var a = (from t1 in db.AssetVendorInfo
                 where t1.ID == id
            select new VM_AssetVendorInfo
            {
                ID = t1.ID,
                VendorName = t1.VendorName,
                Address = t1.Address,
                ContactPersonName = t1.ContactPersonName,
                Designation = t1.Designation,
                Email = t1.Email,
                Phone = t1.Phone,
                Code = t1.Code,
                AlternatePhone = t1.AlternatePhone,
                Entry_Date = t1.Entry_Date,
                Entry_By = t1.Entry_By

            }).FirstOrDefault();
        return a;
     }
    }
}