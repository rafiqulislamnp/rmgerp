﻿using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;


namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AseetReporting:RootModel
    {
        [DisplayName("Asset Tag No")]
        public string Asset_Serial_No { get; set; }
        public int AssetProductInfoId { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }
        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        public string ModelName { get; set; }
        public Asset_ProductModel Model { get; set; }
        [DisplayName("Purchase Date")]
        public string PurchaseDate { get; set; }
        [DisplayName("Business Unit")]
        public int BU_Id { get; set; }
        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }
        [DisplayName("Dept")]
        public int Dept_Id { get; set; }
        [ForeignKey("Dept_Id")]
        public HRMS_Department Department { get; set; }
        [DisplayName("Department")]
        public string DepartmentName { get; set; }


        [DisplayName("Category")]
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Asset_Category Category { get; set; }
        [DisplayName("Category")]
        public string CategoryName { get; set; }



        [DisplayName("Sub Category")]
        public int SubCategoryId { get; set; }
        [ForeignKey("SubCategoryId")]
        public Asset_SubCategory SubCategory { get; set; }
        [DisplayName("Sub Category")]
        public string SubCategoryName { get; set; }



        [DisplayName("Product")]
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        [DisplayName("Product Name")]
        public string ProductName { get; set; }
        public Asset_Product Product { get; set; }


        [DisplayName("Employee")]
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee Employee { get; set; }
        public string EmployeeName { get; set; }
        public string ToFromDeptName { get; set; }
        [DisplayName("Assign Date")]
        public string AssignDate { get; set; }
        public string AssetConfiguration { get; set; }
        [DisplayName("Return Date")]
        public string ReturnDate { get; set; }
        public string Remarks { get; set; }
        public string ActionButtonsHtmlAdmin { get; set; }
        public string ActionButtonsHtmlUser { get; set; }

        public List<VM_AseetReporting> GetAssetReportListByDeptAndEmp(int Bunit, int Department, int Empl)
        {
            string resp = "";
            List<VM_AseetReporting> list = new List<VM_AseetReporting>();
            string query = string.Format("Select aps.ID, aps.AssetProductInfoId,aps.Asset_Serial_No,aps.ProductId, ap.ProductName,aps.BrandId, ab.BrandName,aps.ModelId, am.ModelName, dp.DeptName, he.Name, aps.AssignDate from Asset_ProductAssign aps inner join Asset_Product ap On aps.Dept_Id = aps.Dept_Id inner join Department dp On aps.EmployeeId = aps.EmployeeId inner join HRMS_Employee he ON  aps.ProductId = ap.ID inner join Asset_ProductBrand ab ON aps.BrandId = ab.ID inner join Asset_ProductModel am ON aps.ModelId = am.ID Where [BU_Id] = '" + Bunit + "' and [Dept_Id] = '" + Department + "' and [EmployeeId] = '" + Empl + "' and [IsAssigned]= 1 and [IsReturned] = 0");
            var ds = SqlDataAccess.SQL_ExecuteReader(query, out resp);
            UrlHelper url = new UrlHelper(HttpContext.Current.Request.RequestContext);
            if (ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var productInfoList = new VM_AseetReporting
                    {
                        ID = Convert.ToInt32(ds.Tables[0].Rows[i]["ID"]),
                        AssetProductInfoId = Convert.ToInt32(ds.Tables[0].Rows[i]["AssetProductInfoId"]),
                        Asset_Serial_No = ds.Tables[0].Rows[i]["Asset_Serial_No"].ToString(),
                        ProductName = ds.Tables[0].Rows[i]["ProductName"].ToString(),
                        BrandName = ds.Tables[0].Rows[i]["BrandName"].ToString(),
                        ModelName = ds.Tables[0].Rows[i]["ModelName"].ToString(),
                        DepartmentName = ds.Tables[0].Rows[i]["DeptName"].ToString(),
                        EmployeeName = ds.Tables[0].Rows[i]["Name"].ToString(),

                    };
                    list.Add(productInfoList);
                }
                return list;
            }
            return list;
        }


        public List<VM_AseetReporting> GetAssetReportListByProduct(int Category, int SubCategory, int Product)
        {
            string resp = "";
            List<VM_AseetReporting> list = new List<VM_AseetReporting>();
            string query = string.Format("Select aps.ID, aps.AssetProductInfoId,aps.Asset_Serial_No,aps.ProductId, ap.ProductName,aps.CategoryId, ab.CategoryName,aps.SubCategoryId, am.SubCategoryName, aps.AssignDate from Asset_ProductAssign aps inner join Asset_Product ap On aps.CategoryId = aps.CategoryId inner join Asset_Category dp On aps.EmployeeId = aps.EmployeeId inner join HRMS_Employee he ON  aps.ProductId = ap.ID inner join Asset_ProductBrand ab ON aps.BrandId = ab.ID inner join Asset_ProductModel am ON aps.ModelId = am.ID Where [IsAssigned]= 1 and [IsReturned] = 0");
            var ds = SqlDataAccess.SQL_ExecuteReader(query, out resp);
            UrlHelper url = new UrlHelper(HttpContext.Current.Request.RequestContext);
            if (ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var productList = new VM_AseetReporting
                    {
                        ID = Convert.ToInt32(ds.Tables[0].Rows[i]["ID"]),
                        AssetProductInfoId = Convert.ToInt32(ds.Tables[0].Rows[i]["AssetProductInfoId"]),
                        Asset_Serial_No = ds.Tables[0].Rows[i]["Asset_Serial_No"].ToString(),
                        CategoryName = ds.Tables[0].Rows[i]["CategoryName"].ToString(),
                        SubCategoryName = ds.Tables[0].Rows[i]["SubCategoryName"].ToString(),
                        ProductName = ds.Tables[0].Rows[i]["ProductName"].ToString(),

                    };
                    list.Add(productList);
                }
                return list;
            }
            return list;
        }

    }
}