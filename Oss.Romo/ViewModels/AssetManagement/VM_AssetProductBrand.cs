﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_AssetProductBrand:RootModel
    {
        private xOssContext db = new xOssContext();

        [DisplayName("Brand")]
        [Required(ErrorMessage = "Please Enter Brand  Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Name should be minimum 2 characters and Maximum 100 characters")]
        public string BrandName { get; set; }
        [DisplayName("Product")]
        [Required(ErrorMessage = "Please Select Product Name")]
        public int ProductId { get; set; }

        [DisplayName("Product Type")]
        public string ProductName { get; set; }

        public int IsAccessories { get; set; }
        public IEnumerable<VM_AssetProductBrand> DataList { get; set; }

    }
}