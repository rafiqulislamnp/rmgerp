﻿using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;
using Oss.Romo.Models.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models.Entity.HRMS;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VM_Asset_ReturnInfo : RootModel
    {

        [DisplayName("Asset Tag No")]
        public string Asset_Serial_No { get; set; }
        public int AssetProductInfoId { get; set; }
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public string ProductName { get; set; }
        public Asset_Product Product { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        [ForeignKey("BrandId")]
        public Asset_ProductBrand Brand { get; set; }
        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        public string ModelName { get; set; }

        public Asset_ProductModel Model { get; set; }
        [DisplayName("BusinessUnit")]
        public int BU_Id { get; set; }
        [ForeignKey("BU_Id")]
        public HRMS_BusinessUnit BusinessUnit { get; set; }
        public int Dept_Id { get; set; }
        [ForeignKey("Dept_Id")]
        public HRMS_Department Department { get; set; }

        [DisplayName("Department")]
        public string DepartmentName { get; set; }

        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee Employee { get; set; }
        public string EmployeeName { get; set; }


        [DisplayName("Designation")]
        public int Designation_Id { get; set; }
        [ForeignKey("Designation_Id")]
        public HRMS_Designation Designation { get; set; }
        public int Location_Id { get; set; }
        [ForeignKey("Location_Id")]
        public Asset_Location Location { get; set; }
        public string ToFromDeptName { get; set; }
        public string HandoverDate { get; set; }
        public string AssignDate { get; set; }
        public string Price { get; set; }
        public string AssetConfiguration { get; set; }
        public string ReturnDate { get; set; }
        public string Remarks { get; set; }
        public string ActionButtonsHtmlAdmin { get; set; }
        public string ActionButtonsHtmlUser { get; set; }

        public List<VM_Asset_ReturnInfo> GetProductInfoList(int Bunit, int Department, int Empl)
        {
            string resp = "";
            List<VM_Asset_ReturnInfo> list = new List<VM_Asset_ReturnInfo>();
            string query = string.Format("select aps.ID, aps.AssetProductInfoId,aps.Asset_Serial_No,aps.ProductId, ap.ProductName,aps.BrandId, ab.BrandName,aps.ModelId, am.ModelName from Asset_ProductAssign aps inner join Asset_Product ap ON  aps.ProductId = ap.ID inner join Asset_ProductBrand ab ON aps.BrandId = ab.ID inner join Asset_ProductModel am ON aps.ModelId = am.ID Where [BU_Id] = '" + Bunit + "' and [Dept_Id] = '" + Department + "' and [EmployeeId] = '" + Empl + "' and [IsAssigned]= 1 and [IsReturned] = 0");
            var ds = SqlDataAccess.SQL_ExecuteReader(query, out resp);
            UrlHelper url = new UrlHelper(HttpContext.Current.Request.RequestContext);
            if (ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var productInfoList = new VM_Asset_ReturnInfo
                    {
                        ID = Convert.ToInt32(ds.Tables[0].Rows[i]["ID"]),
                        AssetProductInfoId = Convert.ToInt32(ds.Tables[0].Rows[i]["AssetProductInfoId"]),
                        Asset_Serial_No = ds.Tables[0].Rows[i]["Asset_Serial_No"].ToString(),
                        //ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]),
                        ProductName = ds.Tables[0].Rows[i]["ProductName"].ToString(),
                        //BrandId = Convert.ToInt32(ds.Tables[0].Rows[i]["BrandId"]),
                        BrandName = ds.Tables[0].Rows[i]["BrandName"].ToString(),
                        //ModelId = Convert.ToInt32(ds.Tables[0].Rows[i]["BrandId"]),
                        ModelName = ds.Tables[0].Rows[i]["ModelName"].ToString(),
                        ActionButtonsHtmlUser = "<a href=\"" + url.Action("AssetReturnsToAnother", "AssetManagement", new { id = ds.Tables[0].Rows[i]["AssetProductInfoId"].ToString() }) + "\" id=\"btnastrtn" + ds.Tables[0].Rows[i]["AssetProductInfoId"].ToString() + "\" class=\"btn btn-primary btn-sm no-print\" data-tooltip=\"tooltip\" title=\"Return\" data-placement=\"top\" >Return</a>"
                        //"<a href="@Url.Action("AssetReturnsToAnother", "AssetManagement", new RouteValueDictionary(new { id = ds.Tables[0].Rows[i]["SL"].ToString()}))" class="btn btn-link btn-cstm btn-sm">Return</a>"

                    };
                    list.Add(productInfoList);
                }
                return list;
            }
            return list;
        }

        public VM_AssetProductAssign GetAssignedAssetInfo(string id)
        {
            string result = "";
            VM_AssetProductAssign assetAssignInfo = new VM_AssetProductAssign();
            var query = "SELECT ID,Asset_Serial_No,ProductId,BrandId,ModelId,AssignDate,EmployeeId,BU_Id from Asset_ProductAssign AS apa where apa.ProductId =" + id;
            var ds = SqlDataAccess.SQL_ExecuteReader(query, out result);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                assetAssignInfo = new VM_AssetProductAssign
                {
                    ID = Convert.ToInt32(ds.Tables[0].Rows[0]["ID"]),
                    Asset_Serial_No = ds.Tables[0].Rows[0]["Asset_Serial_No"].ToString(),
                    ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductId"]),
                    //ProductName = ds.Tables[0].Rows[0]["ProductName"].ToString(),
                    //BrandName = ds.Tables[0].Rows[0]["BrandName"].ToString(),
                    BrandId = Convert.ToInt32(ds.Tables[0].Rows[0]["BrandId"]),
                    ModelId = Convert.ToInt32(ds.Tables[0].Rows[0]["ModelId"]),
                    //ModelName = ds.Tables[0].Rows[0]["ModelName"].ToString(),
                    AssignDate = ds.Tables[0].Rows[0]["AssignDate"].ToString(),
                    //EmpName = ds.Tables[0].Rows[0]["EmpName"].ToString(),
                    EmployeeId = Convert.ToInt32(ds.Tables[0].Rows[0]["EmployeeId"]),
                    //ToFromDeptName = string.Format("{0} ({1})", ds.Tables[0].Rows[0]["EmpName"].ToString(), ds.Tables[0].Rows[0]["DepartmentName"].ToString()),
                    BU_Id= Convert.ToInt32(ds.Tables[0].Rows[0]["BU_Id"])

                };
            }
            return assetAssignInfo;
        }


    }

}
