﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.AssetManagement;

namespace Oss.Romo.ViewModels.AssetManagement
{
    public class VMAssetType : RootModel
    {
        public string Type { get; set; }
    }
    public class VM_AssetProduct : RootModel
    {
        private xOssContext db = new xOssContext();
        [DisplayName("Product Type")]
        public string ProductName { get; set; }

        [DisplayName("Category")]
        public int CategoryId { get; set; }

        [DisplayName("Category")]
        public string CategoryName { get; set; }

        [DisplayName("Sub Category")]
        public int SubCategoryId { get; set; }

        [DisplayName("Sub Category")]
        public string SubCategoryName { get; set; }

        [DisplayName("Abbreviation")]
        public string ProductAbbreviation { get; set; }

        public int IsAccessories { get; set; }
        public IEnumerable<VM_AssetProduct> DataListAssetProductTypes { get; set; }

        public void LoadAssetProductList()
        {
            var assetProduct = (from t1 in db.AssetProducts
                                join c in db.AssetCategory on t1.CategoryId equals c.ID
                                select new VM_AssetProduct()
                                {
                                    ID = t1.ID,
                                    ProductName = t1.ProductName,
                                    CategoryName = c.CategoryName,
                                    //SubCategoryName = d.SubCategoryName,
                                    ProductAbbreviation = t1.ProductAbbreviation,
                                    Status = t1.Status,
                                }).AsEnumerable();
            this.DataListAssetProductTypes = assetProduct;
        }

        public VM_AssetProduct LoadSpecificAssetProductInfo(int id)
        {
            var assetProduct = (from t1 in db.AssetProducts
                                join c in db.AssetCategory on t1.CategoryId equals c.ID
                               
                                where t1.Status == true && t1.ID == id
                                select new VM_AssetProduct
                                {
                                    ID = t1.ID,
                                    ProductName = t1.ProductName,
                                    CategoryName = c.CategoryName,
                                    //SubCategoryName = d.SubCategoryName,
                                    ProductAbbreviation = t1.ProductAbbreviation,
                                    Status = t1.Status,

                                }).FirstOrDefault();
            return assetProduct;
        }

        public void LoadAccessoriesProductInfo()
        {
            var accssoriesProduct = (from t1 in db.AssetProducts
                                     select new VM_AssetProduct()
                                     {
                                         ID = t1.ID,
                                         ProductName = t1.ProductName,
                                         ProductAbbreviation = t1.ProductAbbreviation,
                                         Status = t1.Status,
                                         IsAccessories = t1.IsAccessories
                                     }).Where(x => x.IsAccessories == 1).AsEnumerable();
            this.DataListAssetProductTypes = accssoriesProduct;
        }

        public VM_AssetProduct LoadSpecificAccessoriesProductInfo(int id)
        {
            var accssoriesProduct = (from t1 in db.AssetProducts
                                     where t1.Status == true && t1.ID == id
                                     select new VM_AssetProduct()
                                     {
                                         ID = t1.ID,
                                         ProductName = t1.ProductName,
                                         Status = t1.Status,
                                         IsAccessories = t1.IsAccessories,
                                         Entry_Date = t1.Entry_Date,
                                         Entry_By = t1.Entry_By

                                     }).FirstOrDefault();
            return accssoriesProduct;
        }
    }
}