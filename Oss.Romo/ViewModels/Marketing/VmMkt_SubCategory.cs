﻿using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_SubCategory
    {
        private xOssContext db;
        public IEnumerable<VmMkt_SubCategory> DataList { get; set; }
        public Mkt_Category Mkt_Category { get; set; }
        public Mkt_SubCategory Mkt_SubCategory { get; set; }

        public VmControllerHelper VmControllerHelper { get; set; }

        public VmMkt_SubCategory()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_Category
                     from t2 in db.Mkt_SubCategory
                     where t2.Mkt_CategoryFK == t1.ID
                     
                     select new VmMkt_SubCategory
                     {
                         Mkt_Category = t1,
                         Mkt_SubCategory=t2
                      }).Where(x=>x.Mkt_SubCategory.Active==true).OrderByDescending(x => x.Mkt_SubCategory.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(string sCid)
        {
            int id = 0;
            sCid = this.VmControllerHelper.Decrypt(sCid);
            Int32.TryParse(sCid, out id);
            db = new xOssContext();
            var a = (from t1 in db.Mkt_Category
                     from t2 in db.Mkt_SubCategory
                     where t2.Mkt_CategoryFK == t1.ID
                     &&  t2.ID == id
                     select new VmMkt_SubCategory
                     {
                         Mkt_Category = t1,
                         Mkt_SubCategory = t2
                     }).SingleOrDefault();
            this.Mkt_SubCategory = a.Mkt_SubCategory;
        }


        //Create
        public int Add(int userID)
        {
            Mkt_SubCategory.AddReady(userID);
            return Mkt_SubCategory.Add();
        }
        //Edit
        public bool Edit(int userID)
        {
            return Mkt_SubCategory.Edit(userID);
        }
        //Delete
        public bool Delete(int userID)
        {
            return Mkt_SubCategory.Delete(userID);
        }
    }
}