﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMktBuyerIndexVM
    {
        public int ID { get; set; }
        [Display(Name ="BuyerNo")]
        public string BuyerNo { get; set; }
        public int BuyerAccountId { get; set; }
        [Display(Name = "Country")]
        public string CountryName { get; set; }
        public string BuyerName { get; set; }
        [Display(Name = "Address")]
        public string BuyerAddress { get; set; }
        [Display(Name = "Phone")]
        public string BuyerPhone { get; set; }
        public IEnumerable<VmMktBuyerIndexVM> DataList { get; set; }
    }
}