﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class PackingItem
    {
        public string ColorName { get; set; }
        public int ColorQty { get; set; }
        public List<SizeList> QuantityList{ get; set; }
    }


    public class SizeList
    {
        public string Color { get; set; }

        public int ColorQuantity { get; set; }

        public string Size { get; set; }

        public int? Ratio { get; set; }
        public decimal Quantity { get; set; }
    }

    public class VmMkt_OrderDeliverySchedule
    {
        private xOssContext db;
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public Common_Country Common_Country { get; set; }
        public IEnumerable<VmMkt_OrderDeliverySchedule> DataList { get; set; }
        public IEnumerable<VmMkt_BOM> DataList1 { get; set; }
        public IEnumerable<PackingItem> PackingList { get; set; }
        public string Result { get; set; }
        public int SetQuantity { get; set; }
        public string PortColor { get; set; }
        public List<SizeList> SizeList { get; set; }
        public List<PackingItem> ColorList { get; set; }
        public decimal DeliverdQuantity { get; set; }
        public string ErrorMessage { get; set; }

        public void OrderDeliveryScheduleDataLoad(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     join t2 in db.Common_Country on t1.Destination equals t2.ID
                     
                     select new VmMkt_OrderDeliverySchedule
                     {
                         Mkt_OrderDeliverySchedule = t1,
                         Common_Country = t2,

                     }).Where(x => x.Mkt_OrderDeliverySchedule.Common_TheOrderFk == id && x.Mkt_OrderDeliverySchedule.Active == true).OrderByDescending(x => x.Mkt_OrderDeliverySchedule.ID).AsEnumerable();
            
            this.DataList = v;

        }
        public int Add(int userID)
        {
            Mkt_OrderDeliverySchedule.AddReady(userID);
            return Mkt_OrderDeliverySchedule.Add();
        }
        public bool Edit(int userID)
        {
            return Mkt_OrderDeliverySchedule.Edit(userID);
        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Mkt_OrderDeliverySchedule
                     select new VmMkt_OrderDeliverySchedule
                     {
                         Mkt_OrderDeliverySchedule = b

                     }).Where(c => c.Mkt_OrderDeliverySchedule.ID == id).SingleOrDefault();
            this.Mkt_OrderDeliverySchedule = a.Mkt_OrderDeliverySchedule;
        }
        public int GetOrderWiseTotalQuantity(int id)
        {
            
            db = new xOssContext();
            //id = db.Mkt_BOM.Where(x=>x.Common_TheOrderFk == id && x.Active==true).Select(x=>x.ID).FirstOrDefault() ;
           return db.Mkt_BOM.Where(x => x.Common_TheOrderFk == id && x.Active == true).Select(x => x.QPack * x.Quantity * x.SetQuantity).FirstOrDefault();


                     //select new VmMkt_BOM
                     //{
                     //    Mkt_BOM = b

                     //}).Where(c => c.Mkt_BOM.ID == id && c.Mkt_BOM.Active == true).AsEnumerable();
           // this.DataList1 = a;
        }
        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     join t3 in db.Common_Country on t1.Destination equals t3.ID
                     where t1.Common_TheOrderFk == id
                     select new VmMkt_OrderDeliverySchedule
                     {
                         Mkt_OrderDeliverySchedule = t1,
                         Common_Country = t3,

                     }).Where(x => x.Mkt_OrderDeliverySchedule.Active == true).OrderByDescending(x => x.Mkt_OrderDeliverySchedule.ID).AsEnumerable();

            List<VmMkt_OrderDeliverySchedule> list = new List<VmMkt_OrderDeliverySchedule>();
            foreach (var x in v)
            {
                x.DeliverdQuantity = GetDeliverdQuantity(x.Mkt_OrderDeliverySchedule.ID);
                // x.reference = GetReference(x.Mkt_OrderDeliverySchedule.ID);
                list.Add(x);
            }
            this.DataList = list;
        }


        private decimal GetDeliverdQuantity(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Shipment_OrderDeliverdSchedule
                     where t1.Mkt_OrderDeliveryFk == id && t1.Active == true
                     select t1.DeliverdQty).ToList();
            decimal deliverdQty = 0;
            if (v != null)
            {
                deliverdQty = v.Sum();
            }

            return deliverdQty;
        }
        public void GetDeliveryQuantity(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Mkt_OrderDeliverySchedule
                     select new VmMkt_OrderDeliverySchedule
                     {
                         Mkt_OrderDeliverySchedule = b

                     }).Where(c => c.Mkt_OrderDeliverySchedule.Common_TheOrderFk == id && c.Mkt_OrderDeliverySchedule.Active == true).AsEnumerable();
            this.DataList = a;
        }

        public void GetColorList(int Id, decimal Qty)
        {
            List<PackingItem> lst = new List<PackingItem>();
            using (db = new xOssContext())
            {
                var vData = (from o in db.Mkt_OrderColorAndSizeRatio
                             where o.Active == true && o.Common_TheOrderFk == Id
                             join p in db.Mkt_OrderDeliverySchedule on o.Common_TheOrderFk equals p.ID
                             group o by new { o.Color } into Group
                             select new PackingItem
                             {
                                 ColorName = Group.Key.Color,
                                 QuantityList = Group.GroupBy(f => f.Size).Select(m => new SizeList { Size = m.Key, Quantity = m.Sum(c => c.Quantity) }).OrderBy(a => a.Quantity).ToList()
                             }).ToList();
                
                if (vData.Any())
                {
                    List<SizeList> clist = new List<SizeList>();
                    int NumberColor = vData.Count();
                    bool CheckFraction = Qty % NumberColor == 0 ? false : true;
                    if (!CheckFraction)
                    {
                        decimal ColorPart = Qty / NumberColor;

                        foreach (var v in vData)
                        {
                            clist.Clear();
                            decimal lowestValue = v.QuantityList.FirstOrDefault().Quantity;
                            if (lowestValue>0)
                            {
                                foreach (var c in v.QuantityList)
                                {
                                    decimal ColorValue = c.Quantity / lowestValue;
                                    clist.Add(new SizeList { Size = c.Size, Quantity = ColorValue });
                                }

                                decimal rTotal = clist.Sum(a => a.Quantity);
                                decimal fRatio = ColorPart / rTotal;

                                foreach (var c1 in v.QuantityList)
                                {
                                    var item = clist.Where(a => a.Size == c1.Size).FirstOrDefault();
                                    item.Quantity = item.Quantity * fRatio;
                                }

                                PackingItem p = new PackingItem();
                                p.ColorName = v.ColorName;
                                p.QuantityList = clist;
                                lst.Add(p);
                            }
                            

                        }
                    }

                }
                this.PackingList = lst;
            }

        }

        public void GetPackingDetails(int PId, int OId)
        {
            using (db = new xOssContext())
            {
                var vData = (from o in db.Mkt_OrderColorAndSizeRatio
                            where o.Active == true && o.Common_TheOrderFk==OId && o.OrderDeliveryScheduleFk == PId
                            select new SizeList
                            {
                                Color = o.Color,
                                Size = o.Size,
                                Quantity = o.Quantity
                            }).ToList();
                this.SizeList = vData;
            }  
        }

        public void GetSizeList(string InputSize)
        {
            
            SizeList = new List<Marketing.SizeList>();
            if (InputSize.Trim().EndsWith(";"))
            {
                int ColorLoop, SizeLoop, colorqty, sizeqty;

                string[] ColorSizeword = null;

                ColorSizeword = InputSize.Split('\n');

                for (ColorLoop = 0; ColorLoop < ColorSizeword.Length; ColorLoop++)
                {
                    string[] SizeWord = null;
                    string ColorWord = null;
                    
                    ColorWord = ColorSizeword[ColorLoop].Split(';').FirstOrDefault();

                    PortColor += ColorWord + " ";
                    int colorQty = 0;
                    string color = ColorWord.Split('-').First();
                    string qty = ColorWord.Split('-').Last();
                    int.TryParse(qty, out colorQty);
                    SizeWord = ColorSizeword[ColorLoop].Split(';');

                    for (SizeLoop = 0; SizeLoop < SizeWord.Length - 1; SizeLoop++)
                    {
                        if (SizeWord[SizeLoop] != ColorWord)
                        {
                            int q = 0;
                            string SizeNumber = SizeWord[SizeLoop].Split('-').First();
                            string SizeQuantity = SizeWord[SizeLoop].Split('-').Last();
                            int.TryParse(SizeQuantity, out q);

                            SizeList.Add(new SizeList { Color = color, ColorQuantity = colorQty, Size = SizeNumber, Quantity = q });
                        }

                    }
                }
                GetColorSize(SizeList);
            }
            else
            {
                Result = "Please enter valid format.";
            }
        }

        private void GetColorSize(List<SizeList> list)
        {
            ColorList = new List<PackingItem>();
            if (list.Any())
            {
                var vData = from o in list
                        group o by new { o.Color, o.ColorQuantity } into g
                        select new
                        {
                            cName = g.Key.Color,
                            cQty=g.Key.ColorQuantity,
                            sizeList = g.GroupBy(f => f.Size).Select(m => new SizeList { Size = m.Key, Quantity = m.Sum(c => c.Quantity) }).OrderBy(a => a.Quantity).ToList()
                        };
                

                foreach (var item in vData)
                {
                    PackingItem color = new PackingItem();
                    color.ColorName = item.cName;
                    color.ColorQty = item.cQty;
                    if (item.sizeList.Any())
                    {
                        decimal lvalue = item.sizeList.Sum(a => a.Quantity);
                        decimal ratio = item.cQty / lvalue;
                        color.QuantityList = new List<Marketing.SizeList>();
                        foreach (var size in item.sizeList)
                        {
                            SizeList sList = new Marketing.SizeList();
                            sList.Size = size.Size;
                            sList.Quantity = size.Quantity * ratio;
                            sList.Ratio = (int)size.Quantity;
                            color.QuantityList.Add(sList);
                        }
                    }
                    ColorList.Add(color);
                }
            }

            this.ColorList = ColorList;
        }
    }
}