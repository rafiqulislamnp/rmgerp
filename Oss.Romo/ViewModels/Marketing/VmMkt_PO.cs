﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Reports;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.ViewModels.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.Security.Cryptography;
using System.Text;
using Oss.Romo.Models.User;
using Oss.Romo.Models.Commercial;
using Oss.Romo.ViewModels.Common;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_PO
    {
        public int ValuePo;
        public int ValueMlc;
        public int ValueB2blc;
        [Display(Name = "T & C Title")]
        public int StatusId { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        private xOssContext db;
        public string SearchString { get; set; }
        public string Header { get; set; }
        public Raw_InternaleTransfer Raw_InternaleTransfer { get; set; }
        public string POHeader { get; set; }
        public string Message { get; set; }
        public string ButtonName { get; set; }
        public string Text1 { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public DateTime Text2 { get; set; }
        public List<ReportDocType> POReportDoc { get; set; }
        public Prod_Requisition Prod_Requisition { get; set; }
        public Mkt_YarnCalculation Mkt_YarnCalculation { get; set; }
        public Mkt_YarnType Mkt_YarnType { get; set; }
        public VmProd_Requisition VmProd_Requisition { get; set; }
        public Prod_Requisition_Slave Prod_Requisition_Slave { get; set; }
        public VmGeneral_PO VmGeneral_PO { get; set; }
        public VmMkt_BOMSlave VmMkt_BOMSlave { get; set; }
        public VmMkt_POExtTransfer VmMkt_POExtTransfer { get; set; }
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Mkt_POExtTransfer Mkt_POExtTransfer { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public VmMktPO_Slave VmMktPO_Slave { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public User_User User_User { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Mkt_POSlave_Receiving Mkt_POSlave_Receiving { get; set; }
        public User_Department User_Department { get; set; }

        public VmPOSlave_Receiving VmPOSlave_Receiving { get; set; }
        public Mkt_POSlave_Consumption Mkt_POSlave_Consumption { get; set; }
        public IEnumerable<VmMkt_PO> DataList { get; set; }
        public IEnumerable<VmMkt_PO> VmMkt_POList { get; set; }
        public IEnumerable<VmMktPO_Slave> DataList2 { get; set; }
        public Prod_TransitionItemInventory Prod_TransitionItemInventory { get; set; }
        public List<Stoked> StokedList { get; set; }
        public IEnumerable<VmProd_Requisition_Slave> DataList3 { get; set; }
        [DisplayName("Received Quentity")]
        public decimal? Recieve { get; set; }
        [DisplayName("Remaining Quentity")]
        public decimal? Now_Recieved { get; set; }
        [DisplayName("Send Quentity")]
        public decimal? Send { get; set; }
        [DisplayName("Remaining Quentity")]
        public decimal? Now_Send { get; set; }
        [DisplayName("Remaining Amount")]
        public decimal? Now_Paid { get; set; }
        [DisplayName("Total Price")]
        public decimal? Total_Price { get; set; }
        [DisplayName("Line Total")]
        public decimal? Line_Total { get; set; }

        [DisplayName("Total Value")]
        public decimal? TotalVal { get; set; }
        public string Challan { get; set; }

        public decimal? PreviousReturned { get; set; }
        public decimal? PreviousReceived { get; set; }
        public decimal? TotalStock { get; set; }
        public decimal? PreviousDelivery { get; set; }
        public decimal? PreviousDeliveryReturn { get; set; }
        [DisplayName("Value")]
        public decimal? Total_Value { get; set; }
        [DisplayName("Paid")]
        public decimal? Total_Paid { get; set; }
        [DisplayName("Due")]
        public decimal? Total_Due { get; set; }
        [DisplayName("Received Value")]
        public decimal? GoodReceived { get; set; }

        [DisplayName("Pay Able")]
        public decimal? Payable { get; set; }
        public string PageType { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMCID equals Mkt_BOM.CID
                     join Common_Currency in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals Common_Currency.ID

                     where Mkt_PO.Active == true

                     select new VmMkt_PO
                     {
                         Common_Supplier = Common_Supplier,
                         Mkt_PO = Mkt_PO,
                         Mkt_BOM = Mkt_BOM,
                         Common_Currency = Common_Currency
                     }).Where(x => x.Mkt_PO.Active == true).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();
            this.DataList = a;

        }
        public void GetStoreReceivedItem(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave_Receiving
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                     join t4 in db.Raw_Item on t2.Raw_ItemFK equals t4.ID
                     where t1.Active == true && t2.Active == true && t3.ID == id
                     select new VmMkt_PO
                     {
                         Mkt_POSlave_Receiving = t1,
                         Mkt_POSlave = t2,
                         Mkt_PO = t3,
                         Raw_Item = t4
                     }).OrderByDescending(x => x.Mkt_POSlave_Receiving.ID).AsEnumerable();
            this.DataList = a;
        }


        internal bool VmPOSlave_ReceivingEdit(int userID)
        {
            return Mkt_POSlave_Receiving.Edit(userID);

        }

        internal void SelectSingleReceivedItem(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Active == true
                     select new VmMkt_PO
                     {
                         Mkt_POSlave_Receiving = t1
                     }).Where(c => c.Mkt_POSlave_Receiving.ID == id).SingleOrDefault();
            this.Mkt_POSlave_Receiving = a.Mkt_POSlave_Receiving;
        }

        public void GetItemForConsmtion()
        {
            db = new xOssContext();

            var a = (from mkt_BOM in db.Mkt_BOM
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     where mkt_BOM.Active == true && mkt_Item.Active == true && Common_TheOrder.Active == true && Common_TheOrder.IsComplete == false && Common_TheOrder.IsComplete == false
                     && Mkt_Buyer.Active == true


                     select new VmMkt_BOM
                     {
                         Common_TheOrder = Common_TheOrder,
                         Mkt_Buyer = Mkt_Buyer,
                         Mkt_BOM = mkt_BOM,
                         Mkt_Item = mkt_Item
                     }).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();

            this.VmMkt_BOM = new VmMkt_BOM();
            this.VmMkt_BOM.DataList = a;
        }


        internal void POSettledDataLoad()
        {
            List<VmMkt_PO> tempData = new List<VmMkt_PO>();
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Mkt_POSlave in db.Mkt_POSlave
                     on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     select new VmMkt_PO
                     {
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         Mkt_POSlave = Mkt_POSlave
                     }).Where(x => x.Mkt_PO.Active == true && x.Mkt_PO.Status == 1).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();


            foreach (VmMkt_PO vmMkt_PO in c)
            {
                if (CheckPoSlaveIfFullOrderReceived(vmMkt_PO.Mkt_PO.ID))
                {
                    tempData.Add(vmMkt_PO);
                }

            }
            this.DataList = tempData;

        }
        private bool CheckPoSlaveIfFullOrderReceived(int poID)
        {
            db = new xOssContext();
            var c = (from t1 in db.Mkt_POSlave
                     where t1.Mkt_POFK == poID
                     select new
                     {
                         ID = t1.ID,
                         TR = t1.TotalRequired
                     }).ToList();
            foreach (var v in c)
            {
                if (v.TR != GetTotalReceivedFromPoSlave(v.ID))
                    return false;
            }
            return true;
        }
        private decimal? GetTotalReceivedFromPoSlave(int poSlaveID)
        {
            db = new xOssContext();
            return (from t1 in db.Mkt_POSlave_Receiving
                    where t1.Mkt_POSlaveFK == poSlaveID
                    select t1.Quantity).Sum();


        }
        internal void POReadyDataLoad()
        {
            List<VmMkt_PO> tempData = new List<VmMkt_PO>();
            db = new xOssContext();
            var c = (from mkt_PO in db.Mkt_PO
                     join mkt_POSlave in db.Mkt_POSlave on mkt_PO.ID equals mkt_POSlave.Mkt_POFK
                     join common_Supplier in db.Common_Supplier on mkt_PO.Common_SupplierFK equals common_Supplier.ID
                     select new VmMkt_PO
                     {
                         Mkt_PO = mkt_PO,
                         Common_Supplier = common_Supplier,
                         Mkt_POSlave = mkt_POSlave
                     }).Where(x => x.Mkt_PO.Active == true && x.Mkt_PO.Status == 1).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();


            foreach (VmMkt_PO vmMkt_PO in c)
            {
                if (!CheckPoSlaveIfFullOrderReceived(vmMkt_PO.Mkt_PO.ID))
                {
                    tempData.Add(vmMkt_PO);
                }

            }
            this.DataList = tempData;
        }
        //----------Mkt_POSlave--------/////
        public void SelectSingle(int? id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_PO
                         //join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_Supplier on t1.Common_SupplierFK equals t3.ID
                     where t1.Active == true
                     select new VmMkt_PO
                     {
                         Mkt_PO = t1,
                         //Mkt_BOM = t2,
                         Common_Supplier = t3
                     }).Where(c => c.Mkt_PO.ID == id).SingleOrDefault();

            this.Mkt_PO = a.Mkt_PO;



            InitialDataLoad_PoSlave(id.Value);
        }

        public void SelectSingleReceiving(int? id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave_Receiving
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     where t1.Active == true && t2.Active == true
                     select new VmMkt_PO
                     {
                         Mkt_POSlave_Receiving = t1,
                         Mkt_POSlave = t2
                     }).Where(c => c.Mkt_POSlave_Receiving.ID == id).SingleOrDefault();
            this.Mkt_POSlave_Receiving = a.Mkt_POSlave_Receiving;
            this.Mkt_POSlave = a.Mkt_POSlave;
        }
        
        public void SelectSinglePO(int iD)
        {
            db = new xOssContext();
            var v = (from Mkt_PO in db.Mkt_PO
                     where Mkt_PO.Active == true
                     select new VmMkt_PO
                     {
                         Mkt_PO = Mkt_PO
                     }).Where(c => c.Mkt_PO.ID == iD).SingleOrDefault();
            this.Mkt_PO = v.Mkt_PO;

        }
        
        public void InitialDataLoad_PoSlave(int id)
        {
            db = new xOssContext();
            //return all po item from BOM
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
                     join t6 in db.Common_Supplier on t4.Common_SupplierFK equals t6.ID
                     where t1.Active == true
                     select new VmMktPO_Slave
                     {

                         Mkt_PO = t4,
                         Mkt_POSlave = t1,
                         Raw_Item = t2,
                         Common_Unit = t3,
                         Common_Supplier = t6
                     }).Where(x => x.Mkt_POSlave.Mkt_POFK == id).AsEnumerable();

            ////return external trunsfer item of BOM
            //var v1 = (from t1 in db.Mkt_POExtTransfer

            //          join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
            //          join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
            //          join t5 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t5.ID
            //          join t2 in db.Raw_Item on t5.Raw_ItemFK equals t2.ID
            //          join t6 in db.Common_Supplier on t4.Common_SupplierFK equals t6.ID
            //          where t1.Active == true && t1.Mkt_POSlaveFK != 1
            //          select new VmMktPO_Slave
            //          {

            //              Mkt_PO = t4,
            //              Mkt_POSlave = t5,
            //              Mkt_POExtTransfer = t1,
            //              Raw_Item = t2,
            //              Common_Unit = t3,
            //              Common_Supplier = t6
            //          }).Where(x => x.Mkt_POExtTransfer.Mkt_POFK == id).AsEnumerable();

            //var v2 = (from t1 in db.Mkt_POExtTransfer
            //          join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
            //          join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
            //          join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
            //          join t6 in db.Common_Supplier on t4.Common_SupplierFK equals t6.ID
            //          where t1.Active == true && t1.Raw_ItemFK != 1
            //          select new VmMktPO_Slave
            //          {

            //              Mkt_PO = t4,
            //              Mkt_POExtTransfer = t1,
            //              Raw_Item = t2,
            //              Common_Unit = t3,
            //              Common_Supplier = t6
            //          }).Where(x => x.Mkt_POExtTransfer.Mkt_POFK == id).AsEnumerable();
            //var v3 = (from t1 in db.Mkt_POExtTransfer
            //          join t2 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFK equals t2.ID
            //          join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
            //          join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
            //          join t6 in db.Common_Supplier on t4.Common_SupplierFK equals t6.ID
            //          where t1.Active == true && t1.Prod_TransitionItemInventoryFK != 1
            //          select new VmMktPO_Slave
            //          {

            //              Mkt_PO = t4,
            //              Mkt_POExtTransfer = t1,
            //              Prod_TransitionItemInventory = t2,
            //              Common_Unit = t3,
            //              Common_Supplier = t6
            //          }).Where(x => x.Mkt_POExtTransfer.Mkt_POFK == id).AsEnumerable();

            // var unionData = v.Union(v1).Union(v2).Union(v3);

            

            List<VmMktPO_Slave> POSlaveList = new List<VmMktPO_Slave>();
            var po = v.FirstOrDefault();
            //this.Mkt_BOM = ss.Mkt_BOM;
            if (po != null)
            {
                this.Mkt_PO = po.Mkt_PO;
                this.Common_Supplier = po.Common_Supplier;
            }

            foreach (VmMktPO_Slave x in v)
            {
                //if (x.Mkt_POExtTransfer != null)
                //{
                //    x.PreviousReceived = GetPreviousExtReceived(x.Mkt_POExtTransfer.ID);
                //    x.CurrentStock = GetTotalExtStock(x.Mkt_POExtTransfer.ID);
                //    x.CurrentDelivery = GetPreviousExtDelivery(x.Mkt_POExtTransfer.ID).GetValueOrDefault();
                //    x.StoreName = GetStoreExtName(x.Mkt_POExtTransfer.ID);
                //    POSlaveList.Add(x);
                //}
                //else
                //{
                x.PreviousReceived = GetPreviousReceived(x.Mkt_POSlave.ID);
                x.CurrentDelivery = GetPreviousDelivery(x.Mkt_POSlave.ID).GetValueOrDefault();
                x.StoreName = GetStoreName(x.Mkt_POSlave.ID);
                x.PreviousLoss = GetPreviousStockLoss(x.Mkt_POSlave.ID);
                x.CurrentStock = GetTotalStock(x.Mkt_POSlave.ID) - x.PreviousLoss;
                POSlaveList.Add(x);
                //}

            }
            this.DataList2 = POSlaveList;
        }

        private string GetStoreName(int id)
        {
            db = new xOssContext();
            string storeName = "";
            string sName = (from t1 in db.Mkt_POSlave_Receiving
                            join t2 in db.User_Department on t1.User_DepartmentFk equals t2.ID
                            where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                            select (t2.Name)).FirstOrDefault();
            if (sName != null)
            {
                storeName = sName;

            }
            return storeName;
        }

        private string GetStoreExtName(int id)
        {
            db = new xOssContext();
            string storeName = "";
            string sName = (from t1 in db.Mkt_POSlave_Receiving
                            join t2 in db.User_Department on t1.User_DepartmentFk equals t2.ID
                            where t1.Mkt_POExtTransferFK == id && t1.IsReturn == false
                            select (t2.Name)).FirstOrDefault();
            if (sName != null)
            {
                storeName = sName;

            }
            return storeName;
        }

        public decimal? GetTotalStock(int id)
        {
            decimal? received = 0;
            // decimal? returned = 0;
            decimal? Delivery = 0;
            decimal? DeliveryReturned = 0;
            received = GetPreviousReceived(id).GetValueOrDefault();
            // returned = GetPreviousReturned(id).GetValueOrDefault();
            Delivery = GetPreviousDelivery(id).GetValueOrDefault();
            DeliveryReturned = GetPreviousDeliveryReturn(id).GetValueOrDefault();
            return (received + DeliveryReturned) - Delivery;
        }
        public decimal? GetTotalExtStock(int id)
        {
            decimal? received = 0;
            // decimal? returned = 0;
            decimal? Delivery = 0;
            decimal? DeliveryReturned = 0;
            received = GetPreviousExtReceived(id).GetValueOrDefault();
            // returned = GetPreviousExtReturned(id).GetValueOrDefault();
            Delivery = GetPreviousExtDelivery(id).GetValueOrDefault();
            DeliveryReturned = GetPreviousExtDeliveryReturn(id).GetValueOrDefault();
            return (received + DeliveryReturned) - Delivery;
        }
        public void InitialDataLoad_PoSlaveSend(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_Supplier on t4.Common_SupplierFK equals t6.ID
                     //join t7 in db.Common_TheOrder on t5.Common_TheOrderFk equals t7.ID
                     where t1.Active == true
                     select new VmMktPO_Slave
                     {
                         Mkt_PO = t4,
                         Mkt_BOM = t5,
                         Mkt_POSlave = t1,
                         Raw_Item = t2,
                         Common_Unit = t3,
                         Common_Supplier = t6,
                         //Common_TheOrder = t7
                     }).Where(x => x.Mkt_BOM.ID == id).AsEnumerable(); //&& x.Common_TheOrder.Active == true
            List<VmMktPO_Slave> tempVmMktPO_Slave = new List<VmMktPO_Slave>();
            var ss = v.FirstOrDefault();
            List<VmMktPO_Slave> tempVmMktPO_Slave1 = new List<VmMktPO_Slave>();
            if (ss != null)
            {
                this.Mkt_BOM = ss.Mkt_BOM;
                this.Mkt_PO = ss.Mkt_PO;
                this.Common_Supplier = ss.Common_Supplier;

                foreach (VmMktPO_Slave x in v)
                {
                    x.PreviousDelivery = GetPreviousDelivery(x.Mkt_POSlave.ID);
                    x.TotalStock = GetTotalStock(x.Mkt_POSlave.ID);
                    tempVmMktPO_Slave1.Add(x);

                }
                this.DataList2 = tempVmMktPO_Slave1;
            }
            else
            {
                this.Message = "Thare are no item abailable";
            }


        }

        public void InitialDataLoad_PoSlaveSendREQ(int id)
        {
            db = new xOssContext();

            var v = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     select new VmProd_Requisition_Slave
                     {
                         Prod_Requisition = t4,
                         Mkt_BOM = t5,
                         Prod_Requisition_Slave = t1,
                         Raw_Item = t2,
                         Common_Unit = t3,
                     }).Where(x => x.Prod_Requisition_Slave.Prod_RequisitionFK == id).AsEnumerable();
            List<VmProd_Requisition_Slave> tempVmProd_Requisition_Slave = new List<VmProd_Requisition_Slave>();
            var ss = v.FirstOrDefault();
            this.Mkt_BOM = ss.Mkt_BOM;
            this.Prod_Requisition = ss.Prod_Requisition;
            List<VmProd_Requisition_Slave> tempVmProd_Requisition_Slave1 = new List<VmProd_Requisition_Slave>();
            foreach (VmProd_Requisition_Slave x in v)
            {
                x.PreviousDelivery = GetPreviousDeliveryREQ(x.Prod_Requisition_Slave.ID);
                x.TotalStock = GetTotalStock(x.Prod_Requisition_Slave.ID);
                tempVmProd_Requisition_Slave1.Add(x);

            }
            this.DataList3 = tempVmProd_Requisition_Slave1;
        }
        public decimal? GetPreviousReceived(int id)
        {
            db = new xOssContext();
            var receiveItem = (from t1 in db.Mkt_POSlave_Receiving
                               where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                               select t1.Quantity).DefaultIfEmpty(0).Sum();

            var returnItem = (from t1 in db.Mkt_POSlave_Receiving
                              where t1.Mkt_POSlaveFK == id && t1.IsReturn == true
                              select t1.Quantity).DefaultIfEmpty(0).Sum();

            return (receiveItem - returnItem);
        }
        public decimal? GetPreviousStockLoss(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                     select t1.StockLoss).ToList();
            decimal? quantity = 0;
            if (v != null)
            {
                quantity = v.Sum();
            }

            return quantity;
        }
        public decimal? GetPreviousExtReceived(int id)
        {
            db = new xOssContext();
            var recivedItem = (from t1 in db.Mkt_POSlave_Receiving
                               where t1.Mkt_POExtTransferFK == id && t1.IsReturn == false
                               select t1.Quantity).DefaultIfEmpty(0).Sum();
            var returnItem = (from t1 in db.Mkt_POSlave_Receiving
                              where t1.Mkt_POExtTransferFK == id && t1.IsReturn == true
                              select t1.Quantity).DefaultIfEmpty(0).Sum();
            decimal? quantity = recivedItem - returnItem;

            return quantity;
        }
        public decimal? GetPreviousDelivery(int id)
        {
            db = new xOssContext();
            var delivered = (from t1 in db.Mkt_POSlave_Consumption
                             where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                             select t1.Quantity).DefaultIfEmpty(0).Sum();

            var deliveredReturn = (from t1 in db.Mkt_POSlave_Consumption
                                   where t1.Mkt_POSlaveFK == id && t1.IsReturn == true
                                   select t1.Quantity).DefaultIfEmpty(0).Sum();
            decimal? quantity = delivered - deliveredReturn;

            return quantity;
        }
        public decimal? GetPreviousExtDelivery(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Consumption
                     where t1.Mkt_POExtTransferFK == id && t1.IsReturn == false
                     select t1.Quantity).ToList();
            decimal? quantity = 0;
            if (v != null)
            {
                quantity = v.Sum();
            }

            return quantity;
        }
        public decimal? GetPreviousDeliveryREQ(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Consumption
                     where t1.Prod_Requisition_SlaveFK == id && t1.IsReturn == false
                     select (t1.Quantity)).ToList();
            decimal? quantity = 0;
            if (v != null)
            {
                quantity = v.Sum();
            }

            return quantity;
        }

        //-----------POExt------------------------////





        //---------POSlave_Receiving--------/////////////

        public int AddPOSlave_Receiving(int userID)
        {

            Mkt_POSlave_Receiving.AddReady(userID);
            return Mkt_POSlave_Receiving.Add();
        }
        public int VmPOSlave_ReceivingCreate(int userID, IEnumerable<VmMktPO_Slave> DataList2)
        {
            int receiveId = 0;
            foreach (VmMktPO_Slave x in DataList2)
            {
                if (x.Receiving != null)
                {
                    if (x.Mkt_POExtTransfer != null)
                    {
                        Mkt_POSlave_Receiving.Mkt_POExtTransferFK = x.Mkt_POExtTransfer.ID;
                        Mkt_POSlave_Receiving.Mkt_POSlaveFK = 1;
                    }
                    else
                    {
                        Mkt_POSlave_Receiving.Mkt_POSlaveFK = x.Mkt_POSlave.ID;
                        Mkt_POSlave_Receiving.Mkt_POExtTransferFK = 1;
                    }

                    Mkt_POSlave_Receiving.Quantity = x.Receiving;
                    Mkt_POSlave_Receiving.StockLoss = x.StockLoss;
                    Mkt_POSlave_Receiving.Date = this.Mkt_POSlave_Receiving.Date;
                    Mkt_POSlave_Receiving.Challan = this.Challan + "/" + this.Mkt_PO.CID;
                    Mkt_POSlave_Receiving.User_DepartmentFk = this.Mkt_POSlave_Receiving.User_DepartmentFk;
                    Mkt_POSlave_Receiving.IsReturn = false;
                    Mkt_POSlave_Receiving.IsFinishFabric = x.IsFinishFabric;
                    Mkt_POSlave_Receiving.FabricColor = x.FabricColor;
                    receiveId = AddPOSlave_Receiving(userID);
                }

            }
            return receiveId;

        }

        //-----------MKT PO Slave Consumtion-----------------//
        public int AddPOSlave_Delivery(int userID)
        {

            Mkt_POSlave_Consumption.AddReady(userID);
            return Mkt_POSlave_Consumption.Add();
        }
        public int AddDeliveryCreateREQ(int userID)
        {
            Mkt_POSlave_Consumption.AddReady(userID);
            return Mkt_POSlave_Consumption.Add();
        }
        public void VmPOSlave_DeliveryCreate(int userID, IEnumerable<VmMktPO_Slave> DataList2)
        {

            foreach (VmMktPO_Slave x in DataList2)
            {
                if (x.Delivery != null)

                {
                    if (x.Mkt_POSlave != null)
                    {

                        Mkt_POSlave_Consumption.Mkt_POSlaveFK = x.Mkt_POSlave.ID;
                        Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
                    }
                    else
                    {
                        Mkt_POSlave_Consumption.Mkt_POExtTransferFK = x.Mkt_POExtTransfer.ID;
                        Mkt_POSlave_Consumption.Mkt_POSlaveFK = 1;

                    }


                    Mkt_POSlave_Consumption.Quantity = x.Delivery;
                    Mkt_POSlave_Consumption.Mkt_POSlaveFK = x.Mkt_POSlave.ID;
                    Mkt_POSlave_Consumption.Date = this.Mkt_POSlave_Consumption.Date;
                    Mkt_POSlave_Consumption.Requisition = this.Mkt_POSlave_Consumption.Requisition;
                    Mkt_POSlave_Consumption.Supervisor = this.Mkt_POSlave_Consumption.Supervisor;
                    Mkt_POSlave_Consumption.User_DepartmentFk = this.Mkt_POSlave_Consumption.User_DepartmentFk;
                    Mkt_POSlave_Consumption.IsReturn = false;
                    AddPOSlave_Delivery(userID);
                }


            }
        }
        public void VmPOSlave_DeliveryCreateREQ(int userID, IEnumerable<VmProd_Requisition_Slave> DataList3)
        {
            Mkt_POSlave_Consumption Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
            foreach (VmProd_Requisition_Slave x in DataList3)
            {
                if (x.Delivery != null)
                {
                    Mkt_POSlave_Consumption.Requisition = this.Prod_Requisition.RequisitionCID;
                    Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = x.Prod_Requisition_Slave.ID;
                    Mkt_POSlave_Consumption.Date = this.Mkt_POSlave_Consumption.Date;
                    Mkt_POSlave_Consumption.Supervisor = this.Mkt_POSlave_Consumption.Supervisor;
                    Mkt_POSlave_Consumption.IsReturn = false;
                    this.Mkt_POSlave_Consumption = Mkt_POSlave_Consumption;
                    AddDeliveryCreateREQ(userID);
                }


            }
        }

        //---///////////////////////

        //-----//////


        //..For increment PoID..//
        //internal bool CreateCID(int userID, int bomId, int poID )
        //{
        //    Mkt_PO.ID = poID;
        //    Mkt_PO.Mkt_BOMFK = bomId;
        //    Mkt_PO.CID = "PO/"+(10000 + Mkt_PO.Mkt_BOMFK).ToString() +"/" + (10000 + poID).ToString();
        //    return Mkt_PO.Edit(userID);
        //}

        //public int Add(int userID)
        //{
        //    //Mkt_PO.Status = 0;
        //    Mkt_PO.AddReady(userID);
        //    return Mkt_PO.Add();
        //}

        //internal void GetPOFromBOM(int id)
        //{
        //    db = new xOssContext();
        //    var a = (from Mkt_PO in db.Mkt_PO
        //             join Common_Supplier in db.Common_Supplier
        //             on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
        //             join Mkt_BOM in db.Mkt_BOM
        //             on Mkt_PO.Mkt_BOMCID equals Mkt_BOM.CID
        //             //join Raw_Item in db.Raw_Item
        //             //on Mkt_PO.Raw_ItemFK equals Raw_Item.ID
        //             join Common_Currency in db.Common_Currency
        //             on Mkt_PO.Common_CurrencyFK equals Common_Currency.ID
        //             // join Common_Unit in db.Common_Unit
        //             //on Mkt_PO.Common_UnitFK equals Common_Unit.ID
        //             where Mkt_PO.Active == true
        //             && Mkt_PO.Mkt_BOMFK== id
        //             select new VmMkt_PO
        //             {
        //                 Common_Currency = Common_Currency,
        //                 Common_Unit= Common_Unit,
        //                 Common_Supplier = Common_Supplier,
        //                 Mkt_PO = Mkt_PO,
        //                 Mkt_BOM = Mkt_BOM,
        //                 Raw_Item = Raw_Item

        //             }).Where(x => x.Mkt_PO.Active == true).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();
        //    this.DataList = a;

        //}
        
        public bool Dalete(int userID)
        {
            return Mkt_PO.Delete(userID);
        }
        //-------------------------

        //public bool UpdateReceivedQuantity(int userID)
        //{
        //    this.Mkt_PO.Received += this.Now_Recieved;

        //    return this.Edit(userID);
        //}
        //public bool UpdateSendQuantity(int userID)
        //{
        //    this.Mkt_PO.Delivered += this.Now_Send;

        //    return this.Edit(userID);
        //}

        //public bool UpdateTotalPrice(int userID)
        //{

        //    this.Mkt_PO.PaidAmount += this.Now_Paid;

        //    return this.Edit(userID);
        //}

        //.......PurchaseOrders....//
        public decimal? TotlaValue(int id)
        {
            db = new xOssContext();
            return (from Mkt_POSlave in db.Mkt_POSlave

                    where Mkt_POSlave.Mkt_POFK == id && Mkt_POSlave.Active == true
                    select (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)).DefaultIfEmpty(0).Sum();



        }
        public decimal? TotlaPaid(int id)
        {
            db = new xOssContext();
            var a = (from Acc_POPH in db.Acc_POPH

                     where Acc_POPH.Mkt_POFK == id
                     select (Acc_POPH.Amount)).Sum();
            if (a == null)
            {
                a = 0;
            }
            return a;

        }
        public decimal? GoodReceivedValue(int id)
        {
            db = new xOssContext();

            var received = (from Mkt_PO in db.Mkt_PO
                            join Mkt_POSlave in db.Mkt_POSlave
                            on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                            join Mkt_POSlave_Receiving in db.Mkt_POSlave_Receiving
                            on Mkt_POSlave.ID equals Mkt_POSlave_Receiving.Mkt_POSlaveFK
                            where Mkt_PO.ID == id
                            && Mkt_POSlave_Receiving.IsReturn == false
                            select (Mkt_POSlave.Price * Mkt_POSlave_Receiving.Quantity)).Sum().GetValueOrDefault();

            var returned = (from Mkt_PO in db.Mkt_PO
                            join Mkt_POSlave in db.Mkt_POSlave
                            on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                            join Mkt_POSlave_Receiving in db.Mkt_POSlave_Receiving
                            on Mkt_POSlave.ID equals Mkt_POSlave_Receiving.Mkt_POSlaveFK
                            where Mkt_PO.ID == id
                            && Mkt_POSlave_Receiving.IsReturn == true
                            select (Mkt_POSlave.Price * Mkt_POSlave_Receiving.Quantity)).Sum().GetValueOrDefault();

            return received - returned;

        }
        
        internal void PurchaseOrderView(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder
                     on t2.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User
                     on Mkt_PO.FirstCreatedBy equals User_User.ID
                     join Mkt_POSlave in db.Mkt_POSlave
                    on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Raw_SubCategory in db.Raw_SubCategory
                    on Raw_Item.Raw_SubCategoryFK equals Raw_SubCategory.ID
                     join Raw_Category in db.Raw_Category
                    on Raw_SubCategory.Raw_CategoryFK equals Raw_Category.ID
                     where Common_Supplier.Active == true
                     && Mkt_PO.Active == true
                     && Raw_Category.Active == true
                     && Mkt_POSlave.Active == true
                     && t2.Active == true
                     && t6.Active == true
                     && t7.Active == true
                     && Raw_SubCategory.Active == true
                     && Raw_Item.Active == true


                     //&& Mkt_PO.Mkt_BOMFK == id
                     select new VmMkt_PO
                     {
                         Common_TheOrder = t6,
                         Common_Currency = t7,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,

                     }).Where(x => x.Mkt_PO.Active == true).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            List<VmMkt_PO> list = new List<VmMkt_PO>();
            foreach (VmMkt_PO x in c)
            {
                x.Total_Value = TotlaValue(x.Mkt_PO.ID);
                x.Total_Paid = TotlaPaid(x.Mkt_PO.ID);
                x.Total_Due = x.Total_Value - x.Total_Paid;
                x.GoodReceived = GoodReceivedValue(x.Mkt_PO.ID);
                x.Payable = x.GoodReceived - x.Total_Paid;
                list.Add(x);
            }
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);

            }
            else
            {

                this.DataList = list;

                //var bb = list.ToList();
            }


        }

        internal void PurchaseOrderAccessoriesView(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder on t2.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
                     join Mkt_POSlave in db.Mkt_POSlave on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     join Raw_Item in db.Raw_Item on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Raw_SubCategory in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals Raw_SubCategory.ID
                     join Raw_Category in db.Raw_Category on Raw_SubCategory.Raw_CategoryFK equals Raw_Category.ID
                     where
                     Common_Supplier.Active == true
                     && Mkt_PO.Active == true
                     && Raw_Category.Active == true
                     && Mkt_POSlave.Active == true
                     && t2.Active == true
                     && t6.Active == true
                     && t6.IsComplete == false
                     && t7.Active == true
                     && Raw_SubCategory.Active == true
                     && Raw_Item.Active == true
                     && (Raw_Category.ID != 1007 && Raw_Category.ID != 1013 && Raw_Category.ID != 1019 && Raw_Category.ID != 1020 && Raw_Category.ID != 1024 && Raw_Category.ID != 1031)
                     select new VmMkt_PO
                     {
                         Common_TheOrder = t6,
                         Common_Currency = t7,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,

                     }).Where(x => x.Mkt_PO.Active == true).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            //if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 2)
            //{
            //    this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);
            //}
            //else
            //{
            //    this.DataList = c;
            //}
            this.DataList = c;

        }
        internal void GetPurchaseOrderAccessories()
        {
            db = new xOssContext();
            var c = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in db.Common_Currency on t1.Common_CurrencyFK equals t3.ID
                     join t4 in db.User_User on t1.FirstCreatedBy equals t4.ID
                     where t1.Mkt_BOMFK == 1 && t1.Active == true && t1.IsComplete == false
                     select new VmMkt_PO
                     {
                         Common_Currency = t3,
                         Mkt_PO = t1,
                         Common_Supplier = t2,
                         User_User = t4
                     }).OrderByDescending(x => x.Mkt_PO.ID).ToList();


            this.VmMkt_POList = c;
            //}
            //else
            //{
            //    this.VmMkt_POList = c.Where(x => x.User_User.ID == this.User_User.ID);
            //}
        }
        internal void PurchaseOrderFabricView(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder on t2.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
                     join Mkt_POSlave in db.Mkt_POSlave on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     join Raw_Item in db.Raw_Item on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Raw_SubCategory in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals Raw_SubCategory.ID
                     join Raw_Category in db.Raw_Category on Raw_SubCategory.Raw_CategoryFK equals Raw_Category.ID
                     where
                     Common_Supplier.Active == true
                     && Mkt_PO.Active == true
                     && Raw_Category.Active == true
                     && Mkt_POSlave.Active == true
                     && t2.Active == true
                     && t6.Active == true
                     && t6.IsComplete == false
                     && t7.Active == true
                     && Raw_SubCategory.Active == true
                     && Raw_Item.Active == true
                     && (Raw_Category.ID == 1007 || Raw_Category.ID == 1013 || Raw_Category.ID == 1019 ||  Raw_Category.ID == 1020 || Raw_Category.ID == 1024 || Raw_Category.ID == 1031)

                     //&& Mkt_PO.Mkt_BOMFK == id
                     select new VmMkt_PO
                     {
                         Common_TheOrder = t6,
                         Common_Currency = t7,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User
                     }).Where(x => x.Mkt_PO.Active == true).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = c;
                //var bb = list.ToList();
            }
        }

        internal void PurchaseOrderRequsitionView(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t7 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
                     join Mkt_POSlave in db.Mkt_POSlave on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     join t2 in db.Prod_Requisition_Slave on Mkt_POSlave.RequisitionSlaveFK equals t2.ID
                     join t3 in db.Mkt_BOM on t2.Mkt_BOMFK equals t3.ID
                     join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                     join Raw_Item in db.Raw_Item on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Raw_SubCategory in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals Raw_SubCategory.ID
                     join Raw_Category in db.Raw_Category on Raw_SubCategory.Raw_CategoryFK equals Raw_Category.ID
                     where
                     Common_Supplier.Active == true
                     && Mkt_PO.Active == true
                     && Raw_Category.Active == true
                     && Mkt_POSlave.Active == true
                     && t2.Active == true
                     && t4.Active == true
                     && t7.Active == true
                     && Raw_SubCategory.Active == true
                     && Raw_Item.Active == true
                     select new VmMkt_PO
                     {
                         Common_TheOrder = t4,
                         Common_Currency = t7,
                         Mkt_BOM = t3,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User
                     }).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();


            //var c = (from Mkt_PO in db.Mkt_PO
            //         join Common_Supplier in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
            //         join t2 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t2.ID
            //         join t6 in db.Common_TheOrder on t2.Common_TheOrderFk equals t6.ID
            //         join t7 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t7.ID
            //         join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
            //         join Mkt_POSlave in db.Mkt_POSlave on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
            //         join Raw_Item in db.Raw_Item on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
            //         join Raw_SubCategory in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals Raw_SubCategory.ID
            //         join Raw_Category in db.Raw_Category on Raw_SubCategory.Raw_CategoryFK equals Raw_Category.ID
            //         where
            //         Common_Supplier.Active == true
            //         && Mkt_PO.Active == true
            //         && Raw_Category.Active == true
            //         && Mkt_POSlave.Active == true
            //         && t2.Active == true
            //         && t6.Active == true
            //         && t7.Active == true
            //         && Raw_SubCategory.Active == true
            //         && Raw_Item.Active == true

            //         //&& Mkt_PO.Mkt_BOMFK == id
            //         select new VmMkt_PO
            //         {
            //             Common_TheOrder = t6,
            //             Common_Currency = t7,
            //             Mkt_BOM = t2,
            //             Mkt_PO = Mkt_PO,
            //             Common_Supplier = Common_Supplier,
            //             User_User = User_User,

            //         }).Where(x => x.Mkt_PO.Active == true).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = c;
            }
        }

        internal void GetPOWithoutOrder(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder
                     on t2.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User
                     on Mkt_PO.FirstCreatedBy equals User_User.ID
                     where Common_Supplier.Active == true

                     && t2.Active == true
                     && t6.Active == true
                     && t7.Active == true
                     && Mkt_PO.IsAuthorize == false
                     select new VmMkt_PO
                     {
                         Common_TheOrder = t6,
                         Common_Currency = t7,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,

                     }).Where(x => x.Mkt_PO.Active == true).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            List<VmMkt_PO> list = new List<VmMkt_PO>();

            if (this.User_User.User_AccessLevelFK == 2 && this.User_User.User_DepartmentFK == 6)
            {
                var xx = c.Where(x => x.User_User.ID == this.User_User.ID);
                foreach (VmMkt_PO x in xx)
                {
                    x.Total_Value = TotlaValue(x.Mkt_PO.ID);
                    list.Add(x);
                }
                this.DataList = list;
            }
            else
            {

                foreach (VmMkt_PO x in c)
                {
                    x.Total_Value = TotlaValue(x.Mkt_PO.ID);
                    list.Add(x);
                }
                this.DataList = list;


            }


        }
        internal void GetPO(int id, bool isclosed)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder
                     on t2.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User
                     on Mkt_PO.FirstCreatedBy equals User_User.ID
                     // join Mkt_POSlave in db.Mkt_POSlave
                     //on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     // join Raw_Item in db.Raw_Item
                     // on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     // join Raw_SubCategory in db.Raw_SubCategory
                     //on Raw_Item.Raw_SubCategoryFK equals Raw_SubCategory.ID
                     // join Raw_Category in db.Raw_Category
                     //on Raw_SubCategory.Raw_CategoryFK equals Raw_Category.ID
                     where Common_Supplier.Active == true

                     // && Raw_Category.Active == true
                     //&& Mkt_POSlave.Active == true
                     && t2.Active == true
                     && t6.Active == true
                     && t7.Active == true
                     && Mkt_PO.IsComplete == isclosed
                     && Mkt_PO.IsAuthorize == false

                     //&& Mkt_PO.Mkt_BOMFK == id
                     select new VmMkt_PO
                     {
                         Common_TheOrder = t6,
                         Common_Currency = t7,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,
                         Total_Value = (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && x.Active == true select x.TotalRequired * x.Price).Sum() == 0 ? 0 : (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired * x.Price).Sum()
                         //Total_Value = (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select new { x.TotalRequired, x.Price }).Sum(x => x.TotalRequired * x.Price),
                     }).Where(x => x.Mkt_PO.Active == true).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            //List<VmMkt_PO> list = new List<VmMkt_PO>();
            //foreach (VmMkt_PO x in c)
            //{
            //    x.Total_Value = TotlaValue(x.Mkt_PO.ID);
            //    //x.Total_Paid = TotlaPaid(x.Mkt_PO.ID);
            //    //x.Total_Due = x.Total_Value - x.Total_Paid;
            //    //x.GoodReceived = GoodReceivedValue(x.Mkt_PO.ID);
            //    //x.Payable = x.GoodReceived - x.Total_Paid;
            //    list.Add(x);
            //}
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);

            }
            else
            {

                this.DataList = c;

                //var bb = list.ToList();
            }


        }

        internal void GetPOAll(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder on t2.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
                     // join Mkt_POSlave in db.Mkt_POSlave
                     //on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     // join Raw_Item in db.Raw_Item
                     // on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     // join Raw_SubCategory in db.Raw_SubCategory
                     //on Raw_Item.Raw_SubCategoryFK equals Raw_SubCategory.ID
                     // join Raw_Category in db.Raw_Category
                     //on Raw_SubCategory.Raw_CategoryFK equals Raw_Category.ID
                     where Common_Supplier.Active == true

                     // && Raw_Category.Active == true
                     //&& Mkt_POSlave.Active == true
                     && t2.Active == true
                     && t6.Active == true
                     && t7.Active == true
                     && Mkt_PO.IsComplete == false


                     //&& Mkt_PO.Mkt_BOMFK == id
                     select new VmMkt_PO
                     {
                         Common_TheOrder = t6,
                         Common_Currency = t7,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,
                         Total_Value = (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired * x.Price).Sum() == 0 ? 0 : (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired * x.Price).Sum()

                     }).Where(x => x.Mkt_PO.Active == true).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            //List<VmMkt_PO> list = new List<VmMkt_PO>();
            //foreach (VmMkt_PO x in c)
            //{
            //    x.Total_Value = TotlaValue(x.Mkt_PO.ID);
            //    //x.Total_Paid = TotlaPaid(x.Mkt_PO.ID);
            //    //x.Total_Due = x.Total_Value - x.Total_Paid;
            //    //x.GoodReceived = GoodReceivedValue(x.Mkt_PO.ID);
            //    //x.Payable = x.GoodReceived - x.Total_Paid;
            //    list.Add(x);
            //}
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);

            }
            else
            {

                this.DataList = c;

                //var bb = list.ToList();
            }


        }

        internal void GetPOByBOM(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder on t2.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
                     where Common_Supplier.Active == true && Mkt_PO.Active == true && t2.Active == true && t6.Active == true && t7.Active == true
                     && Mkt_PO.Mkt_BOMFK == id
                     select new VmMkt_PO
                     {
                         Common_TheOrder = t6,
                         Common_Currency = t7,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,
                         Total_Value = (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && x.Active == true select x.TotalRequired * x.Price).Sum() == 0 ? 0 : (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && x.Active == true select x.TotalRequired * x.Price).Sum()

                     }).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();
            //List<VmMkt_PO> list = new List<VmMkt_PO>();
            //foreach (VmMkt_PO x in c)
            //{
            //    x.Total_Value = TotlaValue(x.Mkt_PO.ID);
            //    list.Add(x);
            //}
            this.DataList = c;


        }

        public void PeriodicGetPO()
        {
            db = new xOssContext();
            var c = (from Commercial_MasterLCs in db.Commercial_MasterLC
                     join Commercial_MasterLCBuyerPO in db.Commercial_MasterLCBuyerPO on Commercial_MasterLCs.ID equals Commercial_MasterLCBuyerPO.Commercial_MasterLCFK
                     join mkt_BOM in db.Mkt_BOM on Commercial_MasterLCBuyerPO.MKTBOMFK equals mkt_BOM.ID
                     join Mkt_PO in db.Mkt_PO on mkt_BOM.ID equals Mkt_PO.Mkt_BOMFK
                     join Common_Supplier in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join Common_Currency in db.Common_Currency on Mkt_PO.Common_CurrencyFK equals Common_Currency.ID
                     join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
                     where
                     Commercial_MasterLCs.Active == true &&
                     Mkt_PO.Active == true &&
                     Common_Supplier.Active == true &&
                     mkt_BOM.Active == true &&
                     Common_Currency.Active == true
                     select new VmMkt_PO
                     {
                         Common_Currency = Common_Currency,
                         Mkt_BOM = mkt_BOM,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,
                         Total_Value = (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired * x.Price).Sum() == 0 ? 0 : (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired * x.Price).Sum()

                     }).Where(x => x.Mkt_PO.Active == true).AsEnumerable();
            //List<VmMkt_PO> list = new List<VmMkt_PO>();
            //foreach (VmMkt_PO x in c)
            //{
            //    x.Total_Value = TotlaValue(x.Mkt_PO.ID);
            //    //x.Total_Paid = TotlaPaid(x.Mkt_PO.ID);
            //    //x.Total_Due = x.Total_Value - x.Total_Paid;
            //    //x.GoodReceived = GoodReceivedValue(x.Mkt_PO.ID);
            //    //x.Payable = x.GoodReceived - x.Total_Paid;
            //    list.Add(x);
            //}
            this.DataList = c;
        }

        internal void GetAuthorizedPurchaseOrders()
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder
                     on t2.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User
                     on Mkt_PO.FirstCreatedBy equals User_User.ID

                     where Mkt_PO.IsAuthorize == true




                     select new VmMkt_PO
                     {
                         Common_TheOrder = t6,
                         Common_Currency = t7,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,
                         Total_Value = (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired * x.Price).Sum() == 0 ? 0 : (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired * x.Price).Sum()

                     }).Where(x => x.Mkt_PO.Active == true).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            //List<VmMkt_PO> list = new List<VmMkt_PO>();
            //foreach (VmMkt_PO x in c)
            //{
            //    x.Total_Value = TotlaValue(x.Mkt_PO.ID);
            //    //    x.Total_Paid = TotlaPaid(x.Mkt_PO.ID);
            //    //    x.Total_Due = x.Total_Value - x.Total_Paid;
            //    //    x.GoodReceived = GoodReceivedValue(x.Mkt_PO.ID);
            //    //    x.Payable = x.GoodReceived - x.Total_Paid;
            //    list.Add(x);
            //}
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);

            }
            else
            {

                this.DataList = c;

                //var bb = list.ToList();
            }
            //this.DataList = list;

        }
        
        internal void GetAuthorizedPurchaseOrdersSearch(string SearchString)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t6 in db.Common_TheOrder
                     on t2.Common_TheOrderFk equals t6.ID
                     join t7 in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals t7.ID
                     join User_User in db.User_User
                     on Mkt_PO.FirstCreatedBy equals User_User.ID

                     where Mkt_PO.Active == true
                     && Mkt_PO.IsAuthorize == true
                     select new VmMkt_PO
                     {
                         Common_TheOrder = t6,
                         Common_Currency = t7,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,
                         Total_Value = (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && x.Active == true select x.TotalRequired * x.Price).Sum() == 0 ? 0 : (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired * x.Price).Sum()

                     }).Where(x => x.Mkt_PO.Active == true && x.Mkt_PO.CID.Contains(SearchString)).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            //List<VmMkt_PO> list = new List<VmMkt_PO>();
            //foreach (VmMkt_PO x in c)
            //{
            //    x.Total_Value = TotlaValue(x.Mkt_PO.ID);
            //    x.Total_Paid = TotlaPaid(x.Mkt_PO.ID);
            //    x.Total_Due = x.Total_Value - x.Total_Paid;
            //    x.GoodReceived = GoodReceivedValue(x.Mkt_PO.ID);
            //    x.Payable = x.GoodReceived - x.Total_Paid;
            //    list.Add(x);
            //}
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = c;

                //var bb = list.ToList();
            }
            //this.DataList = list;

        }
        
        /// <summary>
        /// ///////////////
        /// </summary>
        /// <param name="id"></param>
        internal void PurchaseOrderViewBySuplier(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join co in db.Common_TheOrder
                     on t2.Common_TheOrderFk equals co.ID
                     join t3 in db.Mkt_Buyer on co.Mkt_BuyerFK equals t3.ID
                     where Mkt_PO.Common_SupplierFK == id

                     select new VmMkt_PO
                     {
                         Common_TheOrder = co,
                         Mkt_Buyer = t3,
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier
                     }).Where(x => x.Mkt_PO.Active == true).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            List<VmMkt_PO> list = new List<VmMkt_PO>();
            foreach (VmMkt_PO x in c)
            {
                x.Total_Value = TotlaValue(x.Mkt_PO.ID);
                x.Total_Paid = TotlaPaid(x.Mkt_PO.ID);
                x.Total_Due = x.Total_Value - x.Total_Paid;
                x.GoodReceived = GoodReceivedValue(x.Mkt_PO.ID);
                x.Payable = x.GoodReceived - x.Total_Paid;
                list.Add(x);
            }

            this.DataList = list;
            //var bb = list.ToList();
        }
        internal void PurchaseOrderSearch(string SearchString)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join User_User in db.User_User
                     on Mkt_PO.FirstCreatedBy equals User_User.ID

                     where Mkt_PO.Active == true
                     select new VmMkt_PO
                     {
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User

                     }).Where(x => x.Mkt_PO.Active == true && x.Mkt_PO.CID.Contains(SearchString)).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            if (this.User_User.User_AccessLevelFK == 1)
            {
                this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = c;
                var bb = c.ToList();
            }


        }
        public int Add(int userID)
        {
            Mkt_PO.AddReady(userID);
            return Mkt_PO.Add();

        }
        public bool Edit(int userID)
        {
            return Mkt_PO.Edit(userID);
        }
        public bool EditReceiving(int userID)
        {
            return Mkt_POSlave_Receiving.Edit(userID);
        }
        internal void GetPOSlave(int id)
        {
            //db = new xOssContext();
            //var a = (from Mkt_POSlave in db.Mkt_POSlave
            //         join Common_Unit in db.Common_Unit
            //         on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
            //         join Raw_Item in db.Raw_Item
            //         on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
            //         join Mkt_PO in db.Mkt_PO
            //         on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
            //         where Mkt_POSlave.Active == true
            //         && Mkt_POSlave.Mkt_POFK == id
            //         select new VmMktPO_Slave
            //         {
            //             Mkt_POSlave = Mkt_POSlave,
            //             Common_Unit = Common_Unit,
            //             Mkt_PO = Mkt_PO,
            //             Raw_Item = Raw_Item

            //         }).Where(x => x.Mkt_POSlave.Active == true).OrderByDescending(x => x.Mkt_POSlave.ID).AsEnumerable();
            //this.DataList = a;

        }

        public VmMkt_PO SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from mkt_PO in db.Mkt_PO
                     join common_Supplier in db.Common_Supplier
                     on mkt_PO.Common_SupplierFK equals common_Supplier.ID
                     join mkt_BOM in db.Mkt_BOM
                     on mkt_PO.Mkt_BOMFK equals mkt_BOM.ID
                     join common_TheOrder in db.Common_TheOrder
                     on mkt_BOM.Common_TheOrderFk equals common_TheOrder.ID
                     join common_Currency in db.Common_Currency
                     on mkt_PO.Common_CurrencyFK equals common_Currency.ID


                     //join common_Unit in db.Common_Unit
                     //on mkt_PO.Common_UnitFK equals common_Unit.ID
                     where mkt_PO.ID == iD
                     select new VmMkt_PO
                     {
                         Common_Currency = common_Currency,
                         Common_TheOrder = common_TheOrder,
                         Mkt_BOM = mkt_BOM,
                         Mkt_PO = mkt_PO,
                         Common_Supplier = common_Supplier,
                         //Common_Unit = common_Unit
                     }).FirstOrDefault();
            return v;
        }
        public VmMkt_PO SelectSingleJoinedGEN(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in db.Common_Currency on t1.Common_CurrencyFK equals t3.ID
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     where t1.ID == iD
                     select new VmMkt_PO
                     {
                         Mkt_PO = t1,
                         Common_Supplier = t2,
                         Common_Currency = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         //Total_Value = (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired).Sum() == null ? 0 : (from x in db.Mkt_POSlave where x.Mkt_POFK == Mkt_PO.ID && Mkt_PO.Active == true select x.TotalRequired).Sum()

                     }).FirstOrDefault();
            return v;
        }

        //.....PurchaseOrderList...///
        public void SelectSingleForPOList(int? id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_PO

                     join t3 in db.Common_Supplier on t1.Common_SupplierFK equals t3.ID
                     where t1.Active == true
                     select new VmMkt_PO
                     {
                         Mkt_PO = t1,

                         Common_Supplier = t3
                     }).Where(c => c.Mkt_PO.ID == id).SingleOrDefault();

            this.Mkt_PO = a.Mkt_PO;
            this.Common_Supplier = a.Common_Supplier;
        }
        //....................../Return to Supplier/++++............................//
        public void SelectSingle1(int? id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_PO
                     join t3 in db.Common_Supplier on t1.Common_SupplierFK equals t3.ID
                     where t1.Active == true
                     select new VmMkt_PO
                     {
                         Mkt_PO = t1,
                         Common_Supplier = t3
                     }).Where(c => c.Mkt_PO.ID == id).SingleOrDefault();

            this.Mkt_PO = a.Mkt_PO;
            InitialDataLoad_PoSlave1(id.Value);
        }
        public void InitialDataLoad_PoSlave1(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
                     join t5 in db.Common_Supplier on t4.Common_SupplierFK equals t5.ID
                     where t1.Active == true
                     select new VmMktPO_Slave
                     {
                         Mkt_POSlave = t1,
                         Mkt_PO = t4,
                         Raw_Item = t2,
                         Common_Unit = t3,
                         Common_Supplier = t5
                     }).Where(x => x.Mkt_POSlave.Mkt_POFK == id).AsEnumerable();
            List<VmMktPO_Slave> tempVmMktPO_Slave = new List<VmMktPO_Slave>();
            var data = v.FirstOrDefault();
            if (data != null)
            {
                this.Mkt_PO = data.Mkt_PO;
                this.Common_Supplier = data.Common_Supplier;

                foreach (VmMktPO_Slave x in v)
                {
                    x.PreviousReturned = GetPreviousReturned(x.Mkt_POSlave.ID);
                    x.TotalStock = GetTotalStock(x.Mkt_POSlave.ID);
                    tempVmMktPO_Slave.Add(x);

                }
            }

            this.DataList2 = tempVmMktPO_Slave;
        }
        public decimal? GetPreviousReturned(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POSlaveFK == id && t1.IsReturn == true
                     select (t1.Quantity)).ToList();
            decimal? quantity = 0;
            if (v != null)
            {
                quantity = v.Sum();
            }
            return quantity;
        }
        public decimal? GetPreviousExtReturned(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POExtTransferFK == id && t1.IsReturn == true
                     select (t1.Quantity)).ToList();
            decimal? quantity = 0;
            if (v != null)
            {
                quantity = v.Sum();
            }
            return quantity;
        }
        public int AddPOSlave_Return(int userID)
        {

            Mkt_POSlave_Receiving.AddReady(userID);
            return Mkt_POSlave_Receiving.Add();
        }
        public void VmPOSlave_Return(int userID, IEnumerable<VmMktPO_Slave> DataList2)
        {

            foreach (VmMktPO_Slave x in DataList2)
            {
                if (x.Receiving != null)
                {
                    Mkt_POSlave_Receiving.Quantity = x.Receiving;
                    Mkt_POSlave_Receiving.Mkt_POSlaveFK = x.Mkt_POSlave.ID;
                    Mkt_POSlave_Receiving.Date = this.Mkt_POSlave_Receiving.Date;
                    Mkt_POSlave_Receiving.Challan = this.Mkt_POSlave_Receiving.Challan;
                    Mkt_POSlave_Receiving.User_DepartmentFk = this.Mkt_POSlave_Receiving.User_DepartmentFk;
                    Mkt_POSlave_Receiving.IsReturn = true;
                    AddPOSlave_Return(userID);
                }


            }

        }
        //....................../Return from Production/++++............................//
        public void InitialDataLoad_ReturnFromProduction(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_Supplier on t4.Common_SupplierFK equals t6.ID
                     where t1.Active == true
                     select new VmMktPO_Slave
                     {
                         Mkt_PO = t4,
                         Mkt_BOM = t5,
                         Mkt_POSlave = t1,
                         Raw_Item = t2,
                         Common_Unit = t3,
                         Common_Supplier = t6
                     }).Where(x => x.Mkt_BOM.ID == id).AsEnumerable();
            List<VmMktPO_Slave> tempVmMktPO_Slave = new List<VmMktPO_Slave>();
            var data = v.FirstOrDefault();
            if (data != null)
            {
                this.Mkt_BOM = data.Mkt_BOM;
                this.Mkt_PO = data.Mkt_PO;
                this.Common_Supplier = data.Common_Supplier;

            }
            List<VmMktPO_Slave> tempVmMktPO_Slave1 = new List<VmMktPO_Slave>();
            foreach (VmMktPO_Slave x in v)
            {
                x.PreviousDeliveryReturn = GetPreviousDeliveryReturn(x.Mkt_POSlave.ID);
                tempVmMktPO_Slave1.Add(x);

            }

            this.DataList2 = tempVmMktPO_Slave1;
        }
        public decimal? GetPreviousDeliveryReturn(int id)
        {
            db = new xOssContext();
            var delevierd = (from t1 in db.Mkt_POSlave_Consumption
                             where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                             select (t1.Quantity)).DefaultIfEmpty(0).Sum();

            var delevirdReturn = (from t1 in db.Mkt_POSlave_Consumption
                                  where t1.Mkt_POSlaveFK == id && t1.IsReturn == true
                                  select (t1.Quantity)).DefaultIfEmpty(0).Sum();

            decimal? quantity = delevierd - delevirdReturn;
            return quantity;
        }
        public decimal? GetPreviousExtDeliveryReturn(int id)
        {
            db = new xOssContext();
            var quantity = (from t1 in db.Mkt_POSlave_Consumption
                            where t1.Mkt_POSlaveFK == id && t1.IsReturn == true
                            select (t1.Quantity)).DefaultIfEmpty(0).Sum();
            return quantity;
        }
        public int AddPOSlave_DeliveryReturn(int userID)
        {

            Mkt_POSlave_Consumption.AddReady(userID);
            return Mkt_POSlave_Consumption.Add();
        }
        public void VmPOSlave_DeliveryReturn(int userID, IEnumerable<VmMktPO_Slave> DataList2)
        {

            foreach (VmMktPO_Slave x in DataList2)
            {
                if (x.Delivery == null)
                {
                    Mkt_POSlave_Consumption.Quantity = 0;
                }
                else
                {
                    Mkt_POSlave_Consumption.Quantity = x.Delivery;
                }
                Mkt_POSlave_Consumption.Mkt_POSlaveFK = x.Mkt_POSlave.ID;
                Mkt_POSlave_Consumption.Date = this.Mkt_POSlave_Consumption.Date;

                Mkt_POSlave_Consumption.User_DepartmentFk = this.Mkt_POSlave_Consumption.User_DepartmentFk;
                Mkt_POSlave_Consumption.IsReturn = true;
                AddPOSlave_DeliveryReturn(userID);
            }
        }


        internal void POReportWithoutBuyerOrderDocLoad(int id)
        {
            db = new xOssContext();
            var v = (from Mkt_POSlave in db.Mkt_POSlave
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals Common_Currency.ID
                     join t1 in db.Prod_Requisition on Mkt_PO.RequisitionFK equals t1.ID
                     //join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                     //join t3 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t3.ID
                     //join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
                     where Mkt_PO.ID == id && Mkt_POSlave.Active == true

                     select new ReportDocType
                     {
                         HeaderLeft1 = Common_Supplier.Name +
                                        "\n" + "Email: " + Common_Supplier.Email +
                                        "\n" + "Address: " + Common_Supplier.Address +
                                        "\n" + "Mobile: " + Common_Supplier.Mobile,
                         HeaderLeft3 = Common_Supplier.Email,
                         HeaderLeft2 = Common_Supplier.Address,

                         HeaderLeft4 = Common_Supplier.Mobile,

                         HeaderRight1 = Mkt_PO.CID,
                         HeaderRight2 = Mkt_PO.Date.ToString(),
                         //HeaderRight3 = t1.CID,
                         // HeaderRight4 = t3.Name,
                         HeaderRight5 = t1.RequisitionCID,
                         //HeaderRight6 = User_User.Name,
                         Body1 = Raw_Item.Name,
                         Body2 = Mkt_POSlave.Description,
                         Body3 = Mkt_POSlave.TotalRequired.ToString(),
                         Body4 = Common_Unit.Name,
                         Body5 = Mkt_POSlave.Price.ToString(),
                         Body6 = (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price).ToString(),
                         Body7 = Common_Currency.Symbol,
                         SubBody3 = Mkt_PO.Description,
                         Body10 = Mkt_POSlave.TotalRequired.ToString(),
                         Body11 = Mkt_PO.IsAuthorize.ToString(),
                         Body12 = ""


                     }).ToList();
            decimal total = 0;
            decimal totalQue = 0;
            foreach (ReportDocType r in v)
            {
                decimal body6 = 0;
                decimal body10 = 0;
                decimal.TryParse(r.Body10, out body10);
                decimal.TryParse(r.Body6, out body6);

                total += body6;
                totalQue += body10;
            }
            foreach (ReportDocType r in v)
            {
                r.SubBody1 = r.Body7 + total.ToString("F");
                r.Body10 = totalQue.ToString("F");
                decimal temp = 0;

                Common.VmForDropDown ntw = new Common.VmForDropDown();

                r.SubBody2 = ntw.NumberToWords(total, Common.CurrencyType.USD) + ".";
                decimal.TryParse(r.Body3, out temp);
                r.Body3 = temp.ToString("F");

                decimal.TryParse(r.Body5, out temp);
                r.Body5 = r.Body7 + temp.ToString("N5"); // Tostring(N5) return Number.00000


                decimal.TryParse(r.Body6, out temp);
                r.Body6 = r.Body7 + temp.ToString("F");

                r.HeaderRight2 = r.HeaderRight2.Substring(0, r.HeaderRight2.Length - 8);

                bool body11 = false;
                bool.TryParse(r.Body11, out body11);

                if (body11 != false)
                {
                    r.Body12 = "";
                }
                else
                {
                    r.Body12 = "This PO is not authorized yet. Please Contact with Admin";
                }

            }
            this.POReportDoc = v.ToList();
        }
        internal void POReportDocLoad(int id)
        {
            db = new xOssContext();
            VmForDropDown ntw = new VmForDropDown();

            var v = (from Mkt_POSlave in db.Mkt_POSlave
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals Common_Currency.ID
                     join t1 in db.Mkt_BOM on Mkt_PO.Mkt_BOMFK equals t1.ID
                     join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                     join t3 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t3.ID
                     join User_User in db.User_User on Mkt_PO.FirstCreatedBy equals User_User.ID
                     where Mkt_PO.ID == id && Mkt_POSlave.Active == true

                     select new ReportDocType
                     {
                         HeaderLeft1 = Common_Supplier.Name,
                         HeaderLeft2 = "Email: " + Common_Supplier.Email + "\nAddress: " + Common_Supplier.Address + "\nMobile: " + Common_Supplier.Mobile,
                         int1 = t1.ID,
                         //HeaderLeft4 = Common_Supplier.Mobile,

                         HeaderRight1 = Mkt_PO.CID,
                         HeaderRight2 = Mkt_PO.Date.ToString(),
                         HeaderRight3 = t1.CID,
                         HeaderRight4 = t3.Name,
                         HeaderRight5 = t2.CID,
                         HeaderRight6 = User_User.Name,
                         Body1 = Raw_Item.Name,
                         Body2 = Mkt_POSlave.Description,
                         Body3 = Mkt_POSlave.TotalRequired.ToString(),
                         Body4 = Common_Unit.Name,
                         Body5 = Mkt_POSlave.Price.ToString(),
                         Body6 = (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price).ToString(),
                         Body7 = Common_Currency.Symbol,
                         SubBody3 = Mkt_PO.Description,
                         Body10 = Mkt_POSlave.TotalRequired.ToString(),
                         Body11 = Mkt_PO.IsAuthorize.ToString(),
                         Body12 = "",
                         Body13 = ""


                     }).ToList();
            decimal total = 0;
            decimal totalQue = 0;
            foreach (ReportDocType r in v)
            {
                decimal body6 = 0;
                decimal body10 = 0;
                decimal.TryParse(r.Body10, out body10);
                decimal.TryParse(r.Body6, out body6);

                total += body6;
                totalQue += body10;
            }
            foreach (ReportDocType r in v)
            {
                r.SubBody1 = r.Body7 + total.ToString("F");
                r.Body10 = totalQue.ToString("F");
                decimal temp = 0;



                if (r.int1 == 1)
                {
                    r.SubBody2 = "BDT" + " " + InWordFunction.ConvertToWords(total.ToString("0.##"));

                }
                else
                {
                    r.SubBody2 = ntw.NumberToWords(total, Common.CurrencyType.USD) + ".";

                }
                decimal.TryParse(r.Body3, out temp);
                r.Body3 = temp.ToString("F");

                decimal.TryParse(r.Body5, out temp);
                r.Body5 = r.Body7 + temp.ToString("N5"); // Tostring(N5) return Number.00000


                decimal.TryParse(r.Body6, out temp);
                r.Body6 = r.Body7 + temp.ToString("F");

                r.HeaderRight2 = r.HeaderRight2.Substring(0, r.HeaderRight2.Length - 8);

                bool body11 = false;
                bool.TryParse(r.Body11, out body11);

                if (body11 != false)
                {
                    r.Body13 = "This PO is computer generated, no need signature";
                    r.Body12 = "";
                }
                else
                {
                    r.Body12 = "This PO is not authorized yet. Please Contact with Admin";
                    r.Body13 = "";
                }

            }
            this.POReportDoc = v.ToList();
        }

        public void InitialDataLoad_PoSlaveReturnREQ(int id)
        {
            db = new xOssContext();

            var v = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     select new VmProd_Requisition_Slave
                     {
                         Prod_Requisition = t4,
                         Mkt_BOM = t5,
                         Prod_Requisition_Slave = t1,
                         Raw_Item = t2,
                         Common_Unit = t3,
                     }).Where(x => x.Prod_Requisition_Slave.Prod_RequisitionFK == id).AsEnumerable();
            List<VmProd_Requisition_Slave> tempVmProd_Requisition_Slave = new List<VmProd_Requisition_Slave>();
            var ss = v.FirstOrDefault();
            this.Mkt_BOM = ss.Mkt_BOM;
            this.Prod_Requisition = ss.Prod_Requisition;
            List<VmProd_Requisition_Slave> tempVmProd_Requisition_Slave1 = new List<VmProd_Requisition_Slave>();
            foreach (VmProd_Requisition_Slave x in v)
            {
                x.PreviousDeliveryReturn = GetPreviousDeliveryReturnREQ(x.Prod_Requisition_Slave.ID);
                x.TotalStock = GetTotalStock(x.Prod_Requisition_Slave.ID);
                tempVmProd_Requisition_Slave1.Add(x);
            }
            this.DataList3 = tempVmProd_Requisition_Slave1;
        }
        public decimal? GetPreviousDeliveryReturnREQ(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave_Consumption
                     where t1.Prod_Requisition_SlaveFK == id && t1.IsReturn == true
                     select (t1.Quantity)).Sum();
            return v;
        }
        public void VmRequisition_ReturnCreateREQ(int userID, IEnumerable<VmProd_Requisition_Slave> DataList3)
        {
            Mkt_POSlave_Consumption Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
            foreach (VmProd_Requisition_Slave x in DataList3)
            {
                if (x.Delivery == null)
                {
                    Mkt_POSlave_Consumption.Quantity = 0;
                }
                else
                {
                    Mkt_POSlave_Consumption.Quantity = x.Delivery;
                }
                Mkt_POSlave_Consumption.Requisition = this.Prod_Requisition.RequisitionCID;
                Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = x.Prod_Requisition_Slave.ID;
                Mkt_POSlave_Consumption.Date = this.Mkt_POSlave_Consumption.Date;
                Mkt_POSlave_Consumption.Supervisor = this.Mkt_POSlave_Consumption.Supervisor;
                Mkt_POSlave_Consumption.IsReturn = true;
                this.Mkt_POSlave_Consumption = Mkt_POSlave_Consumption;
                AddReturnCreateREQ(userID);
            }
        }

        internal void SetPOAuthorization(int userID)
        {
            foreach (var v in DataList)
            {
                Mkt_PO x = new Mkt_PO();
                x.ID = v.Mkt_PO.ID;
                x.IsAuthorize = v.Mkt_PO.IsAuthorize;
                x.Edit(userID);

            }
        }

        public int AddReturnCreateREQ(int userID)
        {
            Mkt_POSlave_Consumption.AddReady(userID);
            return Mkt_POSlave_Consumption.Add();
        }
        //------------------General Stock Out---------------------//
        public void InitialDataLoad_GeneralPoSlaveSend(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
                     join t6 in db.Common_Supplier on t4.Common_SupplierFK equals t6.ID
                     select new VmMktPO_Slave
                     {
                         Mkt_PO = t4,
                         Mkt_POSlave = t1,
                         Raw_Item = t2,
                         Common_Unit = t3,
                         Common_Supplier = t6
                     }).Where(x => x.Mkt_PO.ID == id).AsEnumerable();
            List<VmMktPO_Slave> tempVmMktPO_Slave = new List<VmMktPO_Slave>();
            var ss = v.FirstOrDefault();
            if (ss != null)
            {
                this.Mkt_PO = ss.Mkt_PO;
                this.Common_Supplier = ss.Common_Supplier;
            }
            else
            {
                this.Message = "Thare are no item available";
            }
            List<VmMktPO_Slave> tempVmMktPO_Slave1 = new List<VmMktPO_Slave>();
            foreach (VmMktPO_Slave x in v)
            {
                x.PreviousDelivery = GetPreviousDelivery(x.Mkt_POSlave.ID);
                x.TotalStock = GetTotalStock(x.Mkt_POSlave.ID);
                tempVmMktPO_Slave1.Add(x);

            }
            this.DataList2 = tempVmMktPO_Slave1;
        }
        public void VmGeneralPOSlave_DeliveryCreate(int userID, IEnumerable<VmMktPO_Slave> DataList2)
        {
            foreach (VmMktPO_Slave x in DataList2)
            {
                if (x.Delivery == null)
                {
                    Mkt_POSlave_Consumption.Quantity = 0;
                }
                else
                {
                    Mkt_POSlave_Consumption.Quantity = x.Delivery;
                }
                Mkt_POSlave_Consumption.Mkt_POSlaveFK = x.Mkt_POSlave.ID;
                Mkt_POSlave_Consumption.Date = this.Mkt_POSlave_Consumption.Date;
                Mkt_POSlave_Consumption.IsReturn = false;
                AddGeneralPOSlave_Delivery(userID);
            }
        }
        public int AddGeneralPOSlave_Delivery(int userID)
        {
            Mkt_POSlave_Consumption.AddReady(userID);
            return Mkt_POSlave_Consumption.Add();
        }

        internal void GeneralPurchaseOrderView(int id) //General Stock Index--------------------------//
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t7 in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals t7.ID
                     //join User_User in db.User_User
                     //on Mkt_PO.FirstCreatedBy equals User_User.ID

                     where Mkt_PO.Active == true
                     && Mkt_PO.IsComplete == false
                     && Mkt_PO.Mkt_BOMFK == 1

                     select new VmMkt_PO
                     {
                         Common_Currency = t7,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier
                         //User_User = User_User

                     }).Where(x => x.Mkt_PO.Active == true).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            List<VmMkt_PO> list = new List<VmMkt_PO>();
            foreach (VmMkt_PO x in c)
            {
                x.Total_Value = TotlaValue(x.Mkt_PO.ID);
                x.Total_Paid = TotlaPaid(x.Mkt_PO.ID);
                x.Total_Due = x.Total_Value - x.Total_Paid;
                x.GoodReceived = GoodReceivedValue(x.Mkt_PO.ID);
                x.Payable = x.GoodReceived - x.Total_Paid;
                list.Add(x);
            }
            //if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 1)
            //{
            //    this.DataList = c.Where(x => x.User_User.ID == this.User_User.ID);
            //}
            //else
            //{
            //    this.DataList = list;
            //    //var bb = list.ToList();
            //}

            this.DataList = list;
        }

        public bool POAuthorization(int userID)
        {

            return Mkt_PO.Edit(userID);
        }
        public int Tpo()
        {
            xOssContext db = new xOssContext();
            var a = (from t1 in db.Mkt_PO
                     where t1.Active == true && t1.IsAuthorize != true
                     select Mkt_PO).Count();
            return a;
        }
        public int Tmlc()
        {
            xOssContext db = new xOssContext();
            var a = (from t1 in db.Commercial_MasterLC
                     where t1.Active == true
                     select Commercial_MasterLC).Count();
            return a;
        }
        public int Tb2blc()
        {
            xOssContext db = new xOssContext();
            var a = (from t1 in db.Commercial_B2bLC
                     where t1.Active == true
                     select Commercial_B2bLC).Count();
            return a;
        }

        //------------------------Po stock report---------------------------//
        //public void PoStockReport(int id)
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Mkt_PO
        //             join t2 in db.Mkt_POSlave on t1.ID equals t2.Mkt_POFK
        //             join t3 in db.Mkt_POSlave_Receiving on t2.ID equals t3.Mkt_POSlaveFK
        //             join t4 in db.Common_Unit on t2.Common_UnitFK equals t4.ID
        //             join t5 in db.Raw_Item on t2.Raw_ItemFK equals t5.ID
        //             where t1.ID == id && t3.Quantity != 0
        //             select new VmMkt_PO
        //             {
        //                 Mkt_PO = t1,
        //                 Mkt_POSlave = t2,
        //                 Mkt_POSlave_Receiving = t3,
        //                 Common_Unit = t4,
        //                 Raw_Item = t5
        //             }).OrderByDescending(x => x.Mkt_POSlave_Receiving.Date).AsEnumerable();
        //    this.DataList = v;

        //}
        public void PoDetails(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in db.Mkt_BOM on t1.Mkt_BOMFK equals t3.ID
                     join t4 in db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                     where t1.ID == id
                     select new VmMkt_PO
                     {
                         Mkt_PO = t1,
                         Common_Supplier = t2,
                         Mkt_BOM = t3,
                         Common_Currency = t4
                     }).SingleOrDefault();
            this.Mkt_PO = v.Mkt_PO;
            this.Mkt_BOM = v.Mkt_BOM;
            this.Common_Supplier = v.Common_Supplier;
            this.Common_Currency = v.Common_Currency;
        }

        public void PoRequsitionDetails(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t4 in db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                     where t1.ID == id
                     select new VmMkt_PO
                     {
                         Mkt_PO = t1,
                         Common_Supplier = t2,
                         Common_Currency = t4
                     }).SingleOrDefault();
            this.Mkt_PO = v.Mkt_PO;
            this.Common_Supplier = v.Common_Supplier;
            this.Common_Currency = v.Common_Currency;
        }

        public class Stoked
        {
            public int? POSlaveID { get; set; }
            public int? POID { get; set; }
            [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
            public DateTime Date { get; set; }
            public string ItemName { get; set; }
            public decimal? CurrentQuantity { get; set; }
            public decimal? TotalRequired { get; set; }
            public decimal? Quantity { get; set; }
            public decimal? ReturnQuantity { get; set; }
            public string Store { get; set; }
            public string Challan { get; set; }
            public string UnitName { get; set; }

        }
        public class Delivered
        {

            [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
            public DateTime Date { get; set; }
            public string RequisitionNo { get; set; }
            public bool? IsReturn { get; set; }
            public string Description { get; set; }
            public decimal? Quantity { get; set; }
            public string SupervisorName { get; set; }
            public string Item { get; set; }
            public int BOMID { get; set; }
            public int CONID { get; set; }
            public string Store { get; set; }
        }
        public List<Delivered> DeliverList { get; set; }

        internal void PODeliveredReport(int id)
        {


            db = new xOssContext();

            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Mkt_PO on t1.Mkt_POFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     join t6 in db.Common_Supplier on t4.Common_SupplierFK equals t6.ID
                     join t7 in db.Common_TheOrder on t5.Common_TheOrderFk equals t7.ID
                     join t8 in db.Mkt_POSlave_Consumption on t1.ID equals t8.Mkt_POSlaveFK
                     join t9 in db.User_Department on t8.User_DepartmentFk equals t9.ID

                     where t1.Active == true && t8.Quantity != 0
                     select new Delivered
                     {
                         Date = t8.Date,
                         RequisitionNo = t8.Requisition,
                         Description = t1.Description,
                         Quantity = t8.Quantity,
                         IsReturn = t8.IsReturn,
                         SupervisorName = t8.Supervisor,
                         Item = t2.Name,
                         BOMID = t5.ID,
                         CONID = t8.ID,
                         Store = t9.Name
                     }).Where(x => x.BOMID == id).ToList();
            int xxx = 0;
            string xxxx = "";
            string xxxxx = "";
            foreach (var item in v)
            {
                xxx = item.CONID;
                xxxx = item.Item;
                xxxxx = item.RequisitionNo;
            }
            if (v != null)
            {

                this.DeliverList = v;
            }
            else
            {

                this.Message = "Thare are no item available";
            }

        }
        public void PoStockInRecevedReport(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_POSlave on t1.ID equals t2.Mkt_POFK
                     join t3 in db.Mkt_POSlave_Receiving on t2.ID equals t3.Mkt_POSlaveFK
                     join t4 in db.Common_Unit on t2.Common_UnitFK equals t4.ID
                     join t5 in db.Raw_Item on t2.Raw_ItemFK equals t5.ID
                     join t6 in db.User_Department on t3.User_DepartmentFk equals t6.ID
                     join t7 in db.Mkt_BOM on t1.Mkt_BOMFK equals t7.ID
                     join t8 in db.Common_TheOrder on t7.Common_TheOrderFk equals t8.ID
                     where t1.ID == id && t3.Quantity != 0 && t3.IsReturn == false
                     select new Stoked
                     {
                         POID = t2.Mkt_POFK,
                         POSlaveID = t3.Mkt_POSlaveFK,
                         Date = t3.Date,
                         ItemName = t2.Description == null ? t5.Name + " | " + t8.BuyerPO : t5.Name + " | " + t2.Description + "(" + t8.BuyerPO + ")",
                         TotalRequired = t2.TotalRequired,
                         Quantity = t3.Quantity,
                         Challan = t3.Challan,
                         UnitName = t4.Name,
                         Store = t6.Name
                     }).ToList();
            //decimal? qty = 0;

            //foreach (var x in v)
            //{
            //    qty = GetAllReturnQuantity(x.POSlaveID, x.POID, x.Challan);

            //    x.ReturnQuantity = qty;
            //    x.CurrentQuantity = x.Quantity - qty;

            //}

            this.StokedList = v;


        }
        public void PoStockInReturnReport(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_POSlave on t1.ID equals t2.Mkt_POFK
                     join t3 in db.Mkt_POSlave_Receiving on t2.ID equals t3.Mkt_POSlaveFK
                     join t4 in db.Common_Unit on t2.Common_UnitFK equals t4.ID
                     join t5 in db.Raw_Item on t2.Raw_ItemFK equals t5.ID
                     join t6 in db.User_Department on t3.User_DepartmentFk equals t6.ID
                     where t1.ID == id && t3.Quantity != 0 && t3.IsReturn == true
                     select new Stoked
                     {
                         POID = t2.Mkt_POFK,
                         POSlaveID = t3.Mkt_POSlaveFK,
                         Date = t3.Date,
                         ItemName = t5.Name,
                         TotalRequired = t2.TotalRequired,
                         Quantity = t3.Quantity,
                         Challan = t3.Challan,
                         UnitName = t4.Name,
                         Store = t6.Name
                     }).ToList();

            this.StokedList = v;

        }

        public void PoStockInRequsitionReturnReport(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_POSlave on t1.ID equals t2.Mkt_POFK
                     join t3 in db.Mkt_POSlave_Receiving on t2.ID equals t3.Mkt_POSlaveFK
                     join t4 in db.Common_Unit on t2.Common_UnitFK equals t4.ID
                     join t5 in db.Raw_Item on t2.Raw_ItemFK equals t5.ID
                     join t6 in db.User_Department on t3.User_DepartmentFk equals t6.ID
                     where t1.ID == id && t3.Quantity != 0 && t3.IsReturn == true
                     select new Stoked
                     {
                         POID = t2.Mkt_POFK,
                         POSlaveID = t3.Mkt_POSlaveFK,
                         Date = t3.Date,
                         ItemName = t5.Name,
                         TotalRequired = t2.TotalRequired,
                         Quantity = t3.Quantity,
                         Challan = t3.Challan,
                         UnitName = t4.Name,
                         Store = t6.Name
                     }).ToList();

            this.StokedList = v;

        }

        public decimal? GetAllReturnQuantity(int? id, int? poId, string challan)
        {
            decimal? quantity = 0;
            decimal? x = (from t1 in db.Mkt_POSlave_Receiving
                          join t2 in db.Mkt_POSlave
                          on t1.Mkt_POSlaveFK equals t2.ID
                          join t3 in db.Mkt_PO
                          on t2.Mkt_POFK equals t3.ID
                          where t1.IsReturn == true && t1.Active == true && t3.ID == poId && t1.Challan == challan
                          select t1.Quantity).Sum();
            if (x != 0)
            {
                quantity = x;
            }

            return quantity;
        }

        public VmMkt_PO SelectSingleJoinedForRequisitionPo(int iD)
        {
            db = new xOssContext();
            var v = (from mkt_PO in db.Mkt_PO
                     join common_Supplier in db.Common_Supplier
                         on mkt_PO.Common_SupplierFK equals common_Supplier.ID
                     join common_Currency in db.Common_Currency
                         on mkt_PO.Common_CurrencyFK equals common_Currency.ID
                     join prod_Requisition in db.Prod_Requisition
                     on mkt_PO.RequisitionFK equals prod_Requisition.ID
                     where mkt_PO.ID == iD
                     select new VmMkt_PO
                     {
                         Common_Currency = common_Currency,
                         Mkt_PO = mkt_PO,
                         Common_Supplier = common_Supplier,
                         Prod_Requisition = prod_Requisition
                     }).FirstOrDefault();
            return v;
        }

        public decimal? TotlaValueForSupplierBtB(int id)
        {
            db = new xOssContext();
            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join B2bLcSupplierPOSlave in db.Commercial_B2bLcSupplierPOSlave on Mkt_POSlave.ID equals B2bLcSupplierPOSlave.Mkt_POSlaveFK

                     where Mkt_POSlave.Mkt_POFK == id && Mkt_POSlave.Active == true && B2bLcSupplierPOSlave.Active == true
                     select (B2bLcSupplierPOSlave.TotalRequired * Mkt_POSlave.Price)).Sum();

            if (a == null)
            {
                a = 0;
            }
            return a;

        }

        public void AlsoATransfer(int id, int userid)
        {
            db = new xOssContext();
            var data = db.Mkt_PO.Find(id);
            data.AlsoATransfer = true;
            data.Edit(userid);
        }
    }
}

