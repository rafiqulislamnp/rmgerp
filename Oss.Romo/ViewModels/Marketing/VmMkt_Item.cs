﻿using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_Item
    {
        private xOssContext db;
        public IEnumerable<VmMkt_Item> DataList { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public Mkt_SubCategory Mkt_SubCategory { get; set; }
        public Mkt_Category Mkt_Category { get; set; }
        public int? MktCatID { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }

        public VmMkt_Item()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Mkt_Item in db.Mkt_Item
                     join Mkt_SubCategory in db.Mkt_SubCategory
                     on Mkt_Item.Mkt_SubCategoryFK equals Mkt_SubCategory.ID
                     join Mkt_Category in db.Mkt_Category
                     on Mkt_SubCategory.Mkt_CategoryFK equals Mkt_Category.ID 
                   
                     select new VmMkt_Item
                     {
                         Mkt_Item = Mkt_Item,
                         Mkt_SubCategory = Mkt_SubCategory,
                         Mkt_Category= Mkt_Category
                     }).Where(x=>x.Mkt_Item.Active==true).OrderByDescending(x => x.Mkt_Item.ID).AsEnumerable();
            this.DataList = a;
        }

        public void SelectSingle(string Itemid)
        {
            int id = 0;
            Itemid = this.VmControllerHelper.Decrypt(Itemid);
            Int32.TryParse(Itemid, out id);

            db = new xOssContext();
            var a = (from Mkt_Item in db.Mkt_Item
                     join Mkt_SubCategory in db.Mkt_SubCategory
                     on Mkt_Item.Mkt_SubCategoryFK equals Mkt_SubCategory.ID
                     join Mkt_Category in db.Mkt_Category
                     on Mkt_SubCategory.Mkt_CategoryFK equals Mkt_Category.ID
                   

                     select new VmMkt_Item
                     {
                         Mkt_Item = Mkt_Item,
                         Mkt_SubCategory = Mkt_SubCategory,
                         Mkt_Category= Mkt_Category
                     }).Where(b => b.Mkt_Item.ID == id).SingleOrDefault();
            this.Mkt_Item = a.Mkt_Item;
            this.Mkt_SubCategory = a.Mkt_SubCategory;
            this.Mkt_Category = a.Mkt_Category;
            this.MktCatID = a.Mkt_Category.ID;
        }
        public int Add(int userID)
        {
            Mkt_Item.AddReady(userID);
            return Mkt_Item.Add();
        }
       
        public bool Edit(int userID)
        {
            return Mkt_Item.Edit(userID);
        }
  
        public bool Delete(int userID)
        {
            return Mkt_Item.Delete(userID);
        }

    }
}