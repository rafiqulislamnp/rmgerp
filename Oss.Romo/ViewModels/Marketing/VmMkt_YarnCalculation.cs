﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Common;
using System.ComponentModel;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Store;

namespace Oss.Romo.ViewModels.Marketing
{



    public class VmMkt_YarnCalculation
    {
        private xOssContext db;
        public IEnumerable<VmMkt_YarnCalculation> DataList { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public VmMkt_BOMSlave VmMkt_BOMSlave { get; set; }
        public Mkt_YarnCalculation Mkt_YarnCalculation { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public Mkt_YarnType Mkt_YarnType { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        [DisplayName("FINISH FABRIC")]
        public decimal FinishFabric { get; set; }
        [DisplayName("YARN QNTY(100%)")]
        public decimal YarnQnty { get; set; }

        [DisplayName("TOTAL LYCRA")]
        public decimal TotalLycra { get; set; }
        [DisplayName("ORDER QTY DZ")]
        public decimal QtyDZ { get; set; }

        [DisplayName("TOTAL YARN")]

        public decimal TotalFinishFabric { get; set; }
        public decimal TotalYarnQnty { get; set; }
        public decimal TotalTotalLycra { get; set; }
        public decimal TotalTotalYarn { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public decimal TotalYarn { get; set; }
        public int RawSubCatID { get; set; }
        public int RawCatID { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public VmMkt_YarnCalculation()
        {
            VmControllerHelper = new VmControllerHelper();
        }




        public VmMkt_YarnCalculation GetYarnCalculationByID(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();

            var v = (from t1 in db.Mkt_YarnCalculation
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                     join t5 in db.Raw_SubCategory on t4.Raw_SubCategoryFK equals t5.ID
                     join t6 in db.Raw_Category on t5.Raw_CategoryFK equals t6.ID
                     join t7 in db.Common_Supplier on t1.Common_SupplierFK equals t7.ID
                     join t8 in db.Common_Currency on t1.Common_CurrencyFK equals t8.ID
                     join t9 in db.Common_Unit on t1.Common_UnitFK equals t9.ID
                     join t10 in db.Mkt_YarnType on t1.Mkt_YarnTypeFk equals t10.ID
                     where t1.Mkt_BOMFK == id
                     select new VmMkt_YarnCalculation
                     {
                         Raw_Item = t4,
                         Raw_Category = t6,
                         Common_Currency = t8,
                         Raw_SubCategory = t5,
                         Mkt_YarnCalculation = t1,
                         Common_Unit = t9,
                         Common_Supplier = t7,
                         Mkt_YarnType = t10,
                         QtyDZ = t1.QntyPcs / 12,
                         FinishFabric = (t1.QntyPcs / 12) * t1.Consumption,
                         YarnQnty = (((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss),
                         TotalLycra = ((((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) / 100) * t1.Lycra,
                         TotalYarn = (((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) - ((((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) / 100) * t1.Lycra
                     }).Where(x => x.Mkt_YarnCalculation.Active == true).OrderByDescending(x => x.Mkt_YarnCalculation.ID).AsEnumerable();
            this.DataList = v;
            this.TotalFinishFabric = v.Sum(x => x.FinishFabric);
            this.TotalYarnQnty = v.Sum(x => x.YarnQnty);
            this.TotalTotalLycra = v.Sum(x => x.TotalLycra);
            this.TotalTotalYarn = v.Sum(x => x.TotalYarn);

            return null;

        }
        public VmMkt_YarnCalculation GetYarnCalculationByIdOld(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();

            var v = (from t1 in db.Mkt_YarnCalculation
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                     join t5 in db.Raw_SubCategory on t4.Raw_SubCategoryFK equals t5.ID
                     join t6 in db.Raw_Category on t5.Raw_CategoryFK equals t6.ID
                     join t7 in db.Common_Supplier on t1.Common_SupplierFK equals t7.ID
                     join t8 in db.Common_Currency on t1.Common_CurrencyFK equals t8.ID
                     join t9 in db.Common_Unit on t1.Common_UnitFK equals t9.ID
                     //join t10 in db.Mkt_YarnType on t1.Mkt_YarnTypeFk equals t10.ID
                     where t1.Mkt_BOMFK == id
                     select new VmMkt_YarnCalculation
                     {
                         Raw_Item = t4,
                         Raw_Category = t6,
                         Common_Currency = t8,
                         Raw_SubCategory = t5,
                         Mkt_YarnCalculation = t1,
                         Common_Unit = t9,
                         Common_Supplier = t7,
                         //Mkt_YarnType = t10,
                         QtyDZ = t1.QntyPcs / 12,
                         FinishFabric = (t1.QntyPcs / 12) * t1.Consumption,
                         YarnQnty = (((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss),
                         TotalLycra = ((((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) / 100) * t1.Lycra,
                         TotalYarn = (((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) - ((((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) / 100) * t1.Lycra
                     }).Where(x => x.Mkt_YarnCalculation.Active == true).OrderByDescending(x => x.Mkt_YarnCalculation.ID).AsEnumerable();
            this.DataList = v;
            this.TotalFinishFabric = v.Sum(x => x.FinishFabric);
            this.TotalYarnQnty = v.Sum(x => x.YarnQnty);
            this.TotalTotalLycra = v.Sum(x => x.TotalLycra);
            this.TotalTotalYarn = v.Sum(x => x.TotalYarn);

            return null;

        }

        public void SelectSingle(int id)
        {

            db = new xOssContext();
            var v = (from mktYarnCalculation in db.Mkt_YarnCalculation
                     join rawItem in db.Raw_Item on mktYarnCalculation.Raw_ItemFK equals rawItem.ID
                     join rawsubCategory in db.Raw_SubCategory on rawItem.Raw_SubCategoryFK equals rawsubCategory.ID
                     join rawCategory in db.Raw_Category on rawsubCategory.Raw_CategoryFK equals rawCategory.ID
                     join mktBom in db.Mkt_BOM on mktYarnCalculation.Mkt_BOMFK equals mktBom.ID
                     select new VmMkt_YarnCalculation
                     {
                         Mkt_BOM = mktBom,
                         Mkt_YarnCalculation = mktYarnCalculation,
                         Raw_Item = rawItem,
                         Raw_Category = rawCategory,
                         Raw_SubCategory = rawsubCategory
                     }).Where(c => c.Mkt_YarnCalculation.ID == id).SingleOrDefault();
            this.Mkt_YarnCalculation = v.Mkt_YarnCalculation;
            this.RawCatID = v.Raw_Category.ID;
            this.RawSubCatID = v.Raw_SubCategory.ID;
            this.Mkt_BOM = v.Mkt_BOM;

        }

        public int Add(int userID)
        {
            Mkt_YarnCalculation.Mkt_BOMFK = this.Mkt_YarnCalculation.Mkt_BOMFK;
            Mkt_YarnCalculation.AddReady(userID);
            return Mkt_YarnCalculation.Add();
        }

        public bool Edit(int userID)
        {
            return Mkt_YarnCalculation.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_YarnCalculation.Delete(userID);
        }

        internal void EditYarnOnBOMSlave(int id, int? bomFk)
        {

            db = new xOssContext();
            #region Select Yarn Item BOM Slave
            var bomSlaves = (from t1 in db.Mkt_BOMSlave
                             join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                             join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                             where t1.Active == true
                             select new VmMkt_YarnCalculation
                             {
                                 Mkt_BOMSlave = t1,
                                 Raw_Item = t2,
                                 Raw_SubCategory = t3
                             }).Where(c => c.Mkt_BOMSlave.Mkt_YarnCalculationFK == id && c.Mkt_BOMSlave.Mkt_BOMFK == bomFk).ToList();
            #endregion

            #region Select Lycra Item BOM Slave
            var bomLycra = (from t1 in db.Mkt_BOMSlave
                            join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                            join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                            where t1.Active == true
                            select new VmMkt_YarnCalculation
                            {
                                Mkt_BOMSlave = t1,
                                Raw_Item = t2,
                                Raw_SubCategory = t3
                            }).Where(c => c.Raw_Item.Raw_SubCategoryFK == 2044 && c.Mkt_BOMSlave.Mkt_BOMFK == bomFk).ToList();
            #endregion

            #region Update BOM Slave and PO Slave
            VmMkt_YarnCalculation x = new VmMkt_YarnCalculation();
            x.VmMkt_BOMSlave = new VmMkt_BOMSlave();
            if (bomSlaves.Count() != 0)
            {
                foreach (VmMkt_YarnCalculation i in bomSlaves)
                {
                    #region  Update BOM Slave Yarn Item when update yarn Calculation
                    x.VmMkt_BOMSlave.SelectSingle(i.Mkt_BOMSlave.ID.ToString());
                    x.VmMkt_BOMSlave.Mkt_BOMSlave.Consumption = this.Mkt_YarnCalculation.Consumption;
                    x.VmMkt_BOMSlave.Mkt_BOMSlave.Description = this.Mkt_YarnCalculation.Fabrication + "-GSM:" + Math.Round(this.Mkt_YarnCalculation.GSM) + "-Dia:" + this.Mkt_YarnCalculation.FinishDIA + "-Color:" + this.Mkt_YarnCalculation.Color;

                    if (x.VmMkt_BOMSlave.RawCatID == 1019)
                    {

                        x.VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = (((this.Mkt_YarnCalculation.QntyPcs / 12) * this.Mkt_YarnCalculation.Consumption) / this.Mkt_YarnCalculation.ProcessLoss);
                    }
                    else if (x.VmMkt_BOMSlave.RawCatID == 1020)
                    {
                        x.VmMkt_BOMSlave.Mkt_BOMSlave.Common_SupplierFK = this.Mkt_YarnCalculation.Common_SupplierFK.Value;
                        x.VmMkt_BOMSlave.Mkt_BOMSlave.Price = this.Mkt_YarnCalculation.Price;
                        x.VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = (((this.Mkt_YarnCalculation.QntyPcs / 12) * this.Mkt_YarnCalculation.Consumption) / this.Mkt_YarnCalculation.ProcessLoss) - ((((this.Mkt_YarnCalculation.QntyPcs / 12) * this.Mkt_YarnCalculation.Consumption) / this.Mkt_YarnCalculation.ProcessLoss) / 100) * this.Mkt_YarnCalculation.Lycra;

                    }
                    else
                    {
                        x.VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = (((this.Mkt_YarnCalculation.QntyPcs / 12) * this.Mkt_YarnCalculation.Consumption) / this.Mkt_YarnCalculation.ProcessLoss) - ((((this.Mkt_YarnCalculation.QntyPcs / 12) * this.Mkt_YarnCalculation.Consumption) / this.Mkt_YarnCalculation.ProcessLoss) / 100) * this.Mkt_YarnCalculation.Lycra;

                    }
                    x.VmMkt_BOMSlave.Edit(0);
                    #endregion

                    #region Update PO Slave when Update Yarn Calculation
                    x.SelectSinglePoSlaveByBomSlave(i.Mkt_BOMSlave.ID);
                    if (x.VmMkt_BOMSlave.Mkt_POSlave != null)
                    {
                        x.VmMkt_BOMSlave.Mkt_POSlave.TotalRequired = x.VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Consumption = x.VmMkt_BOMSlave.Mkt_BOMSlave.Consumption;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Description = x.VmMkt_BOMSlave.Mkt_BOMSlave.Description;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Price = x.VmMkt_BOMSlave.Mkt_BOMSlave.Price;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Mkt_BOMSlaveFK = x.VmMkt_BOMSlave.Mkt_BOMSlave.ID;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Raw_ItemFK = x.VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Common_UnitFK = x.VmMkt_BOMSlave.Mkt_BOMSlave.Common_UnitFK;
                        x.VmMkt_BOMSlave.PoSlaveEdit(0);
                    }
                    #endregion
                }
            }
            if (bomLycra.Count() != 0)
            {
                foreach (VmMkt_YarnCalculation i in bomLycra)
                {
                    #region Update BOM Slave Lycra Item when update yarn Calculation
                    x.VmMkt_BOMSlave.SelectSingle(i.Mkt_BOMSlave.ID.ToString());
                    x.VmMkt_BOMSlave.Mkt_BOMSlave.Consumption = this.Mkt_YarnCalculation.Consumption;

                    if (x.VmMkt_BOMSlave.RawSubCatID == 2044)
                    {
                        VmMkt_BOM = new VmMkt_BOM();
                        x.VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = Convert.ToDecimal(VmMkt_BOM.GetLycraValue(i.Mkt_BOMSlave.Mkt_BOMFK.ToString()));
                    }
                    x.VmMkt_BOMSlave.Edit(0);
                    #endregion

                    #region Update PO Slave when Update Yarn Calculation
                    x.SelectSinglePoSlaveByBomSlave(i.Mkt_BOMSlave.ID);
                    if (x.VmMkt_BOMSlave.Mkt_POSlave != null)
                    {
                        x.VmMkt_BOMSlave.Mkt_POSlave.TotalRequired = x.VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Consumption = x.VmMkt_BOMSlave.Mkt_BOMSlave.Consumption;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Description = x.VmMkt_BOMSlave.Mkt_BOMSlave.Description;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Price = x.VmMkt_BOMSlave.Mkt_BOMSlave.Price;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Mkt_BOMSlaveFK = x.VmMkt_BOMSlave.Mkt_BOMSlave.ID;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Raw_ItemFK = x.VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK;
                        x.VmMkt_BOMSlave.Mkt_POSlave.Common_UnitFK = x.VmMkt_BOMSlave.Mkt_BOMSlave.Common_UnitFK;
                        x.VmMkt_BOMSlave.PoSlaveEdit(0);
                    }
                    #endregion
                }
            }
            #endregion
        }
        

        public void DeleteBOMSlave(int yarnId, int bomId)
        {
            db = new xOssContext();
            #region Select Yarn Item BOM Slave
            var bomSlaves = (from t1 in db.Mkt_BOMSlave
                             join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                             join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                             where t1.Active == true
                             select new VmMkt_YarnCalculation
                             {
                                 Mkt_BOMSlave = t1,
                                 Raw_Item = t2,
                                 Raw_SubCategory = t3
                             }).Where(c => c.Mkt_BOMSlave.Mkt_YarnCalculationFK == yarnId && c.Mkt_BOMSlave.Mkt_BOMFK == bomId).ToList();
            #endregion

            #region Select Lycra Item BOM Slave
            var bomLycra = (from t1 in db.Mkt_BOMSlave
                            join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                            join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                            where t1.Active == true
                            select new VmMkt_YarnCalculation
                            {
                                Mkt_BOMSlave = t1,
                                Raw_Item = t2,
                                Raw_SubCategory = t3
                            }).Where(c => c.Raw_Item.Raw_SubCategoryFK == 2044 && c.Mkt_BOMSlave.Mkt_BOMFK == bomId).ToList();
            #endregion

            #region Delete or Update BOM Slave and PO Slave
            VmMkt_YarnCalculation x = new VmMkt_YarnCalculation();
            x.VmMkt_BOMSlave = new VmMkt_BOMSlave();
            if (bomSlaves.Count() != 0)
            {
                foreach (VmMkt_YarnCalculation i in bomSlaves)
                {
                    #region Delete BOM Slave when Delete Yarn Calculation
                    x.VmMkt_BOMSlave.Mkt_BOMSlave = new Mkt_BOMSlave();
                    x.VmMkt_BOMSlave.Mkt_BOMSlave.ID = i.Mkt_BOMSlave.ID;
                    x.VmMkt_BOMSlave.Dalete(0);
                    #endregion


                    #region Delete PO Slave when Delete Yarn Calculation
                    x.SelectSinglePoSlaveByBomSlave(i.Mkt_BOMSlave.ID);
                    if (x.VmMkt_BOMSlave.Mkt_POSlave != null)
                    {
                        x.VmMkt_BOMSlave.PoSlaveDelete(0);
                    }
                    #endregion
                   
                }

            }
          
            if (bomLycra.Count() != 0)
            {
                foreach (VmMkt_YarnCalculation i in bomLycra)
                {
                    VmMkt_BOM = new VmMkt_BOM();
                    decimal? lycraValue = Convert.ToDecimal(VmMkt_BOM.GetLycraValue(i.Mkt_BOMSlave.Mkt_BOMFK.ToString()));
                    if (lycraValue != 0)
                    {
                        try
                        {
                            #region Update BOM Slave Lycta item when Update Yarn Calculation
                            var singleOrder = db.Mkt_BOMSlave.FirstOrDefault(p => p.ID == i.Mkt_BOMSlave.ID);
                            if (singleOrder != null)
                            {
                                singleOrder.RequiredQuantity = lycraValue;
                                singleOrder.Price = i.Mkt_BOMSlave.Price != 0 ? i.Mkt_BOMSlave.Price : 1;
                            }
                            db.SaveChanges();
                            #endregion


                            #region Update PO Slave when Update Yarn Calculation
                            x.SelectSinglePoSlaveByBomSlave(i.Mkt_BOMSlave.ID);
                            if (singleOrder != null)
                            {
                                x.VmMkt_BOMSlave.Mkt_POSlave.TotalRequired = singleOrder.RequiredQuantity;
                                x.VmMkt_BOMSlave.Mkt_POSlave.Consumption = singleOrder.Consumption;
                                x.VmMkt_BOMSlave.Mkt_POSlave.Description = singleOrder.Description;
                                x.VmMkt_BOMSlave.Mkt_POSlave.Price = singleOrder.Price;
                                x.VmMkt_BOMSlave.Mkt_POSlave.Mkt_BOMSlaveFK = singleOrder.ID;
                                x.VmMkt_BOMSlave.Mkt_POSlave.Raw_ItemFK = singleOrder.Raw_ItemFK;
                                x.VmMkt_BOMSlave.Mkt_POSlave.Common_UnitFK = singleOrder.Common_UnitFK;
                                x.VmMkt_BOMSlave.PoSlaveEdit(0);
                            }
                            #endregion
                        }
                        catch (Exception e)
                        {
                            string message = e.Message;
                        }
                    }
                    else
                    {
                        #region Delete BOM Slave when Delete Yarn Calculation
                        x.VmMkt_BOMSlave.Mkt_BOMSlave = new Mkt_BOMSlave();
                        x.VmMkt_BOMSlave.Mkt_BOMSlave.ID = i.Mkt_BOMSlave.ID;
                        x.VmMkt_BOMSlave.Dalete(0);
                        #endregion

                        #region Delete PO Slave when Delete Yarn Calculation
                        x.SelectSinglePoSlaveByBomSlave(i.Mkt_BOMSlave.ID);
                        if (x.VmMkt_BOMSlave.Mkt_POSlave != null)
                        {
                            x.VmMkt_BOMSlave.PoSlaveDelete(0);
                        }
                        #endregion
                    }

                }
            }
            #endregion

        }


        public void SelectSinglePoSlaveByBomSlave(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave

                     where t1.Active == true
                     select new VmMkt_BOMSlave
                     {
                         Mkt_POSlave = t1
                     }).SingleOrDefault(x => x.Mkt_POSlave.Mkt_BOMSlaveFK == id);
            if (a != null)
            {
                this.VmMkt_BOMSlave.Mkt_POSlave = a.Mkt_POSlave;
            }
            else
            {
                this.VmMkt_BOMSlave.Mkt_POSlave = null;
            }


        }
    }
}