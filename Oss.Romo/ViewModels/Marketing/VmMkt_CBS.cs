﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Reports;
using Oss.Romo.ViewModels.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_CBS
    {

        private xOssContext db;
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        //public VmMkt_CBSFabric VmMkt_CBSFabric { get; set; }
        public Mkt_BOMStepJobs Mkt_BOMStepJobs { get; set; }
        public Mkt_BOMSlaveFabric Mkt_BOMSlaveFabric { get; set; }
        public VmMkt_PO VmMkt_PO { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Mkt_CBS Mkt_CBS { get; set; }
        public VmMktPO_Slave VmMktPO_Slave { get; set; }
        public string CustomeCID { get; set; }
        public string SearchString { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public decimal? PreviousDelivery { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public User_User User_User { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public IEnumerable<VmMkt_CBS> DataList { get; set; }
        [DisplayName("Received Quentity")]
        public decimal? Recieve { get; set; }
        [DisplayName("Remaining Quentity")]
        public decimal? Now_Recieved { get; set; }
        [DisplayName("Send Quentity")]
        public decimal? Send { get; set; }
        [DisplayName("Remaining Quentity")]
        public decimal? Now_Send { get; set; }
        [DisplayName("Remaining Amount")]
        public decimal? Now_Paid { get; set; }
        [DisplayName("Total Price")]
        public decimal? Total_Price { get; set; }
        [DisplayName("Line Total")]
        public decimal? Line_Total { get; set; }
        [DisplayName("Required Quantity")]
        public decimal? TotalRequired { get; set; }
        [DisplayName("Total Price")]
        public decimal? TotalItem_Price { get; set; }
        //[DisplayName("System Loss")]
        //public decimal? SystemLoss { get; set; }
        public int RawSubCatID { get; set; }
        public int RawCatID { get; set; }
        public bool Select { get; set; }
        public List<ReportDocType> CBCReportDoc { get; set; }
        public Mkt_CBSMaster Mkt_CBSMaster { get; set; }
        public VmMkt_CBS()
        {
            VmControllerHelper = new VmControllerHelper();
        }
       

      
        //public List<object> GetTempSupplier()
        //{
        //    var supplierList = new List<object>();

        //    foreach (var supplier in db.Common_Supplier)
        //    {
        //        supplierList.Add(new { Text = supplier.Name, Value = supplier.ID });
        //    }
        //    return supplierList;
        //    this.DataList = supplierList.ToList();
        //}
        internal void GetCBSItem(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);

            db = new xOssContext();
            //VmMkt_BOM vmMkt_BOM = new VmMkt_BOM();
            //string yarnValue = vmMkt_BOM.GetYarnValue(id);
            //decimal yarnVal = Convert.ToDecimal(yarnValue);
            //string lycraValue = vmMkt_BOM.GetLycraValue(id);
            //decimal lycraVal = Convert.ToDecimal(lycraValue);
           

            var a = (from mktCbs in db.Mkt_CBS
                     //join Common_Supplier in db.Common_Supplier
                     //on mktCbs.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_CBSMaster
                     on mktCbs.Mkt_CBSMasterFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on mktCbs.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mktCbs.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mktCbs.Raw_ItemFK equals Raw_Item.ID
                     join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID
                     where mktCbs.Mkt_CBSMasterFK == iD
                     select new VmMkt_CBS
                     {
                         TotalRequired = mktCbs.Consumption * Mkt_BOM.Quantity * Mkt_BOM.QPack,
                         TotalItem_Price = mktCbs.RequiredQuantity * mktCbs.Price,// Raw_Item.ID == 1045 ? yarnVal * mkt_BOMSlave.Price : (Raw_Item.ID == 5331 ? lycraVal * mkt_BOMSlave.Price : mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price) ,

                         Mkt_CBS = mktCbs,
                         //Common_Supplier = Common_Supplier,
                         Mkt_CBSMaster = Mkt_CBSMaster,
                         Raw_Item = Raw_Item,
                         Raw_Category = t2,
                         Raw_SubCategory = t1,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency
                     }).Where(x => x.Mkt_CBS.Active == true).OrderBy(x => x.Raw_Item.ID).AsEnumerable();
            this.DataList = a;

        }
        internal void BOMSlaveFabricsPurchaseAndProcessing(int? id)
        {
            db = new xOssContext();
            var a = (from mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID

                     //Left Join
                     join t3 in (from t4 in db.Mkt_POSlave  where t4.Active == true
                     select new { Mkt_POSlave = t4 }
                     ) on mkt_BOMSlave.ID equals t3.Mkt_POSlave.Mkt_BOMSlaveFK
                     into POSlave
                     from xx in POSlave.DefaultIfEmpty()
                     join t5 in db.Mkt_PO on xx.Mkt_POSlave.Mkt_POFK equals t5.ID
                     into PO
                     from yy in PO.DefaultIfEmpty()                     
                     where mkt_BOMSlave.ID == id //mkt_BOMSlave.Common_SupplierFK == 1 && 
                     select new VmMkt_CBS
                     {
                         TotalRequired = mkt_BOMSlave.Consumption * Mkt_BOM.Quantity * Mkt_BOM.QPack,
                         TotalItem_Price = mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price,
                         Mkt_POSlave = xx.Mkt_POSlave,
                         Mkt_PO = yy,
                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Raw_Category = t2,
                         Raw_SubCategory = t1,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency

                     }).Where(x => x.Mkt_BOMSlave.Active == true).OrderBy(x => x.Raw_Item.ID).FirstOrDefault();

            this.TotalRequired = a.TotalRequired;
            this.TotalItem_Price = a.TotalItem_Price;
            this.Mkt_POSlave = a.Mkt_POSlave;
            this.Mkt_PO = a.Mkt_PO;
            this.Common_Supplier = a.Common_Supplier;
            this.Mkt_BOM = a.Mkt_BOM;
            this.Mkt_BOMSlave = a.Mkt_BOMSlave;
            this.Raw_Item = a.Raw_Item;
            this.Raw_Category = a.Raw_Category;
            this.Raw_SubCategory = a.Raw_SubCategory;
            this.Common_Unit = a.Common_Unit;
            this.Common_Currency = a.Common_Currency;
        }
        internal void BOMSlaveFabricsStepJobs(int iD)
        {
            var a = (from mkt_BOMStepJobs in db.Mkt_BOMStepJobs
                     join mkt_BOMSlave in db.Mkt_BOMSlave
                     on mkt_BOMStepJobs.Mkt_BOMSlaveFK equals mkt_BOMSlave.ID
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMStepJobs.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMStepJobs.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMStepJobs.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMStepJobs.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMStepJobs.Raw_ItemFK equals Raw_Item.ID
                     join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID
                     //Left Join
                     join t3 in (
                     from t4 in db.Mkt_POSlave
                     where t4.Active == true
                     select new { Mkt_POSlave = t4 }
                     ) on mkt_BOMStepJobs.ID equals t3.Mkt_POSlave.Mkt_BOMSlaveFK
                     into POSlave
                     from xx in POSlave.DefaultIfEmpty()

                         //join t3 in db.Mkt_POSlave on mkt_BOMSlave.ID equals t3.Mkt_BOMSlaveFK
                         //into POSlave
                         //from xx in POSlave.DefaultIfEmpty()

                     join t5 in db.Mkt_PO on xx.Mkt_POSlave.Mkt_POFK equals t5.ID
                     into PO
                     from yy in PO.DefaultIfEmpty()


                     where mkt_BOMStepJobs.Mkt_BOMSlaveFK == iD //&& xx.Active==true

                     select new VmMkt_CBS
                     {
                         TotalRequired = mkt_BOMSlave.Consumption * Mkt_BOM.Quantity * Mkt_BOM.QPack,
                         TotalItem_Price = mkt_BOMStepJobs.RequiredQuantity * mkt_BOMStepJobs.Price,
                         Mkt_BOMStepJobs = mkt_BOMStepJobs,
                         Mkt_POSlave = xx.Mkt_POSlave,
                         Mkt_PO = yy,
                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Raw_Category = t2,
                         Raw_SubCategory = t1,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency

                     }).Where(x => x.Mkt_BOMStepJobs.Active == true).OrderBy(x => x.Raw_Item.ID).AsEnumerable();
            this.DataList = a;
        }
       
        public void POslave()
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO
                     on t1.Mkt_POFK equals t2.ID
                     where t1.Active == true
                     select new VmMktPO_Slave
                     {
                         Mkt_POSlave = t1,
                         Mkt_PO = t2
                     }).ToList();
            VmMktPO_Slave.DataList = v;
        }
        public bool AuthorizePO { get; set; }
        public int? Mkt_BOMSlaveFK { get; set; }
        public bool GetAuthorizePO(int BOMSlaveFK)
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO
                     on t1.Mkt_POFK equals t2.ID
                     where t1.Mkt_BOMSlaveFK == BOMSlaveFK
                     select new
                     {
                         AuthorizePO = t2.IsAuthorize,

                     }).ToList();

            foreach (var item in v)
            {
                this.AuthorizePO = item.AuthorizePO;
            }

            return this.AuthorizePO;
        }
        public int? GetBOMSlaveFKFromPOSlave(int BOMSlaveFK)
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO
                     on t1.Mkt_POFK equals t2.ID
                     where t1.Mkt_BOMSlaveFK == BOMSlaveFK
                     select new
                     {
                         Mkt_BOMSlaveFK = t1.Mkt_BOMSlaveFK,

                     }).ToList();

            foreach (var item in v)
            {
                this.Mkt_BOMSlaveFK = item.Mkt_BOMSlaveFK;
            }

            return this.Mkt_BOMSlaveFK;
        }

        public void POslaveID(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_Supplier on t1.Common_SupplierFK equals t3.ID
                     where t1.Mkt_BOMFK == id
                     select new VmMkt_CBS
                     {
                         Mkt_PO = t1,
                         Mkt_BOM = t2,
                         Common_Supplier = t3
                     }).ToList();
            this.DataList = v;
        }
        internal void GetBOMSlaveFabric()
        {

            db = new xOssContext();

            //var check = (from mkt_BOMSlave in db.Mkt_BOMSlave
            //             //where mkt_BOMSlave.Mkt_BOMFK == id
            //             select (1)
            //              ).Count();

            //if (check == 0)
            //{
            //    GetSuggestedPOFromBOM(id);
            //}
            var a = (from mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     where mkt_BOMSlave.IsFabric == true
                     //&& mkt_BOMSlave.Mkt_BOMFK == id
                     select new VmMkt_CBS
                     {
                         TotalRequired = mkt_BOMSlave.RequiredQuantity,
                         Total_Price = mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price,

                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency,
                         Common_TheOrder = Common_TheOrder
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOMSlave.ID).AsEnumerable();
            this.DataList = a;

        }
        internal void GetBOMSlaveFabricSearch(string searchString)
        {
            db = new xOssContext();
            var a = (from mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     where mkt_BOMSlave.IsFabric == true

                     select new VmMkt_CBS
                     {
                         TotalRequired = mkt_BOMSlave.RequiredQuantity,
                         Total_Price = mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price,

                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency,
                         Common_TheOrder = Common_TheOrder,
                         CustomeCID = Common_TheOrder.CID + "/" + Raw_Item.Name

                     }).Where(x => x.Mkt_BOM.Active == true && x.CustomeCID.Contains(searchString)).OrderByDescending(x => x.Mkt_BOMSlave.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(string id)
        {
            int iD = 0;
            int.TryParse(id, out iD);        
            db = new xOssContext();
            var a = (from t1 in db.Mkt_CBSMaster
                                    
                     where  t1.Active == true 
                     select new VmMkt_CBS
                     {                                     
                         Mkt_CBSMaster = t1
                     }).Where(x => x.Mkt_CBSMaster.ID == iD).SingleOrDefault();               
                this.Mkt_CBSMaster = a.Mkt_CBSMaster;
              
        }
        public void GetCBSByID(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_CBS
                     join t2 in db.Mkt_CBSMaster
                     on t1.Mkt_CBSMasterFK equals t2.ID
                     join t3 in db.Raw_Item
                     on t1.Raw_ItemFK equals t3.ID
                     join t4 in db.Raw_SubCategory
                     on t3.Raw_SubCategoryFK equals t4.ID
                     join t5 in db.Raw_Category
                     on t4.Raw_CategoryFK equals t5.ID
                     where t1.Active == true
                     select new VmMkt_CBS
                     {
                         Mkt_CBS = t1,
                         Mkt_CBSMaster = t2,
                         Raw_Item = t3,
                         Raw_SubCategory = t4,
                         Raw_Category = t5
                     }).Where(x => x.Mkt_CBS.ID == id).SingleOrDefault();
            this.Mkt_CBS = a.Mkt_CBS;
            this.Mkt_CBSMaster = a.Mkt_CBSMaster;
            this.RawCatID = a.Raw_Category.ID;
            this.RawSubCatID = a.Raw_SubCategory.ID;


        }
        //..For increment PoID..//
        internal bool CreateCID(int userID, int poID)
        {
            Mkt_BOMSlave.ID = poID;
            //Mkt_BOMSlave.CID = "PO/" + (10000 + poID).ToString();
            return Mkt_BOMSlave.Edit(userID);

        }

        public int AddStepJobs(int userID)
        {
            //Mkt_BOMSlave.Status = 0;
            Mkt_BOMStepJobs.AddReady(userID);
            return Mkt_BOMStepJobs.Add();

            //EditBOMUpdateDate()
        }


        public bool EditStepJobs(int userID)
        {
            return Mkt_BOMStepJobs.Edit(userID);

        }

        public int Add(int userID)
        {
            //Mkt_BOMSlave.Status = 0;
            Mkt_CBS.AddReady(userID);
            return Mkt_CBS.Add();

            //EditBOMUpdateDate()
        }

        internal void CBSReportDocLoad(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();

            var a = (from Mkt_CBS in db.Mkt_CBS
                     join Mkt_CBSMaster in db.Mkt_CBSMaster
                     on Mkt_CBS.Mkt_CBSMasterFK equals Mkt_CBSMaster.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_CBS.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_CBS.Raw_ItemFK equals Raw_Item.ID

                     join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID

                     join Common_Unit in db.Common_Unit
                     on Mkt_CBS.Common_UnitFK equals Common_Unit.ID
                    
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_CBSMaster.Mkt_ItemFK equals Mkt_Item.ID
                    
                     join User_User in db.User_User on Mkt_CBSMaster.FirstCreatedBy equals User_User.ID
                     where Mkt_CBS.Mkt_CBSMasterFK == iD && Mkt_CBS.Active == true

                     select new ReportDocType
                     {
                         HeaderLeft1 = ": " + Mkt_CBSMaster.Style,
                         HeaderLeft2 = Mkt_CBSMaster.FirstMove.ToString(),
                         HeaderLeft3 = Mkt_CBSMaster.UpdateDate.ToString(),
                         HeaderLeft4 = ": " + Mkt_CBSMaster.Reference.ToString(),
                         HeaderLeft5 = ": " + Mkt_CBSMaster.Class.ToString(),
                         HeaderLeft6 = ": " + Mkt_CBSMaster.Fabrication,
                         HeaderLeft7 = ": " + Mkt_CBSMaster.SizeRange,
                         HeaderLeft8 = ": " + Mkt_CBSMaster.Style,
                         //HeaderMiddle1 = ": " + Mkt_Buyer.Name,
                        // HeaderMiddle2 = ": " + Common_TheOrder.BuyerPO,
                         //HeaderMiddle3 = Common_TheOrder.OrderDate.ToString(),
                         //HeaderMiddle4 = ": " + Common_TheOrder.Season,
                         HeaderMiddle5 = ": " + Mkt_Item.Name,
                         HeaderRight1 = ": " + Mkt_CBSMaster.QPack.ToString(),
                         HeaderRight2 = ": " + Mkt_CBSMaster.Quantity.ToString(),
                         HeaderRight3 = ": " + Mkt_CBSMaster.UnitPrice.ToString(),
                         HeaderRight4 = ": " + ((Mkt_CBSMaster.QPack * Mkt_CBSMaster.Quantity).ToString()),
                         HeaderRight5 = ((Mkt_CBSMaster.QPack * Mkt_CBSMaster.Quantity) / 12).ToString(),
                         HeaderRight6 = ((Mkt_CBSMaster.QPack * Mkt_CBSMaster.Quantity) * Mkt_CBSMaster.UnitPrice).ToString(),
                         HeaderRight7 = ": " + User_User.Name,
                         Body1 = Raw_Item.Name,
                         Body2 = Mkt_CBS.Description,
                         Body3 = Mkt_CBS.Consumption.ToString(),
                         Body4 = Common_Unit.Name,
                         Body5 = Mkt_CBS.Price.ToString(),
                         //Body6 = Common_Supplier.Name,
                         //Body7 = ((Mkt_BOMSlave.Consumption * (Mkt_BOM.QPack * Mkt_BOM.Quantity)) * Mkt_BOMSlave.Price).ToString(),
                         Body7 = (Mkt_CBS.RequiredQuantity * Mkt_CBS.Price).ToString(),
                         Body8 = Mkt_CBS.IsFabric.ToString(),
                         Body9 = Mkt_CBS.RequiredQuantity.ToString(),
                         Body10 = Common_Currency.Symbol,
                         SubBody8 = (((((Mkt_CBSMaster.QPack * Mkt_CBSMaster.Quantity) * Mkt_CBSMaster.UnitPrice) * Mkt_CBSMaster.Commission)) / 100).ToString(),
                         Body11 = Mkt_CBSMaster.Commission.ToString() + "% ",
                         Body12 = t2.Name,
                         int1 = t2.ID
                     }).OrderBy(x => x.int1).ToList();

            decimal GroupTTL = 0;
            decimal TTL = 0;
            decimal TTLFabrics = 0;
            decimal TTLAccount = 0;
            decimal TTLCM = 0;
            decimal TTLFOB = 0;
            decimal CMperDZ = 0;
            decimal TTLQDZ = 0;
            decimal Comm = 0;


            foreach (ReportDocType r in a)
            {


                decimal body7 = 0;
                decimal.TryParse(r.Body7, out body7);
                TTL += body7;

                if (r.Body8 == "True")
                {
                    decimal subbody2 = 0;
                    decimal.TryParse(r.Body7, out subbody2);
                    TTLFabrics += subbody2;
                }
                else
                {

                    decimal subbody2 = 0;
                    decimal.TryParse(r.Body7, out subbody2);
                    TTLAccount += subbody2;

                }
                decimal temp = 0;
                decimal.TryParse(r.Body3, out temp);
                r.Body3 = temp.ToString("F");
                decimal.TryParse(r.Body5, out temp);
                r.Body5 = r.Body10 + temp.ToString("F");

                decimal subbody3 = 0;
                decimal.TryParse(r.HeaderRight6, out subbody3);
                TTLFOB = subbody3;
                decimal subbody4 = 0;
                decimal.TryParse(r.HeaderRight5, out subbody4);
                TTLQDZ = subbody4;
                //TTLAccount = TTL - TTLFabrics;
                TTLCM = TTLFOB - (TTL + Comm);
                CMperDZ = TTLCM / TTLQDZ;
                decimal SubBody8 = 0;
                decimal.TryParse(r.SubBody8, out SubBody8);
                Comm = SubBody8;

            }
            if (a.Count != 0)
            {
                string tempBody = a[0].Body12;


                decimal groupTotal = 0;
                int counter = 1;
                List<string> groupList = new List<string>();
                foreach (ReportDocType r in a)
                {
                    r.SubBody1 = r.Body10 + TTL.ToString("F");
                    r.SubBody2 = r.Body10 + TTLFabrics.ToString("F");
                    r.SubBody3 = r.Body10 + TTLFOB.ToString("F");
                    r.SubBody4 = TTLQDZ.ToString("F");
                    r.SubBody5 = r.Body10 + TTLAccount.ToString("F");
                    r.SubBody6 = r.Body10 + TTLCM.ToString("F");
                    r.SubBody7 = r.Body10 + CMperDZ.ToString("F");
                    r.SubBody8 = "(" + r.Body11 + ")" + r.Body10 + Comm.ToString("F");
                    decimal temp = 0;

                    decimal.TryParse(r.Body7, out temp);
                    r.Body7 = r.Body10 + temp.ToString("F");

                    r.HeaderLeft2 = ": " + r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
                    r.HeaderLeft3 = ": " + r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
                    //r.HeaderMiddle3 = ": " + r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);

                    if (tempBody == r.Body12)
                    {
                        decimal tempValue = 0;
                        string ddd = r.Body7.Substring(0, r.Body7.Length - 1);
                        decimal.TryParse(r.Body7.Substring(1, r.Body7.Length - 1), out tempValue);
                        groupTotal += tempValue;
                    }

                    else
                    {

                        groupList.Add(tempBody + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")");
                        decimal tempValue = 0;
                        string ddd = r.Body7.Substring(0, r.Body7.Length - 1);
                        decimal.TryParse(r.Body7.Substring(1, r.Body7.Length - 1), out tempValue);
                        groupTotal = tempValue;


                    }
                    if (counter == a.Count())
                    {
                        //tempBody = r.Body12 + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")";
                        groupList.Add(r.Body12 + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")");
                    }
                    tempBody = r.Body12;

                    counter++;
                }
                string tempGroupTotal = "";
                foreach (string st in groupList)
                {
                    tempGroupTotal += st + "\n";

                }
                foreach (string st in groupList)
                {
                    foreach (ReportDocType r in a)
                    {
                        r.Body13 = tempGroupTotal;
                        if (st.Contains(r.Body12))
                        {
                            r.Body12 = st;
                        }
                    }
                }
            }
            this.CBCReportDoc = a.ToList();
        }
        
        internal void POWaitingDataLoad()
        {
            db = new xOssContext();
            var a = (from Mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     where Mkt_BOMSlave.Active == true

                     select new VmMkt_CBS
                     {

                         Mkt_BOMSlave = Mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency,
                         TotalRequired = Mkt_BOMSlave.Consumption * Mkt_BOM.Quantity,
                         TotalItem_Price = Mkt_BOMSlave.Consumption * Mkt_BOM.Quantity * Mkt_BOMSlave.Price,

                     }).Where(x => x.Mkt_BOMSlave.Active == true).OrderByDescending(x => x.Mkt_BOMSlave.ID).AsEnumerable();

            this.DataList = a.ToList();
        }
        internal void POWaitingSelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from Mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID
                     select new VmMkt_CBS
                     {

                         Mkt_BOMSlave = Mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency

                     }).Where(x => x.Mkt_BOMSlave.Mkt_BOMFK == id).AsEnumerable();
            this.DataList = a;

        }
        //public bool Add(int userID)
        //{
        //    Mkt_BOMSlave.AddReady(userID);
        //    return CreateCID(userID, Mkt_BOMSlave.Add());

        //}

        public bool Edit(int userID)
        {
            return Mkt_CBS.Edit(userID);

        }

        public bool EditBOMUpdateDate(int userID, int bomFk)
        {
            VmMkt_BOM x = new VmMkt_BOM();
            x.SelectSingle(bomFk.ToString());
            x.Mkt_BOM.UpdateDate = DateTime.Now;

            return x.Mkt_BOM.Edit(userID);

        }

        public bool Dalete(int userID)
        {
            return Mkt_BOMSlave.Delete(userID);
        }

        //internal IEnumerable<VmMkt_CBS> WriteNewMeathod(List<int> ids)
        //{
        //    var a = (from Mkt_BOMSlave in db.Mkt_BOMSlave
        //             join Common_Supplier in db.Common_Supplier
        //             on Mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
        //             join Mkt_BOM in db.Mkt_BOM
        //             on Mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
        //             join Common_Unit in db.Common_Unit
        //             on Mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
        //             join Common_Currency in db.Common_Currency
        //             on Mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
        //             join Raw_Item in db.Raw_Item
        //             on Mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID
        //             select new VmMkt_CBS
        //             {

        //                 Mkt_BOMSlave = Mkt_BOMSlave,
        //                 Common_Supplier = Common_Supplier,
        //                 Mkt_BOM = Mkt_BOM,
        //                 Raw_Item = Raw_Item,
        //                 Common_Unit = Common_Unit,
        //                 Common_Currency = Common_Currency

        //             }).Where(x => x.Mkt_BOMSlave.Mkt_BOMFK == 3).AsEnumerable();
        //    this.DataList = a;




        //}
        public VmMkt_CBS SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_BOMSlave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Supplier on t1.Common_SupplierFK equals t3.ID
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t6 in db.Common_Unit on t1.Common_UnitFK equals t6.ID
                     join t7 in db.Mkt_Item on t4.Mkt_ItemFK equals t7.ID
                     join t8 in db.Common_Currency on t5.Common_CurrencyFK equals t8.ID
                     where t1.ID == iD && t1.Active == true
                     select new VmMkt_CBS
                     {
                         TotalRequired = t1.RequiredQuantity,
                         TotalItem_Price = t1.RequiredQuantity * t1.Price,
                         Mkt_Item = t7,
                         Mkt_BOMSlave = t1,
                         Raw_Item = t2,
                         Common_Supplier = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         Common_Unit = t6,
                         Common_Currency = t8
                     }).FirstOrDefault();
            return v;
        }

        internal void GetBOMSlaveByID(int id)
        {

            db = new xOssContext();
            var a = (from t1 in db.Mkt_BOMSlave
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     where t1.Active == true
                     select new VmMkt_CBS
                     {
                         Mkt_BOMSlave = t1,
                         Raw_Item = t2
                     }).Where(x => x.Mkt_BOMSlave.ID == id).SingleOrDefault();
            this.Mkt_BOMSlave = a.Mkt_BOMSlave;
            this.Raw_Item = a.Raw_Item;
        }
        //----------------------PO change with BOM edit--------------------------//
        public bool EditPOwithBOMslave(int userID, int BOMslaveFk)
        {
            string id = BOMslaveFk.ToString();

            VmMktPO_Slave x = new VmMktPO_Slave();
              VmMkt_CBS y = new VmMkt_CBS();
                x.SelectSingle(BOMslaveFk);
                y.SelectSingle(id);
            if (x.Mkt_POSlave != null)
            {
                x.Mkt_POSlave.Price = y.Mkt_BOMSlave.Price;
                x.Mkt_POSlave.TotalRequired = y.Mkt_BOMSlave.RequiredQuantity;
                x.Mkt_POSlave.Raw_ItemFK = y.Mkt_BOMSlave.Raw_ItemFK;
                x.Mkt_POSlave.Description = y.Mkt_BOMSlave.Description;
                return x.Mkt_POSlave.Edit(userID);
            }
            return false;
        }



        ///.....................StepJobs................//
    


        internal void SelectSingleStepJobs(int id)
          {

            db = new xOssContext();
             var a = (from mkt_BOMStepJobs in db.Mkt_BOMStepJobs
                     join mkt_BOMSlave in db.Mkt_BOMSlave
                     on mkt_BOMStepJobs.Mkt_BOMSlaveFK equals mkt_BOMSlave.ID
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMStepJobs.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMStepJobs.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMStepJobs.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMStepJobs.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMStepJobs.Raw_ItemFK equals Raw_Item.ID
                     join t1 in db.Raw_SubCategory
                     on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category 
                     on t1.Raw_CategoryFK equals t2.ID

                     select new VmMkt_CBS
                     {
                         TotalRequired = mkt_BOMSlave.Consumption * Mkt_BOM.Quantity * Mkt_BOM.QPack,
                         TotalItem_Price = mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price,
                         Mkt_BOMStepJobs = mkt_BOMStepJobs,
                       
                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Raw_Category = t2,
                         Raw_SubCategory = t1,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency

                     }).Where(c => c.Mkt_BOMStepJobs.ID == id).SingleOrDefault();
            this.Mkt_BOMStepJobs = a.Mkt_BOMStepJobs;
            this.TotalRequired = a.TotalRequired;
            this.TotalItem_Price = a.TotalItem_Price;
            this.Mkt_BOM = a.Mkt_BOM;
            this.Mkt_BOMSlave = a.Mkt_BOMSlave;
            this.Common_Supplier = a.Common_Supplier;
            this.Common_Unit = a.Common_Unit;
            this.Common_Currency = a.Common_Currency;
            this.Raw_Item = a.Raw_Item;
            this.RawCatID = a.Raw_Category.ID;
            this.RawSubCatID = a.Raw_SubCategory.ID;



        }

        internal void GetStepJobsByID(int id)
        {

            db = new xOssContext();
            var a = (from t1 in db.Mkt_BOMStepJobs
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     where t1.Active == true
                     select new VmMkt_CBS
                     {
                         Mkt_BOMStepJobs = t1,
                         Raw_Item = t2
                     }).Where(x => x.Mkt_BOMStepJobs.ID == id).SingleOrDefault();
            this.Mkt_BOMStepJobs = a.Mkt_BOMStepJobs;
            this.Raw_Item = a.Raw_Item;
        }


    }

}