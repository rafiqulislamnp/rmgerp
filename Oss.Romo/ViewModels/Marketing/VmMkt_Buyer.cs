﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Oss.Romo.Models.Common;
using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Reports;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_Buyer
    {
        private xOssContext db = new xOssContext();
        public IEnumerable<VmMkt_Buyer> DataList { get; set; }
        public List<ReportDocType> ReportDoc { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Common_Country Common_Country { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }

        #region ModelProperty
        public int ID { get; set; }
        [DefaultValue(0)]
        [DisplayName("Buyer ID")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string BuyerID { get; set; }
        [DisplayName("Buyer Name")]
        [Required(ErrorMessage = "Buyer Name Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Name { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [MaxLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string Phone { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DisplayName("Contact Person")]
        public string ContactPerson { get; set; }

        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [DisplayName("Country")]
        public int Common_CountryFk { get; set; }

        [DisplayName("Country")]
        public string Country { get; set; }

        [DisplayName("Account Head Category")]
        public int Acc_AcNameFk { get; set; }

        [DisplayName("Chart 2 Account")]
        public int Acc_Chart2IDFk { get; set; }

        [DisplayName("First Notyfy Party")]
        public string FirstNotyfyParty { get; set; }

        [DisplayName("Second Notyfy Party")]
        public string SecondNotyfyParty { get; set; }

        [DisplayName("Third Notyfy Party")]
        public string ThirdNotyfyParty { get; set; }

        public string BillTo { get; set; }

        #endregion


        //public Emran Emran { get; set; }
        public VmMkt_Buyer()
        {
            //db = new xOssContext();
            //InitialDataLoad();
            VmControllerHelper = new VmControllerHelper();

        }

        public void InitialDataLoad()
        {
            //db = new xOssContext();
            var v = (from t1 in db.Mkt_Buyer
                     join t2 in db.Common_Country
                     on t1.Common_CountryFk equals t2.ID
                     where t1.Active == true
                     select new VmMkt_Buyer
                     {
                         ID = t1.ID,
                         BuyerID = t1.BuyerID,
                         Name = t1.Name,
                         Country = t2.Name,
                         Address = t1.Address,
                         Phone = t1.Phone
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            this.DataList = v;

        }

        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);

            db = new xOssContext();
            var v = (from rc in db.Mkt_Buyer
                     join t2 in db.Common_Country
                     on rc.Common_CountryFk equals t2.ID
                     select new VmMkt_Buyer
                     {
                         Acc_AcNameFk = rc.Acc_AcNameFk,
                         Address = rc.Address,
                         BillTo = rc.BillTo,
                         BuyerID = rc.BuyerID,
                         Common_CountryFk = rc.Common_CountryFk,
                         ContactPerson = rc.ContactPerson,
                         Country = t2.Name,
                         Phone = rc.Phone,
                         Name = rc.Name,
                         Description = rc.Description,
                         FirstNotyfyParty = rc.FirstNotyfyParty,
                         Email = rc.Email,
                         ID = rc.ID,
                         SecondNotyfyParty = rc.SecondNotyfyParty,
                         ThirdNotyfyParty = rc.ThirdNotyfyParty,
                         Common_Country = t2,
                         Mkt_Buyer = rc
                     }).Where(c => c.Mkt_Buyer.ID == id).SingleOrDefault();
            this.Acc_AcNameFk = v.Acc_AcNameFk;
            this.Common_CountryFk = v.Common_CountryFk;
            this.Email = v.Email;
            this.Name = v.Name;
            this.Phone = v.Phone;
            this.Country = v.Country;
            this.Address = v.Address;
            this.BuyerID = v.BuyerID;
            this.ContactPerson = v.ContactPerson;
            this.Mkt_Buyer = v.Mkt_Buyer;
            this.Description = v.Description;
            this.BillTo = v.BillTo;
            this.FirstNotyfyParty = v.FirstNotyfyParty;
            this.ID = v.ID;
            this.SecondNotyfyParty = v.SecondNotyfyParty;
            this.ThirdNotyfyParty = v.ThirdNotyfyParty;
            this.Common_Country = v.Common_Country;
            
        }

        public int Add(int userID)
        {
            Mkt_Buyer.AddReady(userID);
            return Mkt_Buyer.Add();
        }

        public bool Edit(int userID)
        {
            return Mkt_Buyer.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_Buyer.Delete(userID);
        }

        internal void ReportDocLoad()
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_Buyer
                     select new ReportDocType
                     {
                         //  HeaderTop1 = ((t1.ID * 5)- t1.ID).ToString(),
                         HeaderTop2 = t1.Name,
                         HeaderLeft1 = t1.Description

                     }).ToList();
            this.ReportDoc = v;

        }
    }

}