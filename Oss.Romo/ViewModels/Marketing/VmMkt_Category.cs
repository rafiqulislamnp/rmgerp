﻿using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_Category
    {
        private xOssContext db;
        public Mkt_Category Mkt_Category { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public IEnumerable<VmMkt_Category> DataList { get; set; }


        public VmMkt_Category()
        {
            VmControllerHelper = new VmControllerHelper();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_Category
                     where t1.Active == true
                     select new VmMkt_Category
                     {
                         Mkt_Category = t1
                     }).Where(x=>x.Mkt_Category.Active==true).OrderByDescending(x => x.Mkt_Category.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(string Cid)
        {
            int id = 0;
            Cid = this.VmControllerHelper.Decrypt(Cid);
            Int32.TryParse(Cid, out id);

            db = new xOssContext();
            var a = (from t1 in db.Mkt_Category
                     select new VmMkt_Category
                     {
                         Mkt_Category = t1
                     }).Where(V => V.Mkt_Category.ID == id).SingleOrDefault();

            this.Mkt_Category = a.Mkt_Category;



        }


        //Create
        public int Add(int userID)
        {

            Mkt_Category.AddReady(userID);
            return Mkt_Category.Add();
        }
        //Edit
        public bool Edit(int userID)
        {
            return Mkt_Category.Edit(userID);
        }
        //Delete
        public bool Delete(int userID)
        {
            return Mkt_Category.Delete(userID);
        }



    }
}