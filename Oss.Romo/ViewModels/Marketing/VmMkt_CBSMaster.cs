﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Planning;
using Oss.Romo.ViewModels.Store;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.Models.Store;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_CBSMaster
    {
        [DisplayName("PC")]
        public int QuantityInPc { get; set; }
        [DisplayName("Total Price")]
        public decimal TotalPrice { get; set; }
        [DisplayName("DZ")]
        public decimal QuantityInDz { get; set; }
        public string SearchString { get; set; }
        public string Header { get; set; }
        public string ButtonName { get; set; }
        public User_User User_User { get; set; }
        private xOssContext db;
        public IEnumerable<VmMkt_CBSMaster> DataList { get; set; }
        public VmMkt_PO VmMkt_PO { get; set; }
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public Mkt_CBSMaster Mkt_CBSMaster { get; set; }
        public VmMkt_CBS VmMkt_CBS { get; set; }
        public VmMkt_PO SuggestedVmMkt_PO { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public List<object> DDownData { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public VmCommon_TheOrder VmCommon_TheOrder { get; set; }
        public VmMkt_Item VmMkt_Item { get; set; }
        public Mkt_BOMSlaveFabric Mkt_BOMSlaveFabric { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public VmPlan_OrderLine VmPlan_OrderLine { get; set; }
        public Mkt_Category Mkt_Category { get; set; }
        public Mkt_SubCategory Mkt_SubCategory { get; set; }
        public decimal? TotalValueOfStyle { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public decimal? Total_Value { get; set; }
        public decimal TotalVal { get; set; }
       // public VmProd_MasterPlan VmProd_MasterPlan { get; set; }
        public int? MktSubCatID { get; set; }
        public int? MktCatID { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Mkt_POSlave_Consumption Mkt_POSlave_Consumption { get; set; }
        public int ItemId { get; set; }
        public string CustomeCID { get; set; }
        public List<ReportDocType> BOMReportDoc { get; set; }
        public VmMkt_YarnCalculation VmMkt_YarnCalculation { get; set; }
        public decimal TotalFOB { get; set; }

     
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmMkt_CBSMaster()
        {
            db = new xOssContext();
            VmControllerHelper = new VmControllerHelper();

        }
       
        internal void InitialDataLoadFromBOMItem()
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                         //join mkt_Buyer in db.Mkt_Buyer
                         //on mkt_BOM.Mkt_BuyerFK equals mkt_Buyer.ID
                         //join common_Currency in db.Common_Currency
                         //on mkt_BOM.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     select new VmMkt_CBSMaster
                     {
                         Mkt_BOM = mkt_BOM,
                         //Mkt_Buyer = mkt_Buyer,
                         //Common_Currency = common_Currency,
                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();
            this.DataList = v;

        }




        public void ChangeBOMStatus(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);

            VmMkt_CBSMaster x = new VmMkt_CBSMaster();
            x.SelectSingle(iD);
            x.Mkt_BOM.IsComplete = !x.Mkt_BOM.IsComplete;
            x.Edit(0);
        }

        public decimal Total { get; set; }
        public void InitialDataLoad()
        {
            var v = (from mkt_CBSMaster in db.Mkt_CBSMaster
                     
                     join mkt_Item in db.Mkt_Item
                     on mkt_CBSMaster.Mkt_ItemFK equals mkt_Item.ID

                     join mkt_SubCategory in db.Mkt_SubCategory
                     on mkt_Item.Mkt_SubCategoryFK equals mkt_SubCategory.ID

                     join mkt_Category in db.Mkt_Category
                    on mkt_SubCategory.Mkt_CategoryFK equals mkt_Category.ID

                     join user_User in db.User_User
                     on mkt_CBSMaster.FirstCreatedBy equals user_User.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where mkt_CBSMaster.Active == true
                     select new VmMkt_CBSMaster
                     {
                         Mkt_CBSMaster = mkt_CBSMaster,
                        
                         User_User = user_User,
                         Mkt_SubCategory = mkt_SubCategory,
                         Mkt_Category = mkt_Category,

                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit,
                         Total = mkt_CBSMaster.QPack * mkt_CBSMaster.Quantity * mkt_CBSMaster.UnitPrice,
                         //Common_Currency = common_Currency
                     }).Where(x => x.Mkt_CBSMaster.Active == true).OrderByDescending(x => x.Mkt_CBSMaster.ID).AsEnumerable();


            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 1)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }





        }

        public void InitialStyleMLCDataLoad()
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                     on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     //join commercial_MasterLCBuyerPO in db.Commercial_MasterLCBuyerPO
                     //on mkt_BOM.ID equals commercial_MasterLCBuyerPO.MKTBOMFK

                     where order.Active == true && mkt_BOM.Active == true
                           && (from x in db.Commercial_MasterLCBuyerPO where x.Active == true select x.MKTBOMFK).Contains(mkt_BOM.ID)
                     select new VmMkt_CBSMaster
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         Mkt_Item = mkt_Item,
                         TotalFOB = mkt_BOM.QPack * mkt_BOM.Quantity * mkt_BOM.UnitPrice,
                         Common_Currency = common_Currency
                     }).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();


            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 1)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }






        }


        public void InitialStyleWithoutMLCDataLoad()
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                         on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                         on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                         on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     //join commercial_MasterLCBuyerPO in db.Commercial_MasterLCBuyerPO
                     //on mkt_BOM.ID equals commercial_MasterLCBuyerPO.MKTBOMFK

                     where order.Active == true && mkt_BOM.Active == true
                                                && !(from x in db.Commercial_MasterLCBuyerPO where x.Active == true select x.MKTBOMFK).Contains(mkt_BOM.ID)
                     select new VmMkt_CBSMaster
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         Mkt_Item = mkt_Item,
                         // TotalFOB = mkt_BOM.QPack  * mkt_BOM.Quantity * mkt_BOM.UnitPrice,
                         Common_Currency = common_Currency
                     }).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();


            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 1)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }
        }

        public void InitialBOMSearch(bool flag, string SearchString)
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                     on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID

                     join mkt_SubCategory in db.Mkt_SubCategory
                     on mkt_Item.Mkt_SubCategoryFK equals mkt_SubCategory.ID

                     join mkt_Category in db.Mkt_Category
                     on mkt_SubCategory.Mkt_CategoryFK equals mkt_Category.ID

                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where mkt_BOM.IsComplete == flag
                     select new VmMkt_CBSMaster
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,

                         Mkt_SubCategory = mkt_SubCategory,
                         Mkt_Category = mkt_Category,

                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit,

                         Common_Currency = common_Currency,
                         CustomeCID = order.CID + "/" + mkt_BOM.Style
                     }).Where(x => x.Mkt_BOM.Active == true && x.CustomeCID.Contains(SearchString)).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();
            this.DataList = v;

        }
        internal void GetPoByStyle(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder
                     on t2.Common_TheOrderFk equals t3.ID
                     join User_User in db.User_User
                     on Mkt_PO.FirstCreatedBy equals User_User.ID
                     where t2.Active == true
                     && Mkt_PO.Mkt_BOMFK == id
                     select new VmMkt_CBSMaster
                     {
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,
                         Common_TheOrder = t3
                     }).Where(x => x.Mkt_PO.Active == true).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            List<VmMkt_CBSMaster> list = new List<VmMkt_CBSMaster>();
            foreach (VmMkt_CBSMaster x in c)
            {
                VmMkt_PO vmMktPo = new VmMkt_PO();
                x.Total_Value = vmMktPo.TotlaValue(x.Mkt_PO.ID);
                // x.TotalValueOfStyle += x.Total_Value;
                list.Add(x);
            }
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 1)
            {
                this.DataList = list.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = list;
            }


        }


        public void GetAuthorizedBOM(bool flag)
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                   on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID

                     join mkt_SubCategory in db.Mkt_SubCategory
                     on mkt_Item.Mkt_SubCategoryFK equals mkt_SubCategory.ID

                     join mkt_Category in db.Mkt_Category
                    on mkt_SubCategory.Mkt_CategoryFK equals mkt_Category.ID

                     join user_User in db.User_User
                     on order.FirstCreatedBy equals user_User.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where mkt_BOM.IsComplete == flag
                     && mkt_BOM.IsAuthorize == true
                     select new VmMkt_CBSMaster
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         User_User = user_User,
                         Mkt_SubCategory = mkt_SubCategory,
                         Mkt_Category = mkt_Category,

                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit,

                         Common_Currency = common_Currency
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();


            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 1)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }


        }

        public VmMkt_CBSMaster SelectSingleJoined(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            var v = (from Mkt_CBSMaster in db.Mkt_CBSMaster
                     
                     join mkt_Item in db.Mkt_Item
                     on Mkt_CBSMaster.Mkt_ItemFK equals mkt_Item.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where Mkt_CBSMaster.ID == id
                     select new VmMkt_CBSMaster
                     {
                         
                         Mkt_CBSMaster = Mkt_CBSMaster,
                        
                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit
                         QuantityInPc = Mkt_CBSMaster.QPack * Mkt_CBSMaster.Quantity
                     }).FirstOrDefault();
            return v;
        }

        public VmMkt_CBSMaster SelectSingleOrder(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                     on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where mkt_BOM.Common_TheOrderFk == id
                     select new VmMkt_CBSMaster
                     {
                         Common_Currency = common_Currency,
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit
                         QuantityInPc = mkt_BOM.QPack * mkt_BOM.Quantity
                     }).FirstOrDefault();
            return v;
        }

        internal bool CreateCID(int userID, int bomID)
        {
            Mkt_BOM.ID = bomID;
            Mkt_BOM.CID = "ROMO/" + (10000 + bomID).ToString();
            return Mkt_BOM.Edit(userID);
        }
        public void SelectSingle(int id)
        {
            
            db = new xOssContext();
            var v = (from Mkt_CBSMaster in db.Mkt_CBSMaster
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_CBSMaster.Mkt_ItemFK equals Mkt_Item.ID
                     join Mkt_SubCategory in db.Mkt_SubCategory
                     on Mkt_Item.Mkt_SubCategoryFK equals Mkt_SubCategory.ID
                     join Mkt_Category in db.Mkt_Category
                     on Mkt_SubCategory.Mkt_CategoryFK equals Mkt_Category.ID

                     select new VmMkt_CBSMaster
                     {
                         Mkt_SubCategory = Mkt_SubCategory,
                         Mkt_Category = Mkt_Category,
                         Mkt_Item = Mkt_Item,
                         Mkt_CBSMaster = Mkt_CBSMaster
                     }).Where(c => c.Mkt_CBSMaster.ID == id).SingleOrDefault();
            if (v != null)
            {
                this.Mkt_CBSMaster = v.Mkt_CBSMaster;
                this.Mkt_Item = v.Mkt_Item;
                this.MktCatID = v.Mkt_Category.ID;
                this.MktSubCatID = v.Mkt_SubCategory.ID;
                this.ItemId = v.Mkt_Item.ID;
            }

        }



        public void GetBOMByID(string iD)
        {
            int id = 0;

            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from Mkt_BOM in db.Mkt_BOM
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join Mkt_SubCategory in db.Mkt_SubCategory
                     on Mkt_Item.Mkt_SubCategoryFK equals Mkt_SubCategory.ID
                     join Mkt_Category in db.Mkt_Category
                     on Mkt_SubCategory.Mkt_CategoryFK equals Mkt_Category.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     select new VmMkt_CBSMaster
                     {
                         Mkt_SubCategory = Mkt_SubCategory,
                         Mkt_Category = Mkt_Category,
                         Mkt_Item = Mkt_Item,
                         Mkt_BOM = Mkt_BOM,
                         Common_TheOrder = Common_TheOrder
                     }).Where(c => c.Mkt_BOM.ID == id).SingleOrDefault();
            this.Mkt_BOM = v.Mkt_BOM;
            this.Mkt_Item = v.Mkt_Item;
            this.Common_TheOrder = v.Common_TheOrder;
            this.MktCatID = v.Mkt_Category.ID;
            this.MktSubCatID = v.Mkt_SubCategory.ID;
            this.ItemId = v.Mkt_Item.ID;
        }

        public int Add(int userID)
        {
            //Mkt_BOM.FirstMove = DateTime.Now;
            //Mkt_BOM.UpdateDate = DateTime.Now;
            Mkt_CBSMaster.AddReady(userID);
            return Mkt_CBSMaster.Add();
        }

        public bool Edit(int userID)
        {
            return Mkt_CBSMaster.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_CBSMaster.Delete(userID);
        }
        //----------------------------//
        internal void GetBOM(string tiD)
        {
            int id = 0;
            tiD = this.VmControllerHelper.Decrypt(tiD);
            Int32.TryParse(tiD, out id);
            db = new xOssContext();
            var a = (from t1 in db.Mkt_BOM
                     join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                     join t3 in db.Mkt_Item on t1.Mkt_ItemFK equals t3.ID
                     //join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
                     where t1.Active == true
                     && t1.Common_TheOrderFk == id
                     select new VmMkt_CBSMaster
                     {
                         Mkt_BOM = t1,
                         Common_TheOrder = t2,
                         Mkt_Item = t3,
                         //Common_Unit = t4,
                         QuantityInPc = t1.QPack * t1.Quantity,
                         QuantityInDz = (t1.QPack * t1.Quantity) / 12,
                         TotalPrice = t1.QPack * t1.Quantity * t1.UnitPrice
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();
            this.DataList = a;

        }

        //...For BOM Crystall Report...//
        //internal void YarnReportDocLoad(string id)
        //{
        //    int iD = 0;
        //    id = VmControllerHelper.Decrypt(id);
        //    Int32.TryParse(id, out iD);
        //    db = new xOssContext();

        //    //VmMkt_CBSMaster VmMkt_CBSMaster = new VmMkt_CBSMaster();
        //    //string yarnValue = VmMkt_CBSMaster.GetYarnValue(id);
        //    //decimal yarnVal = Convert.ToDecimal(yarnValue);
        //    //string lycraValue = VmMkt_CBSMaster.GetLycraValue(id);
        //    //decimal lycraVal = Convert.ToDecimal(lycraValue);
        //    //string yarnConsumption = VmMkt_CBSMaster.GetAverageConsumption(id);
        //    //decimal YarnCon = Convert.ToDecimal(yarnConsumption);
        //    var a = (from mktCbs in db.Mkt_CBS
        //             join Mkt_BOM in db.Mkt_BOM
        //             on mktCbs.Mkt_BOMFK equals Mkt_BOM.ID
        //             join Common_Currency in db.Common_Currency
        //             on mktCbs.Common_CurrencyFK equals Common_Currency.ID
        //             join Raw_Item in db.Raw_Item
        //             on mktCbs.Raw_ItemFK equals Raw_Item.ID

        //             join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
        //             join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID

        //             join Common_Unit in db.Common_Unit
        //             on mktCbs.Common_UnitFK equals Common_Unit.ID
        //             //join Common_Supplier in db.Common_Supplier
        //             //on mktCbs.Common_SupplierFK equals Common_Supplier.ID
        //             join Common_TheOrder in db.Common_TheOrder
        //             on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
        //             join Mkt_Item in db.Mkt_Item
        //             on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
        //             join Mkt_Buyer in db.Mkt_Buyer
        //             on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
        //             where Mkt_BOM.ID == iD && mktCbs.Active == true
        //             join User_User in db.User_User on Common_TheOrder.FirstCreatedBy equals User_User.ID
        //             select new ReportDocType
        //             {
        //                 HeaderLeft1 = ": " + Common_TheOrder.CID + "/" + Mkt_BOM.Style,
        //                 HeaderLeft2 = Mkt_BOM.FirstMove.ToString(),
        //                 HeaderLeft3 = Mkt_BOM.UpdateDate.ToString(),
        //                 HeaderLeft4 = ": " + Mkt_BOM.Reference.ToString(),
        //                 HeaderLeft5 = ": " + Mkt_BOM.Class.ToString(),
        //                 HeaderLeft6 = ": " + Mkt_BOM.Fabrication,
        //                 HeaderLeft7 = ": " + Mkt_BOM.SizeRange,
        //                 HeaderLeft8 = ": " + Mkt_BOM.Style,
        //                 HeaderMiddle1 = ": " + Mkt_Buyer.Name,
        //                 HeaderMiddle2 = ": " + Common_TheOrder.BuyerPO,
        //                 HeaderMiddle3 = Common_TheOrder.OrderDate.ToString(),
        //                 HeaderMiddle4 = ": " + Common_TheOrder.Season,
        //                 HeaderMiddle5 = ": " + Mkt_Item.Name,
        //                 HeaderRight1 = ": " + Mkt_BOM.QPack.ToString(),
        //                 HeaderRight2 = ": " + Mkt_BOM.Quantity.ToString(),
        //                 HeaderRight3 = ": " + Mkt_BOM.UnitPrice.ToString(),
        //                 HeaderRight4 = ": " + ((Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString()),
        //                 HeaderRight5 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
        //                 HeaderRight6 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
        //                 HeaderRight7 = ": " + User_User.Name,
        //                 Body1 = Raw_Item.Name,
        //                 Body2 = mktCbs.Description,
        //                 Body3 = ((mktCbs.Consumption)).ToString(),//(Raw_Item.ID == 1045 ? YarnCon : (Raw_Item.ID == 5331 ? YarnCon : Mkt_BOMSlave.Consumption)).ToString(),
        //                 Body4 = Common_Unit.Name,
        //                 Body5 = mktCbs.Price.ToString(),
        //                 // Body6 = Common_Supplier.Name,
        //                 //Body7 = ((Mkt_BOMSlave.Consumption * (Mkt_BOM.QPack * Mkt_BOM.Quantity)) * Mkt_BOMSlave.Price).ToString(),
        //                 Body7 = ((mktCbs.RequiredQuantity * mktCbs.Price)).ToString(),// (Raw_Item.ID == 1045 ? yarnVal * Mkt_BOMSlave.Price : (Raw_Item.ID == 5331 ? lycraVal * Mkt_BOMSlave.Price : Mkt_BOMSlave.RequiredQuantity * Mkt_BOMSlave.Price)).ToString(),


        //                 Body8 = mktCbs.IsFabric.ToString(),
        //                 Body9 = ((mktCbs.RequiredQuantity)).ToString(),// (Raw_Item.ID == 1045 ? yarnVal :(Raw_Item.ID == 5331 ? lycraVal: Mkt_BOMSlave.RequiredQuantity)).ToString(),
        //                 Body10 = Common_Currency.Symbol,
        //                 SubBody8 = (((((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice) * Common_TheOrder.Commission)) / 100).ToString(),
        //                 Body11 = Common_TheOrder.Commission.ToString() + "% ",
        //                 Body12 = t2.Name,
        //                 int1 = t2.ID
        //             }).OrderBy(x => x.int1).ToList();

        //    decimal GroupTTL = 0;
        //    decimal TTL = 0;
        //    decimal TTLFabrics = 0;
        //    decimal TTLAccount = 0;
        //    decimal TTLCM = 0;
        //    decimal TTLFOB = 0;
        //    decimal CMperDZ = 0;
        //    decimal TTLQDZ = 0;
        //    decimal Comm = 0;


        //    foreach (ReportDocType r in a)
        //    {

        //        r.Body9 = r.Body9 + " " + r.Body4;
        //        decimal body7 = 0;
        //        decimal.TryParse(r.Body7, out body7);
        //        TTL += body7;

        //        if (r.Body8 == "True")
        //        {
        //            decimal subbody2 = 0;
        //            decimal.TryParse(r.Body7, out subbody2);
        //            TTLFabrics += subbody2;
        //        }
        //        else
        //        {

        //            decimal subbody2 = 0;
        //            decimal.TryParse(r.Body7, out subbody2);
        //            TTLAccount += subbody2;

        //        }
        //        decimal temp = 0;
        //        decimal.TryParse(r.Body3, out temp);
        //        r.Body3 = temp.ToString("F");
        //        decimal.TryParse(r.Body5, out temp);
        //        r.Body5 = r.Body10 + temp.ToString("F");

        //        decimal subbody3 = 0;
        //        decimal.TryParse(r.HeaderRight6, out subbody3);
        //        TTLFOB = subbody3;
        //        decimal subbody4 = 0;
        //        decimal.TryParse(r.HeaderRight5, out subbody4);
        //        TTLQDZ = subbody4;
        //        //TTLAccount = TTL - TTLFabrics;
        //        TTLCM = TTLFOB - (TTL + Comm);
        //        CMperDZ = TTLCM / TTLQDZ;
        //        decimal SubBody8 = 0;
        //        decimal.TryParse(r.SubBody8, out SubBody8);
        //        Comm = SubBody8;

        //    }
        //    if (a.Count != 0)
        //    {
        //        string tempBody = a[0].Body12;


        //        decimal groupTotal = 0;
        //        int counter = 1;
        //        List<string> groupList = new List<string>();
        //        foreach (ReportDocType r in a)
        //        {
        //            r.SubBody1 = r.Body10 + TTL.ToString("F");
        //            r.SubBody2 = r.Body10 + TTLFabrics.ToString("F");
        //            r.SubBody3 = r.Body10 + TTLFOB.ToString("F");
        //            r.SubBody4 = TTLQDZ.ToString("F");
        //            r.SubBody5 = r.Body10 + TTLAccount.ToString("F");
        //            r.SubBody6 = r.Body10 + TTLCM.ToString("F");
        //            r.SubBody7 = r.Body10 + CMperDZ.ToString("F");
        //            r.SubBody8 = "(" + r.Body11 + ")" + r.Body10 + Comm.ToString("F");
        //            decimal temp = 0;

        //            decimal.TryParse(r.Body7, out temp);
        //            r.Body7 = r.Body10 + temp.ToString("F");

        //            r.HeaderLeft2 = ": " + r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
        //            r.HeaderLeft3 = ": " + r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
        //            r.HeaderMiddle3 = ": " + r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);

        //            if (tempBody == r.Body12)
        //            {
        //                decimal tempValue = 0;
        //                string ddd = r.Body7.Substring(0, r.Body7.Length - 1);
        //                decimal.TryParse(r.Body7.Substring(1, r.Body7.Length - 1), out tempValue);
        //                groupTotal += tempValue;
        //            }

        //            else
        //            {

        //                groupList.Add(tempBody + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")");
        //                decimal tempValue = 0;
        //                string ddd = r.Body7.Substring(0, r.Body7.Length - 1);
        //                decimal.TryParse(r.Body7.Substring(1, r.Body7.Length - 1), out tempValue);
        //                groupTotal = tempValue;


        //            }
        //            if (counter == a.Count())
        //            {
        //                //tempBody = r.Body12 + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")";
        //                groupList.Add(r.Body12 + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")");
        //            }
        //            tempBody = r.Body12;

        //            counter++;
        //        }
        //        string tempGroupTotal = "";
        //        foreach (string st in groupList)
        //        {
        //            tempGroupTotal += st + "\n";

        //        }
        //        foreach (string st in groupList)
        //        {
        //            foreach (ReportDocType r in a)
        //            {
        //                r.Body13 = tempGroupTotal;
        //                if (st.Contains(r.Body12))
        //                {
        //                    r.Body12 = st;
        //                }
        //            }
        //        }
        //    }
        //    this.BOMReportDoc = a.ToList();
        //}




        public bool BOMAuthorization(int userID)
        {

            return Mkt_CBSMaster.Edit(userID);
        }
        
    }

}