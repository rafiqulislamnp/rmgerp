﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Planning;
using Oss.Romo.ViewModels.Store;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.Models.Store;
using System.ComponentModel.DataAnnotations;
using CrystalDecisions.CrystalReports.Engine;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_BOM
    {
        [DisplayName("PC")]
        public int QuantityInPc { get; set; }
        [DisplayName("Total Price")]
        public decimal TotalPrice { get; set; }
        [DisplayName("DZ")]
        public decimal QuantityInDz { get; set; }
        public string SearchString { get; set; }
        public string Header { get; set; }
        public string ButtonName { get; set; }
        public User_User User_User { get; set; }
        private xOssContext db;
        public IEnumerable<VmMkt_BOM> DataList { get; set; }
        public VmMkt_PO VmMkt_PO { get; set; }
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        //VmMkt_BOMSlaveFabric VmMkt_BOMSlaveFabric;
        public VmMkt_BOMSlave VmMkt_BOMSlave { get; set; }
        public VmMkt_CBS VmMkt_CBS { get; set; }
        public VmMkt_PO SuggestedVmMkt_PO { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public List<object> DDownData { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public VmCommon_TheOrder VmCommon_TheOrder { get; set; }
        public VmMkt_Item VmMkt_Item { get; set; }
        public Mkt_BOMSlaveFabric Mkt_BOMSlaveFabric { get; set; }

        public Raw_Category Raw_Category { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public VmPlan_OrderLine VmPlan_OrderLine { get; set; }
        public Mkt_Category Mkt_Category { get; set; }
        public Mkt_SubCategory Mkt_SubCategory { get; set; }
        public decimal? TotalValueOfStyle { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public decimal? Total_Value { get; set; }
        public decimal TotalVal { get; set; }
        // public VmProd_MasterPlan VmProd_MasterPlan { get; set; }
        public int? MktSubCatID { get; set; }
        public int? MktCatID { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Mkt_POSlave_Consumption Mkt_POSlave_Consumption { get; set; }
        public int ItemId { get; set; }
        public string CustomeCID { get; set; }
        public List<ReportDocType> BOMReportDoc { get; set; }
        public VmMkt_YarnCalculation VmMkt_YarnCalculation { get; set; }
        public decimal TotalFOB { get; set; }
        [Required, DisplayName("From Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [Required, DisplayName("To Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public List<string> MasterLc { get; set; }
        public string PageType { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public VmMkt_BOM()
        {
            db = new xOssContext();
            VmControllerHelper = new VmControllerHelper();

        }
        internal List<object> GetUnitForDropDown()
        {
            var unitList = new List<object>();

            foreach (var unit in db.Common_Unit.Where(d => d.Active == true))
            {
                unitList.Add(new { Text = unit.Name, Value = unit.ID });
            }
            return unitList;
        }
        internal List<object> GetCountryForDropDown()
        {
            var countryList = new List<object>();

            foreach (var country in db.Common_Country.Where(d => d.Active == true))
            {
                countryList.Add(new { Text = country.Name, Value = country.ID });
            }
            return countryList;
        }
        public List<object> GetBuyerForDropDown()
        {
            var BuyerList = new List<object>();

            foreach (var buyer in db.Mkt_Buyer.Where(d => d.Active == true))
            {
                BuyerList.Add(new { Text = buyer.Name, Value = buyer.ID });
            }
            return BuyerList;
        }
        public List<object> GetCurrencyForDropDown()
        {
            var CurrencyList = new List<object>();

            foreach (var buyer in db.Common_Currency.Where(d => d.Active == true))
            {
                CurrencyList.Add(new { Text = buyer.Name, Value = buyer.ID });
            }
            return CurrencyList;
        }

        public List<object> GetMktItemForDropDown()
        {
            var ItemList = new List<object>();

            foreach (var item in db.Mkt_Item.Where(d => d.Active == true))
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }

        public List<object> GetMktSubCategoryForDropDown()
        {
            var sCategoryList = new List<object>();

            foreach (var sCategory in db.Mkt_SubCategory.Where(x => x.Active == true))
            {
                sCategoryList.Add(new { Text = sCategory.Name, Value = sCategory.ID });
            }
            return sCategoryList;
        }
        public List<object> GetMktCategoryForDropDown()
        {
            var categoryList = new List<object>();

            foreach (var category in db.Mkt_Category.Where(x => x.Active == true))
            {
                categoryList.Add(new { Text = category.Name, Value = category.ID });
            }
            return categoryList;
        }
        int? sabCatID;
        int? catID;
        int? mktSabCatID;
        int? mktCatID;
        public List<object> GetTempMktSubCategoryForEdit(int? id)
        {
            var scl = new List<object>();

            mktSabCatID = (from t3 in db.Mkt_Item where t3.ID == id select t3.Mkt_SubCategoryFK).FirstOrDefault();
            mktCatID = (
                    from t2 in db.Mkt_SubCategory
                    where t2.ID == mktSabCatID
                    select t2.Mkt_CategoryFK

                    ).FirstOrDefault();

            var x = (from t1 in db.Mkt_SubCategory
                     where t1.Mkt_CategoryFK == mktCatID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
                 ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }



        public List<object> GetRawItemForDropDown()
        {
            var rawItemList = new List<object>();

            foreach (var raw_Item in db.Raw_Item.Where(d => d.Active == true))
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });
            }
            return rawItemList;
        }
        public List<object> GetSupplierForDropDown()
        {
            var supplierList = new List<object>();

            foreach (var supplier in db.Common_Supplier.Where(d => d.Active == true))
            {
                supplierList.Add(new { Text = supplier.Name, Value = supplier.ID });
            }
            return supplierList;
        }
        public List<object> GetRaw_CategoryForDropDown()
        {
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }
        public List<object> GetBOMForDropDown()
        {
            var Mkt_BOMList = new List<object>();

            foreach (var bom in db.Mkt_BOM.Where(d => d.Active == true))
            {
                Mkt_BOMList.Add(new { Text = bom.CID, Value = bom.ID });
            }
            return Mkt_BOMList;
        }

        public List<object> GetRaw_ItemDropDownForEdit(int? id)
        {
            var rawItemList = new List<object>();
            var x = (from t1 in db.Raw_Item
                     where t1.Raw_SubCategoryFK == (from t2 in db.Raw_Item where t2.ID == id select t2.Raw_SubCategoryFK).FirstOrDefault()
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
                   ).ToList();


            foreach (var raw_Item in x)
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });

            }
            return rawItemList;
        }
        public List<object> GetRawSubCategoryDropDownForEdit(int? id)
        {
            var scl = new List<object>();

            sabCatID = (from t3 in db.Raw_Item where t3.ID == id select t3.Raw_SubCategoryFK).FirstOrDefault();
            catID = (
                    from t2 in db.Raw_SubCategory
                    where t2.ID == sabCatID
                    select t2.Raw_CategoryFK

                    ).FirstOrDefault();

            var x = (from t1 in db.Raw_SubCategory
                     where t1.Raw_CategoryFK == catID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
                 ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }

        public List<object> GetRaw_CategoryDropDownForEdit(int? id)
        {
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }
        public List<object> GetRaw_ItemDropDownEdit(int? id)
        {
            var rawItemList = new List<object>();
            var x = (from t1 in db.Raw_Item
                     where t1.Raw_SubCategoryFK == (from t2 in db.Raw_Item where t2.ID == id select t2.Raw_SubCategoryFK).FirstOrDefault()
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
                   ).ToList();


            foreach (var raw_Item in x)
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });

            }
            return rawItemList;
        }

        internal void InitialDataLoadFromBOMItem()
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                         //join mkt_Buyer in db.Mkt_Buyer
                         //on mkt_BOM.Mkt_BuyerFK equals mkt_Buyer.ID
                         //join common_Currency in db.Common_Currency
                         //on mkt_BOM.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = mkt_BOM,
                         //Mkt_Buyer = mkt_Buyer,
                         //Common_Currency = common_Currency,
                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();
            this.DataList = v;

        }




        public void ChangeBOMStatus(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);

            VmMkt_BOM x = new VmMkt_BOM();
            x.SelectSingle(id);
            x.Mkt_BOM.IsComplete = !x.Mkt_BOM.IsComplete;
            x.Edit(0);
        }

        public decimal Total { get; set; }
        public void InitialDataLoad(bool flag)
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                   on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID

                     join mkt_SubCategory in db.Mkt_SubCategory
                     on mkt_Item.Mkt_SubCategoryFK equals mkt_SubCategory.ID

                     join mkt_Category in db.Mkt_Category
                    on mkt_SubCategory.Mkt_CategoryFK equals mkt_Category.ID

                     join user_User in db.User_User
                     on order.FirstCreatedBy equals user_User.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where mkt_BOM.IsComplete == flag
                     && order.Active == true
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         User_User = user_User,
                         Mkt_SubCategory = mkt_SubCategory,
                         Mkt_Category = mkt_Category,

                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit,
                         Total = mkt_BOM.QPack * mkt_BOM.Quantity * mkt_BOM.UnitPrice,
                         Common_Currency = common_Currency
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();


            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }





        }

        public void InitialPeriodicDataLoad()
        {
            var v = (from Commercial_MasterLCs in db.Commercial_MasterLC
                     where Commercial_MasterLCs.Active == true
                     join Commercial_MasterLCBuyerPO in db.Commercial_MasterLCBuyerPO on Commercial_MasterLCs.ID equals Commercial_MasterLCBuyerPO.Commercial_MasterLCFK
                     join mkt_BOM in db.Mkt_BOM on Commercial_MasterLCBuyerPO.MKTBOMFK equals mkt_BOM.ID
                     join order in db.Common_TheOrder on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID

                     join mkt_SubCategory in db.Mkt_SubCategory
                     on mkt_Item.Mkt_SubCategoryFK equals mkt_SubCategory.ID

                     join mkt_Category in db.Mkt_Category
                    on mkt_SubCategory.Mkt_CategoryFK equals mkt_Category.ID

                     join user_User in db.User_User
                     on order.FirstCreatedBy equals user_User.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where //mkt_BOM.IsComplete == flag
                     order.Active == true
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         User_User = user_User,
                         Mkt_SubCategory = mkt_SubCategory,
                         Mkt_Category = mkt_Category,

                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit,
                         Total = mkt_BOM.QPack * mkt_BOM.Quantity * mkt_BOM.UnitPrice,
                         Common_Currency = common_Currency
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();

            this.DataList = v;
        }

        public void InitialStyleMLCDataLoad()
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                     on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     //join commercial_MasterLCBuyerPO in db.Commercial_MasterLCBuyerPO
                     //on mkt_BOM.ID equals commercial_MasterLCBuyerPO.MKTBOMFK

                     where order.Active == true && mkt_BOM.Active == true
                           && (from x in db.Commercial_MasterLCBuyerPO where x.Active == true select x.MKTBOMFK).Contains(mkt_BOM.ID)
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         Mkt_Item = mkt_Item,
                         TotalFOB = mkt_BOM.QPack * mkt_BOM.Quantity * mkt_BOM.UnitPrice,
                         Common_Currency = common_Currency,
                         MasterLc = (from t1 in db.Commercial_MasterLCBuyerPO
                                     join t2 in db.Commercial_MasterLC on t1.Commercial_MasterLCFK equals t2.ID
                                     where t1.MKTBOMFK == mkt_BOM.ID && t1.Active == true && t2.Active == true
                                     select t2.Name).ToList()
                     }).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();


            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }






        }


        public void InitialStyleWithoutMLCDataLoad()
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                         on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                         on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                         on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     //join commercial_MasterLCBuyerPO in db.Commercial_MasterLCBuyerPO
                     //on mkt_BOM.ID equals commercial_MasterLCBuyerPO.MKTBOMFK

                     where order.Active == true && mkt_BOM.Active == true
                                                && !(from x in db.Commercial_MasterLCBuyerPO where x.Active == true select x.MKTBOMFK).Contains(mkt_BOM.ID)
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         Mkt_Item = mkt_Item,
                         // TotalFOB = mkt_BOM.QPack  * mkt_BOM.Quantity * mkt_BOM.UnitPrice,
                         Common_Currency = common_Currency
                     }).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();


            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }
        }

        public void InitialBOMSearch(bool flag, string SearchString)
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                     on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID

                     join mkt_SubCategory in db.Mkt_SubCategory
                     on mkt_Item.Mkt_SubCategoryFK equals mkt_SubCategory.ID

                     join mkt_Category in db.Mkt_Category
                     on mkt_SubCategory.Mkt_CategoryFK equals mkt_Category.ID

                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where mkt_BOM.IsComplete == flag
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,

                         Mkt_SubCategory = mkt_SubCategory,
                         Mkt_Category = mkt_Category,

                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit,

                         Common_Currency = common_Currency,
                         CustomeCID = order.CID + "/" + mkt_BOM.Style
                     }).Where(x => x.Mkt_BOM.Active == true && x.CustomeCID.Contains(SearchString)).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();
            this.DataList = v;

        }
        internal void GetPoByStyle(int id)
        {
            db = new xOssContext();
            var c = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join t2 in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder
                     on t2.Common_TheOrderFk equals t3.ID
                     join User_User in db.User_User
                     on Mkt_PO.FirstCreatedBy equals User_User.ID
                     where t2.Active == true
                     && Mkt_PO.Mkt_BOMFK == id
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = t2,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier,
                         User_User = User_User,
                         Common_TheOrder = t3
                     }).Where(x => x.Mkt_PO.Active == true).Distinct().OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            List<VmMkt_BOM> list = new List<VmMkt_BOM>();
            foreach (VmMkt_BOM x in c)
            {
                VmMkt_PO vmMktPo = new VmMkt_PO();
                x.Total_Value = vmMktPo.TotlaValue(x.Mkt_PO.ID);
                // x.TotalValueOfStyle += x.Total_Value;
                list.Add(x);
            }
            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = list.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = list;
            }


        }


        public void GetAuthorizedBOM(bool flag)
        {
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                   on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID

                     join mkt_SubCategory in db.Mkt_SubCategory
                     on mkt_Item.Mkt_SubCategoryFK equals mkt_SubCategory.ID

                     join mkt_Category in db.Mkt_Category
                    on mkt_SubCategory.Mkt_CategoryFK equals mkt_Category.ID

                     join user_User in db.User_User
                     on order.FirstCreatedBy equals user_User.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where mkt_BOM.IsComplete == flag
                     && mkt_BOM.IsAuthorize == true
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         User_User = user_User,
                         Mkt_SubCategory = mkt_SubCategory,
                         Mkt_Category = mkt_Category,

                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit,

                         Common_Currency = common_Currency
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();


            if (this.User_User.User_AccessLevelFK == 1 && this.User_User.User_DepartmentFK == 8)
            {
                this.DataList = v.Where(x => x.User_User.ID == this.User_User.ID);
            }
            else
            {
                this.DataList = v;
            }


        }

        public VmMkt_BOM SelectSingleJoined(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                     on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where mkt_BOM.ID == id
                     select new VmMkt_BOM
                     {
                         Common_Currency = common_Currency,
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit
                         QuantityInPc = mkt_BOM.QPack * mkt_BOM.Quantity
                     }).FirstOrDefault();
            return v;
        }

        public VmMkt_BOM SelectSingleOrder(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            var v = (from mkt_BOM in db.Mkt_BOM
                     join order in db.Common_TheOrder
                     on mkt_BOM.Common_TheOrderFk equals order.ID
                     join common_Currency in db.Common_Currency
                     on order.Common_CurrencyFK equals common_Currency.ID
                     join mkt_Item in db.Mkt_Item
                     on mkt_BOM.Mkt_ItemFK equals mkt_Item.ID
                     //join common_Unit in db.Common_Unit
                     //on mkt_BOM.Common_UnitFK equals common_Unit.ID
                     where mkt_BOM.Common_TheOrderFk == id
                     select new VmMkt_BOM
                     {
                         Common_Currency = common_Currency,
                         Mkt_BOM = mkt_BOM,
                         Common_TheOrder = order,
                         Mkt_Item = mkt_Item,
                         //Common_Unit = common_Unit
                         QuantityInPc = mkt_BOM.QPack * mkt_BOM.Quantity
                     }).FirstOrDefault();
            return v;
        }

        internal bool CreateCID(int userID, int bomID)
        {
            Mkt_BOM.ID = bomID;
            Mkt_BOM.CID = "ROMO/" + (10000 + bomID).ToString();
            return Mkt_BOM.Edit(userID);
        }
        public void SelectSingle(string iD)
        {
            int id = 0;

            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from Mkt_BOM in db.Mkt_BOM
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join Mkt_SubCategory in db.Mkt_SubCategory
                     on Mkt_Item.Mkt_SubCategoryFK equals Mkt_SubCategory.ID
                     join Mkt_Category in db.Mkt_Category
                     on Mkt_SubCategory.Mkt_CategoryFK equals Mkt_Category.ID

                     select new VmMkt_BOM
                     {
                         Mkt_SubCategory = Mkt_SubCategory,
                         Mkt_Category = Mkt_Category,
                         Mkt_Item = Mkt_Item,
                         Mkt_BOM = Mkt_BOM
                     }).Where(c => c.Mkt_BOM.ID == id).SingleOrDefault();
            if (v != null)
            {
                this.Mkt_BOM = v.Mkt_BOM;
                this.Mkt_Item = v.Mkt_Item;
                this.MktCatID = v.Mkt_Category.ID;
                this.MktSubCatID = v.Mkt_SubCategory.ID;
                this.ItemId = v.Mkt_Item.ID;
            }

        }



        public void GetBOMByID(string iD)
        {
            int id = 0;

            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from Mkt_BOM in db.Mkt_BOM
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join Mkt_SubCategory in db.Mkt_SubCategory
                     on Mkt_Item.Mkt_SubCategoryFK equals Mkt_SubCategory.ID
                     join Mkt_Category in db.Mkt_Category
                     on Mkt_SubCategory.Mkt_CategoryFK equals Mkt_Category.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     select new VmMkt_BOM
                     {
                         Mkt_SubCategory = Mkt_SubCategory,
                         Mkt_Category = Mkt_Category,
                         Mkt_Item = Mkt_Item,
                         Mkt_BOM = Mkt_BOM,
                         Common_TheOrder = Common_TheOrder
                     }).SingleOrDefault(c => c.Mkt_BOM.ID == id);
            this.Mkt_BOM = v.Mkt_BOM;
            this.Mkt_Item = v.Mkt_Item;
            this.Common_TheOrder = v.Common_TheOrder;
            this.MktCatID = v.Mkt_Category.ID;
            this.MktSubCatID = v.Mkt_SubCategory.ID;
            this.ItemId = v.Mkt_Item.ID;
        }

        public int Add(int userID)
        {
            //Mkt_BOM.FirstMove = DateTime.Now;
            //Mkt_BOM.UpdateDate = DateTime.Now;
            Mkt_BOM.AddReady(userID);
            return Mkt_BOM.Add();
        }

        public bool Edit(int userID)
        {
            return Mkt_BOM.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_BOM.Delete(userID);
        }
        //----------------------------//
        internal void GetBOM(string tiD)
        {
            int id = 0;
            tiD = this.VmControllerHelper.Decrypt(tiD);
            Int32.TryParse(tiD, out id);
            db = new xOssContext();
            var a = (from t1 in db.Mkt_BOM
                     join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                     join t3 in db.Mkt_Item on t1.Mkt_ItemFK equals t3.ID
                     //join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
                     where t1.Active == true
                     && t1.Common_TheOrderFk == id
                     select new VmMkt_BOM
                     {
                         Mkt_BOM = t1,
                         Common_TheOrder = t2,
                         Mkt_Item = t3,
                         //Common_Unit = t4,
                         QuantityInPc = t1.QPack * t1.Quantity,
                         QuantityInDz = (t1.QPack * t1.Quantity) / 12,
                         TotalPrice = t1.QPack * t1.Quantity * t1.UnitPrice
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOM.ID).AsEnumerable();
            this.DataList = a;

        }

        //...For BOM Crystall Report...//
        //internal void YarnReportDocLoad(string id)
        //{
        //    int iD = 0;
        //    id = VmControllerHelper.Decrypt(id);
        //    Int32.TryParse(id, out iD);
        //    db = new xOssContext();

        //    //VmMkt_BOM vmMkt_BOM = new VmMkt_BOM();
        //    //string yarnValue = vmMkt_BOM.GetYarnValue(id);
        //    //decimal yarnVal = Convert.ToDecimal(yarnValue);
        //    //string lycraValue = vmMkt_BOM.GetLycraValue(id);
        //    //decimal lycraVal = Convert.ToDecimal(lycraValue);
        //    //string yarnConsumption = vmMkt_BOM.GetAverageConsumption(id);
        //    //decimal YarnCon = Convert.ToDecimal(yarnConsumption);
        //    var a = (from mktCbs in db.Mkt_CBS
        //             join Mkt_BOM in db.Mkt_BOM
        //             on mktCbs.Mkt_BOMFK equals Mkt_BOM.ID
        //             join Common_Currency in db.Common_Currency
        //             on mktCbs.Common_CurrencyFK equals Common_Currency.ID
        //             join Raw_Item in db.Raw_Item
        //             on mktCbs.Raw_ItemFK equals Raw_Item.ID

        //             join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
        //             join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID

        //             join Common_Unit in db.Common_Unit
        //             on mktCbs.Common_UnitFK equals Common_Unit.ID
        //             //join Common_Supplier in db.Common_Supplier
        //             //on mktCbs.Common_SupplierFK equals Common_Supplier.ID
        //             join Common_TheOrder in db.Common_TheOrder
        //             on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
        //             join Mkt_Item in db.Mkt_Item
        //             on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
        //             join Mkt_Buyer in db.Mkt_Buyer
        //             on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
        //             where Mkt_BOM.ID == iD && mktCbs.Active == true
        //             join User_User in db.User_User on Common_TheOrder.FirstCreatedBy equals User_User.ID
        //             select new ReportDocType
        //             {
        //                 HeaderLeft1 =": "+ Common_TheOrder.CID + "/" + Mkt_BOM.Style,
        //                 HeaderLeft2 = Mkt_BOM.FirstMove.ToString(),
        //                 HeaderLeft3 = Mkt_BOM.UpdateDate.ToString(),
        //                 HeaderLeft4 =": "+ Mkt_BOM.Reference.ToString(),
        //                 HeaderLeft5 =": "+ Mkt_BOM.Class.ToString(),
        //                 HeaderLeft6 =": "+ Mkt_BOM.Fabrication,
        //                 HeaderLeft7 =": "+ Mkt_BOM.SizeRange,
        //                 HeaderLeft8 =": "+ Mkt_BOM.Style,
        //                 HeaderMiddle1 =": "+ Mkt_Buyer.Name,
        //                 HeaderMiddle2 =": "+ Common_TheOrder.BuyerPO,
        //                 HeaderMiddle3 = Common_TheOrder.OrderDate.ToString(),
        //                 HeaderMiddle4 =": "+ Common_TheOrder.Season,
        //                 HeaderMiddle5 = ": " + Mkt_Item.Name,
        //                 HeaderRight1 =": "+ Mkt_BOM.QPack.ToString(),
        //                 HeaderRight2 =": "+ Mkt_BOM.Quantity.ToString(),
        //                 HeaderRight3 =": "+ Mkt_BOM.UnitPrice.ToString(),
        //                 HeaderRight4 =": "+ ((Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString()),
        //                 HeaderRight5 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
        //                 HeaderRight6 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
        //                 HeaderRight7 = ": " + User_User.Name,
        //                 Body1 = Raw_Item.Name,
        //                 Body2 = mktCbs.Description,
        //                 Body3 = ((mktCbs.Consumption)).ToString(),//(Raw_Item.ID == 1045 ? YarnCon : (Raw_Item.ID == 5331 ? YarnCon : Mkt_BOMSlave.Consumption)).ToString(),
        //                 Body4 = Common_Unit.Name,
        //                 Body5 = mktCbs.Price.ToString(),
        //                // Body6 = Common_Supplier.Name,
        //                 //Body7 = ((Mkt_BOMSlave.Consumption * (Mkt_BOM.QPack * Mkt_BOM.Quantity)) * Mkt_BOMSlave.Price).ToString(),
        //                 Body7 =((mktCbs.RequiredQuantity * mktCbs.Price)).ToString(),// (Raw_Item.ID == 1045 ? yarnVal * Mkt_BOMSlave.Price : (Raw_Item.ID == 5331 ? lycraVal * Mkt_BOMSlave.Price : Mkt_BOMSlave.RequiredQuantity * Mkt_BOMSlave.Price)).ToString(),


        //                 Body8 = mktCbs.IsFabric.ToString(),
        //                 Body9 = ((mktCbs.RequiredQuantity)).ToString(),// (Raw_Item.ID == 1045 ? yarnVal :(Raw_Item.ID == 5331 ? lycraVal: Mkt_BOMSlave.RequiredQuantity)).ToString(),
        //                 Body10 = Common_Currency.Symbol,
        //                 SubBody8 = (((((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice) * Common_TheOrder.Commission)) / 100).ToString(),
        //                 Body11 =  Common_TheOrder.Commission.ToString() + "% ",
        //                 Body12 = t2.Name,
        //                 int1 = t2.ID
        //             }).OrderBy(x => x.int1).ToList();

        //    decimal GroupTTL = 0;
        //    decimal TTL = 0;
        //    decimal TTLFabrics = 0;
        //    decimal TTLAccount = 0;
        //    decimal TTLCM = 0;
        //    decimal TTLFOB = 0;
        //    decimal CMperDZ = 0;
        //    decimal TTLQDZ = 0;
        //    decimal Comm = 0;


        //    foreach (ReportDocType r in a)
        //    {

        //        r.Body9 = r.Body9 + " " + r.Body4;
        //        decimal body7 = 0;
        //        decimal.TryParse(r.Body7, out body7);
        //        TTL += body7;

        //        if (r.Body8 == "True")
        //        {
        //            decimal subbody2 = 0;
        //            decimal.TryParse(r.Body7, out subbody2);
        //            TTLFabrics += subbody2;
        //        }
        //        else
        //        {

        //            decimal subbody2 = 0;
        //            decimal.TryParse(r.Body7, out subbody2);
        //            TTLAccount += subbody2;

        //        }
        //        decimal temp = 0;
        //        decimal.TryParse(r.Body3, out temp);
        //        r.Body3 = temp.ToString("F");
        //        decimal.TryParse(r.Body5, out temp);
        //        r.Body5 = r.Body10 + temp.ToString("F");

        //        decimal subbody3 = 0;
        //        decimal.TryParse(r.HeaderRight6, out subbody3);
        //        TTLFOB = subbody3;
        //        decimal subbody4 = 0;
        //        decimal.TryParse(r.HeaderRight5, out subbody4);
        //        TTLQDZ = subbody4;
        //        //TTLAccount = TTL - TTLFabrics;
        //        TTLCM = TTLFOB - (TTL + Comm);
        //        CMperDZ = TTLCM / TTLQDZ;
        //        decimal SubBody8 = 0;
        //        decimal.TryParse(r.SubBody8, out SubBody8);
        //        Comm = SubBody8;

        //    }
        //    if (a.Count != 0)
        //    {
        //        string tempBody = a[0].Body12;


        //        decimal groupTotal = 0;
        //        int counter = 1;
        //        List<string> groupList = new List<string>();
        //        foreach (ReportDocType r in a)
        //        {
        //            r.SubBody1 =  r.Body10 + TTL.ToString("F");
        //            r.SubBody2 =  r.Body10 + TTLFabrics.ToString("F");
        //            r.SubBody3 =  r.Body10 + TTLFOB.ToString("F");
        //            r.SubBody4 =  TTLQDZ.ToString("F");
        //            r.SubBody5 =  r.Body10 + TTLAccount.ToString("F");
        //            r.SubBody6 =  r.Body10 + TTLCM.ToString("F");
        //            r.SubBody7 =  r.Body10 + CMperDZ.ToString("F");
        //            r.SubBody8 ="("+r.Body11 +")" + r.Body10 + Comm.ToString("F");
        //            decimal temp = 0;

        //            decimal.TryParse(r.Body7, out temp);
        //            r.Body7 = r.Body10 + temp.ToString("F");

        //            r.HeaderLeft2 = ": " + r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
        //            r.HeaderLeft3 = ": " + r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
        //            r.HeaderMiddle3 = ": " + r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);

        //            if (tempBody == r.Body12)
        //            {
        //                decimal tempValue = 0;
        //                string ddd = r.Body7.Substring(0, r.Body7.Length - 1);
        //                decimal.TryParse(r.Body7.Substring(1, r.Body7.Length - 1), out tempValue);
        //                groupTotal += tempValue;
        //            }

        //            else
        //            {

        //                groupList.Add(tempBody + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")");
        //                decimal tempValue = 0;
        //                string ddd = r.Body7.Substring(0, r.Body7.Length - 1);
        //                decimal.TryParse(r.Body7.Substring(1, r.Body7.Length - 1), out tempValue);
        //                groupTotal = tempValue;


        //            }
        //            if (counter == a.Count())
        //            {
        //                //tempBody = r.Body12 + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")";
        //                groupList.Add(r.Body12 + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")");
        //            }
        //            tempBody = r.Body12;

        //            counter++;
        //        }
        //        string tempGroupTotal = "";
        //        foreach (string st in groupList)
        //        {
        //            tempGroupTotal += st + "\n";

        //        }
        //        foreach (string st in groupList)
        //        {
        //            foreach (ReportDocType r in a)
        //            {
        //                r.Body13 = tempGroupTotal;
        //                if (st.Contains(r.Body12))
        //                {
        //                    r.Body12 = st;
        //                }
        //            }
        //        }
        //    }
        //    this.BOMReportDoc = a.ToList();
        //}


        internal void BOMReportDocLoad(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();

            var a = (from Mkt_BOMSlave in db.Mkt_BOMSlave
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID

                     join Common_Unit in db.Common_Unit
                     on Mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join User_User in db.User_User on Common_TheOrder.FirstCreatedBy equals User_User.ID
                     where Mkt_BOM.ID == iD && Mkt_BOMSlave.Active == true

                     select new ReportDocType
                     {
                         HeaderLeft1 = ": " + Common_TheOrder.CID + "/" + Mkt_BOM.Style,
                         HeaderLeft2 = Mkt_BOM.FirstMove.ToString(),
                         HeaderLeft3 = Mkt_BOM.UpdateDate.ToString(),
                         HeaderLeft4 = ": " + Mkt_BOM.Reference.ToString(),
                         HeaderLeft5 = ": " + Mkt_BOM.Class.ToString(),
                         HeaderLeft6 = ": " + Mkt_BOM.Fabrication,
                         HeaderLeft7 = ": " + Mkt_BOM.SizeRange,
                         HeaderLeft8 = ": " + Mkt_BOM.Style,
                         HeaderMiddle1 = ": " + Mkt_Buyer.Name,
                         HeaderMiddle2 = ": " + Common_TheOrder.BuyerPO,
                         HeaderMiddle3 = Common_TheOrder.OrderDate.ToString(),
                         HeaderMiddle4 = ": " + Common_TheOrder.Season,
                         HeaderMiddle5 = ": " + Mkt_Item.Name,
                         HeaderRight1 = ": " + Mkt_BOM.QPack.ToString(),
                         HeaderRight2 = ": " + Mkt_BOM.Quantity.ToString(),
                         HeaderRight3 = ": " + Mkt_BOM.UnitPrice.ToString(),
                         HeaderRight4 = ": " + ((Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString()),
                         HeaderRight5 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
                         HeaderRight6 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
                         HeaderRight7 = ": " + User_User.Name,
                         Body1 = Raw_Item.Name,
                         Body2 = Mkt_BOMSlave.Description,
                         Body3 = Mkt_BOMSlave.Consumption.ToString(),
                         Body4 = Common_Unit.Name,
                         Body5 = Mkt_BOMSlave.Price.ToString(),
                         Body6 = Common_Supplier.Name,
                         //Body7 = ((Mkt_BOMSlave.Consumption * (Mkt_BOM.QPack * Mkt_BOM.Quantity)) * Mkt_BOMSlave.Price).ToString(),
                         Body7 = (Mkt_BOMSlave.RequiredQuantity * Mkt_BOMSlave.Price).ToString(),
                         Body8 = Mkt_BOMSlave.IsFabric.ToString(),
                         Body9 = Mkt_BOMSlave.RequiredQuantity.ToString(),
                         Body10 = Common_Currency.Symbol,
                         SubBody8 = (((((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice) * Common_TheOrder.Commission)) / 100).ToString(),
                         Body11 = Common_TheOrder.Commission.ToString() + "% ",
                         Body12 = t2.Name,
                         int1 = t2.ID
                     }).OrderByDescending(x => x.int1).ToList();

            decimal GroupTTL = 0;
            decimal TTL = 0;
            decimal TTLFabrics = 0;
            decimal TTLAccount = 0;
            decimal TTLCM = 0;
            decimal TTLFOB = 0;
            decimal CMperDZ = 0;
            decimal TTLQDZ = 0;
            decimal Comm = 0;


            foreach (ReportDocType r in a)
            {


                decimal body7 = 0;
                decimal.TryParse(r.Body7, out body7);
                TTL += body7;

                if (r.Body8 == "True")
                {
                    decimal subbody2 = 0;
                    decimal.TryParse(r.Body7, out subbody2);
                    TTLFabrics += subbody2;
                }
                else
                {

                    decimal subbody2 = 0;
                    decimal.TryParse(r.Body7, out subbody2);
                    TTLAccount += subbody2;

                }
                decimal temp = 0;
                decimal.TryParse(r.Body3, out temp);
                r.Body3 = temp.ToString("F");
                decimal.TryParse(r.Body5, out temp);
                r.Body5 = r.Body10 + temp.ToString("##.0000");

                decimal subbody3 = 0;
                decimal.TryParse(r.HeaderRight6, out subbody3);
                TTLFOB = subbody3;
                decimal subbody4 = 0;
                decimal.TryParse(r.HeaderRight5, out subbody4);
                TTLQDZ = subbody4;
                //TTLAccount = TTL - TTLFabrics;
                decimal SubBody8 = 0;
                decimal.TryParse(r.SubBody8, out SubBody8);
                Comm = SubBody8;

                TTLCM = TTLFOB - (TTL + Comm);
                if (TTLCM != 0 && TTLQDZ != 0)
                {
                    CMperDZ = TTLCM / TTLQDZ;
                }



            }
            if (a.Count != 0)
            {
                string tempBody = a[0].Body12;


                decimal groupTotal = 0;
                int counter = 1;
                List<string> groupList = new List<string>();
                foreach (ReportDocType r in a)
                {
                    r.SubBody1 = r.Body10 + TTL.ToString("F");
                    r.SubBody2 = r.Body10 + TTLFabrics.ToString("F");
                    r.SubBody3 = r.Body10 + TTLFOB.ToString("F");
                    r.SubBody4 = TTLQDZ.ToString("F");
                    r.SubBody5 = r.Body10 + TTLAccount.ToString("F");
                    r.SubBody6 = r.Body10 + TTLCM.ToString("F");
                    r.SubBody7 = r.Body10 + CMperDZ.ToString("F");
                    r.SubBody8 = "(" + r.Body11 + ")" + r.Body10 + Comm.ToString("F");
                    r.Body9 = r.Body9 + " " + r.Body4;
                    decimal temp = 0;

                    decimal.TryParse(r.Body7, out temp);
                    r.Body7 = r.Body10 + temp.ToString("F");

                    r.HeaderLeft2 = ": " + r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
                    r.HeaderLeft3 = ": " + r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
                    r.HeaderMiddle3 = ": " + r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);

                    if (tempBody == r.Body12)
                    {
                        decimal tempValue = 0;
                        string ddd = r.Body7.Substring(0, r.Body7.Length - 1);
                        decimal.TryParse(r.Body7.Substring(1, r.Body7.Length - 1), out tempValue);
                        groupTotal += tempValue;
                    }

                    else
                    {

                        groupList.Add(tempBody + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")");
                        decimal tempValue = 0;
                        string ddd = r.Body7.Substring(0, r.Body7.Length - 1);
                        decimal.TryParse(r.Body7.Substring(1, r.Body7.Length - 1), out tempValue);
                        groupTotal = tempValue;


                    }
                    if (counter == a.Count())
                    {
                        //tempBody = r.Body12 + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")";
                        groupList.Add(r.Body12 + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")");
                    }
                    tempBody = r.Body12;

                    counter++;
                }
                string tempGroupTotal = "";
                foreach (string st in groupList)
                {
                    tempGroupTotal += st;

                }
                foreach (string st in groupList)
                {
                    foreach (ReportDocType r in a)
                    {
                        r.Body13 = tempGroupTotal;
                        if (st.Contains(r.Body12))
                        {
                            r.Body12 = st;
                        }
                    }
                }
            }
            this.BOMReportDoc = a.ToList();
        }

        public bool EditUpdateDate(int userID, int bomID)
        {
            //VmMkt_BOM x = new VmMkt_BOM();
            this.SelectSingle(bomID.ToString());
            this.Mkt_BOM.UpdateDate = DateTime.Now;
            return this.Mkt_BOM.Edit(userID);
        }

        public bool BOMAuthorization(int userID)
        {

            return Mkt_BOM.Edit(userID);
        }


        internal void YReqGmCvcPcDocLoad(int id)
        {

            db = new xOssContext();

            var a = (from Mkt_BOMSlave in db.Mkt_BOMSlave
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID

                     join Common_Unit in db.Common_Unit
                     on Mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     //join Common_Supplier in db.Common_Supplier
                     //on Mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID

                     where Mkt_BOM.ID == id && Mkt_BOMSlave.Active == true
                     && Raw_Item.Raw_SubCategoryFK == 2023
                     //join User_User in db.User_User on Common_TheOrder.FirstCreatedBy equals User_User.ID
                     select new ReportDocType
                     {

                         Body1 = Raw_Item.Name,
                         Body2 = Mkt_BOMSlave.Description,
                         Body3 = Mkt_BOMSlave.Consumption.ToString(),

                         Body5 = Mkt_BOMSlave.Price.ToString(),

                         Body9 = Mkt_BOMSlave.RequiredQuantity.ToString()

                     }).ToList();

            this.BOMReportDoc = a.ToList();
        }

        //----------------------BOM Cotton Calculation----------------------//

        [DefaultValue(false)]
        public bool ISCount { get; set; }
        public decimal? TotalCottonItem { get; set; }
        public decimal? CottonItem { get; set; }

        public void GetYarnStatementLedger()
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_YarnCalculation
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                     join t4 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t4.ID
                     where t1.Active == true && t2.Active == true && t4.ID == 2024

                     select new ReportDocType
                     {
                         int1 = t1.ID,
                         int2 = t4.ID,
                         // Body1 = t1.CID,
                         Body2 = t4.Name,
                         Body3 = t3.Name,
                     }).ToList();
            this.BOMReportDoc = v;

        }
        #region MyRegion
        private List<int?> GetLCContainSupplierPO()
        {
            db = new xOssContext();
            List<int?> x = new List<int?>();
            using (var ctx = new xOssContext())
            {
                var a = ctx.Commercial_B2bLcSupplierPO.SqlQuery("EXEC dbo.GetLCContainSupplierPO").ToList();
                foreach (var l in a)
                {
                    x.Add(l.Mkt_POFK);
                }
            }
            return x;
        }
        public DataTable GetOpenedLCForYarn(DateTime fromDate, DateTime toDate)
        {
            db = new xOssContext();

            // var poInLc = GetLCContainSupplierPO();


            DataTable dt = new DataTable();
            // var data = db.Mkt_YarnCalculation.Where(x => x.Active == true).ToList();

            var v = (from t1 in db.Commercial_B2bLcSupplierPOSlave
                     join t2 in db.Commercial_B2bLcSupplierPO on t1.Commercial_B2bLcSupplierPOFK equals t2.ID
                     join t3 in db.Commercial_B2bLC on t2.Commercial_B2bLCFK equals t3.ID
                     join t6 in db.Common_Supplier on t3.Common_SupplierFK equals t6.ID
                     join t10 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t10.ID
                     join t9 in db.Raw_Item on t10.Raw_ItemFK equals t9.ID
                     join t4 in db.Mkt_PO on t10.Mkt_POFK equals t4.ID
                     join t5 in db.Mkt_BOM on t4.Mkt_BOMFK equals t5.ID
                     join t8 in db.Common_TheOrder on t5.Common_TheOrderFk equals t8.ID
                     where t1.Active == true && t2.Active == true &&
                     t3.Active == true && t4.Active == true &&
                     t8.Active == true &&
                     t9.Active == true && t10.Active == true
                     //&& poInLc.Contains(t4.ID)
                     group new { t1, t2, t3, t5, t4, t8, t9, t10, t6 } by new { CID = t8.CID, OrderDate = t8.OrderDate, LCName = t3.Name, SupplierName = t6.Name }
                     into Group
                     where Group.Count() > 0
                     select new
                     {
                         CID = Group.Key.CID + ", " + Group.Key.LCName + ", " + Group.Key.SupplierName,
                         OrderDate = Group.Key.OrderDate,
                         ColumnName = Group.GroupBy(x => x.t9.Name).Select
                         (m => new
                         {
                             YarnCount = m.Key,
                             RequiredQuantity = m.Sum(x => (x.t1.TotalRequired))// m.Sum(x => (((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) - ((((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) / 100) * x.t1.Lycra),
                             //Lycra = m.Sum(x => (((((x.t7.QntyPcs / 12) * x.t7.Consumption) / x.t7.ProcessLoss) / 100) * x.t7.Lycra))
                         })
                     }).ToList();



            var sub = (from t1 in db.Mkt_YarnCalculation
                       join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                       join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                       join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                       where t1.Active == true && t2.Active == true// && t1.Mkt_BOMFK == id
                       //&& !poInLc.Contains(t4.ID)
                       select new
                       {
                           YarnCount = t4.Name
                       }).Distinct().ToList();

            ArrayList objDataColumn = new ArrayList();

            if (v.Count > 0)
            {
                objDataColumn.Add("Buyer & PO");
                for (int i = 0; i < sub.Count; i++)
                {
                    objDataColumn.Add(sub[i].YarnCount);
                }

            }

            //Add column name dynamically
            for (int i = 0; i < objDataColumn.Count; i++)
            {
                dt.Columns.Add(objDataColumn[i].ToString());
            }


            var previuosBalance = (from t in v
                                   where t.OrderDate < fromDate
                                   select new
                                   {
                                       ColumnName = t.ColumnName,
                                       LCDate = t.OrderDate,
                                       CID = t.CID
                                   }).Distinct().ToList();


            var currentYarn = (from t in v
                               where t.OrderDate >= fromDate && t.OrderDate <= toDate
                               select new
                               {
                                   ColumnName = t.ColumnName,
                                   LCDate = t.OrderDate,
                                   CID = t.CID
                               }).Distinct().ToList();
            decimal groupTotal = 0;
            int counter = 1;
            List<string> groupList = new List<string>();
            decimal prelycraValue = 0;
            List<string> predatalist = new List<string>();
            #region Previous Yarn

            if (previuosBalance.Count != 0)
            {
                var preRes = previuosBalance[0].ColumnName.ToList();
                predatalist.Add("Opening Balance");

                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;
                    string column = dt.Columns[m].ToString();
                    for (int j = 0; j < preRes.Count; j++)
                    {
                        if (preRes[j].YarnCount == column)
                        {
                            var preReq = PreviousYarnValance(preRes[j].YarnCount, fromDate);//preRes[j].RequiredQuantity;
                                                                                            //prelycraValue += PreviousLycraValance(preRes[j].YarnCount, fromDate);

                            predatalist.Add(preReq.ToString("F"));//preRes[j].RequiredQuantity.ToString("F"));
                            count++;
                        }
                    }

                    if (count == 0) // && column != "Lycra"
                    {
                        string val = "0";
                        predatalist.Add(val);
                    }
                }
            }



            //if (!dt.Columns.Contains("Lycra"))
            //{
            //    dt.Columns.Add("Lycra");
            //}

            //predatalist.Add(prelycraValue.ToString("F"));

            dt.Rows.Add(predatalist.ToArray<string>());

            #endregion

            for (int i = 0; i < currentYarn.Count; i++)
            {

                List<string> datalist = new List<string>();



                #region Current Yarn 
                var res = currentYarn[i].ColumnName.ToList();
                datalist.Add(currentYarn[i].CID.ToString());
                //decimal lycraValue = 0;
                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;
                    string column = dt.Columns[m].ToString();
                    for (int j = 0; j < res.Count; j++)
                    {
                        if (res[j].YarnCount == column)
                        {

                            var requiredQuantity = res[j].RequiredQuantity == null ? 0 : res[j].RequiredQuantity.Value;


                            datalist.Add(requiredQuantity.ToString());
                            count++;
                        }
                    }

                    if (count == 0) // && column != "Lycra"
                    {
                        string val = "0";
                        datalist.Add(val);
                    }
                }
                //if (!dt.Columns.Contains("Lycra"))
                //{
                //    dt.Columns.Add("Lycra");
                //}


                //datalist.Add(lycraValue.ToString("F"));

                dt.Rows.Add(datalist.ToArray<string>());
                #endregion



            }
            return dt;




        }
        public DataTable GetYarnRequirement(DateTime fromDate, DateTime toDate)
        {
            db = new xOssContext();

            ///var poInLc = GetLCContainSupplierPO();


            DataTable dt = new DataTable();
            //var data = db.Mkt_YarnCalculation.Distinct().Where(x => x.Active == true).ToList();
            var v = (from t1 in db.Mkt_YarnCalculation
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                     where t1.Active == true && t2.Active == true// && t1.Mkt_BOMFK == id
                     //&& !poInLc.Contains(t4.ID)
                     group new
                     {
                         t1,
                         t2,
                         t3,
                         t4                                //, t6, t7, t8 

                     } by new { t3.CID, t3.OrderDate }
                     into Group
                     where Group.Count() > 0
                     select new
                     {
                         Group.Key.CID,
                         OrderDate = Group.Key.OrderDate,
                         ColumnName = Group.GroupBy(x => x.t4.Name).Select
                         (m => new
                         {
                             YarnCount = m.Key,
                             OrderDate = m.Select(x => x.t3.OrderDate),
                             RequiredQuantity = m.Sum(x => (((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) - ((((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) / 100) * x.t1.Lycra),
                             Lycra = m.Sum(x => (((((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) / 100) * x.t1.Lycra)),

                         })
                     }).ToList();

            var sub = (from t1 in db.Mkt_YarnCalculation
                       join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                       join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                       join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                       where t1.Active == true && t2.Active == true// && t1.Mkt_BOMFK == id
                       //&& !poInLc.Contains(t4.ID)
                       select new
                       {
                           YarnCount = t4.Name
                       }).Distinct().ToList();

            ArrayList objDataColumn = new ArrayList();

            if (v.Count() > 0)
            {
                objDataColumn.Add("Buyer & PO");
                for (int i = 0; i < sub.Count; i++)
                {
                    objDataColumn.Add(sub[i].YarnCount);
                }

            }

            //Add column name dynamically
            for (int i = 0; i < objDataColumn.Count; i++)
            {
                dt.Columns.Add(objDataColumn[i].ToString());
            }



            var previuosBalance = (from t in v
                                   where t.OrderDate < fromDate
                                   select new
                                   {
                                       ColumnName = t.ColumnName,
                                       OrderDate = t.OrderDate,
                                       CID = t.CID
                                   }).Distinct().ToList();


            var currentYarn = (from t in v
                               where t.OrderDate >= fromDate && t.OrderDate <= toDate
                               select new
                               {
                                   ColumnName = t.ColumnName,
                                   OrderDate = t.OrderDate,
                                   CID = t.CID
                               }).Distinct().ToList();
            //Add data into datatable
            decimal groupTotal = 0;
            int counter = 1;
            List<string> groupList = new List<string>();
            decimal prelycraValue = 0;
            List<string> predatalist = new List<string>();

            #region Previous Yarn

            if (previuosBalance.Count != 0)
            {
                var preRes = previuosBalance[0].ColumnName.ToList();
                predatalist.Add("Opening Balance");


                for (int m = 1; m < dt.Columns.Count - 1; m++)

                {
                    int count = 0;
                    string column = dt.Columns[m].ToString();
                    //for (int j = 0; j < preRes.Count; j++)
                    // {
                    // if (preRes[j].YarnCount == column)
                    //{
                    var preReq = PreviousYarnValance(column, fromDate);//preRes[j].RequiredQuantity;
                    prelycraValue += PreviousLycraValance(column, fromDate);

                    predatalist.Add(preReq.ToString("F"));//preRes[j].RequiredQuantity.ToString("F"));
                    count++;
                    // }
                    // }

                    if (count == 0 && column != "Lycra")
                    {
                        string val = "0";
                        predatalist.Add(val);
                    }
                    if (!dt.Columns.Contains("Lycra"))
                    {
                        dt.Columns.Add("Lycra");
                    }


                }
                predatalist.Add(prelycraValue.ToString());

                int i = 0;

                dt.Rows.Add(predatalist.ToArray<string>());
            }







            #endregion


            for (int i = 0; i < currentYarn.Count; i++)
            {

                List<string> datalist = new List<string>();



                #region Current Yarn 
                var res = currentYarn[i].ColumnName.ToList();
                datalist.Add(currentYarn[i].CID.ToString());
                decimal lycraValue = 0;
                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;
                    string column = dt.Columns[m].ToString();
                    for (int j = 0; j < res.Count; j++)
                    {
                        if (res[j].YarnCount == column)
                        {
                            lycraValue += Convert.ToDecimal(res[j].Lycra.ToString("F"));

                            datalist.Add(res[j].RequiredQuantity.ToString("F"));
                            count++;
                        }
                    }

                    if (count == 0 && column != "Lycra")
                    {
                        string val = "0";
                        datalist.Add(val);
                    }
                }
                if (!dt.Columns.Contains("Lycra"))
                {
                    dt.Columns.Add("Lycra");
                }


                datalist.Add(lycraValue.ToString("F"));

                dt.Rows.Add(datalist.ToArray<string>());
                #endregion

            }

            return dt;




        }


        public DataTable GetYarnSummery(DateTime fromDate, DateTime toDate)
        {
            db = new xOssContext();

            ///var poInLc = GetLCContainSupplierPO();


            DataTable dt = new DataTable();
            //var data = db.Mkt_YarnCalculation.Distinct().Where(x => x.Active == true).ToList();
            var v = (from t1 in db.Mkt_YarnCalculation
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                     where t1.Active == true && t2.Active == true// && t1.Mkt_BOMFK == id
                     //&& !poInLc.Contains(t4.ID)
                     group new
                     {
                         t1,
                         t2,
                         t3,
                         t4                                //, t6, t7, t8 

                     } by new { t3.CID, t3.OrderDate }
                     into Group
                     where Group.Count() > 0
                     select new
                     {
                         Group.Key.CID,
                         OrderDate = Group.Key.OrderDate,
                         ColumnName = Group.GroupBy(x => x.t4.Name).Select
                         (m => new
                         {
                             YarnCount = m.Key,
                             OrderDate = m.Select(x => x.t3.OrderDate),
                             RequiredQuantity = m.Sum(x => (((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) - ((((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) / 100) * x.t1.Lycra),
                             Lycra = m.Sum(x => (((((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) / 100) * x.t1.Lycra)),

                         })
                     }).ToList();

            var sub = (from t1 in db.Mkt_YarnCalculation
                       join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                       join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                       join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                       where t1.Active == true && t2.Active == true// && t1.Mkt_BOMFK == id
                       //&& !poInLc.Contains(t4.ID)
                       select new
                       {
                           YarnCount = t4.Name
                       }).Distinct().ToList();

            ArrayList objDataColumn = new ArrayList();

            if (v.Count() > 0)
            {
                objDataColumn.Add("Buyer & PO");
                for (int i = 0; i < sub.Count; i++)
                {
                    objDataColumn.Add(sub[i].YarnCount);
                }

            }

            //Add column name dynamically
            for (int i = 0; i < objDataColumn.Count; i++)
            {
                dt.Columns.Add(objDataColumn[i].ToString());
            }



            var previuosBalance = (from t in v
                                   where t.OrderDate < fromDate
                                   select new
                                   {
                                       ColumnName = t.ColumnName,
                                       OrderDate = t.OrderDate,
                                       CID = t.CID
                                   }).Distinct().ToList();


            var currentYarn = (from t in v
                               where t.OrderDate >= fromDate && t.OrderDate <= toDate
                               select new
                               {
                                   ColumnName = t.ColumnName,
                                   OrderDate = t.OrderDate,
                                   CID = t.CID
                               }).Distinct().ToList();
            //Add data into datatable
            decimal groupTotal = 0;
            int counter = 1;
            List<string> groupList = new List<string>();
            decimal prelycraValue = 0;

            List<string> predatalist = new List<string>();
            #region Previous Yarn

            if (previuosBalance.Count != 0)
            {
                var preRes = previuosBalance[0].ColumnName.ToList();
                predatalist.Add("Opening Balance");

                // var row = dt.Rows[0];


                int count = 0;

                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    var preReq = PreviousYarnValance(dt.Columns[j].ToString(), fromDate);//preRes[j].RequiredQuantity;
                    prelycraValue += PreviousLycraValance(dt.Columns[j].ToString(), fromDate);

                    predatalist.Add(preReq.ToString());//preRes[j].RequiredQuantity.ToString("F"));
                    count++;

                }

                if (count == 0 && dt.Columns[count].ToString() != "Lycra")
                {
                    string val = "0";
                    predatalist.Add(val);
                }

            }



            if (!dt.Columns.Contains("Lycra"))
            {
                dt.Columns.Add("Lycra");
            }

            predatalist.Add(prelycraValue.ToString());

            dt.Rows.Add(predatalist.ToArray<string>());

            #endregion
            for (int i = 0; i < currentYarn.Count; i++)
            {

                List<string> datalist = new List<string>();



                #region Current Yarn 
                var res = currentYarn[i].ColumnName.ToList();
                datalist.Add("Current Value");//currentYarn[i].CID.ToString());
                decimal lycraValue = 0;
                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;
                    string column = dt.Columns[m].ToString();
                    for (int j = 0; j < res.Count; j++)
                    {
                        if (res[j].YarnCount == column)
                        {
                            lycraValue += Convert.ToDecimal(res[j].Lycra.ToString("F"));
                            var curr = CurrentYarnValance(res[j].YarnCount, fromDate, toDate);
                            datalist.Add(curr.ToString("F"));
                            count++;
                        }
                    }

                    if (count == 0 && column != "Lycra")
                    {
                        string val = "0";
                        datalist.Add(val);
                    }
                }
                if (!dt.Columns.Contains("Lycra"))
                {
                    dt.Columns.Add("Lycra");
                }


                datalist.Add(lycraValue.ToString());

                dt.Rows.Add(datalist.ToArray<string>());
                #endregion

            }

            return dt;




        }

        private decimal PreviousYarnValance(string yarnCount, DateTime fromDate)
        {
            var preRequiredQuantity = (from t1 in db.Mkt_YarnCalculation
                                       join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                                       join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                                       join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                                       where t1.Active == true && t2.Active == true
                                       where t3.OrderDate < fromDate && t4.Name == yarnCount
                                       select

                                           (((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) - ((((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) / 100) * t1.Lycra
                       ).DefaultIfEmpty(0).Sum();

            return preRequiredQuantity;
        }


        private decimal PreviousYarnValanceLcOpen(string yarnCount, DateTime fromDate)
        {
            var preRequiredQuantity = (from t1 in db.Commercial_B2bLcSupplierPOSlave
                                       join t2 in db.Commercial_B2bLcSupplierPO on t1.Commercial_B2bLcSupplierPOFK equals t2.ID
                                       join t3 in db.Commercial_B2bLC on t2.Commercial_B2bLCFK equals t3.ID
                                       join t4 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t4.ID
                                       join t5 in db.Mkt_BOMSlave on t4.Mkt_BOMSlaveFK equals t5.ID
                                       join t6 in db.Mkt_YarnCalculation on t5.Mkt_YarnCalculationFK equals t6.ID
                                       join t7 in db.Raw_Item on t6.Raw_ItemFK equals t7.ID
                                       join t8 in db.Common_Supplier on t3.Common_SupplierFK equals t8.ID
                                       join t9 in db.Mkt_PO on t4.Mkt_POFK equals t9.ID
                                       join t10 in db.Mkt_BOM on t4.Mkt_POFK equals t10.ID
                                       join t11 in db.Common_TheOrder on t10.Common_TheOrderFk equals t11.ID
                                       where t1.Active == true && t2.Active == true &&
                                       t3.Active == true && t4.Active == true &&
                                       t5.Active == true &&
                                       t6.Active == true && t7.Active == true && t8.Active == true && t9.Active == true && t10.Active == true && t11.Active == true
                                       where t11.OrderDate < fromDate && t7.Name == yarnCount
                                       select
                                       t1.TotalRequired.Value
                       //(((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) - ((((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) / 100) * t1.Lycra
                       ).DefaultIfEmpty(0).Sum();

            return preRequiredQuantity;
        }

        private decimal CurrentYarnValance(string yarnCount, DateTime fromDate, DateTime toDate)
        {
            var preRequiredQuantity = (from t1 in db.Mkt_YarnCalculation
                                       join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                                       join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                                       join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                                       where t1.Active == true && t2.Active == true
                                       where t3.OrderDate >= fromDate && t3.OrderDate <= toDate && t4.Name == yarnCount
                                       select

                                           (((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) - ((((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) / 100) * t1.Lycra
                       ).DefaultIfEmpty(0).Sum();

            return preRequiredQuantity;
        }

        private decimal PreviousLycraValance(string yarnCount, DateTime fromDate)
        {
            var preRequiredQuantity = (from t1 in db.Mkt_YarnCalculation
                                       join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                                       join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                                       join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                                       where t1.Active == true && t2.Active == true
                                       where t3.OrderDate < fromDate && t4.Name == yarnCount
                                       select

                                           (((((t1.QntyPcs / 12) * t1.Consumption) / t1.ProcessLoss) / 100) * t1.Lycra)
                       ).DefaultIfEmpty(0).Sum();

            return preRequiredQuantity;
        }
        private decimal PreviousLycraValanceLcOpen(int lycraSubCategory, DateTime fromDate)
        {
            var preRequiredQuantity = (from t1 in db.Commercial_B2bLcSupplierPOSlave
                                       join t2 in db.Commercial_B2bLcSupplierPO on t1.Commercial_B2bLcSupplierPOFK equals t2.ID
                                       join t3 in db.Commercial_B2bLC on t2.Commercial_B2bLCFK equals t3.ID
                                       join t4 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t4.ID
                                       join t5 in db.Mkt_BOMSlave on t4.Mkt_BOMSlaveFK equals t5.ID
                                       join t6 in db.Mkt_YarnCalculation on t5.Mkt_YarnCalculationFK equals t6.ID
                                       join t7 in db.Raw_Item on t6.Raw_ItemFK equals t7.ID
                                       join t8 in db.Common_Supplier on t3.Common_SupplierFK equals t8.ID
                                       join t9 in db.Mkt_PO on t4.Mkt_POFK equals t9.ID
                                       join t10 in db.Mkt_BOM on t4.Mkt_POFK equals t10.ID
                                       join t11 in db.Common_TheOrder on t10.Common_TheOrderFk equals t11.ID
                                       join t12 in db.Raw_SubCategory on t7.Raw_SubCategoryFK equals t12.ID
                                       where t1.Active == true && t2.Active == true &&
                                       t3.Active == true && t4.Active == true &&
                                       t5.Active == true &&
                                       t6.Active == true && t7.Active == true && t8.Active == true && t9.Active == true && t10.Active == true && t11.Active == true
                                       where t11.OrderDate < fromDate && t12.ID == lycraSubCategory
                                       select
                                       t1.TotalRequired.Value

                       ).DefaultIfEmpty(0).Sum();
            return preRequiredQuantity;
        }

        private decimal CurrentLycraValanceLcOpen(int lycraSubCategory, DateTime fromDate, DateTime toDate)
        {
            var preRequiredQuantity = (from t1 in db.Commercial_B2bLcSupplierPOSlave
                                       join t2 in db.Commercial_B2bLcSupplierPO on t1.Commercial_B2bLcSupplierPOFK equals t2.ID
                                       join t3 in db.Commercial_B2bLC on t2.Commercial_B2bLCFK equals t3.ID
                                       join t4 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t4.ID
                                       join t5 in db.Mkt_BOMSlave on t4.Mkt_BOMSlaveFK equals t5.ID
                                       join t6 in db.Mkt_YarnCalculation on t5.Mkt_YarnCalculationFK equals t6.ID
                                       join t7 in db.Raw_Item on t6.Raw_ItemFK equals t7.ID
                                       join t8 in db.Common_Supplier on t3.Common_SupplierFK equals t8.ID
                                       join t9 in db.Mkt_PO on t4.Mkt_POFK equals t9.ID
                                       join t10 in db.Mkt_BOM on t4.Mkt_POFK equals t10.ID
                                       join t11 in db.Common_TheOrder on t10.Common_TheOrderFk equals t11.ID
                                       join t12 in db.Raw_SubCategory on t7.Raw_SubCategoryFK equals t12.ID
                                       where t1.Active == true && t2.Active == true &&
                                       t3.Active == true && t4.Active == true &&
                                       t5.Active == true &&
                                       t6.Active == true && t7.Active == true && t8.Active == true && t9.Active == true && t10.Active == true && t11.Active == true
                                       where t11.OrderDate >= fromDate && t11.OrderDate <= toDate && t12.ID == lycraSubCategory
                                       select
                                       t1.TotalRequired.Value

                       ).DefaultIfEmpty(0).Sum();
            return preRequiredQuantity;
        }
        #endregion


        internal void YarnCalculationByStyle(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();

            var v = (from Mkt_YarnCalculation in db.Mkt_YarnCalculation
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_YarnCalculation.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join User_User in db.User_User
                     on Mkt_BOM.FirstCreatedBy equals User_User.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_YarnCalculation.Raw_ItemFK equals Raw_Item.ID
                     join Raw_SubCategory in db.Raw_SubCategory
                      on Raw_Item.Raw_SubCategoryFK equals Raw_SubCategory.ID
                     join t8 in db.Common_Currency
                     on Mkt_YarnCalculation.Common_CurrencyFK equals t8.ID
                     join t9 in db.Common_Unit
                     on Mkt_YarnCalculation.Common_UnitFK equals t9.ID
                     where Mkt_YarnCalculation.Mkt_BOMFK == id && Mkt_YarnCalculation.Active == true


                     select new ReportDocType
                     {
                         HeaderLeft1 = ": " + Common_TheOrder.CID + "/" + Mkt_BOM.Style,
                         HeaderLeft2 = Mkt_BOM.FirstMove.ToString(),
                         HeaderLeft3 = Mkt_BOM.UpdateDate.ToString(),
                         HeaderLeft4 = ": " + Mkt_BOM.Reference.ToString(),
                         HeaderLeft5 = ": " + Mkt_BOM.Class.ToString(),
                         HeaderLeft6 = ": " + Mkt_BOM.Fabrication,
                         HeaderLeft7 = ": " + Mkt_BOM.SizeRange,
                         HeaderLeft8 = ": " + Mkt_BOM.Style,
                         HeaderMiddle2 = ": " + Common_TheOrder.BuyerPO,
                         HeaderMiddle3 = ": " + Common_TheOrder.OrderDate.ToString(),
                         HeaderMiddle4 = ": " + Common_TheOrder.Season,
                         HeaderMiddle5 = ": " + Mkt_Item.Name,
                         HeaderMiddle6 = Mkt_YarnCalculation.Lycra.ToString() + "% Lycra",
                         HeaderRight1 = ": " + Mkt_BOM.QPack.ToString(),
                         HeaderRight2 = ": " + Mkt_BOM.Quantity.ToString(),
                         HeaderRight3 = ": " + Mkt_BOM.UnitPrice.ToString(),
                         HeaderRight4 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString()),
                         HeaderRight5 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
                         HeaderRight6 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
                         HeaderRight7 = ": " + User_User.Name,
                         Body1 = Mkt_YarnCalculation.ReferenceNo,
                         Body2 = Mkt_YarnCalculation.Color,
                         Body3 = Mkt_YarnCalculation.Combo,
                         Body4 = Raw_Item.Name.ToString(),
                         Body5 = Mkt_YarnCalculation.QntyPcs.ToString(),

                         Body6 = (((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption) / Mkt_YarnCalculation.ProcessLoss).ToString(),
                         Body8 = (((((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption) / Mkt_YarnCalculation.ProcessLoss) / 100) * Mkt_YarnCalculation.Lycra).ToString(),
                         Body7 = ((((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption) / Mkt_YarnCalculation.ProcessLoss)
                                  - ((((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption) / Mkt_YarnCalculation.ProcessLoss) / 100) * Mkt_YarnCalculation.Lycra).ToString(),


                         Body9 = Mkt_YarnCalculation.GSM.ToString(),
                         Body10 = Mkt_YarnCalculation.FinishDIA,
                         Body11 = Mkt_YarnCalculation.Fabrication,
                         Body12 = Mkt_YarnCalculation.Consumption.ToString(),
                         Body13 = Mkt_YarnCalculation.ProcessLoss.ToString(),
                         Body14 = Mkt_YarnCalculation.Lycra.ToString(),
                         Body15 = ((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption).ToString(),
                         Footer1 = (Mkt_YarnCalculation.QntyPcs / 12).ToString(),
                         Footer2 = Raw_Item.Name,
                         int1 = Raw_SubCategory.ID
                     }).OrderBy(x => x.Footer2).ToList();
            decimal YarnQty = 0;
            decimal TotalYarn = 0;
            decimal TotalLycra = 0;
            decimal body7 = 0;
            foreach (var x in v)
            {
                decimal footer1 = Convert.ToDecimal(x.Footer1);
                x.Footer1 = footer1.ToString("F");

                decimal body15 = Convert.ToDecimal(x.Body15);
                x.Body15 = body15.ToString("F");

                decimal bod7 = Convert.ToDecimal(x.Body7);
                x.Body7 = bod7.ToString("F");

                decimal body6 = Convert.ToDecimal(x.Body6);
                x.Body6 = body6.ToString("F");
                TotalYarn += body6;

                decimal body8 = Convert.ToDecimal(x.Body8);
                x.Body8 = body8.ToString("F");
                TotalLycra += body8;

                body7 = Convert.ToDecimal(x.Body7);
                YarnQty += body7;

                decimal headerRight6 = Convert.ToDecimal(x.HeaderRight6);
                x.HeaderRight6 = ": " + headerRight6.ToString("F");

                decimal headerRight5 = Convert.ToDecimal(x.HeaderRight5);
                x.HeaderRight5 = ": " + headerRight5.ToString("F");

                decimal headerRight4 = Convert.ToDecimal(x.HeaderRight4);
                x.HeaderRight4 = ": " + headerRight4.ToString("F");



            }

            if (v.Count != 0)
            {
                string tempBody = v[0].Footer2;


                decimal groupTotal = 0;
                int counter = 1;
                List<string> groupList = new List<string>();
                foreach (ReportDocType r in v)
                {
                    decimal temp = 0;
                    //body7 = Convert.ToDecimal(r.Body7);
                    //r.Body7 = body7.ToString("F");

                    r.SubBody1 = TotalYarn.ToString("F");
                    r.SubBody2 = TotalLycra.ToString("F");
                    r.SubBody3 = YarnQty.ToString("F");
                    decimal sevenBody = Convert.ToDecimal(r.Body7);


                    if (tempBody == r.Footer2)
                    {
                        decimal tempValue = 0;
                        groupTotal += sevenBody;
                    }

                    else
                    {
                        groupList.Add("<div><h4><u>Total " + tempBody + " = " + groupTotal.ToString("F") + "</u><br/>");
                        groupTotal = sevenBody;
                    }
                    if (counter == v.Count())
                    {
                        //tempBody = r.Body12 + " (Total = " + r.Body7.Substring(0, 1) + groupTotal.ToString() + ")";
                        groupList.Add("<div><h4><u>Total " + r.Footer2 + " = " + groupTotal.ToString("F") + "</u></h4><br/></div>");
                    }
                    tempBody = r.Footer2;
                    counter++;
                }
                string tempGroupTotal = "";
                foreach (string st in groupList)
                {
                    tempGroupTotal += st + "\n";
                }
                foreach (string st in groupList)
                {
                    foreach (ReportDocType r in v)
                    {
                        r.Footer3 = tempGroupTotal;
                        if (st.Contains(r.Footer2))
                        {
                            r.Footer2 = st;
                        }
                    }
                }
            }
            this.BOMReportDoc = v.ToList();
        }


        //public string AverageConsumption { get; set; }
        //public string TotalYarn { get; set; }
        //public string TotalLycra { get; set; }

        public int MyProperty { get; set; }
        internal string GetLycraValue(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var x = (from Mkt_YarnCalculation in db.Mkt_YarnCalculation
                     join Mkt_BOM in db.Mkt_BOM on Mkt_YarnCalculation.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Item in db.Mkt_Item on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join User_User in db.User_User on Mkt_BOM.FirstCreatedBy equals User_User.ID
                     join Raw_Item in db.Raw_Item on Mkt_YarnCalculation.Raw_ItemFK equals Raw_Item.ID
                     where Mkt_YarnCalculation.Mkt_BOMFK == id && Mkt_YarnCalculation.Active == true
                     select (((((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption) / Mkt_YarnCalculation.ProcessLoss) / 100) * Mkt_YarnCalculation.Lycra));

            decimal TotalLycra = 0;
            if (x.Count() != 0)
            {
                TotalLycra = x.Sum() / 2;
            }
            return TotalLycra.ToString("F");

        }



        internal string GetYarnValue(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var x = (from Mkt_YarnCalculation in db.Mkt_YarnCalculation
                     join Mkt_BOM in db.Mkt_BOM
                         on Mkt_YarnCalculation.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                         on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Item in db.Mkt_Item
                         on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join User_User in db.User_User
                         on Mkt_BOM.FirstCreatedBy equals User_User.ID
                     join Raw_Item in db.Raw_Item
                         on Mkt_YarnCalculation.Raw_ItemFK equals Raw_Item.ID
                     where Mkt_YarnCalculation.Mkt_BOMFK == id
                     select
                         ((((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption) / Mkt_YarnCalculation.ProcessLoss)
                          - (((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption) /
                             Mkt_YarnCalculation.ProcessLoss) / 100) * Mkt_YarnCalculation.Lycra);

            decimal TotalYarn = 0;
            if (x.Count() != 0)
            {
                TotalYarn = x.Sum();
            }
            return TotalYarn.ToString("F");
        }

        internal string GetAverageConsumption(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var TotalYarn = (from Mkt_YarnCalculation in db.Mkt_YarnCalculation
                             join Mkt_BOM in db.Mkt_BOM on Mkt_YarnCalculation.Mkt_BOMFK equals Mkt_BOM.ID
                             join Common_TheOrder in db.Common_TheOrder on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                             join Mkt_Item in db.Mkt_Item on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                             join User_User in db.User_User on Mkt_BOM.FirstCreatedBy equals User_User.ID
                             join Raw_Item in db.Raw_Item on Mkt_YarnCalculation.Raw_ItemFK equals Raw_Item.ID
                             where Mkt_YarnCalculation.Mkt_BOMFK == id
                             select Mkt_YarnCalculation.Consumption).ToList();


            int count = TotalYarn.Count();
            decimal consumption = 0;
            if (count != 0)
            {
                consumption = TotalYarn.Sum() / count;
            }
            return consumption.ToString("F");
        }


        public DataTable GetYarnStatementledgerExcel(DateTime fromDate, DateTime toDate)
        {
            db = new xOssContext();
            DataTable dt = new DataTable();
            DataTable dtLcOpen = new DataTable();

            #region Required Yarn
            var v = (from t1 in db.Mkt_YarnCalculation
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                     join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                     where t1.Active == true && t2.Active == true
                     group new
                     {
                         t1,
                         t2,
                         t3,
                         t4
                     }
                     by new { t3.CID, t3.OrderDate }
                    into Group
                     where Group.Count() > 0
                     select new
                     {
                         Group.Key.CID,
                         OrderDate = Group.Key.OrderDate,
                         ColumnName = Group.GroupBy(x => x.t4.Name).Select
                         (m => new
                         {
                             YarnCount = m.Key,
                             OrderDate = m.Select(x => x.t3.OrderDate),
                             RequiredQuantity = m.Sum(x => (((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) - ((((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) / 100) * x.t1.Lycra),
                             Lycra = m.Sum(x => (((((x.t1.QntyPcs / 12) * x.t1.Consumption) / x.t1.ProcessLoss) / 100) * x.t1.Lycra)),

                         })
                     }).ToList();

            var sub = (from t1 in db.Mkt_YarnCalculation
                       join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                       join t3 in db.Common_TheOrder on t2.Common_TheOrderFk equals t3.ID
                       join t4 in db.Raw_Item on t1.Raw_ItemFK equals t4.ID
                       where t1.Active == true && t2.Active == true// && t1.Mkt_BOMFK == id
                       //&& !poInLc.Contains(t4.ID)
                       select new
                       {
                           YarnCount = t4.Name
                       }).Distinct().ToList();

            ArrayList objDataColumn = new ArrayList();
            if (v.Count() > 0)
            {
                objDataColumn.Add("Buyer & PO");
                for (int i = 0; i < sub.Count; i++)
                {
                    objDataColumn.Add(sub[i].YarnCount);
                }

            }

            //Add column name dynamically
            for (int i = 0; i < objDataColumn.Count; i++)
            {
                dt.Columns.Add(objDataColumn[i].ToString());
            }
            if (!dt.Columns.Contains("Lycra"))
            {
                dt.Columns.Add("Lycra");
            }


            var previuosBalance = (from t in v
                                   where t.OrderDate < fromDate
                                   select new
                                   {
                                       ColumnName = t.ColumnName,
                                       OrderDate = t.OrderDate,
                                       CID = t.CID
                                   }).Distinct().ToList();


            var currentYarn = (from t in v
                               where t.OrderDate >= fromDate && t.OrderDate <= toDate
                               select new
                               {
                                   ColumnName = t.ColumnName,
                                   OrderDate = t.OrderDate,
                                   CID = t.CID
                               }).Distinct().ToList();
            //Add data into datatable

            decimal prelycraValue = 0;
            List<string> predatalist = new List<string>();

            #region Previous Yarn

            if (previuosBalance.Count != 0)
            {

                predatalist.Add("Opening Balance");

                for (int m = 1; m < dt.Columns.Count - 1; m++)
                {
                    int count = 0;
                    string column = dt.Columns[m].ToString();

                    var preReq = PreviousYarnValance(column, fromDate);
                    prelycraValue += PreviousLycraValance(column, fromDate);

                    predatalist.Add(preReq.ToString("F"));
                    count++;

                    //if (count == 0 && column != "Lycra")
                    //{
                    //    string val = "0";
                    //    predatalist.Add(val);
                    //}

                }
                predatalist.Add(prelycraValue.ToString());

                dt.Rows.Add(predatalist.ToArray<string>());
            }
            #endregion
            for (int i = 0; i < currentYarn.Count; i++)
            {
                List<string> datalist = new List<string>();
                #region Current Yarn 
                var res = currentYarn[i].ColumnName.ToList();
                datalist.Add(currentYarn[i].CID.ToString());
                decimal lycraValue = 0;
                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;
                    decimal requiredQty = 0;
                    string column = dt.Columns[m].ToString();
                    for (int j = 0; j < res.Count; j++)
                    {
                        if (res[j].YarnCount == column)
                        {
                            lycraValue += Convert.ToDecimal(res[j].Lycra.ToString("F"));
                            requiredQty = res[j].RequiredQuantity;
                            //datalist.Add(.ToString("F"));
                            count++;
                        }
                        //else
                        //{
                        //    datalist.Add("0");
                        //}
                    }

                    if (requiredQty != 0)
                    {

                        datalist.Add(requiredQty.ToString());
                    }
                    else
                    {
                        datalist.Add("0");
                    }
                }
                var lycraIndex = dt.Columns.IndexOf("Lycra");
                datalist[lycraIndex] = lycraValue.ToString("F");
                //if (!dt.Columns.Contains("Lycra"))
                //{
                //    dt.Columns.Add("Lycra");
                //}
                //datalist.Add(lycraValue.ToString("F"));


                dt.Rows.Add(datalist.ToArray<string>());
                #endregion

            }
            #region Total
            List<string> datalistTotal = new List<string>();

            int index = 0;
            foreach (DataColumn dc in dt.Columns)
            {
                decimal sum = 0;
                index = dt.Columns.IndexOf(dc);
                if (index == 0)
                {
                    datalistTotal.Add("Total");
                }
                else
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        decimal data = Convert.ToDecimal(dr[dc].ToString());
                        if (data > 0)
                        {
                            sum += data;
                        }

                    }
                    datalistTotal.Add(sum.ToString());
                }
            }

            dt.Rows.Add(datalistTotal.ToArray<string>());
            dt.Rows.Add();
            dt.Rows.Add();
            dt.Rows.Add("L/C Open");
            #endregion
            #endregion

            #region L/C Open
            dtLcOpen = dt.Copy();
            dtLcOpen.Clear();
            var lcOpen = (from t1 in db.Commercial_B2bLcSupplierPOSlave
                          join t2 in db.Commercial_B2bLcSupplierPO on t1.Commercial_B2bLcSupplierPOFK equals t2.ID
                          join t3 in db.Commercial_B2bLC on t2.Commercial_B2bLCFK equals t3.ID
                          join t4 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t4.ID
                          join t5 in db.Mkt_BOMSlave on t4.Mkt_BOMSlaveFK equals t5.ID
                          join t6 in db.Mkt_YarnCalculation on t5.Mkt_YarnCalculationFK equals t6.ID
                          join t7 in db.Raw_Item on t6.Raw_ItemFK equals t7.ID
                          join t8 in db.Common_Supplier on t3.Common_SupplierFK equals t8.ID
                          join t9 in db.Mkt_PO on t4.Mkt_POFK equals t9.ID
                          join t10 in db.Mkt_BOM on t4.Mkt_POFK equals t10.ID
                          join t11 in db.Common_TheOrder on t10.Common_TheOrderFk equals t11.ID
                          where t1.Active == true && t2.Active == true &&
                          t3.Active == true && t4.Active == true &&
                          t5.Active == true &&
                          t6.Active == true && t7.Active == true && t8.Active == true && t9.Active == true && t10.Active == true && t11.Active == true
                          group new { t1, t2, t3, t4, t5, t6, t7, t8 } by new { CID = t8.CID + ", " + t3.Name + ", " + t8.Name, LcDate = t3.Date }
                   into Group
                          where Group.Count() > 0
                          select new
                          {
                              CID = Group.Key.CID,
                              LcDate = Group.Key.LcDate,
                              ColumnName = Group.GroupBy(x => x.t7.Name).Select
                              (m => new
                              {
                                  YarnCount = m.Key,
                                  RequiredQuantity = m.Sum(x => (x.t1.TotalRequired))
                              })
                          }).ToList();




            var previuosBalanceLcOpen = (from t in lcOpen
                                         where t.LcDate < fromDate
                                         select new
                                         {
                                             ColumnName = t.ColumnName,
                                             LCDate = t.LcDate,
                                             CID = t.CID
                                         }).Distinct().ToList();


            var currentYarnLcOpen = (from t in lcOpen
                                     where t.LcDate >= fromDate && t.LcDate <= toDate
                                     select new
                                     {
                                         ColumnName = t.ColumnName,
                                         LCDate = t.LcDate,
                                         CID = t.CID
                                     }).Distinct().ToList();

            List<string> predatalistLcOpen = new List<string>();
            #region Previous Yarn

            if (previuosBalanceLcOpen.Count != 0)
            {
                var preRes = previuosBalanceLcOpen[0].ColumnName.ToList();
                predatalistLcOpen.Add("Opening Balance");
                decimal preReq = 0;
                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;
                    string column = dt.Columns[m].ToString();

                    preReq = PreviousYarnValanceLcOpen(column, fromDate);//preRes[j].RequiredQuantity;
                    prelycraValue += PreviousLycraValanceLcOpen(2044, fromDate); // sub Category of Lycra
                    if (preReq != 0)
                    {
                        predatalistLcOpen.Add(preReq.ToString());
                    }
                    else
                    {
                        predatalistLcOpen.Add("0");
                    }
                    //predatalistLcOpen.Add(preReq.ToString("F"));//preRes[j].RequiredQuantity.ToString("F"));
                    count++;

                }

                int lycraIndex = dt.Columns.IndexOf("Lycra");
                predatalistLcOpen[lycraIndex] = prelycraValue.ToString("F");

                dtLcOpen.Rows.Add(predatalistLcOpen.ToArray<string>());
                dt.Rows.Add(predatalistLcOpen.ToArray<string>());
            }



            #endregion

            for (int i = 0; i < currentYarnLcOpen.Count; i++)
            {
                List<string> datalistLcOpen = new List<string>();

                #region Current Yarn L/C Open
                var res = currentYarnLcOpen[i].ColumnName.ToList();
                datalistLcOpen.Add(currentYarnLcOpen[i].CID.ToString());
                decimal currentLycra = 0;
                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;
                    decimal? requiredQuantityLcOpen = 0;

                    string column = dt.Columns[m].ToString();
                    for (int j = 0; j < res.Count; j++)
                    {
                        if (res[j].YarnCount == column)
                        {
                            requiredQuantityLcOpen = res[j].RequiredQuantity == null ? 0 : res[j].RequiredQuantity;
                            currentLycra = CurrentLycraValanceLcOpen(2044, fromDate, toDate);
                            count++;
                        }

                    }
                    if (requiredQuantityLcOpen != 0) // && column != "Lycra"
                    {
                        datalistLcOpen.Add(requiredQuantityLcOpen.ToString());
                    }
                    else
                    {
                        datalistLcOpen.Add("0");
                    }



                }
                int lycraIndex = dt.Columns.IndexOf("Lycra");
                datalistLcOpen[lycraIndex] = currentLycra.ToString("F");

                dtLcOpen.Rows.Add(datalistLcOpen.ToArray<string>());
                dt.Rows.Add(datalistLcOpen.ToArray<string>());


                #endregion
            }

            #region Total
            List<string> datalistLcOpenTotal = new List<string>();

            int indexLcOpen = 0;
            foreach (DataColumn dcLcOpen in dtLcOpen.Columns)
            {
                decimal sum = 0;
                indexLcOpen = dtLcOpen.Columns.IndexOf(dcLcOpen);
                if (indexLcOpen == 0)
                {
                    datalistLcOpenTotal.Add("Total");
                }
                else
                {
                    foreach (DataRow drLcOpen in dtLcOpen.Rows)
                    {

                        decimal data = Convert.ToDecimal(drLcOpen[dcLcOpen].ToString());
                        if (data > 0)
                        {
                            sum += data;
                        }

                    }
                    datalistLcOpenTotal.Add(sum.ToString());
                }
            }

            dt.Rows.Add(datalistLcOpenTotal.ToArray<string>());

            #endregion
            #endregion

            return dt;
        }
    }

}