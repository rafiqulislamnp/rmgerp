﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_BOMSlave
    {

        private xOssContext db;
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public VmMkt_BOMSlaveFabric VmMkt_BOMSlaveFabric { get; set; }
        public Mkt_BOMStepJobs Mkt_BOMStepJobs { get; set; }
        public Mkt_BOMSlaveFabric Mkt_BOMSlaveFabric { get; set; }
        public VmMkt_PO VmMkt_PO { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public VmMktPO_Slave VmMktPO_Slave { get; set; }
        public string CustomeCID { get; set; }
        public string SearchString { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public decimal? PreviousDelivery { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }
        public User_User User_User { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public IEnumerable<VmMkt_BOMSlave> DataList { get; set; }
        [DisplayName("Received Quentity")]
        public decimal? Recieve { get; set; }
        public Mkt_YarnCalculation Mkt_YarnCalculation { get; set; }
        [DisplayName("Remaining Quentity")]
        public decimal? Now_Recieved { get; set; }
        [DisplayName("Send Quentity")]
        public decimal? Send { get; set; }
        [DisplayName("Remaining Quentity")]
        public decimal? Now_Send { get; set; }
        [DisplayName("Remaining Amount")]
        public decimal? Now_Paid { get; set; }
        [DisplayName("Total Price")]
        public decimal? Total_Price { get; set; }
        [DisplayName("Line Total")]
        public decimal? Line_Total { get; set; }
        [DisplayName("Required Quantity")]
        public decimal? TotalRequired { get; set; }
        [DisplayName("Total Price")]
        public decimal? TotalItem_Price { get; set; }
        //[DisplayName("System Loss")]
        //public decimal? SystemLoss { get; set; }
        public int RawSubCatID { get; set; }
        public int RawCatID { get; set; }
        public bool Select { get; set; }
        public VmMkt_BOMSlave()
        {
            VmControllerHelper = new VmControllerHelper();
        }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID
                     where mkt_BOMSlave.Active == true

                     select new VmMkt_BOMSlave
                     {

                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency

                     }).Where(x => x.Mkt_BOMSlave.Active == true).OrderByDescending(x => x.Mkt_BOMSlave.ID).AsEnumerable();
            this.DataList = a;

        }

      
        internal void AdditionYarnInBOM(int id, bool isNew)
        {
            var insertedYarn = GetInsertedYarn(id);
            db = new xOssContext();
           

            decimal? lycrarequiredQuantity = (from t1 in db.Mkt_BOMSlave
                                              where t1.Active == true && t1.Mkt_BOMFK == id && t1.Raw_ItemFK == 5331
                                              || t1.Active == true && t1.Mkt_BOMFK == id && t1.Raw_ItemFK == 3070
                                              select t1.RequiredQuantity).DefaultIfEmpty(0).Sum();


            var yarns = (from t1 in db.Mkt_YarnCalculation
                         where t1.Active == true
                         && t1.Mkt_BOMFK == id
                         && !insertedYarn.Contains(t1.ID)
                         select new VmMkt_BOMSlave
                         {
                             Mkt_YarnCalculation = t1
                         }).ToList();
            int? commonCurrencyFK = 0;
            int? commonUnitFK = 0;

            VmMkt_BOM vmMkt_BOM = new VmMkt_BOM();

            string lycraValue = vmMkt_BOM.GetLycraValue(id.ToString());


            /*This requirement is approved by Mohsin charman of ROMO
             =======================================================


                Solid: 1 Yarn 100%
                       2 Knitting 100%
                       3 Dying 100%
                       4 No
                       5 Extra Finish

           Yarn Dying: 1 Yarn 100%
                       2 Yarn Dying 100%
                       3 Knitting 100% editable
                       4 Yarn Dying Washing 100% of Knitting
                       5 Extra Finish

                  AOP: 1 Yarn 100%
                       2 Knitting 100%
                       3 Dying for AOP 100%
                       4 AOP 100% editable
                       5 Extra Finish */


            if (yarns.Count() != 0)
            {
                // Serial 1
                foreach (VmMkt_BOMSlave x in yarns)
                {
                    x.Mkt_BOMSlave = new Mkt_BOMSlave();
                    x.Mkt_BOMSlave.Mkt_BOMFK = id;
                    if (x.Mkt_YarnCalculation.Common_CurrencyFK != null)
                        x.Mkt_BOMSlave.Common_CurrencyFK = x.Mkt_YarnCalculation.Common_CurrencyFK.Value;
                    if (x.Mkt_YarnCalculation.Common_SupplierFK != null)
                        x.Mkt_BOMSlave.Common_SupplierFK = x.Mkt_YarnCalculation.Common_SupplierFK.Value;
                    if (x.Mkt_YarnCalculation.Common_UnitFK != null)
                        x.Mkt_BOMSlave.Common_UnitFK = x.Mkt_YarnCalculation.Common_UnitFK.Value;
                    x.Mkt_BOMSlave.Mkt_YarnCalculationFK = x.Mkt_YarnCalculation.ID;
                    x.Mkt_BOMSlave.Consumption = x.Mkt_YarnCalculation.Consumption;
                    x.Mkt_BOMSlave.Description = x.Mkt_YarnCalculation.Fabrication +"-GSM:" + Math.Round(x.Mkt_YarnCalculation.GSM) +"-Dia:"+ x.Mkt_YarnCalculation.FinishDIA + "-Color:"+ x.Mkt_YarnCalculation.Color;
                    if (x.Mkt_YarnCalculation.Raw_ItemFK != null)
                    {
                        x.Mkt_BOMSlave.Raw_ItemFK = x.Mkt_YarnCalculation.Raw_ItemFK.Value;
                    }

                    x.Mkt_BOMSlave.RequiredQuantity = (((x.Mkt_YarnCalculation.QntyPcs / 12) * x.Mkt_YarnCalculation.Consumption) / x.Mkt_YarnCalculation.ProcessLoss) - ((((x.Mkt_YarnCalculation.QntyPcs / 12) * x.Mkt_YarnCalculation.Consumption) / x.Mkt_YarnCalculation.ProcessLoss) / 100) * x.Mkt_YarnCalculation.Lycra;
                    x.Mkt_BOMSlave.Price = x.Mkt_YarnCalculation.Price;
                    x.Mkt_BOMSlave.AddReady(0);
                    x.Mkt_BOMSlave.Add();

                }

                if (isNew == true)
                {
                    // Serial 2
                    foreach (VmMkt_BOMSlave x in yarns)
                    {
                        x.Mkt_BOMSlave = new Mkt_BOMSlave();
                        x.Mkt_BOMSlave.Mkt_BOMFK = id;
                        if (x.Mkt_YarnCalculation.Common_CurrencyFK != null)
                            x.Mkt_BOMSlave.Common_CurrencyFK = x.Mkt_YarnCalculation.Common_CurrencyFK.Value;

                        x.Mkt_BOMSlave.Common_SupplierFK = 1;
                        if (x.Mkt_YarnCalculation.Common_UnitFK != null)
                            x.Mkt_BOMSlave.Common_UnitFK = x.Mkt_YarnCalculation.Common_UnitFK.Value;
                        x.Mkt_BOMSlave.Mkt_YarnCalculationFK = x.Mkt_YarnCalculation.ID;
                        x.Mkt_BOMSlave.Consumption = x.Mkt_YarnCalculation.Consumption;
                        x.Mkt_BOMSlave.Description = x.Mkt_YarnCalculation.Fabrication + "-GSM:" + Math.Round(x.Mkt_YarnCalculation.GSM) + "-Dia:" + x.Mkt_YarnCalculation.FinishDIA + "-Color:" + x.Mkt_YarnCalculation.Color;
                        if (x.Mkt_YarnCalculation.Mkt_YarnTypeFk == 1)
                        {
                            if (x.Mkt_YarnCalculation.Raw_ItemFK != null)
                            {
                                x.Mkt_BOMSlave.Raw_ItemFK = 5484; //Knitting  
                            }
                        }
                        else if (x.Mkt_YarnCalculation.Mkt_YarnTypeFk == 2)
                        {
                            if (x.Mkt_YarnCalculation.Raw_ItemFK != null)
                            {
                                x.Mkt_BOMSlave.Raw_ItemFK = 5488; //Yarn Dyeing
                            }
                        }
                        else if (x.Mkt_YarnCalculation.Mkt_YarnTypeFk == 3)
                        {
                            if (x.Mkt_YarnCalculation.Raw_ItemFK != null)
                            {
                                x.Mkt_BOMSlave.Raw_ItemFK = 5485; //Knitting for AOP
                            }
                        }
                        x.Mkt_BOMSlave.RequiredQuantity = (((x.Mkt_YarnCalculation.QntyPcs / 12) *
                                                            x.Mkt_YarnCalculation.Consumption) /
                                                           x.Mkt_YarnCalculation.ProcessLoss);// - ((((x.Mkt_YarnCalculation.QntyPcs / 12) * x.Mkt_YarnCalculation.Consumption) / x.Mkt_YarnCalculation.ProcessLoss) / 100) * x.Mkt_YarnCalculation.Lycra;
                        x.Mkt_BOMSlave.Price = 0;
                        x.Mkt_BOMSlave.AddReady(0);
                        x.Mkt_BOMSlave.Add();

                    }
                    // Serial 3
                    foreach (VmMkt_BOMSlave x in yarns)
                    {
                        x.Mkt_BOMSlave = new Mkt_BOMSlave();
                        x.Mkt_BOMSlave.Mkt_BOMFK = id;
                        if (x.Mkt_YarnCalculation.Common_CurrencyFK != null)
                            x.Mkt_BOMSlave.Common_CurrencyFK = x.Mkt_YarnCalculation.Common_CurrencyFK.Value;

                        x.Mkt_BOMSlave.Common_SupplierFK = 1;
                        if (x.Mkt_YarnCalculation.Common_UnitFK != null)
                            x.Mkt_BOMSlave.Common_UnitFK = x.Mkt_YarnCalculation.Common_UnitFK.Value;
                        x.Mkt_BOMSlave.Mkt_YarnCalculationFK = x.Mkt_YarnCalculation.ID;
                        x.Mkt_BOMSlave.Consumption = x.Mkt_YarnCalculation.Consumption;
                        x.Mkt_BOMSlave.Description = x.Mkt_YarnCalculation.Fabrication + "-GSM:" + Math.Round(x.Mkt_YarnCalculation.GSM) + "-Dia:" + x.Mkt_YarnCalculation.FinishDIA + "-Color:" + x.Mkt_YarnCalculation.Color;
                        if (x.Mkt_YarnCalculation.Mkt_YarnTypeFk == 1)
                        {
                            if (x.Mkt_YarnCalculation.Raw_ItemFK != null)
                            {
                                x.Mkt_BOMSlave.Raw_ItemFK = 5487; // Dyeing
                            }
                        }
                        else if (x.Mkt_YarnCalculation.Mkt_YarnTypeFk == 2)
                        {
                            if (x.Mkt_YarnCalculation.Raw_ItemFK != null)
                            {
                                x.Mkt_BOMSlave.Raw_ItemFK = 5486; //Yarn Dyeing Knitting
                            }
                        }
                        else if (x.Mkt_YarnCalculation.Mkt_YarnTypeFk == 3)
                        {
                            if (x.Mkt_YarnCalculation.Raw_ItemFK != null)
                            {
                                x.Mkt_BOMSlave.Raw_ItemFK = 5489; //Dyeing for AOP
                            }
                        }
                        x.Mkt_BOMSlave.RequiredQuantity = (((x.Mkt_YarnCalculation.QntyPcs / 12) *
                                                            x.Mkt_YarnCalculation.Consumption) /
                                                           x.Mkt_YarnCalculation.ProcessLoss);// - ((((x.Mkt_YarnCalculation.QntyPcs / 12) * x.Mkt_YarnCalculation.Consumption) / x.Mkt_YarnCalculation.ProcessLoss) / 100) * x.Mkt_YarnCalculation.Lycra;
                        x.Mkt_BOMSlave.Price = 0;
                        x.Mkt_BOMSlave.AddReady(0);
                        x.Mkt_BOMSlave.Add();
                    }
                    // Serial 4
                    foreach (VmMkt_BOMSlave x in yarns)
                    {
                        if (x.Mkt_YarnCalculation.Mkt_YarnTypeFk != 1)
                        {
                            x.Mkt_BOMSlave = new Mkt_BOMSlave();
                            x.Mkt_BOMSlave.Mkt_BOMFK = id;
                            if (x.Mkt_YarnCalculation.Common_CurrencyFK != null)
                                x.Mkt_BOMSlave.Common_CurrencyFK = x.Mkt_YarnCalculation.Common_CurrencyFK.Value;

                            x.Mkt_BOMSlave.Common_SupplierFK = 1;
                            if (x.Mkt_YarnCalculation.Common_UnitFK != null)
                                x.Mkt_BOMSlave.Common_UnitFK = x.Mkt_YarnCalculation.Common_UnitFK.Value;
                            x.Mkt_BOMSlave.Mkt_YarnCalculationFK = x.Mkt_YarnCalculation.ID;
                            x.Mkt_BOMSlave.Consumption = x.Mkt_YarnCalculation.Consumption;
                            x.Mkt_BOMSlave.Description = x.Mkt_YarnCalculation.Fabrication + "-GSM:" + Math.Round(x.Mkt_YarnCalculation.GSM) + "-Dia:" + x.Mkt_YarnCalculation.FinishDIA + "-Color:" + x.Mkt_YarnCalculation.Color;

                            if (x.Mkt_YarnCalculation.Mkt_YarnTypeFk == 2)
                            {
                                if (x.Mkt_YarnCalculation.Raw_ItemFK != null)
                                {
                                    x.Mkt_BOMSlave.Raw_ItemFK = 5490; //yarn Dyeing Washing
                                }
                            }
                            else if (x.Mkt_YarnCalculation.Mkt_YarnTypeFk == 3)
                            {
                                if (x.Mkt_YarnCalculation.Raw_ItemFK != null)
                                {
                                    x.Mkt_BOMSlave.Raw_ItemFK = 5491; //All Over Print (AOP)
                                }
                            }
                            x.Mkt_BOMSlave.RequiredQuantity = (((x.Mkt_YarnCalculation.QntyPcs / 12) *
                                                                x.Mkt_YarnCalculation.Consumption) /
                                                               x.Mkt_YarnCalculation.ProcessLoss);
                            // - ((((x.Mkt_YarnCalculation.QntyPcs / 12) * x.Mkt_YarnCalculation.Consumption) / x.Mkt_YarnCalculation.ProcessLoss) / 100) * x.Mkt_YarnCalculation.Lycra;
                            x.Mkt_BOMSlave.Price = 0;
                            x.Mkt_BOMSlave.AddReady(0);
                            x.Mkt_BOMSlave.Add();
                        }
                    }
                }
                var lycra = (from t1 in db.Mkt_BOMSlave
                             join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID                            
                             where t1.Active == true && t1.Mkt_BOMFK == id && t2.Raw_SubCategoryFK == 2044
                             select new VmMkt_BOMSlave
                             {
                                 Mkt_BOMSlave = t1
                             }).ToList();

                int count = 2;

                if (lycra.Count() != 0)
                {
                    //lycra.Mkt_BOMSlave.Mkt_BOMFK = lycra.Mkt_BOMSlave.Mkt_BOMFK;
                    //lycra.Mkt_BOMSlave.Common_CurrencyFK = lycra.Mkt_BOMSlave.Common_CurrencyFK;
                    //lycra.Mkt_BOMSlave.Common_SupplierFK = lycra.Mkt_BOMSlave.Common_SupplierFK;
                    //lycra.Mkt_BOMSlave.Common_UnitFK = lycra.Mkt_BOMSlave.Common_UnitFK;
                    //lycra.Mkt_BOMSlave.Mkt_YarnCalculationFK = lycra.Mkt_BOMSlave.Mkt_YarnCalculationFK;
                    //lycra.Mkt_BOMSlave.Consumption = lycra.Mkt_BOMSlave.Consumption;
                    //lycra.Mkt_BOMSlave.Description = lycra.Mkt_BOMSlave.Description;
                    //lycra.Mkt_BOMSlave.Raw_ItemFK = lycra.Mkt_BOMSlave.Raw_ItemFK;
                    //lycra.Mkt_BOMSlave.RequiredQuantity = Convert.ToDecimal(lycraValue);
                    //lycra.Mkt_BOMSlave.Price = lycra.Mkt_BOMSlave.Price;
                    //lycra.Mkt_BOMSlave.Edit(0);
                }
                else
                {
                    if (Convert.ToDecimal(lycraValue) != 0)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            VmMkt_BOMSlave vmMktBOMSlave = new VmMkt_BOMSlave();

                            vmMktBOMSlave.Mkt_BOMSlave = new Mkt_BOMSlave();
                            vmMktBOMSlave.Mkt_BOMSlave.Mkt_BOMFK = id;
                            vmMktBOMSlave.Mkt_BOMSlave.Common_CurrencyFK = 1;
                            vmMktBOMSlave.Mkt_BOMSlave.Common_SupplierFK = 1;
                            vmMktBOMSlave.Mkt_BOMSlave.Common_UnitFK = 13;
                            vmMktBOMSlave.Mkt_BOMSlave.Mkt_YarnCalculationFK = 0;
                            vmMktBOMSlave.Mkt_BOMSlave.Consumption = 0;
                            vmMktBOMSlave.Mkt_BOMSlave.Description = "Lycra Yarn 20D";
                            vmMktBOMSlave.Mkt_BOMSlave.Raw_ItemFK = 5331;
                            vmMktBOMSlave.Mkt_BOMSlave.RequiredQuantity = Convert.ToDecimal(lycraValue);
                            vmMktBOMSlave.Mkt_BOMSlave.Price = 0;
                            vmMktBOMSlave.Mkt_BOMSlave.AddReady(0);
                            vmMktBOMSlave.Mkt_BOMSlave.Add();

                        }
                    }
                }

            }
           

        }
        //public List<object> GetTempSupplier()
        //{
        //    var supplierList = new List<object>();

        //    foreach (var supplier in db.Common_Supplier)
        //    {
        //        supplierList.Add(new { Text = supplier.Name, Value = supplier.ID });
        //    }
        //    return supplierList;
        //    this.DataList = supplierList.ToList();
        //}

        private List<int> GetInsertedYarn(int bomID)
        {
            db = new xOssContext();
            List<int> x = new List<int>();
            using (var ctx = new xOssContext())
            {
                object[] parameter = { new SqlParameter("@id", bomID) };
                var a = ctx.Mkt_BOMSlave.SqlQuery("EXEC [dbo].[SP_GetInsertedYarnInBOMSlave] @id", parameter).ToList();
                foreach (var yarnFK in a)
                {
                    x.Add(yarnFK.Mkt_YarnCalculationFK);
                }
            }
            return x;
        }
        internal void GetBOMSlaveItem(string id, bool isNew)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);

            db = new xOssContext();

            var check = (from mkt_BOMSlave in db.Mkt_BOMSlave
                         where mkt_BOMSlave.Mkt_BOMFK == iD
                         select (1)
                          ).Count();

        

            AdditionYarnInBOM(iD, isNew);

            VmMkt_BOM vmMkt_BOM = new VmMkt_BOM();         


            var a = (from mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier on mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM on mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit on mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency on mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item on mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID
                     join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID                 
                     join t3 in (
                     from t4 in db.Mkt_POSlave
                     where t4.Active == true
                     select new { Mkt_POSlave = t4 }
                     ) on mkt_BOMSlave.ID equals t3.Mkt_POSlave.Mkt_BOMSlaveFK
                     into POSlave
                     from xx in POSlave.DefaultIfEmpty()

                     join d in (
                     from t6 in db.Mkt_YarnCalculation
                     where t6.Active == true
                     select new { Mkt_YarnCalculation = t6 }
                     ) on mkt_BOMSlave.Mkt_YarnCalculationFK equals d.Mkt_YarnCalculation.ID
                     into yarnCalculation
                     from dd in yarnCalculation.DefaultIfEmpty()

                     join t5 in db.Mkt_PO on xx.Mkt_POSlave.Mkt_POFK equals t5.ID
                     into PO
                     from yy in PO.DefaultIfEmpty()

                     where mkt_BOMSlave.Mkt_BOMFK == iD //&& xx.Active==true

                     select new VmMkt_BOMSlave
                     {
                         TotalRequired = mkt_BOMSlave.Consumption * Mkt_BOM.Quantity * Mkt_BOM.QPack,
                         TotalItem_Price = mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price,// Raw_Item.ID == 1045 ? yarnVal * mkt_BOMSlave.Price : (Raw_Item.ID == 5331 ? lycraVal * mkt_BOMSlave.Price : mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price) ,
                         Mkt_POSlave = xx.Mkt_POSlave,
                         Mkt_PO = yy,
                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Raw_Category = t2,
                         Raw_SubCategory = t1,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency,
                         Mkt_YarnCalculation = dd.Mkt_YarnCalculation
                     }).Where(x => x.Mkt_BOMSlave.Active == true).OrderByDescending(x => x.Raw_Category.ID).AsEnumerable();


            this.DataList = a;

        }
        internal void BOMSlaveFabricsPurchaseAndProcessing(int? id)
        {
            db = new xOssContext();
            var a = (from mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID

                     //Left Join
                     join t3 in (from t4 in db.Mkt_POSlave
                                 where t4.Active == true
                                 select new { Mkt_POSlave = t4 }
                     ) on mkt_BOMSlave.ID equals t3.Mkt_POSlave.Mkt_BOMSlaveFK
                     into POSlave
                     from xx in POSlave.DefaultIfEmpty()
                     join t5 in db.Mkt_PO on xx.Mkt_POSlave.Mkt_POFK equals t5.ID
                     into PO
                     from yy in PO.DefaultIfEmpty()
                     where mkt_BOMSlave.ID == id //mkt_BOMSlave.Common_SupplierFK == 1 && 
                     select new VmMkt_BOMSlave
                     {
                         TotalRequired = mkt_BOMSlave.Consumption * Mkt_BOM.Quantity * Mkt_BOM.QPack,
                         TotalItem_Price = mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price,
                         Mkt_POSlave = xx.Mkt_POSlave,
                         Mkt_PO = yy,
                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Raw_Category = t2,
                         Raw_SubCategory = t1,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency

                     }).Where(x => x.Mkt_BOMSlave.Active == true).OrderBy(x => x.Raw_Item.ID).FirstOrDefault();

            this.TotalRequired = a.TotalRequired;
            this.TotalItem_Price = a.TotalItem_Price;
            this.Mkt_POSlave = a.Mkt_POSlave;
            this.Mkt_PO = a.Mkt_PO;
            this.Common_Supplier = a.Common_Supplier;
            this.Mkt_BOM = a.Mkt_BOM;
            this.Mkt_BOMSlave = a.Mkt_BOMSlave;
            this.Raw_Item = a.Raw_Item;
            this.Raw_Category = a.Raw_Category;
            this.Raw_SubCategory = a.Raw_SubCategory;
            this.Common_Unit = a.Common_Unit;
            this.Common_Currency = a.Common_Currency;
        }
        internal void BOMSlaveFabricsStepJobs(int iD)
        {
            var a = (from mkt_BOMStepJobs in db.Mkt_BOMStepJobs
                     join mkt_BOMSlave in db.Mkt_BOMSlave
                     on mkt_BOMStepJobs.Mkt_BOMSlaveFK equals mkt_BOMSlave.ID
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMStepJobs.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMStepJobs.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMStepJobs.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMStepJobs.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMStepJobs.Raw_ItemFK equals Raw_Item.ID
                     join t1 in db.Raw_SubCategory on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category on t1.Raw_CategoryFK equals t2.ID
                     //Left Join
                     join t3 in (
                     from t4 in db.Mkt_POSlave
                     where t4.Active == true
                     select new { Mkt_POSlave = t4 }
                     ) on mkt_BOMStepJobs.ID equals t3.Mkt_POSlave.Mkt_BOMSlaveFK
                     into POSlave
                     from xx in POSlave.DefaultIfEmpty()

                         //join t3 in db.Mkt_POSlave on mkt_BOMSlave.ID equals t3.Mkt_BOMSlaveFK
                         //into POSlave
                         //from xx in POSlave.DefaultIfEmpty()

                     join t5 in db.Mkt_PO on xx.Mkt_POSlave.Mkt_POFK equals t5.ID
                     into PO
                     from yy in PO.DefaultIfEmpty()


                     where mkt_BOMStepJobs.Mkt_BOMSlaveFK == iD //&& xx.Active==true

                     select new VmMkt_BOMSlave
                     {
                         TotalRequired = mkt_BOMSlave.Consumption * Mkt_BOM.Quantity * Mkt_BOM.QPack,
                         TotalItem_Price = mkt_BOMStepJobs.RequiredQuantity * mkt_BOMStepJobs.Price,
                         Mkt_BOMStepJobs = mkt_BOMStepJobs,
                         Mkt_POSlave = xx.Mkt_POSlave,
                         Mkt_PO = yy,
                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Raw_Category = t2,
                         Raw_SubCategory = t1,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency

                     }).Where(x => x.Mkt_BOMStepJobs.Active == true).OrderBy(x => x.Raw_Item.ID).AsEnumerable();
            this.DataList = a;
        }

        public void POslave()
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO
                     on t1.Mkt_POFK equals t2.ID
                     where t1.Active == true
                     select new VmMktPO_Slave
                     {
                         Mkt_POSlave = t1,
                         Mkt_PO = t2
                     }).ToList();
            VmMktPO_Slave.DataList = v;
        }
        public bool AuthorizePO { get; set; }
        public int? Mkt_BOMSlaveFK { get; set; }
        public bool GetAuthorizePO(int BOMSlaveFK)
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO
                     on t1.Mkt_POFK equals t2.ID
                     where t1.Mkt_BOMSlaveFK == BOMSlaveFK
                     select new
                     {
                         AuthorizePO = t2.IsAuthorize,

                     }).ToList();

            foreach (var item in v)
            {
                this.AuthorizePO = item.AuthorizePO;
            }

            return this.AuthorizePO;
        }
        public int? GetBOMSlaveFKFromPOSlave(int BOMSlaveFK)
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO
                     on t1.Mkt_POFK equals t2.ID
                     where t1.Mkt_BOMSlaveFK == BOMSlaveFK
                     select new
                     {
                         Mkt_BOMSlaveFK = t1.Mkt_BOMSlaveFK,

                     }).ToList();

            foreach (var item in v)
            {
                this.Mkt_BOMSlaveFK = item.Mkt_BOMSlaveFK;
            }

            return this.Mkt_BOMSlaveFK;
        }

        public void POslaveID(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Common_Supplier on t1.Common_SupplierFK equals t3.ID
                     where t1.Mkt_BOMFK == id
                     select new VmMkt_BOMSlave
                     {
                         Mkt_PO = t1,
                         Mkt_BOM = t2,
                         Common_Supplier = t3
                     }).ToList();
            this.DataList = v;
        }
        internal void GetBOMSlaveFabric()
        {

            db = new xOssContext();

            //var check = (from mkt_BOMSlave in db.Mkt_BOMSlave
            //             //where mkt_BOMSlave.Mkt_BOMFK == id
            //             select (1)
            //              ).Count();

            //if (check == 0)
            //{
            //    GetSuggestedPOFromBOM(id);
            //}
            var a = (from mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     where mkt_BOMSlave.IsFabric == true
                     //&& mkt_BOMSlave.Mkt_BOMFK == id
                     select new VmMkt_BOMSlave
                     {
                         TotalRequired = mkt_BOMSlave.RequiredQuantity,
                         Total_Price = mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price,

                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency,
                         Common_TheOrder = Common_TheOrder
                     }).Where(x => x.Mkt_BOM.Active == true).OrderByDescending(x => x.Mkt_BOMSlave.ID).AsEnumerable();
            this.DataList = a;

        }
        internal void GetBOMSlaveFabricSearch(string searchString)
        {
            db = new xOssContext();
            var a = (from mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     where mkt_BOMSlave.IsFabric == true

                     select new VmMkt_BOMSlave
                     {
                         TotalRequired = mkt_BOMSlave.RequiredQuantity,
                         Total_Price = mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price,

                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency,
                         Common_TheOrder = Common_TheOrder,
                         CustomeCID = Common_TheOrder.CID + "/" + Raw_Item.Name

                     }).Where(x => x.Mkt_BOM.Active == true && x.CustomeCID.Contains(searchString)).OrderByDescending(x => x.Mkt_BOMSlave.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();
            var a = (from t1 in db.Mkt_BOMSlave
                     join t2 in db.Mkt_BOM
                     on t1.Mkt_BOMFK equals t2.ID
                     join t3 in db.Raw_Item
                     on t1.Raw_ItemFK equals t3.ID
                     join t4 in db.Raw_SubCategory
                     on t3.Raw_SubCategoryFK equals t4.ID
                     join t5 in db.Raw_Category
                     on t4.Raw_CategoryFK equals t5.ID

                     //Left join----------------------------------//

                     //join t6 in db.Mkt_POSlave on t1.ID equals t6.Mkt_BOMSlaveFK
                     //into POSlave
                     //from xx in POSlave.DefaultIfEmpty()

                     //join t7 in db.Mkt_PO on xx.Mkt_POFK equals t7.ID
                     //into PO
                     //from yy in PO.DefaultIfEmpty()

                     where t1.Active == true
                     select new VmMkt_BOMSlave
                     {
                         Mkt_BOMSlave = t1,
                         //Recieve = t4.Received,
                         Mkt_BOM = t2,
                         Raw_Item = t3,
                         Raw_SubCategory = t4,
                         Raw_Category = t5,

                         //Now_Recieved = t4.TotalRequired - t4.Received,
                         //Now_Send = t4.Received - t4.Delivered
                     }).Where(x => x.Mkt_BOMSlave.ID == iD).SingleOrDefault();

            this.Mkt_BOM = a.Mkt_BOM;
            this.Mkt_BOMSlave = a.Mkt_BOMSlave;
            this.RawCatID = a.Raw_Category.ID;
            this.RawSubCatID = a.Raw_SubCategory.ID;


        }
        //..For increment PoID..//
        internal bool CreateCID(int userID, int poID)
        {
            Mkt_BOMSlave.ID = poID;
            //Mkt_BOMSlave.CID = "PO/" + (10000 + poID).ToString();
            return Mkt_BOMSlave.Edit(userID);

        }

        public int AddStepJobs(int userID)
        {
            //Mkt_BOMSlave.Status = 0;
            Mkt_BOMStepJobs.AddReady(userID);
            return Mkt_BOMStepJobs.Add();

            //EditBOMUpdateDate()
        }


        public bool EditStepJobs(int userID)
        {
            return Mkt_BOMStepJobs.Edit(userID);

        }

        public int Add(int userID)
        {
            //Mkt_BOMSlave.Status = 0;
            Mkt_BOMSlave.AddReady(userID);
            return Mkt_BOMSlave.Add();

            //EditBOMUpdateDate()
        }
        internal void POWaitingDataLoad()
        {
            db = new xOssContext();
            var a = (from Mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID

                     where Mkt_BOMSlave.Active == true

                     select new VmMkt_BOMSlave
                     {

                         Mkt_BOMSlave = Mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency,
                         TotalRequired = Mkt_BOMSlave.Consumption * Mkt_BOM.Quantity,
                         TotalItem_Price = Mkt_BOMSlave.Consumption * Mkt_BOM.Quantity * Mkt_BOMSlave.Price,

                     }).Where(x => x.Mkt_BOMSlave.Active == true).OrderByDescending(x => x.Mkt_BOMSlave.ID).AsEnumerable();

            this.DataList = a.ToList();
        }
        internal void POWaitingSelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from Mkt_BOMSlave in db.Mkt_BOMSlave
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID
                     select new VmMkt_BOMSlave
                     {

                         Mkt_BOMSlave = Mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency

                     }).Where(x => x.Mkt_BOMSlave.Mkt_BOMFK == id).AsEnumerable();
            this.DataList = a;

        }
        //public bool Add(int userID)
        //{
        //    Mkt_BOMSlave.AddReady(userID);
        //    return CreateCID(userID, Mkt_BOMSlave.Add());

        //}

        public bool Edit(int userID)
        {
            bool dsf = Mkt_BOMSlave.Edit(userID);
            return dsf;

        }

        public bool EditBOMUpdateDate(int userID, int bomFk)
        {
            VmMkt_BOM x = new VmMkt_BOM();
            x.SelectSingle(bomFk.ToString());
            x.Mkt_BOM.UpdateDate = DateTime.Now;

            return x.Mkt_BOM.Edit(userID);

        }

        public bool Dalete(int userID)
        {
            return Mkt_BOMSlave.Delete(userID);
        }

        //internal IEnumerable<VmMkt_BOMSlave> WriteNewMeathod(List<int> ids)
        //{
        //    var a = (from Mkt_BOMSlave in db.Mkt_BOMSlave
        //             join Common_Supplier in db.Common_Supplier
        //             on Mkt_BOMSlave.Common_SupplierFK equals Common_Supplier.ID
        //             join Mkt_BOM in db.Mkt_BOM
        //             on Mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
        //             join Common_Unit in db.Common_Unit
        //             on Mkt_BOMSlave.Common_UnitFK equals Common_Unit.ID
        //             join Common_Currency in db.Common_Currency
        //             on Mkt_BOMSlave.Common_CurrencyFK equals Common_Currency.ID
        //             join Raw_Item in db.Raw_Item
        //             on Mkt_BOMSlave.Raw_ItemFK equals Raw_Item.ID
        //             select new VmMkt_BOMSlave
        //             {

        //                 Mkt_BOMSlave = Mkt_BOMSlave,
        //                 Common_Supplier = Common_Supplier,
        //                 Mkt_BOM = Mkt_BOM,
        //                 Raw_Item = Raw_Item,
        //                 Common_Unit = Common_Unit,
        //                 Common_Currency = Common_Currency

        //             }).Where(x => x.Mkt_BOMSlave.Mkt_BOMFK == 3).AsEnumerable();
        //    this.DataList = a;




        //}
        public VmMkt_BOMSlave SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_BOMSlave
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Common_Supplier on t1.Common_SupplierFK equals t3.ID
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t6 in db.Common_Unit on t1.Common_UnitFK equals t6.ID
                     join t7 in db.Mkt_Item on t4.Mkt_ItemFK equals t7.ID
                     join t8 in db.Common_Currency on t5.Common_CurrencyFK equals t8.ID
                     where t1.ID == iD && t1.Active == true
                     select new VmMkt_BOMSlave
                     {
                         TotalRequired = t1.RequiredQuantity,
                         TotalItem_Price = t1.RequiredQuantity * t1.Price,
                         Mkt_Item = t7,
                         Mkt_BOMSlave = t1,
                         Raw_Item = t2,
                         Common_Supplier = t3,
                         Mkt_BOM = t4,
                         Common_TheOrder = t5,
                         Common_Unit = t6,
                         Common_Currency = t8
                     }).FirstOrDefault();
            return v;
        }

        internal void GetBOMSlaveByID(int id)
        {

            db = new xOssContext();
            var a = (from t1 in db.Mkt_BOMSlave
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     where t1.Active == true
                     select new VmMkt_BOMSlave
                     {
                         Mkt_BOMSlave = t1,
                         Raw_Item = t2
                     }).Where(x => x.Mkt_BOMSlave.ID == id).SingleOrDefault();
            this.Mkt_BOMSlave = a.Mkt_BOMSlave;
            this.Raw_Item = a.Raw_Item;
        }
        //----------------------PO change with BOM edit--------------------------//
        public bool EditPOwithBOMslave(int userID, int BOMslaveFk)
        {
            string id = BOMslaveFk.ToString();

            VmMktPO_Slave x = new VmMktPO_Slave();
            VmMkt_BOMSlave y = new VmMkt_BOMSlave();
            x.SelectSingle(BOMslaveFk);
            y.SelectSingle(id);
            if (x.Mkt_POSlave != null)
            {
                x.Mkt_POSlave.Price = y.Mkt_BOMSlave.Price;
                x.Mkt_POSlave.TotalRequired = y.Mkt_BOMSlave.RequiredQuantity;
                x.Mkt_POSlave.Raw_ItemFK = y.Mkt_BOMSlave.Raw_ItemFK;
                x.Mkt_POSlave.Description = y.Mkt_BOMSlave.Description;
                return x.Mkt_POSlave.Edit(userID);
            }
            return false;
        }



        ///.....................StepJobs................//



        internal void SelectSingleStepJobs(int id)
        {

            db = new xOssContext();
            var a = (from mkt_BOMStepJobs in db.Mkt_BOMStepJobs
                     join mkt_BOMSlave in db.Mkt_BOMSlave
                     on mkt_BOMStepJobs.Mkt_BOMSlaveFK equals mkt_BOMSlave.ID
                     join Common_Supplier in db.Common_Supplier
                     on mkt_BOMStepJobs.Common_SupplierFK equals Common_Supplier.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on mkt_BOMStepJobs.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Unit in db.Common_Unit
                     on mkt_BOMStepJobs.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on mkt_BOMStepJobs.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on mkt_BOMStepJobs.Raw_ItemFK equals Raw_Item.ID
                     join t1 in db.Raw_SubCategory
                     on Raw_Item.Raw_SubCategoryFK equals t1.ID
                     join t2 in db.Raw_Category
                     on t1.Raw_CategoryFK equals t2.ID

                     select new VmMkt_BOMSlave
                     {
                         TotalRequired = mkt_BOMSlave.Consumption * Mkt_BOM.Quantity * Mkt_BOM.QPack,
                         TotalItem_Price = mkt_BOMSlave.RequiredQuantity * mkt_BOMSlave.Price,
                         Mkt_BOMStepJobs = mkt_BOMStepJobs,

                         Mkt_BOMSlave = mkt_BOMSlave,
                         Common_Supplier = Common_Supplier,
                         Mkt_BOM = Mkt_BOM,
                         Raw_Item = Raw_Item,
                         Raw_Category = t2,
                         Raw_SubCategory = t1,
                         Common_Unit = Common_Unit,
                         Common_Currency = Common_Currency

                     }).Where(c => c.Mkt_BOMStepJobs.ID == id).SingleOrDefault();
            this.Mkt_BOMStepJobs = a.Mkt_BOMStepJobs;
            this.TotalRequired = a.TotalRequired;
            this.TotalItem_Price = a.TotalItem_Price;
            this.Mkt_BOM = a.Mkt_BOM;
            this.Mkt_BOMSlave = a.Mkt_BOMSlave;
            this.Common_Supplier = a.Common_Supplier;
            this.Common_Unit = a.Common_Unit;
            this.Common_Currency = a.Common_Currency;
            this.Raw_Item = a.Raw_Item;
            this.RawCatID = a.Raw_Category.ID;
            this.RawSubCatID = a.Raw_SubCategory.ID;



        }

        internal void GetStepJobsByID(int id)
        {

            db = new xOssContext();
            var a = (from t1 in db.Mkt_BOMStepJobs
                     join t2 in db.Raw_Item
                     on t1.Raw_ItemFK equals t2.ID
                     where t1.Active == true
                     select new VmMkt_BOMSlave
                     {
                         Mkt_BOMStepJobs = t1,
                         Raw_Item = t2
                     }).Where(x => x.Mkt_BOMStepJobs.ID == id).SingleOrDefault();
            this.Mkt_BOMStepJobs = a.Mkt_BOMStepJobs;
            this.Raw_Item = a.Raw_Item;
        }


        public void POSlaveEdit()
        {
            

        }

        public bool PoSlaveEdit(int userID)
        {
            return Mkt_POSlave.Edit(userID);

        }

        public bool PoSlaveDelete(int userID)
        {
            return Mkt_POSlave.Delete(userID);
        }
    }

}