﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Oss.Romo.Models.Common;
using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Reports;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_BuyingAgent
    {
        private xOssContext db = new xOssContext();
        public IEnumerable<VmMkt_BuyingAgent> DataList { get; set; }
        public List<ReportDocType> ReportDoc { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Mkt_BuyingAgent Mkt_BuyingAgent { get; set; }
        public Common_Country Common_Country { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }

        [DisplayName("Chart 2 Account")]
        public int Acc_Chart2IDFk { get; set; }
        //public Emran Emran { get; set; }
        public VmMkt_BuyingAgent()
        {
            //db = new xOssContext();
            //InitialDataLoad();
            VmControllerHelper = new VmControllerHelper();

        }

        public void InitialDataLoad()
        {
            //db = new xOssContext();
            var v = (from t1 in db.Mkt_BuyingAgent
                   
                    select new VmMkt_BuyingAgent
                    {
                        Mkt_BuyingAgent = t1
                    }).Where(x => x.Mkt_Buyer.Active == true).OrderByDescending(x => x.Mkt_Buyer.ID).AsEnumerable();
            this.DataList = v;
          
        }
     
        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);

            db = new xOssContext();
            var v = (from rc in db.Mkt_BuyingAgent
                  
                     select new VmMkt_BuyingAgent
                     {

                         Mkt_BuyingAgent = rc
                     }).SingleOrDefault(c => c.Mkt_BuyingAgent.ID == id);
            this.Mkt_BuyingAgent = v.Mkt_BuyingAgent;
          
        }

        public int Add(int userID)
        {
            Mkt_BuyingAgent.AddReady(userID);
            return Mkt_BuyingAgent.Add();
        }

        public bool Edit(int userID)
        {
            return Mkt_BuyingAgent.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_BuyingAgent.Delete(userID);
        }

        
    }

}