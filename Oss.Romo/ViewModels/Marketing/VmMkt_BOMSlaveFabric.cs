﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_BOMSlaveFabric
    {
        private xOssContext db;
        public VmControllerHelper VmControllerHelper { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public VmMkt_BOM VmMkt_BOM { get; set; }
        public Mkt_BOMSlave Mkt_BOMSlave { get; set; }
        public VmMkt_BOMSlave VmMkt_BOMSlave { get; set; }
        public Mkt_BOMSlaveFabric Mkt_BOMSlaveFabric { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Raw_SubCategory Raw_SubCategory { get; set; }
        public Raw_Category Raw_Category { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public IEnumerable<VmMkt_BOMSlaveFabric> DataList { get; set; }
        public List<ReportDocType> FebricReportDoc { get; set; }

        [DisplayName("Required Quantity")]
        public decimal? TotalRequired { get; set; }
        [DisplayName("Total Price")]
        public decimal? TotalItem_Price { get; set; }

        public int RawSubCatID { get; set; }
        public int RawCatID { get; set; }
        public VmMkt_BOMSlaveFabric()
        {
            db = new xOssContext();
            InitialDataLoad();
        }

        public void InitialDataLoad()
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_BOMSlaveFabric
                     join t2 in db.Common_Supplier 
                     on t1.Common_SupplierFK equals t2.ID
                     join t4 in db.Common_Unit 
                     on t1.Common_UnitFK equals t4.ID
                     join t5 in db.Mkt_BOMSlave on t1.Mkt_BOMSlaveFK equals t5.ID
                     join t7 in db.Mkt_BOM on t5.Mkt_BOMFK equals  t7.ID
                     join t6 in db.Raw_Item 
                     on t1.Raw_ItemFK equals t6.ID
                     select new VmMkt_BOMSlaveFabric
                     {                       
                         Mkt_BOMSlaveFabric = t1,
                         Common_Supplier = t2,
                         Common_Unit = t4,
                         Mkt_BOMSlave=t5,
                         Raw_Item = t6,
                         Mkt_BOM=t7
                     }).Where(x => x.Mkt_BOMSlaveFabric.Active == true).OrderByDescending(x => x.Mkt_BOMSlaveFabric.ID).AsEnumerable();
            this.DataList = v;
        }
        public void InitialDataLoadFabric(int?id)
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_BOMSlaveFabric
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Mkt_BOMSlave on t1.Mkt_BOMSlaveFK equals t4.ID
                     join t5 in db.Raw_Item on t1.Raw_ItemFK equals t5.ID
                     join t6 in db.Common_Currency on t1.Common_CurrencyFK equals t6.ID
                     join t7 in db.Mkt_BOM on t4.Mkt_BOMFK equals t7.ID
                     where t1.Mkt_BOMSlaveFK==id
                     select new VmMkt_BOMSlaveFabric
                     {
                         TotalRequired = t1.RequiredQuantity,
                         TotalItem_Price = t1.RequiredQuantity * t1.Price,
                         Mkt_BOMSlaveFabric = t1,
                         Common_Supplier = t2,
                         Common_Unit = t3,
                         Mkt_BOMSlave=t4,
                         Raw_Item = t5,
                         Common_Currency=t6,
                         Mkt_BOM=t7
                     }).Where(x => x.Mkt_BOMSlaveFabric.Active == true).OrderBy(x => x.Mkt_BOMSlaveFabric.ID).AsEnumerable();
            this.DataList = v;
        }


        //public void SelectSingle(int iD)
        //{
        //    db = new xOssContext();
        //    var v = (from t1 in db.Mkt_BOMSlaveFabric
        //             select new VmMkt_BOMSlaveFabric
        //             {
        //                 Mkt_BOMSlaveFabric = t1
        //             }).Where(c => c.Mkt_BOMSlaveFabric.ID == iD).SingleOrDefault();
        //    this.Mkt_BOMSlaveFabric = v.Mkt_BOMSlaveFabric;

        //}
      
        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from t1 in db.Mkt_BOMSlaveFabric
                     join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Raw_SubCategory on t2.Raw_SubCategoryFK equals t3.ID
                     join t4 in db.Raw_Category on t3.Raw_CategoryFK equals t4.ID
                     select new VmMkt_BOMSlaveFabric
                     {
                         Raw_SubCategory = t3,
                         Raw_Category = t4,
                         Raw_Item = t2,
                         Mkt_BOMSlaveFabric = t1
                     }).Where(c => c.Mkt_BOMSlaveFabric.ID == id).FirstOrDefault();
            this.Mkt_BOMSlaveFabric = v.Mkt_BOMSlaveFabric;
            this.Raw_Item = v.Raw_Item;
            this.RawCatID = v.Raw_Category.ID;
            this.RawSubCatID = v.Raw_SubCategory.ID;
        }
       
        public int Add(int userID)
        {
            Mkt_BOMSlaveFabric.AddReady(userID);
            return Mkt_BOMSlaveFabric.Add();
        }

        public bool Edit(int userID)
        {
            return Mkt_BOMSlaveFabric.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Mkt_BOMSlaveFabric.Delete(userID);
        }


   

        internal void FabricReportDocLoad(string id)
        {
            int iD = 0;
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            db = new xOssContext();
            
            var a = (from Mkt_BOMSlaveFabric in db.Mkt_BOMSlaveFabric
                     join Mkt_BOMSlave in db.Mkt_BOMSlave
                     on Mkt_BOMSlaveFabric.Mkt_BOMSlaveFK equals Mkt_BOMSlave.ID
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_BOMSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_BOMSlaveFabric.Common_CurrencyFK equals Common_Currency.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_BOMSlaveFabric.Raw_ItemFK equals Raw_Item.ID
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_BOMSlaveFabric.Common_SupplierFK equals Common_Supplier.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_BOMSlaveFabric.Common_UnitFK equals Common_Unit.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Common_TheOrder.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     where Mkt_BOMSlave.ID == iD &&
                     Mkt_BOMSlaveFabric.Active == true

                     select new ReportDocType
                     {
                         HeaderLeft1 = Common_TheOrder.CID + "/" + Mkt_BOM.Style + "Fabric",
                         HeaderLeft2 = Mkt_BOM.FirstMove.ToString(),
                         HeaderLeft3 = Mkt_BOM.UpdateDate.ToString(),
                         HeaderLeft4 = Mkt_BOM.Reference.ToString(),
                         HeaderLeft5 = Mkt_BOM.Class.ToString(),
                         HeaderLeft6 = Mkt_BOM.Fabrication,
                         HeaderLeft7 = Mkt_BOM.SizeRange,
                         HeaderLeft8 = Mkt_BOM.Style,
                         HeaderMiddle1 = Mkt_Buyer.Name,
                         HeaderMiddle2 = Common_TheOrder.BuyerPO,
                         HeaderMiddle3 = Common_TheOrder.OrderDate.ToString(),
                         HeaderMiddle4 = Common_TheOrder.Season,
                         HeaderMiddle5 = Mkt_Buyer.Description,
                         HeaderRight1 = Mkt_BOM.QPack.ToString(),
                         HeaderRight2 = Mkt_BOM.Quantity.ToString(),
                         HeaderRight3 = Mkt_BOM.UnitPrice.ToString(),
                         HeaderRight4 = (Mkt_BOM.QPack * Mkt_BOM.Quantity).ToString(),
                         HeaderRight5 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) / 12).ToString(),
                         HeaderRight6 = ((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice).ToString(),
                         Body1 = Raw_Item.Name,
                         Body2 = Mkt_BOMSlaveFabric.Description,
                         Body3 = Mkt_BOMSlaveFabric.Consumption.ToString(),
                         Body4 = Common_Unit.Name,
                         Body5 = Mkt_BOMSlaveFabric.Price.ToString(),
                         Body6 = Common_Supplier.Name,
                         Body7 = (Mkt_BOMSlaveFabric.RequiredQuantity * Mkt_BOMSlaveFabric.Price).ToString(),
                         Body8 = Mkt_BOMSlave.IsFabric.ToString(),
                         Body9 = Mkt_BOMSlaveFabric.RequiredQuantity.ToString(),
                         Body10 = Mkt_BOMSlaveFabric.ID.ToString()


                     }).ToList();
            decimal TTL = 0;
            //decimal TTLFabrics = 0;
            //decimal TTLAccount = 0;
            //decimal TTLCM = 0;
            //decimal TTLFOB = 0;
            //decimal CMperDZ = 0;
            //decimal TTLQDZ = 0;



            foreach (ReportDocType r in a)
            {
                decimal body7 = 0;
                decimal.TryParse(r.Body7, out body7);
                TTL += body7;

                ////if (r.Body8 == "True")
                ////{
                //    decimal subbody2 = 0;
                //    decimal.TryParse(r.Body7, out subbody2);
                //    TTLFabrics += subbody2;
                ////}
                ////else
                ////{

                ////    decimal subbody2 = 0;
                ////    decimal.TryParse(r.Body7, out subbody2);
                ////    TTLAccount += subbody2;

                ////}

                //decimal subbody3 = 0;
                //decimal.TryParse(r.HeaderRight6, out subbody3);
                //TTLFOB = subbody3;
                //decimal subbody4 = 0;
                //decimal.TryParse(r.HeaderRight4, out subbody4);
                //TTLQDZ = subbody4;
                ////TTLAccount = TTL - TTLFabrics;
                //TTLCM = TTLFOB - TTL;
                //CMperDZ = TTLCM / TTLQDZ * 12;

            }
            foreach (ReportDocType r in a)
            {
                r.SubBody1 = "$" + TTL.ToString("F");
                //r.SubBody2 = "$" + TTLFabrics.ToString("F");
                //r.SubBody3 = "$" + TTLFOB.ToString("F");
                //r.SubBody4 = "$" + TTLQDZ.ToString("F");
                //r.SubBody5 = "$" + TTLAccount.ToString("F");
                //r.SubBody6 = "$" + TTLCM.ToString("F");
                //r.SubBody7 = "$" + CMperDZ.ToString("F");

                decimal temp = 0;

                decimal.TryParse(r.Body7, out temp);
                r.Body7 = temp.ToString("F");

       

                r.HeaderLeft2 = r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
                r.HeaderLeft3 = r.HeaderLeft3.Substring(0, r.HeaderLeft3.Length - 8);
                r.HeaderMiddle3 = r.HeaderMiddle3.Substring(0, r.HeaderMiddle3.Length - 8);


            }


            this.FebricReportDoc = a;
        }
    }
}