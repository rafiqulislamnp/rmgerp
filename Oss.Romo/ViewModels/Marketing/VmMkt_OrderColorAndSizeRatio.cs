﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMkt_OrderColorAndSizeRatio
    {
        private xOssContext db;
        public Mkt_OrderColorAndSizeRatio Mkt_OrderColorAndSizeRatio { get; set; }
        public Mkt_OrderDeliverySchedule Mkt_OrderDeliverySchedule { get; set; }
        public Common_Country Common_Country { get; set; }
        public IEnumerable<VmMkt_OrderColorAndSizeRatio> DataList { get; set; }
        public IEnumerable<VmMkt_BOM> DataList1 { get; set; }

        public List<SizeList> SizeList { get; set; }
        public decimal? AvailableQty { get; set; }
        public decimal? Quantity { get; set; }
        public string ShipmentColor { get; set; }
        public string ColorSize { get; set; }

        public void OrderColorAndSizeRatioDataLoad(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderColorAndSizeRatio
                     join t2 in db.Mkt_OrderDeliverySchedule on t1.OrderDeliveryScheduleFk equals t2.ID
                     join t3 in db.Common_Country on t2.Destination equals t3.ID
                     select new VmMkt_OrderColorAndSizeRatio
                     {
                         Mkt_OrderColorAndSizeRatio = t1,
                         Mkt_OrderDeliverySchedule = t2,
                         Common_Country=t3

                     }).Where(x => x.Mkt_OrderColorAndSizeRatio.Common_TheOrderFk == id && x.Mkt_OrderColorAndSizeRatio.Active == true).OrderByDescending(x => x.Mkt_OrderColorAndSizeRatio.ID).AsEnumerable();
            this.DataList = v;

        }
        public int Add(int userID)
        {
            Mkt_OrderColorAndSizeRatio.AddReady(userID);
            return Mkt_OrderColorAndSizeRatio.Add();
        }
        public bool Edit(int userID)
        {
            return Mkt_OrderColorAndSizeRatio.Edit(userID);
        }

        public bool BulkDelete(int userID, int Id)
        {
            using (db = new xOssContext())
            {
                var vData = db.Mkt_OrderColorAndSizeRatio.Where(a => a.OrderDeliveryScheduleFk == Id && a.Active == true);
                if (vData.Any())
                {
                    foreach (var item in vData)
                    {
                        item.Delete(userID);
                    }
                }
            }
            return false;
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Mkt_OrderColorAndSizeRatio
                     select new VmMkt_OrderColorAndSizeRatio
                     {
                         Mkt_OrderColorAndSizeRatio = b

                     }).Where(c => c.Mkt_OrderColorAndSizeRatio.ID == id).SingleOrDefault();
            this.Mkt_OrderColorAndSizeRatio = a.Mkt_OrderColorAndSizeRatio;
        }

        public void GetColorAndSizeQuantity(int id)
        {
            db = new xOssContext();
            var a = (from b in db.Mkt_OrderColorAndSizeRatio
                     select new VmMkt_OrderColorAndSizeRatio
                     {
                         Mkt_OrderColorAndSizeRatio = b

                     }).Where(c => c.Mkt_OrderColorAndSizeRatio.Mkt_BOMFk == id && c.Mkt_OrderColorAndSizeRatio.Active == true).AsEnumerable();
            this.DataList = a;
        }

        public DataTable GetColorAndSizeDimension(int id)
        {
            db = new xOssContext();

            DataTable dt = new DataTable();
            var data = db.Mkt_OrderColorAndSizeRatio.Distinct().ToList();

            var d = (from f in data
                     where f.Active == true && f.Common_TheOrderFk == id
                     group f by new { f.Color } into Group
                     where Group.Count() > 0
                     select new
                     {
                         Group.Key.Color,
                         ColumnName = Group.GroupBy(f => f.Size).Select
                         (m => new { Sub = m.Key, Score = m.Sum(c => c.Quantity) })
                     }).ToList();

            var sub = (from f in db.Mkt_OrderColorAndSizeRatio.AsEnumerable()
                       where f.Active == true && f.Common_TheOrderFk == id
                       select new
                       {
                           f.Size
                       }).Distinct().ToList();

            ArrayList objDataColumn = new ArrayList();

            if (data.Count() > 0)
            {
                objDataColumn.Add("Color");
                for (int i = 0; i < sub.Count; i++)
                {
                    objDataColumn.Add(sub[i].Size);
                }
            }

            //Add column name dynamically
            for (int i = 0; i < objDataColumn.Count; i++)
            {
                dt.Columns.Add(objDataColumn[i].ToString());
            }

            //Add data into datatable
            for (int i = 0; i < d.Count; i++)
            {
                List<string> datalist = new List<string>();
                datalist.Add(d[i].Color.ToString());

                var res = d[i].ColumnName.ToList();
                for (int m = 1; m < dt.Columns.Count; m++)
                {
                    int count = 0;

                    for (int j = 0; j < res.Count; j++)
                    {
                        string column = dt.Columns[m].ToString();

                        if (res[j].Sub == column)
                        {
                            datalist.Add(res[j].Score.ToString());
                            count++;
                        }
                    }

                    if (count == 0)
                    {
                        string val = "0";
                        datalist.Add(val);
                    }
                }

                dt.Rows.Add(datalist.ToArray<string>());
                
            }
            return dt;
        }

        public void GetSizeList(string InputSize)
        {
            SizeList=new List<Marketing.SizeList>();
            string[] sizeword = null;
            //string InputSize = "X-100;XL-200;M-200;";
            sizeword = InputSize.Split(';');
            for (int i = 0; i < sizeword.Length-1; i++)
            {
                int q = 0;
                string SizeNumber = sizeword[i].Split('-').First();
                //string SizeNumber = sizeword[i].Substring(0, sizeword[i].LastIndexOf("-"));
                string SizeQuantity = sizeword[i].Split('-').Last();
                int.TryParse(SizeQuantity, out q);

                SizeList.Add(new SizeList { Size = SizeNumber, Quantity = q });
            }
        }

        public void GetColorSummery(int Id)
        {
            using (db = new xOssContext())
            {
                string allColor = string.Empty;
                var vData = (from b in db.Mkt_OrderColorAndSizeRatio
                        where b.Active==true && b.OrderDeliveryScheduleFk==Id
                        group b by new { b.Color } into all
                        select new
                         {
                            Color=all.Key.Color,
                            ColorQuantity=(int)all.Sum(a=>a.Quantity)
                         }).ToList();

                if (vData.Any())
                {
                    foreach (var v in vData)
                    {
                        allColor += v.Color + "-" + v.ColorQuantity + " ";
                    }
                }
                this.ShipmentColor = allColor;
            }
                
        }
    }
}