﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Marketing
{
    public class VmMktBuyerVM : BaseModel
    {
        [DefaultValue(0)]
        [DisplayName("Buyer ID")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string BuyerID { get; set; }

        [DisplayName("Buyer Name")]
        [Required(ErrorMessage = "Buyer Name Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Name { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [MaxLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string Phone { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DisplayName("Contact Person")]
        public string ContactPerson { get; set; }

        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [DisplayName("Country")]
        public int Common_CountryFk { get; set; }

        [DisplayName("Country")]
        public string Country { get; set; }
        
        [DisplayName("Account Head Category")]
        public int Acc_AcNameFk { get; set; }

        [DisplayName("Chart 2 Account")]
        public int Acc_Chart2IDFk { get; set; }

        [DisplayName("First Notyfy Party")]
        public string FirstNotyfyParty { get; set; }

        [DisplayName("Second Notyfy Party")]
        public string SecondNotyfyParty { get; set; }

        [DisplayName("Third Notyfy Party")]
        public string ThirdNotyfyParty { get; set; }

        public string BillTo { get; set; }

        public IEnumerable<VmMktBuyerVM> DataList { get; set; }
    }
}