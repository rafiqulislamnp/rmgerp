﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class SummaryReport
    {
        public decimal MasterLCValue { get; set; }

        public decimal ExportValue { get; set; }

        public decimal BOMValue { get; set; }

        public decimal POIssuedValue { get; set; }

        public decimal BTBLimitValue { get; set; }

        public decimal BTBIssuedValue { get; set; }

        public decimal BTBAvailableValue { get; set; }

        public decimal BOMRestValue { get; set; }

        public decimal PORestValue { get; set; }

        public decimal POPending { get; set; }

        public string MasterLCNo { get; set; }

        [Required, Display(Name = "From Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Required, Display(Name = "To Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
    }

    public class VmCommercial_Report
    {
        private xOssContext db;

        public SummaryReport SummaryReport { get; set; }
        
        public List<SummaryReport> BTBList { get; set; }
        public void GetPeriodicSummaryReport(DateTime FromDate,DateTime ToDate)
        {
            db = new xOssContext();
            SummaryReport model = new SummaryReport();
            decimal BTBIssuedValue = decimal.Zero;
            decimal BOMValue = decimal.Zero;
            decimal POIssuedValue = decimal.Zero;
            decimal MasterLCValue = decimal.Zero;
            decimal BTBLimitValue = decimal.Zero;
            decimal ExportValue = decimal.Zero;
            
            decimal HandlingCharge = decimal.Zero;
            decimal FreightCharge = decimal.Zero;
            decimal GarmentTest = decimal.Zero;
            decimal BuyerCharge = decimal.Zero;
            decimal TotalCharge = decimal.Zero;

            #region ExecuteQuery
            var vData = (from ud in db.Commercial_UD
                         where ud.Active == true
                         group new { ud } by new { ud.ID } into groupUd
                         select new
                         {
                             MasterLC = (from udslave in db.Commercial_UDSlave
                                         join mlc in db.Commercial_MasterLC on udslave.Commercial_MasterLCFK equals mlc.ID
                                         where
                                         udslave.Commercial_UDFK == groupUd.Key.ID &&
                                         udslave.Active == true &&
                                         mlc.Active == true &&
                                         mlc.Date >= FromDate &&
                                         mlc.Date <= ToDate
                                         group mlc by new { mlc.ID } into groupMLC
                                         select new
                                         {
                                             TotalMLc = groupMLC.Sum(a => a.TotalValue),
                                             HandlingCharge = groupMLC.Sum(a => a.HandlingCharge),
                                             FreightCharge = groupMLC.Sum(a => a.FreightCharge),

                                             //TotalBTBLimit = allmlc.Sum(a => (a.mlc.TotalValue * a.BTBOpening) / 100),

                                             TotalBOM = (from mlcbuyerpo in db.Commercial_MasterLCBuyerPO
                                                         join bom in db.Mkt_BOM on mlcbuyerpo.MKTBOMFK equals bom.ID
                                                         join order in db.Common_TheOrder on bom.Common_TheOrderFk equals order.ID
                                                         where
                                                         order.Active == true &&
                                                         bom.Active == true &&
                                                         mlcbuyerpo.Active == true &&
                                                         mlcbuyerpo.Commercial_MasterLCFK == groupMLC.Key.ID
                                                         group new { bom.QPack, bom.Quantity, bom.UnitPrice, order } by new { bom.ID } into allPO
                                                         select new
                                                         {
                                                             TotalPOValue = allPO.Sum(a => (a.QPack * a.Quantity * a.UnitPrice)),

                                                             BOMValue = (from o in db.Mkt_BOMSlave
                                                                         where o.Active == true && o.Mkt_BOMFK == allPO.Key.ID
                                                                         select new { o }).ToList().Sum(b => (b.o.RequiredQuantity * b.o.Price)),

                                                             TotalPOIssued = (from mktpo in db.Mkt_PO
                                                                              join poslave in db.Mkt_POSlave on mktpo.ID equals poslave.Mkt_POFK
                                                                              join currency in db.Common_Currency on mktpo.Common_CurrencyFK equals currency.ID
                                                                              where
                                                                              poslave.Active == true &&
                                                                              mktpo.Mkt_BOMFK == allPO.Key.ID &&
                                                                              mktpo.Active == true
                                                                              select new { poslave }).ToList().Sum(a => a.poslave.TotalRequired * a.poslave.Price),

                                                             BuyerCommission = allPO.Sum(a => (a.QPack * a.Quantity * a.UnitPrice) / 100 * a.order.Commission)
                                                         }).ToList(),
                                         }).ToList(),

                             BTBIssuedValue = (from b2b in db.Commercial_B2bLC
                                               where
                                               b2b.Active == true &&
                                               b2b.Commercial_UDFK == groupUd.Key.ID
                                               group b2b.Amount by new { b2b.ID } into allMlc
                                               select new
                                               {
                                                   total = allMlc.Sum()
                                               }).ToList()
                         }).ToList();

            if (vData.Any())
            {
                foreach (var udItem in vData)
                {

                    if (udItem.MasterLC.Any())
                    {
                        foreach (var lcItem in udItem.MasterLC)
                        {
                            MasterLCValue += lcItem.TotalMLc == null ? 0 : (decimal)lcItem.TotalMLc;
                            HandlingCharge += lcItem.HandlingCharge == null ? 0 : (decimal)lcItem.HandlingCharge;
                            FreightCharge += lcItem.FreightCharge == null ? 0 : (decimal)lcItem.FreightCharge;

                            if (lcItem.TotalBOM.Any())
                            {
                                foreach (var bom in lcItem.TotalBOM)
                                {
                                    ExportValue += bom.TotalPOValue;
                                    BOMValue += bom.BOMValue == null ? 0 : (decimal)bom.BOMValue;
                                    POIssuedValue += bom.TotalPOIssued == null ? 0 : (decimal)bom.TotalPOIssued;
                                    BuyerCharge += bom.BuyerCommission == null ? 0 : (decimal)bom.BuyerCommission;
                                }
                            }

                        }
                    }

                    if (udItem.BTBIssuedValue.Any())
                    {
                        foreach (var b2bItem in udItem.BTBIssuedValue)
                        {
                            BTBIssuedValue += b2bItem.total == null ? 0 : (decimal)b2bItem.total;
                        }
                    }

                }
            }

            #endregion

            TotalCharge = HandlingCharge + FreightCharge + BuyerCharge;

            decimal ActualPO = ExportValue - TotalCharge;
            BTBLimitValue = (ActualPO * 75) / 100;

            //model.MasterLCValue = "$ " + Math.Round(MasterLCValue).ToString("#,##0");
            //model.BOMValue = "$ " + Math.Round(BOMValue).ToString("#,##0");
            //model.ExportValue = "$ " + Math.Round(ActualPO).ToString("#,##0");

            //model.POIssuedValue = "$ " + Math.Round(POIssuedValue).ToString("#,##0");
            //model.POPending = "$ " + Math.Round(BOMValue - POIssuedValue).ToString("#,##0");

            //model.BTBLimitValue = "$ " + Math.Round(BTBLimitValue).ToString("#,##0");
            //model.BTBIssuedValue = "$ " + Math.Round(BTBIssuedValue).ToString("#,##0");

            //model.BTBAvailableValue = "$ " + Math.Round(BTBLimitValue - BTBIssuedValue).ToString("#,##0");
            //model.BOMRestValue = "$ " + Math.Round(BOMValue - BTBIssuedValue).ToString("#,##0");
            //model.PORestValue = "$ " + Math.Round(POIssuedValue - BTBIssuedValue).ToString("#,##0");

            model.MasterLCValue = MasterLCValue;
            model.BOMValue = BOMValue;
            model.ExportValue = ActualPO;

            model.POIssuedValue = POIssuedValue;
            model.POPending = BOMValue - POIssuedValue;

            model.BTBLimitValue = BTBLimitValue;
            model.BTBIssuedValue = BTBIssuedValue;

            model.BTBAvailableValue = BTBLimitValue - BTBIssuedValue;
            model.BOMRestValue = BOMValue - BTBIssuedValue;
            model.PORestValue = POIssuedValue - BTBIssuedValue;

            this.SummaryReport = model;
        }

        public void GetSummaryReport()
        {
            db = new xOssContext();
            SummaryReport model = new SummaryReport();
            decimal BTBIssuedValue = decimal.Zero;
            decimal BOMValue = decimal.Zero;
            decimal POIssuedValue = decimal.Zero;
            decimal MasterLCValue = decimal.Zero;
            decimal BTBLimitValue = decimal.Zero;
            decimal ExportValue = decimal.Zero;


            decimal HandlingCharge = decimal.Zero;
            decimal FreightCharge = decimal.Zero;
            decimal GarmentTest = decimal.Zero;
            decimal BuyerCharge = decimal.Zero;
            decimal TotalCharge = decimal.Zero;
            
            #region ExecuteQuery
            var vData = (from ud in db.Commercial_UD
                         where ud.Active == true
                         group new { ud } by new { ud.ID } into groupUd
                         select new
                         {
                             MasterLC = (from udslave in db.Commercial_UDSlave
                                         join mlc in db.Commercial_MasterLC on udslave.Commercial_MasterLCFK equals mlc.ID
                                         where
                                         udslave.Commercial_UDFK == groupUd.Key.ID &&
                                         udslave.Active == true &&
                                         mlc.Active == true
                                         group mlc by new { mlc.ID } into groupMLC
                                         select new
                                         {
                                             TotalMLc = groupMLC.Sum(a => a.TotalValue),
                                             HandlingCharge = groupMLC.Sum(a => a.HandlingCharge),
                                             FreightCharge = groupMLC.Sum(a => a.FreightCharge),

                                             //TotalBTBLimit = allmlc.Sum(a => (a.mlc.TotalValue * a.BTBOpening) / 100),

                                             TotalBOM = (from mlcbuyerpo in db.Commercial_MasterLCBuyerPO
                                                         join bom in db.Mkt_BOM on mlcbuyerpo.MKTBOMFK equals bom.ID
                                                         join order in db.Common_TheOrder on bom.Common_TheOrderFk equals order.ID
                                                         where
                                                         order.Active == true &&
                                                         bom.Active == true &&
                                                         mlcbuyerpo.Active == true &&
                                                         mlcbuyerpo.Commercial_MasterLCFK == groupMLC.Key.ID
                                                         group new { bom.QPack, bom.Quantity, bom.UnitPrice, order } by new { bom.ID } into allPO
                                                         select new
                                                         {
                                                             TotalPOValue = allPO.Sum(a => (a.QPack * a.Quantity * a.UnitPrice)),

                                                             BOMValue = (from o in db.Mkt_BOMSlave
                                                                         where o.Active == true && o.Mkt_BOMFK == allPO.Key.ID
                                                                         select new { o }).ToList().Sum(b => (b.o.RequiredQuantity * b.o.Price)),

                                                             TotalPOIssued = (from mktpo in db.Mkt_PO
                                                                              join poslave in db.Mkt_POSlave on mktpo.ID equals poslave.Mkt_POFK
                                                                              join currency in db.Common_Currency on mktpo.Common_CurrencyFK equals currency.ID
                                                                              where
                                                                              poslave.Active == true &&
                                                                              mktpo.Mkt_BOMFK == allPO.Key.ID &&
                                                                              mktpo.Active == true
                                                                              select new { poslave }).ToList().Sum(a => a.poslave.TotalRequired * a.poslave.Price),

                                                             BuyerCommission = allPO.Sum(a => (a.QPack * a.Quantity * a.UnitPrice)/100 * a.order.Commission)
                                                        }).ToList(),
                                      }).ToList(),
                                      
                             BTBIssuedValue = (from b2b in db.Commercial_B2bLC
                                               where 
                                               b2b.Active == true &&
                                               b2b.Commercial_UDFK == groupUd.Key.ID
                                               group b2b.Amount by new { b2b.ID } into allMlc
                                               select new
                                               {
                                                   total = allMlc.Sum()
                                               }).ToList()
                            }).ToList();
            
            if (vData.Any())
            {
                foreach (var udItem in vData)
                {

                    if (udItem.MasterLC.Any())
                    {
                        foreach (var lcItem in udItem.MasterLC)
                        {
                            MasterLCValue += lcItem.TotalMLc == null ? 0 : (decimal)lcItem.TotalMLc;
                            HandlingCharge += lcItem.HandlingCharge == null ? 0 : (decimal)lcItem.HandlingCharge;
                            FreightCharge += lcItem.FreightCharge == null ? 0 : (decimal)lcItem.FreightCharge;

                            if (lcItem.TotalBOM.Any())
                            {
                                foreach (var bom in lcItem.TotalBOM)
                                {
                                    ExportValue += bom.TotalPOValue;
                                    BOMValue += bom.BOMValue == null ? 0 : (decimal)bom.BOMValue;
                                    POIssuedValue += bom.TotalPOIssued == null ? 0 : (decimal)bom.TotalPOIssued;
                                    BuyerCharge += bom.BuyerCommission == null ? 0 : (decimal)bom.BuyerCommission;
                                }
                            }
                            
                        }
                    }

                    if (udItem.BTBIssuedValue.Any())
                    {
                        foreach (var b2bItem in udItem.BTBIssuedValue)
                        {
                            BTBIssuedValue += b2bItem.total == null ? 0 : (decimal)b2bItem.total; 
                        }
                    }
                    
                }
            }

            #endregion

            TotalCharge = HandlingCharge + FreightCharge + BuyerCharge;

            decimal ActualPO = ExportValue - TotalCharge;
            BTBLimitValue = (ActualPO * 75) / 100;

            //model.MasterLCValue = "$ " + Math.Round(MasterLCValue).ToString("#,##0");
            //model.BOMValue = "$ " + Math.Round(BOMValue).ToString("#,##0");
            //model.ExportValue = "$ " + Math.Round(ActualPO).ToString("#,##0");

            //model.POIssuedValue = "$ " + Math.Round(POIssuedValue).ToString("#,##0");
            //model.POPending = "$ " + Math.Round(BOMValue - POIssuedValue).ToString("#,##0");

            //model.BTBLimitValue = "$ " + Math.Round(BTBLimitValue).ToString("#,##0");
            //model.BTBIssuedValue = "$ " + Math.Round(BTBIssuedValue).ToString("#,##0");

            //model.BTBAvailableValue = "$ " + Math.Round(BTBLimitValue - BTBIssuedValue).ToString("#,##0");
            //model.BOMRestValue = "$ " + Math.Round(BOMValue - BTBIssuedValue).ToString("#,##0");
            //model.PORestValue = "$ " + Math.Round(POIssuedValue - BTBIssuedValue).ToString("#,##0");

            model.MasterLCValue = MasterLCValue;
            model.BOMValue = BOMValue;
            model.ExportValue = ActualPO;

            model.POIssuedValue = POIssuedValue;
            model.POPending = BOMValue - POIssuedValue;

            model.BTBLimitValue = BTBLimitValue;
            model.BTBIssuedValue = BTBIssuedValue;

            model.BTBAvailableValue = BTBLimitValue - BTBIssuedValue;
            model.BOMRestValue = BOMValue - BTBIssuedValue;
            model.PORestValue = POIssuedValue - BTBIssuedValue;

            this.SummaryReport = model;
        }
        public void GetBTBDetails()
        {
            db = new xOssContext();

            //var vData = (from mlc in db.Commercial_MasterLC
            //             join p in db.Commercial_UDSlave on mlc.ID equals p.Commercial_MasterLCFK
            //             join ud in db.Commercial_UD on p.Commercial_UDFK equals ud.ID
            //             where ud.Active == true && p.Active == true && mlc.Active == true
            //             join btblc in db.Commercial_B2bLC on ud.ID equals btblc.Commercial_UDFK
            //             join sup in db.Common_Supplier on btblc.Common_SupplierFK equals sup.ID
            //             where
            //             p.Active == true &&
            //             ud.Active == true &&
            //             btblc.Active == true &&
            //             sup.Active == true
            //             select new 
            //             {
            //                 MasterLC = mlc.Name,
            //                 MasterTotalValue = mlc.TotalValue,
            //                 BTBLimitValue = ((mlc.TotalValue * ud.BTBOpening) / 100),
            //                 BTBName = btblc.Name,
            //                 BTBDate=btblc.Date,
            //                 Supplier = sup.Name,
            //                 BTBIssued = btblc.Amount
            //             }).ToList();
            //List<SummaryReport> listBtb = new List<SummaryReport>();
            //if (vData.Any())
            //{
            //    foreach (var v in vData)
            //    {
            //        SummaryReport item = new SummaryReport();
            //        item.MasterLCNo = v.MasterLC;
            //        item.BOMValue = v.MasterTotalValue;
            //        item.BTBLimitValue = v.BTBLimitValue;
            //        item.BTBAvailableValue = v.BTBName;
            //        item.PORestValue = v.BTBDate.ToShortDateString();
            //        item.POIssuedValue = v.Supplier;
            //        item.BTBIssuedValue = v.BTBIssued.ToString();
            //        listBtb.Add(item);
            //    }
            //}
            
            this.BTBList = null;

        }
    }
}