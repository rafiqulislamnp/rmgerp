﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_MLcSupplierPO
    {
        private xOssContext db;
        public Mkt_PO Mkt_PO { get; set; }
        public Commercial_MLcSupplierPO Commercial_MLcSupplierPO { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public IEnumerable<VmCommercial_MLcSupplierPO> DataList { get; set; }
        public List<object> DropDownList { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public int Add(int userID)
        {
            Commercial_MLcSupplierPO.AddReady(userID);
            return Commercial_MLcSupplierPO.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_MLcSupplierPO.Edit(userID);
        }
        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_MLcSupplierPO in db.Commercial_MLcSupplierPO
                     join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_MLcSupplierPO.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Mkt_PO in db.Mkt_PO on
                     Commercial_MLcSupplierPO.Mkt_POFK equals Mkt_PO.ID
                     select new VmCommercial_MLcSupplierPO
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_PO = Mkt_PO,
                         Commercial_MLcSupplierPO = Commercial_MLcSupplierPO

                     }).Where(x => x.Commercial_MLcSupplierPO.Active == true && x.Commercial_MasterLC.ID == id)
                       .OrderByDescending(x => x.Commercial_MLcSupplierPO.ID).AsEnumerable();
            this.DataList = a;
        }
        public VmCommercial_MLcSupplierPO SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID

                     select new VmCommercial_MLcSupplierPO
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer
                     }).Where(x=>x.Commercial_MasterLC.ID == iD).FirstOrDefault();
            return v;
        }
        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_MLcSupplierPO in db.Commercial_MLcSupplierPO
                     join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_MLcSupplierPO.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Mkt_PO in db.Mkt_PO on Commercial_MLcSupplierPO.Mkt_POFK equals Mkt_PO.ID
                     select new VmCommercial_MLcSupplierPO
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_PO = Mkt_PO,
                         Commercial_MLcSupplierPO = Commercial_MLcSupplierPO
                     }).Where(c => c.Commercial_MLcSupplierPO.ID == iD).SingleOrDefault();
            this.Commercial_MLcSupplierPO = v.Commercial_MLcSupplierPO;
            this.Commercial_MasterLC = v.Commercial_MasterLC;
            this.Mkt_PO = v.Mkt_PO;
        }



    }
}