﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_LCOregin
    {
        private xOssContext db;
        public Commercial_LCOregin Commercial_LCOregin { get; set; }
        public IEnumerable<VmCommercial_LCOregin> DataList { get; set; }


        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_LCOregin
                     select new VmCommercial_LCOregin
                     {
                         Commercial_LCOregin = t1

                     }).Where(x => x.Commercial_LCOregin.Active == true).OrderByDescending(x => x.Commercial_LCOregin.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_LCOregin

                     select new VmCommercial_LCOregin
                     {
                         Commercial_LCOregin = t1

                     }).Where(c => c.Commercial_LCOregin.ID == id).SingleOrDefault();
            this.Commercial_LCOregin = v.Commercial_LCOregin;

        }

        public int Add(int userID)
        {
            Commercial_LCOregin.AddReady(userID);

            return Commercial_LCOregin.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_LCOregin.Edit(userID);
        }


    }
}