﻿
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_Import
    {
        private xOssContext db;
        public Commercial_Import Commercial_Import { get; set; }
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Acc_POPH Acc_POPH { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Common_Unit Shipment_Unit { get; set; }
        public IEnumerable<VmCommercial_Import> DataList { get; set; }
        
        public VmCommercial_Import SelectSingleJoined(int Id)
        {
            using (db = new xOssContext())
            {
                var vData = (from t1 in db.Commercial_B2bLC
                        where t1.Active == true && t1.ID == Id
                        join t2 in db.Commercial_UDSlave on t1.Commercial_UDFK equals t2.Commercial_UDFK
                        join t3 in db.Commercial_MasterLC on t2.Commercial_MasterLCFK equals t3.ID
                        where t3.Active == true
                        select new VmCommercial_Import
                        {
                            Commercial_B2bLC = t1,
                            Commercial_MasterLC = t3
                        }).FirstOrDefault();

                return vData;
            }  
        }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_Import in db.Commercial_Import
                     join Commercial_B2bLC in db.Commercial_B2bLC
                     on Commercial_Import.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     join Common_Unit in db.Common_Unit on Commercial_Import.Common_UnitFK equals Common_Unit.ID
                     join ShipmeUnit in db.Common_Unit on Commercial_Import.Common_UnitFKForShipment equals ShipmeUnit.ID
                    
                     where Common_Unit.Active==true
                     select new VmCommercial_Import
                     {
                         Commercial_Import = Commercial_Import,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Common_Unit= Common_Unit,
                         Shipment_Unit = ShipmeUnit

                     }).Where(x => x.Commercial_Import.Active == true && x.Commercial_B2bLC.ID == id)
                    .OrderByDescending(x => x.Commercial_Import.ID).AsEnumerable();
                    this.DataList = a;
        }
        
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Commercial_Import in db.Commercial_Import
                     join Commercial_B2bLC in db.Commercial_B2bLC
                     on Commercial_Import.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     //join Commercial_MasterLC in db.Commercial_MasterLC
                     //on Commercial_B2bLC.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     //join Common_Supplier in db.Common_Supplier
                     //on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID


                     select new VmCommercial_Import
                     {
                         Commercial_Import = Commercial_Import,
                         Commercial_B2bLC = Commercial_B2bLC
                         //Commercial_MasterLC = Commercial_MasterLC,
                         //Common_Supplier = Common_Supplier

                     }).Where(c => c.Commercial_Import.ID == id).SingleOrDefault();
            this.Commercial_Import = v.Commercial_Import;
            this.Commercial_B2bLC = v.Commercial_B2bLC;
            //this.Commercial_MasterLC = v.Commercial_MasterLC;
            //this.Common_Supplier = v.Common_Supplier;

        }

        public int Add(int userID)
        {
            Commercial_Import.AddReady(userID);
            return Commercial_Import.Add();
        }

        public bool Edit(int userID)
        {
            return Commercial_Import.Edit(userID);
        }
        
    }
}