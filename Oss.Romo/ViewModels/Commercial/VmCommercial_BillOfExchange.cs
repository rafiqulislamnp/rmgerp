﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_BillOfExchange
    {
        private xOssContext db;
        public IEnumerable<VmCommercial_BillOfExchange> DataList { get; set; }
        public IEnumerable<VmCommercial_BillOfExchange> DataList1 { get; set; }
        public IEnumerable<VmCommercial_BillOfExchange> DataList2 { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Commercial_BillOfExchange Commercial_BillOfExchange { get; set; }
        public Commercial_Invoice Commercial_Invoice { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public User_User User_User { get; set; }
        public VmCommercial_UDSlave VmCommercial_UDSlave { get; set; }
        public Commercial_Bank Buyer_Bank { get; set; }
        public Commercial_Bank Lien_Bank { get; set; }
        public Common_Country Common_Country { get; set; }
        public Commercial_UDSlave Commercial_UDSlave { get; set; }
        public Commercial_BillOfExchangeSlave Commercial_BillOfExchangeSlave { get; set; }

        public List<Commercial_BillOfExchangeSlaveViewModel> BillOfExchangeSlaveList { get; set; }
        public List<ReportDocType> BillOfExchangeReportDoc { get; set; }
        public VmCommercial_MasterLCBuyerPO VmCommercial_MasterLCBuyerPO { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
        public decimal BillValue { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_BillOfExchange
                     join t2 in db.Commercial_MasterLC on t1.Commercial_MasterLCFK equals t2.ID
                     join t3 in db.Commercial_UDSlave on t2.ID equals t3.Commercial_MasterLCFK
                     join t6 in db.Commercial_UD on t3.Commercial_UDFK equals t6.ID
                     join t4 in db.Commercial_Bank on t2.Commercial_BuyerBankFK equals t4.ID
                     join t5 in db.Commercial_Bank on t2.Commercial_LienBankFK equals t5.ID
                 
              
                     where t1.Active == true
                     select new VmCommercial_BillOfExchange
                     {
                         Commercial_BillOfExchange = t1,
                         Commercial_MasterLC = t2,
                         Commercial_UD = t6,
                         Commercial_UDSlave = t3,
                         Buyer_Bank = t4,
                         Lien_Bank = t5,
                        
                         BillOfExchangeSlaveList=(from t7 in db.Commercial_BillOfExchangeSlave
                                                  join t8 in db.Commercial_Invoice on t7.Commercial_InvoiceFK equals t8.ID
                                                  //join t9 in db.Common_Country on t8.CommonCountryFk equals t9.ID
                                                  where t7.Commercial_BillOfExchangeFk==t1.ID && t7.Active==true
                                                  select new Commercial_BillOfExchangeSlaveViewModel
                                                  {
                                                      Slaves = t7,
                                                      InvoiceNo=t8.InvoiceNo,
                                                      InvoiceId = t8.ID
                                                  }).ToList()
                     }).OrderByDescending(x => x.Commercial_BillOfExchange.ID).ToList();

            List<VmCommercial_BillOfExchange> list = new List<VmCommercial_BillOfExchange>();
            
            foreach (VmCommercial_BillOfExchange x in a)
            {
                decimal ttl = 0;
                VmCommercial_BillOfExchangeSlave vmCommercial_BillOfExchangeSlave = new VmCommercial_BillOfExchangeSlave();
                foreach (var data in x.BillOfExchangeSlaveList)
                {
                    ttl += vmCommercial_BillOfExchangeSlave.GetInvoiceValue(data.Slaves.Commercial_InvoiceFK);
                }
                x.BillValue = ttl;
                list.Add(x);
            }
            this.DataList = list;

        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Commercial_BillOfExchange in db.Commercial_BillOfExchange
                     join Commercial_MasterLC in db.Commercial_MasterLC
                     on Commercial_BillOfExchange.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     //join Commercial_Invoice in db.Commercial_Invoice
                     //on Commercial_BillOfExchange.Commercial_InvoiceFk equals Commercial_Invoice.ID
                     select new VmCommercial_BillOfExchange
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Commercial_BillOfExchange = Commercial_BillOfExchange,
                         //Commercial_Invoice = Commercial_Invoice
                     }).SingleOrDefault(c => c.Commercial_BillOfExchange.ID == id);
            this.Commercial_BillOfExchange = v.Commercial_BillOfExchange;
            this.Commercial_MasterLC = v.Commercial_MasterLC;

        }

        public VmCommercial_BillOfExchange SelectSingleJoined(int id)
        {

            db = new xOssContext();

            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join buyerBank in db.Commercial_Bank
                     on Commercial_MasterLC.Commercial_BuyerBankFK equals buyerBank.ID

                     join lienBank in db.Commercial_Bank
                     on Commercial_MasterLC.Commercial_LienBankFK equals lienBank.ID
                     select new VmCommercial_BillOfExchange
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer,
                         Buyer_Bank = buyerBank,
                         Lien_Bank = lienBank
                     }).FirstOrDefault(x => x.Commercial_MasterLC.ID == id);
            return v;
           }




        public int Add(int userID)
        {

            Commercial_BillOfExchange.AddReady(userID);
            return Commercial_BillOfExchange.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_BillOfExchange.Edit(userID);
        }
        internal void BillOfExchangeDocDataLoad(int id)
        {

            db = new xOssContext();

            var a = (from t7 in db.Commercial_BillOfExchangeSlave
                     join t1 in db.Commercial_BillOfExchange
                     on t7.Commercial_BillOfExchangeFk equals t1.ID
                     join t2 in db.Commercial_Invoice
                     on t7.Commercial_InvoiceFK equals t2.ID
                     join t3 in db.Commercial_InvoiceSlave
                     on t2.ID equals t3.Commercial_InvoiceFk
                     join t4 in db.Commercial_MasterLC
                     on t1.Commercial_MasterLCFK equals t4.ID
                     //join t8 in db.Mkt_BOM
                     //on t3.Mkt_BOMFK equals t8.ID
                     join t10 in db.Commercial_Bank
                      on t4.Commercial_BuyerBankFK equals t10.ID
                     join t9 in db.Commercial_Bank
                     on t4.Commercial_LienBankFK equals t9.ID
                     where t1.ID == id && t7.Active == true && t1.Active == true && t3.Active == true
                     select new ReportDocType
                     {

                         //Body1 = t1.d,  //As 30 day's sight..........
                         Body2 = t9.Name +", "+ t9.Address,
                         Body3 = t10.Name + ", " + t10.Address,
                         Body4 = t10.Name + ", " + t10.Address,
                         Body5 = t4.Name, //Master L/C No
                         Body6 = t4.Date.ToString(),
                         Body7 = t2.InvoiceNo,
                         Body8 = t2.ID.ToString()
                     }).ToList();

            decimal TTL = 0;
            foreach (ReportDocType v in a)
            {
                decimal body11 = 0;
                decimal.TryParse(v.Body11, out body11);
                TTL += body11;

            }
            foreach (ReportDocType r in a) 
            {
                r.SubBody1 = r.Body13 + TTL.ToString("F");
                decimal body10 = 0;
                decimal.TryParse(r.Body10, out body10);
                r.Body10 = body10.ToString("F");
                decimal body11 = 0;
                decimal.TryParse(r.Body11, out body11);

                r.Body11 = body11.ToString("F");

                r.Body6 = r.Body6.Substring(0, r.Body6.Length - 8);
                r.Body12 = r.Body12.Substring(0, r.Body12.Length - 8);

                Common.VmForDropDown ntw = new Common.VmForDropDown();

                r.SubBody2 = ntw.NumberToWords(TTL, Common.CurrencyType.USD) + ".";
            }
                this.BillOfExchangeReportDoc = a.ToList();
        }

        public void RealizedBillOfExchange()
        {
            db = new xOssContext();
            var a = (from t9 in db.Acc_ExportRealisation
                     join t1 in db.Commercial_BillOfExchange on t9.Shipment_BillOfExchangeFK equals t1.ID
                     join t2 in db.Commercial_MasterLC on t1.Commercial_MasterLCFK equals t2.ID
                     join t3 in db.Commercial_UDSlave on t2.ID equals t3.Commercial_MasterLCFK
                     join t6 in db.Commercial_UD on t3.Commercial_UDFK equals t6.ID
                     join t4 in db.Commercial_Bank on t2.Commercial_BuyerBankFK equals t4.ID
                     join t5 in db.Commercial_Bank on t2.Commercial_LienBankFK equals t5.ID
                     where t1.Active == true
                     select new VmCommercial_BillOfExchange
                     {
                         Commercial_BillOfExchange = t1,
                         Commercial_MasterLC = t2,
                         Commercial_UD = t6,
                         Commercial_UDSlave = t3,
                         Buyer_Bank = t4,
                         Lien_Bank = t5,
                         BillOfExchangeSlaveList = (from t7 in db.Commercial_BillOfExchangeSlave
                                                    join t8 in db.Commercial_Invoice on t7.Commercial_InvoiceFK equals t8.ID
                                                    where t7.Commercial_BillOfExchangeFk == t1.ID && t7.Active == true
                                                    select new Commercial_BillOfExchangeSlaveViewModel
                                                    {
                                                        Slaves = t7,
                                                        InvoiceNo = t8.InvoiceNo
                                                    }).ToList()
                     }).OrderByDescending(x => x.Commercial_BillOfExchange.ID).ToList();

            List<VmCommercial_BillOfExchange> list = new List<VmCommercial_BillOfExchange>();
            

            foreach (VmCommercial_BillOfExchange x in a)
            {
                decimal ttl = 0;
                VmCommercial_BillOfExchangeSlave vmCommercial_BillOfExchangeSlave = new VmCommercial_BillOfExchangeSlave();
                if (list.All(p => p.Commercial_BillOfExchange.ID != x.Commercial_BillOfExchange.ID))
                {
                    foreach (var data in x.BillOfExchangeSlaveList)
                    {
                        ttl += vmCommercial_BillOfExchangeSlave.GetInvoiceValue(data.Slaves.Commercial_InvoiceFK);
                    }
                    x.BillValue = ttl;
                    list.Add(x);
                    
                }
               
            }
            this.DataList1 = list;
        }

        public void UnRealizedBillOfExchange()
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_BillOfExchange
                     join t2 in db.Commercial_MasterLC on t1.Commercial_MasterLCFK equals t2.ID
                     join t3 in db.Commercial_UDSlave on t2.ID equals t3.Commercial_MasterLCFK
                     join t6 in db.Commercial_UD on t3.Commercial_UDFK equals t6.ID
                     join t4 in db.Commercial_Bank on t2.Commercial_BuyerBankFK equals t4.ID
                     join t5 in db.Commercial_Bank on t2.Commercial_LienBankFK equals t5.ID
                     where t1.Active == true
                     select new VmCommercial_BillOfExchange
                     {
                         Commercial_BillOfExchange = t1,
                         Commercial_MasterLC = t2,
                         Commercial_UD = t6,
                         Commercial_UDSlave = t3,
                         Buyer_Bank = t4,
                         Lien_Bank = t5,
                         BillOfExchangeSlaveList = (from t7 in db.Commercial_BillOfExchangeSlave
                                                    join t8 in db.Commercial_Invoice on t7.Commercial_InvoiceFK equals t8.ID
                                                    where t7.Commercial_BillOfExchangeFk == t1.ID && t7.Active == true
                                                    select new Commercial_BillOfExchangeSlaveViewModel
                                                    {
                                                        Slaves = t7,
                                                        InvoiceNo = t8.InvoiceNo
                                                    }).ToList()
                     }).OrderByDescending(x => x.Commercial_BillOfExchange.ID).ToList();

            List<VmCommercial_BillOfExchange> allBillOfExchangeslist = new List<VmCommercial_BillOfExchange>();

            foreach (VmCommercial_BillOfExchange x in a)
            {
                decimal ttl = 0;
                VmCommercial_BillOfExchangeSlave vmCommercial_BillOfExchangeSlave = new VmCommercial_BillOfExchangeSlave();
                foreach (var data in x.BillOfExchangeSlaveList)
                {
                    ttl += vmCommercial_BillOfExchangeSlave.GetInvoiceValue(data.Slaves.Commercial_InvoiceFK);
                }
                x.BillValue = ttl;
                allBillOfExchangeslist.Add(x);
            }
            var b = (from t9 in db.Acc_ExportRealisation
                     join t1 in db.Commercial_BillOfExchange on t9.Shipment_BillOfExchangeFK equals t1.ID
                     join t2 in db.Commercial_MasterLC on t1.Commercial_MasterLCFK equals t2.ID
                     join t3 in db.Commercial_UDSlave on t2.ID equals t3.Commercial_MasterLCFK
                     join t6 in db.Commercial_UD on t3.Commercial_UDFK equals t6.ID
                     join t4 in db.Commercial_Bank on t2.Commercial_BuyerBankFK equals t4.ID
                     join t5 in db.Commercial_Bank on t2.Commercial_LienBankFK equals t5.ID
                     where t1.Active == true
                     select new VmCommercial_BillOfExchange
                     {
                         Commercial_BillOfExchange = t1,
                         Commercial_MasterLC = t2,
                         Commercial_UD = t6,
                         Commercial_UDSlave = t3,
                         Buyer_Bank = t4,
                         Lien_Bank = t5,
                         BillOfExchangeSlaveList = (from t7 in db.Commercial_BillOfExchangeSlave
                                                    join t8 in db.Commercial_Invoice on t7.Commercial_InvoiceFK equals t8.ID
                                                    where t7.Commercial_BillOfExchangeFk == t1.ID && t7.Active == true
                                                    select new Commercial_BillOfExchangeSlaveViewModel
                                                    {
                                                        Slaves = t7,
                                                        InvoiceNo = t8.InvoiceNo
                                                    }).ToList()
                     }).OrderByDescending(x => x.Commercial_BillOfExchange.ID).ToList();

            List<VmCommercial_BillOfExchange> realizedBillOfExchangeslist = new List<VmCommercial_BillOfExchange>();

            foreach (VmCommercial_BillOfExchange x in b)
            {
                decimal ttl = 0;
                VmCommercial_BillOfExchangeSlave vmCommercial_BillOfExchangeSlave = new VmCommercial_BillOfExchangeSlave();
                foreach (var data1 in x.BillOfExchangeSlaveList)
                {
                    ttl += vmCommercial_BillOfExchangeSlave.GetInvoiceValue(data1.Slaves.Commercial_InvoiceFK);
                }
                x.BillValue = ttl;
                realizedBillOfExchangeslist.Add(x);
            }
            var unrealizedBillOfExchangeslist = allBillOfExchangeslist.Where(x =>
                realizedBillOfExchangeslist.All(y => y.Commercial_BillOfExchange.ID != x.Commercial_BillOfExchange.ID)).ToList();
            this.DataList2 = unrealizedBillOfExchangeslist;
        }
    }
    
    public class Commercial_BillOfExchangeSlaveViewModel
    {
        public Commercial_BillOfExchangeSlave Slaves { get; set; }
        public string InvoiceNo { get; set; }
        public int InvoiceId { get; set; }
    }
}