﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_PI
    {
        private xOssContext db;
        public Commercial_PI Commercial_PI { get; set; }
        public IEnumerable<VmCommercial_PI> DataList { get; set; }
        public List<object> DropDownList { get; set; }
        public HttpPostedFileBase ScanedFile { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Commercial_PI in db.Commercial_PI
                     select new VmCommercial_PI
                     {
                         Commercial_PI = Commercial_PI
                     }).Where(x => x.Commercial_PI.Active == true).OrderByDescending(x => x.Commercial_PI.ID).AsEnumerable();
            this.DataList = a;

        }

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_PI in db.Commercial_PI

                     select new VmCommercial_PI
                     {
                         Commercial_PI = Commercial_PI
                     }).Where(c => c.Commercial_PI.ID == iD).SingleOrDefault();
            this.Commercial_PI = v.Commercial_PI;
           
        }
        public string Upload(string imagePath)
        {
            string s = "";
            //Photo.FileName = imagePath;
            try
            {
                string exten = Path.GetFileName(ScanedFile.FileName);
                string fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                string filePath = Path.Combine(imagePath + fName);
                ScanedFile.SaveAs(filePath);
                Commercial_PI.ScanedFile = fName;

            }
            catch (Exception ex)
            {

                s = ex.Message;

            }
            return s;

        }



        //public int Add(int userID)
        //{
        //    Commercial_PI.AddReady(userID);
        //    return Commercial_PI.Add();

        //}
        //public bool Edit(int userID)
        //{
        //    return Commercial_PI.Edit(userID);
        //}

        public int Add(int userID, string imagePath)
        {
            Commercial_PI.AddReady(userID);
            Upload(imagePath);
            return Commercial_PI.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_PI.Edit(userID);
        }
        public bool EditUser(int userID, string imagePath)
        {
            Commercial_PI.Edit(0);
            Upload(imagePath);
            return Commercial_PI.Edit(userID);
        }
    }
}