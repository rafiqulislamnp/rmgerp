﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_Bank
    {
        private xOssContext db;

        public Commercial_Bank Commercial_Bank { get; set; }
        public Common_Country Common_Country { get; set; }
        public IEnumerable<VmCommercial_Bank> DataList { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Commercial_Bank in db.Commercial_Bank
                     join Common_Country in db.Common_Country on Commercial_Bank.Common_CountryFK equals Common_Country.ID
                     select new VmCommercial_Bank
                     {
                         Common_Country=Common_Country,
                         Commercial_Bank = Commercial_Bank
                     }).Where(x => x.Commercial_Bank.Active == true).OrderByDescending(x => x.Commercial_Bank.ID).AsEnumerable();
            this.DataList = a;

        }

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_Bank in db.Commercial_Bank
                     join Common_Country in db.Common_Country on Commercial_Bank.Common_CountryFK equals Common_Country.ID
                     select new VmCommercial_Bank
                     {
                         Common_Country = Common_Country,
                         Commercial_Bank = Commercial_Bank
                     }).Where(c => c.Commercial_Bank.ID == iD).SingleOrDefault();
            this.Commercial_Bank = v.Commercial_Bank;

        }
        public int Add(int userID)
        {
            Commercial_Bank.AddReady(userID);
            return Commercial_Bank.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_Bank.Edit(userID);
        }

    }
}