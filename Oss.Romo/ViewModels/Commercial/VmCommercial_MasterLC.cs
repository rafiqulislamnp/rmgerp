﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Oss.Romo.ViewModels.Reports;
using System.ComponentModel;
using System.Data.Entity;
using System.Globalization;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_MasterLC
    {

        private xOssContext db;
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        //public Commercial_Bank Commercial_Bank { get; set; }
        public Commercial_Bank Buyer_Bank { get; set; }
        public Commercial_LCOregin Commercial_LCOregin { get; set; }
        public Commercial_LCType Commercial_LCType { get; set; }
        public Commercial_Bank Lien_Bank { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public IEnumerable<VmCommercial_MasterLC> DataList { get; set; }
        public List<object> DropDownList { get; set; }
        public VmCommercial_MasterLCBuyerPO VmCommercial_MasterLCBuyerPO { get; set; }
        public List<ReportDocType> LCStetmentDoc1 { get; set; }
        public List<ReportDocType> LCStetmentDoc2 { get; set; }
        public List<ReportDocType> LCStetmentDoc3 { get; set; }
        public HttpPostedFileBase ScanedFile { get; set; }
        public VmCommercial_UDSlave VmCommercial_UDSlave { get; set; }
        [DefaultValue(0)]
        [DisplayName("Amendment Increase")]
        public decimal AmendmentIncrease { get; set; }
        [DefaultValue(0)]
        [DisplayName("Amendment Decrease")]
        public decimal AmendmentDecrease { get; set; }
        public decimal? ActualValueForBTB { get; set; }

        public int MLCId { get; set; }

        public decimal TotalValue { get; set; }

        string totalBTB = "";
        string availbleBTB = "";

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join buyer_Bank in db.Commercial_Bank on Commercial_MasterLC.Commercial_BuyerBankFK equals buyer_Bank.ID
                     join lien_Bank in db.Commercial_Bank on Commercial_MasterLC.Commercial_LienBankFK equals lien_Bank.ID

                     select new VmCommercial_MasterLC
                     {
                         Mkt_Buyer = Mkt_Buyer,
                         Buyer_Bank = buyer_Bank,
                         Lien_Bank = lien_Bank,
                         Commercial_MasterLC = Commercial_MasterLC,

                     }).Where(x => x.Commercial_MasterLC.Active == true).OrderByDescending(x => x.Commercial_MasterLC.ID).AsEnumerable();
            this.DataList = a;

        }
        public void InitialPeriodicDataLoad(DateTime FDate, DateTime TDate)
        {
            db = new xOssContext();
            var a = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join buyer_Bank in db.Commercial_Bank on Commercial_MasterLC.Commercial_BuyerBankFK equals buyer_Bank.ID
                     join lien_Bank in db.Commercial_Bank on Commercial_MasterLC.Commercial_LienBankFK equals lien_Bank.ID

                     select new VmCommercial_MasterLC
                     {
                         Mkt_Buyer = Mkt_Buyer,
                         Buyer_Bank = buyer_Bank,
                         Lien_Bank = lien_Bank,
                         Commercial_MasterLC = Commercial_MasterLC,

                     }).Where(x => x.Commercial_MasterLC.Active == true && x.Commercial_MasterLC.Date >= FDate && x.Commercial_MasterLC.Date <= TDate).OrderByDescending(x => x.Commercial_MasterLC.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join buyer_Bank in db.Commercial_Bank on Commercial_MasterLC.Commercial_BuyerBankFK equals buyer_Bank.ID
                     join lien_Bank in db.Commercial_Bank on Commercial_MasterLC.Commercial_LienBankFK equals lien_Bank.ID

                     select new VmCommercial_MasterLC
                     {
                         Mkt_Buyer = Mkt_Buyer,
                         Buyer_Bank = buyer_Bank,
                         Lien_Bank = lien_Bank,
                         Commercial_MasterLC = Commercial_MasterLC
                     }).Where(c => c.Commercial_MasterLC.ID == iD).SingleOrDefault();
            this.Commercial_MasterLC = v.Commercial_MasterLC;
            this.Mkt_Buyer = v.Mkt_Buyer;
        }

        public string Upload(string imagePath)
        {
            string s = "";
            //Photo.FileName = imagePath;
            try
            {
                string exten = Path.GetFileName(ScanedFile.FileName);
                string fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                string filePath = Path.Combine(imagePath + fName);
                ScanedFile.SaveAs(filePath);
                Commercial_MasterLC.ScanedFile = fName;

            }
            catch (Exception ex)
            {

                s = ex.Message;

            }
            return s;

        }

        public int Add(int userID, string imagePath)
        {
            Commercial_MasterLC.AddReady(userID);
            Upload(imagePath);
            return Commercial_MasterLC.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_MasterLC.Edit(userID);
        }

        public bool EditMasterLC(int userID, string imagePath)
        {
            if (Commercial_MasterLC.AmendmentIncrease == 0)
            {
                Commercial_MasterLC.AmendmentIncrease = this.AmendmentIncrease;
                Commercial_MasterLC.TotalValue = this.Commercial_MasterLC.TotalValue + this.AmendmentIncrease;
            }
            else if (Commercial_MasterLC.AmendmentIncrease != 0)
            {
                Commercial_MasterLC.AmendmentIncrease = this.Commercial_MasterLC.AmendmentIncrease + this.AmendmentIncrease;
                Commercial_MasterLC.TotalValue = this.Commercial_MasterLC.TotalValue + this.AmendmentIncrease;
            }

            if (Commercial_MasterLC.AmendmentDecrease == 0)
            {
                Commercial_MasterLC.AmendmentDecrease = this.AmendmentDecrease;
                Commercial_MasterLC.TotalValue = this.Commercial_MasterLC.TotalValue - this.AmendmentDecrease;
            }
            else if (Commercial_MasterLC.AmendmentDecrease != 0)
            {
                Commercial_MasterLC.AmendmentDecrease = this.Commercial_MasterLC.AmendmentDecrease + this.AmendmentDecrease;
                Commercial_MasterLC.TotalValue = this.Commercial_MasterLC.TotalValue - this.AmendmentDecrease;
            }


            Commercial_MasterLC.Edit(userID);
            Upload(imagePath);
            return Commercial_MasterLC.Edit(userID);
        }

        //Comment out deu to wrong PO Information (always single) along with some value

        //internal List<ReportDocType> LCStatementDocDataLoad(int udID)
        //{
        //    db = new xOssContext();

        //    var a = (from t1 in db.Commercial_UDSlave
        //             join t2 in db.Commercial_UD on t1.Commercial_UDFK equals t2.ID
        //             join t3 in db.Commercial_MasterLC on t1.Commercial_MasterLCFK equals t3.ID
        //             where t2.ID == udID
        //             && t1.Active == true
        //             && t2.Active == true
        //             && t3.Active == true
        //             select new
        //             {
        //                 int2 = t3.ID, //MLC
        //                 int3 = t2.ID, //UD

        //                 B2BDetails = (from t4 in db.Commercial_MasterLCBuyerPO
        //                               join t5 in db.Mkt_BOM on t4.MKTBOMFK equals t5.ID
        //                               join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
        //                               join t7 in db.Mkt_Buyer on t6.Mkt_BuyerFK equals t7.ID
        //                               join t8 in db.Mkt_Item on t5.Mkt_ItemFK equals t8.ID
        //                               join t9 in db.Common_Currency on t6.Common_CurrencyFK equals t9.ID
        //                               where
        //                               t4.Commercial_MasterLCFK == t3.ID
        //                              && t4.Active == true
        //                              && t5.Active == true
        //                              && t6.Active == true
        //                              && t7.Active == true
        //                              && t8.Active == true
        //                              && t9.Active == true
        //                               select new
        //                               {
        //                                   int1 = t5.ID, //BOM ID
        //                                   HeaderLeft1 = t6.Agent, //buying house
        //                                   HeaderLeft2 = t7.Name, //Buyer Name
        //                                   HeaderMiddle1 = t6.BuyerPO, //Buyer PO NO         // t4.CID + "/" + t3.Style,
        //                                   HeaderMiddle2 = t8.Name, // Item Name
        //                                   HeaderMiddle3 = t5.QPack.ToString(),  //QTY Pack
        //                                   HeaderMiddle4 = (t5.QPack * t5.Quantity).ToString(),  // QTY PCS
        //                                   HeaderMiddle5 = (t5.QPack * t5.Quantity * t5.UnitPrice).ToString(), //Amount
        //                                   HeaderMiddle6 = t5.UnitPrice.ToString(), // ((t5.QPack * t5.Quantity * t5.UnitPrice) / 12).ToString(), //Doz Rate
        //                                   HeaderMiddle7 = (((t5.QPack * t5.Quantity * t5.UnitPrice) / 100) * t6.Commission).ToString(), //Buyer Commission 
        //                                   HeaderRight1 = t6.Commission.ToString(),
        //                                   HeaderRight5 = ((t5.QPack * t5.Quantity * t5.UnitPrice) + (((t5.QPack * t5.Quantity * t5.UnitPrice) / 100) * t2.BTBOpening)).ToString(),
        //                                   Body13 = t9.Symbol
        //                               }).ToList(),


        //                 HeaderLeft3 = t2.UdNo, // UD NO.
        //                 HeaderLeft4 = t2.Date.ToString(), // UD Date
        //                 HeaderLeft5 = t3.Name, // Master Lc NO
        //                 HeaderLeft6 = t3.Date.ToString(),  // Master Lc Date
        //                 HeaderLeft7 = "",//t3.BuyerTolerance.ToString(), //Tolerance
        //                 HeaderLeft8 = "",// ((((t5.QPack * t5.Quantity * t5.UnitPrice) / 100) * t3.BuyerTolerance) + (t5.QPack * t5.Quantity * t5.UnitPrice)).ToString(), //Tolerance Value

        //                 HeaderMiddle8 = t3.TotalValue.ToString(),

        //                 HeaderRight2 = "",
        //                 HeaderRight3 = "",
        //                 HeaderRight4 = t2.BTBOpening.ToString(), //BTB Opening.

        //                 HeaderRight6 = t3.ExpiryDate.ToString(),
        //                 HeaderRight7 = "",
        //             }).ToList();

        //    this.ActualValueForBTB = 0;
        //    decimal TTLQPack = 0;
        //    decimal TTLQPCS = 0;
        //    decimal TTLAmount = 0;
        //    decimal TTLDozRate = 0;
        //    decimal? TTLFreightCharge = 0;
        //    decimal? TTLHandlingCharge = 0;
        //    decimal? TTLLCValue = 0;
        //    decimal? totalBuyerCommission = 0;
        //    decimal? actualValue = 0;
        //    decimal? totalActualValue = 0;
        //    decimal? charge = 0;
        //    decimal? actual = 0;
        //    decimal germentsTest = 0;
        //    decimal? GroupPOValue = 0;
        //    decimal bTBOpening = 0;
        //    decimal? buyerTolerence = 0;
        //    List<ReportDocType> lstItem = new List<ReportDocType>();

        //    TTLFreightCharge = GetFreightCharge(udID);  // headerRight2;
        //    TTLHandlingCharge = GetHandlingCharge(udID);
        //    TTLLCValue = GetTotalLcValue(udID);
        //    buyerTolerence = GetBuyerTolerance(udID);
        //    charge = germentsTest + TTLFreightCharge + TTLHandlingCharge + totalBuyerCommission;

        //    foreach (var v in a)
        //    {
        //        ReportDocType p = new ReportDocType();
        //        p.HeaderLeft3 = v.HeaderLeft3; // UD NO.
        //        p.HeaderLeft4 = v.HeaderLeft4; // UD Date
        //        p.HeaderLeft5 = v.HeaderLeft5; // Master Lc NO
        //        p.HeaderLeft6 = v.HeaderLeft6; // Master Lc Date
        //        p.HeaderLeft7 = v.HeaderLeft7; //t3.BuyerTolerance.ToString(), //Tolerance
        //        p.HeaderLeft8 = v.HeaderLeft8; // ((((t5.QPack * t5.Quantity * t5.UnitPrice) / 100) * t3.BuyerTolerance) + (t5.QPack * t5.Quantity * t5.UnitPrice)).ToString(), //Tolerance Value
        //        p.HeaderMiddle8 = v.HeaderMiddle8;
        //        p.HeaderRight2 = v.HeaderRight2;
        //        p.HeaderRight3 = v.HeaderRight3;
        //        p.HeaderRight4 = v.HeaderRight4; //BTB Opening.
        //        p.HeaderRight6 = v.HeaderRight6;
        //        p.HeaderRight7 = v.HeaderRight7;

        //        //decimal headerLeft8 = 0;
        //        //decimal.TryParse(v.HeaderLeft8, out headerLeft8);
        //        //if (headerLeft8 != 0)
        //        //{
        //        //    buyerTolerence += headerLeft8;

        //        //}
        //        #region B2BDetails
        //        if (v.B2BDetails.Any())
        //        {
        //            foreach (var v1 in v.B2BDetails)
        //            {
        //                if (v1.int1 != null)
        //                {
        //                    VmCommercial_UDSlave = new VmCommercial_UDSlave();
        //                    germentsTest += VmCommercial_UDSlave.GetBOMGarmantsTestValue(v.int2, v1.int1);

        //                    GroupPOValue = VmCommercial_UDSlave.GetGroupPoValue(v.int2);
        //                }

        //                p.HeaderLeft1 = v1.HeaderLeft1;     //buying house
        //                p.HeaderLeft2 = v1.HeaderLeft2;     //Buyer Name
        //                p.HeaderMiddle1 = v1.HeaderMiddle1; //Buyer PO NO         // t4.CID + "/" + t3.Style,
        //                p.HeaderMiddle2 = v1.HeaderMiddle2; // Item Name
        //                p.HeaderMiddle3 = v1.HeaderMiddle3; //QTY Pack
        //                p.HeaderMiddle4 = v1.HeaderMiddle4; // QTY PCS
        //                p.HeaderMiddle5 = v1.HeaderMiddle5; //Amount
        //                p.HeaderMiddle6 = v1.HeaderMiddle6; // ((t5.QPack * t5.Quantity * t5.UnitPrice) / 12).ToString(), //Doz Rate
        //                p.HeaderMiddle7 = v1.HeaderMiddle7; //Buyer Commission 
        //                p.HeaderRight1 = v1.HeaderRight1;
        //                p.HeaderRight5 = v1.HeaderRight5;
        //                p.Body13 = v1.Body13;

        //                decimal.TryParse(v.HeaderRight4, out bTBOpening);

        //                //charge = germentsTest + headerRight2 + headerRight3;
        //                // Total buyer commission of all order

        //                decimal headerMiddle7 = 0; // Buyer Commission
        //                decimal.TryParse(p.HeaderMiddle7, out headerMiddle7);

        //                totalBuyerCommission += headerMiddle7;

        //                decimal headerRight1 = 0;
        //                decimal.TryParse(p.HeaderRight1, out headerRight1);

        //                decimal headerMiddle5 = 0;
        //                decimal.TryParse(p.HeaderMiddle5, out headerMiddle5);
        //                p.HeaderMiddle5 = headerMiddle5.ToString("F");

        //                decimal headerMiddle6 = 0;
        //                decimal.TryParse(p.HeaderMiddle6, out headerMiddle6);
        //                p.HeaderMiddle6 = headerMiddle6.ToString("F");

        //                if (headerRight1 != 0)
        //                {
        //                    decimal HeaderMiddle7 = 0;
        //                    decimal.TryParse(p.HeaderMiddle7, out HeaderMiddle7);

        //                    actualValue = headerMiddle5 - HeaderMiddle7;
        //                    // Total value of all order those include in one master L/C.
        //                    totalActualValue += headerMiddle5 - HeaderMiddle7;
        //                }
        //                else
        //                {
        //                    actualValue = headerMiddle5;
        //                    totalActualValue += headerMiddle5;
        //                }

        //                int headerMiddle3 = 0;
        //                int.TryParse(p.HeaderMiddle3, out headerMiddle3);
        //                TTLQPack += headerMiddle3;

        //                int headerMiddle4 = 0;
        //                int.TryParse(p.HeaderMiddle4, out headerMiddle4);
        //                TTLQPCS += headerMiddle4;
        //                TTLAmount += headerMiddle5;
        //                TTLDozRate += headerMiddle6;
        //            }
        //        }
        //        #endregion

        //        //New Scope//

        //        p.SubBody1 = TTLQPack.ToString();
        //        p.SubBody2 = TTLQPCS.ToString();
        //        p.SubBody3 = p.Body13 + TTLAmount.ToString("F");
        //        p.SubBody4 = p.Body13 + TTLDozRate.ToString("F");

        //        p.HeaderLeft4 = p.HeaderLeft4.Substring(0, p.HeaderLeft4.Length - 8);
        //        p.HeaderLeft6 = p.HeaderLeft6.Substring(0, p.HeaderLeft6.Length - 8);
        //        p.HeaderRight6 = p.HeaderRight6.Substring(0, p.HeaderRight6.Length - 8);
        //        p.HeaderRight3 = p.Body13 + TTLHandlingCharge.ToString();
        //        p.SubBody5 = p.Body13 + TTLFreightCharge.ToString();
        //        p.HeaderRight7 = p.Body13 + TTLLCValue.ToString();
        //        if (buyerTolerence != null)
        //        {
        //            p.HeaderLeft8 = p.Body13 + buyerTolerence.Value.ToString("F");
        //        }
        //        if (totalBuyerCommission != null)
        //        {
        //            p.Footer1 = p.Body13 + totalBuyerCommission.Value.ToString("F");
        //        }

        //        if (GroupPOValue != null)
        //        {
        //            p.Footer5 = GroupPOValue.Value.ToString();
        //        }
        //        if (TTLAmount != null && charge != null)
        //        {
        //            actual = TTLAmount - charge;
        //            p.Footer2 = p.Body13 + actual.Value.ToString("F");
        //            this.ActualValueForBTB = (actual / 100) * bTBOpening;
        //            p.Footer3 = p.Body13 + this.ActualValueForBTB.Value.ToString("F");
        //            p.Footer4 = p.Body13 + germentsTest.ToString("F");
        //        }
        //        else
        //        {
        //            actual = TTLAmount;
        //            p.Footer2 = p.Body13 + actual.Value.ToString("F");
        //            this.ActualValueForBTB = (actual / 100) * bTBOpening;
        //            p.Footer3 = p.Body13 + this.ActualValueForBTB.Value.ToString("F");
        //            p.Footer4 = p.Body13 + germentsTest.ToString("F");
        //        }
        //        lstItem.Add(p);
        //    }


        //    this.LCStetmentDoc1 = lstItem;
        //    return lstItem;

        //}
        //From Mamun vai Old Backup 

        internal List<ReportDocType> LCStatementDocDataLoad(int udID)
        {

            db = new xOssContext();

            var a = (from t1 in db.Commercial_UDSlave
                     join t2 in db.Commercial_UD
                     on t1.Commercial_UDFK equals t2.ID
                     join t3 in db.Commercial_MasterLC
                     on t1.Commercial_MasterLCFK equals t3.ID
                     join t4 in db.Commercial_MasterLCBuyerPO
                     on t3.ID equals t4.Commercial_MasterLCFK
                     join t5 in db.Mkt_BOM
                     on t4.MKTBOMFK equals t5.ID
                     join t6 in db.Common_TheOrder
                     on t5.Common_TheOrderFk equals t6.ID
                     join t7 in db.Mkt_Buyer
                     on t6.Mkt_BuyerFK equals t7.ID
                     join t8 in db.Mkt_Item
                     on t5.Mkt_ItemFK equals t8.ID
                     join t9 in db.Common_Currency
                     on t6.Common_CurrencyFK equals t9.ID

                     where t2.ID == udID
                     && t1.Active == true
                     && t2.Active == true
                     && t3.Active == true
                     && t4.Active == true
                     && t5.Active == true
                     && t6.Active == true
                     && t7.Active == true
                     && t8.Active == true
                     && t9.Active == true

                     select new ReportDocType
                     {

                         int1 = t5.ID, //BOM ID
                         int2 = t3.ID, //MLC
                         int3 = t2.ID, //UD
                         Body1 = (from x in db.Commercial_B2bLC where x.Active == true && x.Commercial_UDFK == udID select x.Amount).DefaultIfEmpty(0).Sum().ToString(),
                         HeaderLeft1 = ": " + t6.Agent, //buying house
                         HeaderLeft2 = ": " + t7.Name, //Buyer Name
                         HeaderLeft3 = ": " + t2.UdNo, // UD NO.
                         HeaderLeft4 = t2.Date.ToString(), // UD Date
                         HeaderLeft5 = ": " + t3.Name, // Master Lc NO
                         HeaderLeft6 = t3.Date.ToString(),  // Master Lc Date
                         HeaderLeft7 = "",//t3.BuyerTolerance.ToString(), //Tolerance
                         HeaderLeft8 = "",// ((((t5.QPack * t5.Quantity * t5.UnitPrice) / 100) * t3.BuyerTolerance) + (t5.QPack * t5.Quantity * t5.UnitPrice)).ToString(), //Tolerance Value
                         HeaderMiddle1 = t6.BuyerPO, //Buyer PO NO         // t4.CID + "/" + t3.Style,
                         HeaderMiddle2 = t8.Name, // Item Name
                         HeaderMiddle3 = t5.QPack.ToString(),  //QTY Pack
                         HeaderMiddle4 = (t5.QPack * t5.Quantity).ToString(),  // QTY PCS
                         HeaderMiddle5 = (t5.QPack * t5.Quantity * t5.UnitPrice).ToString(), //Amount
                         HeaderMiddle6 = t5.UnitPrice.ToString(), // ((t5.QPack * t5.Quantity * t5.UnitPrice) / 12).ToString(), //Doz Rate
                         HeaderMiddle7 = (((t5.QPack * t5.Quantity * t5.UnitPrice) / 100) * t6.Commission).ToString(), //Buyer Commission 
                         HeaderMiddle8 = t3.TotalValue.ToString(),
                         HeaderRight1 = t6.Commission.ToString(),
                         HeaderRight2 = "",
                         HeaderRight3 = "",
                         HeaderRight4 = t2.BTBOpening.ToString(), //BTB Opening.
                         HeaderRight5 = ((t5.QPack * t5.Quantity * t5.UnitPrice) + (((t5.QPack * t5.Quantity * t5.UnitPrice) / 100) * t2.BTBOpening)).ToString(),
                         HeaderRight6 = t3.ExpiryDate.ToString(),
                         HeaderRight7 = "",
                         Body13 = t9.Symbol

                     }).ToList();
            decimal TTLQPack = 0;
            decimal TTLQPCS = 0;
            decimal TTLAmount = 0;
            decimal TTLDozRate = 0;
            decimal? TTLFreightCharge = 0;
            decimal? TTLHandlingCharge = 0;
            decimal? TTLLCValue = 0;
            decimal? totalBuyerCommission = 0;
            decimal? actualValue = 0;
            decimal? totalActualValue = 0;
            decimal? charge = 0;
            decimal? actual = 0;
            this.ActualValueForBTB = 0;
            decimal germentsTest = 0;
            decimal? GroupPOValue = 0;
            decimal bTBOpening = 0;
            decimal? buyerTolerence = 0;
            List<VmCommercial_MasterLC> temp = new List<VmCommercial_MasterLC>();
            foreach (ReportDocType v in a)
            {
                decimal t = 0;
                if (temp.Any(x => x.MLCId == v.int2))
                {

                    decimal.TryParse(v.HeaderMiddle5, out t);
                    var update = temp.FirstOrDefault(b => b.MLCId == v.int2);
                    update.TotalValue += t;
                }
                else
                {
                    decimal.TryParse(v.HeaderMiddle5, out t);
                    temp.Add(new VmCommercial_MasterLC { MLCId = v.int2, TotalValue = t });
                }


                //decimal headerLeft8 = 0;
                //decimal.TryParse(v.HeaderLeft8, out headerLeft8);
                //if (headerLeft8 != 0)
                //{
                //    buyerTolerence += headerLeft8;

                //}

                if (v.int1 != null)
                {
                    VmCommercial_UDSlave = new VmCommercial_UDSlave();
                    germentsTest += VmCommercial_UDSlave.GetBOMGarmantsTestValue(v.int2, v.int1);

                    //Sumaya
                    //GroupPOValue = VmCommercial_UDSlave.GetGroupPoValue(v.int2,v.int1);
                }

                decimal.TryParse(v.HeaderRight4, out bTBOpening);

                //charge = germentsTest + headerRight2 + headerRight3;
                // Total buyer commission of all order

                decimal headerMiddle7 = 0; // Buyer Commission
                decimal.TryParse(v.HeaderMiddle7, out headerMiddle7);

                totalBuyerCommission += headerMiddle7;

                decimal headerRight1 = 0;
                decimal.TryParse(v.HeaderRight1, out headerRight1);

                decimal headerMiddle5 = 0;
                decimal.TryParse(v.HeaderMiddle5, out headerMiddle5);
                v.HeaderMiddle5 = headerMiddle5.ToString("F");

                decimal headerMiddle6 = 0;
                decimal.TryParse(v.HeaderMiddle6, out headerMiddle6);
                v.HeaderMiddle6 = headerMiddle6.ToString("F");

                if (headerRight1 != 0)
                {

                    decimal HeaderMiddle7 = 0;
                    decimal.TryParse(v.HeaderMiddle7, out HeaderMiddle7);

                    actualValue = headerMiddle5 - HeaderMiddle7;
                    // Total value of all order those include in one master L/C.
                    totalActualValue += headerMiddle5 - HeaderMiddle7;

                }
                else
                {
                    actualValue = headerMiddle5;
                    totalActualValue += headerMiddle5;
                }

                int headerMiddle3 = 0;
                int.TryParse(v.HeaderMiddle3, out headerMiddle3);
                TTLQPack += headerMiddle3;

                int headerMiddle4 = 0;
                int.TryParse(v.HeaderMiddle4, out headerMiddle4);
                TTLQPCS += headerMiddle4;
                TTLAmount += headerMiddle5;
                TTLDozRate += headerMiddle6;


            }
            TTLFreightCharge = GetFreightCharge(udID);  // headerRight2;
            TTLHandlingCharge = GetHandlingCharge(udID);
            TTLLCValue = GetTotalLcValue(udID);
            buyerTolerence = GetBuyerTolerance(udID);
            charge = germentsTest + TTLFreightCharge + TTLHandlingCharge + totalBuyerCommission;

            foreach (var i in a)
            {
                i.SubBody1 = ": " + TTLQPack.ToString();
                i.SubBody2 = ": " + TTLQPCS.ToString();
                i.SubBody3 = i.Body13 + TTLAmount.ToString("F");
                i.SubBody4 = i.Body13 + TTLDozRate.ToString("F");

                i.HeaderLeft4 = ": " + i.HeaderLeft4.Substring(0, i.HeaderLeft4.Length - 8);
                i.HeaderLeft6 = ": " + i.HeaderLeft6.Substring(0, i.HeaderLeft6.Length - 8);
                i.HeaderRight6 = ": " + i.HeaderRight6.Substring(0, i.HeaderRight6.Length - 8);
                i.HeaderRight3 = i.Body13 + TTLHandlingCharge.ToString();
                i.SubBody5 = i.Body13 + TTLFreightCharge.ToString();
                i.HeaderRight7 = i.Body13 + TTLLCValue.ToString();
                if (buyerTolerence != null)
                {
                    i.HeaderLeft8 = ": " + i.Body13 + buyerTolerence.Value.ToString("F");
                }
                if (totalBuyerCommission != null)
                {
                    i.Footer1 = i.Body13 + totalBuyerCommission.Value.ToString("F");

                }

                if (GroupPOValue != null)
                {
                    if (temp.Any(ac => ac.MLCId == i.int2))
                    {
                        i.Footer5 = temp.Where(p => p.MLCId == i.int2).Sum(ic => ic.TotalValue).ToString();
                    }

                    //i.Footer5 = GroupPOValue.Value.ToString();
                }
                if (TTLAmount != null && charge != null)
                {
                    actual = TTLAmount - charge;
                    i.Footer2 = i.Body13 + actual.Value.ToString("F");




                    this.ActualValueForBTB = (actual / 100) * bTBOpening;
                    i.Footer3 = i.Body13 + this.ActualValueForBTB.Value.ToString("F");


                    i.Footer4 = i.Body13 + germentsTest.ToString("F");


                }
                else
                {
                    actual = TTLAmount;
                    i.Footer2 = i.Body13 + actual.Value.ToString("F");


                    this.ActualValueForBTB = (actual / 100) * bTBOpening;
                    i.Footer3 = i.Body13 + this.ActualValueForBTB.Value.ToString("F");


                    i.Footer4 = i.Body13 + germentsTest.ToString("F");

                }
            }


            this.LCStetmentDoc1 = a;
            return a;
        }

        internal List<ReportDocType> GetBTBByUdLcStatement(int udID)
        {
            db = new xOssContext();

            var a1 = (
                from t1 in db.Commercial_B2bLC
                join t2 in db.Commercial_UD
                on t1.Commercial_UDFK equals t2.ID
                join t3 in db.Common_Supplier
                on t1.Common_SupplierFK equals t3.ID
                join t4 in db.Commercial_LCOregin
                on t1.Commercial_LCOreginFK equals t4.ID
                join t5 in db.Commercial_BtBLCType
                on t1.Commercial_BtBLCTypeFK equals t5.ID
                join t6 in db.Commercial_BTBItem
               on t1.Commercial_BTBItemFK equals t6.ID
                //  join t7 in db.Commercial_UDSlave
                //on t2.ID equals t7.Commercial_UDFK
                //from t1 in db.Commercial_UDSlave
                //join t2 in db.Commercial_UD
                //on t1.Commercial_UDFK equals t2.ID
                //join t3 in db.Commercial_B2bLC
                //on t2.ID equals t3.Commercial_UDFK
                //join t4 in db.Common_Supplier
                //on t3.Common_SupplierFK equals t4.ID
                //join t5 in db.Commercial_BTBItem
                //on t3.Commercial_BTBItemFK equals t5.ID
                where t1.Commercial_UDFK == udID
                      && t1.Active == true
                      && t2.Active == true
                      && t3.Active == true
                      && t4.Active == true
                      && t5.Active == true
                       && t6.Active == true
                // && t7.Active == true
                select new ReportDocType
                {

                    Body1 = t3.Name,  // Supplier name
                    Body2 = t1.Name,  // BTB L/C No
                    Body3 = t1.Amount.ToString(),
                    Body4 = t1.RequiredQuantity.ToString(),
                    Body5 = t1.Description,
                    Body6 = t6.Name,
                    int1 = t6.ID,
                    Body13 = "$"

                }).Distinct().OrderBy(x => x.int1).ToList();
            decimal TTLBTBAmount = 0;
            string availableBTBVal = "";
            if (a1.Count != 0)
            {
                string tempBody = a1[0].Body6;


                decimal groupTotal = 0;
                int counter = 1;
                List<string> groupList = new List<string>();

                decimal groupTotalRequiredQuentity = 0;
                List<string> groupListRequiredQuentity = new List<string>();
                foreach (var r in a1)
                {

                    decimal body3 = 0;
                    decimal.TryParse(r.Body3, out body3);

                    TTLBTBAmount += body3;
                    r.Body3 = body3.ToString("F");
                    if (this.ActualValueForBTB != null)
                    {
                        r.Body12 = this.ActualValueForBTB.Value.ToString("F");
                    }
                    else
                    {
                        r.Body12 = "";
                    }


                    r.Body15 = TTLBTBAmount.ToString();
                    if (this.ActualValueForBTB != 0)
                    {
                        r.Body14 = (this.ActualValueForBTB - TTLBTBAmount).Value.ToString("F");
                        availableBTBVal = r.Body14;
                    }

                    if (tempBody == r.Body6)
                    {
                        decimal tempValue = 0;
                        decimal.TryParse(r.Body3, out tempValue);
                        groupTotal += tempValue;

                        if (r.Body4 != null)
                        {
                            decimal tempValuequentity = 0;
                            decimal.TryParse(r.Body4, out tempValuequentity);
                            groupTotalRequiredQuentity += tempValuequentity;
                        }
                    }
                    else
                    {
                        string groupTotalRQ = "";
                        if (groupTotalRequiredQuentity != 0)
                        {
                            groupTotalRQ = ", Total Required = " + groupTotalRequiredQuentity.ToString();
                        }
                        else
                        {
                            groupTotalRQ = "";
                        }

                        groupList.Add(tempBody + " Total Value = " + groupTotal.ToString() + groupTotalRQ);
                        decimal tempValue = 0;
                        decimal.TryParse(r.Body3, out tempValue);
                        groupTotal = tempValue;

                        decimal tempValueRequiredQuentity = 0;
                        decimal.TryParse(r.Body4, out tempValueRequiredQuentity);
                        groupTotalRequiredQuentity = tempValueRequiredQuentity;
                    }
                    if (counter == a1.Count())
                    {
                        string groupTotalRQ = "";
                        if (groupTotalRequiredQuentity != 0)
                        {
                            groupTotalRQ = ", Total Required = " + groupTotalRequiredQuentity.ToString();
                        }
                        else
                        {
                            groupTotalRQ = "";
                        }
                        groupList.Add(r.Body6 + " Total Value= " + groupTotal.ToString() + groupTotalRQ);
                    }
                    tempBody = r.Body6;

                    counter++;
                }

                string tempGroupTotal = "";
                foreach (string st in groupList)
                {
                    tempGroupTotal += st + "\n";

                }
                foreach (string st in groupList)
                {
                    foreach (ReportDocType r in a1)
                    {
                        r.Body14 = tempGroupTotal;
                        if (st.Contains(r.Body6))
                        {
                            r.Body6 = st;
                        }
                    }
                }


            }


            foreach (var v1 in this.LCStetmentDoc1)
            {
                v1.Body1 = v1.Body13 + TTLBTBAmount.ToString("F");
                totalBTB = v1.Body1;
                v1.Body2 = v1.Body13 + availableBTBVal;
                availbleBTB = v1.Body2;
            }
            foreach (var i in a1)
            {
                ReportDocType r = new ReportDocType();
                r.ReportDocTypeList = new List<ReportDocType>();
                r.Body1 = i.Body1;
                r.Body2 = i.Body2;
                r.Body3 = i.Body13 + i.Body3;
                r.Body4 = i.Body4 + " " + "KGS";
                r.Body5 = i.Body5;
                r.Body12 = i.Body13 + i.Body12;
                r.Body13 = i.Body13 + i.Body13;

                r.ReportDocTypeList.Add(r);
            }

            foreach (var z in a1)
            {
                z.Body15 = totalBTB;
                z.Body14 = availbleBTB;
            }
            this.LCStetmentDoc2 = a1;
            return a1;
        }

        private decimal? GetBuyerTolerance(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_MasterLC
                     join t2 in db.Commercial_UDSlave
                     on t1.ID equals t2.Commercial_MasterLCFK

                     where t1.Active == true && t2.Active == true && t2.Commercial_UDFK == id
                     select new
                     {
                         LCID = t1.ID,
                         BuyerTolerance = t1.BuyerTolerance
                     }).ToList();
            decimal? totalValue = 0;
            decimal? TBuyerTolerance = 0;
            if (v != null)
            {
                foreach (var x in v)
                {
                    totalValue = GetMLCValue(x.LCID);
                    TBuyerTolerance += ((totalValue / 100) * 2) + totalValue;
                }

            }
            return TBuyerTolerance;
        }

        private decimal? GetMLCValue(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_MasterLC
                     where t1.Active == true && t1.ID == id
                     select t1.TotalValue).ToList();
            decimal? totalValue = 0;
            if (v != null)
            {
                totalValue = v.Sum();
            }
            return totalValue;
        }

        private decimal? GetTotalLcValue(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_MasterLC
                     join t2 in db.Commercial_UDSlave
                     on t1.ID equals t2.Commercial_MasterLCFK

                     where t1.Active == true && t2.Active == true && t2.Commercial_UDFK == id
                     select t1.TotalValue).ToList();
            decimal? totalValue = 0;
            if (v != null)
            {
                totalValue = v.Sum();
            }
            return totalValue;
        }
        public decimal? GetGroupPOValue(int iD)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_MasterLCBuyerPO
                     join t2 in db.Commercial_MasterLC
                     on t1.Commercial_MasterLCFK equals t2.ID
                     join t3 in db.Mkt_BOM
                     on t1.MKTBOMFK equals t3.ID
                     join t4 in db.Commercial_UDSlave
                     on t2.ID equals t4.Commercial_MasterLCFK
                     where t4.Commercial_MasterLCFK == iD
                     select t3.QPack * t3.Quantity * t3.UnitPrice).ToList();

            decimal? groupPO = 0;
            if (v != null)
            {
                groupPO = v.Sum();
            }

            return groupPO;
        }
        private decimal? GetHandlingCharge(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_MasterLC
                     join t2 in db.Commercial_UDSlave
                     on t1.ID equals t2.Commercial_MasterLCFK

                     where t1.Active == true && t2.Active == true && t2.Commercial_UDFK == id
                     select t1.HandlingCharge).ToList();
            decimal? handlingCharge = 0;
            if (v != null)
            {
                handlingCharge = v.Sum();
            }
            return handlingCharge;
        }

        private decimal? GetFreightCharge(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_MasterLC
                     join t2 in db.Commercial_UDSlave
                     on t1.ID equals t2.Commercial_MasterLCFK

                     where t1.Active == true && t2.Active == true && t2.Commercial_UDFK == id
                     select t1.FreightCharge).ToList();
            decimal? freightCharge = 0;
            if (v != null)
            {
                freightCharge = v.Sum();
            }
            return freightCharge;
        }




        internal void GetUnPaidBTBByUd(int udID)
        {
            db = new xOssContext();

            var allPO = (from t1 in db.Commercial_UDSlave
                         join t2 in db.Commercial_UD on t1.Commercial_UDFK equals t2.ID
                         join t3 in db.Commercial_MasterLC on t1.Commercial_MasterLCFK equals t3.ID
                         join t4 in db.Commercial_MasterLCBuyerPO on t3.ID equals t4.Commercial_MasterLCFK
                         join a2 in db.Mkt_BOM on t4.MKTBOMFK equals a2.ID
                         join a3 in db.Common_TheOrder on a2.Common_TheOrderFk equals a3.ID
                         join a4 in db.Mkt_PO on a2.ID equals a4.Mkt_BOMFK
                         join a5 in db.Common_Supplier on a4.Common_SupplierFK equals a5.ID
                         where t2.ID == udID  && t1.Active == true && t2.Active == true  && t3.Active == true  && t4.Active == true
                         && a2.Active == true && a3.Active == true && a4.Active == true && a5.Active == true
                         select new
                         {                            
                             POCID = a4.CID,
                           
                             SupplierName = a5.Name,
                             
                             POPendingValue = (((from ps in db.Mkt_POSlave
                                        where ps.Active == true
                                        && ps.Mkt_POFK == a4.ID
                                        select ps.TotalRequired * ps.Price).DefaultIfEmpty(0).Sum()) - ((from s1 in db.Commercial_B2bLcSupplierPOSlave
                                                                                                         join s2 in db.Commercial_B2bLcSupplierPO on s1.Commercial_B2bLcSupplierPOFK equals s2.ID
                                                                                                         join s3 in db.Mkt_POSlave on s1.Mkt_POSlaveFK equals s3.ID
                                                                                                         join s4 in db.Commercial_B2bLC on s2.Commercial_B2bLCFK equals s4.ID
                                                                                                         where s3.Mkt_POFK == a4.ID && s4.Commercial_UDFK == t2.ID && s1.Active == true && s2.Active == true
                                                                                                         select s1.TotalRequired * s3.Price).DefaultIfEmpty(0).Sum())),

                             //POValue = (from r in db.Mkt_POSlave
                             //           where r.Mkt_POFK == a4.ID && r.Active == true
                             //           group new { r.TotalRequired, r.Price } by new
                             //           {
                             //               r.Mkt_POFK
                             //           } into All
                             //           select new
                             //           {
                             //               TotalValue = All.Sum(a => a.TotalRequired * a.Price)
                             //           }
                             //        ).ToList()
                         }).ToList();


            //var B2BPO = (from o in db.Commercial_B2bLcSupplierPO
            //             join p in db.Mkt_PO on o.Mkt_POFK equals p.ID
            //             join q in db.Common_Supplier on p.Common_SupplierFK equals q.ID
            //             join r in db.Commercial_B2bLC on o.Commercial_B2bLCFK equals r.ID
            //             join s in db.Commercial_UD on r.Commercial_UDFK equals s.ID
            //             where s.ID == udID
            //             && o.Active == true
            //             && p.Active == true
            //             && q.Active == true
            //             && r.Active == true
            //             && s.Active == true
            //             select new
            //             {
            //                 POID = p.ID,
            //                 POCID = p.CID,
            //                 SupplierId = q.ID,
            //                 Supplier = q.Name,
            //                 RequiredPoQuantity = !(from t1 in db.Commercial_B2bLcSupplierPOSlave
            //                                        where t1.Active == true
            //                                        && t1.Commercial_B2bLcSupplierPOFK == o.ID
            //                                        select t1.TotalRequired).Any() ? 0 : (from t1 in db.Commercial_B2bLcSupplierPOSlave
            //                                                                              where t1.Active == true
            //                                                                              && t1.Commercial_B2bLcSupplierPOFK == o.ID
            //                                                                              select t1.TotalRequired).Sum(),
            //                 POValue = (from x in db.Mkt_POSlave
            //                            join y in db.Commercial_B2bLcSupplierPO on x.Mkt_POFK equals y.Mkt_POFK
            //                            join z in db.Commercial_B2bLcSupplierPOSlave on y.ID equals z.Commercial_B2bLcSupplierPOFK
            //                            where x.Mkt_POFK == p.ID && x.Active == true
            //                            group new { z.TotalRequired, x.Price } by new
            //                            {
            //                                x.Mkt_POFK
            //                            } into All
            //                            select new
            //                            {
            //                                TotalValue = All.Sum(a => a.TotalRequired * a.Price)

            //                            }
            //                         ).ToList()

            //             }).ToList();

            List<ReportDocType> list = new List<ReportDocType>();
            if (allPO.Any())
            {
                decimal totalPOValue = 0;
                foreach (var v in allPO)
                {
                    ReportDocType reportDoc = new ReportDocType();
                    reportDoc.Body1 = v.SupplierName;
                    reportDoc.Body2 = v.POCID;
                    reportDoc.Body3 = v.POPendingValue.Value.ToString("F");
                    totalPOValue += v.POPendingValue.Value;
                    reportDoc.Footer1 = totalPOValue.ToString("F");
                    list.Add(reportDoc);

                    //if (!B2BPO.Any(a => a.RequiredPoQuantity < v.POQuantity))
                    //{
                    //    decimal ttl2 = 0;
                    //    foreach (var p in B2BPO.Where(x => x.POID == v.POID).GroupBy(x => x.POValue))
                    //    {
                    //        ttl2 = p.Key.Sum(e => e.TotalValue) == null ? 0 : (decimal)p.Key.Sum(e => e.TotalValue);
                    //    }
                    //    decimal ttl = v.POValue.Sum(e => e.TotalValue) == null ? 0 : (decimal)v.POValue.Sum(e => e.TotalValue) - ttl2;
                    //}
                    //decimal total1 = list.Sum(x => Decimal.Parse(x.Body3, CultureInfo.InvariantCulture));
                    //return list;
                }
            }
            this.LCStetmentDoc3 = list;
        }
    }
}