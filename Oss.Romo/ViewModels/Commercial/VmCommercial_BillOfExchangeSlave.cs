﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_BillOfExchangeSlave
    {
        private xOssContext db;
        public IEnumerable<VmCommercial_BillOfExchangeSlave> DataList { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Commercial_BillOfExchange Commercial_BillOfExchange { get; set; }
        public Commercial_BillOfExchangeSlave Commercial_BillOfExchangeSlave { get; set; }
        public Commercial_Invoice Commercial_Invoice { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Commercial_Bank Buyer_Bank { get; set; }
        public Commercial_Bank Supplier_Bank { get; set; }     
        public List<ReportDocType> BillOfExchangeReportDoc { get; set; }
        public VmCommercial_MasterLCBuyerPO VmCommercial_MasterLCBuyerPO { get; set; }
        [DisplayName("Total Invoice Value ")]
        public decimal TotalInvoiceValue { get; set; }
        [DisplayName("Total Invoice Value")]
        public string TotalInvoiceValueStr { get; set; }
        public void InitialDataLoad(int? id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_BillOfExchangeSlave
                  join t2 in db.Commercial_BillOfExchange on t1.Commercial_BillOfExchangeFk equals t2.ID
                  join t3 in db.Commercial_Invoice on t1.Commercial_InvoiceFK equals t3.ID
                  //join Commercial_MasterLC in db.Commercial_MasterLC
                  //on Commercial_Invoice.Commercial_MasterLCFk equals Commercial_MasterLC.ID
                  where t1.Commercial_BillOfExchangeFk == id

                     select new VmCommercial_BillOfExchangeSlave
                     {
                         Commercial_BillOfExchangeSlave = t1,
                         Commercial_BillOfExchange = t2,
                         Commercial_Invoice=t3
                         //Commercial_MasterLC= Commercial_MasterLC
                     }).Where(x => x.Commercial_BillOfExchangeSlave.Active == true).OrderByDescending(x => x.Commercial_BillOfExchangeSlave.ID).AsEnumerable();
            List<VmCommercial_BillOfExchangeSlave> list = new List<VmCommercial_BillOfExchangeSlave>();
            foreach (VmCommercial_BillOfExchangeSlave x in a)
            {
                x.TotalInvoiceValue = GetInvoiceValue(x.Commercial_Invoice.ID);
                list.Add(x);
            }


            this.DataList = list;

        }

        public decimal GetInvoiceValue(int id)
        {
            db = new xOssContext();
            //var a = (from t1 in db.Commercial_InvoiceSlave
            //         join t3 in db.Mkt_BOM on t1.MktBOMFK equals t3.ID
            //         join t4 in db.Shipment_OrderDeliverdSchedule on t1.Shipment_OrderDeliverdScheduleFK equals t4.ID
            //         where t1.Commercial_InvoiceFk == id && t1.Active == true
            //    select t4.PackQty * t3.PackPrice).ToList();

            //******* Change due to mis-match at Bill of exchange and Invoices

            var a = (from t1 in db.Commercial_InvoiceSlave
                     join t2 in db.Commercial_Invoice on t1.Commercial_InvoiceFk equals t2.ID
                     join t3 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t3.ID
                     join t0 in db.Shipment_OrderDeliverdSchedule on t1.Shipment_OrderDeliverdScheduleFK equals t0.ID
                     join t4 in db.Mkt_BOM on t0.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     join t6 in db.Mkt_Item on t4.Mkt_ItemFK equals t6.ID
                     join t7 in db.Common_Currency on t5.Common_CurrencyFK equals t7.ID
                     where t1.Commercial_InvoiceFk == id && t1.Active == true
                     select (t4.PackPrice * t0.PackQty)).ToList();

            decimal deliverdQty = 0;
            if (a != null)
            {
                var invoice = db.Commercial_Invoice.SingleOrDefault(x => x.ID == id);
                if (invoice.IsIncrease == true)
                {
                    deliverdQty = a.Sum() + invoice.IncDecAmount;
                }
                else
                {
                    deliverdQty = a.Sum() - invoice.IncDecAmount;
                }
               
            }
            return deliverdQty;
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_BillOfExchangeSlave
                     join t2 in db.Commercial_BillOfExchange
                     on t1.Commercial_BillOfExchangeFk equals t2.ID
                     join t3 in db.Commercial_Invoice
                     on t1.Commercial_InvoiceFK equals t3.ID
                     select new VmCommercial_BillOfExchangeSlave
                     {
                         Commercial_BillOfExchangeSlave = t1,
                         Commercial_BillOfExchange = t2,
                         Commercial_Invoice =t3
                     }).SingleOrDefault(c => c.Commercial_BillOfExchangeSlave.ID == id);
            this.Commercial_BillOfExchangeSlave = v.Commercial_BillOfExchangeSlave;
            this.Commercial_BillOfExchange = v.Commercial_BillOfExchange;

        }

        //public VmCommercial_BillOfExchangeSlave SelectSingleJoined(int id)
        //{

        //    db = new xOssContext();

        //    var v = (from Commercial_MasterLC in db.Commercial_MasterLC
        //             join Mkt_Buyer in db.Mkt_Buyer
        //             on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
        //             join lienBank in db.Commercial_Bank
        //             on Commercial_MasterLC.Commercial_Buyer_BankFK equals lienBank.ID

        //             join supplierBank in db.Commercial_Bank
        //             on Commercial_MasterLC.Commercial_Supplier_BankFK equals supplierBank.ID
        //             select new VmCommercial_BillOfExchangeSlave
        //             {
        //                 Commercial_MasterLC = Commercial_MasterLC,
        //                 Mkt_Buyer = Mkt_Buyer,
        //                 Buyer_Bank = lienBank,
        //                 Supplier_Bank = supplierBank
        //             }).Where(x => x.Commercial_MasterLC.ID == id).FirstOrDefault();
        //    return v;
        //   }

        public VmCommercial_BillOfExchangeSlave SelectSingleJoined(int id)
        {

            db = new xOssContext();

            var v = (from t1 in db.Commercial_BillOfExchange
                     join t2 in db.Commercial_MasterLC
                     on t1.Commercial_MasterLCFK equals t2.ID


                     select new VmCommercial_BillOfExchangeSlave
                     {
                         Commercial_BillOfExchange = t1,
                         Commercial_MasterLC = t2

                     }).FirstOrDefault(x => x.Commercial_BillOfExchange.ID == id);
            return v;
        }


        public int Add(int userID)
        {
            Commercial_BillOfExchangeSlave.AddReady(userID);
            return Commercial_BillOfExchangeSlave.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_BillOfExchangeSlave.Edit(userID);
        }
        internal void BillOfExchangeDocDataLoad(int id)
        {

            db = new xOssContext();

            //var a = (from t1 in db.Commercial_BillOfExchange
            //         //join t2 in db.Commercial_Invoice
            //         //on t1.Commercial_InvoiceFk equals t2.ID
            //         join t3 in db.Commercial_InvoiceSlave
            //         on t2.ID equals t3.Commercial_InvoiceFk
            //         join t4 in db.Commercial_MasterLC
            //         on t1.Commercial_MasterLCFk equals t4.ID
            //         join t5 in db.Commercial_Bank
            //        on t4.Commercial_Buyer_BankFK equals t5.ID

            //         join t6 in db.Commercial_Bank
            //         on t4.Commercial_Supplier_BankFK equals t6.ID
            //         where t1.ID == id && t1.Active == true && t3.Active == true
                    
            //         //join User_User in db.User_User on Common_TheOrder.FirstCreatedBy equals User_User.ID
            //         select new ReportDocType
            //         {

            //             Body1 = t1.Description,
            //             //Body2 = t2.InvoiceNo,
            //             //Body3 = t2.InvoiceDate.ToString(),
            //             Body4 = t4.Name, //Master L/C No
            //             Body5 = t4.Date.ToString(),
            //             Body6 = t5.Name  //

            //         }).ToList();

            //this.BillOfExchangeReportDoc = a.ToList();
        }
    }
 
}