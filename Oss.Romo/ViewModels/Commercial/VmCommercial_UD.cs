﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{

    public class MasterLCDetails
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }


    public class VmCommercial_UD
    {
        private xOssContext db;
        public Commercial_UD Commercial_UD { get; set; }
        public Commercial_UDSlave Commercial_UDSlave { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public IEnumerable<VmCommercial_UD> DataList { get; set; }
        public List<MasterLCDetails> MasterLC { get; set; }
        public VmCommercial_MasterLC VmCommercial_MasterLC { get; set; }

        public string TotalLCAmount { get; set; }
        public string TotalPOValue { get; set; }
        public string ActualPOValue { get; set; }
        public string BTBLimitation { get; set; }
        public string BTBValue { get; set; }
        public string AvailableForBTB { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_UD
                     select new VmCommercial_UD
                     {
                         Commercial_UD = t1
        }).Where(x => x.Commercial_UD.Active == true && x.Commercial_UD.ID!=35 && x.Commercial_UD.IsClose == false).OrderByDescending(x => x.Commercial_UD.ID).AsEnumerable().ToList();
            
            foreach (var x in a)
            {
                VmCommercial_MasterLC = new VmCommercial_MasterLC();
                var vData = VmCommercial_MasterLC.LCStatementDocDataLoad(x.Commercial_UD.ID).FirstOrDefault();
                if (vData!=null)
                {
                    x.TotalLCAmount = vData.HeaderRight7;
                    x.TotalPOValue = vData.SubBody3;
                    x.ActualPOValue = vData.Footer2;
                    x.BTBLimitation = vData.Footer3;
                    x.BTBValue = vData.Body1;
                    var footer3 = vData.Footer3.Split('$');
                    decimal arr = 0;
                    decimal arr2 = 0;

                    decimal.TryParse(vData.Body1, out arr2);
                    decimal.TryParse(footer3[1], out arr);

                    x.AvailableForBTB = "$"+ (arr - arr2).ToString();
                }
                
                x.MasterLC = GetMasterLc(x.Commercial_UD.ID);
            }
            this.DataList = a;

        }
        public void ClosedUdInitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_UD
                     select new VmCommercial_UD
                     {
                         Commercial_UD = t1
                     }).Where(x => x.Commercial_UD.Active == true && x.Commercial_UD.IsClose == true 
                     && x.Commercial_UD.ID != 35).OrderByDescending(x => x.Commercial_UD.ID).AsEnumerable().ToList();            
            foreach (var x in a)
            {
                x.MasterLC = GetMasterLc(x.Commercial_UD.ID);
            }
            this.DataList = a;
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = ( from t1 in db.Commercial_UD

                     select new VmCommercial_UD
                     {
                      Commercial_UD = t1

                     }).Where(c => c.Commercial_UD.ID == id).SingleOrDefault();
            this.Commercial_UD = v.Commercial_UD;
         
        }

        public int Add(int userID)
        {
            Commercial_UD.AddReady(userID);
       
            return Commercial_UD.Add();

        }

        public bool Edit(int userID)
        {
            return Commercial_UD.Edit(userID);
        }
        
        public List<MasterLCDetails> GetMasterLc(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_UD
                     join t2 in db.Commercial_UDSlave
                     on t1.ID equals t2.Commercial_UDFK
                     join t3 in db.Commercial_MasterLC
                     on t2.Commercial_MasterLCFK equals t3.ID
                     where t1.ID == id 
                     && t3.Active == true
                     && t1.Active == true 
                     && t2.Active == true

                     select new MasterLCDetails
                     {
                         ID = t3.ID,
                         Name = t3.Name
                       
                     }).Distinct().ToList();
            return v;
        }
    }
}