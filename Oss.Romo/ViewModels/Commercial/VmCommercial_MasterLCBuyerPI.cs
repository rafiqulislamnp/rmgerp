﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_MasterLCBuyerPI
    {
        private xOssContext db;
        public Commercial_MasterLCBuyerPI Commercial_MasterLCBuyerPI { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }


        public IEnumerable<VmCommercial_MasterLCBuyerPI> DataList { get; set; }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_MasterLCBuyerPI in db.Commercial_MasterLCBuyerPI
                     join Commercial_MasterLC in db.Commercial_MasterLC
                     on Commercial_MasterLCBuyerPI.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID


                     select new VmCommercial_MasterLCBuyerPI
                     {
                         Commercial_MasterLCBuyerPI = Commercial_MasterLCBuyerPI,
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer= Mkt_Buyer


                     }).Where(x => x.Commercial_MasterLCBuyerPI.Active == true && x.Commercial_MasterLC.ID == id)
                       .OrderByDescending(x => x.Commercial_MasterLCBuyerPI.ID).AsEnumerable();
            this.DataList = a;

        }
       public VmCommercial_MasterLCBuyerPI SelectSingleJoined(int id)
        {
            db = new xOssContext();
            
            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID

                     select new VmCommercial_MasterLCBuyerPI
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer
                     }).Where(x => x.Commercial_MasterLC.ID == id).FirstOrDefault();
            return v;



           
        }







        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_MasterLCBuyerPI in db.Commercial_MasterLCBuyerPI

                     select new VmCommercial_MasterLCBuyerPI
                     {
                         Commercial_MasterLCBuyerPI = Commercial_MasterLCBuyerPI
                   

                     }).Where(c => c.Commercial_MasterLCBuyerPI.ID == id).SingleOrDefault();
            this.Commercial_MasterLCBuyerPI = v.Commercial_MasterLCBuyerPI;
          
        }
        public int Add(int userID)
        {
            Commercial_MasterLCBuyerPI.AddReady(userID);
            return Commercial_MasterLCBuyerPI.Add();
        }
        public bool Edit(int userID)
        {
            return Commercial_MasterLCBuyerPI.Edit(userID);
        }


    }


}