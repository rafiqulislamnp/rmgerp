﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_UDSlave
    {

        private xOssContext db;
        public Commercial_UDSlave Commercial_UDSlave { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
        public VmCommercial_Invoice VmCommercial_Invoice { get; set; }
        public Commercial_MasterLCBuyerPO Commercial_MasterLCBuyerPO { get; set; }

        public Mkt_BOM Mkt_BOM { get; set; }
        [DisplayName("Total PO Value")]
        public decimal? TotalPoValue { get; set; }
        [DisplayName("Total L/C Value")]
        public decimal? TotalLCValue { get; set; }
        //public decimal? TotalPOValue { get; set; }
        public string TotalPOValueStr { get; set; }
        public decimal? BTBOpening { get; set; }
        public decimal? TotalFreightCharge { get; set; }
        public decimal? TotalHandlingCharge { get; set; }
        public decimal? BuyerCommission { get; set; }
        public decimal? TotalBuyerCommission { get; set; }
        public string TotalLCValueStr { get; set; }
        public string BTBOpeningStr { get; set; }
        public string TotalFreightChargeStr { get; set; }
        public string TotalHandlingChargeStr { get; set; }
        public string BuyerCommissionStr { get; set; }
        public string TotalBuyerCommissionStr { get; set; }
        public VmCommercial_B2bLC VmCommercial_B2bLC { get; set; }
        public IEnumerable<VmCommercial_UDSlave> DataList { get; set; }
        public decimal? TestCharge { get; set; }
        public string TestChargeStr { get; set; }
        public decimal? TotalTestCharge { get; set; }
        public string TotalTestChargeStr { get; set; }
        public Commercial_Bank BuyerBank { get; set; }
        public Commercial_Bank LienBank { get; set; }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_UDSlave
                     join t2 in db.Commercial_UD
                     on t1.Commercial_UDFK equals t2.ID
                     join t3 in db.Commercial_MasterLC
                     on t1.Commercial_MasterLCFK equals t3.ID

                     where t1.Commercial_UDFK == id
                     && t2.Active == true
                     && t3.Active == true

                     select new VmCommercial_UDSlave
                     {
                         Commercial_UDSlave = t1,
                         Commercial_UD = t2,
                         Commercial_MasterLC = t3

                     }).Where(x => x.Commercial_UDSlave.Active == true).OrderByDescending(x => x.Commercial_UDSlave.ID).Distinct().AsEnumerable();

            decimal? btbOpening = 0;
            List<VmCommercial_UDSlave> tempDl = new List<VmCommercial_UDSlave>();

            foreach (VmCommercial_UDSlave x in a)
            {
                btbOpening = x.Commercial_UD.BTBOpening;
                decimal? tempD = GetTotalPoValue(x.Commercial_MasterLC.ID);
                decimal? tempC = GetTotalBuyerCommission(x.Commercial_MasterLC.ID);

                decimal? tempT = GetBOMGarmantsTestValue(x.Commercial_MasterLC.ID);


                if (tempD == null)
                {
                    tempD = 0;
                }

                VmCommercial_UDSlave vmCommercial_UDSlaveTemp = new VmCommercial_UDSlave();
                vmCommercial_UDSlaveTemp.Commercial_UDSlave = x.Commercial_UDSlave;
                vmCommercial_UDSlaveTemp.Commercial_UD = x.Commercial_UD;
                vmCommercial_UDSlaveTemp.Commercial_MasterLC = x.Commercial_MasterLC;
                vmCommercial_UDSlaveTemp.TotalPoValue = tempD;
                vmCommercial_UDSlaveTemp.BuyerCommission = tempC;
                vmCommercial_UDSlaveTemp.TestCharge = tempT;
                tempDl.Add(vmCommercial_UDSlaveTemp);
            }

            this.DataList = tempDl;
            this.TotalPoValue = tempDl.Sum(x => x.TotalPoValue);
            if (this.TotalPoValue != null)
            {
                this.TotalPOValueStr = this.TotalPoValue.Value.ToString("F");
            }
            this.TotalLCValue = tempDl.Sum(x => x.Commercial_MasterLC.TotalValue);
            if (this.TotalLCValue != null)
            {
                this.TotalLCValueStr = this.TotalLCValue.Value.ToString("F");
            }
            this.TotalFreightCharge = tempDl.Sum(x => x.Commercial_MasterLC.FreightCharge);

            if (this.TotalFreightCharge != null)
            {
                TotalFreightChargeStr = this.TotalFreightCharge.Value.ToString("F");
            }
            this.TotalHandlingCharge = tempDl.Sum(x => x.Commercial_MasterLC.HandlingCharge);
            if (this.TotalHandlingCharge != null)
            {
                this.TotalHandlingChargeStr = this.TotalHandlingCharge.Value.ToString("F");
            }

            this.TotalBuyerCommission = tempDl.Sum(x => x.BuyerCommission);
            if (this.TotalBuyerCommission != null)
            {
                this.TotalBuyerCommissionStr = this.TotalBuyerCommission.Value.ToString("F");
            }
            this.TotalTestCharge = tempDl.Sum(x => x.TestCharge);
            if (this.TotalTestCharge != null)
            {
                this.TotalTestChargeStr = this.TotalTestCharge.Value.ToString("F");
            }
            this.BTBOpening = ((this.TotalPoValue - (this.TotalFreightCharge + this.TotalHandlingCharge + this.TotalBuyerCommission + this.TotalTestCharge)) / 100 * btbOpening);

            if (this.BTBOpening != null)
            {
                this.BTBOpeningStr = this.BTBOpening.Value.ToString("F");
            }

        }

        //public decimal GetBOMGarmantsTestValue(int id)
        //{
        //    db = new xOssContext();
        //    var v = ( from t0 in db.Commercial_MasterLCBuyerPO
        //              join t2 in db.Mkt_BOM
        //             on t0.MKTBOMFK equals t2.ID
        //              join t1 in db.Mkt_BOMSlave
        //             on t2.ID equals t1.Mkt_BOMFK
        //             where t0.Commercial_MasterLCFK == id && t1.Raw_ItemFK == 2058 // Set it dynamicaly leter
        //             select new
        //             {
        //                 TestPrice = t1.Price
        //             }).ToList();
        //    decimal gTestPrice = 0;
        //    foreach (var i in v)
        //    {
        //        if (i != null)
        //        {
        //            gTestPrice = i.TestPrice;
        //        }
        //    }
        //    return gTestPrice;
        //}
        public decimal GetBOMGarmantsTestValue(int lcID)
        {
            db = new xOssContext();
            var v = (from t0 in db.Commercial_MasterLCBuyerPO
                     join t2 in db.Mkt_BOM
                    on t0.MKTBOMFK equals t2.ID
                     join t1 in db.Mkt_BOMSlave
                    on t2.ID equals t1.Mkt_BOMFK
                     where t0.Commercial_MasterLCFK == lcID && t1.Raw_ItemFK == 2058 // Set it dynamicaly leter
                     && t0.Active == true && t1.Active == true
                     select t1.Price).ToList();
            decimal gTestPrice = 0;

            if (v != null)
            {
                gTestPrice = v.Sum();
            }

            return gTestPrice;
        }
        public decimal GetBOMGarmantsTestValue(int lID, int bID)
        {
            db = new xOssContext();
            var v = (from t0 in db.Commercial_MasterLCBuyerPO
                     join t2 in db.Mkt_BOM
                    on t0.MKTBOMFK equals t2.ID
                     join t1 in db.Mkt_BOMSlave
                    on t2.ID equals t1.Mkt_BOMFK
                     where t0.Commercial_MasterLCFK == lID && t0.MKTBOMFK == bID && t1.Raw_ItemFK == 2058 // Set it dynamicaly leter
                     && t0.Active == true && t1.Active == true && t2.Active == true
                     select t1.Price).FirstOrDefault();
            decimal gTestPrice = 0;

            if (v != null)
            {
                gTestPrice = v;
            }
            return gTestPrice;
        }
        //public decimal? GetGroupPoValue(int iD,int id)
        //{
        //    db = new xOssContext();
        //    var a1 = (from t1 in db.Commercial_MasterLCBuyerPO
        //              join t2 in db.Mkt_BOM
        //             on t1.MKTBOMFK equals t2.ID
        //              join t3 in db.Commercial_UDSlave
        //             on t1.Commercial_MasterLCFK equals t3.Commercial_MasterLCFK

        //              where t1.Commercial_MasterLCFK == iD && t1.MKTBOMFK == id && t1.Active == true && t2.Active == true && t3.Active == true //  && t3.Commercial_UDFK == UdID && t1.MKTBOMFK == bomID
        //              select (t2.QPack * t2.Quantity * t2.UnitPrice)).DefaultIfEmpty();
        //    decimal? a = 0;
        //    if (a1 != null)
        //    {
        //        a = a1.Sum();
        //    }

        //    return a;
        //}

  

        public decimal? GetTotalPoValue(int iD)
        {
            db = new xOssContext();
            var a1 = (from t1 in db.Commercial_MasterLCBuyerPO
                      join t2 in db.Mkt_BOM
                     on t1.MKTBOMFK equals t2.ID
                      where t1.Commercial_MasterLCFK == iD && t1.Active == true
                      select (t2.QPack * t2.Quantity * t2.UnitPrice)).DefaultIfEmpty();
            decimal? a = 0;
            if (a1 != null)
            {
                a = a1.Sum();
            }

            return a;
        }
        private decimal? GetTotalBuyerCommission(int iD)
        {
            db = new xOssContext();
            var a1 = (from t1 in db.Commercial_MasterLCBuyerPO
                      join t2 in db.Mkt_BOM
                      on t1.MKTBOMFK equals t2.ID
                      join t3 in db.Common_TheOrder
                      on t2.Common_TheOrderFk equals t3.ID
                      where t1.Commercial_MasterLCFK == iD && t1.Active == true && t2.Active == true && t3.Active == true
                      select (((t2.QPack * t2.Quantity * t2.UnitPrice) / 100) * t3.Commission)).DefaultIfEmpty().Sum();

            return a1;
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_UDSlave
                     join t2 in db.Commercial_UD
                     on t1.Commercial_UDFK equals t2.ID
                     join t3 in db.Commercial_MasterLC
                     on t1.Commercial_MasterLCFK equals t3.ID
                     select new VmCommercial_UDSlave
                     {
                         Commercial_UDSlave = t1,
                         Commercial_UD = t2,
                         Commercial_MasterLC = t3
                     }).Where(c => c.Commercial_UDSlave.ID == id).SingleOrDefault();
            this.Commercial_UD = v.Commercial_UD;
            this.Commercial_UDSlave = v.Commercial_UDSlave;
            this.Commercial_MasterLC = v.Commercial_MasterLC;

        }
        
        public VmCommercial_UDSlave SelectSingleJoined(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_UD
                     where t1.Active == true
                     select new VmCommercial_UDSlave
                     {
                         Commercial_UD = t1

                     }).Where(x => x.Commercial_UD.ID == id).FirstOrDefault();
            return v;
        }
        
        public int Add(int userID)
        {
            Commercial_UDSlave.AddReady(userID);

            return Commercial_UDSlave.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_UDSlave.Edit(userID);
        }
    }
}