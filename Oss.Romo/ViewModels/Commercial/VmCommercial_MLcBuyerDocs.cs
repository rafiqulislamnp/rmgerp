﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_MLcBuyerDocs
    {
        private xOssContext db;

        public Commercial_MLcBuyerDocs Commercial_MLcBuyerDocs { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
      //  public Commercial__MasterLCDocs Commercial__MasterLCDocs { get; set; }
        public IEnumerable<VmCommercial_MLcBuyerDocs> DataList { get; set; }
        public List<object> DropDownList { get; set; }

        public int Add(int userID)
        {
            Commercial_MLcBuyerDocs.AddReady(userID);
            return Commercial_MLcBuyerDocs.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_MLcBuyerDocs.Edit(userID);
        }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_MLcBuyerDocs in db.Commercial_MLcBuyerDocs
                     join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_MLcBuyerDocs.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                   //  join Commercial__MasterLCDocs in db.Commercial__MasterLCDocs on Commercial_MLcBuyerDocs.Commercial__MasterLCDocsFK equals Commercial__MasterLCDocs.ID
                     select new VmCommercial_MLcBuyerDocs
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                     //    Commercial__MasterLCDocs = Commercial__MasterLCDocs,
                         Commercial_MLcBuyerDocs = Commercial_MLcBuyerDocs

                     }).Where(x => x.Commercial_MLcBuyerDocs.Active == true && x.Commercial_MasterLC.ID == id)
                       .OrderByDescending(x => x.Commercial_MLcBuyerDocs.ID).AsEnumerable();
            this.DataList = a;
        }
        public VmCommercial_MLcBuyerDocs SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID

                     select new VmCommercial_MLcBuyerDocs
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer
                     }).Where(x => x.Commercial_MasterLC.ID == iD).FirstOrDefault();
            return v;
        }

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_MLcBuyerDocs in db.Commercial_MLcBuyerDocs
                     join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_MLcBuyerDocs.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                 //    join Commercial__MasterLCDocs in db.Commercial__MasterLCDocs on Commercial_MLcBuyerDocs.Commercial__MasterLCDocsFK equals Commercial__MasterLCDocs.ID
                     select new VmCommercial_MLcBuyerDocs
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                     //    Commercial__MasterLCDocs = Commercial__MasterLCDocs,
                         Commercial_MLcBuyerDocs = Commercial_MLcBuyerDocs

                     }).Where(c => c.Commercial_MLcBuyerDocs.ID == iD).SingleOrDefault();
            this.Commercial_MLcBuyerDocs = v.Commercial_MLcBuyerDocs;
            this.Commercial_MasterLC = v.Commercial_MasterLC;
           // this.Commercial__MasterLCDocs = v.Commercial__MasterLCDocs;
        }



    }
}