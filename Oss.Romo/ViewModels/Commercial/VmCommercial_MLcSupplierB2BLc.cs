﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_MLcSupplierB2BLc
    {

        private xOssContext db;

        public Commercial_MLcSupplierB2BLc Commercial_MLcSupplierB2BLc { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        public IEnumerable<VmCommercial_MLcSupplierB2BLc> DataList { get; set; }
        public List<object> DropDownList { get; set; }


        public int Add(int userID)
        {
            Commercial_MLcSupplierB2BLc.AddReady(userID);
            return Commercial_MLcSupplierB2BLc.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_MLcSupplierB2BLc.Edit(userID);
        }

        public VmCommercial_MLcSupplierB2BLc SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID

                     select new VmCommercial_MLcSupplierB2BLc
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer
                     }).Where(x => x.Commercial_MasterLC.ID == iD).FirstOrDefault();
            return v;
        }
        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_MLcSupplierB2BLc in db.Commercial_MLcSupplierB2BLc
                     join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_MLcSupplierB2BLc.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Commercial_B2bLC in db.Commercial_B2bLC on Commercial_MLcSupplierB2BLc.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     select new VmCommercial_MLcSupplierB2BLc
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_MLcSupplierB2BLc = Commercial_MLcSupplierB2BLc

                     }).Where(x => x.Commercial_MLcSupplierB2BLc.Active == true && x.Commercial_MasterLC.ID == id)
                       .OrderByDescending(x => x.Commercial_MLcSupplierB2BLc.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_MLcSupplierB2BLc in db.Commercial_MLcSupplierB2BLc
                     join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_MLcSupplierB2BLc.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Commercial_B2bLC in db.Commercial_B2bLC on Commercial_MLcSupplierB2BLc.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     select new VmCommercial_MLcSupplierB2BLc
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_MLcSupplierB2BLc = Commercial_MLcSupplierB2BLc
                     }).Where(c => c.Commercial_MLcSupplierB2BLc.ID == iD).SingleOrDefault();
            this.Commercial_MLcSupplierB2BLc = v.Commercial_MLcSupplierB2BLc;
            this.Commercial_MasterLC = v.Commercial_MasterLC;
            this.Commercial_B2bLC = v.Commercial_B2bLC;
        }





    }
}