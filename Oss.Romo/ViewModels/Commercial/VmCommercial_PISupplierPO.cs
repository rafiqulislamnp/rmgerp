﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_PISupplierPO
    {
        private xOssContext db;
        public Mkt_PO Mkt_PO { get; set; }
        public Commercial_PI Commercial_PI { get; set; }
        public Commercial_PISupplierPO Commercial_PISupplierPO { get; set; }
        public IEnumerable<VmCommercial_PISupplierPO> DataList { get; set; }
        public List<object> DropDownList { get; set; }
        public int Add(int userID)
        {
            Commercial_PISupplierPO.AddReady(userID);
            return Commercial_PISupplierPO.Add();
        }
        public bool Edit(int userID)
        {
            return Commercial_PISupplierPO.Edit(userID);
        }
        public VmCommercial_PISupplierPO SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_PI in db.Commercial_PI
                     //from Commercial_MasterLC in db.Commercial_MasterLC
                     //join Mkt_Buyer in db.Mkt_Buyer
                     //on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID

                     select new VmCommercial_PISupplierPO
                     {
                         Commercial_PI= Commercial_PI
                         //Commercial_MasterLC = Commercial_MasterLC,
                         //Mkt_Buyer = Mkt_Buyer
                     }).Where(x => x.Commercial_PI.ID == iD).FirstOrDefault();
            return v;
        }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_PISupplierPO in db.Commercial_PISupplierPO
                     join Commercial_PI in db.Commercial_PI on Commercial_PISupplierPO.Commercial_PIFK equals Commercial_PI.ID
                     join Mkt_PO in db.Mkt_PO on Commercial_PISupplierPO.Mkt_POFK equals Mkt_PO.ID
                     select new VmCommercial_PISupplierPO
                     {
                         Commercial_PI = Commercial_PI,
                         Mkt_PO = Mkt_PO,
                         Commercial_PISupplierPO = Commercial_PISupplierPO

                     }).Where(x => x.Commercial_PISupplierPO.Active == true && x.Commercial_PI.ID == id)
                       .OrderByDescending(x => x.Commercial_PISupplierPO.ID).AsEnumerable();
            this.DataList = a;
        }

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_PISupplierPO in db.Commercial_PISupplierPO
                     join Commercial_PI in db.Commercial_PI on Commercial_PISupplierPO.Commercial_PIFK equals Commercial_PI.ID
                     join Mkt_PO in db.Mkt_PO on Commercial_PISupplierPO.Mkt_POFK equals Mkt_PO.ID
                     select new VmCommercial_PISupplierPO
                     {
                         Commercial_PI = Commercial_PI,
                         Mkt_PO = Mkt_PO,
                         Commercial_PISupplierPO = Commercial_PISupplierPO

                     }).Where(c => c.Commercial_PISupplierPO.ID == iD).SingleOrDefault();
            this.Commercial_PISupplierPO = v.Commercial_PISupplierPO;
            this.Commercial_PI = v.Commercial_PI;
            this.Mkt_PO = v.Mkt_PO;
        }

    }
}