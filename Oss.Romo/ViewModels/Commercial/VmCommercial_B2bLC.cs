﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Oss.Romo.Models.Common;
using Oss.Romo.ViewModels.Reports;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Romo.Models.Store;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_B2bLC
    {
        private xOssContext db;
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public VmCommercial_MasterLCBuyerPO VmCommercial_MasterLCBuyerPO { get; set; }
        public Commercial_B2BPaymentInformation Commercial_B2BPaymentInformation { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Commercial_B2bLcSupplierPOSlave Commercial_B2bLcSupplierPOSlave { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
        public VmCommercial_UDSlave VmCommercial_UDSlave { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Commercial_Bank Buyer_Bank { get; set; }
        public Commercial_Bank Lien_Bank { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Commercial_LCOregin Commercial_LCOregin { get; set; }
        public Commercial_BtBLCType Commercial_BtBLCType { get; set; }
        public List<List<B2bLcSupplierPOView>> b2B { get; set; }
        public decimal PoValue { get; set; }
        public int Sid { get; set; }
        public IEnumerable<VmCommercial_B2bLC> DataList { get; set; }

        public Commercial_BTBItem Commercial_BTBItem { get; set; }
        public List<object> DropDownList { get; set; }
        [DefaultValue(0)]
        [DisplayName("Amendment Increase")]
        public decimal AmendmentIncrease { get; set; }
        [DefaultValue(0)]
        [DisplayName("Amendment Decrease")]
        public decimal AmendmentDecrease { get; set; }
        [DisplayName("Payble")]
        public decimal PaybleBTB { get; set; }

        public int SupplierID { get; set; }
        [DisplayName("Total Payment")]
        public decimal B2bLcPaymentHistory { get; set; }
        public Commercial_UDSlave Commercial_UDSlave { get; set; }

        public HttpPostedFileBase ScanedFile { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_B2bLC
                         //join t2 in db.Commercial_UDSlave
                         //on t1.Commercial_UDFK equals t2.Commercial_UDFK

                     join t2 in db.Commercial_UD
                    on t1.Commercial_UDFK equals t2.ID
                     join t4 in db.Common_Supplier
                     on t1.Common_SupplierFK equals t4.ID

                     select new VmCommercial_B2bLC
                     {
                         Commercial_B2bLC = t1,
                         // Commercial_UDSlave = t2,
                         Commercial_UD = t2,
                         Common_Supplier = t4,


                     }).Where(x => x.Commercial_B2bLC.Active == true).OrderByDescending(x => x.Commercial_B2bLC.ID).AsEnumerable();
            this.DataList = a;

        }

        public decimal SumAmount { get; set; }
        public string BTBOpening { get; set; }
        public decimal? ValueForBTBOpening { get; set; }
        public decimal Balance { get; set; }

        [DisplayName("Value")]
        public decimal Amendment { get; set; }
        public static int DisplayName { get; private set; }

        public void GetB2BLCByMasterLC(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join Commercial_MasterLC in db.Commercial_MasterLC
                     on Commercial_B2bLC.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Common_Supplier in db.Common_Supplier
                     on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                     where Commercial_B2bLC.Commercial_MasterLCFK == id && Commercial_MasterLC.Active == true

                     select new VmCommercial_B2bLC
                     {
                         Common_Supplier = Common_Supplier,
                         Commercial_MasterLC = Commercial_MasterLC,
                         Commercial_B2bLC = Commercial_B2bLC
                         //BTBOpening = this.VmCommercial_MasterLCBuyerPO.ActualValueOf75Percent
                         //Amendment = Commercial_B2bLC.Amount - Commercial_B2bLC.AmendmentValue
                     }).Where(x => x.Commercial_B2bLC.Active == true).OrderByDescending(x => x.Commercial_B2bLC.ID).AsEnumerable();
            //decimal actual75 = 0;
            //foreach (var i in a)
            //{
            //    ///
            //    decimal.TryParse(i.ActualValueOf75, out actual75);
            //}



            //this.ValueForB2B75 = actual75;
            //this.SumAmount = a.Sum(x => x.Commercial_B2bLC.Amount);
            //this.Balance = actual75 - SumAmount;
            this.DataList = a;

        }

        public decimal? TotalRequired { get; set; }
        public void GetB2BLCByUD(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join commercial_UD in db.Commercial_UD
                     on Commercial_B2bLC.Commercial_UDFK equals commercial_UD.ID
                     join Common_Supplier in db.Common_Supplier
                     on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                     //join Common_Unit in db.Common_Unit
                     //on Commercial_B2bLC.Common_UnitFK equals Common_Unit.ID
                     join Commercial_LCOregin in db.Commercial_LCOregin
                     on Commercial_B2bLC.Commercial_LCOreginFK equals Commercial_LCOregin.ID
                     join Commercial_BtBLCType in db.Commercial_BtBLCType
                     on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BtBLCType.ID
                     join Commercial_BTBItem in db.Commercial_BTBItem
                     on Commercial_B2bLC.Commercial_BTBItemFK equals Commercial_BTBItem.ID

                     where Commercial_B2bLC.Commercial_UDFK == id
                     && commercial_UD.Active == true
                     && Common_Supplier.Active == true
                     && Commercial_LCOregin.Active == true
                     && Commercial_BtBLCType.Active == true
                     && Commercial_BTBItem.Active == true

                     select new VmCommercial_B2bLC
                     {
                         Common_Supplier = Common_Supplier,
                         Commercial_UD = commercial_UD,
                         Commercial_LCOregin = Commercial_LCOregin,
                         Commercial_BtBLCType = Commercial_BtBLCType,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_BTBItem = Commercial_BTBItem,
                        // Common_Unit = Common_Unit
                         // Amendment = Commercial_B2bLC.Amount - Commercial_B2bLC.AmendmentValue
                     }).Where(x => x.Commercial_B2bLC.Active == true).OrderByDescending(x => x.Commercial_B2bLC.ID).OrderByDescending(x => x.Commercial_BTBItem.Name).AsEnumerable();


            //this.BTBOpening = VmCommercial_UDSlave.BTBOpeningStr;

            VmCommercial_B2bLcSupplierPO VmCommercial_B2bLcSupplierPO = new VmCommercial_B2bLcSupplierPO();
            decimal bTBOpening = 0;

            this.ValueForBTBOpening = bTBOpening;
            this.SumAmount = a.Sum(x => x.Commercial_B2bLC.Amount);
            this.TotalRequired = a.Sum(x => x.Commercial_B2bLC.RequiredQuantity);
            
            this.Balance = bTBOpening - SumAmount;

            b2B = new List<List<B2bLcSupplierPOView>>();

            List<VmCommercial_B2bLC> list = new List<VmCommercial_B2bLC>();
            foreach (VmCommercial_B2bLC vmCommercialB2bLc in a)
            {
                //var b = VmCommercial_B2bLcSupplierPO.GetB2BLcSupplierPo(vmCommercialB2bLc.Commercial_B2bLC.ID);
                var c = VmCommercial_B2bLcSupplierPO.GetTotalPoValue(vmCommercialB2bLc.Commercial_B2bLC.ID);
                //b2B.Add(b);
                vmCommercialB2bLc.PoValue = c;
                list.Add(vmCommercialB2bLc);

            }
            this.DataList = list;

        }
        public VmCommercial_B2bLC SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join buyerBank in db.Commercial_Bank
                     on Commercial_MasterLC.Commercial_BuyerBankFK equals buyerBank.ID
                     join lienBank in db.Commercial_Bank
                    on Commercial_MasterLC.Commercial_LienBankFK equals lienBank.ID
                     where Commercial_MasterLC.Active == true
                     select new VmCommercial_B2bLC
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer,
                         Buyer_Bank = buyerBank,
                         Lien_Bank = lienBank

                     }).FirstOrDefault(x => x.Commercial_MasterLC.ID == iD);
            return v;
        }

        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join Commercial_UD in db.Commercial_UD
                     on Commercial_B2bLC.Commercial_UDFK equals Commercial_UD.ID
                     join Common_Supplier in db.Common_Supplier
                     on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                     join Commercial_LCOregin in db.Commercial_LCOregin
                    on Commercial_B2bLC.Commercial_LCOreginFK equals Commercial_LCOregin.ID
                     join Commercial_BtBLCType in db.Commercial_BtBLCType
                     on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BtBLCType.ID
                     select new VmCommercial_B2bLC
                     {
                         Common_Supplier = Common_Supplier,
                         Commercial_UD = Commercial_UD,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_BtBLCType = Commercial_BtBLCType,
                         Commercial_LCOregin = Commercial_LCOregin
                     }).SingleOrDefault(c => c.Commercial_B2bLC.ID == iD);
            this.Commercial_B2bLC = v.Commercial_B2bLC;

            this.Commercial_UD = v.Commercial_UD;
            this.Common_Supplier = v.Common_Supplier;
            this.Commercial_LCOregin = v.Commercial_LCOregin;
            this.Commercial_BtBLCType = v.Commercial_BtBLCType;
            this.Common_Currency = v.Common_Currency;


        }

        public string Upload(string imagePath)
        {
            string s = "";
            //Photo.FileName = imagePath;
            try
            {
                string exten = Path.GetFileName(ScanedFile.FileName);
                string fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                string filePath = Path.Combine(imagePath + fName);
                ScanedFile.SaveAs(filePath);
                Commercial_B2bLC.ScanedFile = fName;

            }
            catch (Exception ex)
            {

                s = ex.Message;

            }
            return s;

        }

        public int Add(int userID, string imagePath)
        {
           
            Commercial_B2bLC.AddReady(userID);
            Upload(imagePath);
            return Commercial_B2bLC.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_B2bLC.Edit(userID);
        }
        public int B2BPaymentAdd(int userID)
        {
            Commercial_B2BPaymentInformation.Commercial_B2bLCFK = Commercial_B2bLC.ID;
            Commercial_B2BPaymentInformation.AddReady(userID);
            return Commercial_B2BPaymentInformation.Add();
        }
        public bool EditUser(int userID, string imagePath)
        {
            if (Commercial_B2bLC.AmendmentIncrease == 0)
            {
                Commercial_B2bLC.AmendmentIncrease = this.AmendmentIncrease;
                Commercial_B2bLC.Amount = this.Commercial_B2bLC.Amount + this.AmendmentIncrease;
            }
            else if (Commercial_B2bLC.AmendmentIncrease != 0)
            {
                Commercial_B2bLC.AmendmentIncrease = this.Commercial_B2bLC.AmendmentIncrease + this.AmendmentIncrease;
                Commercial_B2bLC.Amount = this.Commercial_B2bLC.Amount + this.AmendmentIncrease;
            }

            if (Commercial_B2bLC.AmendmentDecrease == 0)
            {
                Commercial_B2bLC.AmendmentDecrease = this.AmendmentDecrease;
                Commercial_B2bLC.Amount = this.Commercial_B2bLC.Amount - this.AmendmentDecrease;
            }
            else if (Commercial_B2bLC.AmendmentDecrease != 0)
            {
                Commercial_B2bLC.AmendmentDecrease = this.Commercial_B2bLC.AmendmentDecrease + this.AmendmentDecrease;
                Commercial_B2bLC.Amount = this.Commercial_B2bLC.Amount - this.AmendmentDecrease;
            }
            Commercial_B2bLC.Edit(0);
            Upload(imagePath);
            return Commercial_B2bLC.Edit(userID);
        }

        internal void SelectSingleLCInfo(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join Commercial_UDSlave in db.Commercial_UDSlave
                     on Commercial_B2bLC.Commercial_UDFK equals Commercial_UDSlave.Commercial_UDFK
                     join Commercial_UD in db.Commercial_UD
                    on Commercial_UDSlave.Commercial_UDFK equals Commercial_UD.ID
                     select new VmCommercial_B2bLC
                     {
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_UDSlave = Commercial_UDSlave,
                         Commercial_UD = Commercial_UD
                     }).FirstOrDefault(c => c.Commercial_B2bLC.ID == iD);

            this.Commercial_B2bLC = v.Commercial_B2bLC;
            this.Commercial_UDSlave = v.Commercial_UDSlave;
            this.Commercial_UD = v.Commercial_UD;
            this.Commercial_B2BPaymentInformation = new Commercial_B2BPaymentInformation();
            this.Commercial_B2BPaymentInformation.Date = DateTime.Now;
            this.Commercial_B2BPaymentInformation.Commercial_B2bLCFK = iD;
            this.Commercial_B2BPaymentInformation.B2bLCPayment = GetB2BLCWiseTotalSummary(iD);

            GetCommercial_B2BPaymentInformation(iD);

        }

        public decimal GetB2BLCWiseTotalSummary(int id)
        {
            decimal b2bLcPaymentHistory = 0;
            decimal b2bLcAmmount = 0;

            db = new xOssContext();
            b2bLcAmmount = GetB2BLCAmmount(id);
            b2bLcPaymentHistory = GetB2BLCWisePaymentSummary(id);

            return b2bLcAmmount - b2bLcPaymentHistory;
        }
        public decimal GetB2BLCAmmount(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_B2bLC
                     where t1.ID == id && t1.Active == true
                     select (t1.Amount)).SingleOrDefault();

            return a;
        }

        public decimal GetB2BLCWisePaymentSummary(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_B2BPaymentInformation
                     
                     where t1.Commercial_B2bLCFK == id && t1.Active == true
                     select t1.B2bLCPayment).ToList();

            decimal paymentInfo = 0;


            if (v.Count() != 0)
            {
                var a = (from t1 in db.Commercial_B2BPaymentInformation

                         where t1.Commercial_B2bLCFK == id && t1.Active == true
                         select (t1.B2bLCPayment)).Sum();
                paymentInfo = a;
            }

            return paymentInfo;
        }

        public void GetCommercial_B2BPaymentInformation(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_B2BPaymentInformation
                     //join t2 in db.Commercial_B2bLC
                     //on t1.Commercial_B2bLCFK equals t2.ID
                     //join t3 in db.Commercial_UDSlave
                     //on t2.Commercial_UDFK equals t3.Commercial_UDFK
                     where t1.Commercial_B2bLCFK == id && t1.Active == true
                     select new VmCommercial_B2bLC
                     {
                         Commercial_B2BPaymentInformation = t1,
                         //Commercial_B2bLC = t2,
                         //Commercial_UDSlave = t3
                     }).AsEnumerable();
            this.DataList = v;
        }

        public int Add(int userID)
        {
            Commercial_B2bLC.AddReady(userID);
            return Commercial_B2bLC.Add();

        }

        public void InitialDataLoadForBTB()
        {
            db = new xOssContext();
            var a = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join Common_Supplier in db.Common_Supplier
                     on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                     join Common_Unit in db.Common_Unit
                     on Commercial_B2bLC.Common_UnitFK equals Common_Unit.ID
                     join Commercial_LCOregin in db.Commercial_LCOregin
                     on Commercial_B2bLC.Commercial_LCOreginFK equals Commercial_LCOregin.ID
                     join Commercial_BtBLCType in db.Commercial_BtBLCType
                     on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BtBLCType.ID
                     join Commercial_UD in db.Commercial_UD
                      on Commercial_B2bLC.Commercial_UDFK equals Commercial_UD.ID
                     join Commercial_BTBItem in db.Commercial_BTBItem
                     on Commercial_B2bLC.Commercial_BTBItemFK equals Commercial_BTBItem.ID

                     //join commercial_B2BPaymentInformation in db.Commercial_B2BPaymentInformation
                     //on Commercial_B2bLC.ID equals commercial_B2BPaymentInformation.Commercial_B2bLCFK

                     select new VmCommercial_B2bLC
                     {
                         Common_Supplier = Common_Supplier,
                         Commercial_LCOregin = Commercial_LCOregin,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_BtBLCType = Commercial_BtBLCType,
                         Commercial_UD = Commercial_UD,
                         Commercial_BTBItem = Commercial_BTBItem,
                         Common_Unit = Common_Unit
                         //Commercial_B2BPaymentInformation = commercial_B2BPaymentInformation
                     }).Where(x => x.Commercial_B2bLC.Active == true).OrderByDescending(x => x.Commercial_B2bLC.ID).OrderByDescending(x => x.Commercial_UD.UdNo).AsEnumerable();
            List<VmCommercial_B2bLC> list = new List<VmCommercial_B2bLC>();
            foreach (VmCommercial_B2bLC x in a)
            {
                x.B2bLcPaymentHistory = GetB2BLCWisePaymentSummary(x.Commercial_B2bLC.ID);
                x.PaybleBTB = GetB2BLCWiseTotalSummary(x.Commercial_B2bLC.ID);
                list.Add(x);
            }

            this.DataList = list;
        }

        public void InitialDataLoadForImportBTB(int Id)
        {
            db = new xOssContext();
            var a = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join Common_Supplier in db.Common_Supplier
                     on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID

                     join Commercial_LCOregin in db.Commercial_LCOregin
                     on Commercial_B2bLC.Commercial_LCOreginFK equals Commercial_LCOregin.ID
                     join Commercial_BtBLCType in db.Commercial_BtBLCType
                     on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BtBLCType.ID
                     join Commercial_UD in db.Commercial_UD
                      on Commercial_B2bLC.Commercial_UDFK equals Commercial_UD.ID
                     join Commercial_BTBItem in db.Commercial_BTBItem
                     on Commercial_B2bLC.Commercial_BTBItemFK equals Commercial_BTBItem.ID

                     //join commercial_B2BPaymentInformation in db.Commercial_B2BPaymentInformation
                     //on Commercial_B2bLC.ID equals commercial_B2BPaymentInformation.Commercial_B2bLCFK

                     select new VmCommercial_B2bLC
                     {
                         Common_Supplier = Common_Supplier,
                         Commercial_LCOregin = Commercial_LCOregin,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_BtBLCType = Commercial_BtBLCType,
                         Commercial_UD = Commercial_UD,
                         Commercial_BTBItem = Commercial_BTBItem,
                         //Commercial_B2BPaymentInformation = commercial_B2BPaymentInformation
                     }).Where(x => x.Commercial_B2bLC.Active == true && x.Commercial_B2bLC.Commercial_LCOreginFK == Id).OrderByDescending(x => x.Commercial_B2bLC.ID).OrderByDescending(x => x.Commercial_UD.UdNo).AsEnumerable();
            List<VmCommercial_B2bLC> list = new List<VmCommercial_B2bLC>();
            foreach (VmCommercial_B2bLC x in a)
            {
                x.B2bLcPaymentHistory = GetB2BLCWisePaymentSummary(x.Commercial_B2bLC.ID);
                x.PaybleBTB = GetB2BLCWiseTotalSummary(x.Commercial_B2bLC.ID);
                list.Add(x);
            }

            this.DataList = list;
        }

        public void SelectSingleForBTB(int id)
        {
            db = new xOssContext();
            var v = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join Common_Supplier in db.Common_Supplier
                     on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                     join Common_Unit in db.Common_Unit
                     on Commercial_B2bLC.Common_UnitFK equals Common_Unit.ID
                     join Commercial_LCOregin in db.Commercial_LCOregin
                     on Commercial_B2bLC.Commercial_LCOreginFK equals Commercial_LCOregin.ID
                     join Commercial_BtBLCType in db.Commercial_BtBLCType
                     on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BtBLCType.ID
                     select new VmCommercial_B2bLC
                     {
                         Common_Supplier = Common_Supplier,
                         Commercial_LCOregin = Commercial_LCOregin,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_BtBLCType = Commercial_BtBLCType,
                         Common_Unit= Common_Unit
                     }).SingleOrDefault(c => c.Commercial_B2bLC.ID == id);
            this.Commercial_B2bLC = v.Commercial_B2bLC;
            this.Common_Supplier = v.Common_Supplier;
            this.Commercial_LCOregin = v.Commercial_LCOregin;
            this.Commercial_BtBLCType = v.Commercial_BtBLCType;
            this.Common_Unit = v.Common_Unit;


        }





        public List<VmCommercial_B2bLC> GetB2BLCByUDForReport(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_B2bLcSupplierPO in db.Commercial_B2bLcSupplierPO
                     join Commercial_B2bLcSupplierPOSlave in db.Commercial_B2bLcSupplierPOSlave
                     on Commercial_B2bLcSupplierPO.ID equals Commercial_B2bLcSupplierPOSlave.Commercial_B2bLcSupplierPOFK
                     join Mkt_POSlave in db.Mkt_POSlave
                     on Commercial_B2bLcSupplierPOSlave.Mkt_POSlaveFK equals Mkt_POSlave.ID
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Commercial_B2bLC in db.Commercial_B2bLC
                     on Commercial_B2bLcSupplierPO.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     join Commercial_UD in db.Commercial_UD
                     on Commercial_B2bLC.Commercial_UDFK equals Commercial_UD.ID
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     where Commercial_UD.ID == id

                           && Commercial_B2bLcSupplierPO.Active == true
                           && Commercial_B2bLcSupplierPOSlave.Active == true
                           && Mkt_POSlave.Active == true
                           && Mkt_PO.Active == true
                           && Raw_Item.Active == true
                           && Commercial_UD.Active == true
                           && Commercial_B2bLC.Active == true
                           && Common_Supplier.Active == true
                     group new { Raw_Item, Common_Supplier, Mkt_POSlave, Common_Unit, Commercial_B2bLcSupplierPOSlave } by new
                {
                    //Commercial_B2bLC= Commercial_B2bLC,
                    Raw_Item = Raw_Item,
                    Common_Supplier = Common_Supplier,
                    Common_Unit = Common_Unit,
                    Mkt_PO = Mkt_PO,
                    Mkt_POSlave= Mkt_POSlave,
                    Commercial_B2bLcSupplierPOSlave = Commercial_B2bLcSupplierPOSlave.Mkt_POSlaveFK
                         //UnitPrice=Mkt_POSlave.Price
                     } into gr
                select new VmCommercial_B2bLC
                {
                    Common_Supplier=gr.Key.Common_Supplier,
                    Raw_Item = gr.Key.Raw_Item,
                    Common_Unit= gr.Key.Common_Unit,
                    Mkt_PO = gr.Key.Mkt_PO,
                    Mkt_POSlave = gr.Key.Mkt_POSlave,
                    Sid = gr.Key.Commercial_B2bLcSupplierPOSlave.Value
                    //Commercial_B2bLC = gr.Key.Commercial_B2bLC

                }).ToList();


            //this.BTBOpening = VmCommercial_UDSlave.BTBOpeningStr;

            VmCommercial_B2bLcSupplierPO VmCommercial_B2bLcSupplierPO = new VmCommercial_B2bLcSupplierPO();
            decimal bTBOpening = 0;

            //this.ValueForBTBOpening = bTBOpening;
            //this.SumAmount = a.Sum(x => x.Commercial_B2bLC.Amount);
            //this.Balance = bTBOpening - SumAmount;

            b2B = new List<List<B2bLcSupplierPOView>>();

            List<VmCommercial_B2bLC> list = new List<VmCommercial_B2bLC>();
            //foreach (VmCommercial_B2bLC vmCommercialB2bLc in a)
            //{

            //    // list.Add(vmCommercialB2bLc);

            //}
            foreach (VmCommercial_B2bLC vmCommercialB2bLc in a)
            {
                List<B2bLcSupplierPOView> b = VmCommercial_B2bLcSupplierPO.GetB2BLcSupplierPo(vmCommercialB2bLc.Sid, vmCommercialB2bLc.Mkt_PO.CID);
                b2B.Add(b);
                //vmCommercialB2bLc.PoValue = VmCommercial_B2bLcSupplierPO.GetTotalPoValue(vmCommercialB2bLc.Commercial_B2bLC.ID);
                vmCommercialB2bLc.b2B = b2B;
                list.Add(vmCommercialB2bLc);
            }
            return list;
        }







    }
}