﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_B2BLcSupplierPI
    {
        private xOssContext db;
       
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        public Commercial_B2BLcSupplierPI Commercial_B2BLcSupplierPI { get; set; }
        public Commercial_PI Commercial_PI { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public IEnumerable<VmCommercial_B2BLcSupplierPI> DataList { get; set; }
        public List<object> DropDownList { get; set; }
        public int Add(int userID)
        {
            Commercial_B2BLcSupplierPI.AddReady(userID);
            return Commercial_B2BLcSupplierPI.Add();
        }
        public bool Edit(int userID)
        {
            return Commercial_B2BLcSupplierPI.Edit(userID);
        }

        public VmCommercial_B2BLcSupplierPI SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_B2bLC in db.Commercial_B2bLC
                     join Commercial_MasterLC in db.Commercial_MasterLC
                     on Commercial_B2bLC.Commercial_MasterLCFK equals Commercial_MasterLC.ID

                     select new VmCommercial_B2BLcSupplierPI
                     {
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_MasterLC = Commercial_MasterLC
                     }).Where(x => x.Commercial_B2bLC.ID == iD).FirstOrDefault();
            return v;
        }
        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_B2BLcSupplierPI in db.Commercial_B2BLcSupplierPI
                     join Commercial_B2bLC in db.Commercial_B2bLC on Commercial_B2BLcSupplierPI.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     join Commercial_PI in db.Commercial_PI on Commercial_B2BLcSupplierPI.Commercial_PIFK equals Commercial_PI.ID
                     select new VmCommercial_B2BLcSupplierPI
                     {
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_PI = Commercial_PI,
                         Commercial_B2BLcSupplierPI = Commercial_B2BLcSupplierPI

                     }).Where(x => x.Commercial_B2BLcSupplierPI.Active == true && x.Commercial_B2bLC.ID == id)
                       .OrderByDescending(x => x.Commercial_B2BLcSupplierPI.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_B2BLcSupplierPI in db.Commercial_B2BLcSupplierPI
                     join Commercial_B2bLC in db.Commercial_B2bLC on Commercial_B2BLcSupplierPI.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     join Commercial_PI in db.Commercial_PI on Commercial_B2BLcSupplierPI.Commercial_PIFK equals Commercial_PI.ID
                     select new VmCommercial_B2BLcSupplierPI
                     {
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_PI = Commercial_PI,
                         Commercial_B2BLcSupplierPI = Commercial_B2BLcSupplierPI

                     }).Where(c => c.Commercial_B2BLcSupplierPI.ID == iD).SingleOrDefault();
            this.Commercial_B2BLcSupplierPI = v.Commercial_B2BLcSupplierPI;
            this.Commercial_B2bLC = v.Commercial_B2bLC;
            this.Commercial_PI = v.Commercial_PI;
        }
    }
}