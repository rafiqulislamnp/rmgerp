﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Microsoft.Data.Edm.Library.Values;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.Store;

namespace Oss.Romo.ViewModels.Commercial
{
    public class B2bLcSupplierPOView
    {
        public string CID { get; set; }
        public decimal TotalQuantity { get; set; }
        public string IpoNo { get; set; }
        public decimal? BTBValue { get; set; }
        public decimal? POValue { get; set; }
        public string BTBNo { get; set; }
        public int Mkt_POSlaveFK { get; set; }
        public string UniName { get; set; }
        public int BTBID { get; set; }
        public decimal PaidBTBValue { get; set; }
    }

    public class SupplierListByUD
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public int B2bLCId { get; set; }
    }

    public class LCStatementReport
    {
        public List<SupplierListByUD> SupplierListByUD { get; set; }
        public List<List<B2bLcSupplierPOView>> B2bLcSupplierPOView { get; set; }
    }
    public class VmCommercial_B2bLcSupplierPO
    {
        private xOssContext db;
        public Mkt_PO Mkt_PO { get; set; }
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Commercial_B2bLcSupplierPO Commercial_B2bLcSupplierPO { get; set; }
        public Commercial_B2bLcSupplierPOSlave Commercial_B2bLcSupplierPOSlave { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Commercial_LCOregin Commercial_LCOregin { get; set; }
        public Commercial_BtBLCType Commercial_BtBLCType { get; set; }
        public IEnumerable<VmCommercial_B2bLcSupplierPO> DataList { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
        public List<object> DropDownList { get; set; }
        [DisplayName("PO Value")]
        public decimal? TotalPOValue { get; set; }
        [DisplayName("Required Qty")]
        public string RequiredQty { get; set; }
        [DisplayName("Remaining Qty")]
        public string RemainingQty { get; set; }

        public decimal? AllPOValue { get; set; }
        public string AllPOValueStr { get; set; }
        public int Add(int userID)
        {
            Commercial_B2bLcSupplierPO.AddReady(userID);
            return Commercial_B2bLcSupplierPO.Add();
        }
        public bool Edit(int userID)
        {
            return Commercial_B2bLcSupplierPO.Edit(userID);
        }

        public VmCommercial_B2bLcSupplierPO SelectSingleJoined(int id)
        {
            db = new xOssContext();
            var b = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join Common_Supplier in db.Common_Supplier
                     on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                     join Commercial_LCOregin in db.Commercial_LCOregin
                     on Commercial_B2bLC.Commercial_LCOreginFK equals Commercial_LCOregin.ID
                     join Commercial_BtBLCType in db.Commercial_BtBLCType
                     on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BtBLCType.ID
                     join Commercial_UD in db.Commercial_UD
                    on Commercial_B2bLC.Commercial_UDFK equals Commercial_UD.ID
                     select new VmCommercial_B2bLcSupplierPO
                     {
                         Common_Supplier = Common_Supplier,
                         Commercial_LCOregin = Commercial_LCOregin,
                         Commercial_BtBLCType = Commercial_BtBLCType,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_UD = Commercial_UD
                     }).Where(x => x.Commercial_B2bLC.ID == id).FirstOrDefault();
            return b;
        }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_B2bLcSupplierPO in db.Commercial_B2bLcSupplierPO
                     join Commercial_B2bLC in db.Commercial_B2bLC on Commercial_B2bLcSupplierPO.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     join Mkt_PO in db.Mkt_PO on Commercial_B2bLcSupplierPO.Mkt_POFK equals Mkt_PO.ID                    
                     join b2BLcSupplierPoSlave in db.Commercial_B2bLcSupplierPOSlave on Commercial_B2bLcSupplierPO.ID equals b2BLcSupplierPoSlave.Commercial_B2bLcSupplierPOFK
                     join Mkt_POSlave in db.Mkt_POSlave on b2BLcSupplierPoSlave.Mkt_POSlaveFK equals Mkt_POSlave.ID
                     join Raw_Item in db.Raw_Item on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID

                     select new VmCommercial_B2bLcSupplierPO
                     {
                         Commercial_B2bLC = Commercial_B2bLC,
                         Mkt_PO = Mkt_PO,
                         Mkt_POSlave = Mkt_POSlave,
                         Commercial_B2bLcSupplierPO = Commercial_B2bLcSupplierPO,
                         Raw_Item= Raw_Item,
                         Commercial_B2bLcSupplierPOSlave= b2BLcSupplierPoSlave
                     }).Where(x => x.Commercial_B2bLcSupplierPO.Active == true && x.Commercial_B2bLC.ID == id)
                       .OrderByDescending(x => x.Commercial_B2bLcSupplierPO.ID).AsEnumerable();
            List<VmCommercial_B2bLcSupplierPO> list = new List<VmCommercial_B2bLcSupplierPO>();
            foreach (VmCommercial_B2bLcSupplierPO x in a)
            {
                //VmMkt_PO vmMkt_PO = new VmMkt_PO();
                x.TotalPOValue = x.Commercial_B2bLcSupplierPOSlave.TotalRequired*x.Mkt_POSlave.Price; /*vmMkt_PO.TotlaValueForSupplierBtB(x.Mkt_PO.ID);*/
                list.Add(x);
            }
            this.AllPOValue = list.Sum(x => x.TotalPOValue);
            AllPOValueStr = this.AllPOValue.Value.ToString("F");
            this.DataList = list;
        }
        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_B2bLcSupplierPO in db.Commercial_B2bLcSupplierPO
                     join Commercial_B2bLcSupplierPOSlave in db.Commercial_B2bLcSupplierPOSlave on Commercial_B2bLcSupplierPO.ID equals Commercial_B2bLcSupplierPOSlave.Commercial_B2bLcSupplierPOFK
                     join Commercial_B2bLC in db.Commercial_B2bLC on Commercial_B2bLcSupplierPO.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     join Mkt_PO in db.Mkt_PO on Commercial_B2bLcSupplierPO.Mkt_POFK equals Mkt_PO.ID                    
                     join Mkt_POSlave in db.Mkt_POSlave on Commercial_B2bLcSupplierPOSlave.Mkt_POSlaveFK equals Mkt_POSlave.ID
                     join Raw_Item in db.Raw_Item on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     select new VmCommercial_B2bLcSupplierPO
                     {
                         Commercial_B2bLC = Commercial_B2bLC,
                         Mkt_PO = Mkt_PO,
                         Commercial_B2bLcSupplierPO = Commercial_B2bLcSupplierPO,
                         Commercial_B2bLcSupplierPOSlave = Commercial_B2bLcSupplierPOSlave,
                         Raw_Item= Raw_Item
                     }).Where(c => c.Commercial_B2bLcSupplierPO.ID == iD).SingleOrDefault();
            this.Commercial_B2bLcSupplierPO = v.Commercial_B2bLcSupplierPO;
            this.Commercial_B2bLC = v.Commercial_B2bLC;
            this.Commercial_B2bLcSupplierPOSlave = v.Commercial_B2bLcSupplierPOSlave;
            this.Mkt_PO = v.Mkt_PO;
            this.Raw_Item = v.Raw_Item;
        }

        public List<B2bLcSupplierPOView> GetB2BLcSupplierPo(int sid,string cid)
        {
            db = new xOssContext();
            
            var a = (from Commercial_B2bLcSupplierPOSlave in db.Commercial_B2bLcSupplierPOSlave
                     join Commercial_B2bLcSupplierPO in db.Commercial_B2bLcSupplierPO 
                     on Commercial_B2bLcSupplierPOSlave.Commercial_B2bLcSupplierPOFK equals Commercial_B2bLcSupplierPO.ID
                     join Mkt_POSlave in db.Mkt_POSlave on Commercial_B2bLcSupplierPOSlave.Mkt_POSlaveFK equals Mkt_POSlave.ID
                     join Mkt_PO in db.Mkt_PO on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Commercial_B2bLC in db.Commercial_B2bLC on Commercial_B2bLcSupplierPO.Commercial_B2bLCFK equals Commercial_B2bLC.ID
                     join Common_Unit in db.Common_Unit on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     where Commercial_B2bLcSupplierPOSlave.Mkt_POSlaveFK == sid && Mkt_PO.CID==cid 
                     && Commercial_B2bLcSupplierPOSlave.Active == true
                     && Commercial_B2bLcSupplierPO.Active == true
                     && Mkt_POSlave.Active == true
                     && Mkt_PO.Active == true
                     && Commercial_B2bLC.Active == true
                     && Common_Unit.Active == true
                     select new B2bLcSupplierPOView
                     {
                         TotalQuantity = Commercial_B2bLcSupplierPOSlave.TotalRequired.Value,
                         IpoNo = Mkt_PO.CID,
                         POValue = Commercial_B2bLcSupplierPOSlave.TotalRequired* Mkt_POSlave.Price,
                         BTBNo = Commercial_B2bLC.Name,
                         BTBValue = Commercial_B2bLC.Amount,
                         CID=Mkt_PO.CID,
                         Mkt_POSlaveFK = Commercial_B2bLcSupplierPOSlave.Mkt_POSlaveFK.Value,
                         UniName = Common_Unit.Name,
                         BTBID = Commercial_B2bLC.ID
                     }).ToList();


            List<B2bLcSupplierPOView> xList = new List<B2bLcSupplierPOView>();
            foreach (B2bLcSupplierPOView x in a)
            {
              x.PaidBTBValue = GetPaidBtbValue(x.BTBID);
                xList.Add(x);
            }

            return xList;
        }

        private decimal GetPaidBtbValue(int bTBID)
        {
           db = new xOssContext();
            var a = (from t1 in db.Commercial_B2BPaymentInformation
                where t1.Active == true && t1.Commercial_B2bLCFK == bTBID
                select t1.B2bLCPayment).ToList();
            decimal b2bLCPayment = 0;
            if (a != null)
            {
                b2bLCPayment = a.Sum();
            }
            return b2bLCPayment;
        }

        public decimal GetTotalPoValue(int id)
        {
            db = new xOssContext();
            var totalpovalue = from Commercial_B2bLcSupplierPO in db.Commercial_B2bLcSupplierPO
                join Commercial_B2bLC in db.Commercial_B2bLC on Commercial_B2bLcSupplierPO.Commercial_B2bLCFK equals
                    Commercial_B2bLC.ID
                join Mkt_PO in db.Mkt_PO on Commercial_B2bLcSupplierPO.Mkt_POFK equals Mkt_PO.ID
                join Mkt_POSlave in db.Mkt_POSlave on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                where Commercial_B2bLC.ID == id && Commercial_B2bLcSupplierPO.Active == true
                select new
                {
                    Price= Mkt_POSlave.TotalRequired * Mkt_POSlave.Price
                };
            if (totalpovalue.Count()!=0)
            {
                return totalpovalue.Sum(x => x.Price.Value);
            }
            return 0;

        }

        //public LCStatementReport SupplierByUDReport(int id)
        //{
        //    db = new xOssContext();
        //    LCStatementReport lcStatement = new LCStatementReport();
        //    var a=(from commercial_B2bLC in db.Commercial_B2bLC
        //        join commercial_UD in db.Commercial_UD on commercial_B2bLC.Commercial_UDFK equals commercial_UD.ID
        //        join common_Supplier in db.Common_Supplier  on commercial_B2bLC.Common_SupplierFK equals common_Supplier.ID
        //        where commercial_B2bLC.Commercial_UDFK == id
        //        && commercial_UD.Active == true
        //        && common_Supplier.Active == true
        //            group new { commercial_B2bLC, common_Supplier } by new
        //            {
        //                SupplierId = common_Supplier.ID,
        //                SupplierName = common_Supplier.Name,
        //                B2bLCId = Commercial_B2bLC.ID                                           
        //            } into gr
        //            select new SupplierListByUD
        //            {
        //                SupplierId = gr.Key.SupplierId,
        //                B2bLCId = gr.Key.B2bLCId,
        //                SupplierName = gr.Key.SupplierName                      
        //            })
        //        .ToList();
        //    lcStatement.SupplierListByUD = a;
        //    List<List<B2bLcSupplierPOView>> b2B=new List<List<B2bLcSupplierPOView>>();
        //    foreach (var supplierListByUd in a)
        //    {
        //        var b = GetB2BLcSupplierPo(supplierListByUd.B2bLCId);
        //        b2B.Add(b);
        //    }
        //    lcStatement.B2bLcSupplierPOView = b2B;
        //    return lcStatement;
        //}

        internal int AddB2bLcSupplierPOSlave(int userID)
        {
            Commercial_B2bLcSupplierPOSlave.AddReady(userID);
            return Commercial_B2bLcSupplierPOSlave.Add();
        }

        public void GetQuantityForSupplierPo(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                     where t1.ID == id && t1.Active == true && t2.Active == true
                     select new
                     {
                         Quantity = t1.TotalRequired,
                         Unit = t2.Name
                     }).FirstOrDefault();

            var p = db.Commercial_B2bLcSupplierPOSlave.Where(x => x.Mkt_POSlaveFK == id && x.Active == true).Sum(x => x.TotalRequired);

            string nowReq = " ";
            if (p == null || p == 0)
            {
                nowReq = v.Quantity + " " + v.Unit;
            }
            else
            {
                nowReq = (v.Quantity - p.Value) + " " + v.Unit;
            }

            this.RequiredQty = v.Quantity + " " + v.Unit;
            this.RemainingQty = nowReq;
        }


        //public bool BulkDelete(int userID, int Id)
        //{
        //    using (db = new xOssContext())
        //    {
        //        var vData = db.Commercial_B2bLcSupplierPOSlave.Where(a => a.Commercial_B2bLcSupplierPOFK== Id && a.Active == true);
        //        if (vData.Any())
        //        {
        //            foreach (var item in vData)
        //            {
        //                item.Delete(userID);
        //            }
        //        }
        //    }
        //    return false;
        //}


        public bool BulkDelete(int userID, int Id)
        {
            db = new xOssContext();
            var key = db.Commercial_B2bLcSupplierPOSlave.Where(a => a.Commercial_B2bLcSupplierPOFK == Id && a.Active == true);
                if (key.Any())
                {
                    foreach (var a in key)
                    {
                       a.Delete(userID);
                    }
           
            }
            return false;
        }
    }
}