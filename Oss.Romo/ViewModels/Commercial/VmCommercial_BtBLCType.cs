﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_BtBLCType
    {
        private xOssContext db;
        public Commercial_BtBLCType Commercial_BtBLCType { get; set; }
        public IEnumerable<VmCommercial_BtBLCType> DataList { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_BtBLCType
                     select new VmCommercial_BtBLCType
                     {
                         Commercial_BtBLCType = t1

                     }).Where(x => x.Commercial_BtBLCType.Active == true).OrderByDescending(x => x.Commercial_BtBLCType.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_BtBLCType

                     select new VmCommercial_BtBLCType
                     {
                         Commercial_BtBLCType = t1

                     }).Where(c => c.Commercial_BtBLCType.ID == id).SingleOrDefault();
            this.Commercial_BtBLCType = v.Commercial_BtBLCType;

        }

        public int Add(int userID)
        {
            Commercial_BtBLCType.AddReady(userID);

            return Commercial_BtBLCType.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_BtBLCType.Edit(userID);
        }




    }
}