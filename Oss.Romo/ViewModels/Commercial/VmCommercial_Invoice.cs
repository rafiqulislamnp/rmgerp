﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_Invoice
    {
        private xOssContext db;
        public IEnumerable<VmCommercial_Invoice> DataList { get; set; }

        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Commercial_Invoice Commercial_Invoice { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public VmCommercial_UDSlave VmCommercial_UDSlave { get; set; }

        public Commercial_Bank Buyer_Bank { get; set; }
        public Commercial_Bank Lien_Bank { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Commercial_InvoiceSlave Commercial_InvoiceSlave { get; set; }
        public VmCommercial_MasterLCBuyerPO VmCommercial_MasterLCBuyerPO { get; set; }
        public List<ReportDocType> InvoiceReportDoc { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
        public List<TotalGenerateClass> TotalGenerateClass { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Commercial_Invoice in db.Commercial_Invoice
                         // join Commercial_MasterLC in db.Commercial_MasterLC
                         //on Commercial_Invoice.Commercial_MasterLCFk equals Commercial_MasterLC.ID
                         // join Commercial_UD in db.Commercial_UD
                         // on Commercial_Invoice.Commercial_UDFK equals Commercial_UD.ID

                     select new VmCommercial_Invoice
                     {
                         Commercial_Invoice = Commercial_Invoice,
                         TotalGenerateClass = (from t1 in db.Commercial_InvoiceSlave
                                               join t2 in db.Commercial_Invoice on t1.Commercial_InvoiceFk equals t2.ID
                                               join t3 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t3.ID
                                               join t0 in db.Shipment_OrderDeliverdSchedule on t1.Shipment_OrderDeliverdScheduleFK equals t0.ID
                                               join t4 in db.Mkt_BOM on t0.Mkt_BOMFK equals t4.ID
                                               join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                                               join t6 in db.Mkt_Item on t4.Mkt_ItemFK equals t6.ID
                                               join t7 in db.Common_Currency on t5.Common_CurrencyFK equals t7.ID
                                               where t1.Commercial_InvoiceFk == Commercial_Invoice.ID && t1.Active == true
                                               select new TotalGenerateClass
                                               {
                                                   TotalOrderQty = t0.DeliverdQty,
                                                   TotalCtnQty = t0.CtnQty,
                                                   TotalPackQty = t0.DeliverdQty / t4.Quantity,
                                                   TotalPrice = t4.PackPrice * (t0.DeliverdQty / t4.Quantity),
                                               }).ToList()
                         // Commercial_MasterLC = Commercial_MasterLC
                         //Commercial_UD = Commercial_UD
                     }).Where(x => x.Commercial_Invoice.Active == true).OrderByDescending(x => x.Commercial_Invoice.ID).AsEnumerable();
            this.DataList = a;

        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Commercial_Invoice in db.Commercial_Invoice
                         //join Commercial_MasterLC in db.Commercial_MasterLC
                         //on Commercial_Invoice.Commercial_MasterLCFk equals Commercial_MasterLC.ID

                     select new VmCommercial_Invoice
                     {
                         // Commercial_MasterLC = Commercial_MasterLC,

                         Commercial_Invoice = Commercial_Invoice
                     }).Where(c => c.Commercial_Invoice.ID == id).SingleOrDefault();
            this.Commercial_Invoice = v.Commercial_Invoice;
            //  this.Commercial_MasterLC = v.Commercial_MasterLC;

        }

        public VmCommercial_Invoice SelectSingleJoined(int id)
        {

            db = new xOssContext();

            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join buyerBank in db.Commercial_Bank
                     on Commercial_MasterLC.Commercial_BuyerBankFK equals buyerBank.ID

                     join lienBank in db.Commercial_Bank
                     on Commercial_MasterLC.Commercial_LienBankFK equals lienBank.ID
                     select new VmCommercial_Invoice
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer,
                         Buyer_Bank = buyerBank,
                         Lien_Bank = lienBank
                     }).Where(x => x.Commercial_MasterLC.ID == id).FirstOrDefault();
            return v;
        }

        // internal void InvoiceReportDocLoad(int id)
        //{

        //    db = new xOssContext();

        //    var a = (from Commercial_InvoiceSlave in db.Commercial_InvoiceSlave
        //             join Commercial_Invoice in db.Commercial_Invoice
        //             on Commercial_InvoiceSlave.Commercial_InvoiceFk equals Commercial_Invoice.ID
        //             join Commercial_MasterLC in db.Commercial_MasterLC
        //             on Commercial_Invoice.Commercial_MasterLCFk equals Commercial_MasterLC.ID
        //             join Mkt_BOM in db.Mkt_BOM
        //             on Commercial_InvoiceSlave.Mkt_BOMFK equals Mkt_BOM.ID
        //             join x in db.Mkt_Item
        //             on Mkt_BOM.Mkt_ItemFK equals x.ID
        //             join Common_TheOrder in db.Common_TheOrder
        //             on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
        //             join Buyer_Bank in db.Commercial_Bank
        //             on Commercial_MasterLC.Commercial_BuyerBankFK equals Buyer_Bank.ID
        //             join lien_Bank in db.Commercial_Bank
        //             on Commercial_MasterLC.Commercial_LienBankFK equals lien_Bank.ID
        //             join Mkt_Buyer in db.Mkt_Buyer
        //             on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
        //             join t1 in db.Commercial_UDSlave
        //             on Commercial_Invoice.Commercial_UDFK equals t1.Commercial_UDFK
        //             join t2 in db.Commercial_UD
        //             on t1.Commercial_UDFK equals t2.ID
        //             where Commercial_Invoice.ID == id && Commercial_MasterLC.Active == true
        //             && Commercial_Invoice.Active == true && Commercial_InvoiceSlave.Active == true
        //             select new ReportDocType
        //             {
        //                 HeaderLeft1 = Commercial_Invoice.InvoiceNo,
        //                 HeaderLeft2 = Commercial_Invoice.InvoiceDate.ToString(),
        //                 HeaderLeft3 = Commercial_MasterLC.Name,
        //                 HeaderLeft4 = Commercial_MasterLC.Date.ToString(),
        //                 HeaderLeft5 = Common_TheOrder.PaymentTerms,
        //                 HeaderLeft6 = t2.UdNo,
        //                 HeaderLeft7 = t2.Date.ToString(),
        //                 HeaderMiddle1 = lien_Bank.Name + "\n" + lien_Bank.Address,
        //                 HeaderMiddle2 = Commercial_Invoice.ErcNo,
        //                 HeaderMiddle3 = Commercial_Invoice.InvoiceIdNo,
        //                 HeaderMiddle4 = Commercial_Invoice.ShippedPer,
        //                 HeaderMiddle5 = Commercial_Invoice.PortOfLoding,                       
        //                 HeaderRight1 = Commercial_Invoice.PortOfDischarge,
        //                 HeaderRight2 = Commercial_Invoice.FinalDestination,
        //                 HeaderRight3 = Commercial_Invoice.BLNo,
        //                 HeaderRight4 = Commercial_Invoice.BLDate.ToString(),
        //                 HeaderRight5 = Commercial_MasterLC.NotifyParty,
        //                 HeaderRight6 = Buyer_Bank.Name + "\n" + Buyer_Bank.Address,                        
        //                 Body1 = Common_TheOrder.CID + "/"+ Mkt_BOM.Style + "\n" + Mkt_BOM.Fabrication,
        //                 Body2 = x.Name + "\n" + Mkt_BOM.Fabrication + "\n" +"PO "+ Common_TheOrder.BuyerPO ,

        //                 Body5 = Commercial_InvoiceSlave.InvoiceQPack.ToString(),
        //                 Body6 = ((Commercial_InvoiceSlave.InvoiceQPack * Mkt_BOM.Quantity).ToString()),
        //                 Body7 =  (((Mkt_BOM.QPack * Mkt_BOM.Quantity) * Mkt_BOM.UnitPrice) / Mkt_BOM.QPack).ToString(),
        //                 Body8=  (Commercial_InvoiceSlave.InvoiceQPack * Mkt_BOM.Quantity * Mkt_BOM.UnitPrice).ToString()


        //             }).ToList();
        //   decimal TOTALPACK = 0;
        //   decimal TOTALPCS = 0;

        //   decimal TOTALAMOUNT = 0;

        //    foreach (ReportDocType r in a)
        //    {
        //        decimal body5 = 0;
        //        decimal.TryParse(r.Body5, out body5);
        //        TOTALPACK += body5;


        //        decimal body6 = 0;
        //        decimal.TryParse(r.Body6, out body6);
        //        TOTALPCS += body6;

        //        decimal body7 = 0;
        //        decimal.TryParse(r.Body7, out body7);
        //        r.Body7 = "$ " + body7.ToString("F");

        //        decimal body8 = 0;
        //        decimal.TryParse(r.Body8, out body8);
        //        r.Body8 = body8.ToString("F");
        //        TOTALAMOUNT += body8;



        //    }
        //    foreach (ReportDocType r in a)
        //    {
        //        r.SubBody1 = TOTALPACK.ToString() + " PACK";
        //        r.SubBody2 = TOTALPCS.ToString() + " PCS";
        //        r.SubBody3 = "$ " + TOTALAMOUNT.ToString("F");

        //        Common.VmForDropDown ntw = new Common.VmForDropDown();

        //        r.SubBody4 = ntw.NumberToWords(TOTALAMOUNT, Common.CurrencyType.USD) + ".";



        //        r.HeaderLeft2 = r.HeaderLeft2.Substring(0, r.HeaderLeft2.Length - 8);
        //        r.HeaderLeft4 = r.HeaderLeft4.Substring(0, r.HeaderLeft4.Length - 8);             
        //        r.HeaderRight4 = r.HeaderRight4.Substring(0, r.HeaderRight4.Length - 8);
        //        r.HeaderLeft7 = r.HeaderLeft7.Substring(0, r.HeaderLeft7.Length - 8);


        //    }

        //    this.InvoiceReportDoc = a.ToList();
        //}



        public int Add(int userID)
        {
            Commercial_Invoice.AddReady(userID);
            return Commercial_Invoice.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_Invoice.Edit(userID);
        }


    }


    public class TotalGenerateClass
    {
        public decimal? TotalOrderQty { get; set; }
        public decimal? TotalPackQty { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? TotalCtnQty { get; set; }
    }
}