﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Store;
using Oss.Romo.Models.User;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmFilter
    {
        [Required, DisplayName("From Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        
        [Required, DisplayName("To Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        [Required, DisplayName("Filter By Bank")]
        public int FilterId { get; set; }
        [Required, DisplayName("Filter UD No")]
        public int UDId { get; set; }
    }


    public class VmImportReport
    {
        private xOssContext db;
        
        public VmFilter VmFilter { get; set; }
        public Commercial_Import Commercial_Import { get; set; }
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        public List<ReportDocType> ReportDoc { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Acc_POPH Acc_POPH { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Common_Currency Common_Currency { get; set; }

        public decimal? Balance{ get; set; }
        public bool IsETAORETD { get; set; }
        
        public List<ReportDocType> ImportReport { get; set; }
        public List<ReportDocType> YarnReport { get; set; }
        public Mkt_YarnType Mkt_YarnType { get; set; }
        public  Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        
        public void ImportReportLoad(VmFilter model)
        {
            using (db = new xOssContext())
            {
                List<ReportDocType> report = new List<ReportDocType>();
                int serial = 0;
                if (model.FilterId == 1)
                {
                    #region B2BL/C Date
                    var vData = (from Commercial_B2bLC in db.Commercial_B2bLC
                                 join Common_Supplier in db.Common_Supplier on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                                 join Commercial_BTBItem in db.Commercial_BTBItem on Commercial_B2bLC.Commercial_BTBItemFK equals Commercial_BTBItem.ID
                                 //join Commercial_UDSlave in db.Commercial_UDSlave on Commercial_B2bLC.Commercial_UDFK equals Commercial_UDSlave.Commercial_UDFK
                                 //join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_UDSlave.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                                 join Commercial_UD in db.Commercial_UD on Commercial_B2bLC.Commercial_UDFK equals Commercial_UD.ID
                                 //join Mkt_Buyer in db.Mkt_Buyer on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                                 join Common_Unit in db.Common_Unit on Commercial_B2bLC.Common_UnitFK equals Common_Unit.ID
                                 where
                                 Commercial_B2bLC.Date >= model.FromDate && Commercial_B2bLC.Date <= model.ToDate &&
                                 Commercial_B2bLC.Commercial_LCOreginFK == 2 &&
                                 Commercial_B2bLC.Active == true &&
                                 Common_Supplier.Active == true &&
                                 //Mkt_Buyer.Active == true &&
                                 //Commercial_MasterLC.Active == true// &&
                                 Commercial_UD.Active == true
                                 select new
                                 {
                                     B2BLCID = Commercial_B2bLC.ID,
                                     B2BLCName = Commercial_B2bLC.Name,
                                     B2BAmount = Commercial_B2bLC.Amount.ToString(),
                                    // MLCName = Commercial_MasterLC.Name,
                                     Supplier = Common_Supplier.Name,
                                     ItemName = Commercial_BTBItem.Name,
                                     //Buyer = Mkt_Buyer.Name,
                                     UDName= Commercial_UD.UdNo,
                                     Status= Commercial_B2bLC.Status,
                                     UDID = Commercial_UD.ID,
                                     CommonUnit = Common_Unit.Name,
                                     OrderQty = Commercial_B2bLC.RequiredQuantity ?? 0,
                                     ImportItem = (from im in db.Commercial_Import
                                                  where(im.Commercial_B2bLCFK == Commercial_B2bLC.ID && im.Active == true)
                                                  join unit in db.Common_Unit on im.Common_UnitFK equals unit.ID
                                                  join shipmentunit in db.Common_Unit on im.Common_UnitFKForShipment equals shipmentunit.ID
                                                  select new { im, unit,shipmentunit }).ToList()

                                 }).ToList();

                    if (vData.Any())
                    {
                        foreach (var lc in vData)
                        {
                            ++serial;
                            ReportDocType lcitem = new ReportDocType();
                            lcitem.Body1 = lc.B2BLCName;
                            lcitem.Body2 = lc.B2BAmount;
                            //lcitem.Body3 = lc.MLCName;
                            lcitem.Body4 = lc.Supplier;
                            lcitem.Body5 = lc.ItemName;
                            lcitem.int1 = lc.UDID;
                            lcitem.Body7 = lc.OrderQty +" "+ lc.CommonUnit;
                            lcitem.Body8 = serial.ToString();
                            lcitem.Body9 = lc.B2BLCID.ToString();
                            lcitem.Body10 = lc.UDName;                         
                            lcitem.Body11 = lc.Status;                         
                            lcitem.ReportDocTypeList = new List<ReportDocType>();
                            if (lc.ImportItem.Any())
                            {
                                decimal totalorder = 0;
                                foreach (var v in lc.ImportItem)
                                {
                                    totalorder += v.im.Quantity;

                                    ReportDocType item = new ReportDocType();
                                    item.SubBody1 = v.im.Quantity+"-"+v.shipmentunit.Name;
                                    item.SubBody2 = (lc.OrderQty - totalorder)+"-"+ v.shipmentunit.Name;
                                    item.SubBody3 = v.im.UnitQty + v.unit.Name;
                                    item.SubBody4 = v.im.ETDDate.ToShortDateString();
                                    item.SubBody5 = v.im.ETADate.ToShortDateString();
                                    item.SubBody6 = v.im.DocDate == null ? "" : v.im.DocDate.Value.ToShortDateString();
                                    item.SubBody7 = v.im.Ship;
                                    item.SubBody8 = v.im.Remarks;
                                    item.SubBody9 = v.im.LastEdited.Value.ToShortDateString();
                                    lcitem.ReportDocTypeList.Add(item);
                                }
                            }
                            report.Add(lcitem);
                        }
                    }
                    #endregion
                }
                else if (model.FilterId==2)
                {
                    #region MasterL/C Date
                    var vData = (from Commercial_B2bLC in db.Commercial_B2bLC
                                 join Common_Supplier in db.Common_Supplier on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                                 join Commercial_BTBItem in db.Commercial_BTBItem on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BTBItem.ID
                                 join Commercial_UDSlave in db.Commercial_UDSlave on Commercial_B2bLC.Commercial_UDFK equals Commercial_UDSlave.Commercial_UDFK
                                 join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_UDSlave.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                                 join Commercial_UD in db.Commercial_UD on Commercial_UDSlave.Commercial_UDFK equals Commercial_UD.ID
                                 join Mkt_Buyer in db.Mkt_Buyer on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                                 where
                                 Commercial_B2bLC.Commercial_LCOreginFK == 2 &&
                                 Commercial_B2bLC.Active == true &&
                                 Common_Supplier.Active == true &&
                                 Mkt_Buyer.Active == true &&
                                 Commercial_MasterLC.Active == true &&
                                 Commercial_UD.Active == true &&
                                 Commercial_MasterLC.Date >= model.FromDate && Commercial_MasterLC.Date <= model.ToDate
                                 select new
                                 {
                                     B2BLCID = Commercial_B2bLC.ID,
                                     B2BLCName = Commercial_B2bLC.Name,
                                     B2BAmount = Commercial_B2bLC.Amount.ToString(),
                                     MLCName = Commercial_MasterLC.Name,
                                     Supplier = Common_Supplier.Name,
                                     ItemName = Commercial_BTBItem.Name,
                                     Buyer = Mkt_Buyer.Name,
                                     UDName = Commercial_UD.UdNo,
                                     OrderQty = Commercial_B2bLC.RequiredQuantity == null ? 0 : Commercial_B2bLC.RequiredQuantity,
                                     ImportItem = (from im in db.Commercial_Import
                                                   where (im.Commercial_B2bLCFK == Commercial_B2bLC.ID && im.Active == true)
                                                   join unit in db.Common_Unit on im.Common_UnitFK equals unit.ID
                                                   select new { im, unit }).ToList()

                                 }).ToList();

                    if (vData.Any())
                    {
                        foreach (var lc in vData)
                        {
                            ++serial;
                            ReportDocType lcitem = new ReportDocType();
                            lcitem.Body1 = lc.B2BLCName;
                            lcitem.Body2 = lc.B2BAmount;
                            lcitem.Body3 = lc.MLCName;
                            lcitem.Body4 = lc.Supplier;
                            lcitem.Body5 = lc.ItemName;
                            lcitem.Body6 = lc.Buyer;
                            lcitem.Body7 = lc.OrderQty.ToString();
                            lcitem.Body8 = serial.ToString();
                            lcitem.Body9 = lc.B2BLCID.ToString();
                            lcitem.Body10 = lc.UDName;
                            lcitem.ReportDocTypeList = new List<ReportDocType>();
                            if (lc.ImportItem.Any())
                            {
                                decimal totalorder = 0;
                                foreach (var v in lc.ImportItem)
                                {
                                    totalorder += v.im.Quantity;

                                    ReportDocType item = new ReportDocType();
                                    item.SubBody1 = v.im.Quantity.ToString();
                                    item.SubBody2 = (lc.OrderQty - totalorder).ToString();
                                    item.SubBody3 = v.im.UnitQty + v.unit.Name;
                                    item.SubBody4 = v.im.ETDDate.ToShortDateString();
                                    item.SubBody5 = v.im.ETADate.ToShortDateString();
                                    item.SubBody6 = v.im.DocDate == null ? "" : v.im.DocDate.Value.ToShortDateString();
                                    item.SubBody7 = v.im.Ship;
                                    item.SubBody8 = v.im.Remarks;
                                    item.SubBody9 = v.im.LastEdited.Value.ToShortDateString();
                                    lcitem.ReportDocTypeList.Add(item);
                                }
                            }
                            report.Add(lcitem);
                        }
                    }
                    #endregion
                }
                else if (model.FilterId == 3)
                {
                    #region ETD
                    var vData = (from Commercial_B2bLC in db.Commercial_B2bLC
                                 join Common_Supplier in db.Common_Supplier on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                                 join Commercial_BTBItem in db.Commercial_BTBItem on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BTBItem.ID
                                 join Commercial_UDSlave in db.Commercial_UDSlave on Commercial_B2bLC.Commercial_UDFK equals Commercial_UDSlave.Commercial_UDFK
                                 join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_UDSlave.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                                 join Commercial_UD in db.Commercial_UD on Commercial_UDSlave.Commercial_UDFK equals Commercial_UD.ID
                                 join Mkt_Buyer in db.Mkt_Buyer on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                                 where
                                 Commercial_B2bLC.Commercial_LCOreginFK == 2 &&
                                 Commercial_B2bLC.Active == true &&
                                 Common_Supplier.Active == true &&
                                 Mkt_Buyer.Active == true &&
                                 Commercial_MasterLC.Active == true &&
                                 Commercial_UD.Active==true
                                 select new
                                 {
                                     B2BLCID = Commercial_B2bLC.ID,
                                     B2BLCName = Commercial_B2bLC.Name,
                                     B2BAmount = Commercial_B2bLC.Amount.ToString(),
                                     MLCName = Commercial_MasterLC.Name,
                                     Supplier = Common_Supplier.Name,
                                     ItemName = Commercial_BTBItem.Name,
                                     Buyer = Mkt_Buyer.Name,
                                     UDName=Commercial_UD.UdNo,

                                     OrderQty = Commercial_B2bLC.RequiredQuantity == null ? 0 : Commercial_B2bLC.RequiredQuantity,
                                     ImportItem = (from im in db.Commercial_Import
                                                   where (im.Commercial_B2bLCFK == Commercial_B2bLC.ID && im.Active == true && im.ETDDate >= model.FromDate && im.ETDDate <= model.ToDate)
                                                   join unit in db.Common_Unit on im.Common_UnitFK equals unit.ID
                                                   select new { im, unit }).ToList()
                                                   
                                 }).ToList();

                    if (vData.Any())
                    {
                        foreach (var lc in vData)
                        {
                            if (lc.ImportItem.Any())
                            {
                                ++serial;
                                ReportDocType lcitem = new ReportDocType();
                                lcitem.Body1 = lc.B2BLCName;
                                lcitem.Body2 = lc.B2BAmount;
                                lcitem.Body3 = lc.MLCName;
                                lcitem.Body4 = lc.Supplier;
                                lcitem.Body5 = lc.ItemName;
                                lcitem.Body6 = lc.Buyer;
                                lcitem.Body7 = lc.OrderQty.ToString();
                                lcitem.Body8 = serial.ToString();
                                lcitem.Body9 = lc.B2BLCID.ToString();
                                lcitem.Body10 = lc.UDName;
                                lcitem.ReportDocTypeList = new List<ReportDocType>();
                                if (lc.ImportItem.Any())
                                {
                                    decimal totalorder = 0;
                                    foreach (var v in lc.ImportItem)
                                    {
                                        totalorder += v.im.Quantity;

                                        ReportDocType item = new ReportDocType();
                                        item.SubBody1 = v.im.Quantity.ToString();
                                        item.SubBody2 = (lc.OrderQty - totalorder).ToString();
                                        item.SubBody3 = v.im.UnitQty + v.unit.Name;
                                        item.SubBody4 = v.im.ETDDate.ToShortDateString();
                                        item.SubBody5 = v.im.ETADate.ToShortDateString();
                                        item.SubBody6 = v.im.DocDate == null ? "" : v.im.DocDate.Value.ToShortDateString();
                                        item.SubBody7 = v.im.Ship;
                                        item.SubBody8 = v.im.Remarks;
                                        item.SubBody9 = v.im.LastEdited.Value.ToShortDateString();
                                        lcitem.ReportDocTypeList.Add(item);
                                    }
                                }
                                report.Add(lcitem);
                            }
                            
                        }
                    }
                    #endregion
                }
                else if (model.FilterId == 4)
                {
                    #region ETA
                    var vData = (from Commercial_B2bLC in db.Commercial_B2bLC
                                 join Common_Supplier in db.Common_Supplier on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                                 join Commercial_BTBItem in db.Commercial_BTBItem on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BTBItem.ID
                                 join Commercial_UDSlave in db.Commercial_UDSlave on Commercial_B2bLC.Commercial_UDFK equals Commercial_UDSlave.Commercial_UDFK
                                 join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_UDSlave.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                                 join Commercial_UD in db.Commercial_UD on Commercial_UDSlave.Commercial_UDFK equals Commercial_UD.ID
                                 join Mkt_Buyer in db.Mkt_Buyer on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                                 where
                                 Commercial_B2bLC.Commercial_LCOreginFK == 2 &&
                                 Commercial_B2bLC.Active == true &&
                                 Common_Supplier.Active == true &&
                                 Mkt_Buyer.Active == true &&
                                 Commercial_MasterLC.Active == true &&
                                 Commercial_UD.Active == true
                                 select new
                                 {
                                     B2BLCID = Commercial_B2bLC.ID,
                                     B2BLCName = Commercial_B2bLC.Name,
                                     B2BAmount = Commercial_B2bLC.Amount.ToString(),
                                     MLCName = Commercial_MasterLC.Name,
                                     Supplier = Common_Supplier.Name,
                                     ItemName = Commercial_BTBItem.Name,
                                     Buyer = Mkt_Buyer.Name,
                                     UDName = Commercial_UD.UdNo,
                                     OrderQty = Commercial_B2bLC.RequiredQuantity==null?0: Commercial_B2bLC.RequiredQuantity,
                                     ImportItem = (from im in db.Commercial_Import
                                                   where (im.Commercial_B2bLCFK == Commercial_B2bLC.ID && im.Active == true && im.ETADate >= model.FromDate && im.ETADate <= model.ToDate)
                                                   join unit in db.Common_Unit on im.Common_UnitFK equals unit.ID
                                                   select new { im, unit }).ToList()

                                 }).ToList();

                    if (vData.Any())
                    {
                        foreach (var lc in vData)
                        {
                            if (lc.ImportItem.Any())
                            {
                                ++serial;
                                ReportDocType lcitem = new ReportDocType();
                                lcitem.Body1 = lc.B2BLCName;
                                lcitem.Body2 = lc.B2BAmount;
                                lcitem.Body3 = lc.MLCName;
                                lcitem.Body4 = lc.Supplier;
                                lcitem.Body5 = lc.ItemName;
                                lcitem.Body6 = lc.Buyer;
                                lcitem.Body7 = lc.OrderQty.ToString();
                                lcitem.Body8 = serial.ToString();
                                lcitem.Body9 = lc.B2BLCID.ToString();
                                lcitem.Body10 = lc.UDName;
                                lcitem.ReportDocTypeList = new List<ReportDocType>();
                                if (lc.ImportItem.Any())
                                {
                                    decimal totalorder = 0;
                                    foreach (var v in lc.ImportItem)
                                    {
                                        totalorder += v.im.Quantity;

                                        ReportDocType item = new ReportDocType();
                                        item.SubBody1 = v.im.Quantity.ToString();
                                        item.SubBody2 = (lc.OrderQty - totalorder).ToString();
                                        item.SubBody3 = v.im.UnitQty + v.unit.Name;
                                        item.SubBody4 = v.im.ETDDate.ToShortDateString();
                                        item.SubBody5 = v.im.ETADate.ToShortDateString();
                                        item.SubBody6 = v.im.DocDate == null ? "": v.im.DocDate.Value.ToShortDateString();
                                        item.SubBody7 = v.im.Ship;
                                        item.SubBody8 = v.im.Remarks;
                                        item.SubBody9 = v.im.LastEdited.Value.ToString();
                                        lcitem.ReportDocTypeList.Add(item);
                                    }
                                }
                                report.Add(lcitem);
                            }
                                
                        }
                    }
                    #endregion
                }
                
                this.ImportReport = report;
            }
        }



        public void ForeignBtbImport(int btbId)
        {
            db = new xOssContext();


            var v = (from t1 in db.Commercial_Import
                     join t2 in db.Commercial_B2bLC on t1.Commercial_B2bLCFK equals t2.ID
                     join t3 in db.Common_Unit on t1.Common_UnitFK equals t3.ID
                     join t4 in db.Common_Unit on t1.Common_UnitFKForShipment equals t4.ID
                     join t5 in db.Common_Supplier on t2.Common_SupplierFK equals t5.ID
                     join t6 in db.Commercial_BTBItem on t2.Commercial_BTBItemFK equals t6.ID
                     join t7 in db.Commercial_BtBLCType on t2.Commercial_BtBLCTypeFK equals t7.ID
                     join t8 in db.Commercial_LCOregin on t2.Commercial_LCOreginFK equals t8.ID
                     join t9 in db.Commercial_UD on t2.Commercial_UDFK equals t9.ID
                     join t10 in db.Common_Unit on t2.Common_UnitFK equals t10.ID
                     where t1.Commercial_B2bLCFK == btbId && t1.Active == true && t2.Active == true

                     select new ReportDocType
                     {
                         HeaderLeft1 = t2.Name,
                         HeaderLeft2 = t5.Name  + "\nEmail: " + t5.Email + "\nAddress: " + t5.Address + "\nMobile: " + t5.Mobile,
                         HeaderLeft3 = t2.Description ,
                         HeaderLeft4 = t2.Shipment,
                         HeaderLeft5 = t2.Status,
                         HeaderLeft6 = t2.AmendmentDecrease.ToString(),
                         HeaderLeft7 = t2.AmendmentIncrease.ToString(),
                         HeaderLeft8 = t2.Amount.ToString(),
                        
                         HeaderRight1 = t2.Date.ToString(),
                         HeaderRight2 = (t2.RequiredQuantity ?? 0).ToString(),
                         HeaderRight3 = t6.Name,
                         HeaderRight4 = t7.Name,
                         HeaderRight5 = t8.Name,
                         HeaderRight6 = t9.UdNo,
                         HeaderRight7 = t9.BTBOpening.ToString(),
                         HeaderRight8 = t9.Date.ToString(),
                         
                         Body1 = t1.Quantity.ToString(),// + " " + t3.Name,
                         Body2 = t1.UnitQty.ToString(), //+ " " + t4.Name,
                         Body3 = t1.ETDDate.ToString(),
                         Body4 = t1.ETADate.ToString(),
                         Body5 = t1.DocDate.ToString(),
                         Body6 = t1.Ship,
                         Body7 = t1.Remarks,
                         Body9 = t3.Name,
                         Body10 = t4.Name,
                         Body11 = t10.Name
                     }).ToList();
            decimal totalorder = 0;
            foreach (ReportDocType r in v)
            {
                decimal orderQuantity = Convert.ToDecimal(r.Body1);
                decimal totalRequired = Convert.ToDecimal(r.HeaderRight2);
                totalorder += orderQuantity;
                Common.VmForDropDown ntw = new Common.VmForDropDown();

               // r.SubBody2 = ntw.NumberToWords(total, Common.CurrencyType.USD) + ".";


                if (r.Body5 != "")
                {
                    r.Body5 = r.Body5.Substring(0, r.Body5.Length - 8);
                }
                r.HeaderRight1 = r.HeaderRight1.Substring(0, r.HeaderRight1.Length - 8);
                r.HeaderRight8 = r.HeaderRight8.Substring(0, r.HeaderRight8.Length - 8);
                r.Body3 = r.Body3.Substring(0, r.Body3.Length - 8);
                r.Body4 = r.Body4.Substring(0, r.Body4.Length - 8);
                r.Body8 = (totalRequired - totalorder).ToString() +" " + r.Body10;
                r.Body1 = r.Body1 + " " + r.Body10;
                r.Body2 = r.Body2 + " "  +r.Body9;
                r.HeaderRight2 = r.HeaderRight2 + " " + r.Body11;
            }
            this.ReportDoc = v.ToList();
        }
    }
}