﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_BTBItem
    {
        private xOssContext db;
        public Commercial_BTBItem Commercial_BTBItem { get; set; }
        public IEnumerable<VmCommercial_BTBItem> DataList { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_BTBItem
                     select new VmCommercial_BTBItem
                     {
                         Commercial_BTBItem = t1

                     }).Where(x => x.Commercial_BTBItem.Active == true).OrderByDescending(x => x.Commercial_BTBItem.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_BTBItem

                     select new VmCommercial_BTBItem
                     {
                         Commercial_BTBItem = t1

                     }).Where(c => c.Commercial_BTBItem.ID == id).SingleOrDefault();
            this.Commercial_BTBItem = v.Commercial_BTBItem;

        }





        public int Add(int userID)
        {
            Commercial_BTBItem.AddReady(userID);
            return Commercial_BTBItem.Add();

        }

        public bool Edit(int userID)
        {
            return Commercial_BTBItem.Edit(userID);
        }

    }
}