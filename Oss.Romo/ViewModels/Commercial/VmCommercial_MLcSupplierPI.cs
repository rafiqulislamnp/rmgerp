﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_MLcSupplierPI
    {
        private xOssContext db;
        
        public Commercial_MLcSupplierPI Commercial_MLcSupplierPI { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Commercial_PI Commercial_PI { get; set; }
        public IEnumerable<VmCommercial_MLcSupplierPI> DataList { get; set; }
        public List<object> DropDownList { get; set; }


        public int Add(int userID)
        {
            Commercial_MLcSupplierPI.AddReady(userID);
            return Commercial_MLcSupplierPI.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_MLcSupplierPI.Edit(userID);
        }

        public VmCommercial_MLcSupplierPI SelectSingleJoined(int iD)
        {
            db = new xOssContext();
            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID

                     select new VmCommercial_MLcSupplierPI
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer
                     }).Where(x => x.Commercial_MasterLC.ID == iD).FirstOrDefault();
            return v;
        }
        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_MLcSupplierPI in db.Commercial_MLcSupplierPI
                     join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_MLcSupplierPI.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Commercial_PI in db.Commercial_PI on Commercial_MLcSupplierPI.Commercial_PIFK equals Commercial_PI.ID
                     select new VmCommercial_MLcSupplierPI
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Commercial_PI = Commercial_PI,
                         Commercial_MLcSupplierPI = Commercial_MLcSupplierPI

                     }).Where(x => x.Commercial_MLcSupplierPI.Active == true && x.Commercial_MasterLC.ID == id)
                       .OrderByDescending(x => x.Commercial_MLcSupplierPI.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (
                     from Commercial_MLcSupplierPI in db.Commercial_MLcSupplierPI
                     join Commercial_MasterLC in db.Commercial_MasterLC on Commercial_MLcSupplierPI.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Commercial_PI in db.Commercial_PI on Commercial_MLcSupplierPI.Commercial_PIFK equals Commercial_PI.ID
                     select new VmCommercial_MLcSupplierPI
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Commercial_PI = Commercial_PI,
                         Commercial_MLcSupplierPI = Commercial_MLcSupplierPI
                     }).Where(c => c.Commercial_MLcSupplierPI.ID == iD).SingleOrDefault();
            this.Commercial_MLcSupplierPI = v.Commercial_MLcSupplierPI;
            this.Commercial_MasterLC = v.Commercial_MasterLC;
            this.Commercial_PI = v.Commercial_PI;
        }

    }
}
