﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_MLCRealization
    {
        private xOssContext db;
        public Commercial_MLCRealization Commercial_MLCRealization { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }

        public IEnumerable<VmCommercial_MLCRealization> DataList { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }



        public VmCommercial_MLCRealization SelectSingleJoined(int id)
        {
            db = new xOssContext();

            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID

                     select new VmCommercial_MLCRealization
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer
                     }).Where(x => x.Commercial_MasterLC.ID == id).FirstOrDefault();
            return v;
         }
        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_MLCRealization in db.Commercial_MLCRealization
                     join Commercial_MasterLC in db.Commercial_MasterLC
                     on Commercial_MLCRealization.Commercial_MasterLCFK equals Commercial_MasterLC.ID

                     select new VmCommercial_MLCRealization
                     {
                         Commercial_MLCRealization= Commercial_MLCRealization,

                         Commercial_MasterLC = Commercial_MasterLC

                     }).Where(x => x.Commercial_MLCRealization.Active == true  && x.Commercial_MasterLC.ID == id)
                       .OrderByDescending(x => x.Commercial_MLCRealization.ID).AsEnumerable().ToList();
              this.DataList = a;


        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v =( from Commercial_MLCRealization in db.Commercial_MLCRealization
                     join Commercial_MasterLC in db.Commercial_MasterLC
                     on Commercial_MLCRealization.Commercial_MasterLCFK equals Commercial_MasterLC.ID
          
                    select new VmCommercial_MLCRealization
                    {
                        Commercial_MLCRealization = Commercial_MLCRealization,
                      
                        Commercial_MasterLC = Commercial_MasterLC
                
                    }).Where(c => c.Commercial_MLCRealization.ID == id).SingleOrDefault();
            this.Commercial_MLCRealization = v.Commercial_MLCRealization;
            this.Commercial_MasterLC = v.Commercial_MasterLC;


        }
      public int Add(int userID)
        {
            Commercial_MLCRealization.AddReady(userID);
            return Commercial_MLCRealization.Add();
        }
        public bool Edit(int userID)
        {
            return Commercial_MLCRealization.Edit(userID);
        }

    }
}