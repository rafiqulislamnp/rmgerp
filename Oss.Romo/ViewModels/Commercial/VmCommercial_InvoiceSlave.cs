﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Shipment;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmInvoiceInfo
    {
        public Shipment_OrderDeliverdSchedule Shipment_OrderDeliverdSchedule { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Commercial_MasterLCBuyerPO Commercial_MasterLCBuyerPO { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Mkt_Item Mkt_Item { get; set; }


    }

    public class VmCommercial_InvoiceSlave
    {
        private xOssContext db;
        public IEnumerable<VmCommercial_InvoiceSlave> DataList { get; set; }
        public IEnumerable<VmInvoiceInfo> VmInvoiceInfo { get; set; }
        public Commercial_Invoice Commercial_Invoice { get; set; }
        public Commercial_InvoiceSlave Commercial_InvoiceSlave { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Shipment_CNFDelivaryChallan Shipment_CNFDelivaryChallan { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public decimal TotalOrderQty { get; set; }
        public decimal RatePack { get; set; }
        public decimal MyProperty { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal SumRatePack { get; set; }
        public decimal SumTotalPrice { get; set; }
        public int SumTotalOrderQty { get; set; }
        public int SumTotalPackQty { get; set; }
        public int SumInvoiceQPackQty { get; set; }
        public string SumTotalPriceStr { get; set; }
        public Shipment_PortOfLoading Shipment_PortOfLoading { get; set; }
        public Shipment_PortOfDischarge Shipment_PortOfDischarge { get; set; }
        public Common_Country Common_Country { get; set; }
        public Shipment_OrderDeliverdSchedule Shipment_OrderDeliverdSchedule { get; set; }
        public Commercial_MasterLCBuyerPO Commercial_MasterLCBuyerPO { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public Common_Currency Common_Currency { get; set; }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var invoiceSlave = (from t1 in db.Commercial_InvoiceSlave
                                join t2 in db.Commercial_Invoice on t1.Commercial_InvoiceFk equals t2.ID
                                join t3 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t3.ID
                                join t0 in db.Shipment_OrderDeliverdSchedule on t1.Shipment_OrderDeliverdScheduleFK equals t0.ID
                                join t4 in db.Mkt_BOM on t0.Mkt_BOMFK equals t4.ID
                                join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                                join t6 in db.Mkt_Item on t4.Mkt_ItemFK equals t6.ID 
                                join t7 in db.Common_Currency on t5.Common_CurrencyFK equals t7.ID
                                where t1.Commercial_InvoiceFk == id && t1.Active == true
                                select new VmCommercial_InvoiceSlave
                                {
                                    Commercial_InvoiceSlave = t1,
                                    Commercial_Invoice = t2,
                                    Shipment_CNFDelivaryChallan = t3,
                                    Shipment_OrderDeliverdSchedule = t0,
                                    Mkt_BOM = t4,
                                    Common_TheOrder = t5,
                                    Mkt_Item = t6,
                                    Common_Currency = t7
                                }).Where(x => x.Commercial_InvoiceSlave.Active == true).OrderBy(x => x.Commercial_InvoiceSlave.ID).AsEnumerable();
            this.DataList = invoiceSlave;

        }

        private decimal GetDelivaryChallan(int challanId)
        {
            db = new xOssContext();
            var deliverdQty = (from t1 in db.Shipment_OrderDeliverdSchedule
                               where t1.Shipment_CNFDelivaryChallanFk == challanId && t1.Active == true
                               select t1.DeliverdQty).ToList();
            decimal delQty = 0;
            if (deliverdQty != null)
            {
                delQty = deliverdQty.Sum();
            }

            return delQty;

        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Commercial_InvoiceSlave in db.Commercial_InvoiceSlave
                     join Commercial_Invoice in db.Commercial_Invoice
                     on Commercial_InvoiceSlave.Commercial_InvoiceFk equals Commercial_Invoice.ID
                     //join Mkt_BOM in db.Mkt_BOM
                     //on Commercial_InvoiceSlave.Mkt_BOMFK equals Mkt_BOM.ID
                     //join common_TheOrder in db.Common_TheOrder
                     //on Mkt_BOM.Common_TheOrderFk equals common_TheOrder.ID

                     select new VmCommercial_InvoiceSlave
                     {
                         Commercial_InvoiceSlave = Commercial_InvoiceSlave,
                         Commercial_Invoice = Commercial_Invoice
                         //Mkt_BOM = Mkt_BOM,
                         //Common_TheOrder = common_TheOrder
                     }).SingleOrDefault(c => c.Commercial_InvoiceSlave.ID == id);
            this.Commercial_InvoiceSlave = v.Commercial_InvoiceSlave;
            this.Commercial_Invoice = v.Commercial_Invoice;


        }

        public VmCommercial_InvoiceSlave SelectSingleJoined(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_Invoice
                     join t2 in db.Shipment_PortOfLoading on t1.ShipmentPortOfLoadingFk equals t2.ID
                     join t3 in db.Shipment_PortOfDischarge on t1.ShipmentPortOfDischargeFk equals t3.ID
                     join t4 in db.Common_Country on t1.CommonCountryFk equals t4.ID
                     select new VmCommercial_InvoiceSlave
                     {
                         Commercial_Invoice = t1,
                         Shipment_PortOfLoading = t2,
                         Shipment_PortOfDischarge = t3,
                         Common_Country = t4


                     }).FirstOrDefault(x => x.Commercial_Invoice.ID == id);
            return v;
        }

        public int Add(int userID)
        {
            //Commercial_InvoiceSlave.Commercial_InvoiceFk = 
            Commercial_InvoiceSlave.AddReady(userID);
            return Commercial_InvoiceSlave.Add();
        }
        public bool Edit(int userID)
        {
            return Commercial_InvoiceSlave.Edit(userID);
        }
    }

}