﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_LCType
    {
        private xOssContext db;
        public Commercial_LCType Commercial_LCType { get; set; }
        public IEnumerable<VmCommercial_LCType> DataList { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_LCType
                     select new VmCommercial_LCType
                     {
                         Commercial_LCType = t1

                     }).Where(x => x.Commercial_LCType.Active == true).OrderByDescending(x => x.Commercial_LCType.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_LCType

                     select new VmCommercial_LCType
                     {
                         Commercial_LCType = t1

                     }).Where(c => c.Commercial_LCType.ID == id).SingleOrDefault();
            this.Commercial_LCType = v.Commercial_LCType;

        }

        public int Add(int userID)
        {
            Commercial_LCType.AddReady(userID);

            return Commercial_LCType.Add();

        }
        public bool Edit(int userID)
        {
            return Commercial_LCType.Edit(userID);
        }
        
    }
}