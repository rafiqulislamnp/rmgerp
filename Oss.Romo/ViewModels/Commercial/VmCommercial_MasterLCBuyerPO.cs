﻿using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Commercial
{
    public class VmCommercial_MasterLCBuyerPO
    {
        private xOssContext db;
        public Commercial_MasterLCBuyerPO Commercial_MasterLCBuyerPO { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Commercial_Bank Buyer_Bank { get; set; }
        public Commercial_Bank Lien_Bank { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_Item Mkt_Item { get; set; }
        public IEnumerable<VmCommercial_MasterLCBuyerPO> DataList { get; set; }

        public VmCommercial_MasterLCBuyerPO SelectSingleJoined(int id)
        {
            db = new xOssContext();

            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID

                     select new VmCommercial_MasterLCBuyerPO
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer
                     }).Where(x => x.Commercial_MasterLC.ID == id).FirstOrDefault();
            return v;
        }
        [DisplayName("Rate Pack")]
        public decimal RatePack { get; set; }
        [DisplayName("Amount")]
        public decimal TotalPrice { get; set; }
        [DisplayName("Total Order")]
        public int TotalOrderQty { get; set; }

        public decimal SumRatePack { get; set; }
        public decimal SumTotalPrice { get; set; }
        public string SumTotalPriceStr { get; set; }
        public int SumTotalOrderQty { get; set; }
        public int SumTotalPackQty { get; set; }
        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_MasterLCBuyerPO
                     join t2 in db.Commercial_MasterLC
                     on t1.Commercial_MasterLCFK equals t2.ID
                     join t3 in db.Mkt_BOM
                     on t1.MKTBOMFK equals t3.ID
                     join t4 in db.Common_TheOrder
                     on t3.Common_TheOrderFk equals t4.ID
                     join t5 in db.Mkt_Item
                    on t3.Mkt_ItemFK equals t5.ID
                     where t1.Commercial_MasterLCFK == id
                     select new VmCommercial_MasterLCBuyerPO
                     {
                         Commercial_MasterLCBuyerPO = t1,
                         Commercial_MasterLC = t2,
                         Common_TheOrder = t4,
                         Mkt_BOM = t3,
                         Mkt_Item = t5,
                         TotalOrderQty = t3.QPack * t3.Quantity,
                         RatePack = (t3.QPack * t3.Quantity * t3.UnitPrice) / t3.QPack,
                         TotalPrice = t3.QPack * t3.Quantity * t3.UnitPrice
                     }).Where(x => x.Commercial_MasterLCBuyerPO.Active == true)
                       .OrderByDescending(x => x.Commercial_MasterLCBuyerPO.ID).AsEnumerable();
            this.DataList = a;
            SumRatePack = a.Sum(x => x.RatePack);
            SumTotalPrice = a.Sum(x => x.TotalPrice);
            SumTotalOrderQty = a.Sum(x => x.TotalOrderQty);
            SumTotalPackQty = a.Sum(x => x.Mkt_BOM.QPack);
            SumTotalPriceStr = SumTotalPrice.ToString("F");
        }

        public decimal? Commission { get; set; }
        public string TotalBuyerCommission { get; set; }
        public string ActualValueOf75Percent { get; set; }
        public string GarmentsTest { get; set; }
        public string ActualValue { get; set; }
        public string BuyerTolerance { get; set; }


        public void GetBuyerCommission(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_MasterLC
                     join t2 in db.Commercial_MasterLCBuyerPO
                     on t1.ID equals t2.Commercial_MasterLCFK
                     join t3 in db.Mkt_BOM
                     on t2.MKTBOMFK equals t3.ID

                     join t4 in db.Common_TheOrder
                     on t3.Common_TheOrderFk equals t4.ID
                     join t5 in db.Mkt_Buyer
                     on t1.Mkt_BuyerFK equals t5.ID
                     join t6 in db.Commercial_Bank
                     on t1.Commercial_BuyerBankFK equals t6.ID

                     join t7 in db.Commercial_Bank
                     on t1.Commercial_LienBankFK equals t7.ID

                     where t2.Commercial_MasterLCFK == id
                     && t1.Active == true
                     && t2.Active == true
                     && t3.Active == true
                     && t4.Active == true

                     select new VmCommercial_MasterLCBuyerPO
                     {
                         Commercial_MasterLC = t1,
                         Commercial_MasterLCBuyerPO = t2,
                         Mkt_BOM = t3,
                         Common_TheOrder = t4,
                         Mkt_Buyer = t5,
                         Buyer_Bank = t6,
                         Lien_Bank = t7
                     }).Where(x => x.Commercial_MasterLC.ID == id).AsEnumerable();
            decimal? totalBuyerCommission = 0;
            decimal? actualValue = 0;
            decimal? totalActualValue = 0;
            decimal? charge = 0;
            decimal? actual = 0;
            decimal? actualValueOf75Percent = 0;
            decimal germentsTest = 0;
            decimal? bTBOpening = 0;
            decimal? buyerTolerance = 0;
            decimal? bTolerance = 0;
            decimal? TTLbuyerTolerance = 0;
            foreach (var i in a)
            {
                //if (i.Commercial_MasterLC.BTBOpening != null)
                //{
                //    bTBOpening = i.Commercial_MasterLC.BTBOpening;
                //}
                if (i.Commercial_MasterLC.BuyerTolerance != null)
                {
                    buyerTolerance = i.Commercial_MasterLC.BuyerTolerance;
                }
                if (i.Mkt_BOM.ID != null)
                {
                    germentsTest += 12; //GetBOMGarmantsTestValue(i.Mkt_BOM.ID);
                }
                // All charge of L/C such as Test, Freight, Handling charge
                charge = germentsTest + i.Commercial_MasterLC.FreightCharge + i.Commercial_MasterLC.HandlingCharge;
                // Total buyer commission of all order
                totalBuyerCommission += ((i.Mkt_BOM.QPack * i.Mkt_BOM.Quantity * i.Mkt_BOM.UnitPrice) / 100) * i.Common_TheOrder.Commission;
                // Actual Value of a single order.
                if (i.Common_TheOrder.Commission != null)
                {
                    actualValue = ((i.Mkt_BOM.QPack * i.Mkt_BOM.Quantity * i.Mkt_BOM.UnitPrice)
                  - (((i.Mkt_BOM.QPack * i.Mkt_BOM.Quantity * i.Mkt_BOM.UnitPrice) / 100) * i.Common_TheOrder.Commission));
                    // Total value of all order those include in one master L/C.
                    totalActualValue += ((i.Mkt_BOM.QPack * i.Mkt_BOM.Quantity * i.Mkt_BOM.UnitPrice)
                        - (((i.Mkt_BOM.QPack * i.Mkt_BOM.Quantity * i.Mkt_BOM.UnitPrice) / 100) * i.Common_TheOrder.Commission));

                }
                else
                {
                    actualValue = i.Mkt_BOM.QPack * i.Mkt_BOM.Quantity * i.Mkt_BOM.UnitPrice;
                    totalActualValue += i.Mkt_BOM.QPack * i.Mkt_BOM.Quantity * i.Mkt_BOM.UnitPrice;
                }
                if (buyerTolerance != null)
                {
                    // Total value of all order those include in one master L/C.
                    TTLbuyerTolerance += ((i.Mkt_BOM.QPack * i.Mkt_BOM.Quantity * i.Mkt_BOM.UnitPrice)
                        + (((i.Mkt_BOM.QPack * i.Mkt_BOM.Quantity * i.Mkt_BOM.UnitPrice) / 100) * buyerTolerance));
                    
                }

            }
            if (totalBuyerCommission != null)
            {
                string totalBuyerCommissionstr = totalBuyerCommission.Value.ToString("F");
                this.TotalBuyerCommission = totalBuyerCommissionstr;
            }
            if (totalActualValue != null && charge != null)
            {
                actual = totalActualValue - charge;
                string actualValuestr = actual.Value.ToString("F");
                this.ActualValue = actualValuestr;

                actualValueOf75Percent = (actual / 100) * bTBOpening;
                string actualValueOf75Percentstr = actualValueOf75Percent.Value.ToString("F");
                this.ActualValueOf75Percent = actualValueOf75Percentstr;

                string gTestStr = germentsTest.ToString("F");
                this.GarmentsTest = gTestStr;
            }
            else
            {
                actual = totalActualValue;
                string actualValuestr = actual.Value.ToString("F");
                this.ActualValue = actualValuestr;

                actualValueOf75Percent = (actual / 100) * bTBOpening;
                string actualValueOf75Percentstr = actualValueOf75Percent.Value.ToString("F");
                this.ActualValueOf75Percent = actualValueOf75Percentstr;

                string gTestStr = germentsTest.ToString("F");
                this.GarmentsTest = gTestStr;
            }
            if (TTLbuyerTolerance != null)
            {
                string buyerTolerances = TTLbuyerTolerance.Value.ToString("F");
                this.BuyerTolerance = buyerTolerances;
            }
        }
        
        public VmCommercial_MasterLCBuyerPO GetSingleMasterLCByID(int id)
        {
            db = new xOssContext();
            //this.DataList = a;
            var v = (from Commercial_MasterLC in db.Commercial_MasterLC
                     join Mkt_Buyer in db.Mkt_Buyer
                     on Commercial_MasterLC.Mkt_BuyerFK equals Mkt_Buyer.ID
                     join buyerBank in db.Commercial_Bank
                    on Commercial_MasterLC.Commercial_BuyerBankFK equals buyerBank.ID

                     join lienBank in db.Commercial_Bank
                     on Commercial_MasterLC.Commercial_LienBankFK equals lienBank.ID
                     select new VmCommercial_MasterLCBuyerPO
                     {
                         Commercial_MasterLC = Commercial_MasterLC,
                         Mkt_Buyer = Mkt_Buyer,
                         Buyer_Bank = buyerBank,
                         Lien_Bank = lienBank
                     }).Where(x => x.Commercial_MasterLC.ID == id).FirstOrDefault();

            return v;
        }

        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Commercial_MasterLCBuyerPO in db.Commercial_MasterLCBuyerPO
                     join Mkt_BOM in db.Mkt_BOM
                     on Commercial_MasterLCBuyerPO.MKTBOMFK equals Mkt_BOM.ID

                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID


                     select new VmCommercial_MasterLCBuyerPO
                     {
                         Commercial_MasterLCBuyerPO = Commercial_MasterLCBuyerPO,
                         Mkt_BOM = Mkt_BOM,
                         Common_TheOrder = Common_TheOrder


                     }).Where(c => c.Commercial_MasterLCBuyerPO.ID == id).SingleOrDefault();
            this.Commercial_MasterLCBuyerPO = v.Commercial_MasterLCBuyerPO;

        }

        public int Add(int userID)
        {
            Commercial_MasterLCBuyerPO.AddReady(userID);
            return Commercial_MasterLCBuyerPO.Add();
        }
        public bool Edit(int userID)
        {
            return Commercial_MasterLCBuyerPO.Edit(userID);
        }

    }
}