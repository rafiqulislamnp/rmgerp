﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_LCAlocation
    {
        private xOssContext db;
        public Acc_LCAlocation Acc_LCAlocation { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
      



        public IEnumerable<VmAcc_LCAlocation> DataList { get; set; }

        public VmAcc_LCAlocation SelectSingleSupplier(int id)
        {
            db = new xOssContext();
            var v = (from Common_Supplier in db.Common_Supplier
                     select new VmAcc_LCAlocation
                     {
                         Common_Supplier = Common_Supplier
                     }).Where(c => c.Common_Supplier.ID == id).SingleOrDefault();
            this.Common_Supplier = v.Common_Supplier;
            return v;
        }


        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var v = (from Acc_LCAlocation in db.Acc_LCAlocation
                     join Mkt_PO in db.Mkt_PO
                     on Acc_LCAlocation.Mkt_POFK equals Mkt_PO.ID
                     join Commercial_MasterLC in db.Commercial_MasterLC
                     on Acc_LCAlocation.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID

                     select new VmAcc_LCAlocation
                     {
                         Acc_LCAlocation = Acc_LCAlocation,
                         Mkt_PO = Mkt_PO,
                         Commercial_MasterLC = Commercial_MasterLC,
                         Common_Supplier = Common_Supplier
                     }).Where(x => x.Acc_LCAlocation.Active == true && x.Common_Supplier.ID == id)
                       .OrderByDescending(x => x.Acc_LCAlocation.ID).AsEnumerable()/*.ToList()*/;
         
          this.DataList = v;
        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Acc_LCAlocation in db.Acc_LCAlocation
                     join Mkt_PO in db.Mkt_PO
                     on Acc_LCAlocation.Mkt_POFK equals Mkt_PO.ID
                     join Commercial_MasterLC in db.Commercial_MasterLC
                     on Acc_LCAlocation.Commercial_MasterLCFK equals Commercial_MasterLC.ID
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     select new VmAcc_LCAlocation
                     {
                         Acc_LCAlocation = Acc_LCAlocation,
                         Mkt_PO = Mkt_PO,
                         Commercial_MasterLC = Commercial_MasterLC,
                         Common_Supplier = Common_Supplier


                     }).Where(c => c.Acc_LCAlocation.ID == id).SingleOrDefault();
            this.Acc_LCAlocation = v.Acc_LCAlocation;
            this.Mkt_PO = v.Mkt_PO;
            this.Commercial_MasterLC = v.Commercial_MasterLC;
            this.Common_Supplier = v.Common_Supplier;

        }

      public int Add(int userID)
        {
            Acc_LCAlocation.AddReady(userID);
            return Acc_LCAlocation.Add();
        }

        public bool Edit(int userID)
        {
            return Acc_LCAlocation.Edit(userID);
        }




    }
}