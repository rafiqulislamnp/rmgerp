﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmSupplierTransaction
    {
        public DateTime Date { get; set; }
        public int POID { get; set; }
        public string Challan { get; set; }
        public string B2BLC { get; set; }
        public decimal GoodReceivedValue  { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal Due { get; set; }

    }
    public class VmAcc_POPH
    {

        private xOssContext db;
        public Acc_POPH Acc_POPH { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_Buyer Mkt_Buyer { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public IEnumerable<VmAcc_POPH> DataList { get; set; }
        public IEnumerable<VmMkt_PO> DataList1 { get; set; }
        public String Status { get; set; }
        public VmMkt_PO VmMkt_PO { get; set; }
        public List<ReportDocType> SupplierLedger { get; set; }

        [DisplayName("Final Payment?")]
        public bool FinalPayment { get; set; }


        [DisplayName("Cheque No")]
        [Required(ErrorMessage = "Cheque No is Required")]
        public string ChequeNo { get; set; }

        [DisplayName("Cheque Value")]
        [Required(ErrorMessage = "Cheque Value is Required")]
        public decimal? ChequeValue { get; set; }

        public void InitialDataloadForAccPOPh()
        {
            db = new xOssContext();
            var a = (from Acc_POPH in db.Acc_POPH
                     join Mkt_PO in db.Mkt_PO
                     on Acc_POPH.Mkt_POFK equals Mkt_PO.ID
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID

                     select new VmAcc_POPH
                     {
                         Acc_POPH = Acc_POPH,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier

                     }).Where(x => x.Acc_POPH.Active == true).OrderByDescending(x => x.Acc_POPH.ID).AsEnumerable();
            this.DataList = a;
        }

        public void InitialDataloadForAccPoPh(int id)
        {
            db = new xOssContext();
            var a = (from Acc_POPH in db.Acc_POPH
                     join Mkt_PO in db.Mkt_PO
                     on Acc_POPH.Mkt_POFK equals Mkt_PO.ID
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
                     where Acc_POPH.Mkt_POFK == id
                     select new VmAcc_POPH
                     {
                         Acc_POPH = Acc_POPH,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier
                     }).AsEnumerable();
            this.DataList = a;


        }
        public void SelectSingleForAccPoPH(int id)
        {
            db = new xOssContext();

            var a = (from Mkt_PO in db.Mkt_PO
                     join Common_Supplier in db.Common_Supplier
                     on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID

                     select new VmAcc_POPH
                     {
                         //Acc_POPH = acc_POPH
                         //,
                         Mkt_PO = Mkt_PO,
                         Common_Supplier = Common_Supplier
                     }).Where(x => x.Mkt_PO.ID == id).FirstOrDefault();

            this.Acc_POPH = new Acc_POPH();
            this.Acc_POPH.Mkt_POFK = id;
            this.Mkt_PO = a.Mkt_PO;

            this.Common_Supplier = a.Common_Supplier;

            this.Acc_POPH.Date = DateTime.Now;
            InitialDataloadForAccPoPh(id);
        }
        public void SelectSingleSupplier(int id)
        {
            db = new xOssContext();
            var v = (from Common_Supplier in db.Common_Supplier
                     select new VmAcc_POPH
                     {
                         Common_Supplier = Common_Supplier
                     }).Where(c => c.Common_Supplier.ID == id).SingleOrDefault();
            this.Common_Supplier = v.Common_Supplier;

        }
        internal void SupplierOrderView(int id)
        {
            //db = new xOssContext();
            //var c = (from Mkt_PO in db.Mkt_PO
            //         join Common_Supplier in db.Common_Supplier
            //         on Mkt_PO.Common_SupplierFK equals Common_Supplier.ID
            //         where Mkt_PO.Active == true
            //         && Common_Supplier.ID == id
            //         select new VmAcc_POPH
            //         {

            //             Mkt_PO = Mkt_PO,
            //             Common_Supplier = Common_Supplier,
            //         }).Where(x => x.Mkt_PO.Active == true).OrderByDescending(x => x.Mkt_PO.ID).ToList();
            //foreach (VmAcc_POPH x in c)
            //{
            //    if (x.Mkt_PO.Status == 0)
            //    {
            //        x.Status = "In Progress";
            //    }
            //    else if (x.Mkt_PO.Status == 1)
            //    {
            //        x.Status = "Store Settled";
            //    }
            //    else
            //    {
            //        x.Status = "Payment Settled";
            //    }

            //}
            VmMkt_PO vmMkt_PO = new VmMkt_PO();
            vmMkt_PO.PurchaseOrderViewBySuplier(id);
            this.DataList1 = vmMkt_PO.DataList;
        }


        //...ForDelegateMethod...//

        public int Add(int userID, bool FinalPayment)
        {
            if (FinalPayment)
            {
                this.Mkt_PO.Status = 2;
                this.Mkt_PO.PaymentCleared = FinalPayment;
                this.Mkt_PO.Edit(0);
            }
            if (Acc_POPH.Ischeque == true)
            {
                
                Acc_POPH.LC_D = "CHQ: " + this.ChequeNo;
                Acc_POPH.Amount = this.ChequeValue;
            }
            else
            {
                Acc_POPH.LC_D = "L/C: "+ Acc_POPH.LC_D;
            }
            Acc_POPH.AddReady(userID);
            return Acc_POPH.Add();

        }
        public bool Edit(int userID)
        {
            return Acc_POPH.Edit(userID);
        }
        public bool Delete(int userID)
        {
            return Acc_POPH.Delete(userID);
        }

        internal void GetSupplierLedger1LCIssuedInfo(int Id)
        {
            db = new xOssContext();
            var v1 = (from t2 in db.Mkt_PO                                         
                     join t4 in db.Mkt_BOM
                     on t2.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder
                     on t4.Common_TheOrderFk equals t5.ID
                     join t6 in db.Mkt_Buyer
                     on t5.Mkt_BuyerFK equals t6.ID
                     join t7 in db.Common_Supplier
                     on t2.Common_SupplierFK equals t7.ID
                     where t2.Common_SupplierFK==Id
                     select new ReportDocType
                     {
                         HeaderLeft1 = t7.Name,         // 
                         HeaderLeft2 = t7.Address,      // Supplier Info
                         HeaderLeft3 = t7.Mobile,       //
                         HeaderLeft4 = t7.Email,

                         Body1 = t2.Date.ToString(),      // L/C Date
                         Body2 = t2.CID,                  // IPO ID
                         Body3 = t2.Date.ToString(),      // IPO Date.
                         Body4 = t2.ID.ToString(),        //IPO Value.
                         Body5 = "",                 //Master L/C
                         Body6 = "",      //Master L/C Date
                         Body7 = "",    //IPO Value set inside the loop
                         Body8 = t5.CID + "/" + t4.Style, // Style No
                         Body9 = t6.Name,                 // Buyer Name
                         Body10 = "",         //Remarks
                         Body11 = "",    //B2B L/C Value
                         Body13 = t7.ID.ToString(),       //Supplier ID.
                         Body14 = t2.ID.ToString()        //PO ID.
                     }).ToList();

            var v = (from t1 in db.Acc_LCAlocation
                     join t2 in db.Mkt_PO
                     on t1.Mkt_POFK equals t2.ID
                     join t3 in db.Commercial_MasterLC
                     on t1.Commercial_MasterLCFK equals t3.ID
                     join t4 in db.Mkt_BOM
                     on t2.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder
                     on t4.Common_TheOrderFk equals t5.ID
                     join t6 in db.Mkt_Buyer
                     on t5.Mkt_BuyerFK equals t6.ID
                     join t7 in db.Common_Supplier
                     on t2.Common_SupplierFK equals t7.ID
                     where t2.Common_SupplierFK == Id
                     select new ReportDocType
                     {
                         HeaderLeft1 = t7.Name,         // 
                         HeaderLeft2 = t7.Address,      // Supplier Info
                         HeaderLeft3 = t7.Mobile,       //
                         HeaderLeft4 = t7.Email,

                         Body1 = t1.Date.ToString(),      // L/C Date
                         Body2 = t2.CID,                  // IPO ID
                         Body3 = t2.Date.ToString(),      // IPO Date.
                         Body4 = t2.ID.ToString(),        //IPO Value.
                         Body5 = t3.Name,                 //Master L/C
                         Body6 = t3.Date.ToString(),      //Master L/C Date
                         Body7 = t1.Value.ToString(),     //IPO Value set inside the loop
                         Body8 = t5.CID + "/" + t4.Style, // Style No
                         Body9 = t6.Name,                 // Buyer Name
                         Body10 = t1.Description,         //Remarks
                         Body11 = t1.Value.ToString(),    //B2B L/C Value
                         //Body13 = t7.ID.ToString(),       //Supplier ID.
                         Body14 = t2.ID.ToString(),         //PO ID. 
                         Body13 = Id.ToString()            //Supplier ID.                       
                     }).ToList();

            //var v2 = v1.Where(x=>x.bod) 

            var x = v1.Union(v).ToList();
            decimal body4 = 0;
            decimal body7 = 0;
            decimal body12 = 0;
            decimal body11 = 0;
            foreach (ReportDocType r in x)
            {

                r.Body1 = r.Body1.Substring(0, r.Body1.Length - 8);
                r.Body3 = r.Body3.Substring(0, r.Body3.Length - 8);
                if (r.Body6!="")
                {
                    
                r.Body6 = r.Body6.Substring(0, r.Body6.Length - 8);
                }
              
                r.Body4 = TotlaValue(r.Body4).Substring(0,8);
                r.Body12 = TotlaBalance(r.Body13);
                decimal.TryParse(r.Body12, out body12);
                decimal.TryParse(r.Body11, out body11);
                decimal.TryParse(r.Body4, out body4);           
                decimal.TryParse(r.Body7, out body7);
                r.Body7 = (body12- body11).ToString();
               
            }



            this.SupplierLedger = x.ToList();
        }

        private string GetPoTotal(string poID)
        {
            int id = 0;
            int.TryParse(poID, out id);
            db = new xOssContext();
            var v = (from t1 in db.Mkt_POSlave
                     where t1.Mkt_POFK == id && t1.Active == true
                     select t1.Price).Sum().ToString();
            return v;
        }
        private string GetTotalPoValue(string poID)
        {
            int id = 0;
            int.TryParse(poID, out id);
            db = new xOssContext();

            var r = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                     where t1.Active == true
                     && t1.Mkt_POFK == id
                     select new 
                     {
                        Price = t1.Price,
                        Trequired = t1.TotalRequired
                     }).ToList();
            decimal? g = 0;
            decimal? h = 0;
            decimal? R = 0;
            foreach (var a in r)
            {
                g = a.Price;
                h = a.Trequired;
                R += (g * h);              
            }                       
            string s = R.Value.ToString("F");
            return s;
        }
        public string TotlaBalance(string Sid)
        {
            db = new xOssContext();
            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join t2 in db.Mkt_PO on Mkt_POSlave.Mkt_POFK equals t2.ID
                     join t3 in db.Common_Supplier on t2.Common_SupplierFK equals t3.ID
                     //where Mkt_POSlave.Mkt_POFK.ToString() == poID && Mkt_POSlave.Active == true && t2.Active==true
                     where t2.Common_SupplierFK.ToString()==Sid && Mkt_POSlave.Active == true && t2.Active==true
                     select new
                     {
                         PoslaveTotal=Mkt_POSlave.TotalRequired * Mkt_POSlave.Price
                         
                     }).ToList();
            decimal? e = 0;
            decimal? f = 0;
            //decimal? g = 0;
            
            foreach (var item in a)
            {
                //Convert.ToDecimal(item);
                e = item.PoslaveTotal;
                f += e;
            }
           
            string s = f.Value.ToString("F");

            return s;
            

        }
        public string TotlaValue(string poID)
        {
            db = new xOssContext();
            var a = (from Mkt_POSlave in db.Mkt_POSlave

                     where Mkt_POSlave.Mkt_POFK.ToString() == poID && Mkt_POSlave.Active==true
                     select (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)).Sum().ToString();
          
            return a;

        }
        private decimal GetTotalReceviedValeByDate(DateTime dt, int poID, string challan)
        {
            var v1 = (
                      from t2 in db.Mkt_POSlave

                      join t3 in db.Mkt_POSlave_Receiving
                      on t2.ID equals t3.Mkt_POSlaveFK
                      where t3.Date == dt && t3.Challan == challan && t2.Mkt_POFK == poID
                      select
                      (
                          t3.Quantity * t2.Price

                      )).Sum();
            return v1.Value;
        }
        internal void GetSupplierLedger2B2bDocInfo(int SupplierID)
        {
            db = new xOssContext();

            var v1 = (from t1 in db.Mkt_PO
                      join t2 in db.Mkt_POSlave
                      on t1.ID equals t2.Mkt_POFK
                      join t3 in db.Mkt_POSlave_Receiving
                      on t2.ID equals t3.Mkt_POSlaveFK
                      where t1.Common_SupplierFK == SupplierID
                      select new 
                      {
                          Date = t3.Date,
                          POID = t1.ID,
                          Challan = t3.Challan,
                          GoodReceivedValue =1.2M

                      }).AsEnumerable().OrderBy(x => x.POID).Distinct().ToList();
            List<VmSupplierTransaction> vmST = new List<VmSupplierTransaction>();
            foreach (var v2 in v1)
            {
                VmSupplierTransaction vmSupplierTransaction = new VmSupplierTransaction();
                vmSupplierTransaction.POID = v2.POID;
                vmSupplierTransaction.Date = v2.Date;
                vmSupplierTransaction.Challan = v2.Challan;
                vmSupplierTransaction.GoodReceivedValue =  (decimal)GetTotalReceviedValeByDate(v2.Date, v2.POID, v2.Challan);
                vmST.Add(vmSupplierTransaction);
            }
            var lc = (from t1 in db.Acc_POPH
                     join t2 in db.Mkt_PO 
                     on t1.Mkt_POFK equals t2.ID
                     where t2.Common_SupplierFK == SupplierID
                     select new
                     {
                         Date = t1.Date,
                         POID = t2.ID,
                         LC = t1.LC_D,
                         PaidAmount = t1.Amount
                         

                     }).AsEnumerable().OrderBy(x => x.POID).Distinct().ToList();
            foreach (var v2 in lc)
            {
                VmSupplierTransaction vmSupplierTransaction = new VmSupplierTransaction();
                vmSupplierTransaction.POID = v2.POID;
                vmSupplierTransaction.Date = v2.Date;
                vmSupplierTransaction.B2BLC = v2.LC;
                vmSupplierTransaction.PaidAmount = v2.PaidAmount.Value;
                vmST.Add(vmSupplierTransaction);
            }

            var v = (
                from tt in vmST
                join t2 in db.Mkt_PO
                     on tt.POID equals t2.ID                
                     join t3 in db.Common_Supplier
                     on t2.Common_SupplierFK equals t3.ID
                     join t4 in db.Mkt_BOM
                     on t2.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder
                     on t4.Common_TheOrderFk equals t5.ID
                     join t6 in db.Mkt_Buyer
                     on t5.Mkt_BuyerFK equals t6.ID
                     join t7 in db.Commercial_B2bLC
                     on t3.ID equals t7.Common_SupplierFK
                     join t8 in db.Commercial_MasterLC
                     on t7.Commercial_UDFK equals t8.ID                    
                     where t2.Common_SupplierFK == SupplierID
                     select new ReportDocType
                     {
                         HeaderLeft1 = t3.Name,             // 
                         HeaderLeft2 = t3.Address,          // Supplier Info
                         HeaderLeft3 = t3.Mobile,           //
                         HeaderLeft4 = t3.Email,
                       

                         Body1 = tt.Date.ToString(),        // Payment Date
                         Body2 = t6.Name,                   // Buyer Name                   
                         Body3 = t5.CID + "/" + t4.Style,   // Style No.
                         Body4 = t2.CID,                    // PO ID
                         Body5 = t2.ID.ToString(),          // PO Value 

                         Body6 = tt.GoodReceivedValue.ToString("F"),       // Payable amount goods received value
                         Body7 = tt.PaidAmount.ToString("F"),       //Doc. paid OK
                         Body8 = tt.Due.ToString(),         //Due
                         Body9 = t8.Name,                   // Master L/C Number
                         Body10 = "Supplier PI",            //Come from Commercial
                         Body11 = tt.Challan,            //Mkt_POSlave_Receiving.Challan
                         Body12 = t2.ID.ToString(),
                         Body13 = "",
                        
                     }).OrderBy(x=>Convert.ToDateTime(x.Body1)).ToList();

            decimal body6 = 0, body7 = 0, body13= 0;
            // decimal ss = body6 - body7;

            decimal previouDue = 0, Body6 = 0;

            foreach (ReportDocType r in v)
            {

                r.Body1 = r.Body1.Substring(0, r.Body1.Length - 8);
                r.Body5 = GetTotalPoValue(r.Body12);
                //r.Body6 = GetSupplierTotalDue(r.Body12);
                decimal.TryParse(r.Body6, out body6);
                decimal.TryParse(r.Body7, out body7);
                body13 = body6 - body7 + previouDue;
              
                r.Body8 = (body13).ToString("F"); //due.ToString();
                //r.Body11 = GetChallanNo(r.Body11);
                previouDue = body13;

            }
          
            this.SupplierLedger = v.ToList();

        }

     


        private string GetSupplierTotalDue(string poid)
        {
            int pOId = 0;
            int.TryParse(poid, out pOId);
            var db = new xOssContext();
            var received = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in db.Mkt_POSlave on t1.ID equals t3.Mkt_POFK
                     join t4 in db.Mkt_POSlave_Receiving on t3.ID equals t4.Mkt_POSlaveFK
                     where t1.ID == pOId && t3.Active== true && t4.IsReturn == false
                     select (t3.Price * t4.Quantity)).Sum().GetValueOrDefault();
            var returned = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in db.Mkt_POSlave on t1.ID equals t3.Mkt_POFK
                     join t4 in db.Mkt_POSlave_Receiving on t3.ID equals t4.Mkt_POSlaveFK
                     where t1.ID == pOId && t3.Active == true && t4.IsReturn == true
                     select (t3.Price * t4.Quantity)).Sum().GetValueOrDefault();
            decimal result = received - returned;
            return result.ToString("F");
        }
        private string GetChallanNo(string poid)
        {
            int pOId = 0;
            int.TryParse(poid, out pOId);
            var db =new xOssContext();
            var v = (from t2 in db.Mkt_PO
                    join t9 in db.Mkt_POSlave 
                    on t2.ID equals t9.Mkt_POFK
                    join t10 in db.Mkt_POSlave_Receiving 
                    on t9.ID equals t10.Mkt_POSlaveFK
                    where t2.ID== pOId
                    select new { t10.Challan }).Distinct().ToList();
            string chl = "";
            
            foreach(var s in v)
            {
                chl += s.Challan + ",";
            }
            return chl;
        }

        private string PAmountGoodReceivedValue(string poID)
        {
            decimal id = 0;
            decimal.TryParse(poID, out id);
            db = new xOssContext();

            var received = (from Mkt_PO in db.Mkt_PO
                            join Mkt_POSlave in db.Mkt_POSlave
                            on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                            join Mkt_POSlave_Receiving in db.Mkt_POSlave_Receiving
                            on Mkt_POSlave.ID equals Mkt_POSlave_Receiving.Mkt_POSlaveFK
                            where Mkt_PO.ID == id
                            && Mkt_POSlave_Receiving.IsReturn == false
                            select (Mkt_POSlave.Price * Mkt_POSlave_Receiving.Quantity)).Sum().GetValueOrDefault();

            var returned = (from Mkt_PO in db.Mkt_PO
                            join Mkt_POSlave in db.Mkt_POSlave
                            on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                            join Mkt_POSlave_Receiving in db.Mkt_POSlave_Receiving
                            on Mkt_POSlave.ID equals Mkt_POSlave_Receiving.Mkt_POSlaveFK
                            where Mkt_PO.ID == id
                            && Mkt_POSlave_Receiving.IsReturn == true
                            select (Mkt_POSlave.Price * Mkt_POSlave_Receiving.Quantity)).Sum().GetValueOrDefault();

            decimal result = received - returned; 

             

            return result.ToString("F");

        }

        //private string DocumentPaid(string poID)
        //{
        //    decimal id = 0;
        //    decimal.TryParse(poID, out id);
        //    db = new xOssContext();
        //    var a = (from Acc_POPH in db.Acc_POPH

        //             where Acc_POPH.Mkt_POFK == id
        //             select new { Acc_POPH.Amount });
        //    foreach (var item in a)
        //    {

        //    }
        //    //if (a == null)
        //    //{
        //    //    a = 0;
        //    //}
        //    return a.ToString();
        //}

    }
}