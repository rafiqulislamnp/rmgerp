﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Common;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_DollarRate
    {
        private xOssContext db;
        public Acc_DollarRate Acc_DollarRate { get; set; }
        public IEnumerable<VmAcc_DollarRate> DataList { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Acc_DollarRate in db.Acc_DollarRate
                     select new VmAcc_DollarRate
                     {
                         Acc_DollarRate = Acc_DollarRate
                     }).Where(x => x.Acc_DollarRate.Active == true).OrderByDescending(x => x.Acc_DollarRate.ID).AsEnumerable();
            this.DataList = a;
        }
        public void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from Acc_DollarRate in db.Acc_DollarRate
                     select new VmAcc_DollarRate
                     {
                         Acc_DollarRate = Acc_DollarRate
                     }).SingleOrDefault(c => c.Acc_DollarRate.ID == id);
            this.Acc_DollarRate = v.Acc_DollarRate;
        }
        public int Add(int userID)
        {
            Acc_DollarRate.AddReady(userID);
            return Acc_DollarRate.Add();
        }
        public bool Edit(int userID)
        {
            return Acc_DollarRate.Edit(userID);
        }
        public bool Delete(int userID)
        {
            return Acc_DollarRate.Delete(userID);
        }
    }
}