﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_AcName
    {
        private xOssContext db;
        public Acc_Type Acc_Type { get; set; }
        public Acc_Chart1 Acc_Chart1 { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
        public Acc_Chart2 Acc_Chart2 { get; set; }
        public Acc_CostPool Acc_CostPool { get; set; }
        public IEnumerable<VmAcc_AcName> DataList { get; set; }
        [DisplayName("Account Head ID")]
        public int AccountHeadID { get; set; }
        [DisplayName("Total Debit")]
        public decimal TotalDebit { get; set; }
        [DisplayName("Total Credit")]
        public decimal TotalCredit { get; set; }
        [DisplayName("Account Head")]
        public string AccountHead { get; set; }
        public int? Acc_TypeID { get; set; }
        public int? Acc_Chart1ID { get; set; }
        public int? Acc_Chart2ID { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = (from tbl in t.GetAcc_Type()
                                  select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease });
            var a = (from Acc_AcName in db.Acc_AcName.AsEnumerable()
                     join Acc_Chart2 in db.Acc_Chart2.AsEnumerable() on Acc_AcName.Acc_Chart2FK equals Acc_Chart2.ID
                     join Acc_Chart1 in db.Acc_Chart1.AsEnumerable() on Acc_Chart2.Acc_Chart1FK equals Acc_Chart1.ID
                     join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                     join Acc_CostPool in db.Acc_CostPool.AsEnumerable() on Acc_AcName.Acc_CostPoolFK equals Acc_CostPool.ID into Acc_CostPool
                     from p in Acc_CostPool.DefaultIfEmpty()
                     select new VmAcc_AcName
                     {
                         Acc_Type = t1,
                         Acc_Chart1 = Acc_Chart1,
                         Acc_Chart2 = Acc_Chart2,
                         Acc_AcName = Acc_AcName,
                         Acc_CostPool = p,
                     }).Where(x => x.Acc_AcName.Active == true).OrderByDescending(x => x.Acc_AcName.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingleAcc_AcName(int id)
        {
            db = new xOssContext();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = (from tbl in t.GetAcc_Type()
                                  select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease });
            var v = (from Acc_AcName in db.Acc_AcName.AsEnumerable()
                     join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                     on Acc_AcName.Acc_Chart2FK equals Acc_Chart2.ID
                     join Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                     on Acc_Chart2.Acc_Chart1FK equals Acc_Chart1.ID
                     join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                     select new VmAcc_AcName
                     {
                         Acc_Type = t1,
                         Acc_Chart1 = Acc_Chart1,
                         Acc_Chart2 = Acc_Chart2,
                         Acc_AcName = Acc_AcName
                     }).Where(c => c.Acc_AcName.ID == id).SingleOrDefault();
            this.Acc_AcName = v.Acc_AcName;
            this.Acc_TypeID = v.Acc_Type.ID;
            this.Acc_Chart1ID = v.Acc_Chart1.ID;
            this.Acc_Chart2ID = v.Acc_Chart2.ID;
        }

        public int Add(int userID)
        {
            Acc_AcName.AddReady(userID);
            return Acc_AcName.Add();
        }

      
         
        public bool Edit(int userID)
        {
            return Acc_AcName.Edit(userID);
        }

        public void AccountTypeWiseInventory()
        {
            Acc_Type Acc_Type = new Acc_Type();
            var a = Acc_Type.GetAcc_Type();

            List<VmAcc_AcName> list = new List<VmAcc_AcName>();
            foreach (var x in a)
            {
                VmAcc_AcName vmAccAcName = new VmAcc_AcName();
                vmAccAcName.AccountHeadID = x.ID;
                vmAccAcName.AccountHead = x.Name;
                vmAccAcName.TotalDebit = vmAccAcName.GetTotalDebit(x.ID);
                vmAccAcName.TotalCredit = vmAccAcName.GetTotalCredit(x.ID);

                list.Add(vmAccAcName);

            }

            this.DataList = list;
        }

        private decimal GetTotalCredit(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID
                     join t3 in db.Acc_Chart2 on t2.Acc_Chart2FK equals t3.ID
                     join t4 in db.Acc_Chart1 on t3.Acc_Chart1FK equals t4.ID

                     where t1.Active == true && t4.Acc_TypeFK == id
                     select t1.Credit).ToList();
            decimal credit = 0;
            if (v != null)
            {
                credit = v.Sum();
            }
            return credit;
        }

        private decimal GetTotalDebit(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID
                     join t3 in db.Acc_Chart2 on t2.Acc_Chart2FK equals t3.ID
                     join t4 in db.Acc_Chart1 on t3.Acc_Chart1FK equals t4.ID

                     where t1.Active == true && t4.Acc_TypeFK == id
                     select t1.Debit).ToList();
            decimal debit = 0;
            if (v != null)
            {
                debit = v.Sum();
            }
            return debit;
        }
        private decimal GetTotalChart1Debit(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID
                     join t3 in db.Acc_Chart2 on t2.Acc_Chart2FK equals t3.ID

                     where t1.Active == true && t3.Acc_Chart1FK == id
                     select t1.Debit).ToList();
            decimal debit = 0;
            if (v != null)
            {
                debit = v.Sum();
            }
            return debit;
        }

        private decimal GetTotalChart2Debit(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID

                     where t1.Active == true && t2.Acc_Chart2FK == id
                     select t1.Debit).ToList();
            decimal debit = 0;
            if (v != null)
            {
                debit = v.Sum();
            }
            return debit;
        }
        private decimal GetTotalChart1Credit(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID
                     join t3 in db.Acc_Chart2 on t2.Acc_Chart2FK equals t3.ID

                     where t1.Active == true && t3.Acc_Chart1FK == id
                     select t1.Credit).ToList();
            decimal credit = 0;
            if (v != null)
            {
                credit = v.Sum();
            }
            return credit;
        }
        private decimal GetTotalChart2Credit(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID

                     where t1.Active == true && t2.Acc_Chart2FK == id
                     select t1.Credit).ToList();
            decimal credit = 0;
            if (v != null)
            {
                credit = v.Sum();
            }
            return credit;
        }
        public void AccountChart1WiseInventory(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Acc_Chart1
                     where t1.Active == true && t1.Acc_TypeFK == id
                     select new VmAcc_AcName
                     {
                         Acc_Chart1 = t1
                     }).OrderByDescending(x => x.Acc_Chart1.ID).AsEnumerable();
            List<VmAcc_AcName> list = new List<VmAcc_AcName>();
            foreach (var x in a)
            {
                VmAcc_AcName vmAccAcName = new VmAcc_AcName();
                vmAccAcName.AccountHeadID = x.Acc_Chart1.ID;
                vmAccAcName.AccountHead = x.Acc_Chart1.Name;
                vmAccAcName.TotalDebit = vmAccAcName.GetTotalChart1Debit(x.Acc_Chart1.ID);
                vmAccAcName.TotalCredit = vmAccAcName.GetTotalChart1Credit(x.Acc_Chart1.ID);

                list.Add(vmAccAcName);
            }

            this.DataList = list;
        }

        public VmAcc_AcName SelectSingleAccountType(int id)
        {
            VmAcc_AcName vmAccAcName = new VmAcc_AcName();
            Acc_Type accType = new Acc_Type();
            Acc_Type type = accType.GetAcc_Type().FirstOrDefault(x => x.ID == id);

            vmAccAcName.Acc_Type = type;
            return vmAccAcName;
        }



        public VmAcc_AcName SelectSingleChartOne(int id)
        {
            db = new xOssContext();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = (from tbl in t.GetAcc_Type()
                                  select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease });
            var v = (from t1 in db.Acc_Chart1.AsEnumerable()
                     join t2 in listOfAcc_Type on t1.Acc_TypeFK equals t2.ID
                     select new VmAcc_AcName
                     {
                         Acc_Type = t2,
                         Acc_Chart1 = t1,

                     }).SingleOrDefault(c => c.Acc_Chart1.ID == id);
            //this.Acc_Type = v.Acc_Type;
            //this.Acc_Chart1 = v.Acc_Chart1;
            return v;
        }


        public void AccountChart2WiseInventory(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Acc_Chart2
                     where t1.Active == true && t1.Acc_Chart1FK == id
                     select new VmAcc_AcName
                     {
                         Acc_Chart2 = t1
                     }).OrderByDescending(x => x.Acc_Chart2.ID).AsEnumerable();
            List<VmAcc_AcName> list = new List<VmAcc_AcName>();
            foreach (var x in a)
            {
                VmAcc_AcName vmAccAcName = new VmAcc_AcName();
                vmAccAcName.AccountHeadID = x.Acc_Chart2.ID;
                vmAccAcName.AccountHead = x.Acc_Chart2.Name;
                vmAccAcName.TotalDebit = vmAccAcName.GetTotalChart2Debit(x.Acc_Chart2.ID);
                vmAccAcName.TotalCredit = vmAccAcName.GetTotalChart2Credit(x.Acc_Chart2.ID);

                list.Add(vmAccAcName);
            }

            this.DataList = list;
        }



        public VmAcc_AcName SelectSingleAccountsName(int id)
        {
            db = new xOssContext();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = (from tbl in t.GetAcc_Type()
                                  select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease });
            var v = (from t1 in db.Acc_Chart2.AsEnumerable()
                     join t2 in db.Acc_Chart1.AsEnumerable() on t1.Acc_Chart1FK equals t2.ID
                     join t3 in listOfAcc_Type on t2.Acc_TypeFK equals t3.ID
                     select new VmAcc_AcName
                     {
                         Acc_Type = t3,
                         Acc_Chart1 = t2,
                         Acc_Chart2 = t1
                     }).SingleOrDefault(c => c.Acc_Chart2.ID == id);
            //this.Acc_Type = v.Acc_Type;
            //this.Acc_Chart1 = v.Acc_Chart1;
            return v;
        }


        public void AccountNameWiseInventory(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Acc_AcName
                     where t1.Active == true && t1.Acc_Chart2FK == id
                     select new VmAcc_AcName
                     {
                         Acc_AcName = t1
                     }).OrderByDescending(x => x.Acc_AcName.ID).AsEnumerable();
            List<VmAcc_AcName> list = new List<VmAcc_AcName>();
            foreach (var x in a)
            {
                VmAcc_AcName vmAccAcName = new VmAcc_AcName();
                vmAccAcName.AccountHeadID = x.Acc_AcName.ID;
                vmAccAcName.AccountHead = x.Acc_AcName.Name;
                vmAccAcName.TotalDebit = vmAccAcName.GetTotalAccAcNameDebit(x.Acc_AcName.ID);
                vmAccAcName.TotalCredit = vmAccAcName.GetTotalAccAcNameCredit(x.Acc_AcName.ID);

                list.Add(vmAccAcName);
            }

            this.DataList = list;
        }

        private decimal GetTotalAccAcNameDebit(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave

                     where t1.Active == true && t1.Acc_AcNameFK == id
                     select t1.Debit).ToList();
            decimal debit = 0;
            if (v != null)
            {
                debit = v.Sum();
            }
            return debit;
        }

        private decimal GetTotalAccAcNameCredit(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave

                     where t1.Active == true && t1.Acc_AcNameFK == id
                     select t1.Credit).ToList();
            decimal credit = 0;
            if (v != null)
            {
                credit = v.Sum();
            }
            return credit;
        }
    }
}