﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_IntegrationJournal
    {
        private xOssContext db;
        [DisplayName("Transaction Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public int FromAccNo { get; set; }
        public int ToAccNo { get; set; }
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public decimal Amount { get; set; }
        public int CurrencyType { get; set; } = 2;
        public decimal CurrencyRate { get; set; } = 1;
        public int TableIdFk { get; set; }
        public int TableRowIdFk { get; set; }
        public int? CostPool { get; set; }
        public string AddJounalFromIntegratedTable()
        {
            try
            {
                Acc_Journal accJournal = new Acc_Journal();
                accJournal.Date = Date;
                accJournal.Description = Descriptions;
                accJournal.Acc_CostPoolFK = CostPool ?? 0;
                accJournal.Title = Title;
                accJournal.TableIdFk = TableIdFk;
                accJournal.TableRowIdFk = TableRowIdFk;
                accJournal.isFinal = true;
                accJournal.Active = new bool?(true);

                accJournal.AddReady(UserId);

                var returnval = accJournal.Add();
                if (returnval != -1 && returnval > 0)
                {
                    Acc_JournalSlave Acc_JournalSlave = new Acc_JournalSlave();
                    Acc_JournalSlave.AddReady(UserId);
                    Acc_JournalSlave.CurrencyType = CurrencyType;
                    Acc_JournalSlave.CurrencyRate = CurrencyRate;
                    Acc_JournalSlave.Description = "";
                    Acc_JournalSlave.Debit = 0;
                    Acc_JournalSlave.Credit = Amount;
                    Acc_JournalSlave.Acc_AcNameFK = FromAccNo;
                    Acc_JournalSlave.Acc_JournalFK = returnval;
                    Acc_JournalSlave.Add();

                    Acc_JournalSlave.Debit = Amount;
                    Acc_JournalSlave.Credit = 0;
                    Acc_JournalSlave.Acc_AcNameFK = ToAccNo;
                    Acc_JournalSlave.Add();
                }
                else
                {
                    return "";
                }

                db = new xOssContext();
                var accExistId = db.Acc_Journal.Find(returnval);

                if (accExistId != null)
                    return accExistId.JournalCId;
                else
                    return "";
            }
            catch (Exception e)
            {
                return "";
            }
        }
        public string EditJounalFromIntegratedTable()
        {
            try
            {
                db = new xOssContext();
                var accExistJournal = db.Acc_Journal.SingleOrDefault(x => x.TableIdFk == TableIdFk && x.TableRowIdFk == TableRowIdFk && x.Title == Title);

                if (accExistJournal != null)
                {
                    List<Acc_JournalSlave> journalslave =
                        db.Acc_JournalSlave.Where(x => x.Acc_JournalFK == accExistJournal.ID).ToList();
                    foreach (Acc_JournalSlave accJournalSlave in journalslave)
                    {
                        accJournalSlave.Delete(UserId);
                    }


                    Acc_JournalSlave Acc_JournalSlave = new Acc_JournalSlave();
                    Acc_JournalSlave.AddReady(UserId);
                    Acc_JournalSlave.CurrencyType = CurrencyType;
                    Acc_JournalSlave.CurrencyRate = CurrencyRate;
                    Acc_JournalSlave.Description = Descriptions;
                    Acc_JournalSlave.Debit = 0;
                    Acc_JournalSlave.Credit = Amount;
                    Acc_JournalSlave.Acc_AcNameFK = FromAccNo;
                    Acc_JournalSlave.Acc_JournalFK = accExistJournal.ID;
                    Acc_JournalSlave.Add();

                    Acc_JournalSlave.Debit = Amount;
                    Acc_JournalSlave.Credit = 0;
                    Acc_JournalSlave.Acc_AcNameFK = ToAccNo;
                    Acc_JournalSlave.Add();
                }
                else
                {
                    var journalcid = AddJounalFromIntegratedTable();
                    return journalcid;
                }

                db = new xOssContext();
                var accExistId = db.Acc_Journal.Find(accExistJournal.ID);

                if (accExistId != null)
                    return accExistId.JournalCId;
                else
                    return "";
            }
            catch (Exception e)
            {
                return "";
            }
        }
        public bool DeleteJounalFromIntegratedTable()
        {
            try
            {
                db = new xOssContext();
                Acc_Journal accExistJournal = db.Acc_Journal.SingleOrDefault(x => x.TableIdFk == TableIdFk && x.TableRowIdFk == TableRowIdFk);

                if (accExistJournal != null)
                {
                    List<Acc_JournalSlave> journalslave = db.Acc_JournalSlave.Where(x => x.Acc_JournalFK == accExistJournal.ID).ToList();
                    bool slavedelete = false;
                    foreach (var item in journalslave)
                    {
                        slavedelete = item.Delete(UserId);
                    }
                    if (slavedelete)
                    {
                        slavedelete = accExistJournal.Delete(UserId);
                    }
                    return slavedelete;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
    public class VmAcc_IntegrationAccountHead
    {
        private xOssContext db;
        public int UserId { get; set; }
        public int Chart2Id { get; set; }
        public int ExistAccId { get; set; }
        public string AccountName { get; set; }

        public int AddFromIntegratedModule()
        {
            Acc_AcName accAcName = new Acc_AcName();
            accAcName.Acc_Chart2FK = Chart2Id;
            accAcName.Name = AccountName;
            accAcName.Code = "";
            accAcName.Description = AccountName;
            accAcName.Editable = false;
            accAcName.AddReady(UserId);
            var returnval = accAcName.Add();
            return returnval;
        }
        public int EditFromIntegratedModule()
        {
            db = new xOssContext();
            Acc_AcName accAcName = new Acc_AcName();
            var accExistId = db.Acc_AcName.Find(ExistAccId);
            if (accExistId != null)
            {
                accAcName = accExistId;
                accAcName.Acc_Chart2FK = Chart2Id;
                accAcName.Name = AccountName;
                var s = accAcName.Edit(UserId);
                return accAcName.ID;
            }
            else
            {
                accAcName.Acc_Chart2FK = Chart2Id;
                accAcName.Name = AccountName;
                accAcName.Code = "";
                accAcName.Description = AccountName;
                accAcName.Editable = false;
                accAcName.AddReady(UserId);
                var returnval = accAcName.Add();
                return returnval;
            }
        }
    }
    public static class VmAcc_Database_Table
    {
        private static xOssContext db;
        public static int TableName(string className)
        {
            db = new xOssContext();
            var accDatabaseTable = db.Acc_Database_Table.Single(x => x.Name == className);
            return accDatabaseTable.ID;
        }
    }

    public class VmAcc_ChequeIntegration
    {
        private xOssContext db;
        public Acc_Notifier Acc_Notifier { get; set; }
        public int UserId { get; set; }

        public int AddCheque()
        {
            Acc_Notifier accJournal = new Acc_Notifier();
            accJournal = Acc_Notifier;
            accJournal.AddReady(UserId);
            return accJournal.Add();
        }
        public bool AddDelete(int id)
        {
            db = new xOssContext();
            Acc_Notifier accExistJournal = db.Acc_Notifier.SingleOrDefault(x => x.TableIdFk == Acc_Notifier.TableIdFk && x.RowIdFk == Acc_Notifier.RowIdFk);

            var s = accExistJournal?.Delete(UserId);
            return s ?? false;
        }
        public bool EditCheque(int userID)
        {
            return Acc_Notifier.Edit(userID);
        }
    }
}