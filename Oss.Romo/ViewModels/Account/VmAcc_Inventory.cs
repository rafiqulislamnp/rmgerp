﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_Inventory
    {
        private xOssContext db;
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Mkt_POSlave_Consumption Mkt_POSlave_Consumption { get; set; }
        public Mkt_POSlave_Receiving Mkt_POSlave_Receiving { get; set; }
        public Raw_Item Raw_ItemFK { get; set; }
        public Common_Unit Common_Unit { get; set; }
        public Common_Currency Common_Currency { get; set; }
        decimal? Quantity { get; set; }
        private decimal? Value { get; set; }
        public DataTable Inventory()
        {
            db = new xOssContext();
            DataTable dt = new DataTable();
            dt.Columns.Add("BOM", typeof(string));
            dt.Columns.Add("PO", typeof(string));
            dt.Columns.Add("Items", typeof(string));
            dt.Columns.Add("Quantity", typeof(string));
            dt.Columns.Add("Value", typeof(string));
            //dt.Columns.Add("Unit", typeof(string));
            //dt.Columns.Add("Currency", typeof(string));
            var a = (from Mkt_PO in db.Mkt_PO
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_PO.Mkt_BOMCID equals Mkt_BOM.CID

                     join Mkt_POSlave in db.Mkt_POSlave
                     on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join Common_Currency in db.Common_Currency
                     on Mkt_PO.Common_CurrencyFK equals Common_Currency.ID

                     select new
                     {
                         POCID = Mkt_PO.CID,
                         BOMCID = Mkt_BOM.CID,
                         Items = Raw_Item.Name,
                         ID = Mkt_POSlave.ID,
                         Unit = Common_Unit.Name,
                         Currency = Common_Currency.Symbol
                     }

                     ).ToList();
            foreach (var po in a)
            {
                dt.Rows.Add(po.BOMCID, po.POCID, po.Items, po.Unit+" "+GetQuantity(po.ID).ToString(), po.Currency +""  + Value.ToString());
            }

            return dt;
        }
        public decimal? GetQuantity(int id)
        {
            decimal? quantityR = (from Mkt_POSlave in db.Mkt_POSlave
                                  join Mkt_POSlave_Receiving in db.Mkt_POSlave_Receiving
                                    on Mkt_POSlave.ID equals Mkt_POSlave_Receiving.Mkt_POSlaveFK
                                    where Mkt_POSlave_Receiving.Mkt_POSlaveFK == id
                                  select
                                   Mkt_POSlave_Receiving.Quantity

                     ).Sum();

            decimal? quantityC = (from Mkt_POSlave in db.Mkt_POSlave
                                  join Mkt_POSlave_Consumption in db.Mkt_POSlave_Consumption
                                    on Mkt_POSlave.ID equals Mkt_POSlave_Consumption.Mkt_POSlaveFK
                                    where Mkt_POSlave_Consumption.Mkt_POSlaveFK == id
                                  select
                                   Mkt_POSlave_Consumption.Quantity

                    ).Sum();
            decimal? rate = (from Mkt_POSlave in db.Mkt_POSlave                                  
                                  where Mkt_POSlave.ID == id
                                  select
                                   Mkt_POSlave.Price
                    ).FirstOrDefault();
            if (quantityR == null)
            {
                quantityR = 0;
            }
            if (quantityC == null)
            {
                quantityC = 0;
            }
           
            Value = (quantityR - quantityC) * rate;
            if (Value == null)
            {
                Value = 0;
            }
            return quantityR - quantityC;
        }
        public decimal GetValue(int id)
        {
            return 0;
        }
    }
}