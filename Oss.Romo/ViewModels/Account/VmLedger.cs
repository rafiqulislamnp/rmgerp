﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmLedger
    {
        private xOssContext db;
        public IEnumerable<VmLedger> DataList { get; set; }
        public IEnumerable<VmAcc_Journal> DataList1{ get; set; }
        public IEnumerable<VmAcc_JournalSlave> DataList2 { get; set; }
        public decimal? Balance { get; set; }



    }
}