﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_Chart2
    {
        private xOssContext db;
        public Acc_Type Acc_Type { get; set; }
        public Acc_Chart1 Acc_Chart1 { get; set; }
        public Acc_Chart2 Acc_Chart2 { get; set; }
        public IEnumerable<VmAcc_Chart2> DataList { get; set; }
  
        public int? Acc_TypeID { get; set; }

       
        public void InitialDataLoad()
        {
            db = new xOssContext();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                 select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var a = (from Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                     join Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                     on Acc_Chart2.Acc_Chart1FK equals Acc_Chart1.ID
                     join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                     select new VmAcc_Chart2
                     {
                         Acc_Type = t1,
                         Acc_Chart1 = Acc_Chart1,
                         Acc_Chart2 = Acc_Chart2

                     }).Where(x => x.Acc_Chart2.Active == true).OrderByDescending(x => x.Acc_Chart2.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingleAcc_Chart2(int id)
        {
            db = new xOssContext();
            Acc_Type t = new Acc_Type();
            var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                 select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var v = (from Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                     join Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                     on Acc_Chart2.Acc_Chart1FK equals Acc_Chart1.ID
                     join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID

                     where Acc_Chart2.Active == true
                  
                     select new VmAcc_Chart2
                     {
                         Acc_Type = t1,
                         Acc_Chart1 = Acc_Chart1,
                         Acc_Chart2 = Acc_Chart2
                     }).Where(c => c.Acc_Chart2.ID == id).SingleOrDefault();
            this.Acc_Chart2 = v.Acc_Chart2;
            this.Acc_Chart1 = v.Acc_Chart1;
            this.Acc_Type = v.Acc_Type;
            this.Acc_TypeID = v.Acc_Type.ID;

        }
        
        public int Add(int userID)
        {
            Acc_Chart2.AddReady(userID);
            return Acc_Chart2.Add();

        }
        public bool Edit(int userID)
        {
            return Acc_Chart2.Edit(userID);
        }





       
    }
}