﻿using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Oss.Romo.Models.Accounts;
using Oss.Romo.ViewModels.Common;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_SupplierAdjustment
    {
        private xOssContext db;
        public IEnumerable<VmAcc_SupplierAdjustment> DataList { get; set; }
        public Acc_SupplierAdjustment Acc_SupplierAdjustment { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public VmControllerHelper VmControllerHelper { get; set; }


        public VmAcc_SupplierAdjustment()
        {
            db = new xOssContext();
            VmControllerHelper = new VmControllerHelper();
        }

        public void SupplierAdjustmentById(int id)
        {
            var v = (from t1 in db.Acc_SupplierAdjustment
                     join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     where t1.Active == true && t1.Common_SupplierFk == id
                     select new VmAcc_SupplierAdjustment
                     {
                         Acc_SupplierAdjustment = t1,
                         Common_Supplier = t2

                     }).OrderByDescending(x => x.Acc_SupplierAdjustment.ID).AsEnumerable();
            this.DataList = v;
        }

        public void SelectSingleSupplier(int id)
        {
            var v = (from t1 in db.Common_Supplier
                     where t1.Active == true && t1.ID == id
                     select new VmAcc_SupplierAdjustment
                     {                        
                         Common_Supplier = t1
                     }).FirstOrDefault();           
            this.Common_Supplier = v.Common_Supplier;
        }

        public void SelectSingle(string iD)
        {
            int id = 0;
            iD = this.VmControllerHelper.Decrypt(iD);
            Int32.TryParse(iD, out id);
            db = new xOssContext();
            var v = (from t1 in db.Acc_SupplierAdjustment
                     join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     where t1.Active == true
                     select new VmAcc_SupplierAdjustment
                     {
                         Acc_SupplierAdjustment = t1,
                         Common_Supplier = t2
                     }).Where(c => c.Acc_SupplierAdjustment.ID == id).SingleOrDefault();
            this.Acc_SupplierAdjustment = v.Acc_SupplierAdjustment;
            this.Common_Supplier = v.Common_Supplier;
        }

        public int Add(int userID)
        {
            Acc_SupplierAdjustment.FirstCreatedBy = userID;
            Acc_SupplierAdjustment.AddReady(userID);
            return Acc_SupplierAdjustment.Add();
        }

        public bool Edit(int userID)
        {
            return Acc_SupplierAdjustment.Edit(userID);
        }

        public bool Delete(int userID)
        {
            return Acc_SupplierAdjustment.Delete(userID);
        }
    }
}