﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_TrialBalance
    {
        private xOssContext db;
        public List<VmAcc_TrialBalanceReport> DataList { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ChartOfAccHeadNo { get; set; }
        public int CurrencyType { get; set; } = 2;
        public decimal CurrencyRate { get; set; } = 1;
        public void InitialDataLoad()
        {
            db = new xOssContext();
            Acc_Type t = new Acc_Type();

            var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                 select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

            if (ChartOfAccHeadNo == "Chart Of 2 Account Head")
            {
                var v1 = (from Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                          join Acc_AcName in db.Acc_AcName.AsEnumerable()
                              on Acc_JournalSlave.Acc_AcNameFK equals Acc_AcName.ID
                          join Acc_Journal in db.Acc_Journal.AsEnumerable()
                              on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                          join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                              on Acc_AcName.Acc_Chart2FK equals Acc_Chart2.ID
                          join Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                              on Acc_Chart2.Acc_Chart1FK equals Acc_Chart1.ID
                          where Acc_JournalSlave.Active == true && Acc_AcName.Active == true && Acc_Journal.Active == true &&
                                Acc_Chart2.Active == true && Acc_Chart1.Active == true && Acc_Journal.isFinal
                                && (Acc_Journal.Date >= FromDate && Acc_Journal.Date <= ToDate)
                          group Acc_JournalSlave by new { Acc_Chart2.Name } into grp
                          select new VmAcc_TrialBalanceReport
                          {
                              Name = grp.Key.Name,
                              Debit = CurrencyType == 1 ? grp.Sum(x => x.Debit) : grp.Sum(x => x.Debit * x.CurrencyRate),
                              Credit = CurrencyType == 1 ? grp.Sum(x => x.Credit) : grp.Sum(x => x.Credit * x.CurrencyRate)
                          }).OrderByDescending(x => x.Name).ToList();

                //var v2_1 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate) 
                //            group t1 by t3.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = g.Sum(x => x.Amount),
                //                Credit = 0
                //            }).ToList();

                //var v2_2 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate)
                //            group t1 by t3.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = 0,
                //                Credit = g.Sum(x => x.Amount)
                //            }).ToList();

                this.DataList = v1
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_TrialBalanceReport
                    {
                        Name = cl.First().Name,
                        Debit = cl.Sum(c => c.Debit),
                        Credit = cl.Sum(c => c.Credit),
                    })
                    .OrderBy(x => x.Name).ToList();
            }
            else if (ChartOfAccHeadNo == "Chart Of 1 Account Head")
            {
                var v1 = (from Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                          join Acc_AcName in db.Acc_AcName.AsEnumerable()
                              on Acc_JournalSlave.Acc_AcNameFK equals Acc_AcName.ID
                          join Acc_Journal in db.Acc_Journal.AsEnumerable()
                              on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                          join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                              on Acc_AcName.Acc_Chart2FK equals Acc_Chart2.ID
                          join Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                              on Acc_Chart2.Acc_Chart1FK equals Acc_Chart1.ID
                          where Acc_JournalSlave.Active == true && Acc_AcName.Active == true && Acc_Journal.Active == true && Acc_Journal.isFinal == true &&
                                Acc_Chart2.Active == true && Acc_Chart1.Active == true && (Acc_Journal.Date >= FromDate && Acc_Journal.Date <= ToDate)
                          group Acc_JournalSlave by new { Acc_Chart1.Name } into grp
                          select new VmAcc_TrialBalanceReport
                          {
                              Name = grp.Key.Name,
                              Debit = CurrencyType == 1 ? grp.Sum(x => x.Debit) : grp.Sum(x => x.Debit * x.CurrencyRate),
                              Credit = CurrencyType == 1 ? grp.Sum(x => x.Credit) : grp.Sum(x => x.Credit * x.CurrencyRate)
                          }).OrderByDescending(x => x.Name).ToList();

                //var v2_1 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate)
                //            group t1 by t4.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = g.Sum(x => x.Amount),
                //                Credit = 0
                //            }).ToList();
                //var v2_2 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate)
                //            group t1 by t4.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = 0,
                //                Credit = g.Sum(x => x.Amount)
                //            }).ToList();

                //var v3_1 = (from t1 in db.Acc_Transaction.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate) && t1.IsFinal == true
                //            group t1 by t4.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = g.Sum(x => x.Amount),
                //                Credit = 0
                //            }).ToList();
                //var v3_2 = (from t1 in db.Acc_Transaction.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate) && t1.IsFinal == true
                //            group t1 by t4.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = 0,
                //                Credit = g.Sum(x => x.Amount)
                //            }).ToList();

                this.DataList = v1
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_TrialBalanceReport
                    {
                        Name = cl.First().Name,
                        Debit = cl.Sum(c => c.Debit),
                        Credit = cl.Sum(c => c.Credit),
                    })
                    .OrderBy(x => x.Name).ToList();
            }
            else if (ChartOfAccHeadNo == "Account Head")
            {
                var v1 = (from Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                          join Acc_AcName in db.Acc_AcName.AsEnumerable()
                              on Acc_JournalSlave.Acc_AcNameFK equals Acc_AcName.ID
                          join Acc_Journal in db.Acc_Journal.AsEnumerable()
                              on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                          join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                              on Acc_AcName.Acc_Chart2FK equals Acc_Chart2.ID
                          join Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                              on Acc_Chart2.Acc_Chart1FK equals Acc_Chart1.ID
                          where Acc_JournalSlave.Active == true && Acc_AcName.Active == true && Acc_Journal.Active == true && Acc_Journal.isFinal == true &&
                                Acc_Chart2.Active == true && Acc_Chart1.Active == true && (Acc_Journal.Date >= FromDate && Acc_Journal.Date <= ToDate)
                          group Acc_JournalSlave by new { Acc_AcName.Name } into grp
                          select new VmAcc_TrialBalanceReport
                          {
                              Name = grp.Key.Name,
                              Debit = CurrencyType == 1 ? grp.Sum(x => x.Debit) : grp.Sum(x => x.Debit * x.CurrencyRate),
                              Credit = CurrencyType == 1 ? grp.Sum(x => x.Credit) : grp.Sum(x => x.Credit * x.CurrencyRate)
                          }).OrderByDescending(x => x.Name).ToList();

                //var v2_1 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate)
                //            group t1 by t2.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = g.Sum(x => x.Amount),
                //                Credit = 0
                //            }).ToList();
                //var v2_2 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate)
                //            group t1 by t2.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = 0,
                //                Credit = g.Sum(x => x.Amount)
                //            }).ToList();

                //var v3_1 = (from t1 in db.Acc_Transaction.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate) && t1.IsFinal == true
                //            group t1 by t2.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = g.Sum(x => x.Amount),
                //                Credit = 0
                //            }).ToList();
                //var v3_2 = (from t1 in db.Acc_Transaction.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //            where t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                //            && (t1.Date >= FromDate && t1.Date <= ToDate) && t1.IsFinal == true
                //            group t1 by t2.Name into g
                //            select new VmAcc_TrialBalanceReport
                //            {
                //                Name = g.Key,
                //                Debit = 0,
                //                Credit = g.Sum(x => x.Amount)
                //            }).ToList();

                this.DataList = v1
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_TrialBalanceReport
                    {
                        Name = cl.First().Name,
                        Debit = cl.Sum(c => c.Debit),
                        Credit = cl.Sum(c => c.Credit),
                    })
                    .OrderBy(x => x.Name).ToList();
            }
        }
    }
    public class VmAcc_TrialBalanceClass
    {
        [DisplayName("Chart of Account Head No")]
        public List<string> ChartOfAccountType { get; set; }

        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public int CurrencyType { get; set; } = 2;
        public decimal CurrencyRate { get; set; } = 1;
    }
    public class VmAcc_TrialBalanceReport
    {
        public string Name { get; set; }
        public Decimal Debit { get; set; }
        public Decimal Credit { get; set; }
    }
}