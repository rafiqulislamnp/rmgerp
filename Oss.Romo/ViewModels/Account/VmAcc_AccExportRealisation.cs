﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Shipment;
using Oss.Romo.ViewModels.Commercial;

namespace Oss.Romo.ViewModels.Account
{
    public class RealSummary
    {
        public string BankName { get; set; }
        public decimal TotalB2b { get; set; }
        public decimal TotalFCBPAR { get; set; }

        public string ExportLC { get; set; }
        public string ExportPoValue { get; set; }

        public string RelisedValue { get; set; }

        public string TotalTT { get; set; }
        public string TotalIfbc { get; set; }
        public string TotalFcbpar { get; set; }
        public string ReaminingExportValue { get; set; }
        public string ReceiveAbleValue { get; set; }

        public decimal Balance { get { return TotalB2b - TotalFCBPAR; } }

    }
    public class VmAcc_AccExportRealisation
    {
        private xOssContext db;
        public Acc_ExportRealisation Acc_ExportRealisation { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime BillOfExchangeDate { get; set; }
        public Commercial_UDSlave Commercial_UDSlave { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Commercial_BillOfExchange Commercial_BillOfExchange { get; set; }
        public Acc_ExportRealisationIndexView SingleData { get; set; }
        public List<Acc_ExportRealisationIndexView> DataList { get; set; }
        public List<RealSummary> DataList1 { get; set; }
        public RealSummary RealSummary { get; set; }
        public VmFilter VmFilter { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var c = (from t1 in db.Acc_ExportRealisation.AsEnumerable()
                     join t2 in db.Commercial_BillOfExchange on t1.Shipment_BillOfExchangeFK equals t2.ID
                     join t3 in db.Commercial_MasterLC on t2.Commercial_MasterLCFK equals t3.ID
                     join t4 in db.Commercial_UDSlave on t3.ID equals t4.Commercial_MasterLCFK
                     join t5 in db.Commercial_UD on t4.Commercial_UDFK equals t5.ID
                     where t1.Active == true
                     select new Acc_ExportRealisationIndexView
                     {
                         Acc_ExportRealisation = t1,
                         Commercial_BillOfExchange = t2,
                         Commercial_MasterLC = t3,
                         Commercial_UDSlave = t4,
                         Commercial_UD = t5
                     }).OrderByDescending(x => x.Acc_ExportRealisation.ID);

            this.DataList = c.ToList();
            DataList1 = new List<RealSummary>();
            var bankList = db.Commercial_Bank.Where(x => x.IsLien == true).ToList();
            foreach (var bank in bankList)
            {
                RealSummary bankRealSummary = new RealSummary();
                bankRealSummary.BankName = bank.Name;
                //bankRealSummary.TotalB2b = GetTotalB2b(bank.ID);
                bankRealSummary.TotalFCBPAR = GetTotalFCBPAR(bank.ID);
                DataList1.Add(bankRealSummary);
            }

        }

        public void InitialDataLoadByFilter(VmFilter model)
        {
            db = new xOssContext();
            if (model.UDId != 0)
            {
                var c = (from t1 in db.Acc_ExportRealisation.AsEnumerable()
                         join t2 in db.Commercial_BillOfExchange on t1.Shipment_BillOfExchangeFK equals t2.ID
                         join t3 in db.Commercial_MasterLC on t2.Commercial_MasterLCFK equals t3.ID
                         join t4 in db.Commercial_UDSlave on t3.ID equals t4.Commercial_MasterLCFK
                         join t5 in db.Commercial_UD on t4.Commercial_UDFK equals t5.ID
                         where t1.Active == true
                         && t5.ID == model.UDId
                         //&& t1.Date >= model.FromDate
                         //&& t1.Date <= model.ToDate
                         select new Acc_ExportRealisationIndexView
                         {
                             Acc_ExportRealisation = t1,
                             Commercial_BillOfExchange = t2,
                             Commercial_MasterLC = t3,
                             Commercial_UDSlave = t4,
                             Commercial_UD = t5
                         }).OrderByDescending(x => x.Acc_ExportRealisation.ID);



                var bankList = db.Commercial_Bank.Where(x => x.IsLien == true && x.ID == model.FilterId).ToList();


                var mLcList = (from t1 in db.Commercial_MasterLC
                               join t2 in db.Commercial_UDSlave
                                   on t1.ID equals t2.Commercial_MasterLCFK
                               where t2.Commercial_UDFK == model.UDId && t1.Active == true && t2.Active == true
                               select t1.ID).ToList();

                var exportLC = db.Commercial_MasterLC.Where(x => mLcList.Contains(x.ID) && x.Active == true).Select(x => x.TotalValue).DefaultIfEmpty(0).Sum();


                var poList =
                    db.Commercial_MasterLCBuyerPO.Where(x => mLcList.Contains(x.Commercial_MasterLCFK.Value) && x.Active == true)
                        .Select(x => x.MKTBOMFK);

                var exportPoValue = (from t1 in db.Commercial_MasterLCBuyerPO
                                     join t2 in db.Mkt_BOM on t1.MKTBOMFK equals t2.ID
                                     where mLcList.Contains(t1.Commercial_MasterLCFK.Value) && t1.Active == true && t2.Active == true
                                     select (t2.QPack * t2.Quantity * t2.UnitPrice)).DefaultIfEmpty(0).Sum();

                var invoice = (from t1 in db.Commercial_InvoiceSlave
                                        join t3 in db.Shipment_OrderDeliverdSchedule on t1.Shipment_OrderDeliverdScheduleFK equals t3.ID
                                        join t2 in db.Mkt_BOM on t3.Mkt_BOMFK equals t2.ID
                                        where poList.Contains(t2.ID) && t1.Active == true && t3.Active == true && t2.Active == true
                                        select t1.Commercial_InvoiceFk).Distinct().ToList();
                decimal receiveAbleValue = 0;
                foreach (var invoiceId in invoice)
                {
                    var inv = db.Commercial_Invoice.Where(x => x.ID == invoiceId).SingleOrDefault();
                    if (inv.IsIncrease == true)
                    {
                        receiveAbleValue += (from t1 in db.Commercial_InvoiceSlave
                                             join t3 in db.Shipment_OrderDeliverdSchedule on t1.Shipment_OrderDeliverdScheduleFK equals t3.ID
                                             join t2 in db.Mkt_BOM on t3.Mkt_BOMFK equals t2.ID
                                             where poList.Contains(t2.ID) && t1.Active == true && t3.Active == true && t2.Active == true
                                             select t3.PackQty * t2.Quantity * t2.UnitPrice).DefaultIfEmpty(0).Sum() + inv.IncDecAmount;

                    }
                    else
                    {
                        receiveAbleValue += (from t1 in db.Commercial_InvoiceSlave
                                             join t3 in db.Shipment_OrderDeliverdSchedule on t1.Shipment_OrderDeliverdScheduleFK equals t3.ID
                                             join t2 in db.Mkt_BOM on t3.Mkt_BOMFK equals t2.ID
                                             where poList.Contains(t2.ID) && t1.Active == true && t3.Active == true && t2.Active == true
                                             select t3.PackQty * t2.Quantity * t2.UnitPrice).DefaultIfEmpty(0).Sum() - inv.IncDecAmount;
                    }

                }


                 



                var RelisedValue = GetTotalRealisedValue(model.UDId);


                var totalB2b = db.Commercial_B2bLC.Where(x => x.Commercial_UDFK == model.UDId && x.Active == true).Select(x => x.Amount).DefaultIfEmpty(0).Sum();
                var totalTT = db.Commercial_B2bLC.Where(x => x.Commercial_UDFK == model.UDId && x.Commercial_BtBLCTypeFK == 3 && x.Active == true).Select(x => x.Amount).DefaultIfEmpty(0).Sum();

                var btbList =
                   db.Commercial_B2bLC.Where(x => x.Commercial_UDFK == model.UDId && x.Active == true)
                       .Select(x => x.ID);
                var totalIfbc = db.Commercial_B2BPaymentInformation.Where(x => btbList.Contains(x.Commercial_B2bLCFK) && x.Active == true).Select(x => x.B2bLCPayment).DefaultIfEmpty(0).Sum();
                var totalFcbpar = GetTotalFCBPAR(model.UDId); ;

                var reaminingExportValue = exportPoValue - receiveAbleValue - RelisedValue;
                receiveAbleValue -= RelisedValue;

                var realSummary = new RealSummary();

                realSummary.ExportLC = exportLC.ToString();
                realSummary.ExportPoValue = exportPoValue.ToString();
                realSummary.ReceiveAbleValue = receiveAbleValue.ToString();
                realSummary.RelisedValue = RelisedValue.ToString();
                realSummary.TotalB2b = totalB2b;
                realSummary.TotalTT = totalTT.ToString();
                realSummary.TotalIfbc = totalIfbc.ToString();
                realSummary.TotalFCBPAR = totalFcbpar;
                realSummary.ReaminingExportValue = reaminingExportValue.ToString();

                this.RealSummary = realSummary;
                this.DataList = c.ToList();
            }
        }

        private decimal GetTotalFCBPAR(int iD)
        {
            db = new xOssContext();
            var v = (from t2 in db.Acc_ExportRealisation
                     join t3 in db.Commercial_BillOfExchange on t2.Shipment_BillOfExchangeFK equals t3.ID
                     join t4 in db.Commercial_MasterLC on t3.Commercial_MasterLCFK equals t4.ID
                     join t5 in db.Commercial_UDSlave on t4.ID equals t5.Commercial_MasterLCFK
                     where t5.Active == true
                     && t2.Active == true
                     && t3.Active == true
                     && t4.Active == true
                     && t5.Active == true
                     && t5.Commercial_UDFK == iD
                     select new
                     {
                         FcbParAmt = t2.FcbParAmt
                     }).ToList();
            if (v.Any())
            {
                return v.Sum(a => a.FcbParAmt);
            }


            return 0;
        }
        private decimal GetTotalRealisedValue(int iD)
        {
            db = new xOssContext();
            var v = (from t2 in db.Acc_ExportRealisation
                     join t3 in db.Commercial_BillOfExchange on t2.Shipment_BillOfExchangeFK equals t3.ID
                     join t4 in db.Commercial_MasterLC on t3.Commercial_MasterLCFK equals t4.ID
                     join t5 in db.Commercial_UDSlave on t4.ID equals t5.Commercial_MasterLCFK
                     where t5.Active == true
                     && t2.Active == true
                     && t3.Active == true
                     && t4.Active == true
                     && t5.Active == true
                     && t5.Commercial_UDFK == iD
                     select new
                     {
                         RealisedAmount = t2.RealisedAmount
                     }).ToList();
            if (v.Any())
            {
                return v.Sum(a => a.RealisedAmount);
            }


            return 0;
        }

        private decimal GetTotalB2b(int iD, int udid)
        {
            db = new xOssContext();
            try
            {
                var v = (from t1 in db.Commercial_B2bLC
                         join t2 in db.Commercial_UDSlave on t1.Commercial_UDFK equals t2.Commercial_UDFK
                         join t3 in db.Commercial_MasterLC on t2.Commercial_MasterLCFK equals t3.ID
                         where t1.Active == true
                               && t2.Active == true
                               && t3.Active == true
                               && t3.Commercial_LienBankFK == iD && t2.Commercial_UDFK == udid
                         select new
                         {
                             b2bAmount = t1.Amount == null ? 0 : t1.Amount
                         }).Sum(a => a.b2bAmount);



                return v;
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        public void SingleDataLoad(int id)
        {
            db = new xOssContext();
            var c = (from t1 in db.Acc_ExportRealisation.AsEnumerable()
                     join t2 in db.Commercial_BillOfExchange on t1.Shipment_BillOfExchangeFK equals t2.ID
                     join t3 in db.Commercial_MasterLC on t2.Commercial_MasterLCFK equals t3.ID
                     join t4 in db.Commercial_UDSlave on t3.ID equals t4.Commercial_MasterLCFK
                     join t5 in db.Commercial_UD on t4.Commercial_UDFK equals t5.ID
                     where t1.Active == true && t1.ID == id
                     select new Acc_ExportRealisationIndexView
                     {
                         Acc_ExportRealisation = t1,
                         Commercial_BillOfExchange = t2,
                         Commercial_MasterLC = t3,
                         Commercial_UDSlave = t4,
                         Commercial_UD = t5
                     }).OrderByDescending(x => x.Acc_ExportRealisation.ID);

            this.SingleData = c.Single();
        }
        public int Add(int userID)
        {
            Acc_ExportRealisation.AddReady(userID);
            return Acc_ExportRealisation.Add();
        }
        public bool Edit(int userID)
        {
            return Acc_ExportRealisation.Edit(userID);
        }
        public bool Delete(int userID)
        {
            Acc_ExportRealisation.Active = false;
            return Acc_ExportRealisation.Edit(userID);
        }
        public bool Finalized(int userID)
        {
            Acc_ExportRealisation.IsFinal = true;
            return Acc_ExportRealisation.Edit(userID);
        }
    }
    public class Acc_ExportRealisationIndexView
    {
        public Acc_ExportRealisation Acc_ExportRealisation { get; set; }
        public Commercial_BillOfExchange Commercial_BillOfExchange { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Commercial_UDSlave Commercial_UDSlave { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
    }
}