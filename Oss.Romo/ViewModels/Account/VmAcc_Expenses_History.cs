﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_Expenses_History
    {
        private xOssContext db;
        public Acc_Expenses_History Acc_Expenses_History { get; set; }
        //public Acc_AcName Acc_AcName { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public IEnumerable<VmAcc_Expenses_History> DataList { get; set; }

        public Acc_AcName ToAcc_Name { get; set; }
        public Acc_AcName FromAcc_Name { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
                     
            var a = (from t1 in db.Acc_Expenses_History
                     join t2 in db.Acc_AcName on t1.To_Acc_NameFK equals t2.ID
                     join t3 in db.Common_Currency on t1.Common_CurrencyFK equals t3.ID
                     join t4 in db.Acc_AcName on t1.From_Acc_NameFK equals t4.ID
                     select new VmAcc_Expenses_History
                     {
                         Acc_Expenses_History = t1,
                         ToAcc_Name = t2,
                         FromAcc_Name = t4,
                         Common_Currency=t3

                     }).Where(x => x.Acc_Expenses_History.Active == true).OrderByDescending(x => x.Acc_Expenses_History.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingleAcc_Expenses_History(int id)
        {
            db = new xOssContext();
            Acc_Expenses_History t = new Acc_Expenses_History();
          
            var v = (from t1 in db.Acc_Expenses_History
                     join t2 in db.Acc_AcName on t1.To_Acc_NameFK equals t2.ID
                     join t3 in db.Common_Currency on t1.Common_CurrencyFK equals t3.ID
                     join t4 in db.Acc_AcName on t1.From_Acc_NameFK equals t4.ID
                     select new VmAcc_Expenses_History
                     {
                         Acc_Expenses_History = t1,
                         ToAcc_Name = t2,
                         FromAcc_Name = t4,
                         Common_Currency=t3
                     }).Where(c => c.Acc_Expenses_History.ID == id).SingleOrDefault();
            this.Acc_Expenses_History = v.Acc_Expenses_History;
            this.ToAcc_Name = v.ToAcc_Name;
            this.FromAcc_Name = v.FromAcc_Name;
            this.Common_Currency = v.Common_Currency;
        }

        public int Add(int userID)
        {
            Acc_Expenses_History.IsIncome = true;
            Acc_Expenses_History.AddReady(userID);
            return Acc_Expenses_History.Add();

        }
        public int AddExp(int userID)
        {
            Acc_Expenses_History.IsIncome = false;
            Acc_Expenses_History.AddReady(userID);
            return Acc_Expenses_History.Add();

        }
        public bool Edit(int userID)
        {
            return Acc_Expenses_History.Edit(userID);
        }
    }
}