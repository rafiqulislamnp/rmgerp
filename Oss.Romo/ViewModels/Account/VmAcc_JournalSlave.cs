﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_JournalSlave
    {
        private xOssContext db;
        public Acc_Type Acc_Type { get; set; }
        public Acc_Chart1 Acc_Chart1 { get; set; }
        public Acc_Chart2 Acc_Chart2 { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
        public Acc_Journal Acc_Journal { get; set;}
        public int? Acc_TypeID { get; set; }
        public int? Acc_Chart1ID { get; set; }
        public int? Acc_Chart2ID { get; set; }
        public int? Acc_AcNameID { get; set; }
        public Acc_JournalSlave Acc_JournalSlave { get; set; }
        public IEnumerable<VmAcc_JournalSlave> DataList { get; set; }
        public decimal DrSummary { get; set; }
        public decimal CrSummary { get; set; }
        public String IsEqual { get; set; }
        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Acc_JournalSlave in db.Acc_JournalSlave
                     join Acc_AcName in db.Acc_AcName
                     on Acc_JournalSlave.Acc_AcNameFK equals Acc_AcName.ID
                     join Acc_Journal in db.Acc_Journal
                     on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID

                     select new VmAcc_JournalSlave
                     {
                         Acc_AcName = Acc_AcName,
                         Acc_JournalSlave = Acc_JournalSlave,
                         Acc_Journal= Acc_Journal
                     }).Where(x => x.Acc_JournalSlave.Active == true).OrderByDescending(x => x.Acc_JournalSlave.ID).AsEnumerable();
            this.DataList = a;

        }
        public void InitialDataLoadForAcc_Journal(int id)
        {
            db = new xOssContext();

            Acc_Type accountType = new Acc_Type();
            var ListOfAcc_Type = accountType.GetAcc_Type();
            var a = (from Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                     join Acc_AcName in db.Acc_AcName.AsEnumerable()
                     on Acc_JournalSlave.Acc_AcNameFK equals Acc_AcName.ID
                     join Acc_Journal in db.Acc_Journal.AsEnumerable()
                     on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                     join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                     on Acc_AcName.Acc_Chart2FK equals Acc_Chart2.ID
                     join Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                     on Acc_Chart2.Acc_Chart1FK equals Acc_Chart1.ID
                     join Acc_Type in ListOfAcc_Type
                     on Acc_Chart1.Acc_TypeFK equals Acc_Type.ID
                     where Acc_Journal.ID == id
                     select new VmAcc_JournalSlave
                     {
                         Acc_Type = Acc_Type,
                         Acc_Chart1 = Acc_Chart1,
                         Acc_Chart2 = Acc_Chart2,
                         Acc_AcName = Acc_AcName,
                         Acc_JournalSlave = Acc_JournalSlave,
                         Acc_Journal = Acc_Journal
                     }).Where(x => x.Acc_JournalSlave.Active == true).OrderByDescending(x => x.Acc_JournalSlave.ID).AsEnumerable();
            this.DataList = a;
            DrSummary = a.Sum(x=>x.Acc_JournalSlave.Debit);
            CrSummary = a.Sum(x => x.Acc_JournalSlave.Credit);
            if (DrSummary == CrSummary)
            {
                IsEqual = "alert-success";
            }
            else
            {
                IsEqual = "alert-danger";
            }
        }
        public void SelectSingleAcc_JournalSlave(int id)
        {
            db = new xOssContext();
            var v = (from Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                     join Acc_AcName in db.Acc_AcName.AsEnumerable()
                     on Acc_JournalSlave.Acc_AcNameFK equals Acc_AcName.ID
                     join Acc_Journal in db.Acc_Journal.AsEnumerable()
                     on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                     join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                     on Acc_AcName.Acc_Chart2FK equals Acc_Chart2.ID
                     join Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                     on Acc_Chart2.Acc_Chart1FK equals Acc_Chart1.ID
                     join Acc_Type in Acc_Type.GetAcc_Type()
                     on Acc_Chart1.Acc_TypeFK equals Acc_Type.ID
                     where Acc_JournalSlave.Active == true
                     && Acc_JournalSlave.ID == id

                     select new VmAcc_JournalSlave
                     {
                         Acc_Type = Acc_Type,
                         Acc_Chart1 = Acc_Chart1,
                         Acc_Chart2 = Acc_Chart2,
                         Acc_AcName = Acc_AcName,
                         Acc_JournalSlave = Acc_JournalSlave,
                         Acc_Journal = Acc_Journal
                     }).SingleOrDefault();
            this.Acc_JournalSlave = v.Acc_JournalSlave;
            this.Acc_TypeID = v.Acc_Type.ID;
            this.Acc_Chart1ID = v.Acc_Chart1.ID;
            this.Acc_Chart2ID = v.Acc_Chart2.ID;
            this.Acc_AcNameID = v.Acc_AcName.ID;
        }
        public void SingleAcc_Journal(int id)
        {
            db = new xOssContext();
            var v = (from Acc_Journal in db.Acc_Journal
                     where Acc_Journal.Active == true
                     select new VmAcc_Journal
                     {
                         Acc_Journal = Acc_Journal
                     }).Where(c => c.Acc_Journal.ID == id).SingleOrDefault();
            this.Acc_Journal = v.Acc_Journal;

        }

        public int Add(int userID)
        {
            Acc_JournalSlave.AddReady(userID);
            return Acc_JournalSlave.Add();

        }
        public bool Edit(int userID)
        {
            return Acc_JournalSlave.Edit(userID);
        }

    }
}