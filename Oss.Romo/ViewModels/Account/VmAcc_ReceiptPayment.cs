﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_ReceiptPayment
    {
        private xOssContext db = new xOssContext();
        public List<VmAcc_BalanceSheetReport> DataListIncome { get; set; }
        public List<VmAcc_BalanceSheetReport> DataListExpenses { get; set; }
        public List<VmAcc_BalanceSheetReport> DataListClosingBalance { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal OpeningBalanceIncome { get; set; }
        public decimal OpeningBalanceExpenses { get; set; }
        public void InitialDataLoad(VmAcc_ReceiptPayment vmAcc_ReceiptPayment)
        {
            try
            {
                Acc_Type t = new Acc_Type();
                IEnumerable<Acc_Type> listOfAcc_Type = from tbl in t.GetAcc_Type()
                                                       select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

                var TotalIncomeFromJournal = vmAcc_ReceiptPayment.TotalReceiptFromJournal(listOfAcc_Type);

                var TotalExpenseFromJournal = vmAcc_ReceiptPayment.TotalPaymentFromJournal(listOfAcc_Type);
                var TotalClosingBalanceFromJournal = vmAcc_ReceiptPayment.TotalClosingBalance(listOfAcc_Type);

                var joinqueryIncome = TotalIncomeFromJournal.VmAcc_BalanceSheetReport
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_BalanceSheetReport
                    {
                        Name = cl.First().Name,
                        Balance = cl.Sum(c => c.Balance)
                    })
                    .OrderBy(x => x.Name).ToList();

                var joinqueryExpense = TotalExpenseFromJournal.VmAcc_BalanceSheetReport
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_BalanceSheetReport
                    {
                        Name = cl.First().Name,
                        Balance = cl.Sum(c => c.Balance)
                    })
                    .OrderBy(x => x.Name).ToList();


                var joinqueryClosingBanlance = TotalClosingBalanceFromJournal.VmAcc_BalanceSheetReport
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_BalanceSheetReport
                    {
                        Name = cl.First().Name,
                        Balance = cl.Sum(c => c.Balance)
                    })
                    .OrderBy(x => x.Name).ToList();



                OpeningBalanceIncome = TotalIncomeFromJournal.OpeingBalance;
                OpeningBalanceExpenses = TotalExpenseFromJournal.OpeingBalance;

                DataListIncome = joinqueryIncome;
                DataListExpenses = joinqueryExpense;
                DataListClosingBalance = joinqueryClosingBanlance;
            }
            catch (Exception ex)
            {
                //ignore
            }
        }


        public VmAcc_BalanceSheetDataReturn TotalReceiptFromJournal(IEnumerable<Acc_Type> listOfAcc_Type)
        {
            //DateTime openingdate = new DateTime(2018, 1, 1);
            var v1Income = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                            join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                            join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                            join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                            join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                            where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                            && (t3.Date >= FromDate && t3.Date <= ToDate) && t1.Debit != 0 &&
                            (t3.Title.Equals("Cash Receipt") || t3.Title.Equals("Bank Deposit"))
                            select new VmAcc_BalanceSheetReport
                            {
                                Name = t2.Name,
                                Balance = t1.Debit
                            }).ToList();

            //var v1valIncome = (from t1 in db.Acc_JournalSlave.AsEnumerable()
            //                   join t2 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t2.ID
            //                   join t3 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t3.ID
            //                   join t4 in db.Acc_Chart2.AsEnumerable() on t3.Acc_Chart2FK equals t4.ID
            //                   join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
            //                   where t2.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
            //                   && t2.Date < FromDate && t5.ID == 30
            //                   select new
            //                   {
            //                       Balance = t1.Debit - t1.Credit
            //                   }).ToList().Sum(x => x.Balance);

            var v1 = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                      join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                      join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                      join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                      join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                      where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                            && (t3.Date < FromDate) && t1.Debit != 0 && t5.ID == 30
                      select new ClosingBalance
                      {
                          Name = t2.Name,
                          Debit = t1.Debit,
                          Credit = 0
                      }).ToList();
            var v2 = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                      join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                      join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                      join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                      join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                      where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                            && (t3.Date < FromDate) && t1.Credit != 0 &&
                            (t3.Title.Equals("Cash Payment") || t3.Title.Equals("Bank Withdraw") || t3.Title.Equals("Fund Transfer"))
                      select new ClosingBalance
                      {
                          Name = t2.Name,
                          Debit = 0,
                          Credit = t1.Credit
                      }).ToList();
            var c = v1.Union(v2);
            var v1ValIncome = c.Sum(x=>x.Debit) - c.Sum(x => x.Credit);

            return new VmAcc_BalanceSheetDataReturn()
            {
                VmAcc_BalanceSheetReport = v1Income,
                OpeingBalance = v1ValIncome
            };
        }
        public VmAcc_BalanceSheetDataReturn TotalPaymentFromJournal(IEnumerable<Acc_Type> listOfAcc_Type)
        {
            DateTime openingdate = new DateTime(2018, 1, 1);
            var v1Expense = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                             join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                             join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                             join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                             join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                             where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                             && (t3.Date >= FromDate && t3.Date <= ToDate) && t1.Debit != 0 &&
                             (t3.Title.Equals("Cash Payment") || t3.Title.Equals("Bank Withdraw"))
                             select new VmAcc_BalanceSheetReport
                             {
                                 Name = t2.Name,
                                 Balance = t1.Debit
                             }).ToList();

            //var v1valExpense = (from t1 in db.Acc_JournalSlave.AsEnumerable()
            //                    join t2 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t2.ID
            //                    join t3 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t3.ID
            //                    join t4 in db.Acc_Chart2.AsEnumerable() on t3.Acc_Chart2FK equals t4.ID
            //                    join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
            //                    where t2.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
            //                    && t2.Date < FromDate && t2.Date >= openingdate &&
            //                          (t2.Title.Equals("Cash Payment") || t2.Title.Equals("Bank Withdraw"))
            //                    select new
            //                    {
            //                        Balance = t1.Debit
            //                    }).ToList().Sum(x => x.Balance);
            var v1valExpense = 0;

            return new VmAcc_BalanceSheetDataReturn()
            {
                VmAcc_BalanceSheetReport = v1Expense,
                OpeingBalance = v1valExpense
            };
        }
        public VmAcc_BalanceSheetDataReturn TotalClosingBalance(IEnumerable<Acc_Type> listOfAcc_Type)
        {
            DateTime openingdate = new DateTime(2018, 1, 1);
            var v1 = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                      join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                      join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                      join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                      join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                      where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                            && (t3.Date <= ToDate) && t1.Debit != 0 && t5.ID == 30
                      select new ClosingBalance
                      {
                          Name = t2.Name,
                          Debit = t1.Debit,
                          Credit = 0
                      }).ToList();
            var v2 = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                      join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                      join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                      join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                      join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                      where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                            && (t3.Date <= ToDate) && t1.Credit != 0 &&
                            (t3.Title.Equals("Cash Payment") || t3.Title.Equals("Bank Withdraw") || t3.Title.Equals("Fund Transfer"))
                      select new ClosingBalance
                      {
                          Name = t2.Name,
                          Debit = 0,
                          Credit = t1.Credit
                      }).ToList();
            var c = v1.Union(v2);
            var d = (from t1 in c
                     group new {t1} by t1.Name into g
                     select new VmAcc_BalanceSheetReport
                     {
                         Name = g.Key,
                         Balance = g.Sum(_ => _.t1.Debit)- g.Sum(_ => _.t1.Credit),
                     }).ToList();

            return new VmAcc_BalanceSheetDataReturn()
            {
                VmAcc_BalanceSheetReport = d,
                OpeingBalance = 0
            };
        }
    }

    public class ClosingBalance
    {
        public string Name { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
    }
}