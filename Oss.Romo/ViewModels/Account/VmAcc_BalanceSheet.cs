﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_BalanceSheet
    {
        private xOssContext db = new xOssContext();
        public List<VmAcc_BalanceSheetReport> DataListIncome { get; set; }
        public List<VmAcc_BalanceSheetReport> DataListExpenses { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal OpeningBalanceIncome { get; set; }
        public decimal OpeningBalanceExpenses { get; set; }
        public List<VmAcc_BalanceSheetList> VmAcc_BalanceSheetListAsset { get; set; }
        public List<VmAcc_BalanceSheetList> VmAcc_BalanceSheetListLib { get; set; }
        public List<VmAcc_BalanceSheetReport> accBalanceSheetReportList { get; set; }

        public void InitialDataLoad(VmAcc_BalanceSheet vmAcc_BalanceSheet)
        {
            try
            {
                Acc_Type t = new Acc_Type();
                IEnumerable<Acc_Type> listOfAcc_Type = from tbl in t.GetAcc_Type()
                                                       select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };


                //var TotalIncomeFromJournal = vmAcc_BalanceSheet.TotalAssetFromJournal(listOfAcc_Type);
                //var TotalExpenseFromJournal = vmAcc_BalanceSheet.TotalLiabilitiesFromJournal(listOfAcc_Type);


                //var joinqueryIncome = TotalIncomeFromJournal.VmAcc_BalanceSheetReport
                //    .GroupBy(l => l.Name).Select(cl => new VmAcc_BalanceSheetReport
                //    {
                //        Name = cl.First().Name,
                //        Balance = cl.Sum(c => c.Balance)
                //    })
                //    .OrderBy(x => x.Name).ToList();

                //var joinqueryExpense = TotalExpenseFromJournal.VmAcc_BalanceSheetReport
                //    .GroupBy(l => l.Name).Select(cl => new VmAcc_BalanceSheetReport
                //    {
                //        Name = cl.First().Name,
                //        Balance = cl.Sum(c => c.Balance)
                //    })
                //    .OrderBy(x => x.Name).ToList();



                //OpeningBalanceIncome = TotalIncomeFromJournal.OpeingBalance;
                //OpeningBalanceExpenses = TotalExpenseFromJournal.OpeingBalance;

                //DataListIncome = joinqueryIncome;
                //DataListExpenses = joinqueryExpense;
                List<VmAcc_BalanceSheetList> vmAcc_BalanceSheetListAsset = new List<VmAcc_BalanceSheetList>();
                List<VmAcc_BalanceSheetList> vmAcc_BalanceSheetListLib = new List<VmAcc_BalanceSheetList>();


                VmAcc_BalanceSheetList accBalanceSheetListAsset;

                var assetdata = db.Acc_Chart1.Where(x => x.Acc_TypeFK == 1).ToList();
                foreach (var asset in assetdata)
                {
                    accBalanceSheetListAsset = new VmAcc_BalanceSheetList();
                    accBalanceSheetListAsset.Name = asset.Name;
                    accBalanceSheetReportList = new List<VmAcc_BalanceSheetReport>();

                    var assetsublist = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                                        join t6 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t6.ID
                                        join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                                        join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                                        join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                                        where t2.Active == true && t4.ID == asset.ID && t6.isFinal && t1.Active == true && t3.Active == true && t4.Active == true
                                        && t6.Date <= ToDate
                                        group new { t1, t3 } by t3.Name into g
                                        select new VmAcc_BalanceSheetReport
                                        {
                                            Name = g.Key,
                                            Balance = g.Sum(_ => _.t1.Debit * _.t1.CurrencyRate) - g.Sum(_ => _.t1.Credit * _.t1.CurrencyRate)
                                        }).ToList();
                    if (assetsublist.Any())
                    {
                        accBalanceSheetListAsset.BalanceSheetHeadWiseList = assetsublist;
                        vmAcc_BalanceSheetListAsset.Add(accBalanceSheetListAsset);
                    }
                }
                VmAcc_BalanceSheetListAsset = vmAcc_BalanceSheetListAsset;

                var libdata = db.Acc_Chart1.Where(x => x.Acc_TypeFK == 2 || x.Acc_TypeFK == 3).ToList();
                foreach (var lib in libdata)
                {
                    accBalanceSheetListAsset = new VmAcc_BalanceSheetList();
                    accBalanceSheetListAsset.Name = lib.Name;
                    accBalanceSheetReportList = new List<VmAcc_BalanceSheetReport>();

                    var libsublist = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                        join t6 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t6.ID
                        join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                        join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                        join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                        where t2.Active == true && t4.ID == lib.ID && t6.isFinal && t1.Active == true && t3.Active == true && t4.Active == true
                              && t6.Date <= ToDate
                        group new { t1, t3 } by t3.Name into g
                        select new VmAcc_BalanceSheetReport
                        {
                            Name = g.Key,
                            Balance = g.Sum(_ => _.t1.Credit * _.t1.CurrencyRate) - g.Sum(_ => _.t1.Debit * _.t1.CurrencyRate)
                        }).ToList();
                    if (libsublist.Any())
                    {
                        accBalanceSheetListAsset.BalanceSheetHeadWiseList = libsublist;
                        vmAcc_BalanceSheetListLib.Add(accBalanceSheetListAsset);
                    }
                }
                VmAcc_BalanceSheetListLib = vmAcc_BalanceSheetListLib;

            }
            catch (Exception ex)
            {
                //ignore
            }
        }

        public VmAcc_BalanceSheetDataReturn TotalAssetFromJournal(IEnumerable<Acc_Type> listOfAcc_Type)
        {

            var v1Income = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                            join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                            join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                            join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                            join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                            where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                            && t5.Acc_TypeFK == 1 && t3.Date <= ToDate
                            select new VmAcc_BalanceSheetReport
                            {
                                Name = t4.Name,
                                Balance = t1.Debit - t1.Credit
                            }).ToList();

            //var v1valIncome = (from t1 in db.Acc_JournalSlave.AsEnumerable()
            //                   join t2 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t2.ID
            //                   join t3 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t3.ID
            //                   join t4 in db.Acc_Chart2.AsEnumerable() on t3.Acc_Chart2FK equals t4.ID
            //                   join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
            //                   where t2.isFinal && t1.Active == true && t2.Active == true && t3.Active == true && t2.Date < FromDate
            //                   && t5.Acc_TypeFK == 1
            //                   select new
            //                   {
            //                       Balance = t1.Debit - t1.Credit
            //                   }).ToList().Sum(x => x.Balance);
            var v1valIncome = 0;

            return new VmAcc_BalanceSheetDataReturn()
            {
                VmAcc_BalanceSheetReport = v1Income,
                OpeingBalance = v1valIncome
            };
        }
        public VmAcc_BalanceSheetDataReturn TotalLiabilitiesFromJournal(IEnumerable<Acc_Type> listOfAcc_Type)
        {

            var v1Expense = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                             join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                             join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                             join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                             join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                             where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                                   && t5.Acc_TypeFK == 2 && t3.Date <= ToDate
                             select new VmAcc_BalanceSheetReport
                             {
                                 Name = t4.Name,
                                 Balance = t1.Credit - t1.Debit
                             }).ToList();

            //var v1valExpense = (from t1 in db.Acc_JournalSlave.AsEnumerable()
            //                    join t2 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t2.ID
            //                    join t3 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t3.ID
            //                    join t4 in db.Acc_Chart2.AsEnumerable() on t3.Acc_Chart2FK equals t4.ID
            //                    join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
            //                    where t2.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
            //                    && t2.Date < FromDate && t5.Acc_TypeFK == 2
            //                    select new
            //                    {
            //                        Balance = t1.Credit - t1.Debit
            //                    }).ToList().Sum(x => x.Balance);
            var v1valExpense = 0;

            return new VmAcc_BalanceSheetDataReturn()
            {
                VmAcc_BalanceSheetReport = v1Expense,
                OpeingBalance = v1valExpense
            };
        }
        public VmAcc_BalanceSheetDataReturn TotalAssetFromAcc_TransactionAndExpHistory(IEnumerable<Acc_Type> listOfAcc_Type)
        {

            //----------------------------------------Acc_Transaction Income------------------------------///
            //var v3 = (from t1 in db.Acc_Transaction.AsEnumerable()
            //          join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
            //          join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
            //          join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
            //          where t2.Active == true && (t4.Acc_TypeFK == 4) && t1.Active == true && t1.IsFinal == true
            //          && t1.Date >= FromDate && t1.Date <= ToDate
            //          select new VmTransaction
            //          {
            //              Account = t2.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
            //              Code = t2.Code,
            //              Date = t1.Date,
            //              Description = "Voucher No: " + t1.VoucherNo + ", Des: " + t1.Description,
            //              Debit = 0,
            //              Credit = t1.Amount
            //          }).ToList();

            //var v3val = (from t1 in db.Acc_Transaction.AsEnumerable()
            //             join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
            //             join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
            //             join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
            //             where t2.Active == true && (t4.Acc_TypeFK == 4) && t1.Active == true && t1.Date < FromDate
            //             select new
            //             {
            //                 Credit = t1.Amount
            //             }).ToList().Sum(x => x.Credit);

            //var v3_1 = (from t1 in db.Acc_Transaction.AsEnumerable()
            //            join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
            //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
            //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
            //            where t2.Active == true && (t4.Acc_TypeFK == 4)
            //            && t1.Active == true && t1.IsFinal == true && t1.Date >= FromDate && t1.Date <= ToDate
            //            select new VmTransaction
            //            {
            //                Account = t2.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
            //                Code = t2.Code,
            //                Date = t1.Date,
            //                Description = "Voucher No: " + t1.VoucherNo + ", Des: " + t1.Description,
            //                Debit = t1.Amount,
            //                Credit = 0
            //            }).ToList();

            //var v3_1val = (from t1 in db.Acc_Transaction.AsEnumerable()
            //               join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
            //               join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
            //               join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
            //               join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
            //               where t2.Active == true && (t5.ID == 4) && t1.Active == true && t1.Date < FromDate
            //               select new
            //               {
            //                   Debit = t1.Amount
            //               }).ToList().Sum(x => x.Debit);

            var v4 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                      join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                      join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                      join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                      where t2.Active == true && (t4.Acc_TypeFK == 1) && t1.Active == true
                      && t1.Date >= FromDate && t1.Date <= ToDate && t1.IsIncome == true
                      select new VmTransaction
                      {
                          Account = t3.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                          Code = t2.Code,
                          Date = t1.Date,
                          Description = "Ref No: " + t1.Referance + ", Des: " + t1.Comment + ", Note: " + t1.Note,
                          Debit = 0,
                          Credit = t1.Amount
                      }).ToList();

            var v4val = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                         join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                         join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                         join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                         where t2.Active == true && (t4.Acc_TypeFK == 1) && t1.Active == true && t1.IsIncome == true
                         && t1.Date < FromDate
                         select new
                         {
                             Credit = t1.Amount
                         }).ToList().Sum(x => x.Credit);


            var v4_1 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                        join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                        join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                        join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                        where t2.Active == true && (t4.Acc_TypeFK == 1) && t1.IsIncome == true
                        && t1.Active == true && t1.Date >= FromDate && t1.Date <= ToDate
                        select new VmTransaction
                        {
                            Account = t3.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                            Code = t2.Code,
                            Date = t1.Date,
                            Description = "Ref No: " + t1.Referance + ", Des: " + t1.Comment + ", Note: " + t1.Note,
                            Debit = t1.Amount,
                            Credit = 0
                        }).ToList();

            var v4_1val = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                           join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                           join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                           join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                           join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                           where t2.Active == true && (t5.ID == 1) && t1.Active == true && t1.Date < FromDate
                           && t1.IsIncome == true
                           select new
                           {
                               Debit = t1.Amount
                           }).ToList().Sum(x => x.Debit);



            var joinqueryIncome = v4.Union(v4_1)
                .GroupBy(l => l.Account).Select(cl => new VmAcc_BalanceSheetReport
                {
                    Name = cl.First().Account,
                    Balance = cl.Sum(c => c.Debit) - cl.Sum(c => c.Credit)
                })
                .OrderBy(x => x.Name).ToList();

            return new VmAcc_BalanceSheetDataReturn()
            {
                VmAcc_BalanceSheetReport = joinqueryIncome,
                OpeingBalance = v4val - v4_1val
            };
        }
        public VmAcc_BalanceSheetDataReturn TotalLiabilitiesFromAcc_TransactionAndExpHistory(IEnumerable<Acc_Type> listOfAcc_Type)
        {

            //----------------------------------------Acc_Transaction Income------------------------------///
            //var v3 = (from t1 in db.Acc_Transaction.AsEnumerable()
            //          join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
            //          join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
            //          join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
            //          where t2.Active == true && (t4.Acc_TypeFK == 5) && t1.Active == true && t1.IsFinal == true
            //          && t1.Date >= FromDate && t1.Date <= ToDate
            //          select new VmTransaction
            //          {
            //              Account = t2.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
            //              Code = t2.Code,
            //              Date = t1.Date,
            //              Description = "Voucher No: " + t1.VoucherNo + ", Des: " + t1.Description,
            //              Debit = 0,
            //              Credit = t1.Amount
            //          }).ToList();

            //var v3val = (from t1 in db.Acc_Transaction.AsEnumerable()
            //             join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
            //             join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
            //             join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
            //             where t2.Active == true && (t4.Acc_TypeFK == 5) && t1.Active == true && t1.Date < FromDate
            //             select new
            //             {
            //                 Credit = t1.Amount
            //             }).ToList().Sum(x => x.Credit);

            //var v3_1 = (from t1 in db.Acc_Transaction.AsEnumerable()
            //            join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
            //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
            //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
            //            where t2.Active == true && (t4.Acc_TypeFK == 5)
            //            && t1.Active == true && t1.IsFinal == true && t1.Date >= FromDate && t1.Date <= ToDate
            //            select new VmTransaction
            //            {
            //                Account = t2.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
            //                Code = t2.Code,
            //                Date = t1.Date,
            //                Description = "Voucher No: " + t1.VoucherNo + ", Des: " + t1.Description,
            //                Debit = t1.Amount,
            //                Credit = 0
            //            }).ToList();

            //var v3_1val = (from t1 in db.Acc_Transaction.AsEnumerable()
            //               join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
            //               join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
            //               join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
            //               join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
            //               where t2.Active == true && (t5.ID == 5) && t1.Active == true && t1.Date < FromDate
            //               select new
            //               {
            //                   Debit = t1.Amount
            //               }).ToList().Sum(x => x.Debit);

            var v4 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                      join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                      join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                      join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                      where t2.Active == true && (t4.Acc_TypeFK == 2) && t1.Active == true
                      && t1.Date >= FromDate && t1.Date <= ToDate && t1.IsIncome == false
                      select new VmTransaction
                      {
                          Account = t3.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                          Code = t2.Code,
                          Date = t1.Date,
                          Description = "Ref No: " + t1.Referance + ", Des: " + t1.Comment + ", Note: " + t1.Note,
                          Debit = 0,
                          Credit = t1.Amount
                      }).ToList();

            var v4val = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                         join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                         join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                         join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                         where t2.Active == true && (t4.Acc_TypeFK == 2) && t1.Active == true && t1.IsIncome == false
                         && t1.Date < FromDate
                         select new
                         {
                             Credit = t1.Amount
                         }).ToList().Sum(x => x.Credit);


            var v4_1 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                        join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                        join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                        join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                        where t2.Active == true && (t4.Acc_TypeFK == 2) && t1.IsIncome == false
                        && t1.Active == true && t1.Date >= FromDate && t1.Date <= ToDate
                        select new VmTransaction
                        {
                            Account = t3.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                            Code = t2.Code,
                            Date = t1.Date,
                            Description = "Ref No: " + t1.Referance + ", Des: " + t1.Comment + ", Note: " + t1.Note,
                            Debit = t1.Amount,
                            Credit = 0
                        }).ToList();

            var v4_1val = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                           join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                           join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                           join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                           join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                           where t2.Active == true && (t5.ID == 2) && t1.Active == true && t1.Date < FromDate
                           && t1.IsIncome == false
                           select new
                           {
                               Debit = t1.Amount
                           }).ToList().Sum(x => x.Debit);



            var joinqueryIncome = v4.Union(v4_1)
                .GroupBy(l => l.Account).Select(cl => new VmAcc_BalanceSheetReport
                {
                    Name = cl.First().Account,
                    Balance = cl.Sum(c => c.Credit) - cl.Sum(c => c.Debit)
                })
                .OrderBy(x => x.Name).ToList();

            return new VmAcc_BalanceSheetDataReturn()
            {
                VmAcc_BalanceSheetReport = joinqueryIncome,
                OpeingBalance = v4_1val - v4val
            };
        }
    }

    public class VmAcc_BalanceSheetClass
    {
        [DisplayName("Chart of Account Head No")]
        public List<string> ChartOfAccountType { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
    }
    public class VmAcc_BalanceSheetList
    {
        public string Name { get; set; }
        public List<VmAcc_BalanceSheetReport> BalanceSheetHeadWiseList { get; set; }
    }
    public class VmAcc_BalanceSheetReport
    {
        public string Name { get; set; }
        public Decimal Balance { get; set; }
    }
    public class VmAcc_BalanceSheetDataReturn
    {
        public List<VmAcc_BalanceSheetReport> VmAcc_BalanceSheetReport { get; set; }
        public Decimal OpeingBalance { get; set; }
    }
}