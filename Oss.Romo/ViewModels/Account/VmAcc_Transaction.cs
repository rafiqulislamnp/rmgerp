﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_Transaction
    {
        private xOssContext db;
        public Acc_Transaction Acc_Transaction { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime Date { get; set; }
        public List<VmAcc_TransactionView> DataList { get; set; }
        public VmAcc_TransactionView SingleData { get; set; }


        public void InitialDataLoad()
        {
            db = new xOssContext();
            //Acc_Type t = new Acc_Type();
            //var listOfAcc_Type = from tbl in t.GetAcc_Type()
            //                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var c = (from Acc_Transaction in db.Acc_Transaction.AsEnumerable()
                     join Acc_AcNameDr in db.Acc_AcName on Acc_Transaction.To_Acc_NameFK equals Acc_AcNameDr.ID
                     join Acc_AcNameCr in db.Acc_AcName on Acc_Transaction.From_Acc_NameFK equals Acc_AcNameCr.ID
                     join Acc_CostPool in db.Acc_CostPool on Acc_Transaction.Acc_CostPoolFK equals Acc_CostPool.ID into acccostpool
                     from p in acccostpool.DefaultIfEmpty()
                     select new VmAcc_TransactionView
                     {
                         Acc_Transaction = Acc_Transaction,
                         Acc_AcNameDr = Acc_AcNameDr,
                         Acc_AcNameCr = Acc_AcNameCr,
                         Acc_CostPool = p
                     }).Where(x => x.Acc_Transaction.Active == true && x.Acc_AcNameDr.Active == true
                     && x.Acc_AcNameCr.Active == true && x.Acc_Transaction.IsFinal == false)
                .OrderByDescending(x => x.Acc_Transaction.VoucherNo).AsEnumerable();

            this.DataList = c.ToList();
        }
        public void SingleDataLoad(int id)
        {
            db = new xOssContext();
            //Acc_Type t = new Acc_Type();
            //var listOfAcc_Type = from tbl in t.GetAcc_Type()
            //                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var c = (from Acc_Transaction in db.Acc_Transaction.AsEnumerable()
                     join Acc_AcNameDr in db.Acc_AcName on Acc_Transaction.To_Acc_NameFK equals Acc_AcNameDr.ID
                     join Acc_AcNameCr in db.Acc_AcName on Acc_Transaction.From_Acc_NameFK equals Acc_AcNameCr.ID
                    join Acc_CostPool in db.Acc_CostPool on Acc_Transaction.Acc_CostPoolFK equals Acc_CostPool.ID into acccostpool
                    from p in acccostpool.DefaultIfEmpty()
                     select new VmAcc_TransactionView
                     {
                         Acc_Transaction = Acc_Transaction,
                         Acc_AcNameDr = Acc_AcNameDr,
                         Acc_AcNameCr = Acc_AcNameCr,
                         Acc_CostPool = p
                     }).Where(x => x.Acc_Transaction.Active == true && x.Acc_AcNameDr.Active == true
                                   && x.Acc_AcNameCr.Active == true && x.Acc_Transaction.ID == id)
                .OrderByDescending(x => x.Acc_Transaction.VoucherNo).AsEnumerable();

            this.SingleData = c.Single();
        }
        public void FinalizedDataLoad()
        {
            db = new xOssContext();
            //Acc_Type t = new Acc_Type();
            //var listOfAcc_Type = from tbl in t.GetAcc_Type()
            //                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var c = (from Acc_Transaction in db.Acc_Transaction.AsEnumerable()
                     join Acc_AcNameDr in db.Acc_AcName on Acc_Transaction.To_Acc_NameFK equals Acc_AcNameDr.ID
                     join Acc_AcNameCr in db.Acc_AcName on Acc_Transaction.From_Acc_NameFK equals Acc_AcNameCr.ID
                    join Acc_CostPool in db.Acc_CostPool on Acc_Transaction.Acc_CostPoolFK equals Acc_CostPool.ID into acccostpool
                    from p in acccostpool.DefaultIfEmpty()
                     select new VmAcc_TransactionView
                     {
                         Acc_Transaction = Acc_Transaction,
                         Acc_AcNameDr = Acc_AcNameDr,
                         Acc_AcNameCr = Acc_AcNameCr,
                         Acc_CostPool = p
                     }).Where(x => x.Acc_Transaction.Active == true && x.Acc_AcNameDr.Active == true
                     && x.Acc_AcNameCr.Active == true && x.Acc_Transaction.IsFinal == true && x.Acc_Transaction.Date == Date)
                .OrderByDescending(x => x.Acc_Transaction.VoucherNo).AsEnumerable();

            this.DataList = c.ToList();
        }
        public void InitialDataLoadByDate()
        {
            db = new xOssContext();
            //Acc_Type t = new Acc_Type();
            //var listOfAcc_Type = from tbl in t.GetAcc_Type()
            //                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

            var c = (from Acc_Transaction in db.Acc_Transaction.AsEnumerable()
                     join Acc_AcNameDr in db.Acc_AcName on Acc_Transaction.To_Acc_NameFK equals Acc_AcNameDr.ID
                     join Acc_AcNameCr in db.Acc_AcName on Acc_Transaction.From_Acc_NameFK equals Acc_AcNameCr.ID
                    join Acc_CostPool in db.Acc_CostPool on Acc_Transaction.Acc_CostPoolFK equals Acc_CostPool.ID into acccostpool
                    from p in acccostpool.DefaultIfEmpty()
                     select new VmAcc_TransactionView
                     {
                         Acc_Transaction = Acc_Transaction,
                         Acc_AcNameDr = Acc_AcNameDr,
                         Acc_AcNameCr = Acc_AcNameCr,
                         Acc_CostPool = p
                     }).Where(x => x.Acc_Transaction.Active == true && x.Acc_AcNameDr.Active == true && x.Acc_AcNameCr.Active == true
                    && x.Acc_Transaction.Date == Date)
                .OrderByDescending(x => x.Acc_Transaction.VoucherNo).AsEnumerable();

            this.DataList = c.ToList();

        }
        public void SelectSingleAcc_Transaction(int id)
        {
            db = new xOssContext();
            //Acc_Type t = new Acc_Type();
            //var listOfAcc_Type = from tbl in t.GetAcc_Type()
            //                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
            var c = (from Acc_Transaction in db.Acc_Transaction
                     join Acc_AcNameDr in db.Acc_AcName on Acc_Transaction.To_Acc_NameFK equals Acc_AcNameDr.ID
                     join Acc_AcNameCr in db.Acc_AcName on Acc_Transaction.From_Acc_NameFK equals Acc_AcNameCr.ID
                    join Acc_CostPool in db.Acc_CostPool on Acc_Transaction.Acc_CostPoolFK equals Acc_CostPool.ID into acccostpool
                    from p in acccostpool.DefaultIfEmpty()
                     select new VmAcc_TransactionView
                     {
                         Acc_Transaction = Acc_Transaction,
                         Acc_AcNameDr = Acc_AcNameDr,
                         Acc_AcNameCr = Acc_AcNameCr,
                         Acc_CostPool = p
                     }).Where(x => x.Acc_Transaction.Active == true && x.Acc_AcNameDr.Active == true && x.Acc_AcNameCr.Active == true
                    && x.Acc_Transaction.ID == id)
                .OrderByDescending(x => x.Acc_Transaction.VoucherNo).SingleOrDefault();

            this.Acc_Transaction = c.Acc_Transaction;
        }

        public int Add(int userID)
        {
            Acc_Transaction.AddReady(userID);
            return Acc_Transaction.Add();
        }
        public bool Edit(int userID)
        {
            return Acc_Transaction.Edit(userID);
        }
        public void ChangeFinaliseStatus(int id)
        {
            VmAcc_Transaction o = new VmAcc_Transaction();
            o.SelectSingleAcc_Transaction(id);
            o.Acc_Transaction.IsFinal = true;
            o.Edit(0);
        }
    }

    public class VmAcc_TransactionView
    {
        public Acc_Transaction Acc_Transaction { get; set; }
        public Acc_AcName Acc_AcNameDr { get; set; }
        public Acc_AcName Acc_AcNameCr { get; set; }
        public Acc_CostPool Acc_CostPool { get; set; }
    }
}