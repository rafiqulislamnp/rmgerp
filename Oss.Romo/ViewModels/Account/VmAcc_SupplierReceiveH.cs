﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Reports;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_SupplierReceiveH
    {
        private xOssContext db;
        public Acc_Type Acc_Type { get; set; }
        public Acc_Chart1 Acc_Chart1 { get; set; }
        public Acc_Chart2 Acc_Chart2 { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
        public Common_Country Common_Country { get; set; }
        public Acc_SupplierReceiveH Acc_SupplierReceiveH { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Commercial_Bank Lein_Bank { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Commercial_LCOregin Commercial_LCOregin { get; set; }
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        public Commercial_BtBLCType Commercial_BtBLCType { get; set; }
        public Acc_SupplierPaymentType Acc_SupplierPaymentType { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
        public Commercial_BTBItem Commercial_BTBItem { get; set; }
        public Mkt_POSlave_Receiving Mkt_POSlave_Receiving { get; set; }
        public IEnumerable<VmAcc_SupplierReceiveH> DataList { get; set; }
        public List<ReportDocType> GoodsReportDoc { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        [DisplayName("PO Wise Due")]
        public decimal? Due { get; set; }
        [DisplayName("Payable")]
        public decimal? Payable { get; set; }
        public decimal Recived { get; set; }
        public decimal Returned { get; set; }
        public decimal Total { get; set; }
        public decimal PaidBtbValue { get; set; }
        public decimal? Balance { get; set; }

        public void InitialDataLoadforSupplier()
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier
                         //join t1 in db.Mkt_PO on Common_Supplier.ID equals t1.Common_SupplierFK
                     where Common_Supplier.Active == true && Common_Supplier.ID != 1
                     select new VmAcc_SupplierReceiveH
                     {
                         Common_Supplier = Common_Supplier
                     }).ToList();
            int sdfds = a.Count();
            List<VmAcc_SupplierReceiveH> tempDl = new List<VmAcc_SupplierReceiveH>();

            foreach (VmAcc_SupplierReceiveH vmAcc_SupplierReceiveH in a)
            {
                decimal? tempD = LCallocationHistory(vmAcc_SupplierReceiveH.Common_Supplier.ID);
                decimal? B2Bpayment = GetB2BLCWisePaymentSummary(vmAcc_SupplierReceiveH.Common_Supplier.ID);
                decimal? ChequePayment = ChequeWisePaymentSummary(vmAcc_SupplierReceiveH.Common_Supplier.ID);
                decimal? ChallanValue = GetPoRecevingChallanValue(vmAcc_SupplierReceiveH.Common_Supplier.ID);
                decimal? BillValue = GetBillValue(vmAcc_SupplierReceiveH.Common_Supplier.ID);
                if (tempD == null)
                {
                    tempD = 0;
                }
                if (B2Bpayment == null)
                {
                    B2Bpayment = 0;
                }
                if (ChequePayment == null)
                {
                    ChequePayment = 0;
                }
                if (ChallanValue == null)
                {
                    ChallanValue = 0;
                }
                if (BillValue == null)
                {
                    BillValue = 0;
                }
                VmAcc_SupplierReceiveH vmAcc_SupplierReceiveHTemp = new VmAcc_SupplierReceiveH();
                vmAcc_SupplierReceiveHTemp.Common_Supplier = vmAcc_SupplierReceiveH.Common_Supplier;
                vmAcc_SupplierReceiveHTemp.Due = POallocationValue(vmAcc_SupplierReceiveH.Common_Supplier.ID) - (tempD + ChequePayment);

                vmAcc_SupplierReceiveHTemp.Payable = (ChallanValue + BillValue) - (B2Bpayment + ChequePayment);
                tempDl.Add(vmAcc_SupplierReceiveHTemp);
            }
            this.DataList = tempDl;


        }

        public SumofPayableDueSupplier SumOfDueAndPayableForSupplier()
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier
                         //join t1 in db.Mkt_PO on Common_Supplier.ID equals t1.Common_SupplierFK
                     where Common_Supplier.Active == true// && t1.Active==true
                     select new VmAcc_SupplierReceiveH
                     {
                         Common_Supplier = Common_Supplier

                     }).Distinct();

            decimal due = 0;
            decimal payable = 0;
            foreach (VmAcc_SupplierReceiveH vmAcc_SupplierReceiveH in a)
            {
                decimal? tempD = LCallocationHistory(vmAcc_SupplierReceiveH.Common_Supplier.ID);
                decimal? B2Bpayment = GetB2BLCWisePaymentSummary(vmAcc_SupplierReceiveH.Common_Supplier.ID);
                decimal? ChequePayment = ChequeWisePaymentSummary(vmAcc_SupplierReceiveH.Common_Supplier.ID);
                decimal? ChallanValue = GetPoRecevingChallanValue(vmAcc_SupplierReceiveH.Common_Supplier.ID);
                if (tempD == null)
                {
                    tempD = 0;
                }
                if (B2Bpayment == null)
                {
                    B2Bpayment = 0;
                }
                if (ChequePayment == null)
                {
                    ChequePayment = 0;
                }
                if (ChallanValue == null)
                {
                    ChallanValue = 0;
                }
                if ((POallocationValue(vmAcc_SupplierReceiveH.Common_Supplier.ID) - (tempD + ChequePayment)) == null)
                    due += 0;
                else
                    due += (decimal)(POallocationValue(vmAcc_SupplierReceiveH.Common_Supplier.ID) - (tempD + ChequePayment));

                if (ChallanValue - (B2Bpayment + ChequePayment) == null)
                    payable += 0;
                else
                    payable += (decimal)(ChallanValue - (B2Bpayment + ChequePayment));
            }
            SumofPayableDueSupplier sumofPayableDueSupplier = new SumofPayableDueSupplier();
            sumofPayableDueSupplier.Due = (decimal)due;
            sumofPayableDueSupplier.Payable = (decimal)payable;
            return sumofPayableDueSupplier;
        }

        internal VmAcc_SupplierReceiveH SelectSingleSupplier(int id)
        {
            db = new xOssContext();
            var v = (from common_Supplier in db.Common_Supplier
                     join t2 in db.Common_Country
                     on common_Supplier.Common_CountryFk equals t2.ID
                     where common_Supplier.ID == id
                     select new VmAcc_SupplierReceiveH
                     {
                         Common_Supplier = common_Supplier,
                         Common_Country = t2
                     }).FirstOrDefault();
            return v;
        }

        internal void OpenedBTBBySupplier(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join Common_Supplier in db.Common_Supplier
                     on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID

                     join Commercial_LCOregin in db.Commercial_LCOregin
                     on Commercial_B2bLC.Commercial_LCOreginFK equals Commercial_LCOregin.ID
                     join Commercial_BtBLCType in db.Commercial_BtBLCType
                     on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BtBLCType.ID
                     join Commercial_UD in db.Commercial_UD
                      on Commercial_B2bLC.Commercial_UDFK equals Commercial_UD.ID
                     join Commercial_BTBItem in db.Commercial_BTBItem
                     on Commercial_B2bLC.Commercial_BTBItemFK equals Commercial_BTBItem.ID
                     where Commercial_B2bLC.Common_SupplierFK == id && Commercial_B2bLC.IsApproved
                     select new VmAcc_SupplierReceiveH
                     {
                         Common_Supplier = Common_Supplier,
                         Commercial_LCOregin = Commercial_LCOregin,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_BtBLCType = Commercial_BtBLCType,
                         Commercial_UD = Commercial_UD,
                         Commercial_BTBItem = Commercial_BTBItem,
                         // PaidBtbValue = 0
                     }).Where(x => x.Commercial_B2bLC.Active == true).OrderByDescending(x => x.Commercial_B2bLC.ID).OrderByDescending(x => x.Commercial_UD.UdNo).AsEnumerable();

            List<VmAcc_SupplierReceiveH> list = new List<VmAcc_SupplierReceiveH>();
            foreach (VmAcc_SupplierReceiveH x in a)
            {
                x.PaidBtbValue = GetPaidValue(x.Commercial_B2bLC.ID);
                list.Add(x);
            }
            this.DataList = list;

        }

        private decimal GetPaidValue(int id)
        {
            db = new xOssContext();
            decimal paidBtbValue = 0;
            var value = (from commercialB2BPaymentInformation in db.Commercial_B2BPaymentInformation

                         where commercialB2BPaymentInformation.Commercial_B2bLCFK == id && commercialB2BPaymentInformation.Active == true
                         select commercialB2BPaymentInformation.B2bLCPayment);

            if (value.Count() != 0)
            {

                paidBtbValue = value.Sum();
            }
            return paidBtbValue;
        }



        //----------------------Supplier allocatoin History------------------//
        public decimal LCallocationHistory(int? id)
        {
            db = new xOssContext();

            var b = (from t1 in db.Commercial_B2bLC
                     where t1.Common_SupplierFK == id && t1.Active == true && t1.IsApproved
                     select t1.Amount).DefaultIfEmpty().Sum();
            return b;
        }
        public decimal? POallocationValue(int? id)
        {
            db = new xOssContext();

            var b = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_POSlave on t1.ID equals t2.Mkt_POFK
                     where t1.Active == true && t2.Active == true && t1.Common_SupplierFK == id
                     select (t2.Price * t2.TotalRequired)).DefaultIfEmpty().Sum();
            return b;
        }

        //----------------------Supplier Payment History------------------//
        public decimal? GetB2BLCWisePaymentSummary(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_B2BPaymentInformation
                     join t2 in db.Commercial_B2bLC on t1.Commercial_B2bLCFK equals t2.ID
                     join t3 in db.Common_Supplier on t2.Common_SupplierFK equals t3.ID
                     where t2.Common_SupplierFK == id && t2.Active == true && t1.Active == true
                     && t3.Active == true && t1.IsApproved && t2.IsApproved
                     select t1.B2bLCPayment).DefaultIfEmpty().Sum();
            return v;
        }
        public decimal? ChequeWisePaymentSummary(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Acc_SupplierPH
                     where t1.Common_SupplierFK == id && t1.Active == true && t1.IsApproved
                     select t1.Amount).DefaultIfEmpty().Sum();

            return a;
        }
        public decimal? GetPoRecevingChallanValue(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                     join t3 in db.Mkt_POSlave_Receiving on t1.ID equals t3.Mkt_POSlaveFK
                     where t2.Common_SupplierFK == id && t3.Active == true && t1.Active == true && t2.Active == true
                     select (t3.Quantity * t1.Price)).DefaultIfEmpty().Sum();
            return a;
        }
        public decimal? GetBillValue(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Acc_SupplierBillSlave
                     join t2 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t2.ID
                     where t2.Common_SupplierFk == id && t1.Active == true && t1.IsApproved && t2.Active == true
                     select new
                     {
                         Amount = (decimal)t1.Amount
                     }).ToList();
            var amt = a.Sum(x => x.Amount);
            return amt;
        }

        public void InitialDataLoadSupplier(int id)
        {
            db = new xOssContext();
            var c = (from Acc_SupplierReceiveH in db.Acc_SupplierReceiveH
                     join Acc_SupplierPaymentType in db.Acc_AcName
                     on Acc_SupplierReceiveH.Acc_AcNameFK equals Acc_SupplierPaymentType.ID
                     join Lein_Bank in db.Commercial_Bank
                    on Acc_SupplierReceiveH.Commercial_BankFK equals Lein_Bank.ID
                     where Acc_SupplierReceiveH.Common_SupplierFK == id
                     && Acc_SupplierReceiveH.Active == true
                     select new VmAcc_SupplierReceiveH
                     {
                         Acc_SupplierReceiveH = Acc_SupplierReceiveH,
                         Acc_AcName = Acc_SupplierPaymentType,
                         Lein_Bank = Lein_Bank
                     }).OrderByDescending(x => x.Acc_SupplierReceiveH.ID).AsEnumerable();
            this.DataList = c;

        }


        public void SelectSingleForSupplierReceiveH(int id)
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier

                     where Common_Supplier.Active == true
                     select new
                     {
                         Common_Supplier = Common_Supplier

                     }).Where(c => c.Common_Supplier.ID == id).SingleOrDefault();
            this.Common_Supplier = a.Common_Supplier;
            this.Acc_SupplierReceiveH = new Acc_SupplierReceiveH();
            this.Acc_SupplierReceiveH.Common_SupplierFK = this.Common_Supplier.ID;
            this.Acc_SupplierReceiveH.Date = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"));
            InitialDataLoadSupplier(this.Common_Supplier.ID);
        }

        internal decimal? GetOpenBTBValueForThisSupplier(int iD)
        {
            db = new xOssContext();
            var totalBTBValue = (from t1 in db.Commercial_B2bLC
                                 join t2 in db.Commercial_UD
                                 on t1.Commercial_UDFK equals t2.ID
                                 where t1.Active == true && t2.Active == true && t1.Common_SupplierFK == iD
                                 select t1.Amount).ToList();
            decimal tTLbTBValue = 0;
            if (totalBTBValue.Count() != 0)
            {
                foreach (var x in totalBTBValue)
                {
                    tTLbTBValue += x;
                }
            }

            return tTLbTBValue;
        }

        public int Add(int userID)
        {
            Acc_SupplierReceiveH.AddReady(userID);
            return Acc_SupplierReceiveH.Add();

        }
        public bool Edit(int userID)
        {
            return Acc_SupplierReceiveH.Edit(userID);
        }
        public bool Delete(int userID)
        {
            return Acc_SupplierReceiveH.Delete(userID);
        }
    }
}