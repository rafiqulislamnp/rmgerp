﻿using Oss.Romo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
   
    public class VmTransaction
    {
       
        public int ID { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime Date { get; set; }
        public DateTime FirstCreateDate { get; set; }
        [DisplayName("A/C Head")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Account { get; set; }

        [DisplayName("A/C Code")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Code { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DefaultValue(0)]
        public decimal Debit { get; set; }
        [DefaultValue(0)]
        public decimal Credit { get; set; }
        [DefaultValue(0)]
        public decimal Balance { get; set; }
        public decimal TotalDebit { get; set; }
        public decimal TotalCredit { get; set; }

        public string StringUrl { get; set; }

        public bool DrIncrease { get; set; }
        //----------------------IncomeStatement--------------------//
        public decimal? CostOfGoodsSold { get; set; }
        public decimal? GrossProfit { get; set; }
        public decimal? NetProfit { get; set; }
        public decimal? Sales { get; set; }
        public decimal? Purchased { get; set; }
        public decimal? OperatingExp { get; set; }

        public string Name { get; set; }
        [DefaultValue(0)]
        public decimal Name2 { get; set; }
        [DefaultValue(0)]
        public decimal Name3 { get; set; }
        [DefaultValue(0)]
        public decimal Name4 { get; set; }
        [DefaultValue(0)]
        public decimal Name5 { get; set; }
        [DefaultValue(0)]
        public decimal Name6 { get; set; }

        public decimal CurrencyRate { get; set; }

        public bool IsPoDeleted { get; set; }
        public string IpoNumber { get; set; }
    }
}