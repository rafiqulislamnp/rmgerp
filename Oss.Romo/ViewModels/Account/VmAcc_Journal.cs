﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_Journal
    {
        private xOssContext db;
        public Acc_Journal Acc_Journal { get; set; }

        public Acc_Type Acc_Type { get; set; }
        public Acc_Chart1 Acc_Chart1 { get; set; }
        public Acc_Chart2 Acc_Chart2 { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
       
        public Acc_JournalSlave Acc_JournalSlave { get; set; }
        public DateTime Date { get; set; }
        public IEnumerable<VmAcc_Journal> DataList { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();
            var a = (from Acc_Journal in db.Acc_Journal
                     where Acc_Journal.Active == true && Acc_Journal.Date == Date
                     select new VmAcc_Journal
                     {
                         Acc_Journal = Acc_Journal
                     }).OrderByDescending(x => x.Acc_Journal.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingleAcc_Type(int id)
        {
            db = new xOssContext();
            var v = (from Acc_Journal in db.Acc_Journal
                     where Acc_Journal.Active == true
                     select new VmAcc_Journal
                     {
                         Acc_Journal = Acc_Journal
                     }).Where(c => c.Acc_Journal.ID == id).SingleOrDefault();
            this.Acc_Journal = v.Acc_Journal;
        }

        public int Add(int userID)
        {
            Acc_Journal.AddReady(userID);
            return Acc_Journal.Add();
        }
        public bool Edit(int userID)
        {
            return Acc_Journal.Edit(userID);
        }


        public void SelectSingle(int iD)
        {
            db = new xOssContext();
            var v = (from rc in db.Acc_Journal
                     select new VmAcc_Journal
                     {
                         Acc_Journal = rc
                     }).Where(c => c.Acc_Journal.ID == iD).SingleOrDefault();
            this.Acc_Journal = v.Acc_Journal;

        }

        private void AdjustTheHeadBalance(int JournalId)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     where t1.Acc_JournalFK == JournalId && t1.Active == true
                     select new VmAcc_Journal
                     {
                         Acc_JournalSlave = t1
                     }).ToList();
            Acc_Type accountType = new Acc_Type();
            var ListOfAccType = accountType.GetAcc_Type();
            foreach (var i in v)
            {
                var headType = (from t1 in db.Acc_AcName.AsEnumerable()
                                join t2 in db.Acc_Chart2.AsEnumerable() on t1.Acc_Chart2FK equals t2.ID
                                join t3 in db.Acc_Chart1.AsEnumerable() on t2.Acc_Chart1FK equals t3.ID
                                join t4 in ListOfAccType on t3.Acc_TypeFK equals t4.ID
                                where t1.ID == i.Acc_JournalSlave.Acc_AcNameFK
                                select new VmAcc_AcName
                                {
                                    Acc_Type = t4,                                    
                                    Acc_AcName = t1,
                                }).SingleOrDefault();

                if (headType.Acc_Type.DrIncrease)
                {

                    headType.Acc_AcName.CurrentBalance += i.Acc_JournalSlave.Debit - i.Acc_JournalSlave.Credit;
                     
                }
                else
                {
                    headType.Acc_AcName.CurrentBalance += i.Acc_JournalSlave.Credit - i.Acc_JournalSlave.Debit;
                }

               bool fff = headType.Edit(0);
                int sdas = 0;
            }



        }

       

        public void ChangeFinaliseStatus(int id)
        {
            VmAcc_Journal o = new VmAcc_Journal();

            AdjustTheHeadBalance(id);

            o.SelectSingle(id);
            o.Acc_Journal.isFinal = true;
            o.Edit(0);
        }

        
    }




}