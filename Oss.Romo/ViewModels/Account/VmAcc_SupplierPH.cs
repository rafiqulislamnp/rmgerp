﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_SupplierPH
    {

        private xOssContext db;
        public Acc_Type Acc_Type { get; set; }
        public Acc_Chart1 Acc_Chart1 { get; set; }
        public Acc_Chart2 Acc_Chart2 { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
        public Common_Country Common_Country { get; set; }
        public Acc_SupplierPH Acc_SupplierPH { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Commercial_Bank Lein_Bank { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Mkt_POSlave Mkt_POSlave { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Commercial_LCOregin Commercial_LCOregin { get; set; }
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        public Commercial_BtBLCType Commercial_BtBLCType { get; set; }
        public Acc_SupplierPaymentType Acc_SupplierPaymentType { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
        public Commercial_BTBItem Commercial_BTBItem { get; set; }
        public Mkt_POSlave_Receiving Mkt_POSlave_Receiving { get; set; }
        public IEnumerable<VmAcc_SupplierPH> DataList { get; set; }
        public List<ReportDocType> GoodsReportDoc { get; set; }
        public DateTime Date { get; set; }
        public Raw_Item Raw_Item { get; set; }
        public Common_Unit Common_Unit { get; set; }
        [DisplayName("PO Wise Due")]
        public decimal? Due { get; set; }
        [DisplayName("Payable")]
        public decimal? Payable { get; set; }
        public decimal AdjustmentValue { get;  set; }
        public decimal Recived { get; set; }
        public decimal Returned { get; set; }
        public decimal Total { get; set; }
        public decimal PaidBtbValue { get; set; }
        public decimal? Balance { get; set; }

        public void InitialDataLoadforSupplier()
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier
                         //join t1 in db.Mkt_PO on Common_Supplier.ID equals t1.Common_SupplierFK
                     where Common_Supplier.Active == true && Common_Supplier.ID != 1
                     select new VmAcc_SupplierPH
                     {
                         Common_Supplier = Common_Supplier
                     }).ToList();
           
            List<VmAcc_SupplierPH> tempDl = new List<VmAcc_SupplierPH>();

            foreach (VmAcc_SupplierPH vmAcc_SupplierPH in a)
            {
                //decimal? tempD = LCallocationHistory(vmAcc_SupplierPH.Common_Supplier.ID);
                //decimal? B2Bpayment = GetB2BLCWisePaymentSummary(vmAcc_SupplierPH.Common_Supplier.ID);
                //decimal? ChequePayment = ChequeWisePaymentSummary(vmAcc_SupplierPH.Common_Supplier.ID);
                //decimal? ChequeReceive = ChequeWiseReceiveSummary(vmAcc_SupplierPH.Common_Supplier.ID);
                //decimal? ChallanValue = GetPoRecevingChallanValue(vmAcc_SupplierPH.Common_Supplier.ID);
                //decimal? BillValue = GetBillValue(vmAcc_SupplierPH.Common_Supplier.ID);
                //if (tempD == null)
                //{
                //    tempD = 0;
                //}
                //if (B2Bpayment == null)
                //{
                //    B2Bpayment = 0;
                //}
                //if (ChequePayment == null)
                //{
                //    ChequePayment = 0;
                //}
                //if (ChequeReceive == null)
                //{
                //    ChequeReceive = 0;
                //}
                //if (ChallanValue == null)
                //{
                //    ChallanValue = 0;
                //}
                //if (BillValue == null)
                //{
                //    BillValue = 0;
                //}
                VmAcc_SupplierPH vmAcc_SupplierPHTemp = new VmAcc_SupplierPH();
                vmAcc_SupplierPHTemp.Common_Supplier = vmAcc_SupplierPH.Common_Supplier;
                //vmAcc_SupplierPHTemp.Due = POallocationValue(vmAcc_SupplierPH.Common_Supplier.ID) - (tempD + ChequePayment - ChequeReceive);
                vmAcc_SupplierPHTemp.Due = db.Database.SqlQuery<decimal>($"Select dbo.Fun_TOTALLCOpeningBalanceBySupplier({vmAcc_SupplierPH.Common_Supplier.ID})").FirstOrDefault();
                //vmAcc_SupplierPHTemp.Payable = (ChallanValue + BillValue) - (B2Bpayment + ChequePayment - ChequeReceive);
                vmAcc_SupplierPHTemp.Payable = db.Database.SqlQuery<decimal>($"Select dbo.Fun_TOTALLCPaymentBalanceBySupplier({vmAcc_SupplierPH.Common_Supplier.ID})").FirstOrDefault();
                tempDl.Add(vmAcc_SupplierPHTemp);
            }
            this.DataList = tempDl;


        }

        public void SupplierAdjustmentPreview()
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier
                     where Common_Supplier.Active == true && Common_Supplier.ID != 1
                     select new VmAcc_SupplierPH
                     {
                         Common_Supplier = Common_Supplier
                     }).ToList();

            List<VmAcc_SupplierPH> tempDl = new List<VmAcc_SupplierPH>();

            foreach (VmAcc_SupplierPH vmAcc_SupplierPH in a)
            {
               
                VmAcc_SupplierPH vmAcc_SupplierPHTemp = new VmAcc_SupplierPH();
                vmAcc_SupplierPHTemp.Common_Supplier = vmAcc_SupplierPH.Common_Supplier;
                vmAcc_SupplierPHTemp.Payable = db.Database.SqlQuery<decimal>($"Select [dbo].[Fun_TOTALPOWiseLiabilitiesBalanceBySupplier]({vmAcc_SupplierPH.Common_Supplier.ID})").FirstOrDefault();
                vmAcc_SupplierPHTemp.AdjustmentValue = SupplierAddjustmentValue(vmAcc_SupplierPH.Common_Supplier.ID);
                vmAcc_SupplierPHTemp.Balance = (vmAcc_SupplierPHTemp.Payable - vmAcc_SupplierPHTemp.AdjustmentValue);
                tempDl.Add(vmAcc_SupplierPHTemp);
            }
            this.DataList = tempDl;


        }

        public void SupplierPaymentAdjustmentPreview()
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier
                     where Common_Supplier.Active == true && Common_Supplier.ID != 1
                     select new VmAcc_SupplierPH
                     {
                         Common_Supplier = Common_Supplier
                     }).ToList();

            List<VmAcc_SupplierPH> tempDl = new List<VmAcc_SupplierPH>();

            foreach (VmAcc_SupplierPH vmAcc_SupplierPH in a)
            {

                VmAcc_SupplierPH vmAcc_SupplierPHTemp = new VmAcc_SupplierPH();
                vmAcc_SupplierPHTemp.Common_Supplier = vmAcc_SupplierPH.Common_Supplier;
                vmAcc_SupplierPHTemp.Payable = db.Database.SqlQuery<decimal>($"Select [dbo].[Fun_TOTALLCPaymentBalanceBySupplier]({vmAcc_SupplierPH.Common_Supplier.ID})").FirstOrDefault();
                vmAcc_SupplierPHTemp.AdjustmentValue = SupplierAddjustmentValue(vmAcc_SupplierPH.Common_Supplier.ID);
                vmAcc_SupplierPHTemp.Balance = (vmAcc_SupplierPHTemp.Payable - vmAcc_SupplierPHTemp.AdjustmentValue);
                tempDl.Add(vmAcc_SupplierPHTemp);
            }
            this.DataList = tempDl;


        }

        public decimal SupplierAddjustmentValue(int id)
        {
            var v = db.Acc_SupplierAdjustment.Where(x => x.Active == true && x.Common_SupplierFk == id).Select(x => x.AdjustmentAmmount).DefaultIfEmpty(0).Sum();
            return v;
        }

        public void InitialDataLoadforSupplierSearch(DateTime dateTime)
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier                      
                     where Common_Supplier.Active == true && Common_Supplier.ID != 1
                     select new VmAcc_SupplierPH
                     {
                         Common_Supplier = Common_Supplier
                     }).ToList();

            List<VmAcc_SupplierPH> tempDl = new List<VmAcc_SupplierPH>();

            foreach (VmAcc_SupplierPH vmAcc_SupplierPH in a)
            {               
                VmAcc_SupplierPH vmAcc_SupplierPHTemp = new VmAcc_SupplierPH();
                vmAcc_SupplierPHTemp.Common_Supplier = vmAcc_SupplierPH.Common_Supplier;
                vmAcc_SupplierPHTemp.Due = db.Database.SqlQuery<decimal>($"Select dbo.Fun_TOTALLCOpeningBalanceBySupplierSearch(" + vmAcc_SupplierPH.Common_Supplier.ID + ",'" + dateTime + "')").FirstOrDefault();
                vmAcc_SupplierPHTemp.Payable = db.Database.SqlQuery<decimal>($"Select dbo.Fun_TOTALLCPaymentBalanceBySupplierSearch(" + vmAcc_SupplierPH.Common_Supplier.ID + ",'" + dateTime + "')").FirstOrDefault();

               
                
               tempDl.Add(vmAcc_SupplierPHTemp);
            }
            this.DataList = tempDl;


        }

        public SumofPayableDueSupplier SumOfDueAndPayableForSupplier()
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier
                         //join t1 in db.Mkt_PO on Common_Supplier.ID equals t1.Common_SupplierFK
                     where Common_Supplier.Active == true// && t1.Active==true
                     select new VmAcc_SupplierPH
                     {
                         Common_Supplier = Common_Supplier

                     }).Distinct();

            decimal due = 0;
            decimal payable = 0;
            foreach (VmAcc_SupplierPH vmAcc_SupplierPH in a)
            {
                decimal? tempD = LCallocationHistory(vmAcc_SupplierPH.Common_Supplier.ID);
                decimal? B2Bpayment = GetB2BLCWisePaymentSummary(vmAcc_SupplierPH.Common_Supplier.ID);
                decimal? ChequePayment = ChequeWisePaymentSummary(vmAcc_SupplierPH.Common_Supplier.ID);
                decimal? ChequeReceive = ChequeWiseReceiveSummary(vmAcc_SupplierPH.Common_Supplier.ID);
                decimal? ChallanValue = GetPoRecevingChallanValue(vmAcc_SupplierPH.Common_Supplier.ID);
                if (tempD == null)
                {
                    tempD = 0;
                }
                if (B2Bpayment == null)
                {
                    B2Bpayment = 0;
                }
                if (ChequePayment == null)
                {
                    ChequePayment = 0;
                }
                if (ChequeReceive == null)
                {
                    ChequeReceive = 0;
                }
                if (ChallanValue == null)
                {
                    ChallanValue = 0;
                }
                if ((POallocationValue(vmAcc_SupplierPH.Common_Supplier.ID) - (tempD + ChequePayment - ChequeReceive)) == null)
                    due += 0;
                else
                    due += (decimal)(POallocationValue(vmAcc_SupplierPH.Common_Supplier.ID) - (tempD + ChequePayment - ChequeReceive));

                if (ChallanValue - (B2Bpayment + ChequePayment - ChequeReceive) == null)
                    payable += 0;
                else
                    payable += (decimal)(ChallanValue - (B2Bpayment + ChequePayment - ChequeReceive));
            }
            SumofPayableDueSupplier sumofPayableDueSupplier = new SumofPayableDueSupplier();
            sumofPayableDueSupplier.Due = (decimal)due;
            sumofPayableDueSupplier.Payable = (decimal)payable;
            return sumofPayableDueSupplier;
        }

        internal VmAcc_SupplierPH SelectSingleSupplier(int id)
        {
            db = new xOssContext();
            var v = (from common_Supplier in db.Common_Supplier
                     join t2 in db.Common_Country
                     on common_Supplier.Common_CountryFk equals t2.ID
                     where common_Supplier.ID == id
                     select new VmAcc_SupplierPH
                     {
                         Common_Supplier = common_Supplier,
                         Common_Country = t2
                     }).FirstOrDefault();
            return v;
        }

        internal void OpenedBTBBySupplier(int id)
        {
            db = new xOssContext();
            var a = (from Commercial_B2bLC in db.Commercial_B2bLC
                     join Common_Supplier in db.Common_Supplier
                     on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID

                     join Commercial_LCOregin in db.Commercial_LCOregin
                     on Commercial_B2bLC.Commercial_LCOreginFK equals Commercial_LCOregin.ID
                     join Commercial_BtBLCType in db.Commercial_BtBLCType
                     on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BtBLCType.ID
                     join Commercial_UD in db.Commercial_UD
                      on Commercial_B2bLC.Commercial_UDFK equals Commercial_UD.ID
                     join Commercial_BTBItem in db.Commercial_BTBItem
                     on Commercial_B2bLC.Commercial_BTBItemFK equals Commercial_BTBItem.ID
                     where Commercial_B2bLC.Common_SupplierFK == id && Commercial_B2bLC.IsApproved
                     select new VmAcc_SupplierPH
                     {
                         Common_Supplier = Common_Supplier,
                         Commercial_LCOregin = Commercial_LCOregin,
                         Commercial_B2bLC = Commercial_B2bLC,
                         Commercial_BtBLCType = Commercial_BtBLCType,
                         Commercial_UD = Commercial_UD,
                         Commercial_BTBItem = Commercial_BTBItem,
                         // PaidBtbValue = 0
                     }).Where(x => x.Commercial_B2bLC.Active == true).OrderByDescending(x => x.Commercial_B2bLC.ID).OrderByDescending(x => x.Commercial_UD.UdNo).AsEnumerable();

            List<VmAcc_SupplierPH> list = new List<VmAcc_SupplierPH>();
            foreach (VmAcc_SupplierPH x in a)
            {
                x.PaidBtbValue = GetPaidValue(x.Commercial_B2bLC.ID);
                list.Add(x);
            }
            this.DataList = list;

        }

        private decimal GetPaidValue(int id)
        {
            db = new xOssContext();
            decimal paidBtbValue = 0;
            var value = (from commercialB2BPaymentInformation in db.Commercial_B2BPaymentInformation

                         where commercialB2BPaymentInformation.Commercial_B2bLCFK == id && commercialB2BPaymentInformation.Active == true
                         select commercialB2BPaymentInformation.B2bLCPayment);

            if (value.Count() != 0)
            {

                paidBtbValue = value.Sum();
            }
            return paidBtbValue;
        }



        //----------------------Supplier allocatoin History------------------//
        public decimal LCallocationHistory(int? id)
        {
            db = new xOssContext();

            var b = (from t1 in db.Commercial_B2bLC
                     where t1.Common_SupplierFK == id && t1.Active == true && t1.IsApproved
                     select t1.Amount).DefaultIfEmpty().Sum();
            return b;
        }
        public decimal? POallocationValue(int? id)
        {
            db = new xOssContext();

            var b = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_POSlave on t1.ID equals t2.Mkt_POFK
                     where t1.Active == true && t2.Active == true && t1.Common_SupplierFK == id
                     select (t2.Price * t2.TotalRequired)).DefaultIfEmpty().Sum();
            return b;
        }

        //----------------------Supplier Payment History------------------//
        public decimal? GetB2BLCWisePaymentSummary(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Commercial_B2BPaymentInformation
                     join t2 in db.Commercial_B2bLC on t1.Commercial_B2bLCFK equals t2.ID
                     join t3 in db.Common_Supplier on t2.Common_SupplierFK equals t3.ID
                     where t2.Common_SupplierFK == id && t2.Active == true && t1.Active == true
                     && t3.Active == true && t1.IsApproved && t2.IsApproved
                     select t1.B2bLCPayment).DefaultIfEmpty().Sum();
            return v;
        }
        public decimal? ChequeWisePaymentSummary(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Acc_SupplierPH
                     where t1.Common_SupplierFK == id && t1.Active == true && t1.IsApproved
                     select t1.Amount).DefaultIfEmpty().Sum();

            return a;
        }
        public decimal? ChequeWiseReceiveSummary(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Acc_SupplierReceiveH
                where t1.Common_SupplierFK == id && t1.Active == true && t1.IsApproved
                select t1.Amount).DefaultIfEmpty().Sum();

            return a;
        }
        public decimal? GetPoRecevingChallanValue(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                     join t3 in db.Mkt_POSlave_Receiving on t1.ID equals t3.Mkt_POSlaveFK
                     where t2.Common_SupplierFK == id && t3.Active == true && t1.Active == true && t2.Active == true
                     select (t3.Quantity * t1.Price)).DefaultIfEmpty().Sum();
            return a;
        }
        public decimal? GetBillValue(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Acc_SupplierBillSlave
                     join t2 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t2.ID
                     where t2.Common_SupplierFk == id && t1.Active == true && t1.IsApproved && t2.Active == true
                     select new
                     {
                         Amount = (decimal)t1.Amount
                     }).ToList();
            var amt = a.Sum(x => x.Amount);
            return amt;
        }
        //-------------------------------------------------------------------//
        private decimal? GetPaymentHistory(int? id)
        {
            db = new xOssContext();

            var b = (from Acc_SupplierPH in db.Acc_SupplierPH
                     where Acc_SupplierPH.Common_SupplierFK == id
                     select Acc_SupplierPH.Amount).Sum();
            return b;
        }
        private decimal? GetPoWiseDue(int? id)
        {
            db = new xOssContext();

            var c = (from Mkt_PO in db.Mkt_PO
                     join Mkt_POSlave in db.Mkt_POSlave
                     on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     where Mkt_PO.Common_SupplierFK == id
                     select Mkt_POSlave.Price * Mkt_POSlave.TotalRequired).Sum();
            return c;
        }

        private decimal? GetPayable(int id)
        {
            db = new xOssContext();

            var d = (from Mkt_PO in db.Mkt_PO
                     join Mkt_POSlave in db.Mkt_POSlave
                     on Mkt_PO.ID equals Mkt_POSlave.Mkt_POFK
                     join Mkt_POSlave_Receiving in db.Mkt_POSlave_Receiving
                     on Mkt_POSlave.ID equals Mkt_POSlave_Receiving.Mkt_POSlaveFK
                     where Mkt_PO.Common_SupplierFK == id
                     select (Mkt_POSlave.Price * Mkt_POSlave_Receiving.Quantity)).Sum();

            return d;
        }

        public void InitialDataLoadSupplier(int id)
        {
            db = new xOssContext();
            var c = (from t1 in db.Acc_SupplierPH
                     join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID
                     join t3 in db.Commercial_Bank on t1.Commercial_BankFK equals t3.ID
                     where t1.Common_SupplierFK == id
                     && t1.Active == true && t2.Active == true && t3.Active == true
                     select new VmAcc_SupplierPH
                     {
                         Acc_SupplierPH = t1,
                         Acc_AcName = t2,
                         Lein_Bank = t3
                     }).OrderByDescending(x => x.Acc_SupplierPH.ID).AsEnumerable();
            this.DataList = c;
        }


        public void SelectSingleForSupplierPH(int id)
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier
                     where Common_Supplier.Active == true
                     select new
                     {
                         Common_Supplier = Common_Supplier

                     }).Where(c => c.Common_Supplier.ID == id).SingleOrDefault();
            this.Common_Supplier = a.Common_Supplier;
            this.Acc_SupplierPH = new Acc_SupplierPH();
            this.Acc_SupplierPH.Common_SupplierFK = a.Common_Supplier.ID;
            this.Acc_SupplierPH.Date = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"));
          
        }

        internal decimal? GetOpenBTBValueForThisSupplier(int iD)
        {
            db = new xOssContext();
            var totalBTBValue = (from t1 in db.Commercial_B2bLC
                                 join t2 in db.Commercial_UD
                                 on t1.Commercial_UDFK equals t2.ID
                                 where t1.Active == true && t2.Active == true && t1.Common_SupplierFK == iD
                                 select t1.Amount).ToList();
            decimal tTLbTBValue = 0;
            if (totalBTBValue.Count() != 0)
            {
                foreach (var x in totalBTBValue)
                {
                    tTLbTBValue += x;
                }
            }

            return tTLbTBValue;
        }

        public int Add(int userID)
        {
            Acc_SupplierPH.AddReady(userID);
            return Acc_SupplierPH.Add();

        }
        public bool Edit(int userID)
        {
            return Acc_SupplierPH.Edit(userID);
        }
        public bool Delete(int userID)
        {
            return Acc_SupplierPH.Delete(userID);
        }
        //....................................//
        public void SelectSingle(int id)
        {

            db = new xOssContext();
            var v = (from rc in db.Common_Supplier
                     select new VmAcc_SupplierPH
                     {
                         Common_Supplier = rc
                     }).Where(c => c.Common_Supplier.ID == id).SingleOrDefault();
            this.Common_Supplier = v.Common_Supplier;

        }
        internal void GoodsReceivedDocLoad(int id)
        {
            db = new xOssContext();

            var a = (from Mkt_POSlave in db.Mkt_POSlave
                     join Mkt_PO in db.Mkt_PO
                     on Mkt_POSlave.Mkt_POFK equals Mkt_PO.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_POSlave.Raw_ItemFK equals Raw_Item.ID
                     join Common_Unit in db.Common_Unit
                     on Mkt_POSlave.Common_UnitFK equals Common_Unit.ID
                     join t1 in db.Common_Supplier on Mkt_PO.Common_SupplierFK equals t1.ID

                     where t1.ID == id && Mkt_PO.Active == true
                     && Mkt_POSlave.Active == true


                     select new ReportDocType
                     {
                         Body1 = Mkt_PO.CID,
                         Body2 = Raw_Item.Name,
                         Body3 = Mkt_POSlave.TotalRequired.ToString(),
                         Body7 = Mkt_POSlave.ID.ToString(),
                         Body8 = Common_Unit.Name,
                         Body9 = t1.Name,
                         Body11 = Mkt_PO.ID.ToString()
                     }).OrderByDescending(x => x.Body11).ToList();

            foreach (ReportDocType r in a)
            {
                int poSlaveID = 0;
                decimal balance = 0;
                decimal body3 = 0;
                decimal.TryParse(r.Body3, out body3);
                int.TryParse(r.Body7, out poSlaveID);
                decimal recived, returned;
                recived = GetTotalReceive(poSlaveID);
                returned = GetTotalReturn(poSlaveID);
                //r.ReportDocTypeList = GetChallanNo(poSlaveID);
                r.Body4 = recived.ToString("F");
                r.Body5 = returned.ToString("F");
                r.Body6 = (recived - returned).ToString("F");
                decimal body6 = 0;
                decimal.TryParse(r.Body6, out body6);
                balance = body3 - body6;
                r.Body10 = balance.ToString("F");


            }


            this.GoodsReportDoc = a.ToList();
        }

        internal decimal GetTotalReceive(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POSlaveFK == id
                     && t1.IsReturn == false
                     select t1.Quantity).Sum().GetValueOrDefault();
            return x;

        }
        public class ChallanVM
        {
            public int ID { get; set; }
            public string Challan { get; set; }
            public decimal? Quantity { get; set; }
            public decimal? Price { get; set; }
            public decimal? TotalPrice { get; set; }

        }
        public List<ChallanVM> ChallanList { get; set; }
        internal List<ChallanVM> GetChallanNo(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave_Receiving
                     join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                     where t1.Mkt_POSlaveFK == id
                     && t1.IsReturn == false
                     select new ChallanVM
                     {
                         ID = t1.ID,
                         Challan = t1.Challan,
                         Quantity = t1.Quantity,
                         Price = t2.Price,
                         TotalPrice = t1.Quantity * t2.Price
                     }).Distinct().ToList();
            return x;

        }
        internal decimal GetTotalReturn(int id)
        {
            db = new xOssContext();
            var x = (from t1 in db.Mkt_POSlave_Receiving
                     where t1.Mkt_POSlaveFK == id
                     && t1.IsReturn == true
                     select t1.Quantity).Sum().GetValueOrDefault();
            return x;
        }

        //public string MyProperty { get; set; }
        internal void POwiseReceivedGoods(int id)
        {
            db = new xOssContext();

            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO
                     on t1.Mkt_POFK equals t2.ID
                     join t3 in db.Raw_Item
                     on t1.Raw_ItemFK equals t3.ID
                     join t4 in db.Common_Unit
                     on t1.Common_UnitFK equals t4.ID
                     join t5 in db.Common_Supplier
                     on t2.Common_SupplierFK equals t5.ID
                     join t6 in db.Mkt_BOM
                     on t2.Mkt_BOMFK equals t6.ID
                     join t7 in db.Common_TheOrder
                     on t6.Common_TheOrderFk equals t7.ID
                     where t5.ID == id
                     && t2.Active == true

                     select new VmAcc_SupplierPH
                     {
                         Mkt_POSlave = t1,
                         Mkt_PO = t2,
                         Raw_Item = t3,
                         Common_Unit = t4,
                         Common_Supplier = t5,
                         Mkt_BOM = t6,
                         Common_TheOrder = t7
                     }).Where(x => x.Mkt_POSlave.Active == true).OrderByDescending(x => x.Mkt_PO.ID).AsEnumerable();

            List<VmAcc_SupplierPH> list = new List<VmAcc_SupplierPH>();
            foreach (VmAcc_SupplierPH x in a)
            {
                x.Recived = GetTotalReceive(x.Mkt_POSlave.ID);
                x.Returned = GetTotalReturn(x.Mkt_POSlave.ID);
                x.ChallanList = GetChallanNo(x.Mkt_POSlave.ID);
                x.Total = x.Recived - x.Returned;
                x.Balance = x.Mkt_POSlave.TotalRequired - x.Total;
                list.Add(x);
            }

            this.DataList = list;
        }
    }

    public class SumofPayableDueSupplier
    {
        public decimal Due { get; set; }
        public decimal Payable { get; set; }
    }
}