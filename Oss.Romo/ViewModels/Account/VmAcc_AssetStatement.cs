﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_AssetStatement
    {
        private xOssContext db = new xOssContext();
        public List<VmAcc_LiabilitiesStatementReport> DataListLiabilities { get; set; }
        public List<VmAcc_LiabilitiesStatementReport> DataListAssets { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal OpeningBalanceLiabilities { get; set; }
        public decimal OpeningBalanceAssets { get; set; }
        public void InitialDataLoad(VmAcc_AssetStatement vmAcc_LiabilitiesStatement)
        {
            try
            {
                Acc_Type t = new Acc_Type();
                IEnumerable<Acc_Type> listOfAcc_Type = from tbl in t.GetAcc_Type()
                                                       select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };


                var TotalLiabilitiesFromJournal = vmAcc_LiabilitiesStatement.TotalLiabilitiesFromJournal(listOfAcc_Type);

                var TotalAssetFromJournal = vmAcc_LiabilitiesStatement.TotalAssetFromJournal(listOfAcc_Type);

                var joinqueryLiabilities = TotalLiabilitiesFromJournal.VmAcc_LiabilitiesStatementReport
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_LiabilitiesStatementReport
                    {
                        Name = cl.First().Name,
                        Balance = cl.Sum(c => c.Balance)
                    })
                    .OrderBy(x => x.Name).ToList();

                var joinqueryAsset = TotalAssetFromJournal.VmAcc_LiabilitiesStatementReport
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_LiabilitiesStatementReport
                    {
                        Name = cl.First().Name,
                        Balance = cl.Sum(c => c.Balance)
                    })
                    .OrderBy(x => x.Name).ToList();



                OpeningBalanceLiabilities = TotalLiabilitiesFromJournal.OpeingBalance;
                OpeningBalanceAssets = TotalAssetFromJournal.OpeingBalance;

                DataListLiabilities = joinqueryLiabilities;
                DataListAssets = joinqueryAsset;
            }
            catch (Exception ex)
            {
                //ignore
            }
        }
        public void InitialDataLoadOnlyLiabilities(VmAcc_AssetStatement vmAcc_LiabilitiesStatement)
        {
            try
            {
                Acc_Type t = new Acc_Type();
                IEnumerable<Acc_Type> listOfAcc_Type = from tbl in t.GetAcc_Type()
                                                       select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };


                var TotalLiabilitiesFromJournal = vmAcc_LiabilitiesStatement.TotalLiabilitiesFromJournal(listOfAcc_Type);


                var joinqueryLiabilities = TotalLiabilitiesFromJournal.VmAcc_LiabilitiesStatementReport
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_LiabilitiesStatementReport
                    {
                        Name = cl.First().Name,
                        Balance = cl.Sum(c => c.Balance)
                    })
                    .OrderBy(x => x.Name).ToList();


                OpeningBalanceLiabilities = TotalLiabilitiesFromJournal.OpeingBalance;

                DataListLiabilities = joinqueryLiabilities;
            }
            catch (Exception ex)
            {
                //ignore
            }
        }
        public void InitialDataLoadOnlyAsset(VmAcc_AssetStatement vmAcc_LiabilitiesStatement)
        {
            try
            {
                Acc_Type t = new Acc_Type();
                IEnumerable<Acc_Type> listOfAcc_Type = from tbl in t.GetAcc_Type()
                                                       select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

                var TotalAssetFromJournal = vmAcc_LiabilitiesStatement.TotalAssetFromJournal(listOfAcc_Type);


                var joinqueryAsset = TotalAssetFromJournal.VmAcc_LiabilitiesStatementReport
                    .GroupBy(l => l.Name).Select(cl => new VmAcc_LiabilitiesStatementReport
                    {
                        Name = cl.First().Name,
                        Balance = cl.Sum(c => c.Balance)
                    })
                    .OrderBy(x => x.Name).ToList();

                OpeningBalanceAssets = TotalAssetFromJournal.OpeingBalance;
                
                DataListAssets = joinqueryAsset;
            }
            catch (Exception ex)
            {
                //ignore
            }
        }
        public VmAcc_LiabilitiesStatementDataReturn TotalLiabilitiesFromJournal(IEnumerable<Acc_Type> listOfAcc_Type)
        {

            var v1Liabilities = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                                 join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                                 join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                                 join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                                 join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                                 where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                                 && t5.Acc_TypeFK == 2 && (t3.Date >= FromDate && t3.Date <= ToDate)
                                 select new VmAcc_LiabilitiesStatementReport
                                 {
                                     Name = t2.Name,
                                     Balance = t1.Credit - t1.Debit
                                 }).ToList();

            var v1valLiabilities = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                                    join t2 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t2.ID
                                    join t3 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t3.ID
                                    join t4 in db.Acc_Chart2.AsEnumerable() on t3.Acc_Chart2FK equals t4.ID
                                    join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                                    where t2.isFinal && t1.Active == true && t2.Active == true && t3.Active == true && t2.Date < FromDate
                                    && t5.Acc_TypeFK == 2
                                    select new
                                    {
                                        Balance = t1.Credit - t1.Debit
                                    }).ToList().Sum(x => x.Balance);

            return new VmAcc_LiabilitiesStatementDataReturn()
            {
                VmAcc_LiabilitiesStatementReport = v1Liabilities,
                OpeingBalance = v1valLiabilities
            };
        }
        public VmAcc_LiabilitiesStatementDataReturn TotalAssetFromJournal(IEnumerable<Acc_Type> listOfAcc_Type)
        {

            var v1Asset = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                           join t3 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t3.ID
                           join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                           join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                           join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                           where t3.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                                 && t5.Acc_TypeFK == 1 && (t3.Date >= FromDate && t3.Date <= ToDate)
                           select new VmAcc_LiabilitiesStatementReport
                           {
                               Name = t2.Name,
                               Balance = t1.Debit - t1.Credit
                           }).ToList();

            var v1valAsset = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                              join t2 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t2.ID
                              join t3 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t3.ID
                              join t4 in db.Acc_Chart2.AsEnumerable() on t3.Acc_Chart2FK equals t4.ID
                              join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                              where t2.isFinal && t1.Active == true && t2.Active == true && t3.Active == true
                              && t2.Date < FromDate && t5.Acc_TypeFK == 1
                              select new
                              {
                                  Balance = t1.Debit - t1.Credit
                              }).ToList().Sum(x => x.Balance);

            return new VmAcc_LiabilitiesStatementDataReturn()
            {
                VmAcc_LiabilitiesStatementReport = v1Asset,
                OpeingBalance = v1valAsset
            };
        }
    }

    public class VmAcc_LiabilitiesStatementClass
    {
        [DisplayName("Chart of Account Head No")]
        public List<string> ChartOfAccountType { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
    }
    public class VmAcc_LiabilitiesStatementReport
    {
        public string Name { get; set; }
        public Decimal Balance { get; set; }
    }
    public class VmAcc_LiabilitiesStatementDataReturn
    {
        public List<VmAcc_LiabilitiesStatementReport> VmAcc_LiabilitiesStatementReport { get; set; }
        public Decimal OpeingBalance { get; set; }
    }
}