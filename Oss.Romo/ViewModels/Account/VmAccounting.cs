﻿using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
namespace Oss.Romo.ViewModels.Account
{
    public class VmAccounting
    {
        private xOssContext db;
        public Acc_Type Acc_Type { get; set; }
        public Acc_Chart1 Acc_Chart1 { get; set; }
        public Acc_Chart2 Acc_Chart2 { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
        public Acc_JournalSlave Acc_JournalSlave { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Mkt_BOM Mkt_BOM { get; set; }
        public Mkt_PO Mkt_PO { get; set; }
        public Common_User Common_User { get; set; }
        public Common_TheOrder Common_TheOrder { get; set; }
        public Commercial_B2bLC Commercial_B2bLC { get; set; }
        public Commercial_MasterLC Commercial_MasterLC { get; set; }
        public Commercial_Bank Commercial_Bank { get; set; }

        public decimal PreviousDebit { get; set; }
        public decimal PreviousCredit { get; set; }
        //----------------------------------------------
        public IEnumerable<VmAccounting> DataList1 { get; set; }
        public Acc_Journal Acc_Journal { get; set; }

        public string Name { get; set; }
        public string Name2 { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public string OperatingRevenues { get; set; }
        public string TotalOperatingExpenses { get; set; }
        public string NonOperatingRevenues { get; set; }
        public string NonOperatingExpenses { get; set; }
        public string LedgerName { get; set; }
        public decimal CostofGoods { get; set; }


        //------------------------------------------------------

        public IEnumerable<Commercial_B2BPaymentInformation> DataList { get; set; }
        public IEnumerable<VmTransaction> DataList2 { get; set; }

        public List<VmTransaction> Transaction { get; set; }
        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        public int ChartType { get; set; }
        public int CurrencyType { get; set; } = 2;
        public decimal CurrencyRate { get; set; } = 1;

        public List<ReportDocType> LedgerReport { get; set; }

        public void GetLedgerAccountChart1(int id, string ledgerName)
        {
            List<VmTransaction> tempList = new List<VmTransaction>();

            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                Acc_Type t = new Acc_Type();
                var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
                //int? typeId = 

                var a = (from Acc_Type in listOfAcc_Type.AsEnumerable()
                         join Acc_Chart1 in db.Acc_Chart1.AsEnumerable() on Acc_Type.ID equals Acc_Chart1.Acc_TypeFK
                         where Acc_Chart1.ID == id
                         select new
                         {
                             Acc_Type.DrIncrease
                         }).FirstOrDefault();

                bool isDrIncrease = a.DrIncrease;

                var v1 = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                          join t6 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t6.ID
                          join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                          join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                          join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                          where t2.Active == true && t4.ID == id && t6.isFinal
                          && t1.Active == true && t6.Active == true && t6.Date >= FromDate && t6.Date <= ToDate
                          group new { t1, t3 } by t3.Name into g
                          select new VmTransaction
                          {
                              //  DrIncrease = t5.DrIncrease,
                              Account = g.Key, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                              Description = g.Key,
                              Debit = g.Sum(_ => CurrencyType == 1 ? _.t1.Debit : _.t1.Debit * _.t1.CurrencyRate),
                              Credit = g.Sum(_ => CurrencyType == 1 ? _.t1.Credit : _.t1.Credit * _.t1.CurrencyRate)
                          }).ToList();



                var v1val = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                             join t6 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t6.ID
                             join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                             join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                             join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                             where t2.Active == true && t4.ID == id && t6.isFinal && t1.Active == true
                             && t6.Active == true && t6.Date < FromDate
                             group new { t1, t3 } by t3.Name into g
                             select new VmTransaction
                             {
                                 Debit = g.Sum(_ => CurrencyType == 1 ? _.t1.Debit : _.t1.Debit * _.t1.CurrencyRate),
                                 Credit = g.Sum(_ => CurrencyType == 1 ? _.t1.Credit : _.t1.Credit * _.t1.CurrencyRate)
                             }).ToList();


                //---------------------------------------- Acc_Expenses_History  ------------------------------///
                var v2 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                          join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                          join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                          join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                          join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID

                          where t2.Active == true
                          && (t1.From_Acc_NameFK == id || t1.To_Acc_NameFK == id)
                         && t1.Active == true
                         //&& t1.IsIncome == false
                         && t1.Date >= FromDate && t1.Date <= ToDate
                          select new VmTransaction
                          {
                              //  DrIncrease = t5.DrIncrease,'
                              Account = t3.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                              Debit = (t1.To_Acc_NameFK == id) ? t1.Amount : 0,
                              Credit = (t1.From_Acc_NameFK == id) ? t1.Amount : 0
                          }).ToList();

                var v2val = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                             join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                             join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                             join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                             join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID

                             where t2.Active == true
                                   && (t1.From_Acc_NameFK == id || t1.To_Acc_NameFK == id)
                                   && t1.Active == true
                                   //&& t1.IsIncome == false
                                   && t1.Date < FromDate
                             select new
                             {
                                 Debit = (t1.To_Acc_NameFK == id) ? t1.Amount : 0,
                                 Credit = (t1.From_Acc_NameFK == id) ? t1.Amount : 0
                             }).ToList();


                if (!isDrIncrease)
                {
                    decimal temp = 0;
                    foreach (var varTransaction in v2)
                    {
                        temp = varTransaction.Debit;
                        varTransaction.Debit = varTransaction.Credit;
                        varTransaction.Credit = temp;
                    }
                }

                var v = v1.Union(v2).OrderBy(x => x.Date).ToList();
                DateTime dateBeforeStart = v[0].Date;


                decimal previuosBalance = 0;
                if (isDrIncrease)
                {
                    decimal totaldebit = v1val.Sum(item => item.Debit) + v2val.Sum(item => item.Debit);
                    decimal totalcredit = v1val.Sum(item => item.Credit) + v2val.Sum(item => item.Credit);
                    previuosBalance = totaldebit - totalcredit;
                }
                else
                {
                    decimal totaldebit = v1val.Sum(item => item.Debit) + v2val.Sum(item => item.Debit);
                    decimal totalcredit = v1val.Sum(item => item.Credit) + v2val.Sum(item => item.Credit);
                    previuosBalance = totalcredit - totaldebit;
                }

                vmTransition.Date = dateBeforeStart;
                vmTransition.Account = v[0].Account;
                vmTransition.Code = v[0].Code;
                vmTransition.Description = "Opening Balance";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = previuosBalance;
                tempList.Add(vmTransition);
                foreach (var x in v)
                {
                    if (isDrIncrease)
                    {
                        previuosBalance += x.Debit;
                        previuosBalance -= x.Credit;
                    }
                    else
                    {
                        previuosBalance -= x.Debit;
                        previuosBalance += x.Credit;
                    }
                    x.Balance = previuosBalance;
                    tempList.Add(x);
                }

            }
            catch (Exception ex)
            {
                //ignore
            }
            Transaction = tempList;

            BuildLedgerReport(ledgerName);
        }
        public void GetLedgerAccountChart2(int id, string ledgerName)
        {
            List<VmTransaction> tempList = new List<VmTransaction>();

            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                Acc_Type t = new Acc_Type();
                var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
                //int? typeId = 

                var a = (from Acc_Type in listOfAcc_Type.AsEnumerable()
                         join Acc_Chart1 in db.Acc_Chart1.AsEnumerable() on Acc_Type.ID equals Acc_Chart1.Acc_TypeFK
                         join Acc_Chart2 in db.Acc_Chart2.AsEnumerable() on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                         where Acc_Chart2.ID == id
                         select new
                         {
                             Acc_Type.DrIncrease
                         }).FirstOrDefault();

                bool isDrIncrease = a.DrIncrease;

                var v1 = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                          join t6 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t6.ID
                          join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                          join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                          where t2.Active == true && t3.ID == id && t6.isFinal
                          && t1.Active == true && t6.Active == true && t6.Date >= FromDate && t6.Date <= ToDate
                          group new { t1, t2 } by t2.Name into g
                          select new VmTransaction
                          {
                              //  DrIncrease = t5.DrIncrease,
                              Account = g.Key, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                              Description = g.Key,
                              Debit = g.Sum(_ => CurrencyType == 1 ? _.t1.Debit : _.t1.Debit * _.t1.CurrencyRate),
                              Credit = g.Sum(_ => CurrencyType == 1 ? _.t1.Credit : _.t1.Credit * _.t1.CurrencyRate)
                          }).ToList();



                var v1val = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                             join t6 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t6.ID
                             join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                             join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                             where t2.Active == true && t3.ID == id && t6.isFinal
                                   && t1.Active == true && t6.Active == true && t6.Date < FromDate
                             group new { t1, t2 } by t2.Name into g
                             select new VmTransaction
                             {
                                 Debit = g.Sum(_ => CurrencyType == 1 ? _.t1.Debit : _.t1.Debit * _.t1.CurrencyRate),
                                 Credit = g.Sum(_ => CurrencyType == 1 ? _.t1.Credit : _.t1.Credit * _.t1.CurrencyRate)
                             }).ToList();


                //---------------------------------------- Acc_Expenses_History  ------------------------------///
                var v2 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                          join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                          join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                          join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                          join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID

                          where t2.Active == true
                          && (t1.From_Acc_NameFK == id || t1.To_Acc_NameFK == id)
                         && t1.Active == true
                         //&& t1.IsIncome == false
                         && t1.Date >= FromDate && t1.Date <= ToDate
                          select new VmTransaction
                          {
                              //  DrIncrease = t5.DrIncrease,'
                              Account = t3.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                              Debit = (t1.To_Acc_NameFK == id) ? t1.Amount : 0,
                              Credit = (t1.From_Acc_NameFK == id) ? t1.Amount : 0
                          }).ToList();

                var v2val = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                             join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                             join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                             join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                             join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID

                             where t2.Active == true
                                   && (t1.From_Acc_NameFK == id || t1.To_Acc_NameFK == id)
                                   && t1.Active == true
                                   //&& t1.IsIncome == false
                                   && t1.Date < FromDate
                             select new
                             {
                                 Debit = (t1.To_Acc_NameFK == id) ? t1.Amount : 0,
                                 Credit = (t1.From_Acc_NameFK == id) ? t1.Amount : 0
                             }).ToList();


                if (!isDrIncrease)
                {
                    decimal temp = 0;
                    foreach (var varTransaction in v2)
                    {
                        temp = varTransaction.Debit;
                        varTransaction.Debit = varTransaction.Credit;
                        varTransaction.Credit = temp;
                    }
                }

                var v = v1.Union(v2).OrderBy(x => x.Date).ToList();
                DateTime dateBeforeStart = v[0].Date;


                decimal previuosBalance = 0;
                if (isDrIncrease)
                {
                    decimal totaldebit = v1val.Sum(item => item.Debit) + v2val.Sum(item => item.Debit);
                    decimal totalcredit = v1val.Sum(item => item.Credit) + v2val.Sum(item => item.Credit);
                    previuosBalance = totaldebit - totalcredit;
                }
                else
                {
                    decimal totaldebit = v1val.Sum(item => item.Debit) + v2val.Sum(item => item.Debit);
                    decimal totalcredit = v1val.Sum(item => item.Credit) + v2val.Sum(item => item.Credit);
                    previuosBalance = totalcredit - totaldebit;
                }

                vmTransition.Date = dateBeforeStart;
                vmTransition.Account = v[0].Account;
                vmTransition.Code = v[0].Code;
                vmTransition.Description = "Opening Balance";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = previuosBalance;
                tempList.Add(vmTransition);
                foreach (var x in v)
                {
                    if (isDrIncrease)
                    {
                        previuosBalance += x.Debit;
                        previuosBalance -= x.Credit;
                    }
                    else
                    {
                        previuosBalance -= x.Debit;
                        previuosBalance += x.Credit;
                    }
                    x.Balance = previuosBalance;
                    tempList.Add(x);
                }

            }
            catch (Exception ex)
            {
                //ignore
            }
            Transaction = tempList;

            BuildLedgerReport(ledgerName);
        }
        public void GetLedgerAccountHeadFromJournal(int id)
        {
            List<VmTransaction> tempList = new List<VmTransaction>();
            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                Acc_Type t = new Acc_Type();
                var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

                var v = (from Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                         join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                         join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                         on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                         join Acc_AcName in db.Acc_AcName.AsEnumerable()
                         on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                         join Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                         on Acc_AcName.ID equals Acc_JournalSlave.Acc_AcNameFK
                         join Acc_Journal in db.Acc_Journal.AsEnumerable()
                         on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                         where
                         Acc_JournalSlave.Acc_AcNameFK == id
                         && Acc_Chart1.Active == true
                         && Acc_Chart2.Active == true
                         && Acc_AcName.Active == true
                         && Acc_Journal.Active == true
                         && Acc_Journal.isFinal == true && Acc_JournalSlave.Active == true
                         && Acc_Journal.Date >= FromDate && Acc_Journal.Date <= ToDate
                         select new VmTransaction
                         {
                             DrIncrease = Acc_Type.DrIncrease,
                             Account = Acc_Type.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,
                             Code = Acc_AcName.Code,
                             Date = Acc_Journal.Date,
                             Description = "JE:" + Acc_JournalSlave.Description,
                             Debit = Acc_JournalSlave.Debit,
                             Credit = Acc_JournalSlave.Credit
                         }).OrderBy(x => x.Date).ToList();



                DateTime dateBeforeStart = v[0].Date;
                bool isDrIncrease = v[0].DrIncrease;
                var pDR = (from Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                           join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                           join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                           on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                           join Acc_AcName in db.Acc_AcName.AsEnumerable()
                           on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                           join Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                           on Acc_AcName.ID equals Acc_JournalSlave.Acc_AcNameFK
                           join Acc_Journal in db.Acc_Journal.AsEnumerable()
                           on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                           where
                           Acc_JournalSlave.Acc_AcNameFK == id
                           && Acc_Chart1.Active == true
                           && Acc_Chart2.Active == true
                           && Acc_AcName.Active == true
                           && Acc_Journal.Active == true
                           && Acc_Journal.isFinal == true && Acc_JournalSlave.Active == true
                           && Acc_Journal.Date < FromDate
                           select
                           (
                               Acc_JournalSlave.Debit
                           )).DefaultIfEmpty(0).Sum();

                var pCR = (from Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                           join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                           join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                           on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                           join Acc_AcName in db.Acc_AcName.AsEnumerable()
                           on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                           join Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                           on Acc_AcName.ID equals Acc_JournalSlave.Acc_AcNameFK
                           join Acc_Journal in db.Acc_Journal.AsEnumerable()
                           on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                           where
                           Acc_JournalSlave.Acc_AcNameFK == id
                           && Acc_Chart1.Active == true
                           && Acc_Chart2.Active == true
                           && Acc_AcName.Active == true
                           && Acc_Journal.Active == true
                           && Acc_Journal.isFinal == true && Acc_JournalSlave.Active == true
                           && Acc_Journal.Date < FromDate
                           select
                            (
                                Acc_JournalSlave.Credit
                            )).DefaultIfEmpty(0).Sum();
                decimal previuosBalance = 0;
                if (isDrIncrease)
                {
                    previuosBalance += pDR;
                    previuosBalance -= pCR;
                }
                else
                {
                    previuosBalance -= pDR;
                    previuosBalance += pCR;
                }

                vmTransition.Date = dateBeforeStart;
                vmTransition.Account = v[0].Account;
                vmTransition.Code = v[0].Code;
                vmTransition.Description = "Opening Balance";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = previuosBalance;
                tempList.Add(vmTransition);
                foreach (var x in v)
                {
                    if (isDrIncrease)
                    {
                        previuosBalance += x.Debit;
                        previuosBalance -= x.Credit;
                    }
                    else
                    {
                        previuosBalance -= x.Debit;
                        previuosBalance += x.Credit;
                    }
                    x.Balance = previuosBalance;
                    tempList.Add(x);
                }

            }
            catch { }
            Transaction = tempList;

        }
        public void GetLedgerAccountHead(int id, string ledgerName)
        {
            List<VmTransaction> tempList = new List<VmTransaction>();

            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                Acc_Type t = new Acc_Type();
                var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
                //int? typeId = 

                var a = (from Acc_Type in listOfAcc_Type.AsEnumerable()
                         join Acc_Chart1 in db.Acc_Chart1.AsEnumerable() on Acc_Type.ID equals Acc_Chart1.Acc_TypeFK
                         join Acc_Chart2 in db.Acc_Chart2.AsEnumerable() on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                         join Acc_AcName in db.Acc_AcName.AsEnumerable() on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                         where Acc_AcName.ID == id
                         select new
                         {
                             Acc_Type.DrIncrease
                         }).FirstOrDefault();

                bool isDrIncrease = a.DrIncrease;

                var v1 = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                          join t6 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t6.ID
                          join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                          where t2.Active == true
                          && t1.Acc_AcNameFK == id
                          && t6.isFinal
                         && t1.Active == true
                         && t6.Active == true
                         && t6.Date >= FromDate && t6.Date <= ToDate
                          select new VmTransaction
                          {
                              //  DrIncrease = t5.DrIncrease,
                              Account = t2.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                              Code = t2.Code,
                              Date = t6.Date,
                              Description = t6.Title + "|" + t6.Description + "|" + t1.Description,
                              Debit = CurrencyType == 1 ? t1.Debit : t1.Debit * t1.CurrencyRate,
                              Credit = CurrencyType == 1 ? t1.Credit : t1.Credit * t1.CurrencyRate,
                          }).ToList();



                var v1val = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                             join t6 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t6.ID
                             join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                             where t2.Active == true
                                   && t1.Acc_AcNameFK == id
                                   && t6.isFinal
                                   && t1.Active == true
                                   && t6.Active == true
                                   && t6.Date < FromDate
                             select new VmTransaction
                             {
                                 Debit = CurrencyType == 1 ? t1.Debit : t1.Debit * t1.CurrencyRate,
                                 Credit = CurrencyType == 1 ? t1.Credit : t1.Credit * t1.CurrencyRate,
                             }).ToList();


                //---------------------------------------- Acc_Expenses_History  ------------------------------///
                var v2 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                          join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                          join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                          join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                          join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID

                          where t2.Active == true
                          && (t1.From_Acc_NameFK == id || t1.To_Acc_NameFK == id)
                         && t1.Active == true
                         //&& t1.IsIncome == false
                         && t1.Date >= FromDate && t1.Date <= ToDate
                          select new VmTransaction
                          {
                              //  DrIncrease = t5.DrIncrease,'
                              Account = t2.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                              Code = t2.Code,
                              Date = t1.Date,
                              Description = t1.Referance,
                              Debit = (t1.To_Acc_NameFK == id) ? t1.Amount : 0,
                              Credit = (t1.From_Acc_NameFK == id) ? t1.Amount : 0
                          }).ToList();

                var v2val = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                             join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                             join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                             join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                             join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID

                             where t2.Active == true
                                   && (t1.From_Acc_NameFK == id || t1.To_Acc_NameFK == id)
                                   && t1.Active == true
                                   //&& t1.IsIncome == false
                                   && t1.Date < FromDate
                             select new
                             {
                                 Debit = (t1.To_Acc_NameFK == id) ? t1.Amount : 0,
                                 Credit = (t1.From_Acc_NameFK == id) ? t1.Amount : 0
                             }).ToList();


                if (!isDrIncrease)
                {
                    decimal temp = 0;
                    foreach (var varTransaction in v2)
                    {
                        temp = varTransaction.Debit;
                        varTransaction.Debit = varTransaction.Credit;
                        varTransaction.Credit = temp;
                    }
                }

                var v = v1.Union(v2).OrderBy(x => x.Date).ToList();
                DateTime dateBeforeStart = v[0].Date;


                decimal previuosBalance = 0;
                if (isDrIncrease)
                {
                    decimal totaldebit = v1val.Sum(item => item.Debit) + v2val.Sum(item => item.Debit);
                    decimal totalcredit = v1val.Sum(item => item.Credit) + v2val.Sum(item => item.Credit);
                    previuosBalance = totaldebit - totalcredit;
                }
                else
                {
                    decimal totaldebit = v1val.Sum(item => item.Debit) + v2val.Sum(item => item.Debit);
                    decimal totalcredit = v1val.Sum(item => item.Credit) + v2val.Sum(item => item.Credit);
                    previuosBalance = totalcredit - totaldebit;
                }

                vmTransition.Date = dateBeforeStart;
                vmTransition.Account = v[0].Account;
                vmTransition.Code = v[0].Code;
                vmTransition.Description = "Opening Balance";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = previuosBalance;
                tempList.Add(vmTransition);
                foreach (var x in v)
                {
                    if (isDrIncrease)
                    {
                        previuosBalance += x.Debit;
                        previuosBalance -= x.Credit;
                    }
                    else
                    {
                        previuosBalance -= x.Debit;
                        previuosBalance += x.Credit;
                    }
                    x.Balance = previuosBalance;
                    tempList.Add(x);
                }

            }
            catch (Exception ex)
            {
                //ignore
            }
            Transaction = tempList;

            BuildLedgerReport(ledgerName);
        }

        public void GetIncomeStatementReport()
        {
            List<VmTransaction> tempList = new List<VmTransaction>();

            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                Acc_Type t = new Acc_Type();
                var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
                //int? typeId = 

                var a = (from Acc_Type in listOfAcc_Type.AsEnumerable()
                         where Acc_Type.ID == 4
                         select new
                         {
                             Acc_Type.DrIncrease
                         }).FirstOrDefault();

                bool isDrIncrease = a.DrIncrease;

                var v1 = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                          join t6 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t6.ID
                          join t2 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t2.ID
                          join t4 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t4.ID
                          join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                          where t2.Active == true && t6.isFinal && t1.Active == true && t6.Active == true
                          && t5.Acc_TypeFK == 4 //&& (t6.Date >= FromDate && t6.Date <= ToDate)
                          select new VmTransaction
                          {
                              Account = t2.Name,
                              Code = t2.Code,
                              Date = t6.Date,
                              Description = t6.Title + "|" + t6.Description + "|" + t1.Description,
                              Debit = t1.Debit,
                              Credit = t1.Credit
                          }).ToList();

                var v1val = (from t1 in db.Acc_JournalSlave.AsEnumerable()
                             join t2 in db.Acc_Journal.AsEnumerable() on t1.Acc_JournalFK equals t2.ID
                             join t3 in db.Acc_AcName.AsEnumerable() on t1.Acc_AcNameFK equals t3.ID
                             join t4 in db.Acc_Chart2.AsEnumerable() on t3.Acc_Chart2FK equals t4.ID
                             join t5 in db.Acc_Chart1.AsEnumerable() on t4.Acc_Chart1FK equals t5.ID
                             where t2.isFinal && t1.Active == true && t2.Date < FromDate
                             && t5.Acc_TypeFK == 4
                             select new
                             {
                                 Balance = t1.Credit - t1.Debit
                             }).ToList().Sum(x => x.Balance);

                ////---------------------------------------- Acc_Expenses_History  ------------------------------///
                //var v2 = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                //          join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                //          join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //          join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //          join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //          where t2.Active == true && t1.Active == true && t1.IsIncome
                //          && t1.Date >= FromDate && t1.Date <= ToDate
                //          select new VmTransaction
                //          {
                //              Account = t2.Name, 
                //              Code = t2.Code,
                //              Date = t1.Date,
                //              Description = "JE:" + t1.Referance,
                //              Debit = (t1.To_Acc_NameFK == id) ? t1.Amount : 0,
                //              Credit = (t1.From_Acc_NameFK == id) ? t1.Amount : 0
                //          }).ToList();

                //var v2val = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                //             join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                //             join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //             join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //             join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID

                //             where t2.Active == true
                //                   && (t1.From_Acc_NameFK == id || t1.To_Acc_NameFK == id)
                //                   && t1.Active == true
                //                   //&& t1.IsIncome == false
                //                   && t1.Date < FromDate
                //             select new
                //             {
                //                 Debit = (t1.To_Acc_NameFK == id) ? t1.Amount : 0,
                //                 Credit = (t1.From_Acc_NameFK == id) ? t1.Amount : 0
                //             }).ToList();

                //----------------------------------------Acc_Transaction------------------------------///
                //var v3 = (from t1 in db.Acc_Transaction.AsEnumerable()
                //          join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                //          join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //          join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //          where t2.Active == true && (t4.Acc_TypeFK == 4)
                //          //&& t1.Active == true && t1.IsFinal == true
                //          //&& t1.Date >= FromDate && t1.Date <= ToDate
                //          select new VmTransaction
                //          {
                //              Account = t2.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                //              Code = t2.Code,
                //              Date = t1.Date,
                //              Description = "Voucher No: " + t1.VoucherNo + ", Des: " + t1.Description,
                //              Debit = 0,
                //              Credit = t1.Amount
                //          }).ToList();

                //var v3val = (from t1 in db.Acc_Transaction.AsEnumerable()
                //             join t2 in db.Acc_AcName.AsEnumerable() on t1.From_Acc_NameFK equals t2.ID
                //             join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //             join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //             where t2.Active == true && (t4.Acc_TypeFK == 4) && t1.Active == true && t1.Date < FromDate
                //             select new
                //             {
                //                 Credit = t1.Amount
                //             }).ToList().Sum(x => x.Credit);

                //var v3_1 = (from t1 in db.Acc_Transaction.AsEnumerable()
                //            join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                //            join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //            join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //            where t2.Active == true && (t4.Acc_TypeFK == 4)
                //            //&& t1.Active == true && t1.IsFinal == true && t1.Date >= FromDate && t1.Date <= ToDate
                //            select new VmTransaction
                //            {
                //                Account = t2.Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                //                Code = t2.Code,
                //                Date = t1.Date,
                //                Description = "Voucher No: " + t1.VoucherNo + ", Des: " + t1.Description,
                //                Debit = t1.Amount,
                //                Credit = 0
                //            }).ToList();

                //var v3_1val = (from t1 in db.Acc_Transaction.AsEnumerable()
                //               join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                //               join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                //               join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                //               join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                //               where t2.Active == true && (t5.ID == 4) && t1.Active == true && t1.Date < FromDate
                //               select new
                //               {
                //                   Debit = t1.Amount
                //               }).ToList().Sum(x => x.Debit);


                var joinquery = v1
                    .GroupBy(l => l.Account).Select(cl => new VmTransaction
                    {
                        Account = cl.First().Name, /*t2.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,*/
                        Code = "",
                        Date = Date,
                        Description = Description,
                        Debit = cl.Sum(c => c.Debit),
                        Credit = cl.Sum(c => c.Credit)
                    })
                    .OrderBy(x => x.Name).ToList();





                decimal previuosBalance = v1val;

                //if (isDrIncrease)
                //{
                //    decimal totaldebit = v1val.Sum(item => item.Balance) + v2val.Sum(item => item.Debit) + v3val.Sum(item => item.Debit);
                //    decimal totalcredit = v1val.Sum(item => item.Credit) + v2val.Sum(item => item.Credit) + +v3val.Sum(item => item.Credit);
                //    previuosBalance = totaldebit - totalcredit;
                //}
                //else
                //{
                //    decimal totaldebit = v1val.Sum(item => item.Balance) + v2val.Sum(item => item.Debit) + v3val.Sum(item => item.Debit);
                //    decimal totalcredit = v1val.Sum(item => item.Credit) + v2val.Sum(item => item.Credit) + +v3val.Sum(item => item.Credit);
                //    previuosBalance = totalcredit - totaldebit;
                //}

                //vmTransition.Date = dateBeforeStart;
                //vmTransition.Account = v[0].Account;
                //vmTransition.Code = v[0].Code;
                //vmTransition.Description = "Opening Balance";
                //vmTransition.Debit = 0;
                //vmTransition.Credit = 0;
                //vmTransition.Balance = previuosBalance;
                //tempList.Add(vmTransition);
                //foreach (var x in v)
                //{
                //    previuosBalance += x.Balance;
                //    x.Balance = previuosBalance;
                //    tempList.Add(x);
                //}

            }
            catch (Exception ex)
            {
                //ignore
            }
            Transaction = tempList;

            BuildLedgerReport("Ledger");
        }

        private void BuildLedgerReport(string ledgername)
        {
            ReportDocType reportDocType;
            LedgerReport = new List<ReportDocType>();
            int sl = 0;
            string bCarriedForward;
            if (Transaction.Any())
            {
                bCarriedForward = Transaction[Transaction.Count() - 1].Balance.ToString("F");
            }
            else
            {
                bCarriedForward = "0";
            }
            decimal totalDebit = 0;
            decimal totalCredit = 0;
            foreach (VmTransaction vmT in Transaction)
            {

                totalDebit += vmT.Debit;
                totalCredit += vmT.Credit;
                string newDr = vmT.Debit.ToString("F");
                string newCr = vmT.Credit.ToString("F");
                if (newDr == "0" || newDr == "0.00")
                {
                    newDr = "";
                }
                if (newCr == "0" || newCr == "0.00")
                {
                    newCr = "";
                }
                var sign = CurrencyType == 1 ? "$" : "৳";
                reportDocType = new ReportDocType();
                reportDocType.Body7 = ledgername;
                reportDocType.HeaderLeft1 = vmT.Account + ".  Ledger Type : " + vmT.Code;
                reportDocType.HeaderLeft2 = "(From: " + this.FromDate.ToShortDateString() + " - To: " + this.ToDate.ToShortDateString() + (")");
                reportDocType.Body1 = (++sl).ToString();
                reportDocType.Body2 = vmT.Date.ToShortDateString();
                reportDocType.Body3 = vmT.Description;
                reportDocType.Body4 = newDr + sign;
                reportDocType.Body5 = newCr + sign;
                reportDocType.Body6 = vmT.Balance.ToString("F") + sign;
                reportDocType.Body9 = totalDebit.ToString("F") + sign;
                reportDocType.Body10 = totalCredit.ToString("F") + sign;
                reportDocType.SubBody1 = bCarriedForward + sign;
                LedgerReport.Add(reportDocType);

            }
        }
        private void BuildIncomeStatementReport()
        {
            ReportDocType reportDocType;
            LedgerReport = new List<ReportDocType>();
            //int sl = 0;
            // string bCarriedForward = Transaction[Transaction.Count()-1].Balance.ToString("F");
            //string bCarriedForward = Transaction[Transaction.Count()].Balance.ToString("F");
            foreach (VmTransaction vmT in Transaction)
            {
                string newDr = vmT.Debit.ToString("F");
                string newCr = vmT.Credit.ToString("F");
                if (newDr == "0" || newDr == "0.00")
                {
                    newDr = "";
                }
                if (newCr == "0" || newCr == "0.00")
                {
                    newCr = "";
                }
                reportDocType = new ReportDocType();
                reportDocType.HeaderLeft1 = vmT.Account + "  Reference : " + vmT.Code;
                reportDocType.HeaderLeft2 = "(From: " + this.FromDate.ToShortDateString() + " - To: " + this.ToDate.ToShortDateString() + (")");
                //reportDocType.Body1 = (++sl).ToString();
                reportDocType.Body2 = vmT.Date.ToShortDateString();
                reportDocType.Body3 = vmT.Description;
                reportDocType.Body4 = newDr;
                reportDocType.Body5 = newCr;
                reportDocType.Body6 = vmT.Balance.ToString("F");
                //reportDocType.Body7 = vmT.Name2.ToString("F");
                //reportDocType.Body8 = vmT.Name3.ToString("F");
                //reportDocType.Body9 = vmT.Name4.ToString("F");
                //reportDocType.Body10 = vmT.Name5.ToString("F");
                //reportDocType.Body11 = vmT.Name6.ToString("F");
                //reportDocType.SubBody1 = bCarriedForward;
                LedgerReport.Add(reportDocType);

            }
        }
        private void IncomeStatementReport()
        {
            ReportDocType reportDocType;
            LedgerReport = new List<ReportDocType>();
            VmAccounting v = new VmAccounting();
            int sl = 0;
            foreach (VmTransaction vmT in Transaction)
            {
                string newDr = vmT.Debit.ToString();
                string newCr = vmT.Credit.ToString();
                if (newDr == "0" || newDr == "0.00")
                {
                    newDr = "";
                }
                if (newCr == "0" || newCr == "0.00")
                {
                    newCr = "";
                }
                reportDocType = new ReportDocType();
                reportDocType.HeaderLeft1 = vmT.Account + " Code: " + vmT.Code;
                reportDocType.HeaderLeft2 = "(From: " + this.FromDate.ToShortDateString() + " - To: " + this.ToDate.ToShortDateString() + (")");
                reportDocType.Body1 = (++sl).ToString();
                reportDocType.Body2 = vmT.Date.ToShortDateString();
                reportDocType.Body3 = vmT.Description;
                reportDocType.Body4 = v.NetProfit().ToString();
                reportDocType.Body5 = newCr;
                reportDocType.Body6 = vmT.Balance.ToString();
                reportDocType.Body7 = v.NetProfit().ToString();

                LedgerReport.Add(reportDocType);

            }
        }

        public void GetLedgerAccountType(int id)
        {

            List<VmTransaction> tempList = new List<VmTransaction>();
            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                Acc_Type t = new Acc_Type();
                var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
                var v = (from Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                         join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                         join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                         on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                         join Acc_AcName in db.Acc_AcName.AsEnumerable()
                         on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                         join Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                         on Acc_AcName.ID equals Acc_JournalSlave.Acc_AcNameFK
                         join Acc_Journal in db.Acc_Journal.AsEnumerable()
                         on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                         where
                         Acc_Type.ID == id
                         && Acc_Chart1.Active == true
                         && Acc_Chart2.Active == true
                         && Acc_AcName.Active == true
                         && Acc_Journal.Active == true
                         && Acc_Journal.isFinal == true && Acc_JournalSlave.Active == true
                         && Acc_Journal.Date >= FromDate && Acc_Journal.Date <= ToDate
                         select new VmTransaction
                         {
                             DrIncrease = Acc_Type.DrIncrease,
                             Account = Acc_Type.Name,
                             Code = Acc_AcName.Code,
                             Date = Acc_Journal.Date,
                             Description = "JE: (" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name + "): " + Acc_JournalSlave.Description,
                             Debit = Acc_JournalSlave.Debit,
                             Credit = Acc_JournalSlave.Credit
                         }).OrderBy(x => x.Date).ToList();

                bool isDrIncrease = v[0].DrIncrease;
                var pDR = (from Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                           join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                           join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                           on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                           join Acc_AcName in db.Acc_AcName.AsEnumerable()
                           on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                           join Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                           on Acc_AcName.ID equals Acc_JournalSlave.Acc_AcNameFK
                           join Acc_Journal in db.Acc_Journal.AsEnumerable()
                           on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                           where
                           Acc_Type.ID == id
                           && Acc_Chart1.Active == true
                           && Acc_Chart2.Active == true
                           && Acc_AcName.Active == true
                           && Acc_Journal.Active == true
                           && Acc_Journal.isFinal == true && Acc_JournalSlave.Active == true
                           && Acc_Journal.Date < FromDate
                           select
                           (
                               Acc_JournalSlave.Debit
                           )).DefaultIfEmpty(0).Sum();

                var pCR = (from Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                           join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                           join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                           on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                           join Acc_AcName in db.Acc_AcName.AsEnumerable()
                           on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                           join Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                           on Acc_AcName.ID equals Acc_JournalSlave.Acc_AcNameFK
                           join Acc_Journal in db.Acc_Journal.AsEnumerable()
                           on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                           where
                             Acc_Type.ID == id
                           && Acc_Chart1.Active == true
                           && Acc_Chart2.Active == true
                           && Acc_AcName.Active == true
                           && Acc_Journal.Active == true
                           && Acc_Journal.isFinal == true && Acc_JournalSlave.Active == true
                           && Acc_Journal.Date < FromDate
                           select
                           (
                               Acc_JournalSlave.Credit
                           )).DefaultIfEmpty(0).Sum();
                decimal previuosBalance = 0;
                if (isDrIncrease)
                {
                    previuosBalance += pDR;
                    previuosBalance -= pCR;
                }
                else
                {
                    previuosBalance -= pDR;
                    previuosBalance += pCR;
                }

                vmTransition.Date = FromDate;
                vmTransition.Account = v[0].Account;
                vmTransition.Code = v[0].Code;
                vmTransition.Description = "Opening Balance";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = previuosBalance;
                tempList.Add(vmTransition);
                foreach (var x in v)
                {
                    if (isDrIncrease)
                    {
                        previuosBalance += x.Debit;
                        previuosBalance -= x.Credit;
                    }
                    else
                    {
                        previuosBalance -= x.Debit;
                        previuosBalance += x.Credit;
                    }
                    x.Balance = previuosBalance;
                    tempList.Add(x);
                }

            }
            catch { }
            Transaction = tempList;
            BuildLedgerReport("Ledger");
        }
        public decimal GetTotalPoValue(string poID)
        {
            db = new xOssContext();
            var a = (from Mkt_POSlave in db.Mkt_POSlave

                     where Mkt_POSlave.Mkt_POFK.ToString() == poID && Mkt_POSlave.Active == true
                     select (Mkt_POSlave.TotalRequired * Mkt_POSlave.Price)).Sum().GetValueOrDefault(0);

            return a;

        }

        public void GetSupplierLedgerLCOpening(int supplierId)
        {


            List<VmTransaction> tempList = new List<VmTransaction>();
            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                var v = (from t1 in db.Mkt_PO
                         join t2 in db.Common_Supplier
                         on t1.Common_SupplierFK equals t2.ID
                         join t3 in db.Mkt_BOM on t1.Mkt_BOMFK equals t3.ID
                         join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                         //join t5 in db.Common_User on t4.FirstCreatedBy equals t5.ID
                         //where t1.Date >= FromDate && t1.Date <= ToDate
                         where t1.Common_SupplierFK == supplierId && t1.IsAuthorize == true && t1.Active == true && t1.PaymentType.Equals("L/C")
                         select new VmTransaction
                         {

                             Account = t1.ID.ToString(),

                             Date = t1.AuthorizeDate.Value,
                             Description = "PO ID : <a target='_New' href='../../marketing/VmMkt_PODetails/" + t1.ID + "'>" + t1.CID + "</a> Date: " + SqlFunctions.DateName("day", t1.Date) + "-" + SqlFunctions.DateName("month", t1.Date) + "-" + SqlFunctions.DateName("year", t1.Date) + "<br /> Order Details : <a target='_New' href='../../Marketing/Mkt_BOMSlaveItem/" + t1.Mkt_BOMFK + "'>" + t4.CID + "/" + t3.Style + "</a>", //+ "Merchandiser :" + t5.Name,
                             Credit = 0,//Debit
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).Distinct().ToList();
                foreach (var x in v)
                {
                    //x.Debit = GetTotalPoValue(x.Account);
                    x.Credit = GetTotalPoValue(x.Account);

                }

                var v1 = (from t1 in db.Commercial_B2bLC
                          join t2 in db.Common_Supplier
                          on t1.Common_SupplierFK equals t2.ID
                          //join t3 in db.Commercial_MasterLC on t1.Commercial_MasterLCFK equals t3.ID
                          join t3 in db.Commercial_UDSlave on t1.Commercial_UDFK equals t3.Commercial_UDFK
                          join t4 in db.Commercial_UD on t3.Commercial_UDFK equals t4.ID
                          where t2.ID == supplierId && t1.Active == true && t1.IsApproved == true
                          select new VmTransaction
                          {
                              Account = t1.ID.ToString(),
                              Date = t1.FirstCreated.Value,
                              Description = "BTB L/C : <a target='_New' href='../../commercial/VmCommercial_UDSlaveIndex/" + t4.ID + "'>" + t1.Name + "</a>BTB Date: " + SqlFunctions.DateName("day", t1.Date) + " - " + SqlFunctions.DateName("month", t1.Date) + " - " + SqlFunctions.DateName("year", t1.Date) + "\nUD : <a href='../../commercial/VmCommercial_UDSlaveIndex/" + t4.ID + "' target='_blank'>" + t4.UdNo + "</a>",// + t4.UdNo,//+"\nBank : " + t4.Name,

                              //Debit = 0,
                              //Credit = t1.Amount,
                              Credit = 0,
                              Debit = t1.Amount,
                              Balance = 0,
                              FirstCreateDate = t1.FirstCreated.Value
                          }).Distinct().ToList();

                //var C = (from t1 in db.Acc_SupplierPH
                //         join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                //         join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                //         join t4 in db.Acc_AcName on t1.Acc_AcNameFK equals t4.ID
                //         join t6 in db.Acc_Chart2 on t4.Acc_Chart2FK equals t6.ID
                //         join t7 in db.Acc_Chart1 on t6.Acc_Chart1FK equals t7.ID

                //         join t5 in db.Commercial_Bank on t1.Commercial_BankFK equals t5.ID
                //         where t1.Common_SupplierFK == supplierId && t1.Active == true && t1.IsApproved == true
                //         select new VmTransaction
                //         {
                //             ID = t1.ID,
                //             Account = t1.ID.ToString(),
                //             Date = t1.Date,
                //             Description = "Acounts Head : " + t7.Name + "/" + t6.Name + "/" + t4.Name + ". Cheque or Voucher no : " + t1.ChequeVoucherNo + ". \nUser Name : " + t3.Name + ". Bank : " + t5.Name + ".\nDesc: " + t1.Description,
                //             Credit = 0,
                //             Debit = 0,
                //             Balance = 0,
                //             FirstCreateDate = t1.FirstCreated.Value
                //         }).ToList();
                //foreach (var x in C)
                //{
                //    //x.Credit = ChequeWisePaymentSummary(x.Account);
                //    x.Debit = ChequeWisePaymentSummary(x.Account);
                //}
                //var D = (from t1 in db.Acc_SupplierReceiveH
                //         join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                //         join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                //         join t4 in db.Acc_AcName on t1.Acc_AcNameFK equals t4.ID
                //         join t6 in db.Acc_Chart2 on t4.Acc_Chart2FK equals t6.ID
                //         join t7 in db.Acc_Chart1 on t6.Acc_Chart1FK equals t7.ID

                //         join t5 in db.Commercial_Bank on t1.Commercial_BankFK equals t5.ID
                //         where t1.Common_SupplierFK == supplierId && t1.Active == true && t1.IsApproved == true
                //         select new VmTransaction
                //         {
                //             ID = t1.ID,
                //             Account = t1.ID.ToString(),
                //             Date = t1.Date,
                //             Description = "Acounts Head : " + t7.Name + "/" + t6.Name + "/" + t4.Name + ". Cheque or Voucher no : " + t1.ChequeVoucherNo + ". \nUser Name : " + t3.Name + ". Bank : " + t5.Name + ".\nDesc: " + t1.Description,
                //             Credit = 0,
                //             Debit = 0,
                //             Balance = 0,
                //             FirstCreateDate = t1.FirstCreated.Value
                //         }).ToList();
                //foreach (var x in D)
                //{
                //    x.Credit = ChequeWiseReceiveSummary(x.Account);
                //}
                // ledger integration for bill (C&F)
                //var d = (from t1 in db.Acc_SupplierBillSlave
                //         join t2 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t2.ID
                //         join t3 in db.Common_BillType on t2.Common_BillTypeFk equals t3.ID
                //         join t4 in db.Common_Supplier on t2.Common_SupplierFk equals t4.ID
                //         join t5 in db.Commercial_Invoice on t1.ChallanOrInvoiceFk equals t5.ID
                //         where t4.ID == supplierId && t1.Active == true && t2.Active == true && t4.ID != 1
                //         && t3.ID == 1 && t1.IsApproved == true
                //         select new VmTransaction
                //         {
                //             ID = t1.ID,
                //             Account = "",
                //             Name = t3.ID.ToString(),
                //             Date = t1.Date,
                //             Description = "Bill Type: " + t3.Name + ". Invoice No: " + t5.InvoiceNo + " Des:" + t1.Particulars + "-",
                //             Credit = t1.Amount,
                //             Debit = 0,
                //             Balance = 0,
                //             FirstCreateDate = t1.FirstCreated.Value
                //         }).Distinct().ToList();
                // ledger integration for bill(Challan)
                //var e = (from t1 in db.Acc_SupplierBillSlave
                //         join t2 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t2.ID
                //         join t3 in db.Common_BillType on t2.Common_BillTypeFk equals t3.ID
                //         join t4 in db.Common_Supplier on t2.Common_SupplierFk equals t4.ID
                //         join t5 in db.Shipment_CNFDelivaryChallan on t1.ChallanOrInvoiceFk equals t5.ID
                //         where t4.ID == supplierId && t1.Active == true && t2.Active == true && t4.ID != 1
                //         && t3.ID == 2 && t1.IsApproved == true
                //         select new VmTransaction
                //         {
                //             ID = t1.ID,
                //             Account = "",
                //             Name = t3.ID.ToString(),
                //             Date = t1.Date,
                //             Description = "Bill Type: " + t3.Name + ". Challan No: " + t5.ChallanNo + " Des:" + t1.Particulars + "-",
                //             Credit = t1.Amount,
                //             Debit = 0,
                //             Balance = 0,
                //             FirstCreateDate = t1.FirstCreated.Value
                //         }).Distinct().ToList();

                //var f = d.Union(e);
                //var totalV1 = v1.Union(C).Union(D).Union(f);
                var totalV = v.Union(v1).OrderBy(x => x.Date).Distinct();

                var previuosBalanceTable = (from t in totalV
                                            where t.Date < FromDate
                                            select
                                            (
                                             //t.Debit - t.Credit
                                             t.Credit - t.Debit
                                            )).ToList();

                var countForId = previuosBalanceTable.Count();

                var previuosBalance = (previuosBalanceTable).DefaultIfEmpty(0).Sum();



                var sortedV = (from t in totalV
                               where t.Date >= FromDate && t.Date <= ToDate
                               select new VmTransaction
                               {
                                   ID = ++countForId,
                                   Account = t.Account.ToString(),
                                   Date = t.Date,
                                   Description = t.Description,
                                   Credit = t.Credit,
                                   Debit = t.Debit,
                                   Balance = 0
                               }).Distinct().ToList();


                var supplier = (from t in db.Common_Supplier
                                where t.ID == supplierId
                                select new VmCommon_Supplier
                                {
                                    Common_Supplier = t
                                }).First();

                vmTransition.Date = FromDate;
                vmTransition.Name = supplier.Common_Supplier.Name;
                vmTransition.Description = "Opening Balance";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = previuosBalance;
                tempList.Add(vmTransition);




                foreach (var x in sortedV)
                {
                    x.Balance = previuosBalance += x.Credit - x.Debit;// x.Debit - x.Credit;
                    x.Name = supplier.Common_Supplier.Name;
                    tempList.Add(x);

                }
            }
            catch { }
            Transaction = tempList;
            BuildLCOpeningLedgerReport();
        }

        public void GetPOWiseSupplierLedgerOpening(int supplierId)
        {
            List<VmTransaction> tempList = new List<VmTransaction>();
            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                var v = (from t1 in db.Mkt_PO
                         join t2 in db.Common_Supplier
                         on t1.Common_SupplierFK equals t2.ID
                         join t3 in db.Mkt_BOM on t1.Mkt_BOMFK equals t3.ID
                         join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                         //join t5 in db.Common_User on t4.FirstCreatedBy equals t5.ID
                         //where t1.Date >= FromDate && t1.Date <= ToDate
                         where t1.Common_SupplierFK == supplierId && t1.IsAuthorize == true && t1.Active == true
                         select new VmTransaction
                         {
                             Account = t1.ID.ToString(),
                             Date = t1.AuthorizeDate.Value,
                             Description = "PO ID : <a target='_New' href='../../marketing/VmMkt_PODetails/" + t1.ID + "'>" + t1.CID + "</a> Date: " + SqlFunctions.DateName("day", t1.Date) + "-" + SqlFunctions.DateName("month", t1.Date) + "-" + SqlFunctions.DateName("year", t1.Date) + "<br /> Order Details : <a target='_New' href='../../Marketing/Mkt_BOMSlaveItem/" + t1.Mkt_BOMFK + "'>" + t4.CID + "/" + t3.Style + "</a>", //+ "Merchandiser :" + t5.Name,
                             Credit = 0,//Debit
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).Distinct().ToList();
                foreach (var x in v)
                {
                    //x.Debit = GetTotalPoValue(x.Account);
                    x.Credit = GetTotalPoValue(x.Account);
                }
                var supplierAdjustment = (from t1 in db.Acc_SupplierAdjustment
                                          join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID                                          
                                          where t2.ID == supplierId && t1.Active == true
                                          select new VmTransaction
                                          {
                                              Account = t1.ID.ToString(),
                                              Date = t1.FirstCreated.Value,
                                              Description = "<span style='color:red;'>Supplier Adjustment :</span> <a target='_New' href='../../Accounts/VmAccSupplierAdjustment/" + t2.ID + "'>" + t1.CID + "</a> Adjustment Date: " + SqlFunctions.DateName("day", t1.Date) + " - " + SqlFunctions.DateName("month", t1.Date) + " - " + SqlFunctions.DateName("year", t1.Date) + " Remarks: "+ t1.Remarks,
                                                                                                                                                                                                                                                                                                                                                                                                                                                   
                                              Credit = 0,
                                              Debit = t1.AdjustmentAmmount,
                                              Balance = 0,
                                              FirstCreateDate = t1.FirstCreated.Value
                                          }).Distinct().ToList();

                var v1 = (from t1 in db.Commercial_B2bLC
                          join t2 in db.Common_Supplier
                          on t1.Common_SupplierFK equals t2.ID
                          //join t3 in db.Commercial_MasterLC on t1.Commercial_MasterLCFK equals t3.ID
                          join t3 in db.Commercial_UDSlave on t1.Commercial_UDFK equals t3.Commercial_UDFK
                          join t4 in db.Commercial_UD on t3.Commercial_UDFK equals t4.ID
                          where t2.ID == supplierId && t1.Active == true && t1.IsApproved == true
                          select new VmTransaction
                          {
                              Account = t1.ID.ToString(),
                              Date = t1.FirstCreated.Value,
                              Description = "BTB L/C : <a target='_New' href='../../commercial/VmCommercial_UDSlaveIndex/" + t4.ID + "'>" + t1.Name + "</a>BTB Date: " + SqlFunctions.DateName("day", t1.Date) + " - " + SqlFunctions.DateName("month", t1.Date) + " - " + SqlFunctions.DateName("year", t1.Date) + "\nUD : <a href='../../commercial/VmCommercial_UDSlaveIndex/" + t4.ID + "' target='_blank'>" + t4.UdNo + "</a>",// + t4.UdNo,//+"\nBank : " + t4.Name,
                              //Debit = 0,
                              //Credit = t1.Amount,
                              Credit = 0,
                              Debit = t1.Amount,
                              Balance = 0,
                              FirstCreateDate = t1.FirstCreated.Value
                          }).Distinct().ToList();

                var C = (from t1 in db.Acc_SupplierPH
                         join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                         join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                         join t4 in db.Acc_AcName on t1.Acc_AcNameFK equals t4.ID
                         join t6 in db.Acc_Chart2 on t4.Acc_Chart2FK equals t6.ID
                         join t7 in db.Acc_Chart1 on t6.Acc_Chart1FK equals t7.ID
                         join t5 in db.Commercial_Bank on t1.Commercial_BankFK equals t5.ID
                         where t1.Common_SupplierFK == supplierId && t1.Active == true && t1.IsApproved == true
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = t1.ID.ToString(),
                             Date = t1.Date,
                             Description = "Acounts Head : " + t7.Name + "/" + t6.Name + "/" + t4.Name + ". Cheque or Voucher no : " + t1.ChequeVoucherNo + ". \nUser Name : " + t3.Name + ". Bank : " + t5.Name + ".\nDesc: " + t1.Description,
                             Credit = 0,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).ToList();
                foreach (var x in C)
                {
                    //x.Credit = ChequeWisePaymentSummary(x.Account);
                    x.Debit = ChequeWisePaymentSummary(x.Account);
                }
                var D = (from t1 in db.Acc_SupplierReceiveH
                         join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                         join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                         join t4 in db.Acc_AcName on t1.Acc_AcNameFK equals t4.ID
                         join t6 in db.Acc_Chart2 on t4.Acc_Chart2FK equals t6.ID
                         join t7 in db.Acc_Chart1 on t6.Acc_Chart1FK equals t7.ID

                         join t5 in db.Commercial_Bank on t1.Commercial_BankFK equals t5.ID
                         where t1.Common_SupplierFK == supplierId && t1.Active == true && t1.IsApproved == true
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = t1.ID.ToString(),
                             Date = t1.Date,
                             Description = "Acounts Head : " + t7.Name + "/" + t6.Name + "/" + t4.Name + ". Cheque or Voucher no : " + t1.ChequeVoucherNo + ". \nUser Name : " + t3.Name + ". Bank : " + t5.Name + ".\nDesc: " + t1.Description,
                             Credit = 0,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).ToList();
                foreach (var x in D)
                {
                    x.Credit = ChequeWiseReceiveSummary(x.Account);
                }
                //ledger integration for bill(C & F)

                var d = (from t1 in db.Acc_SupplierBillSlave
                         join t2 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t2.ID
                         join t3 in db.Common_BillType on t2.Common_BillTypeFk equals t3.ID
                         join t4 in db.Common_Supplier on t2.Common_SupplierFk equals t4.ID
                         join t5 in db.Commercial_Invoice on t1.ChallanOrInvoiceFk equals t5.ID
                         where t4.ID == supplierId && t1.Active == true && t2.Active == true && t4.ID != 1
                         && t3.ID == 1 && t1.IsApproved == true
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = "",
                             Name = t3.ID.ToString(),
                             Date = t1.Date,
                             Description = "Bill Type: " + t3.Name + ". Invoice No: " + t5.InvoiceNo + " Des:" + t1.Particulars + "-",
                             Credit = t1.Amount,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).Distinct().ToList();
                //ledger integration for bill(Challan)

                var e = (from t1 in db.Acc_SupplierBillSlave
                         join t2 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t2.ID
                         join t3 in db.Common_BillType on t2.Common_BillTypeFk equals t3.ID
                         join t4 in db.Common_Supplier on t2.Common_SupplierFk equals t4.ID
                         join t5 in db.Shipment_CNFDelivaryChallan on t1.ChallanOrInvoiceFk equals t5.ID
                         where t4.ID == supplierId && t1.Active == true && t2.Active == true && t4.ID != 1
                         && t3.ID == 2 && t1.IsApproved == true
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = "",
                             Name = t3.ID.ToString(),
                             Date = t1.Date,
                             Description = "Bill Type: " + t3.Name + ". Challan No: " + t5.ChallanNo + " Des:" + t1.Particulars + "-",
                             Credit = t1.Amount,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).Distinct().ToList();

                var f = d.Union(e);
                var totalV1 = v1.Union(C).Union(D).Union(f);
                var totalV = v.Union(supplierAdjustment).Union(totalV1).OrderBy(x => x.Date).Distinct();

                var previuosBalanceTable = (from t in totalV
                                            where t.Date < FromDate
                                            select
                                            (
                                             //t.Debit - t.Credit
                                             t.Credit - t.Debit
                                            )).ToList();

                var countForId = previuosBalanceTable.Count();

                var previuosBalance = (previuosBalanceTable).DefaultIfEmpty(0).Sum();

                var sortedV = (from t in totalV
                               where t.Date >= FromDate && t.Date <= ToDate
                               select new VmTransaction
                               {
                                   ID = ++countForId,
                                   Account = t.Account.ToString(),
                                   Date = t.Date,
                                   Description = t.Description,
                                   Credit = t.Credit,
                                   Debit = t.Debit,
                                   Balance = 0
                               }).Distinct().ToList();


                var supplier = (from t in db.Common_Supplier
                                where t.ID == supplierId
                                select new VmCommon_Supplier
                                {
                                    Common_Supplier = t
                                }).First();

                vmTransition.Date = FromDate;
                vmTransition.Name = supplier.Common_Supplier.Name;
                vmTransition.Description = "Opening Balance";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = previuosBalance;
                tempList.Add(vmTransition);

                foreach (var x in sortedV)
                {
                    x.Balance = previuosBalance += x.Credit - x.Debit;// x.Debit - x.Credit;
                    x.Name = supplier.Common_Supplier.Name;
                    tempList.Add(x);
                }
            }
            catch { }
            Transaction = tempList;
            BuildLCOpeningLedgerReport();
        }



        private void BuildLCOpeningLedgerReport()
        {
            ReportDocType reportDocType;
            LedgerReport = new List<ReportDocType>();
            int sl = 0;
            string bCarriedForward = "0";
            try
            {
                bCarriedForward = Transaction[Transaction.Count() - 1].Balance.ToString("F");
            }
            catch (Exception e)
            {
                bCarriedForward = "0";
            }
            foreach (VmTransaction vmT in Transaction)
            {
                //string newDr = vmT.Debit.ToString("F");
                string newCr = vmT.Credit.ToString("F");
                string newDr = vmT.Debit.ToString("F");

                //if (newDr == "0" || newDr == "0.00")
                //{
                //    newDr = "";
                //}
                if (newCr == "0" || newCr == "0.00")
                {
                    newCr = "";
                }
                if (newDr == "0" || newDr == "0.00")
                {
                    newDr = "";
                }
                reportDocType = new ReportDocType();
                reportDocType.HeaderLeft1 = vmT.Name;
                reportDocType.HeaderLeft2 = "From: " + this.FromDate.ToShortDateString() + " - To: " + this.ToDate.ToShortDateString();
                reportDocType.Body1 = vmT.ID.ToString();
                reportDocType.Body2 = vmT.Date.ToShortDateString();
                reportDocType.Body3 = vmT.Description;
                reportDocType.Body4 = newDr;
                reportDocType.Body5 = newCr;
                reportDocType.Body6 = vmT.Balance.ToString("F");
                reportDocType.SubBody1 = bCarriedForward;
                LedgerReport.Add(reportDocType);

            }
        }
        public decimal GetPoRecevingChallanValue(int ReceivedID, string poID)
        {
            db = new xOssContext();
            var a = (from t1 in db.Mkt_POSlave
                     join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                     join t3 in db.Common_Supplier on t2.Common_SupplierFK equals t3.ID
                     join t4 in db.Mkt_POSlave_Receiving on t1.ID equals t4.Mkt_POSlaveFK
                     where t4.ID == ReceivedID && t4.Active == true && t1.Mkt_POFK.ToString() == poID && t2.Active == true && t1.Active == true
                     select new
                     {
                         Challanvalue = t4.Quantity * t1.Price,

                     }).ToList();
            decimal? ChallanReceving = 0;
            decimal? ChallanItemTotal = 0;

            foreach (var x in a)
            {
                if (x.Challanvalue != null)
                {
                    ChallanReceving = x.Challanvalue;
                    ChallanItemTotal += ChallanReceving;
                }

            }
            return ChallanItemTotal.Value;
        }

        internal void SelectSingle(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Common_Supplier
                     where t1.ID == id
                     select new VmAccounting
                     {
                         Common_Supplier = t1
                     }).FirstOrDefault();
            this.Common_Supplier = v.Common_Supplier;
        }

        //public decimal GetPoRecevingValue(string Poid)
        //{
        //    db = new xOssContext();
        //    var a = (from t1 in db.Mkt_POSlave
        //             join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
        //             join t3 in db.Common_Supplier on t2.Common_SupplierFK equals t3.ID
        //             join t4 in db.Mkt_POSlave_Receiving on t1.ID equals t4.Mkt_POSlaveFK
        //             where t1.Mkt_POFK.ToString() == Poid && t4.Active == true
        //             select new
        //             {
        //                 Challanvalue= t4.Quantity * t1.Price,
        //                 PoslaveReceving = t4.Quantity * t1.Price                        
        //             }).ToList();
        //    decimal? poslaveReceving = 0;
        //    decimal? poslaveRecevingTotal = 0;
        //    foreach (var item in a)
        //    {
        //        poslaveReceving = item.PoslaveReceving;
        //        poslaveRecevingTotal += poslaveReceving;
        //    }
        //    return poslaveRecevingTotal.Value;
        //}
        public decimal ChequeWisePaymentSummary(string sid)
        {
            var v = (from t1 in db.Acc_SupplierPH
                     join t2 in db.Acc_AcName
                     on t1.Acc_AcNameFK equals t2.ID
                     join t3 in db.Commercial_Bank
                     on t1.Commercial_BankFK equals t3.ID
                     where t1.ID.ToString() == sid && t1.Active == true
                     select new { payment = t1.Amount }).ToList();
            decimal? total = 0;
            foreach (var item in v)
            {
                total = item.payment;
            }
            return total.Value;
        }
        public decimal ChequeWiseReceiveSummary(string sid)
        {
            var v = (from t1 in db.Acc_SupplierReceiveH
                     join t2 in db.Acc_AcName
                         on t1.Acc_AcNameFK equals t2.ID
                     join t3 in db.Commercial_Bank
                         on t1.Commercial_BankFK equals t3.ID
                     where t1.ID.ToString() == sid && t1.Active == true
                     select new { payment = t1.Amount }).ToList();
            decimal? total = 0;
            foreach (var item in v)
            {
                total = item.payment;
            }
            return total.Value;
        }
        public decimal GetB2BLCWisePaymentSummary(string id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Commercial_B2BPaymentInformation
                     join t2 in db.Commercial_B2bLC
                     on t1.Commercial_B2bLCFK equals t2.ID
                     join t3 in db.Common_Supplier
                     on t2.Common_SupplierFK equals t3.ID

                     where t1.ID.ToString() == id && t1.Active == true && t2.Active == true
                     select new
                     {
                         Payment = t1.B2bLCPayment
                     }).ToList();
            decimal? payment = 0;

            foreach (var item in a)
            {
                payment = item.Payment;

            }
            decimal? d = payment.Value;
            return d.Value;
        }

        public void DeletedPOList(VmAccounting vm)
        {
            db = new xOssContext();

            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Common_Supplier
                     on t1.Common_SupplierFK equals t2.ID
                     join t3 in db.Mkt_BOM on t1.Mkt_BOMFK equals t3.ID
                     join t4 in db.Common_TheOrder on t3.Common_TheOrderFk equals t4.ID
                     where t1.AuthorizeDate >= FromDate && t1.AuthorizeDate <= ToDate
                     where t1.Common_SupplierFK == vm.Common_Supplier.ID && t1.Active == false
                     select new VmTransaction
                     {
                         Date = t1.AuthorizeDate.Value,
                         Description = "PO ID : <b>" + t1.CID + " </b>Date: <b>" + SqlFunctions.DateName("day", t1.Date) + "-" + SqlFunctions.DateName("month", t1.Date) + "-" + SqlFunctions.DateName("year", t1.Date) + "</b><br /> Order Details : <a target='_New' href='../../Marketing/Mkt_BOMSlaveItem/" + t1.Mkt_BOMFK + "'>" + t4.CID + "/" + t3.Style + "</a>", //+ "Merchandiser :" + t5.Name,

                         FirstCreateDate = t1.FirstCreated.Value
                     }).Distinct().ToList();

            DataList2 = v;
        }

        public void GetSupplierPaymentLedger(int supplierId)
        {
            List<VmTransaction> tempList = new List<VmTransaction>();
            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                var v = (from t1 in db.Mkt_POSlave_Receiving
                         join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                         join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                         join t4 in db.Common_Supplier on t3.Common_SupplierFK equals t4.ID
                         join t5 in db.Mkt_BOM on t3.Mkt_BOMFK equals t5.ID
                         join t6 in db.Common_TheOrder on t5.Common_TheOrderFk equals t6.ID
                         where t4.ID == supplierId && t1.Active == true && t2.Active == true && t3.Active == true
                          && t4.ID != 1 && t4.Active == true && t5.Active == true && t6.Active == true
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = t1.Challan,
                             Name = t3.ID.ToString(),
                             Date = t1.FirstCreated.Value,
                             Description = "Challan & PO: <a target='_New' href='../../marketing/VmMkt_PODetails/" + t3.ID + "'>" + t1.Challan + "</a> Date: " + SqlFunctions.DateName("day", t1.Date) + "-" + SqlFunctions.DateName("month", t1.Date) + "-" + SqlFunctions.DateName("year", t1.Date) + "\nOrder Details : <a target='_New' href='../../Marketing/Mkt_BOMSlaveItem/" + t3.Mkt_BOMFK + "'>" + t6.CID + "/" + t5.Style + "</a>",
                             Credit = 0,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).Distinct().ToList();
                foreach (var x in v)
                {
                    //x.Debit = GetPoRecevingChallanValue(x.Account,x.Name);
                    x.Credit = GetPoRecevingChallanValue(x.ID, x.Name);
                }

                var v1 = (from t1 in db.Commercial_B2bLC
                          join t2 in db.Common_Supplier
                          on t1.Common_SupplierFK equals t2.ID
                          join t3 in db.Commercial_B2BPaymentInformation on t1.ID equals t3.Commercial_B2bLCFK
                          //join t4 in db.Commercial_UDSlave on t1.Commercial_UDFK equals t4.ID
                          join t6 in db.Commercial_UD on t1.Commercial_UDFK equals t6.ID
                          join t5 in db.User_User on t3.FirstCreatedBy equals t5.ID
                          where t2.ID == supplierId && t2.Active == true && t3.Active == true && t1.Active == true && t1.IsApproved && t3.IsApproved
                          select new VmTransaction
                          {
                              ID = t1.ID,
                              Account = t3.ID.ToString(),
                              Date = t3.Date,
                              Description = "BTB L/C: <a target='_New' href='../../commercial/VmCommercial_UDSlaveIndex/" + t1.Commercial_UDFK + "'>" + t1.Name + "</a>BTB Date: " + SqlFunctions.DateName("day", t1.Date) + " - " + SqlFunctions.DateName("month", t1.Date) + " - " + SqlFunctions.DateName("year", t1.Date) + "\nUD : <a href='../../commercial/VmCommercial_UDSlaveIndex/" + t1.Commercial_UDFK + "' target='_blank'>" + t6.UdNo + "</a>",
                              Debit = 0,
                              Credit = 0,
                              Balance = 0,
                              FirstCreateDate = t3.FirstCreated.Value
                          }).ToList();

                foreach (var x in v1)
                {

                    //x.Credit = GetB2BLCWisePaymentSummary(x.Account);
                    x.Debit = GetB2BLCWisePaymentSummary(x.Account);

                }


                var C = (from t1 in db.Acc_SupplierPH
                         join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                         join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                         join t4 in db.Acc_AcName on t1.Acc_AcNameFK equals t4.ID
                         join t6 in db.Acc_Chart2 on t4.Acc_Chart2FK equals t6.ID
                         join t7 in db.Acc_Chart1 on t6.Acc_Chart1FK equals t7.ID

                         join t5 in db.Commercial_Bank on t1.Commercial_BankFK equals t5.ID
                         where t1.Common_SupplierFK == supplierId && t1.Active == true && t2.Active == true && t4.Active == true && t1.IsApproved
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = t1.ID.ToString(),
                             Date = t1.Date,
                             Description = "Acounts Head : " + t7.Name + "/" + t6.Name + "/" + t4.Name + ". Cheque or Voucher no : " + t1.ChequeVoucherNo + ". \nUser Name : " + t3.Name + ". Bank : " + t5.Name + ".\nDesc: " + t1.Description,
                             Credit = 0,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).ToList();
                foreach (var x in C)
                {
                    //x.Credit = ChequeWisePaymentSummary(x.Account);
                    x.Debit = ChequeWisePaymentSummary(x.Account);
                }
                var D = (from t1 in db.Acc_SupplierReceiveH
                         join t2 in db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                         join t3 in db.User_User on t1.FirstCreatedBy equals t3.ID
                         join t4 in db.Acc_AcName on t1.Acc_AcNameFK equals t4.ID
                         join t6 in db.Acc_Chart2 on t4.Acc_Chart2FK equals t6.ID
                         join t7 in db.Acc_Chart1 on t6.Acc_Chart1FK equals t7.ID

                         join t5 in db.Commercial_Bank on t1.Commercial_BankFK equals t5.ID
                         where t1.Common_SupplierFK == supplierId && t1.Active == true && t1.IsApproved == true
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = t1.ID.ToString(),
                             Date = t1.Date,
                             Description = "Acounts Head : " + t7.Name + "/" + t6.Name + "/" + t4.Name + ". Cheque or Voucher no : " + t1.ChequeVoucherNo + ". \nUser Name : " + t3.Name + ". Bank : " + t5.Name + ".\nDesc: " + t1.Description,
                             Credit = 0,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).ToList();
                foreach (var x in D)
                {
                    x.Credit = ChequeWiseReceiveSummary(x.Account);
                }

                // ledger integration for bill (C&F)
                var d = (from t1 in db.Acc_SupplierBillSlave
                         join t2 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t2.ID
                         join t3 in db.Common_BillType on t2.Common_BillTypeFk equals t3.ID
                         join t4 in db.Common_Supplier on t2.Common_SupplierFk equals t4.ID
                         join t5 in db.Commercial_Invoice on t1.ChallanOrInvoiceFk equals t5.ID into ps
                         from p in ps.DefaultIfEmpty()
                         where t4.ID == supplierId && t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true && t4.ID != 1
                         && t3.ID == 1 && t1.IsApproved == true
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = "",
                             Name = t3.ID.ToString(),
                             Date = t1.Date,
                             Description = "Bill Type: " + t3.Name + ". Invoice No: " + (p.InvoiceNo ?? t1.ChallanInvoiceNo) + " Des:" + t1.Particulars,
                             Credit = t1.Amount,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).Distinct().ToList();
                // ledger integration for bill(Challan)
                var e = (from t1 in db.Acc_SupplierBillSlave
                         join t2 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t2.ID
                         join t3 in db.Common_BillType on t2.Common_BillTypeFk equals t3.ID
                         join t4 in db.Common_Supplier on t2.Common_SupplierFk equals t4.ID
                         join t5 in db.Shipment_CNFDelivaryChallan on t1.ChallanOrInvoiceFk equals t5.ID into ps
                         from p in ps.DefaultIfEmpty()
                         where t4.ID == supplierId && t1.Active == true && t2.Active == true && p.Active == true && t4.ID != 1
                         && t3.ID == 2 && t1.IsApproved == true
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = "",
                             Name = t3.ID.ToString(),
                             Date = t1.Date,
                             Description = "Bill Type: " + t3.Name + ". Challan No: " + (p.ChallanNo ?? t1.ChallanInvoiceNo) + " Des:" + t1.Particulars,
                             Credit = t1.Amount,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).Distinct().ToList();
                // ledger integration for bill(Accoun Head)
                var f = (from t1 in db.Acc_SupplierBillSlave
                         join t2 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t2.ID
                         join t3 in db.Common_BillType on t2.Common_BillTypeFk equals t3.ID
                         join t4 in db.Common_Supplier on t2.Common_SupplierFk equals t4.ID
                         join t5 in db.Acc_AcName on t1.ChallanOrInvoiceFk equals t5.ID into ps
                         from p in ps.DefaultIfEmpty()
                         where t4.ID == supplierId && t1.Active == true && t2.Active == true && p.Active == true && t4.ID != 1
                               && t3.ID == 3 && t1.IsApproved == true && p.Active == true
                         select new VmTransaction
                         {
                             ID = t1.ID,
                             Account = "",
                             Name = t3.ID.ToString(),
                             Date = t1.Date,
                             Description = "Bill Type: " + t3.Name + ". Bill No: " + (t1.ChallanInvoiceNo) + ". From:" + p.Name + ". Des:" + t1.Particulars,
                             Credit = t1.Amount,
                             Debit = 0,
                             Balance = 0,
                             FirstCreateDate = t1.FirstCreated.Value
                         }).Distinct().ToList();

                var h = d.Union(e).Union(f);
                var totalP = v1.Union(C).Union(D).Union(h);
                var totalV = v.Union(totalP).OrderBy(x => x.FirstCreateDate).Distinct();

                var previuosBalanceTable = (from t in totalV
                                            where t.Date < FromDate
                                            select
                                            (
                                              t.Credit - t.Debit
                                            )).ToList();


                var countForId = previuosBalanceTable.Count();
                var previuosBalance = (previuosBalanceTable).DefaultIfEmpty(0).Sum();




                var sortedV = (from t in totalV
                               where t.Date >= FromDate && t.Date <= ToDate
                               select new VmTransaction
                               {
                                   ID = ++countForId,
                                   Account = t.Account.ToString(),
                                   Date = t.Date,
                                   Description = t.Description,
                                   Credit = t.Credit,
                                   Debit = t.Debit,
                                   Balance = 0
                               }).ToList();


                var supplier = (from t in db.Common_Supplier
                                where t.ID == supplierId
                                select new VmCommon_Supplier
                                {
                                    Common_Supplier = t
                                }).First();

                vmTransition.Date = FromDate;
                vmTransition.Name = supplier.Common_Supplier.Name;
                vmTransition.Code = "  Payment History";
                vmTransition.Description = "Opening Balance";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = previuosBalance;
                tempList.Add(vmTransition);
                foreach (var x in sortedV)
                {
                    x.Name = supplier.Common_Supplier.Name;
                    x.Code = "  Payment History";
                    x.Balance = previuosBalance += x.Credit - x.Debit;// x.Debit - x.Credit;
                    tempList.Add(x);

                }
            }
            catch { }
            Transaction = tempList;
            BuildPaymentLedgerReport();
        }

        private void BuildPaymentLedgerReport()
        {
            ReportDocType reportDocType;
            LedgerReport = new List<ReportDocType>();
            int sl = 0;
            string bCarriedForward = "0.00";
            if (Transaction.Count > 0)
                bCarriedForward = Transaction[Transaction.Count() - 1].Balance.ToString("F");
            foreach (VmTransaction vmT in Transaction)
            {
                string newCr = vmT.Credit.ToString("F");
                string newDr = vmT.Debit.ToString("F");

                if (newCr == "0" || newCr == "0.00")
                {
                    newCr = "";
                }
                if (newDr == "0" || newDr == "0.00")
                {
                    newDr = "";
                }
                reportDocType = new ReportDocType();
                reportDocType.HeaderLeft1 = vmT.Name + ".  Ledger Type : " + vmT.Code;
                reportDocType.HeaderLeft2 = "From: " + this.FromDate.ToShortDateString() + " - To: " + this.ToDate.ToShortDateString();
                reportDocType.Body2 = vmT.Date.ToShortDateString();
                reportDocType.Body3 = vmT.Description;
                reportDocType.Body4 = newDr;
                reportDocType.Body5 = newCr;
                reportDocType.Body6 = vmT.Balance.ToString("F");
                reportDocType.Body7 = vmT.ID.ToString();
                reportDocType.SubBody1 = bCarriedForward;
                LedgerReport.Add(reportDocType);

            }
        }
        private string GetChallanNo(string poid)
        {
            int pOId = 0;
            int.TryParse(poid, out pOId);
            var db = new xOssContext();
            var v = (from t2 in db.Mkt_PO
                     join t9 in db.Mkt_POSlave
                     on t2.ID equals t9.Mkt_POFK
                     join t10 in db.Mkt_POSlave_Receiving
                     on t9.ID equals t10.Mkt_POSlaveFK
                     where t2.ID == pOId && t2.Active == true && t2.IsAuthorize == true
                     select new { t10.Challan }).Distinct().ToList();
            string chl = "";

            foreach (var s in v)
            {
                chl += s.Challan + ",";
            }
            return chl;
        }

        //----------------------Income Statement---------------------//
        public decimal? CostOfGoodsSold()
        {
            decimal? openingInventory = 0;
            decimal? closingInventory = 0;
            decimal? purchased = 0;
            openingInventory = OpeningInventory().GetValueOrDefault();
            closingInventory = ClosingInventory().GetValueOrDefault();
            purchased = TotalPurchased().GetValueOrDefault();
            return (openingInventory + purchased - closingInventory);
        }
        public decimal? GrossProfit()
        {
            decimal? sales = 0;
            decimal? costOfGoodsSold = 0;
            //sales = TotalSold().GetValueOrDefault();
            costOfGoodsSold = CostOfGoodsSold().GetValueOrDefault();
            return (sales - costOfGoodsSold);
        }
        public decimal? NetProfit()
        {
            decimal? grossProfit = 0;
            decimal? operatingExp = 0;
            grossProfit = GrossProfit().GetValueOrDefault();
            operatingExp = OperatingExp().GetValueOrDefault();
            return (grossProfit - operatingExp);
        }
        public decimal Sold { get; set; }
        public void TotalSold()
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     where t1.Acc_AcNameFK == 29 && t1.Active == true
                     select (t1.Credit)).Sum();
            this.Sold = v;
        }
        public decimal? TotalPurchased()
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     where t1.Acc_AcNameFK == 28 && t1.Active == true
                     select (t1.Debit)).Sum();
            return v;
        }
        public decimal? OpeningInventory()
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     where t1.Acc_AcNameFK == 24 && t1.Active == true
                     select (t1.Debit)).Sum();
            return v;
        }
        public decimal? ClosingInventory()
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_JournalSlave
                     where t1.Acc_AcNameFK == 24 && t1.Active == true
                     select (t1.Credit)).Sum();
            return v;
        }
        public decimal? OperatingExp()
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_Chart1
                     join t2 in db.Acc_Chart2 on t1.ID equals t2.Acc_Chart1FK
                     join t3 in db.Acc_AcName on t2.ID equals t3.Acc_Chart2FK
                     join t4 in db.Acc_JournalSlave on t3.ID equals t4.Acc_AcNameFK
                     where t1.ID == 7 && t4.Active == true
                     select (t4.Debit)).Sum();
            return v;
        }
        public void IncomeStatement(int id)
        {
            List<VmTransaction> tempList = new List<VmTransaction>();
            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                Acc_Type t = new Acc_Type();
                var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
                var v = (from Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                         join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                         join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                         on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                         join Acc_AcName in db.Acc_AcName.AsEnumerable()
                         on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                         join Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                         on Acc_AcName.ID equals Acc_JournalSlave.Acc_AcNameFK
                         join Acc_Journal in db.Acc_Journal.AsEnumerable()
                         on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                         where
                         Acc_JournalSlave.Acc_AcNameFK == id
                         && Acc_Chart1.Active == true
                         && Acc_Chart2.Active == true
                         && Acc_AcName.Active == true
                         && Acc_Journal.Active == true
                         && Acc_Journal.isFinal == true && Acc_JournalSlave.Active == true
                         && Acc_Journal.Date >= FromDate && Acc_Journal.Date <= ToDate
                         select new VmTransaction
                         {
                             DrIncrease = Acc_Type.DrIncrease,
                             Account = Acc_Type.Name + "/" + Acc_Chart1.Name + "/" + Acc_Chart2.Name + "/" + Acc_AcName.Name,
                             Code = Acc_AcName.Code,
                             Date = Acc_Journal.Date,
                             Description = Acc_JournalSlave.Description,
                             Debit = Acc_JournalSlave.Debit,
                             Credit = Acc_JournalSlave.Credit
                         }).OrderBy(x => x.Date).ToList();
                DateTime dateBeforeStart = v[0].Date;
                bool isDrIncrease = v[0].DrIncrease;
                var pDR = (from Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                           join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                           join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                           on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                           join Acc_AcName in db.Acc_AcName.AsEnumerable()
                           on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                           join Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                           on Acc_AcName.ID equals Acc_JournalSlave.Acc_AcNameFK
                           join Acc_Journal in db.Acc_Journal.AsEnumerable()
                           on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                           where
                           Acc_JournalSlave.Acc_AcNameFK == id
                           && Acc_Chart1.Active == true
                           && Acc_Chart2.Active == true
                           && Acc_AcName.Active == true
                           && Acc_Journal.Active == true
                           && Acc_Journal.isFinal == true && Acc_JournalSlave.Active == true
                           && Acc_Journal.Date < FromDate
                           select
                           (
                               Acc_JournalSlave.Debit
                           )).DefaultIfEmpty(0).Sum();

                var pCR = (from Acc_Chart1 in db.Acc_Chart1.AsEnumerable()
                           join t1 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t1.ID
                           join Acc_Chart2 in db.Acc_Chart2.AsEnumerable()
                           on Acc_Chart1.ID equals Acc_Chart2.Acc_Chart1FK
                           join Acc_AcName in db.Acc_AcName.AsEnumerable()
                           on Acc_Chart2.ID equals Acc_AcName.Acc_Chart2FK
                           join Acc_JournalSlave in db.Acc_JournalSlave.AsEnumerable()
                           on Acc_AcName.ID equals Acc_JournalSlave.Acc_AcNameFK
                           join Acc_Journal in db.Acc_Journal.AsEnumerable()
                           on Acc_JournalSlave.Acc_JournalFK equals Acc_Journal.ID
                           where
                           Acc_JournalSlave.Acc_AcNameFK == id
                           && Acc_Chart1.Active == true
                           && Acc_Chart2.Active == true
                           && Acc_AcName.Active == true
                           && Acc_Journal.Active == true
                           && Acc_Journal.isFinal == true && Acc_JournalSlave.Active == true
                           && Acc_Journal.Date < FromDate
                           select
                            (
                                Acc_JournalSlave.Credit
                            )).DefaultIfEmpty(0).Sum();
                //decimal previuosBalance = 0;

                foreach (VmTransaction x in v)
                {
                    x.CostOfGoodsSold = CostOfGoodsSold();
                    x.GrossProfit = GrossProfit();
                    x.NetProfit = NetProfit();
                    tempList.Add(x);
                }

            }
            catch { }
            Transaction = tempList;
            //BuildLedgerReport();
            IncomeStatementReport();
        }


        public void ProfitAndLossAccount()
        {
            List<VmTransaction> tempList = new List<VmTransaction>();
            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                Acc_Type t = new Acc_Type();
                //var listOfAcc_Type = from tbl in t.GetAcc_Type()
                //                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };
                var Revid = 6;
                //var CoGid = 8;
                var v = (from t1 in db.Acc_Chart1
                             //join t2 in listOfAcc_Type on Acc_Chart1.Acc_TypeFK equals t2.ID
                         join t3 in db.Acc_Chart2 on t1.ID equals t3.Acc_Chart1FK
                         join t4 in db.Acc_AcName on t3.ID equals t4.Acc_Chart2FK
                         join t5 in db.Acc_JournalSlave on t4.ID equals t5.Acc_AcNameFK
                         join t6 in db.Acc_Journal on t5.Acc_JournalFK equals t6.ID
                         //join t7 in db.Acc_Expenses_History on t4.ID equals t7.From_Acc_NameFK
                         where t1.ID == Revid && t1.Active == true
                         select new VmTransaction
                         {
                             //Account = t2.ID.ToString(),
                             Name = t1.ID.ToString(),
                             Date = t6.Date,
                             Description = t1.Name,
                             Debit = 0,
                             Credit = 0,
                             Balance = 0
                         }).ToList();
                foreach (var x in v)
                {
                    x.Description = AccountTital(x.Name);
                    //x.Debit= CostofGoodsBalance();
                    x.Credit = RevenueBalance(x.Name);
                    //x.Name2 = AdministrativeExpenses();
                    //x.Name3 = SellingAndDrstributionExpenses();
                    //x.Name4 = OperatingExpenses();
                }
                var GrossProfit = (from a in v
                                   where a.Date < FromDate
                                   select
                                   (
                                    a.Credit - a.Debit
                                   )).DefaultIfEmpty(0).Sum();
                var sortedV = (from a in v
                               where a.Date >= FromDate && a.Date <= ToDate
                               select new VmTransaction
                               {
                                   //Account = a.Account.ToString(),
                                   Date = a.Date,
                                   Description = a.Description,
                                   Debit = a.Debit,
                                   Credit = a.Credit,
                                   Balance = 0,
                                   //Name2 = a.Name2,
                                   //Name3 = a.Name3,
                                   //Name4 = a.Name4,
                                   //Name5 = 0,
                                   //Name6 = 0
                               });

                var C1tital = (from a in db.Acc_Chart1
                                   //where a.ID == 6 
                               select new
                               {
                                   Acc_Chart1 = a
                               }).First();

                vmTransition.Date = FromDate;
                vmTransition.Account = C1tital.Acc_Chart1.Name;
                vmTransition.Code = "";
                vmTransition.Description = "";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = GrossProfit;
                //vmTransition.Name2 = 0;
                //vmTransition.Name3 = 0;
                //vmTransition.Name4 = 0;
                tempList.Add(vmTransition);
                foreach (var x in sortedV)
                {
                    x.Balance = GrossProfit = x.Credit - x.Debit;
                    tempList.Add(x);

                }
            }
            catch { }
            Transaction = tempList;
            BuildIncomeStatementReport();
        }
        public string AccountTital(string C1ID)
        {
            var v = (from t1 in db.Acc_Chart2
                     join t2 in db.Acc_Chart1 on t1.Acc_Chart1FK equals t2.ID
                     where t1.Active == true && t1.Acc_Chart1FK.ToString() == C1ID
                     select new { tital = t1.Name }).ToList();
            string a = "";
            foreach (var item in v)
            {
                a = item.tital;
            }
            return a;
        }
        public decimal RevenueBalance(string C2ID)
        {
            var v = (from t1 in db.Acc_Chart2
                     join t3 in db.Acc_AcName on t1.ID equals t3.Acc_Chart2FK
                     join t4 in db.Acc_JournalSlave on t3.ID equals t4.Acc_AcNameFK
                     where t4.Active == true && t1.Acc_Chart1FK.ToString() == C2ID
                     select new { ammount = t4.Credit }).ToList();
            decimal a = 0;
            foreach (var item in v)
            {
                a += item.ammount;
            }
            return a;
        }
        public List<VmAccounting> GetCofGitem(int id)
        {
            db = new xOssContext();
            var ItemName = (from t1 in db.Acc_Chart2
                            join t2 in db.Acc_AcName on t1.ID equals t2.Acc_Chart2FK
                            where t2.Acc_Chart2FK == id
                            select new VmAccounting
                            {
                                Name = t2.Name
                            }).ToList();
            return ItemName;
        }
        public void GoodsAvailableForSale()
        {
            db = new xOssContext();
            var G = (from t1 in db.Acc_Chart2
                     join t2 in db.Acc_AcName on t1.ID equals t2.Acc_Chart2FK
                     //join t3 in db.Acc_JournalSlave on t2.ID equals t3.Acc_AcNameFK
                     where t1.Acc_Chart1FK == 8
                     select new VmAccounting
                     {
                         Acc_Chart2 = t1,
                         Acc_AcName = t2,
                         //Acc_JournalSlave=t3
                     }).ToList();
            foreach (var x in G)
            {
                x.DataList1 = GetCofGitem(x.Acc_Chart2.ID);
            }
        }
        public void CostofGoodsBalance()
        {
            db = new xOssContext();
            var purchased = (from t1 in db.Acc_Chart2
                             join t3 in db.Acc_AcName on t1.ID equals t3.Acc_Chart2FK
                             join t4 in db.Acc_JournalSlave on t3.ID equals t4.Acc_AcNameFK
                             where t4.Active == true && t1.Acc_Chart1FK == 8
                             select (t4.Debit)).Sum();
            var openingInventory = (from t1 in db.Acc_Chart2
                                    join t3 in db.Acc_AcName on t1.ID equals t3.Acc_Chart2FK
                                    join t4 in db.Acc_JournalSlave on t3.ID equals t4.Acc_AcNameFK
                                    where t4.Active == true && t1.Acc_Chart1FK == 1
                                    select (t4.Debit)).Sum();
            var closingInventory = (from t1 in db.Acc_Chart2
                                    join t3 in db.Acc_AcName on t1.ID equals t3.Acc_Chart2FK
                                    join t4 in db.Acc_JournalSlave on t3.ID equals t4.Acc_AcNameFK
                                    where t4.Active == true && t1.Acc_Chart1FK == 1
                                    select (t4.Credit)).Sum();
            this.CostofGoods = (purchased + openingInventory - closingInventory);
            //return (purchased+ openingInventory - closingInventory);
        }
        public decimal AdministrativeExpenses()
        {
            var Total = (from t1 in db.Acc_Chart2
                         join t3 in db.Acc_AcName on t1.ID equals t3.Acc_Chart2FK
                         join t4 in db.Acc_JournalSlave on t3.ID equals t4.Acc_AcNameFK
                         where t4.Active == true && t1.ID == 23
                         select (t4.Debit)).Sum();
            return Total;
        }
        public decimal SellingAndDrstributionExpenses()
        {
            var Total = (from t1 in db.Acc_Chart2
                         join t3 in db.Acc_AcName on t1.ID equals t3.Acc_Chart2FK
                         join t4 in db.Acc_JournalSlave on t3.ID equals t4.Acc_AcNameFK
                         where t4.Active == true && t1.ID == 24
                         select (t4.Debit)).Sum();
            return Total;
        }
        public decimal OperatingExpenses()
        {
            decimal a = 0;
            decimal b = 0;
            a = AdministrativeExpenses();
            b = SellingAndDrstributionExpenses();
            return (a + b);
        }

        //--------------------------Balance Sheet--------------------------//

        public void BalanceSheet()
        {
            List<VmTransaction> tempList = new List<VmTransaction>();
            try
            {
                VmTransaction vmTransition = new VmTransaction();
                db = new xOssContext();
                Acc_Type t = new Acc_Type();
                var listOfAcc_Type = from tbl in t.GetAcc_Type()
                                     select new Acc_Type { ID = tbl.ID, Name = tbl.Name, DrIncrease = tbl.DrIncrease };

                var v = (from t1 in db.Acc_Expenses_History.AsEnumerable()
                         join t2 in db.Acc_AcName.AsEnumerable() on t1.To_Acc_NameFK equals t2.ID
                         join t3 in db.Acc_Chart2.AsEnumerable() on t2.Acc_Chart2FK equals t3.ID
                         join t4 in db.Acc_Chart1.AsEnumerable() on t3.Acc_Chart1FK equals t4.ID
                         join t5 in listOfAcc_Type on t4.Acc_TypeFK equals t5.ID
                         where t2.Active == true
                        //&& t1.To_Acc_NameFK == id

                        && t1.Active == true
                        //&& t1.IsIncome==true
                        && t1.Date >= FromDate && t1.Date <= ToDate
                         select new VmTransaction
                         {
                             DrIncrease = t5.DrIncrease,
                             Account = t2.Name,
                             Code = t2.ID.ToString(),
                             Date = t1.Date,
                             Description = t2.Name,
                             Debit = 0,
                             Credit = 0,
                             Balance = 0
                         }).ToList().GroupBy(x => x.Description).Select(x => x.First());
                foreach (var item in v)
                {
                    item.Credit = Asset(item.Code);
                }
                var sortedV = (from a in v
                               where a.Date >= FromDate && a.Date <= ToDate
                               select new VmTransaction
                               {
                                   Account = a.Account.ToString(),
                                   Date = a.Date,
                                   Description = a.Description,
                                   Debit = a.Debit,
                                   Credit = a.Credit,
                                   Balance = 0,

                               });
                decimal total = 0;
                //var C1tital = (from a in db.Acc_Chart1
                //               where a.ID == 1
                //               select new
                //               {
                //                   Acc_Chart1 = a
                //               }).First();

                vmTransition.Date = FromDate;
                vmTransition.Account = "";
                vmTransition.Code = "";
                vmTransition.Description = "";
                vmTransition.Debit = 0;
                vmTransition.Credit = 0;
                vmTransition.Balance = total;

                tempList.Add(vmTransition);

                foreach (var x in sortedV)
                {
                    total += x.Credit;
                    tempList.Add(x);

                }
            }
            catch { }
            Transaction = tempList;
            BuildIncomeStatementReport();
        }

        public decimal Asset(string id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Acc_Expenses_History
                     join t2 in db.Acc_AcName on t1.To_Acc_NameFK equals t2.ID
                     join t3 in db.Acc_Chart2 on t2.Acc_Chart2FK equals t3.ID
                     join t4 in db.Acc_Chart1 on t3.Acc_Chart1FK equals t4.ID
                     where t4.Acc_TypeFK == 1
                     select new
                     {
                         Acc_Expenses_History = t1,
                         Acc_AcName = t2,
                         Acc_Chart2 = t3,
                         Acc_Chart1 = t4
                     }).ToList();
            foreach (var x in v)
            {

            }
            return 0;
        }

        //-------------------------------IncomeStatement HTML---------------------------//

        public List<VmAcc_Chart1> Chart1 { get; set; }
        public void IncomeStatementHTML()
        {
            db = new xOssContext();
            var Revid = 6;
            var v = (from t1 in db.Acc_Chart1
                     join t2 in db.Acc_Chart2 on t1.ID equals t2.Acc_Chart1FK
                     join t3 in db.Acc_AcName on t2.ID equals t3.Acc_Chart2FK
                     join t4 in db.Acc_JournalSlave on t3.ID equals t4.Acc_AcNameFK
                     join t5 in db.Acc_Journal on t4.Acc_JournalFK equals t5.ID
                     where t1.Active == true
                     select new VmAccounting
                     {
                         Acc_Chart1 = t1,
                         Acc_Chart2 = t2,
                         Acc_AcName = t3,
                         Acc_JournalSlave = t4,
                         Acc_Journal = t5,
                         Name = t1.ID.ToString(),
                         Name2 = t2.ID.ToString(),
                         Date = t5.Date,
                         Description = t1.Name,
                         Description1 = t2.Name,
                         Debit = 0,
                         Credit = 0,
                         Balance = 0
                     }).OrderBy(x => x.Acc_Chart1.ID).AsEnumerable().ToList();

            foreach (var x in v)
            {
                x.Description = AccountTital(x.Name);
                foreach (var a in x.Description)
                {
                    x.Description1 = AccountTital(x.Name2);
                }
                //x.Debit = CostofGoodsBalance();
                //x.Credit = RevenueBalance(x.Name);
                //x.Name2 = AdministrativeExpenses();
                //x.Name3 = SellingAndDrstributionExpenses();
                //x.Name4 = OperatingExpenses();

            }

            foreach (var y in v)
            {
                //y.Debit = CostofGoodsBalance();
                y.Credit = RevenueBalance(y.Name);
            }
            this.DataList1 = v;

        }
        public class Income
        {
            public string Acc_Head { get; set; }

            public decimal? TotalRequired { get; set; }
            public decimal? Quantity { get; set; }
            public string Challan { get; set; }
            public string UnitName { get; set; }
        }
        public List<Income> IncomeList { get; set; }
        public void PoStockReport(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_POSlave on t1.ID equals t2.Mkt_POFK
                     join t3 in db.Mkt_POSlave_Receiving on t2.ID equals t3.Mkt_POSlaveFK
                     join t4 in db.Common_Unit on t2.Common_UnitFK equals t4.ID
                     join t5 in db.Raw_Item on t2.Raw_ItemFK equals t5.ID
                     where t1.ID == id && t3.Quantity != 0
                     select new Income
                     {
                         Acc_Head = t5.Name,
                         TotalRequired = SellingAndDrstributionExpenses(),
                         Quantity = t3.Quantity,
                         Challan = t3.Challan,
                         UnitName = t4.Name
                     }).ToList();
            this.IncomeList = v;

        }
    }
}