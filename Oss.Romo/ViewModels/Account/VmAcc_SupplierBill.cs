﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Common;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_SupplierBill
    {
        private xOssContext db;
        public Acc_SupplierBill Acc_SupplierBill { get; set; }
        public Acc_SupplierBillSlave Acc_SupplierBillSlave { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Common_SupplierType Common_SupplierType { get; set; }
        public Common_BillType Common_BillType { get; set; }
        public IEnumerable<VmAcc_SupplierBill> DataList { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();

            var a = (from t1 in db.Acc_SupplierBill
                     join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     join t3 in db.Common_BillType on t1.Common_BillTypeFk equals t3.ID
                     select new VmAcc_SupplierBill
                     {
                         Acc_SupplierBill = t1,
                         Common_Supplier = t2,
                         Common_BillType = t3
                     }).Where(x => x.Acc_SupplierBill.Active == true).OrderByDescending(x => x.Acc_SupplierBill.Date).AsEnumerable();

            this.DataList = a;
        }
        public void SelectSingleAcc_SupplierBill(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Acc_SupplierBill
                     join t2 in db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     join t3 in db.Common_BillType on t1.Common_BillTypeFk equals t3.ID
                     select new VmAcc_SupplierBill
                     {
                         Acc_SupplierBill = t1,
                         Common_Supplier = t2,
                         Common_BillType = t3
                     }).SingleOrDefault(x => x.Acc_SupplierBill.ID == id);

            this.Acc_SupplierBill = a.Acc_SupplierBill;
        }
        public int Add(int userID)
        {
            this.Acc_SupplierBill.AddReady(userID);
            return this.Acc_SupplierBill.Add();
        }
        public bool Edit(int userID)
        {
            return this.Acc_SupplierBill.Edit(userID);
        }
    }
}