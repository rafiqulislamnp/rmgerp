﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.ViewModels.Commercial;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_AccExportRealisationExpense
    {
        private xOssContext db;
        public Acc_ExportRealisationExpense Acc_ExportRealisationExpense { get; set; }
        public Commercial_UD Commercial_UD { get; set; }
        public Acc_ExportRealisationIndexView SingleRealisationData { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
        public Acc_ExportRealisationExpenseIndexView SingleData { get; set; }
        [DisplayName("BTB Value")]
        public decimal B2BValueDecimal { get; set; }
        public List<Acc_ExportRealisationExpenseIndexView> DataList { get; set; }

        public void InitialDataLoad(int id)
        {
            db = new xOssContext();
            var c = (from t1 in db.Acc_ExportRealisationExpense.AsEnumerable()
                     join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID
                     where t1.Active == true && t1.Acc_ExportRealisationFK == id
                     select new Acc_ExportRealisationExpenseIndexView
                     {
                         Acc_ExportRealisationExpense = t1,
                         Acc_AcName = t2
                     }).OrderBy(x => x.Acc_ExportRealisationExpense.ID);

            this.DataList = c.ToList();
        }
        public void SingleDataLoad(int id)
        {
            db = new xOssContext();
            var c = (from t1 in db.Acc_ExportRealisationExpense.AsEnumerable()
                     join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID
                     where t1.Active == true && t1.ID == id
                     select new Acc_ExportRealisationExpenseIndexView
                     {
                         Acc_ExportRealisationExpense = t1,
                         Acc_AcName = t2
                     }).OrderByDescending(x => x.Acc_ExportRealisationExpense.ID);

            this.SingleData = c.Single();
        }
        public void SingleRealisationDataLoad(int id)
        {
            //db = new xOssContext();
            //var c = (from t1 in db.Acc_ExportRealisation.AsEnumerable()
            //         join t2 in db.Commercial_UD on t1.Commercial_UDFK equals t2.ID
            //         join t3 in db.Commercial_Bank on t1.Commercial_BankFK equals t3.ID
            //         where t1.Active == true && t1.ID == id
            //         select new Acc_ExportRealisationIndexView
            //         {
            //             Acc_ExportRealisation = t1,
            //             Commercial_UD = t2,
            //             Commercial_Bank = t3
            //         }).OrderByDescending(x => x.Acc_ExportRealisation.ID);

            //this.SingleRealisationData = c.Single();
        }
        public void BtbLcValueByMasterLc(int id)
        {
            db = new xOssContext();

            var c = (from Commercial_B2bLC in db.Commercial_B2bLC
                join commercial_UD in db.Commercial_UD
                    on Commercial_B2bLC.Commercial_UDFK equals commercial_UD.ID
                join Common_Supplier in db.Common_Supplier
                    on Commercial_B2bLC.Common_SupplierFK equals Common_Supplier.ID
                join Commercial_LCOregin in db.Commercial_LCOregin
                    on Commercial_B2bLC.Commercial_LCOreginFK equals Commercial_LCOregin.ID
                join Commercial_BtBLCType in db.Commercial_BtBLCType
                    on Commercial_B2bLC.Commercial_BtBLCTypeFK equals Commercial_BtBLCType.ID
                join Commercial_BTBItem in db.Commercial_BTBItem
                    on Commercial_B2bLC.Commercial_BTBItemFK equals Commercial_BTBItem.ID

                where Commercial_B2bLC.Commercial_UDFK == id
                      && commercial_UD.Active == true
                      && Common_Supplier.Active == true
                      && Commercial_LCOregin.Active == true
                      && Commercial_BtBLCType.Active == true
                      && Commercial_BTBItem.Active == true

                select new VmCommercial_B2bLC
                {
                    Common_Supplier = Common_Supplier,
                    Commercial_UD = commercial_UD,
                    Commercial_LCOregin = Commercial_LCOregin,
                    Commercial_BtBLCType = Commercial_BtBLCType,
                    Commercial_B2bLC = Commercial_B2bLC,
                    Commercial_BTBItem = Commercial_BTBItem
                    // Amendment = Commercial_B2bLC.Amount - Commercial_B2bLC.AmendmentValue
                }).Where(x => x.Commercial_B2bLC.Active == true).OrderByDescending(x => x.Commercial_B2bLC.ID).OrderByDescending(x => x.Commercial_BTBItem.Name).AsEnumerable();


            //var c = (from t1 in db.Commercial_B2bLC.AsEnumerable()
            //         join t2 in db.Commercial_UD on t1.Commercial_UDFK equals t2.ID
            //         //join t3 in db.Commercial_UDSlave on t2.ID equals t3.Commercial_UDFK
            //         //join t4 in db.Commercial_UD on t3.Commercial_MasterLCFK equals t4.ID
            //         where t1.Active == true && t2.ID == id
            //         group new { t1, t2 } by new
            //         {
            //             t2.ID
            //         } into g
            //         select new
            //         {
            //             Id = g.Key.ID,
            //             Total = g.Sum(p => p.t1.Amount)
            //         }).Single();

            B2BValueDecimal = c.ToList().Sum(x=>x.Commercial_B2bLC.Amount);
        }
        public int Add(int userID)
        {
            Acc_ExportRealisationExpense.AddReady(userID);


            // Acc_ExportRealisationExpense.Add(); not work that's why i use here db.savechages();
            db = new xOssContext();
            db.Acc_ExportRealisationExpense.Add(Acc_ExportRealisationExpense);
            return db.SaveChanges();

            //return Acc_ExportRealisationExpense.Add();
        }
        public int Edit(int userID)
        {
            Acc_ExportRealisationExpense.LastEdited = DateTime.Now;
            db = new xOssContext();
            db.Entry(Acc_ExportRealisationExpense).State = EntityState.Modified;
            return db.SaveChanges();
            //return Acc_ExportRealisationExpense.Edit(userID);
        }
        public int Delete(int userID)
        {
            Acc_ExportRealisationExpense.Active = false;
            db = new xOssContext();
            db.Entry(Acc_ExportRealisationExpense).State = EntityState.Modified;
            return db.SaveChanges();
            // return Acc_ExportRealisationExpense.Edit(userID);
        }
    }
    public class Acc_ExportRealisationExpenseIndexView
    {
        public Acc_ExportRealisationExpense Acc_ExportRealisationExpense { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
    }
}