﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Shipment;
using Oss.Romo.Views.Reports;

namespace Oss.Romo.ViewModels.Account
{
    public class VmAcc_SupplierBillSlave
    {
        private xOssContext db;
        public Acc_SupplierBill Acc_SupplierBill { get; set; }
        public Shipment_CNFDelivaryChallan Shipment_CNFDelivaryChallan { get; set; }
        public Acc_SupplierBillSlave Acc_SupplierBillSlave { get; set; }
        public Common_Supplier Common_Supplier { get; set; }
        public Commercial_Invoice Commercial_Invoice { get; set; }
        public Common_SupplierType Common_SupplierType { get; set; }
        public Common_BillType Common_BillType { get; set; }
        public Common_Currency Common_Currency { get; set; }
        public VmAcc_SupplierBillSlave vmAcc_SupplierBillSlave { get; set; }
        public Acc_AcName Acc_AcName { get; set; }
        public IEnumerable<VmAcc_SupplierBillSlave> DataList { get; set; }
        public int SupplierBillId { get; set; }

        public void InitialDataLoad()
        {
            db = new xOssContext();

            var v = (from t2 in db.Acc_SupplierBill
                     join t3 in db.Common_Supplier on t2.Common_SupplierFk equals t3.ID
                     join t4 in db.Common_BillType on t2.Common_BillTypeFk equals t4.ID
                     select new VmAcc_SupplierBillSlave
                     {
                         Acc_SupplierBill = t2,
                         Common_Supplier = t3,
                         Common_BillType = t4
                     }).SingleOrDefault(c => c.Acc_SupplierBill.ID == SupplierBillId);




            if (v.Acc_SupplierBill.Common_BillTypeFk == 1)
            {
                var a = (from t1 in db.Acc_SupplierBillSlave
                         join t2 in db.Commercial_Invoice on t1.ChallanOrInvoiceFk equals t2.ID into ps
                         from p in ps.DefaultIfEmpty()
                         join t3 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t3.ID
                         join t4 in db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                         select new VmAcc_SupplierBillSlave
                         {
                             Acc_SupplierBillSlave = t1,
                             Acc_SupplierBill = t3,
                             Common_Currency = t4,
                             Commercial_Invoice = p
                         }).Where(x => x.Acc_SupplierBill.Active == true && x.Acc_SupplierBillSlave.Active == true && x.Acc_SupplierBill.ID == SupplierBillId)
                    .OrderByDescending(x => x.Acc_SupplierBillSlave.Date).AsEnumerable();

                this.DataList = a;
            }
            else if (v.Acc_SupplierBill.Common_BillTypeFk == 2)
            {
                var a = (from t1 in db.Acc_SupplierBillSlave
                         join t2 in db.Shipment_CNFDelivaryChallan on t1.ChallanOrInvoiceFk equals t2.ID into ps
                         from p in ps.DefaultIfEmpty()
                         join t3 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t3.ID
                         join t4 in db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                         select new VmAcc_SupplierBillSlave
                         {
                             Acc_SupplierBillSlave = t1,
                             Acc_SupplierBill = t3,
                             Common_Currency = t4,
                             Shipment_CNFDelivaryChallan = p
                         }).Where(x => x.Acc_SupplierBill.Active == true && x.Acc_SupplierBillSlave.Active == true && x.Acc_SupplierBill.ID == SupplierBillId)
                    .OrderByDescending(x => x.Acc_SupplierBillSlave.Date).AsEnumerable();

                this.DataList = a;
            }
            else if (v.Acc_SupplierBill.Common_BillTypeFk == 3)
            {
                var a = (from t1 in db.Acc_SupplierBillSlave
                         join t2 in db.Acc_AcName on t1.ChallanOrInvoiceFk equals t2.ID into ps
                         from p in ps.DefaultIfEmpty()
                         join t3 in db.Acc_SupplierBill on t1.Acc_SupplierBillFk equals t3.ID
                         join t4 in db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                         select new VmAcc_SupplierBillSlave
                         {
                             Acc_SupplierBillSlave = t1,
                             Acc_SupplierBill = t3,
                             Common_Currency = t4,
                             Acc_AcName = p
                         }).Where(x => x.Acc_SupplierBill.Active == true && x.Acc_SupplierBillSlave.Active == true && x.Acc_SupplierBill.ID == SupplierBillId)
                    .OrderByDescending(x => x.Acc_SupplierBillSlave.Date).AsEnumerable();

                this.DataList = a;
            }

            this.Acc_SupplierBill = v.Acc_SupplierBill;
            this.Common_Supplier = v.Common_Supplier;
            this.Common_BillType = v.Common_BillType;

        }
        public void SelectSingleAcc_SupplierBill()
        {
            db = new xOssContext();
            var v = (from t2 in db.Acc_SupplierBill
                     join t3 in db.Common_Supplier on t2.Common_SupplierFk equals t3.ID
                     join t4 in db.Common_BillType on t2.Common_BillTypeFk equals t4.ID
                     select new VmAcc_SupplierBillSlave
                     {
                         Acc_SupplierBill = t2,
                         Common_Supplier = t3,
                         Common_BillType = t4
                     }).SingleOrDefault(c => c.Acc_SupplierBill.ID == SupplierBillId);

            this.Acc_SupplierBill = v.Acc_SupplierBill;
            this.Common_Supplier = v.Common_Supplier;
            this.Common_BillType = v.Common_BillType;
        }
        public void SelectSingleAcc_SupplierBillSlave(int id)
        {
            db = new xOssContext();
            var v = (from t2 in db.Acc_SupplierBill
                     join t3 in db.Common_Supplier on t2.Common_SupplierFk equals t3.ID
                     join t4 in db.Common_BillType on t2.Common_BillTypeFk equals t4.ID
                     select new VmAcc_SupplierBillSlave
                     {
                         Acc_SupplierBill = t2,
                         Common_Supplier = t3,
                         Common_BillType = t4
                     }).SingleOrDefault(c => c.Acc_SupplierBill.ID == id);

            this.Acc_SupplierBill = v.Acc_SupplierBill;
            this.Common_Supplier = v.Common_Supplier;
            this.Common_BillType = v.Common_BillType;
        }


        public int Add(int userID)
        {
            Acc_SupplierBillSlave.AddReady(userID);
            return Acc_SupplierBillSlave.Add();

        }
        public bool Edit(int userID)
        {
            return Acc_SupplierBillSlave.Edit(userID);
        }
    }
}