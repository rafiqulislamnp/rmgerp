﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.ViewModels;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Home;
using Oss.Romo.ViewModels.MIS;
using Oss.Romo.ViewModels.MisDashboard;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.ViewModels.Store;
using Oss.Romo.ViewModels.User;

namespace Oss.Romo.Controllers
{
    public class MisController : Controller
    {
        private xOssContext db;
        private VmCommercial_Report VmCommercial_Report;
        private VmMis_AdminBoard VmMis_AdminBoard;
        private VmForDropDown VmForDropDown;
        private VmAcc_AcName VmAcc_AcName;
        private VmAcc_Transaction VmAcc_Transaction;
        private VmAcc_IntegrationJournal VmAcc_IntegrationJournal;
        private VmAcc_ChequeIntegration VmAcc_ChequeIntegration;
        private Vm_AtaGlance Vm_AtaGlance;
        private Vm_SupplierPayable Vm_SupplierPayable;
        private Vm_BuyerReceivable Vm_BuyerReceivable;
        private VmProductionReport Vmproductionreport;

        // GET: MisDashboard
        public MisController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();

        }

        #region MIS Dashboard 1
        // GET: MIS
        public async Task<ActionResult> Mis1()

        {
            var u = (VmUser_User)Session["One"];

            VmNotifier x = new VmNotifier();
            x.InitialDataLoad();
            x.VmCommercial_UD = new VmCommercial_UD();

            Vm_AtaGlance = new Vm_AtaGlance();
            await Task.Run(() => Vm_AtaGlance.GetAtaGlanceList());
            ViewBag.AtaglanceList = Vm_AtaGlance.AtaGlanceDataList;
            //await Task.Run(() => Vm_AtaGlance.GetAtaGlanceList2());
            //ViewBag.AtaglanceList2 = Vm_AtaGlance.AtaGlanceDataList2;

            Vm_SupplierPayable = new Vm_SupplierPayable();
            await Task.Run(() => Vm_SupplierPayable.GetSupplierPayableList());
            ViewBag.SupplierPayableList = Vm_SupplierPayable.SupplierPayableDataList;

            Vm_BuyerReceivable = new Vm_BuyerReceivable();
            await Task.Run(() => Vm_BuyerReceivable.GetBuyerReceivableList());
            ViewBag.BuyerReceivableList = Vm_BuyerReceivable.BuyerReceivableDataList;

            VmMis_OrderStatus vmMisOrderStatus = new VmMis_OrderStatus();
            await Task.Run(() => vmMisOrderStatus.GetOrderStatus());
            ViewBag.OrderStatusDataList = vmMisOrderStatus.OrderStatusDataList;

            return View(x);
        }

        public async Task<ActionResult> VmAcc_AccNotifierSeen(int id, string type)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            db = new xOssContext();
            Acc_Notifier accNotifier = db.Acc_Notifier.Find(id);
            accNotifier.IsSeen = true;

            this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
            {
                UserId = user.User_User.ID,
                Acc_Notifier = accNotifier
            };
            // update status
            await Task.Run(() => this.VmAcc_ChequeIntegration.EditCheque(user.User_User.ID));

            return RedirectToAction("Mis1", new { type = type });
        }

        #endregion

        #region MIS Dashboard 2

        public async Task<ActionResult> Mis2()
        {
            VmMis_AdminBoard = new VmMis_AdminBoard();
            VmMis_AdminBoard.VmAcc_AcName = new VmAcc_AcName();
            await Task.Run(() => VmMis_AdminBoard.VmAcc_AcName.AccountTypeWiseInventory());

            VmMis_AdminBoard.VmStoreInventory = new VmStoreInventory();

            await Task.Run(() => VmMis_AdminBoard.VmStoreInventory.CategoryWiseInventory());

            await Task.Run(() => VmMis_AdminBoard.VmStoreInventory.GetClosingInventorys());
            return View(VmMis_AdminBoard);
        }

        #endregion

        #region MIS Dashboard 3

        public async Task<ActionResult> Mis3()
        {
            VmMis_AdminBoard = new VmMis_AdminBoard();

            await Task.Run(() => VmMis_AdminBoard.SupplierPayble());
            ViewBag.SupplierList = VmMis_AdminBoard.SupplierInfoChartList;


            await Task.Run(() => VmMis_AdminBoard.BuyerChart());
            ViewBag.BuyerList = VmMis_AdminBoard.BuyerInfoChartList;
            return View(VmMis_AdminBoard);
        }

        #endregion

        #region MIS Dashboard 4

        public async Task<ActionResult> Mis4()
        {
            VmMis_AdminBoard = new VmMis_AdminBoard();

            await Task.Run(() => VmMis_AdminBoard.SupplierPayble());
            ViewBag.SupplierList = VmMis_AdminBoard.SupplierInfoChartList;


            await Task.Run(() => VmMis_AdminBoard.BuyerChart());
            ViewBag.BuyerList = VmMis_AdminBoard.BuyerInfoChartList;


            await Task.Run(() => VmMis_AdminBoard.GetFactoryOverview(DateTime.Now));
            ViewBag.FactoryOverview = VmMis_AdminBoard.FactoryOverview;

            await Task.Run(() => VmMis_AdminBoard.GetBusinessCashFlow(DateTime.Now));
            ViewBag.BusinessCashFlow = VmMis_AdminBoard.BusinessCashFlow;

            VmMis_AdminBoard.GetCollectionDueReceivableAmountLast12Months(DateTime.Now);
            ViewBag.CollectionDueReceivableAmount12Months = VmMis_AdminBoard.CollectionDueReceivableAmount;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMkt_BuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BuyerName = sl1;

            await Task.Run(() => VmMis_AdminBoard.GetAllOrder(0));
            await Task.Run(() => VmMis_AdminBoard.GetPlan_OrderStepProgres());

            await Task.Run(() => VmMis_AdminBoard.GetSummaryReport());
            VmAcc_SupplierPH VmAcc_SupplierPH = new VmAcc_SupplierPH();
            SumofPayableDueSupplier supplier = await Task.Run(() => VmAcc_SupplierPH.SumOfDueAndPayableForSupplier());
            ViewBag.SupplierData = supplier;
            return View(VmMis_AdminBoard);
        }

        #endregion


        #region MIS Dashboard 5

        [HttpGet]
        public async Task<ActionResult> Mis5()
        {
            Vmproductionreport = new VmProductionReport();
            DateTime dt = DateTime.Today.AddDays(-1);
            xOssContext db = new xOssContext();
            var data = (from o in db.Prod_Reference
                        where (o.FromDate == dt)
                        select new
                        {
                            o
                        }).ToList();
            if (data.Any())
            {
                await Task.Run(() => Vmproductionreport.GetTargetAchivementReportOne(data.FirstOrDefault().o.ID));
                await Task.Run(() => Vmproductionreport.GetProductionOrderSummary(data.FirstOrDefault().o.ID));

            }

            return View(Vmproductionreport);
        }

        [HttpPost]
        public async Task<ActionResult> Mis5(VmProductionReport Vmproductionreport)
        {
            xOssContext db = new xOssContext();
            var data = (from o in db.Prod_Reference
                        where (o.FromDate == Vmproductionreport.FromDate)
                        select new
                        {
                            o
                        }).ToList();
            if (data.Any())
            {
                if (Vmproductionreport.Searchby == 1)
                {
                    await Task.Run(() => Vmproductionreport.GetTargetAchivementReportOne(data.FirstOrDefault().o.ID));
                }
                else if (Vmproductionreport.Searchby == 2)
                {
                    await Task.Run(() => Vmproductionreport.GetProductionOrderSummary(data.FirstOrDefault().o.ID));
                }
                else if (Vmproductionreport.Searchby == 3)
                {

                    await Task.Run(() =>
                        Vmproductionreport.GetTargetAchivementReportOne(data.FirstOrDefault().o.ID));
                    await Task.Run(() =>
                        Vmproductionreport.GetProductionOrderSummary(data.FirstOrDefault().o.ID));
                }

            }
            return View(Vmproductionreport);
        }
        #endregion
    }
}