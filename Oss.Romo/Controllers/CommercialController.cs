﻿using CrystalDecisions.CrystalReports.Engine;
using Oss.Romo.Models;
using Oss.Romo.Models.Commercial;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Home;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Reports;
using Oss.Romo.ViewModels.Store;
using Oss.Romo.ViewModels.User;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Oss.Romo.Models.Accounts;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.MisDashboard;


namespace Oss.Romo.Controllers
{
    public class CommercialController : Controller
    {
        // GET: Commercial
        private xOssContext db = new xOssContext();
        //VmCommon_TheOrder VmCommon_TheOrder;
        VmCommercial_PI VmCommercial_PI;
        VmCommercial_MasterLC VmCommercial_MasterLC;
        VmCommercial_B2bLC VmCommercial_B2bLC;
        VmCommercial_UD VmCommercial_UD;
        VmCommercial_UDSlave VmCommercial_UDSlave;
        VmCommercial_MLcSupplierPO VmCommercial_MLcSupplierPO;
        VmCommercial_MLcSupplierPI VmCommercial_MLcSupplierPI;
        VmCommercial_MLcSupplierB2BLc VmCommercial_MLcSupplierB2BLc;
        VmMktPO_Slave VmMktPO_Slave;
        VmCommercial__MasterLCDocs VmCommercial__MasterLCDocs;
        VmCommercial_MLcBuyerDocs VmCommercial_MLcBuyerDocs;

        VmCommercial_PISupplierPO VmCommercial_PISupplierPO;
        VmCommercial_B2bLcSupplierPO VmCommercial_B2bLcSupplierPO;
        VmCommercial_B2BLcSupplierPI VmCommercial_B2BLcSupplierPI;
        VmCommercial_MasterLCBuyerPI VmCommercial_MasterLCBuyerPI;
        VmCommercial_MasterLCBuyerPO VmCommercial_MasterLCBuyerPO;
        VmCommercial_MLCRealization VmCommercial_MLCRealization;

        VmCommercial_Bank VmCommercial_Bank;
        VmCommercial_Import VmCommercial_Import;
        VmImportReport VmImportReport;
        VmCommercial_BillOfExchange VmCommercial_BillOfExchange;
        VmCommercial_Invoice VmCommercial_Invoice;
        VmCommercial_InvoiceSlave VmCommercial_InvoiceSlave;
        VmCommercial_BillOfExchangeSlave VmCommercial_BillOfExchangeSlave;
        VmCommercial_BtBLCType VmCommercial_BtBLCType;
        VmCommercial_LCType VmCommercial_LCType;
        VmCommercial_LCOregin VmCommercial_LCOregin;
        VmCommercial_BTBItem VmCommercial_BTBItem;
        VmCommercial_Report VmCommercial_Report;
        VmMkt_PO vmMkt_PO;
        VmForDropDown VmForDropDown;
        VmAcc_IntegrationJournal VmAcc_IntegrationJournal;
        VmAcc_ChequeIntegration VmAcc_ChequeIntegration;
        Int32 UserID = 0;

        public CommercialController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }


        //...........Commercial_PI.............//
        public async Task<ActionResult> VmCommercial_PIIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_PI = new VmCommercial_PI();
            await Task.Run(() => VmCommercial_PI.InitialDataLoad());
            return View(VmCommercial_PI);
        }

        public ActionResult VmCommercial_PICreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_PI = new VmCommercial_PI();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_PICreate(VmCommercial_PI a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                // await Task.Run(() => a.Add(UserID));

                string s = MvcApplication.FilePrefix;
                await Task.Run(() => a.Add(UserID, Server.MapPath(s + "CustomeFile/Commercial/")));

                return RedirectToAction("VmCommercial_PIIndex");
            }
            return RedirectToAction("VmCommercial_PIIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_PIEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmCommercial_PI = new VmCommercial_PI();
            await Task.Run(() => VmCommercial_PI.SelectSingle(id.Value));

            if (VmCommercial_PI.Commercial_PI == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_PI);
        }
        // To update Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_PIEdit(VmCommercial_PI VmCommercial_PI)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (ModelState.IsValid)
            {
                // await Task.Run(() => Commercial_PI.Edit(0));
                string s = MvcApplication.FilePrefix;
                await Task.Run(() => VmCommercial_PI.EditUser(UserID, Server.MapPath(MvcApplication.FilePrefix + "CustomeFile/Commercial/")));


                return RedirectToAction("VmCommercial_PIIndex");
            }
            // return View(VmCommercial_PI);
            return RedirectToAction("VmCommercial_PIIndex");
        }
        public ActionResult VmCommercial_PIDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_PI b = new Commercial_PI();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_PIIndex");
        }

        //...........Commercial_MasterLC.............//
        public async Task<ActionResult> VmCommercial_MasterLCIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_MasterLC = new VmCommercial_MasterLC();
            await Task.Run(() => VmCommercial_MasterLC.InitialDataLoad());
            return View(VmCommercial_MasterLC);
        }
        [HttpGet]
        public ActionResult VmCommercial_MasterLCCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_MasterLC = new VmCommercial_MasterLC();
            VmCommercial_MasterLC.Commercial_MasterLC = new Commercial_MasterLC();
            VmCommercial_MasterLC.Commercial_MasterLC.Date = DateTime.Now;
            VmCommercial_MasterLC.Commercial_MasterLC.ExpiryDate = DateTime.Now;
            VmCommercial_MasterLC.Commercial_MasterLC.ShipmentDate = DateTime.Now;

            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMkt_BuyerForDropDown();
            ViewBag.MasterLCMkt_Buyer = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetCommercial_BankForDropDown();
            ViewBag.Buyer_Bank = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetLienBankForDropDown();
            ViewBag.Lien_Bank = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetMasterLcTypeDropDown();
            ViewBag.MasterLcType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetLCOreginDropDown();
            ViewBag.LCOregin = new SelectList(VmForDropDown.DDownData, "Value", "Text");



            return View(VmCommercial_MasterLC);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MasterLCCreate(VmCommercial_MasterLC a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                // chart of account integration
                try
                {
                    VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                    {
                        UserId = UserID,
                        AccountName = "MASTER L/C No. " + a.Commercial_MasterLC.Name,
                        Chart2Id = 173
                    };
                    a.Commercial_MasterLC.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.AddFromIntegratedModule());
                }
                catch (Exception e)
                {
                    a.Commercial_MasterLC.Acc_AcNameFk = 0;
                }
                //string s = MvcApplication.FilePrefix;
                //await Task.Run(() => a.Add(UserID));
                var value = await Task.Run(() => a.Add(UserID, Server.MapPath("~/CustomeFile/Commercial/")));


                // journal integration
                try
                {
                    var mkt_Buyer = db.Mkt_Buyer.Find(a.Commercial_MasterLC.Mkt_BuyerFK);
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = this.UserID,
                        Date = a.Commercial_MasterLC.Date,
                        Title = "Master L/C Reveice",
                        FromAccNo = mkt_Buyer.Acc_AcNameFk,
                        ToAccNo = a.Commercial_MasterLC.Acc_AcNameFk,
                        Amount = (decimal)(a.Commercial_MasterLC.TotalValue ?? 0),
                        Descriptions = "Master L/C " + a.Commercial_MasterLC.Name + " Receive from " + mkt_Buyer.Name,
                        TableIdFk = VmAcc_Database_Table.TableName(a.Commercial_MasterLC.GetType().Name),
                        TableRowIdFk = value,
                    };
                    var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                }
                catch (Exception e)
                {

                }

                return RedirectToAction("VmCommercial_MasterLCIndex");
            }
            return RedirectToAction("VmCommercial_MasterLCIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmCommercial_MasterLCEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMkt_BuyerForDropDown();
            ViewBag.MasterLCMkt_Buyer = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetCommercial_BankForDropDown();
            ViewBag.Buyer_Bank = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetLienBankForDropDown();
            ViewBag.Lien_Bank = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetMasterLcTypeDropDown();
            ViewBag.MasterLcType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetLCOreginDropDown();
            ViewBag.LCOregin = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_MasterLC = new VmCommercial_MasterLC();
            await Task.Run(() => VmCommercial_MasterLC.SelectSingle(id.Value));


            if (VmCommercial_MasterLC.Commercial_MasterLC == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_MasterLC);
        }
        //To update Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MasterLCEdit(VmCommercial_MasterLC VmCommercial_MasterLC)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                // chart of account integration
                try
                {
                    VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                    {
                        UserId = UserID,
                        AccountName = "MASTER L/C No. " + VmCommercial_MasterLC.Commercial_MasterLC.Name,
                        Chart2Id = 173,
                        ExistAccId = VmCommercial_MasterLC.Commercial_MasterLC.Acc_AcNameFk
                    };
                    VmCommercial_MasterLC.Commercial_MasterLC.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.EditFromIntegratedModule());

                }
                catch (Exception e)
                {
                    VmCommercial_MasterLC.Commercial_MasterLC.Acc_AcNameFk = 0;
                }

                //string s = MvcApplication.FilePrefix;
                await Task.Run(() => VmCommercial_MasterLC.EditMasterLC(UserID, Server.MapPath("~/CustomeFile/Commercial/")));


                // journal integration
                try
                {
                    var mkt_Buyer = db.Mkt_Buyer.Find(VmCommercial_MasterLC.Commercial_MasterLC.Mkt_BuyerFK);
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = this.UserID,
                        Date = VmCommercial_MasterLC.Commercial_MasterLC.Date,
                        Title = "Master L/C Reveice",
                        FromAccNo = mkt_Buyer.Acc_AcNameFk,
                        ToAccNo = VmCommercial_MasterLC.Commercial_MasterLC.Acc_AcNameFk,
                        Amount = (decimal)(VmCommercial_MasterLC.Commercial_MasterLC.TotalValue ?? 0),
                        Descriptions = "Master L/C " + VmCommercial_MasterLC.Commercial_MasterLC.Name + " Receive from " + mkt_Buyer.Name,
                        TableIdFk = VmAcc_Database_Table.TableName(VmCommercial_MasterLC.Commercial_MasterLC.GetType().Name),
                        TableRowIdFk = VmCommercial_MasterLC.Commercial_MasterLC.ID,
                    };
                    var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
                }
                catch (Exception e)
                {

                }


                return RedirectToAction("VmCommercial_MasterLCIndex");
            }
            return View(VmCommercial_MasterLC);
        }
        public ActionResult VmCommercial_MasterLCDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_MasterLC b = new Commercial_MasterLC();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_MasterLCIndex");
        }

        //...........Commercial_B2bLC.............//
        public async Task<ActionResult> VmCommercial_B2bLCIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            await Task.Run(() => VmCommercial_B2bLC.InitialDataLoadForBTB());

            return View(VmCommercial_B2bLC);
        }


        public async Task<ActionResult> VmCommercial_B2BPaymentInformationCreate(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            int btbID = 0, supplierID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out btbID);
            id = id.Replace(btbID.ToString() + "x", "");
            int.TryParse(id, out supplierID);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            await Task.Run(() => VmCommercial_B2bLC.SelectSingleLCInfo(btbID));
            VmCommercial_B2bLC.SupplierID = supplierID;
            if (VmCommercial_B2bLC.Commercial_B2bLC == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_B2bLC);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_B2BPaymentInformationCreate(VmCommercial_B2bLC x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            var preBtbPayment = db.Commercial_B2BPaymentInformation.Where(p => p.Commercial_B2bLCFK == x.Commercial_B2bLC.ID  && p.Active == true).Select(p => p.B2bLCPayment).DefaultIfEmpty(0).Sum();
            var availableValue = x.Commercial_B2bLC.Amount - preBtbPayment;
         
            if (preBtbPayment <= x.Commercial_B2bLC.Amount && x.Commercial_B2BPaymentInformation.B2bLCPayment <= (x.Commercial_B2bLC.Amount - preBtbPayment))
            {

                VmControllerHelper vmControllerHelper = new VmControllerHelper();
                x.Commercial_B2BPaymentInformation.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
                x.Commercial_B2BPaymentInformation.IsApproved = false;
                

                if (ModelState.IsValid)
                {
                    //Notifier Integration Get Closing Balance

                    var commercial_B2bLC = db.Commercial_B2bLC.Find(x.Commercial_B2bLC.ID);
                    var common_Supplier = db.Common_Supplier.Find(commercial_B2bLC.Common_SupplierFK);

                    VmNotifier vmNotifier = new VmNotifier();
                    var Common_SupplierOpeningBalance = vmNotifier.PartyPaymentLedger(common_Supplier);

                    //Notifier Integration Get Closing Balance
                    var value = await Task.Run(() => x.B2BPaymentAdd(UserID));
                    //Notifier Integration
                    try
                    {
                        VmForDropDown vmForDropDown = new VmForDropDown();
                        VmAccounting accounting = new VmAccounting();
                        accounting.FromDate = new DateTime(2001, 1, 1);
                        accounting.ToDate = DateTime.Today;



                        Acc_Notifier accNotifier = new Acc_Notifier
                        {
                            Type = (int)Acc_NotifierType.DocPayment,
                            Amount = (decimal)(x.Commercial_B2BPaymentInformation.B2bLCPayment),
                            FromBalance = 0,
                            ChequeNo = "",
                            ToBalance = Common_SupplierOpeningBalance,
                            FromName = "",
                            ToName = common_Supplier.Name,
                            Particulars = $@"",
                            TableIdFk = VmAcc_Database_Table.TableName(x.Commercial_B2BPaymentInformation.GetType().Name),
                            RowIdFk = value,
                            FromFk = commercial_B2bLC.Acc_AcNameFk,
                            ToFk = common_Supplier.Acc_AcNameFk,
                            IsApproved = 0,
                            Date = DateTime.Now
                        };

                        this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
                        {
                            UserId = this.UserID,
                            Acc_Notifier = accNotifier
                        };
                        // call integration and receive boolean. if true data add in journal successfully else not added
                        var id = await Task.Run(() => this.VmAcc_ChequeIntegration.AddCheque());
                        //aproval method
                        VmNotifierApproval vmNotifierApproval = new VmNotifierApproval();
                        await Task.Run(() => vmNotifierApproval.NotifierDocApproved(id, user));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }




                    if (x.SupplierID == 0)
                    {
                        return RedirectToAction("VmCommercial_B2bLCIndex");

                    }
                    else
                    {
                        return RedirectToAction("./../Accounts/OpenedBTBBySupplier/" + x.SupplierID);
                    }


                }

            }

           
            return RedirectToAction("VmCommercial_B2bLCIndex");
        }


        [HttpGet]
        public ActionResult VmCommercial_B2bLCCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            VmCommercial_B2bLC.Commercial_B2bLC = new Commercial_B2bLC();
            VmCommercial_B2bLC.Commercial_B2bLC.Date = DateTime.Now;

            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommercial_MasterLCForDropDown();
            ViewBag.B2bMasterLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommercial_BankForDropDown();
            ViewBag.Commercial_Bank = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommon_SupplierForDropDown();
            ViewBag.Common_Supplier = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            ViewBag.Common_Currency = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            return View(VmCommercial_B2bLC);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_B2bLCCreate(VmCommercial_B2bLC a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                // a.AddReady(UserID);
                //await Task.Run(() => a.Add(UserID));
                string s = MvcApplication.FilePrefix;
                //  await Task.Run(() => a.Add(UserID));
                await Task.Run(() => a.Add(UserID, Server.MapPath(MvcApplication.FilePrefix + "CustomeFile/Commercial/")));
                return RedirectToAction("VmCommercial_B2bLCIndex");
            }
            return RedirectToAction("VmCommercial_B2bLCIndex");
        }

        //  VmCommercial_B2bLCEdit
        [HttpGet]
        public async Task<ActionResult> VmCommercial_B2bLCEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            await Task.Run(() => VmCommercial_B2bLC.SelectSingle(id.Value));

            ///....For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommercial_MasterLCForDropDown();
            ViewBag.B2bMasterLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmForDropDown.DDownData = VmForDropDown.GetCommercial_BankForDropDown();
            ViewBag.Commercial_Bank = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmForDropDown.DDownData = VmForDropDown.GetCommon_SupplierForDropDown();
            ViewBag.Common_Supplier = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            ViewBag.Common_Currency = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            if (VmCommercial_B2bLC.Commercial_B2bLC == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_B2bLC);
        }
        // To update Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_B2bLCEdit(VmCommercial_B2bLC x)
        {

            if (ModelState.IsValid)
            {
                //await Task.Run(() => x.Edit(0));
                string s = MvcApplication.FilePrefix;
                await Task.Run(() => x.EditUser(UserID, Server.MapPath(MvcApplication.FilePrefix + "CustomeFile/Commercial/")));

                return RedirectToAction("VmCommercial_B2bLCIndex");
            }
            return RedirectToAction("VmCommercial_B2bLCIndex");
        }

        public ActionResult VmCommercial_B2bLCDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_B2bLC b = new Commercial_B2bLC();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_B2bLCIndex");
        }
        //...............Commercial_Bank.....................

        public async Task<ActionResult> VmCommercial_BankIndex()
        {


            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_Bank = new VmCommercial_Bank();
            await Task.Run(() => VmCommercial_Bank.InitialDataLoad());
            return View(VmCommercial_Bank);
        }
        [HttpGet]
        public ActionResult VmCommercial_BankCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_Bank = new VmCommercial_Bank();
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            ViewBag.Common_Country = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            return View(VmCommercial_Bank);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BankCreate(VmCommercial_Bank a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_BankIndex");
            }
            return RedirectToAction("VmCommercial_BankIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_BankEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmCommercial_Bank = new VmCommercial_Bank();
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            ViewBag.Common_Country = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            await Task.Run(() => VmCommercial_Bank.SelectSingle(id.Value));

            if (VmCommercial_Bank.Commercial_Bank == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_Bank);
        }
        //// To update Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BankEdit(Commercial_Bank Commercial_Bank)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (ModelState.IsValid)
            {
                await Task.Run(() => Commercial_Bank.Edit(0));
                return RedirectToAction("VmCommercial_BankIndex");
            }
            return RedirectToAction("VmCommercial_BankIndex");
        }

        public ActionResult VmCommercial_BankDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_Bank b = new Commercial_Bank();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_BankIndex");
        }
        //...............Commercial_MLcB2BLc.....................

        public async Task<ActionResult> VmCommercial_MLcB2BLcIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            // int id1=0; int.TryParse(id,out id1);

            VmCommercial_MasterLCBuyerPO = new VmCommercial_MasterLCBuyerPO();
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            await Task.Run(() => VmCommercial_B2bLC = VmCommercial_B2bLC.SelectSingleJoined(id.Value));

            VmCommercial_B2bLC.VmCommercial_MasterLCBuyerPO = new VmCommercial_MasterLCBuyerPO();
            await Task.Run(() => VmCommercial_B2bLC.VmCommercial_MasterLCBuyerPO.GetBuyerCommission(id.Value));

            await Task.Run(() => VmCommercial_B2bLC.GetB2BLCByMasterLC(id.Value));

            return View(VmCommercial_B2bLC);

        }
        [HttpGet]
        public ActionResult VmCommercial_MLcB2BLcCreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {

                VmForDropDown = new VmForDropDown();
                //VmForDropDown.DDownData = VmForDropDown.GetCommercial_BankForDropDown();
                //ViewBag.Commercial_Bank = new SelectList(VmForDropDown.DDownData, "Value", "Text");


                VmForDropDown.DDownData = VmForDropDown.GetCommon_SupplierForDropDown();
                ViewBag.Common_Supplier = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmForDropDown.DDownData = VmForDropDown.GetBTBItemDropDown();
                ViewBag.ComBTBItem = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
                ViewBag.Currency = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmForDropDown.DDownData = VmForDropDown.GetLCOreginDropDown();
                ViewBag.LCOregin = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmForDropDown.DDownData = VmForDropDown.GetBtBLCTypeDropDown();
                ViewBag.BtBLCType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
                SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.Unit = sl4;

                VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
                ViewBag.CurrencyList = new SelectList(VmForDropDown.DDownData, "Value", "Text");



                VmCommercial_B2bLC = new VmCommercial_B2bLC();
                VmCommercial_B2bLC.Commercial_B2bLC = new Commercial_B2bLC();
                VmCommercial_B2bLC.Commercial_B2bLC.Date = DateTime.Now;
                VmCommercial_B2bLC.Commercial_B2bLC.Status = "Running";
                VmCommercial_B2bLC.Commercial_B2bLC.Commercial_UDFK = id.Value;
                return View(VmCommercial_B2bLC);

                // return RedirectToAction("VmCommercial_MLcB2BLcIndex/" + VmCommercial_B2bLC.Commercial_B2bLC.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_UDSlaveIndex/" + id);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcB2BLcCreate(VmCommercial_B2bLC a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                // chart of account integration
                try
                {
                    VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                    {
                        UserId = UserID,
                        AccountName = "BTB Account No. " + a.Commercial_B2bLC.Name,
                        Chart2Id = 177,
                    };
                    a.Commercial_B2bLC.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.AddFromIntegratedModule());

                }
                catch (Exception e)
                {
                    a.Commercial_B2bLC.Acc_AcNameFk = 0;
                }

                //Notifier Integration Get Closing Balance
                var commercial_UD = db.Commercial_UD.Find(a.Commercial_B2bLC.Commercial_UDFK);
                var common_Supplier = db.Common_Supplier.Find(a.Commercial_B2bLC.Common_SupplierFK);

                VmNotifier vmNotifier = new VmNotifier();
                var Common_SupplierOpeningBalance = vmNotifier.PartyOpeningLedger(common_Supplier);
                //Notifier Integration Get Closing Balance

                a.Commercial_B2bLC.IsApproved = false;
                var value = await Task.Run(() => a.Add(UserID, Server.MapPath("~/CustomeFile/Commercial/")));

                //Notifier Integration
                try
                {
                    VmAccounting accounting = new VmAccounting();
                    accounting.FromDate = new DateTime(2001, 1, 1);
                    accounting.ToDate = DateTime.Today;



                    Acc_Notifier accNotifier = new Acc_Notifier
                    {
                        Type = (int)Acc_NotifierType.LcOpen,
                        Amount = (decimal)(a.Commercial_B2bLC.Amount),
                        FromBalance = 0,
                        ChequeNo = "",
                        ToBalance = Common_SupplierOpeningBalance,
                        FromName = "",
                        ToName = common_Supplier.Name,
                        Particulars = $@"",
                        TableIdFk = VmAcc_Database_Table.TableName(a.Commercial_B2bLC.GetType().Name),
                        RowIdFk = value,
                        FromFk = commercial_UD.Acc_AcNameFk,
                        ToFk = a.Commercial_B2bLC.Acc_AcNameFk,
                        IsApproved = 0,
                        Date = DateTime.Now
                    };

                    this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
                    {
                        UserId = this.UserID,
                        Acc_Notifier = accNotifier
                    };
                    // call integration and receive boolean. if true data add in journal successfully else not added
                    var id = await Task.Run(() => this.VmAcc_ChequeIntegration.AddCheque());

                    // auto approval
                    VmNotifierApproval vmNotifierApproval = new VmNotifierApproval();
                    await Task.Run(() => vmNotifierApproval.NotifierBtbApproved(id, user));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                //// journal integration
                //try
                //{
                //    var commercial_UD = db.Commercial_UD.Find(a.Commercial_B2bLC.Commercial_UDFK);
                //    var common_Supplier = db.Common_Supplier.Find(a.Commercial_B2bLC.Common_SupplierFK);
                //    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                //    {
                //        UserId = this.UserID,
                //        Date = a.Commercial_B2bLC.Date,
                //        Title = "BTB Opening",
                //        FromAccNo = commercial_UD.Acc_AcNameFk,
                //        ToAccNo = a.Commercial_B2bLC.Acc_AcNameFk,
                //        Amount = (decimal)(a.Commercial_B2bLC.Amount),
                //        Descriptions = "UD: " + commercial_UD.UdNo + " Payable to BTB L/C : " + a.Commercial_B2bLC.Name + ". Party Name: " + common_Supplier.Name,
                //        TableIdFk = VmAcc_Database_Table.TableName(a.Commercial_B2bLC.GetType().Name),
                //        TableRowIdFk = value,
                //    };
                //    var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
                //}
                //catch (Exception e)
                //{

                //}


                return RedirectToAction("VmCommercial_UDSlaveIndex/" + a.Commercial_B2bLC.Commercial_UDFK);
            }
            return RedirectToAction("VmCommercial_UDSlaveIndex");
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_MLcB2BLcEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommon_SupplierForDropDown();
            ViewBag.Common_Supplier = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetBTBItemDropDown();
            ViewBag.ComBTBItem = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            ViewBag.Currency = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetLCOreginDropDown();
            ViewBag.LCOregin = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetBtBLCTypeDropDown();
            ViewBag.BtBLCType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;


            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            ViewBag.CurrencyList = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);

            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            VmCommercial_B2bLC.Commercial_B2bLC = new Commercial_B2bLC();
            VmCommercial_B2bLC.Commercial_B2bLC.Date = DateTime.Now;
            VmCommercial_B2bLC.Commercial_B2bLC.Commercial_UDFK = redirictId;

            await Task.Run(() => VmCommercial_B2bLC.SelectSingle(editId));

            return View(VmCommercial_B2bLC);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcB2BLcEdit(VmCommercial_B2bLC x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                // chart of account integration
                try
                {
                    VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                    {
                        UserId = UserID,
                        AccountName = "BTB Account No. " + x.Commercial_B2bLC.Name,
                        Chart2Id = 177,
                        ExistAccId = x.Commercial_B2bLC.Acc_AcNameFk
                    };
                    x.Commercial_B2bLC.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.EditFromIntegratedModule());

                }
                catch (Exception e)
                {
                    x.Commercial_B2bLC.Acc_AcNameFk = 0;
                }

                await Task.Run(() => x.EditUser(UserID, Server.MapPath("~/CustomeFile/Commercial/")));

                // journal integration
                try
                {
                    var commercial_UD = db.Commercial_UD.Find(x.Commercial_B2bLC.Commercial_UDFK);
                    var common_Supplier = db.Common_Supplier.Find(x.Commercial_B2bLC.Common_SupplierFK);
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = this.UserID,
                        Date = x.Commercial_B2bLC.Date,
                        Title = "BTB Opening",
                        CurrencyType = x.Commercial_B2bLC.CurrencyType,
                        CurrencyRate = x.Commercial_B2bLC.CurrencyRate,
                        FromAccNo = commercial_UD.Acc_AcNameFk,
                        ToAccNo = x.Commercial_B2bLC.Acc_AcNameFk,
                        Amount = (decimal)(x.Commercial_B2bLC.Amount),
                        Descriptions = "UD: " + commercial_UD.UdNo + " Payable to BTB L/C : " + x.Commercial_B2bLC.Name + ". Party Name: " + common_Supplier.Name,
                        TableIdFk = VmAcc_Database_Table.TableName(x.Commercial_B2bLC.GetType().Name),
                        TableRowIdFk = x.Commercial_B2bLC.ID,
                    };
                    var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
                }
                catch (Exception e)
                {

                }

                return RedirectToAction("VmCommercial_UDSlaveIndex/" + x.Commercial_B2bLC.Commercial_UDFK);
            }
            return RedirectToAction("VmCommercial_UDSlaveIndex", "Commercial");
        }
        public async Task<ActionResult> VmCommercial_MLcB2BLcDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_B2bLC b = new Commercial_B2bLC();
            b.ID = deleteId;
            b.Delete(0);

            this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
            {
                TableIdFk = VmAcc_Database_Table.TableName(b.GetType().Name),
                TableRowIdFk = b.ID,
            };
            var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.DeleteJounalFromIntegratedTable());

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_UDSlaveIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_UDSlaveIndex", "Commercial");
        }

        //...............Commercial_MLcSupplierPO.....................
        public async Task<ActionResult> VmCommercial_MLcSupplierPOIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MLcSupplierPO = new VmCommercial_MLcSupplierPO();
                await Task.Run(() => VmCommercial_MLcSupplierPO = VmCommercial_MLcSupplierPO.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_MLcSupplierPO.InitialDataLoad(id.Value));
                return View(VmCommercial_MLcSupplierPO);
            }
            return View("VmCommercial_MasterLCIndex");
        }
        [HttpGet]
        public ActionResult VmCommercial_MLcSupplierPOCreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MLcSupplierPO = new VmCommercial_MLcSupplierPO();
                //...For dropdrown...///
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetMktPODropDown();
                ViewBag.MktPO = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmCommercial_MLcSupplierPO = new VmCommercial_MLcSupplierPO();
                VmCommercial_MLcSupplierPO.Commercial_MLcSupplierPO = new Commercial_MLcSupplierPO();
                VmCommercial_MLcSupplierPO.Commercial_MLcSupplierPO.Commercial_MasterLCFK = id.Value;
                VmCommercial_MLcSupplierPO.Commercial_MLcSupplierPO.Date = DateTime.Now.AddDays(30);

                return View(VmCommercial_MLcSupplierPO);
            }

            return RedirectToAction("VmCommercial_MLcSupplierPOIndex/" + VmCommercial_MLcSupplierPO.Commercial_MLcSupplierPO.Commercial_MasterLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcSupplierPOCreate(VmCommercial_MLcSupplierPO a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_MLcSupplierPOIndex/" + a.Commercial_MLcSupplierPO.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MLcSupplierPOIndex/" + a.Commercial_MLcSupplierPO.Commercial_MasterLCFK);
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_MLcSupplierPOEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMktPODropDown();
            ViewBag.MktPO = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_MLcSupplierPO = new VmCommercial_MLcSupplierPO();
            await Task.Run(() => VmCommercial_MLcSupplierPO.SelectSingle(editId));

            return View(VmCommercial_MLcSupplierPO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcSupplierPOEdit(VmCommercial_MLcSupplierPO x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_MLcSupplierPOIndex/" + x.Commercial_MLcSupplierPO.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }
        public ActionResult VmCommercial_MLcSupplierPODelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_MLcSupplierPO b = new Commercial_MLcSupplierPO();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_MLcSupplierPOIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }

        //...............Commercial_MLcSupplierPI.....................

        public async Task<ActionResult> VmCommercial_MLcSupplierPIIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MLcSupplierPI = new VmCommercial_MLcSupplierPI();
                await Task.Run(() => VmCommercial_MLcSupplierPI = VmCommercial_MLcSupplierPI.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_MLcSupplierPI.InitialDataLoad(id.Value));
                return View(VmCommercial_MLcSupplierPI);
            }
            return View("VmCommercial_MasterLCIndex");
        }

        [HttpGet]
        public ActionResult VmCommercial_MLcSupplierPICreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MLcSupplierPI = new VmCommercial_MLcSupplierPI();
                //...For dropdrown...///
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetCommercial_PIForDropDown();
                ViewBag.Commercial_PI = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmCommercial_MLcSupplierPI = new VmCommercial_MLcSupplierPI();
                VmCommercial_MLcSupplierPI.Commercial_MLcSupplierPI = new Commercial_MLcSupplierPI();
                VmCommercial_MLcSupplierPI.Commercial_MLcSupplierPI.Commercial_MasterLCFK = id.Value;
                VmCommercial_MLcSupplierPI.Commercial_MLcSupplierPI.Date = DateTime.Now;

                return View(VmCommercial_MLcSupplierPI);
            }

            return RedirectToAction("VmCommercial_MLcSupplierPIIndex/" + VmCommercial_MLcSupplierPI.Commercial_MLcSupplierPI.Commercial_MasterLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcSupplierPICreate(VmCommercial_MLcSupplierPI a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_MLcSupplierPIIndex/" + a.Commercial_MLcSupplierPI.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MLcSupplierPIIndex/" + a.Commercial_MLcSupplierPI.Commercial_MasterLCFK);
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_MLcSupplierPIEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommercial_PIForDropDown();
            ViewBag.Commercial_PI = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_MLcSupplierPI = new VmCommercial_MLcSupplierPI();
            await Task.Run(() => VmCommercial_MLcSupplierPI.SelectSingle(editId));

            return View(VmCommercial_MLcSupplierPI);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcSupplierPIEdit(VmCommercial_MLcSupplierPI x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_MLcSupplierPIIndex/" + x.Commercial_MLcSupplierPI.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }

        public ActionResult VmCommercial_MLcSupplierPIDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_MLcSupplierPI b = new Commercial_MLcSupplierPI();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_MLcSupplierPIIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }

        //...............Commercial_MLcSupplierB2BLc.....................

        public async Task<ActionResult> VmCommercial_MLcSupplierB2BLcIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MLcSupplierB2BLc = new VmCommercial_MLcSupplierB2BLc();
                await Task.Run(() => VmCommercial_MLcSupplierB2BLc = VmCommercial_MLcSupplierB2BLc.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_MLcSupplierB2BLc.InitialDataLoad(id.Value));
                return View(VmCommercial_MLcSupplierB2BLc);
            }
            return View("VmCommercial_MasterLCIndex");
        }
        [HttpGet]
        public ActionResult VmCommercial_MLcSupplierB2BLcCreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MLcSupplierB2BLc = new VmCommercial_MLcSupplierB2BLc();
                //...For dropdrown...///
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetCommercial_B2bLCForDropDown();
                ViewBag.Commercial_B2bLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmCommercial_MLcSupplierB2BLc = new VmCommercial_MLcSupplierB2BLc();
                VmCommercial_MLcSupplierB2BLc.Commercial_MLcSupplierB2BLc = new Commercial_MLcSupplierB2BLc();
                VmCommercial_MLcSupplierB2BLc.Commercial_MLcSupplierB2BLc.Commercial_MasterLCFK = id.Value;
                VmCommercial_MLcSupplierB2BLc.Commercial_MLcSupplierB2BLc.Date = DateTime.Now;

                return View(VmCommercial_MLcSupplierB2BLc);
            }

            return RedirectToAction("VmCommercial_MLcSupplierB2BLcIndex/" + VmCommercial_MLcSupplierB2BLc.Commercial_MLcSupplierB2BLc.Commercial_MasterLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcSupplierB2BLcCreate(VmCommercial_MLcSupplierB2BLc a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_MLcSupplierB2BLcIndex/" + a.Commercial_MLcSupplierB2BLc.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MLcSupplierB2BLcIndex/" + a.Commercial_MLcSupplierB2BLc.Commercial_MasterLCFK);
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_MLcSupplierB2BLcEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommercial_B2bLCForDropDown();
            ViewBag.Commercial_B2bLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_MLcSupplierB2BLc = new VmCommercial_MLcSupplierB2BLc();
            await Task.Run(() => VmCommercial_MLcSupplierB2BLc.SelectSingle(editId));

            return View(VmCommercial_MLcSupplierB2BLc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcSupplierB2BLcEdit(VmCommercial_MLcSupplierB2BLc x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_MLcSupplierB2BLcIndex/" + x.Commercial_MLcSupplierB2BLc.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }

        public ActionResult VmCommercial_MLcSupplierB2BLcDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_MLcSupplierB2BLc b = new Commercial_MLcSupplierB2BLc();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_MLcSupplierB2BLcIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }
        //.........................................................................
        //...........Commercial__MasterLCDocs.............//
        //public async Task<ActionResult> VmCommercial__MasterLCDocsIndex()
        //{
        //    VmCommercial__MasterLCDocs = new VmCommercial__MasterLCDocs();
        //    await Task.Run(() => VmCommercial__MasterLCDocs.InitialDataLoad());
        //    return View(VmCommercial__MasterLCDocs);
        //}
        //[HttpGet]
        //public ActionResult VmCommercial__MasterLCDocsCreate()
        //{
        //    VmCommercial__MasterLCDocs = new VmCommercial__MasterLCDocs();
        //    VmCommercial__MasterLCDocs.Commercial__MasterLCDocs = new Commercial__MasterLCDocs();
        //    VmCommercial__MasterLCDocs.Commercial__MasterLCDocs.Date = DateTime.Now;
        //    return View(VmCommercial__MasterLCDocs);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VmCommercial__MasterLCDocsCreate(VmCommercial__MasterLCDocs a)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        await Task.Run(() => a.Add(UserID));
        //        return RedirectToAction("VmCommercial__MasterLCDocsIndex");
        //    }
        //    return RedirectToAction("VmCommercial__MasterLCDocsIndex");
        //}

        //[HttpGet]
        //public async Task<ActionResult> VmCommercial__MasterLCDocsEdit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    VmCommercial__MasterLCDocs = new VmCommercial__MasterLCDocs();
        //    await Task.Run(() => VmCommercial__MasterLCDocs.SelectSingle(id.Value));

        //    if (VmCommercial__MasterLCDocs.Commercial__MasterLCDocs == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(VmCommercial__MasterLCDocs);
        //}
        //// To update Buyer Information. -Post
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VmCommercial__MasterLCDocsEdit(Commercial__MasterLCDocs Commercial__MasterLCDocs)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        await Task.Run(() => Commercial__MasterLCDocs.Edit(0));
        //        return RedirectToAction("VmCommercial__MasterLCDocsIndex");
        //    }
        //    return View(Commercial__MasterLCDocs);
        //}
        //public ActionResult VmCommercial__MasterLCDocsDelete(int id)
        //{
        //    Commercial__MasterLCDocs b = new Commercial__MasterLCDocs();
        //    b.ID = id;
        //    b.Delete(0);
        //    return RedirectToAction("VmCommercial__MasterLCDocsIndex");
        //}
        //.........................................................................
        //...........Commercial_MLcBuyerDocs.............//
        public async Task<ActionResult> VmCommercial_MLcBuyerDocsIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MLcBuyerDocs = new VmCommercial_MLcBuyerDocs();
                await Task.Run(() => VmCommercial_MLcBuyerDocs = VmCommercial_MLcBuyerDocs.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_MLcBuyerDocs.InitialDataLoad(id.Value));
                return View(VmCommercial_MLcBuyerDocs);
            }
            return View("VmCommercial_MasterLCIndex");
        }
        [HttpGet]
        public ActionResult VmCommercial_MLcBuyerDocsCreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MLcBuyerDocs = new VmCommercial_MLcBuyerDocs();
                //...For dropdrown...///
                //VmForDropDown = new VmForDropDown();
                //VmForDropDown.DDownData = VmForDropDown.GetMasterLCDocsDropDown();
                //ViewBag.MasterLCDocs = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmCommercial_MLcBuyerDocs = new VmCommercial_MLcBuyerDocs();
                VmCommercial_MLcBuyerDocs.Commercial_MLcBuyerDocs = new Commercial_MLcBuyerDocs();
                VmCommercial_MLcBuyerDocs.Commercial_MLcBuyerDocs.Commercial_MasterLCFK = id.Value;
                VmCommercial_MLcBuyerDocs.Commercial_MLcBuyerDocs.Date = DateTime.Now;

                return View(VmCommercial_MLcBuyerDocs);
            }

            return RedirectToAction("VmCommercial_MLcBuyerDocsIndex/" + VmCommercial_MLcBuyerDocs.Commercial_MLcBuyerDocs.Commercial_MasterLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcBuyerDocsCreate(VmCommercial_MLcBuyerDocs a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_MLcBuyerDocsIndex/" + a.Commercial_MLcBuyerDocs.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MLcBuyerDocsIndex/" + a.Commercial_MLcBuyerDocs.Commercial_MasterLCFK);
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_MLcBuyerDocsEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            //VmForDropDown = new VmForDropDown();
            //VmForDropDown.DDownData = VmForDropDown.GetMasterLCDocsDropDown();
            //ViewBag.MasterLCDocs = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_MLcBuyerDocs = new VmCommercial_MLcBuyerDocs();
            await Task.Run(() => VmCommercial_MLcBuyerDocs.SelectSingle(editId));

            return View(VmCommercial_MLcBuyerDocs);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLcBuyerDocsEdit(VmCommercial_MLcBuyerDocs x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_MLcBuyerDocsIndex/" + x.Commercial_MLcBuyerDocs.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }
        public ActionResult VmCommercial_MLcBuyerDocsDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_MLcBuyerDocs b = new Commercial_MLcBuyerDocs();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_MLcBuyerDocsIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }

        //...............Commercial_PISupplierPO.....................

        public async Task<ActionResult> VmCommercial_PISupplierPOIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_PISupplierPO = new VmCommercial_PISupplierPO();
                await Task.Run(() => VmCommercial_PISupplierPO = VmCommercial_PISupplierPO.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_PISupplierPO.InitialDataLoad(id.Value));
                return View(VmCommercial_PISupplierPO);
            }
            return View("VmCommercial_PIIndex");
        }

        [HttpGet]
        public ActionResult VmCommercial_PISupplierPOCreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_PISupplierPO = new VmCommercial_PISupplierPO();
                //...For dropdrown...///
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetMkt_POForDropDown();
                ViewBag.Mkt_PO = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmCommercial_PISupplierPO = new VmCommercial_PISupplierPO();
                VmCommercial_PISupplierPO.Commercial_PISupplierPO = new Commercial_PISupplierPO();
                VmCommercial_PISupplierPO.Commercial_PISupplierPO.Commercial_PIFK = id.Value;
                VmCommercial_PISupplierPO.Commercial_PISupplierPO.Date = DateTime.Now;

                return View(VmCommercial_PISupplierPO);
            }

            return RedirectToAction("VmCommercial_PISupplierPOIndex/" + VmCommercial_PISupplierPO.Commercial_PISupplierPO.Commercial_PIFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_PISupplierPOCreate(VmCommercial_PISupplierPO a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_PISupplierPOIndex/" + a.Commercial_PISupplierPO.Commercial_PIFK);
            }
            return RedirectToAction("VmCommercial_PISupplierPOIndex/" + a.Commercial_PISupplierPO.Commercial_PIFK);
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_PISupplierPOEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMkt_POForDropDown();
            ViewBag.Mkt_PO = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_PISupplierPO = new VmCommercial_PISupplierPO();
            await Task.Run(() => VmCommercial_PISupplierPO.SelectSingle(editId));

            return View(VmCommercial_PISupplierPO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_PISupplierPOEdit(VmCommercial_PISupplierPO x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_PISupplierPOIndex/" + x.Commercial_PISupplierPO.Commercial_PIFK);
            }
            return RedirectToAction("VmCommercial_PISupplierPOIndex", "Commercial");
        }

        public ActionResult VmCommercial_PISupplierPODelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_PISupplierPO b = new Commercial_PISupplierPO();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_PISupplierPOIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_PIIndex", "Commercial");
        }

        //...............Commercial_B2bLcSupplierPO.....................

        public async Task<ActionResult> VmCommercial_B2bLcSupplierPOIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id != null)
            {
                VmCommercial_B2bLcSupplierPO = new VmCommercial_B2bLcSupplierPO();
                await Task.Run(() => VmCommercial_B2bLcSupplierPO = VmCommercial_B2bLcSupplierPO.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_B2bLcSupplierPO.InitialDataLoad(id.Value));
                return View(VmCommercial_B2bLcSupplierPO);
            }
            return View("VmCommercial_B2bLCIndex");
        }

        [HttpGet]
        public ActionResult VmCommercial_B2bLcSupplierPOCreate(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, supplierId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("+")), out editId);
            id = id.Replace(editId.ToString() + "+", "");
            int.TryParse(id, out supplierId);
            if (id != null)
            {
                VmCommercial_B2bLcSupplierPO = new VmCommercial_B2bLcSupplierPO();
                //...For dropdrown...///
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetMktPOBySupplier(supplierId);
                ViewBag.Mkt_PO = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmCommercial_B2bLcSupplierPO = new VmCommercial_B2bLcSupplierPO();
                VmCommercial_B2bLcSupplierPO.Commercial_B2bLcSupplierPO = new Commercial_B2bLcSupplierPO();
                VmCommercial_B2bLcSupplierPO.Commercial_B2bLcSupplierPO.Commercial_B2bLCFK = editId;

                return View(VmCommercial_B2bLcSupplierPO);
            }

            return RedirectToAction("VmCommercial_B2bLcSupplierPOIndex/" + VmCommercial_B2bLcSupplierPO.Commercial_B2bLcSupplierPO.Commercial_B2bLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_B2bLcSupplierPOCreate(VmCommercial_B2bLcSupplierPO a)
        {

            int b2bSupplierPo = await Task.Run(() => a.Add(UserID));
            a.Commercial_B2bLcSupplierPOSlave.Commercial_B2bLcSupplierPOFK = b2bSupplierPo;

            await Task.Run(() => a.AddB2bLcSupplierPOSlave(0));
            return RedirectToAction("VmCommercial_B2bLcSupplierPOIndex/" + a.Commercial_B2bLcSupplierPO.Commercial_B2bLCFK);


        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_B2bLcSupplierPOEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmCommercial_B2bLcSupplierPO = new VmCommercial_B2bLcSupplierPO();
            await Task.Run(() => VmCommercial_B2bLcSupplierPO.SelectSingle(editId));
            int poid = 0;
            poid = VmCommercial_B2bLcSupplierPO.Mkt_PO.ID;
            //...For dropdrown...///
            //VmForDropDown = new VmForDropDown();
            //VmForDropDown.DDownData = VmForDropDown.GetMkt_POForDropDown();
            //ViewBag.Mkt_PO = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            //VmForDropDown = new VmForDropDown();
            //VmForDropDown.DDownData = VmForDropDown.GetItemForSupplierPoEditDropDown(poid);
            //ViewBag.Items = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            await Task.Run(() => VmCommercial_B2bLcSupplierPO.GetQuantityForSupplierPo(VmCommercial_B2bLcSupplierPO.Commercial_B2bLcSupplierPOSlave.Mkt_POSlaveFK.Value));
            //HelperController helper = new HelperController();
            //var result = helper.GetQuantityForSupplierPoList(VmCommercial_B2bLcSupplierPO.Commercial_B2bLcSupplierPOSlave.Mkt_POSlaveFK.Value);
            //ViewBag.res= JsonConvert.SerializeObject(result, Formatting.Indented);


            return View(VmCommercial_B2bLcSupplierPO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_B2bLcSupplierPOEdit(VmCommercial_B2bLcSupplierPO VmCommercial_B2bLcSupplierPO)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => VmCommercial_B2bLcSupplierPO.Commercial_B2bLcSupplierPO.Edit(0));
                await Task.Run(() => VmCommercial_B2bLcSupplierPO.Commercial_B2bLcSupplierPOSlave.Edit(0));
                return RedirectToAction("VmCommercial_B2bLcSupplierPOIndex/" + VmCommercial_B2bLcSupplierPO.Commercial_B2bLcSupplierPO.Commercial_B2bLCFK);
            }
            return RedirectToAction("VmCommercial_B2bLcSupplierPOIndex", "Commercial");
        }

        public async Task<ActionResult> VmCommercial_B2bLcSupplierPODelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_B2bLcSupplierPO b = new Commercial_B2bLcSupplierPO();
            b.ID = deleteId;
            b.Delete(UserID);

            VmCommercial_B2bLcSupplierPO = new VmCommercial_B2bLcSupplierPO();
            await Task.Run(() => VmCommercial_B2bLcSupplierPO.BulkDelete(UserID, deleteId));
            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_B2bLcSupplierPOIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_B2bLCIndex", "Commercial");
        }

        //...............Commercial_B2BLcSupplierPI.....................

        public async Task<ActionResult> VmCommercial_B2BLcSupplierPIIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_B2BLcSupplierPI = new VmCommercial_B2BLcSupplierPI();
                await Task.Run(() => VmCommercial_B2BLcSupplierPI = VmCommercial_B2BLcSupplierPI.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_B2BLcSupplierPI.InitialDataLoad(id.Value));
                return View(VmCommercial_B2BLcSupplierPI);
            }
            return View("VmCommercial_B2bLCIndex");
        }

        [HttpGet]
        public ActionResult VmCommercial_B2BLcSupplierPICreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_B2BLcSupplierPI = new VmCommercial_B2BLcSupplierPI();
                //...For dropdrown...///
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetCommercial_PIForDropDown();
                ViewBag.Commercial_PI = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmCommercial_B2BLcSupplierPI = new VmCommercial_B2BLcSupplierPI();
                VmCommercial_B2BLcSupplierPI.Commercial_B2BLcSupplierPI = new Commercial_B2BLcSupplierPI();
                VmCommercial_B2BLcSupplierPI.Commercial_B2BLcSupplierPI.Commercial_B2bLCFK = id.Value;
                VmCommercial_B2BLcSupplierPI.Commercial_B2BLcSupplierPI.Date = DateTime.Now;

                return View(VmCommercial_B2BLcSupplierPI);
            }

            return RedirectToAction("VmCommercial_B2BLcSupplierPIIndex/" + VmCommercial_B2BLcSupplierPI.Commercial_B2BLcSupplierPI.Commercial_B2bLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_B2BLcSupplierPICreate(VmCommercial_B2BLcSupplierPI a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_B2BLcSupplierPIIndex/" + a.Commercial_B2BLcSupplierPI.Commercial_B2bLCFK);
            }
            return RedirectToAction("VmCommercial_B2BLcSupplierPIIndex/" + a.Commercial_B2BLcSupplierPI.Commercial_B2bLCFK);
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_B2BLcSupplierPIEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommercial_PIForDropDown();
            ViewBag.Commercial_PI = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_B2BLcSupplierPI = new VmCommercial_B2BLcSupplierPI();
            await Task.Run(() => VmCommercial_B2BLcSupplierPI.SelectSingle(editId));

            return View(VmCommercial_B2BLcSupplierPI);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_B2BLcSupplierPIEdit(VmCommercial_B2BLcSupplierPI x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_B2BLcSupplierPIIndex/" + x.Commercial_B2BLcSupplierPI.Commercial_B2bLCFK);
            }
            return RedirectToAction("VmCommercial_B2BLcSupplierPIIndex", "Commercial");
        }

        public ActionResult VmCommercial_B2BLcSupplierPIDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_B2BLcSupplierPI b = new Commercial_B2BLcSupplierPI();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_B2BLcSupplierPIIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_B2bLCIndex", "Commercial");
        }

        //............Commercial_MasterLCBuyerPI..............//
        public async Task<ActionResult> VmCommercial_MasterLCBuyerPIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MasterLCBuyerPI = new VmCommercial_MasterLCBuyerPI();
                await Task.Run(() => VmCommercial_MasterLCBuyerPI = VmCommercial_MasterLCBuyerPI.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_MasterLCBuyerPI.InitialDataLoad(id.Value));
                return View(VmCommercial_MasterLCBuyerPI);
            }

            return View("VmCommercial_MasterLCIndex");
        }
        [HttpGet]
        public ActionResult VmCommercial_MasterLCBuyerPICreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MasterLCBuyerPI = new VmCommercial_MasterLCBuyerPI();
                VmCommercial_MasterLCBuyerPI = new VmCommercial_MasterLCBuyerPI();
                VmCommercial_MasterLCBuyerPI.Commercial_MasterLCBuyerPI = new Commercial_MasterLCBuyerPI();
                VmCommercial_MasterLCBuyerPI.Commercial_MasterLCBuyerPI.Commercial_MasterLCFK = id.Value;


                return View(VmCommercial_MasterLCBuyerPI);
            }

            return RedirectToAction("VmCommercial_MasterLCBuyerPIndex/" + VmCommercial_MasterLCBuyerPI.Commercial_MasterLCBuyerPI.Commercial_MasterLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MasterLCBuyerPICreate(VmCommercial_MasterLCBuyerPI a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_MasterLCBuyerPIndex/" + a.Commercial_MasterLCBuyerPI.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MasterLCBuyerPIndex/" + a.Commercial_MasterLCBuyerPI.Commercial_MasterLCFK);
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_MasterLCBuyerPIEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            //VmForDropDown = new VmForDropDown();
            //VmForDropDown.DDownData = VmForDropDown.GetMktPODropDown();
            //ViewBag.MktPO = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_MasterLCBuyerPI = new VmCommercial_MasterLCBuyerPI();
            await Task.Run(() => VmCommercial_MasterLCBuyerPI.SelectSingle(editId));

            return View(VmCommercial_MasterLCBuyerPI);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MasterLCBuyerPIEdit(VmCommercial_MasterLCBuyerPI x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_MasterLCBuyerPIndex/" + x.Commercial_MasterLCBuyerPI.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }
        public ActionResult VmCommercial_MasterLCBuyerPIDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_MasterLCBuyerPI b = new Commercial_MasterLCBuyerPI();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_MasterLCBuyerPIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }


        //............Commercial_MasterLCBuyerPO..............//
        public async Task<ActionResult> VmCommercial_MasterLCBuyerPOIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MasterLCBuyerPO = new VmCommercial_MasterLCBuyerPO();

                await Task.Run(() => VmCommercial_MasterLCBuyerPO = VmCommercial_MasterLCBuyerPO.GetSingleMasterLCByID(id.Value));
                await Task.Run(() => VmCommercial_MasterLCBuyerPO.InitialDataLoad(id.Value));
                await Task.Run(() => VmCommercial_MasterLCBuyerPO.GetBuyerCommission(id.Value));
                return View(VmCommercial_MasterLCBuyerPO);
            }

            return View("VmCommercial_MasterLCIndex");
        }
        [HttpGet]
        public ActionResult VmCommercial_MasterLCBuyerPOCreate(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            int mLCId = 0, buyerId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("+")), out mLCId);
            id = id.Replace(mLCId.ToString() + "+", "");
            int.TryParse(id, out buyerId);
            if (id != null)
            {
                VmCommercial_MasterLCBuyerPO = new VmCommercial_MasterLCBuyerPO();
                //...For dropdrown...///
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetOrderByBuyer(buyerId);
                ViewBag.MktBomCIDStyle = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                VmCommercial_MasterLCBuyerPO = new VmCommercial_MasterLCBuyerPO();
                VmCommercial_MasterLCBuyerPO.Commercial_MasterLCBuyerPO = new Commercial_MasterLCBuyerPO();
                VmCommercial_MasterLCBuyerPO.Commercial_MasterLCBuyerPO.Commercial_MasterLCFK = mLCId;


                return View(VmCommercial_MasterLCBuyerPO);
            }

            return RedirectToAction("VmCommercial_MasterLCBuyerPOIndex/" + VmCommercial_MasterLCBuyerPO.Commercial_MasterLCBuyerPO.Commercial_MasterLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MasterLCBuyerPOCreate(VmCommercial_MasterLCBuyerPO a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_MasterLCBuyerPOIndex/" + a.Commercial_MasterLCBuyerPO.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MasterLCBuyerPIndex/" + a.Commercial_MasterLCBuyerPO.Commercial_MasterLCFK);
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_MasterLCBuyerPOEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderByBuyerDropDown();
            ViewBag.MktBomCIDStyle = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_MasterLCBuyerPO = new VmCommercial_MasterLCBuyerPO();
            await Task.Run(() => VmCommercial_MasterLCBuyerPO.SelectSingle(editId));

            return View(VmCommercial_MasterLCBuyerPO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MasterLCBuyerPOEdit(VmCommercial_MasterLCBuyerPO x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_MasterLCBuyerPOIndex/" + x.Commercial_MasterLCBuyerPO.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }
        public ActionResult VmCommercial_MasterLCBuyerPODelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_MasterLCBuyerPO b = new Commercial_MasterLCBuyerPO();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_MasterLCBuyerPOIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }



        //............Commercial_MLCRealization..............//
        public async Task<ActionResult> VmCommercial_MLCRealizationIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_MLCRealization = new VmCommercial_MLCRealization();
                await Task.Run(() => VmCommercial_MLCRealization = VmCommercial_MLCRealization.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_MLCRealization.InitialDataLoad(id.Value));
                return View(VmCommercial_MLCRealization);
            }

            return View("VmCommercial_MasterLCIndex");
        }
        [HttpGet]
        public ActionResult VmCommercial_MLCRealizationCreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {

                VmCommercial_MLCRealization = new VmCommercial_MLCRealization();
                VmCommercial_MLCRealization.Commercial_MLCRealization = new Commercial_MLCRealization();
                VmCommercial_MLCRealization.Commercial_MLCRealization.Commercial_MasterLCFK = id.Value;

                VmCommercial_MLCRealization.Commercial_MLCRealization.InvoiceDate = DateTime.Today;
                VmCommercial_MLCRealization.Commercial_MLCRealization.ExportDate = DateTime.Today;
                VmCommercial_MLCRealization.Commercial_MLCRealization.BLDate = DateTime.Today;
                VmCommercial_MLCRealization.Commercial_MLCRealization.FDBPDate = DateTime.Today;
                VmCommercial_MLCRealization.Commercial_MLCRealization.CourierDate = DateTime.Today;
                VmCommercial_MLCRealization.Commercial_MLCRealization.Date = DateTime.Today;


                return View(VmCommercial_MLCRealization);
            }

            return RedirectToAction("VmCommercial_MLCRealizationIndex/" + VmCommercial_MLCRealization.Commercial_MLCRealization.Commercial_MasterLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLCRealizationCreate(VmCommercial_MLCRealization a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_MLCRealizationIndex/" + a.Commercial_MLCRealization.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MLCRealizationIndex/" + a.Commercial_MLCRealization.Commercial_MasterLCFK);
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_MLCRealizationEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            VmCommercial_MLCRealization = new VmCommercial_MLCRealization();
            await Task.Run(() => VmCommercial_MLCRealization.SelectSingle(editId));

            return View(VmCommercial_MLCRealization);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_MLCRealizationEdit(VmCommercial_MLCRealization x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_MLCRealizationIndex/" + x.Commercial_MLCRealization.Commercial_MasterLCFK);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");


        }
        public ActionResult VmCommercial_MLCRealizationDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_MLCRealization b = new Commercial_MLCRealization();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_MLCRealizationIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_MasterLCIndex", "Commercial");
        }

        //..............Summary Report...............//

        [HttpGet]
        public async Task<ActionResult> VmSummaryReport()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_Report = new VmCommercial_Report();
            await Task.Run(() => VmCommercial_Report.GetSummaryReport());
            return View(VmCommercial_Report);
        }

        [HttpGet]
        public async Task<ActionResult> VmSummaryReportIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmCommercial_Report = new VmCommercial_Report();
            VmCommercial_Report.SummaryReport = new SummaryReport();

            DateTime fromdate = new DateTime(2017, 1, 1);
            VmCommercial_Report.SummaryReport.FromDate = fromdate;
            VmCommercial_Report.SummaryReport.ToDate = DateTime.Today;
            await Task.Run(() => VmCommercial_Report.GetPeriodicSummaryReport(VmCommercial_Report.SummaryReport.FromDate, VmCommercial_Report.SummaryReport.ToDate));
            return View(VmCommercial_Report);
        }

        [HttpPost]
        public async Task<ActionResult> VmSummaryReportIndex(VmCommercial_Report VmCommercial_Report)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmCommercial_Report.GetPeriodicSummaryReport(VmCommercial_Report.SummaryReport.FromDate, VmCommercial_Report.SummaryReport.ToDate));
            }
            return View(VmCommercial_Report);
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_MasterLCDetails()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_MasterLC = new VmCommercial_MasterLC();
            await Task.Run(() => VmCommercial_MasterLC.InitialDataLoad());
            return View("VmCommercial_MasterLCIndex", VmCommercial_MasterLC);
        }

        //..............Import Status..............//

        public async Task<ActionResult> VmCommercial_ImportIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_Import = new VmCommercial_Import();
                await Task.Run(() => VmCommercial_Import = VmCommercial_Import.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_Import.InitialDataLoad(id.Value));
                return View(VmCommercial_Import);
            }
            return View("VmCommercial_B2bLCIndex");
        }
        [HttpGet]
        public ActionResult VmCommercial_ImportCreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {

                //...For dropdrown...///
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
                ViewBag.Common_UnitFK = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                VmForDropDown.DDownData = VmForDropDown.GetShipmentMode();
                ViewBag.ShipmentMode = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                VmCommercial_Import = new VmCommercial_Import();
                VmCommercial_Import.Commercial_Import = new Commercial_Import();
                VmCommercial_Import.Commercial_Import.Commercial_B2bLCFK = id.Value;
                VmCommercial_Import.Commercial_Import.DocDate = DateTime.Now;
                VmCommercial_Import.Commercial_Import.ETADate = DateTime.Now;
                VmCommercial_Import.Commercial_Import.ETDDate = DateTime.Now;

                return View(VmCommercial_Import);
            }

            return RedirectToAction("VmCommercial_ImportIndex/" + VmCommercial_Import.Commercial_Import.Commercial_B2bLCFK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_ImportCreate(VmCommercial_Import a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_ImportIndex/" + a.Commercial_Import.Commercial_B2bLCFK);
            }
            return RedirectToAction("VmCommercial_ImportIndex/" + a.Commercial_Import.Commercial_B2bLCFK);
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_ImportEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            ViewBag.Common_UnitFK = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///


            VmCommercial_Import = new VmCommercial_Import();
            await Task.Run(() => VmCommercial_Import.SelectSingle(editId));

            return View(VmCommercial_Import);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_ImportEdit(VmCommercial_Import x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmCommercial_ImportIndex/" + x.Commercial_Import.Commercial_B2bLCFK);
            }
            return RedirectToAction("VmCommercial_B2bLCIndex", "Commercial");
        }
        public ActionResult VmCommercial_ImportDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_Import b = new Commercial_Import();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_ImportIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_B2bLCIndex", "Commercial");
        }


        //..............Import Status Report..............//

        public ActionResult VmImportReportIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetFilterType();
            ViewBag.Filter = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmImportReport = new VmImportReport();
            VmImportReport.VmFilter = new VmFilter();
            VmImportReport.VmFilter.FromDate = DateTime.Now.AddMonths(-6);
            VmImportReport.VmFilter.ToDate = DateTime.Now;
            return View(VmImportReport);
        }
        [HttpPost]
        public async Task<ActionResult> VmImportReportIndex(VmImportReport model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetFilterType();
            ViewBag.Filter = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            if (ModelState.IsValid)
            {
                int result = DateTime.Compare(model.VmFilter.FromDate, model.VmFilter.ToDate);
                if (result < 0)
                {
                    await Task.Run(() => model.ImportReportLoad(model.VmFilter));
                }
                else
                {
                    ModelState.AddModelError("error", "From date cannot be grater than to date.");
                }
            }

            return View(model);
        }

        //........................Bills Of Exchange...............//

        public async Task<ActionResult> VmCommercial_BillOfExchangeIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            VmCommercial_BillOfExchange = new VmCommercial_BillOfExchange();
            VmCommercial_BillOfExchange.VmCommercial_UDSlave = new VmCommercial_UDSlave();
            //await Task.Run(() => VmCommercial_BillOfExchange.VmCommercial_UDSlave = VmCommercial_BillOfExchange.VmCommercial_UDSlave.SelectSingleJoined(id.Value));
            //await Task.Run(() => VmCommercial_BillOfExchange.VmCommercial_UDSlave.InitialDataLoad(id.Value));
            await Task.Run(() => VmCommercial_BillOfExchange.InitialDataLoad());

            return View(VmCommercial_BillOfExchange);

        }

        [HttpGet]
        public ActionResult VmCommercial_BillOfExchangeCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_BillOfExchange VmCommercial_BillOfExchange = new VmCommercial_BillOfExchange();
            VmCommercial_BillOfExchange.Commercial_BillOfExchange = new Commercial_BillOfExchange();

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetShipmentBillOfExchageType();
            ViewBag.BillOfExchange = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmForDropDown.DDownData = VmForDropDown.GetCommercial_MasterLCForDropDown();
            ViewBag.MasterLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_BillOfExchange.Commercial_BillOfExchange.BillDate = DateTime.Today;
            VmCommercial_BillOfExchange.Commercial_BillOfExchange.CourierDate = DateTime.Today;
            VmCommercial_BillOfExchange.Commercial_BillOfExchange.FDBCDate = DateTime.Today;
            return View(VmCommercial_BillOfExchange);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BillOfExchangeCreate(VmCommercial_BillOfExchange a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_BillOfExchangeIndex");
            }
            return RedirectToAction("VmCommercial_BillOfExchangeIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_BillOfExchangeEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetShipmentBillOfExchageType();
            ViewBag.BillOfExchange = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmForDropDown.DDownData = VmForDropDown.GetCommercial_MasterLCForDropDown();
            ViewBag.MasterLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmCommercial_BillOfExchange = new VmCommercial_BillOfExchange();


            await Task.Run(() => VmCommercial_BillOfExchange.SelectSingle(id));

            return View(VmCommercial_BillOfExchange);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BillOfExchangeEdit(VmCommercial_BillOfExchange x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(UserID));


                return RedirectToAction("VmCommercial_BillOfExchangeIndex", "Commercial");
            }
            return RedirectToAction("VmCommercial_BillOfExchangeIndex", "Commercial");
        }

        public ActionResult VmCommercial_BillOfExchangeDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Commercial_BillOfExchange b = new Commercial_BillOfExchange();
            b.ID = id;
            b.Delete(0);

            return RedirectToAction("VmCommercial_BillOfExchangeIndex", "Commercial");
        }

        //..............Invoice..................//
        public async Task<ActionResult> VmCommercial_InvoiceIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmCommercial_Invoice = new VmCommercial_Invoice();
            //VmCommercial_MasterLCBuyerPO = new VmCommercial_MasterLCBuyerPO();
            ///await Task.Run(() => VmCommercial_Invoice = VmCommercial_Invoice.SelectSingleJoined(id.Value));


            VmCommercial_Invoice.VmCommercial_UDSlave = new VmCommercial_UDSlave();
            //await Task.Run(() => VmCommercial_Invoice.VmCommercial_UDSlave = VmCommercial_Invoice.VmCommercial_UDSlave.SelectSingleJoined());
            //await Task.Run(() => VmCommercial_Invoice.VmCommercial_UDSlave.InitialDataLoad(id.Value));
            await Task.Run(() => VmCommercial_Invoice.InitialDataLoad());
            //VmCommercial_Invoice.VmCommercial_MasterLCBuyerPO = new VmCommercial_MasterLCBuyerPO();
            // await Task.Run(() => VmCommercial_Invoice.VmCommercial_MasterLCBuyerPO.GetBuyerCommission(id.Value));

            return View(VmCommercial_Invoice);

        }

        [HttpGet]
        public ActionResult VmCommercial_InvoiceCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            VmCommercial_Invoice = new VmCommercial_Invoice();
            VmCommercial_Invoice.Commercial_Invoice = new Commercial_Invoice();
            VmCommercial_Invoice.Commercial_Invoice.InvoiceDate = DateTime.Now;
            VmCommercial_Invoice.Commercial_Invoice.EstimatedReceivedDate = DateTime.Now.AddDays(45);
            VmCommercial_Invoice.Commercial_Invoice.BLDate = DateTime.Now;
            VmCommercial_Invoice.Commercial_Invoice.ExpDate = DateTime.Now;
            VmCommercial_Invoice.Commercial_Invoice.ErcNo = "RA-0106831";

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Country = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetPortOfLoadingDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PortLoading = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetPortOfDischargeDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Discharge = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetShippedByDropdown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ShippedPer = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetTermsOfShipmentDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.TermOfShipment = sl5;


            return View(VmCommercial_Invoice);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_InvoiceCreate(VmCommercial_Invoice a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_InvoiceIndex");
            }
            return RedirectToAction("VmCommercial_InvoiceIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_InvoiceEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Country = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetPortOfLoadingDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PortLoading = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetPortOfDischargeDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Discharge = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetShippedByDropdown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ShippedPer = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetTermsOfShipmentDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.TermOfShipment = sl5;

            VmCommercial_Invoice = new VmCommercial_Invoice();
            await Task.Run(() => VmCommercial_Invoice.SelectSingle(id));
            return View(VmCommercial_Invoice);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_InvoiceEdit(VmCommercial_Invoice x)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(UserID));
                return RedirectToAction("VmCommercial_InvoiceIndex");
            }
            return RedirectToAction("VmCommercial_MasterLCIndex");


        }

        public ActionResult VmCommercial_InvoiceDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Commercial_Invoice b = new Commercial_Invoice();
            b.ID = id;
            b.Delete(0);

            var slavedata = db.Commercial_InvoiceSlave.Where(x=>x.Commercial_InvoiceFk==id).ToList();
            if (slavedata!=null)
            {
                foreach(var data in slavedata)
                {
                    Commercial_InvoiceSlave slave = new Commercial_InvoiceSlave();
                    slave.ID = data.ID;
                    slave.Delete(0);
                }
            }
            return RedirectToAction("VmCommercial_InvoiceIndex", "Commercial");
        }

        //.................Invoice_Slave.............//

        public async Task<ActionResult> VmCommercial_InvoiceSlaveIndex(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_InvoiceSlave = new VmCommercial_InvoiceSlave();
                await Task.Run(() => VmCommercial_InvoiceSlave = VmCommercial_InvoiceSlave.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_InvoiceSlave.InitialDataLoad(id.Value));
                return View(VmCommercial_InvoiceSlave);
            }
            return View("VmCommercial_MasterLCIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_InvoiceSlaveCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetChallanList();
                SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.DelivaryChallan = sl6;

                VmCommercial_InvoiceSlave = new VmCommercial_InvoiceSlave();
                VmCommercial_InvoiceSlave.Commercial_InvoiceSlave = new Commercial_InvoiceSlave();
                VmCommercial_InvoiceSlave.Commercial_InvoiceSlave.Commercial_InvoiceFk = id;
                return View(VmCommercial_InvoiceSlave);
            }

            return RedirectToAction("VmCommercial_InvoiceSlaveIndex", "Commercial");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_InvoiceSlaveCreate(VmCommercial_InvoiceSlave a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_InvoiceSlaveIndex/" + a.Commercial_InvoiceSlave.Commercial_InvoiceFk);
            }
            return RedirectToAction("VmCommercial_InvoiceSlaveIndex/" + a.Commercial_InvoiceSlave.Commercial_InvoiceFk);
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_InvoiceSlaveEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetChallanList();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.DelivaryChallan = sl6;



            VmCommercial_InvoiceSlave = new VmCommercial_InvoiceSlave();
            await Task.Run(() => VmCommercial_InvoiceSlave.SelectSingle(id));

            VmForDropDown.DDownData = VmForDropDown.GetShipmentOrderDeliverdScheduleByBom(VmCommercial_InvoiceSlave.Commercial_InvoiceSlave.Shipment_CNFDelivaryChallanFk);
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ShipmentOrderDeliverdSchedule = sl7;

            return View(VmCommercial_InvoiceSlave);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_InvoiceSlaveEdit(VmCommercial_InvoiceSlave x)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(UserID));


                return RedirectToAction("VmCommercial_InvoiceSlaveIndex/" + x.Commercial_InvoiceSlave.Commercial_InvoiceFk);
            }
            return RedirectToAction("VmCommercial_InvoiceIndex", "Commercial");


        }

        public ActionResult VmCommercial_InvoiceSlaveDelete(int id, int invoiceId)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Commercial_InvoiceSlave b = new Commercial_InvoiceSlave();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_InvoiceSlaveIndex/" + invoiceId);

        }

        public async Task<ActionResult> PurchaseOrdersWithoutBuyerOrder(VmMkt_PO VmMkt_PO, int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string SearchString = VmMkt_PO.SearchString;
            int theID = 0;
            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.GetPOWithoutOrder(theID));
            if (VmMkt_PO.POHeader == "Purchase Orders :")
            {
                if (id != null && id.Value <= 2)
                {
                    VmMkt_PO.Header = "Purchase Orders";
                    theID = id.Value;
                }
                return View(VmMkt_PO);
            }
            return View(VmMkt_PO);
        }

        [HttpPost]
        public async Task<ActionResult> PurchaseOrdersWithoutBuyerOrder(VmMkt_PO VmMkt_PO)
        {
            string SearchString = VmMkt_PO.SearchString;

            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.PurchaseOrderSearch(SearchString));

            return View(VmMkt_PO);

        }

        [HttpGet]
        public ActionResult PurchaseOrdersWithoutBuyerOrderCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();


            VmForDropDown = new VmForDropDown();
            VmForDropDown.User_User = vMUser.User_User;
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl2;


            VmForDropDown.DDownData = VmForDropDown.GetRequisitionDropDownCID();
            SelectList sR = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RequisitionCID = sR;

            vmMkt_PO = new VmMkt_PO();
            vmMkt_PO.Mkt_PO = new Mkt_PO();
            vmMkt_PO.Mkt_PO.Date = DateTime.Today;
            vmMkt_PO.Mkt_PO.Mkt_BOMFK = 1;
            return View(vmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PurchaseOrdersWithoutBuyerOrderCreate(VmMkt_PO a)
        {

            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            a.Mkt_PO.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            UserID = vmControllerHelper.GetCurrentUser().User_User.ID;

            if (ModelState.IsValid)
            {
                a.Mkt_PO.Status = 0;
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("PurchaseOrdersWithoutBuyerOrder");
            }
            return RedirectToAction("PurchaseOrdersWithoutBuyerOrder");
        }

        [HttpGet]
        public async Task<ActionResult> PurchaseOrdersWithoutBuyerOrderEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl2;

            vmMkt_PO = new VmMkt_PO();
            await Task.Run(() => vmMkt_PO.SelectSinglePO(id.Value));

            if (vmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(vmMkt_PO);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PurchaseOrdersWithoutBuyerOrderEdit(Mkt_PO Mkt_PO)
        {
            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            Mkt_PO.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;

            if (ModelState.IsValid)
            {
                await Task.Run(() => Mkt_PO.Edit(0));
                return RedirectToAction("PurchaseOrdersWithoutBuyerOrder");
            }
            return View(vmMkt_PO);
        }

        public ActionResult PurchaseOrdersWithoutBuyerDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Mkt_PO mkt_PO = new Mkt_PO();
            mkt_PO.ID = id;
            mkt_PO.Delete(0);
            VmMktPO_Slave = new VmMktPO_Slave();
            VmMktPO_Slave.DaleteBulk(UserID, id);

            return RedirectToAction("PurchaseOrdersWithoutBuyerOrder");
        }

        public async Task<ActionResult> PurchaseOrdersWithoutBuyerOrderSlave(int id)
        {

            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            vmMkt_PO = new VmMkt_PO();
            await Task.Run(() => vmMkt_PO = vmMkt_PO.SelectSingleJoined(id));

            Mkt_POSlave pos = new Mkt_POSlave();
            pos.Mkt_POFK = id;

            vmMkt_PO.VmMktPO_Slave = new VmMktPO_Slave();
            await Task.Run(() => vmMkt_PO.VmMktPO_Slave.GetPOSlaveWithoutOrder(id));
            vmMkt_PO.TotalVal = vmMkt_PO.VmMktPO_Slave.DataList.Sum(x => x.LineTotal);
            return View(vmMkt_PO);
        }
        [HttpGet]
        public ActionResult VmMktPO_SlaveWithoutBuyerOrderCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetMktPODropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PO = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            ViewModels.Store.VmMktPO_Slave vmMkt_POSlave = new VmMktPO_Slave();
            vmMkt_POSlave.Mkt_POSlave = new Mkt_POSlave();
            vmMkt_POSlave.Mkt_POSlave.Mkt_POFK = id;
            vmMkt_POSlave.Mkt_POSlave.Mkt_BOMSlaveFK = 1;
            vmMkt_POSlave.Mkt_POSlave.TotalRequired = 1;
            vmMkt_POSlave.Mkt_POSlave.Consumption = 1;
            vmMkt_POSlave.Mkt_POSlave.Common_UnitFK = 15; //"PCS"

            return View(vmMkt_POSlave);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMktPO_SlaveWithoutBuyerOrderCreate(Mkt_POSlave mkt_POSlave)
        {
            if (ModelState.IsValid)
            {
                mkt_POSlave.AddReady(UserID);
                await Task.Run(() => mkt_POSlave.Add());
                return RedirectToAction("PurchaseOrdersWithoutBuyerOrderSlave/" + mkt_POSlave.Mkt_POFK);
            }
            return RedirectToAction("PurchaseOrdersWithoutBuyerOrderSlave/" + mkt_POSlave.Mkt_POFK + mkt_POSlave);
        }

        [HttpGet]
        public async Task<ActionResult> VmMktPOSlaveWithoutBuyerOrderEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMktPODropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PO = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmMktPO_Slave = new VmMktPO_Slave();
            await Task.Run(() => VmMktPO_Slave.SelectSingleSlaveOfCommercial(id.Value));
            if (VmMktPO_Slave.Mkt_POSlave == null)
            {
                return HttpNotFound();
            }
            //no chg

            VmForDropDown.DDownData = VmForDropDown.GetTempRaw_ItemForEdit(VmMktPO_Slave.Mkt_POSlave.Raw_ItemFK);

            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Item = sl2;
            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryDropDownForEdit(VmMktPO_Slave.Mkt_POSlave.Raw_ItemFK.ToString());
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryDropDownForEdit(VmMktPO_Slave.Mkt_POSlave.Raw_ItemFK);
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;
            return View(VmMktPO_Slave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMktPOSlaveWithoutBuyerOrderEdit(VmMktPO_Slave VmMktPO_Slave)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmMktPO_Slave.Edit(0));
                return RedirectToAction("PurchaseOrdersWithoutBuyerOrderSlave/" + VmMktPO_Slave.Mkt_POSlave.Mkt_POFK);
            }
            return View(VmMktPO_Slave);
        }

        public ActionResult VmMkt_POSlaveWithoutBuyerOrderDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Mkt_POSlave b = new Mkt_POSlave();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("PurchaseOrdersWithoutBuyerOrderSlave/" + id);
        }
        //..............Invoice Inventory Report.........../

        public ActionResult POReportWithoutBuyerOrderDoc(int? id)
        {

            if (!id.HasValue)
            {
                IdNullDivertion("POReportDoc", "Id is Null");
            }
            try
            {
                VmMkt_PO p = new VmMkt_PO();
                p.Mkt_PO = new Mkt_PO();
                p.SelectSingle(id.Value);

                //if (p.Mkt_PO.IsAuthorize == false)
                // {
                //return Redirect("/Home/ErrorView/This PO is not authorized yet. Please Contact Admin");
                //NotAuthorisedDivertion("POReportDoc", "This PO is not Authorized yet!");
                // }
                // else
                // {

                p.POReportWithoutBuyerOrderDocLoad(id.Value);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/POBaseWithoutBuyerOrderDoc.rpt");
                cr.Load();
                cr.SetDataSource(p.POReportDoc);
                cr.SummaryInfo.ReportTitle = "Report Of " + p.Mkt_PO.CID;
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
                // }
            }

            catch (Exception ex)
            {
                IdNullDivertion("POReportDoc", ex.Message);
            }
            return null;
        }

        private ActionResult IdNullDivertion(string methodName, string messege)
        {
            ErrorReports er = new ErrorReports();
            er.ControllerName = this.ToString();
            er.MethodName = methodName;
            er.ErrorDescription = "Id is Null";
            er.EmailErrorAuto();
            return View("../Home/ErrorView", er);
        }

        public ActionResult BillOfExchangeDoc(int id)
        {
            try
            {
                VmCommercial_BillOfExchange b = new VmCommercial_BillOfExchange();
                /////
                //VmCommon_TheOrder y = new VmCommon_TheOrder();
                //y.Common_TheOrder = new Common_TheOrder();
                //b.Mkt_BOM = new Mkt_BOM();
                ////b.SelectSingle(id);
                //y.SelectSingle(b.Mkt_BOM.Common_TheOrderFk.Value.ToString());
                //b.Common_TheOrder = new Common_TheOrder();
                b.BillOfExchangeDocDataLoad(id);
                ///////
                int ss = 0;
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/BillOfExchangeDoc.rpt");
                cr.Load();
                cr.SetDataSource(b.BillOfExchangeReportDoc);
                /////
                cr.SummaryInfo.ReportTitle = "Report of bill of exchange";

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            return null;
        }

        public ActionResult LCStatementDoc(int id)
        {
            try
            {
                VmCommercial_MasterLC b = new VmCommercial_MasterLC();
                b.LCStatementDocDataLoad(id);
                b.GetBTBByUdLcStatement(id);
                b.GetUnPaidBTBByUd(id);
                ReportClass cr = new ReportClass();

                cr.FileName = Server.MapPath("~/Views/Reports/LCStatement.rpt");
                cr.Load();

                cr.SetDataSource(b.LCStetmentDoc1);
                //cr.SetDataSource(b.LCStetmentDoc2);
                //b.SelectSingle(id);
                /////
                cr.SummaryInfo.ReportTitle = "L/C STATEMENT";
                cr.Subreports["BTBDoc.rpt"].SetDataSource(b.LCStetmentDoc2);
                cr.Subreports["BTBUnpaidUD.rpt"].SetDataSource(b.LCStetmentDoc3);

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            return null;
        }



        //.........Commercial_BillOfExchangeSlave..........//

        public async Task<ActionResult> VmCommercial_BillOfExchangeSlaveIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            if (id != null)
            {
                VmCommercial_BillOfExchangeSlave = new VmCommercial_BillOfExchangeSlave();
                await Task.Run(() => VmCommercial_BillOfExchangeSlave = VmCommercial_BillOfExchangeSlave.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_BillOfExchangeSlave.InitialDataLoad(id.Value));
                return View(VmCommercial_BillOfExchangeSlave);
            }

            return View("VmCommercial_MasterLCIndex");
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_BillOfExchangeSlaveCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            if (id != null)
            {

                VmCommercial_BillOfExchange = new VmCommercial_BillOfExchange();
                await Task.Run(() => VmCommercial_BillOfExchange.SelectSingle(id));

                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetInvoiceForDropDown(VmCommercial_BillOfExchange.Commercial_BillOfExchange.Commercial_MasterLCFK);
                ViewBag.Invoice = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmCommercial_BillOfExchangeSlave = new VmCommercial_BillOfExchangeSlave();
                VmCommercial_BillOfExchangeSlave.Commercial_BillOfExchangeSlave = new Commercial_BillOfExchangeSlave();

                VmCommercial_BillOfExchangeSlave.Commercial_BillOfExchangeSlave.Commercial_BillOfExchangeFk = id;
                return View(VmCommercial_BillOfExchangeSlave);


            }

            return RedirectToAction("VmCommercial_BillOfExchangeIndex", "Commercial");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BillOfExchangeSlaveCreate(VmCommercial_BillOfExchangeSlave a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommercial_BillOfExchangeSlaveIndex/" + a.Commercial_BillOfExchangeSlave.Commercial_BillOfExchangeFk);
            }
            return RedirectToAction("VmCommercial_BillOfExchangeSlaveIndex/" + a.Commercial_BillOfExchangeSlave.Commercial_BillOfExchangeFk);
        }
        [HttpGet]
        public async Task<ActionResult> VmCommercial_BillOfExchangeSlaveEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmCommercial_BillOfExchangeSlave = new VmCommercial_BillOfExchangeSlave();
            await Task.Run(() => VmCommercial_BillOfExchangeSlave.SelectSingle(id));

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetInvoiceForDropDown(VmCommercial_BillOfExchangeSlave.Commercial_BillOfExchange.Commercial_MasterLCFK);
            ViewBag.Invoice = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            return View(VmCommercial_BillOfExchangeSlave);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BillOfExchangeSlaveEdit(VmCommercial_BillOfExchangeSlave x)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(UserID));


                return RedirectToAction("VmCommercial_BillOfExchangeSlaveIndex/" + x.Commercial_BillOfExchangeSlave.Commercial_BillOfExchangeFk);
            }
            return RedirectToAction("VmCommercial_BillOfExchangeIndex", "Commercial");


        }
        public ActionResult VmCommercial_BillOfExchangeSlaveDelete(int id, int invoiceId)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Commercial_BillOfExchangeSlave b = new Commercial_BillOfExchangeSlave();
            b.ID = id;
            b.Delete(0);

            return RedirectToAction("VmCommercial_BillOfExchangeSlaveIndex/" + invoiceId);
        }

        public ActionResult Yarnview()
        {

            return View();

        }





        //.........Commercial_UD..............//

        public async Task<ActionResult> VmCommercial_UDIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_UD = new VmCommercial_UD();
            await Task.Run(() => VmCommercial_UD.InitialDataLoad());
            return View(VmCommercial_UD);
        }

        public async Task<ActionResult> VmCommercial_CloseUDIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_UD = new VmCommercial_UD();
            await Task.Run(() => VmCommercial_UD.ClosedUdInitialDataLoad());
            return View(VmCommercial_UD);
        }
        [HttpGet]
        public ActionResult VmCommercial_UDCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_UD = new VmCommercial_UD();
            VmCommercial_UD.Commercial_UD = new Commercial_UD();
            VmCommercial_UD.Commercial_UD.Date = DateTime.Now;

            return View(VmCommercial_UD);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_UDCreate(VmCommercial_UD a)
        {
            if (ModelState.IsValid)
            {
                // chart of account integration
                try
                {
                    VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                    {
                        UserId = UserID,
                        AccountName = "UD Account No. " + a.Commercial_UD.UdNo,
                        Chart2Id = 179
                    };
                    a.Commercial_UD.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.AddFromIntegratedModule());
                }
                catch (Exception e)
                {
                    a.Commercial_UD.Acc_AcNameFk = 0;
                }


                await Task.Run(() => a.Add(UserID));

                return RedirectToAction("VmCommercial_UDIndex");
            }
            return RedirectToAction("VmCommercial_UDIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmCommercial_UDEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            VmCommercial_UD = new VmCommercial_UD();
            await Task.Run(() => VmCommercial_UD.SelectSingle(id.Value));


            if (VmCommercial_UD.Commercial_UD == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_UD);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_UDEdit(VmCommercial_UD a)
        {
            if (ModelState.IsValid)
            {

                // chart of account integration
                try
                {
                    VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                    {
                        UserId = UserID,
                        AccountName = "UD Account No. " + a.Commercial_UD.UdNo,
                        Chart2Id = 179,
                        ExistAccId = a.Commercial_UD.Acc_AcNameFk
                    };
                    a.Commercial_UD.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.EditFromIntegratedModule());

                }
                catch (Exception e)
                {
                    a.Commercial_MasterLC.Acc_AcNameFk = 0;
                }

                await Task.Run(() => a.Edit(UserID));
                return RedirectToAction("VmCommercial_UDIndex");
            }
            return View(VmCommercial_UD);
        }
        public async Task<ActionResult> VmCommercial_UDClose(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_UD a = new VmCommercial_UD();
            var data = db.Commercial_UD.Find(id);
            data.IsClose = true;
            a.Commercial_UD = data;
            await Task.Run(() => a.Edit(UserID));
            return RedirectToAction("VmCommercial_UDIndex");
        }
        public async Task<ActionResult> VmCommercial_UDReOpen(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_UD a = new VmCommercial_UD();
            var data = db.Commercial_UD.Find(id);
            data.IsClose = false;
            a.Commercial_UD = data;
            await Task.Run(() => a.Edit(UserID));
            return RedirectToAction("VmCommercial_CloseUDIndex");
        }
        public ActionResult VmCommercial_UDDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_UD b = new Commercial_UD();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_UDIndex");
        }


        //................VmCommercial_UDSlave.......................//
        public async Task<ActionResult> VmCommercial_UDSlaveIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommercial_UDSlave = new VmCommercial_UDSlave();
                await Task.Run(() => VmCommercial_UDSlave = VmCommercial_UDSlave.SelectSingleJoined(id.Value));
                await Task.Run(() => VmCommercial_UDSlave.InitialDataLoad(id.Value));

                VmCommercial_UDSlave.VmCommercial_B2bLC = new VmCommercial_B2bLC();
                await Task.Run(() => VmCommercial_UDSlave.VmCommercial_B2bLC.GetB2BLCByUD(id.Value));

                return View(VmCommercial_UDSlave);
            }

            return View("VmCommercial_UDIndex");
        }


        [HttpGet]
        public ActionResult VmCommercial_UDSlaveCreate(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetMasterLCWithoutUD();
                ViewBag.MasterLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");

                VmCommercial_UDSlave = new VmCommercial_UDSlave();
                VmCommercial_UDSlave.Commercial_UDSlave = new Commercial_UDSlave();
                VmCommercial_UDSlave.Commercial_UDSlave.Commercial_UDFK = id.Value;

                return View(VmCommercial_UDSlave);
            }

            return RedirectToAction("VmCommercial_UDSlaveIndex");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_UDSlaveCreate(VmCommercial_UDSlave a)
        {
            if (ModelState.IsValid)
            {
                var value = await Task.Run(() => a.Add(UserID));
                if (value >= 0)
                {
                    // journal integration
                    try
                    {
                        var commercial_MasterLC = db.Commercial_MasterLC.Find(a.Commercial_UDSlave.Commercial_MasterLCFK);
                        var commercial_UD = db.Commercial_UD.Find(a.Commercial_UDSlave.Commercial_UDFK);
                        var b2btotalamt = (commercial_MasterLC.TotalValue * commercial_UD.BTBOpening) / 100;
                        this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                        {
                            UserId = this.UserID,
                            Date = commercial_UD.Date,
                            Title = "UD Transfer",
                            FromAccNo = commercial_MasterLC.Acc_AcNameFk,
                            ToAccNo = commercial_UD.Acc_AcNameFk,
                            Amount = (decimal)(b2btotalamt ?? 0),
                            Descriptions = "Master L/C: " + commercial_MasterLC.Name + " Transfer to UD A/C : " + commercial_UD.UdNo,
                            TableIdFk = VmAcc_Database_Table.TableName(a.Commercial_UDSlave.GetType().Name),
                            TableRowIdFk = value,
                        };
                        var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
                    }
                    catch (Exception e)
                    {

                    }
                }

                return RedirectToAction("VmCommercial_UDSlaveIndex/" + a.Commercial_UDSlave.Commercial_UDFK);
            }
            return RedirectToAction("VmCommercial_UDSlaveIndex/" + a.Commercial_UDSlave.Commercial_UDFK);
        }

        [HttpGet]
        public async Task<ActionResult> VmCommercial_UDSlaveEdit(string id)
        {
            int editId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);


            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMasterLCWithoutUD1();
            ViewBag.MasterLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmCommercial_UDSlave = new VmCommercial_UDSlave();

            await Task.Run(() => VmCommercial_UDSlave.SelectSingle(editId));

            return View(VmCommercial_UDSlave);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_UDSlaveEdit(VmCommercial_UDSlave x)
        {

            if (ModelState.IsValid)
            {
                var value = await Task.Run(() => x.Edit(UserID));

                if (value)
                {
                    // journal integration
                    try
                    {
                        var commercial_MasterLC =
                            db.Commercial_MasterLC.Find(x.Commercial_UDSlave.Commercial_MasterLCFK);
                        var commercial_UD = db.Commercial_UD.Find(x.Commercial_UDSlave.Commercial_UDFK);
                        var b2btotalamt = (commercial_MasterLC.TotalValue * commercial_UD.BTBOpening) / 100;
                        this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                        {
                            UserId = this.UserID,
                            Date = commercial_UD.Date,
                            Title = "UD Transfer",
                            FromAccNo = commercial_MasterLC.Acc_AcNameFk,
                            ToAccNo = commercial_UD.Acc_AcNameFk,
                            Amount = (decimal)(b2btotalamt ?? 0),
                            Descriptions =
                                "Master L/C: " + commercial_MasterLC.Name + " Transfer to UD A/C : " +
                                commercial_UD.UdNo,
                            TableIdFk = VmAcc_Database_Table.TableName(x.Commercial_UDSlave.GetType().Name),
                            TableRowIdFk = x.Commercial_UDSlave.ID,
                        };
                        var journalstatus = await Task.Run(() =>
                            this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
                    }
                    catch (Exception e)
                    {

                    }
                }
                return RedirectToAction("VmCommercial_UDSlaveIndex/" + x.Commercial_UDSlave.Commercial_UDFK);
            }
            return RedirectToAction("VmCommercial_UDIndex", "Commercial");


        }
        public async Task<ActionResult> VmCommercial_UDSlaveDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Commercial_UDSlave b = new Commercial_UDSlave();
            b.ID = deleteId;
            b.Delete(0);

            this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
            {
                TableIdFk = VmAcc_Database_Table.TableName(b.GetType().Name),
                TableRowIdFk = b.ID,
            };
            var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.DeleteJounalFromIntegratedTable());


            if (redirictId != null)
            {
                return RedirectToAction("VmCommercial_UDSlaveIndex/" + redirictId);
            }
            return RedirectToAction("VmCommercial_UDIndex", "Commercial");
        }

        //.............Commercial_LCType.............//
        public async Task<ActionResult> VmCommercial_LCTypeIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_LCType = new VmCommercial_LCType();
            await Task.Run(() => VmCommercial_LCType.InitialDataLoad());
            return View(VmCommercial_LCType);
        }
        [HttpGet]
        public ActionResult VmCommercial_LCTypeCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_LCType = new VmCommercial_LCType();
            VmCommercial_LCType.Commercial_LCType = new Commercial_LCType();

            return View(VmCommercial_LCType);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_LCTypeCreate(VmCommercial_LCType a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));

                return RedirectToAction("VmCommercial_LCTypeIndex");
            }
            return RedirectToAction("VmCommercial_LCTypeIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmCommercial_LCTypeEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            VmCommercial_LCType = new VmCommercial_LCType();
            await Task.Run(() => VmCommercial_LCType.SelectSingle(id.Value));


            if (VmCommercial_LCType.Commercial_LCType == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_LCType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_LCTypeEdit(VmCommercial_LCType a)
        {
            if (ModelState.IsValid)
            {

                await Task.Run(() => a.Edit(UserID));
                return RedirectToAction("VmCommercial_LCTypeIndex");
            }
            return View(VmCommercial_LCType);
        }
        public ActionResult VmCommercial_LCTypeDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_LCType b = new Commercial_LCType();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_LCTypeIndex");
        }

        //..................Commercial_BtBLCType..............//
        public async Task<ActionResult> VmCommercial_BtBLCTypeIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_BtBLCType = new VmCommercial_BtBLCType();
            await Task.Run(() => VmCommercial_BtBLCType.InitialDataLoad());
            return View(VmCommercial_BtBLCType);
        }
        [HttpGet]
        public ActionResult VmCommercial_BtBLCTypeCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_BtBLCType = new VmCommercial_BtBLCType();
            VmCommercial_BtBLCType.Commercial_BtBLCType = new Commercial_BtBLCType();


            return View(VmCommercial_BtBLCType);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BtBLCTypeCreate(VmCommercial_BtBLCType a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));

                return RedirectToAction("VmCommercial_BtBLCTypeIndex");
            }
            return RedirectToAction("VmCommercial_BtBLCTypeIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmCommercial_BtBLCTypeEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            VmCommercial_BtBLCType = new VmCommercial_BtBLCType();
            await Task.Run(() => VmCommercial_BtBLCType.SelectSingle(id.Value));


            if (VmCommercial_BtBLCType.Commercial_BtBLCType == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_BtBLCType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BtBLCTypeEdit(VmCommercial_BtBLCType a)
        {
            if (ModelState.IsValid)
            {

                await Task.Run(() => a.Edit(UserID));
                return RedirectToAction("VmCommercial_BtBLCTypeIndex");
            }
            return View(VmCommercial_BtBLCType);
        }
        public ActionResult VmCommercial_BtBLCTypeDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_BtBLCType b = new Commercial_BtBLCType();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_BtBLCTypeIndex");
        }

        //.............Commercial_LCOregin...............//
        public async Task<ActionResult> VmCommercial_LCOreginIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_LCOregin = new VmCommercial_LCOregin();
            await Task.Run(() => VmCommercial_LCOregin.InitialDataLoad());
            return View(VmCommercial_LCOregin);
        }
        [HttpGet]
        public ActionResult VmCommercial_LCOreginCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_LCOregin = new VmCommercial_LCOregin();
            VmCommercial_LCOregin.Commercial_LCOregin = new Commercial_LCOregin();


            return View(VmCommercial_LCOregin);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_LCOreginCreate(VmCommercial_LCOregin a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));

                return RedirectToAction("VmCommercial_LCOreginIndex");
            }
            return RedirectToAction("VmCommercial_LCOreginIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmCommercial_LCOreginEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            VmCommercial_LCOregin = new VmCommercial_LCOregin();
            await Task.Run(() => VmCommercial_LCOregin.SelectSingle(id.Value));


            if (VmCommercial_LCOregin.Commercial_LCOregin == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_LCOregin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_LCOreginEdit(VmCommercial_LCOregin a)
        {
            if (ModelState.IsValid)
            {

                await Task.Run(() => a.Edit(UserID));
                return RedirectToAction("VmCommercial_LCOreginIndex");
            }
            return View(VmCommercial_LCOregin);
        }
        public ActionResult VmCommercial_LCOreginDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_LCOregin b = new Commercial_LCOregin();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_LCOreginIndex");
        }


        //......................................LocalBTBLC......................//
        public async Task<ActionResult> Commercial_BTBLCIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            await Task.Run(() => VmCommercial_B2bLC.InitialDataLoadForBTB());
            return View(VmCommercial_B2bLC);
        }

        public async Task<ActionResult> Commercial_BTBImportLCIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            await Task.Run(() => VmCommercial_B2bLC.InitialDataLoadForImportBTB(2));
            return View(VmCommercial_B2bLC);
        }

        [HttpGet]
        public ActionResult Commercial_BTBLCCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            VmCommercial_B2bLC.Commercial_B2bLC = new Commercial_B2bLC();
            VmCommercial_B2bLC.Commercial_B2bLC.Date = DateTime.Now;
            VmCommercial_B2bLC.Commercial_B2bLC.Status = "Running";

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommon_SupplierForDropDown();
            ViewBag.Common_Supplier = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetBTBItemDropDown();
            ViewBag.ComBTBItem = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetCommercial_MasterLCForDropDown();
            ViewBag.MasterLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmForDropDown.DDownData = VmForDropDown.GetUDDropDown();
            ViewBag.UD = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmForDropDown.DDownData = VmForDropDown.GetLCOreginDropDown();
            ViewBag.LCOregin = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            ViewBag.CurrencyList = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetBtBLCTypeDropDown();
            ViewBag.BtBLCType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            return View(VmCommercial_B2bLC);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Commercial_BTBLCCreate(VmCommercial_B2bLC a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                // chart of account integration
                try
                {
                    VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                    {
                        UserId = UserID,
                        AccountName = "BTB Account No. " + a.Commercial_B2bLC.Name,
                        Chart2Id = 177,
                    };
                    a.Commercial_B2bLC.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.AddFromIntegratedModule());

                }
                catch (Exception e)
                {
                    a.Commercial_B2bLC.Acc_AcNameFk = 0;
                }

                string s = MvcApplication.FilePrefix;
                a.Commercial_B2bLC.IsApproved = false;


                //Notifier Integration Get Closing Balance
                var commercial_UD = db.Commercial_UD.Find(a.Commercial_B2bLC.Commercial_UDFK);
                var common_Supplier = db.Common_Supplier.Find(a.Commercial_B2bLC.Common_SupplierFK);

                VmNotifier vmNotifier = new VmNotifier();
                var Common_SupplierOpeningBalance = vmNotifier.PartyOpeningLedger(common_Supplier);
                //Notifier Integration Get Closing Balance

                //a.Commercial_B2bLC.IsApproved = false;
                var value = await Task.Run(() => a.Add(UserID, Server.MapPath(MvcApplication.FilePrefix + "CustomeFile/Commercial/")));


                //Notifier Integration
                try
                {
                    VmForDropDown vmForDropDown = new VmForDropDown();
                    VmAccounting accounting = new VmAccounting();
                    accounting.FromDate = new DateTime(2001, 1, 1);
                    accounting.ToDate = DateTime.Today;



                    Acc_Notifier accNotifier = new Acc_Notifier
                    {
                        Type = (int)Acc_NotifierType.LcOpen,
                        Amount = (decimal)(a.Commercial_B2bLC.Amount),
                        FromBalance = 0,
                        ChequeNo = "",
                        ToBalance = Common_SupplierOpeningBalance,
                        FromName = "",
                        ToName = common_Supplier.Name,
                        Particulars = $@"",
                        TableIdFk = VmAcc_Database_Table.TableName(a.Commercial_B2bLC.GetType().Name),
                        RowIdFk = value,
                        FromFk = commercial_UD.Acc_AcNameFk,
                        ToFk = a.Commercial_B2bLC.Acc_AcNameFk,
                        IsApproved = 0,
                        Date = DateTime.Now
                    };

                    this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
                    {
                        UserId = this.UserID,
                        Acc_Notifier = accNotifier
                    };
                    // call integration and receive boolean. if true data add in journal successfully else not added
                    var id = await Task.Run(() => this.VmAcc_ChequeIntegration.AddCheque());

                    // auto approval
                    VmNotifierApproval vmNotifierApproval = new VmNotifierApproval();
                    await Task.Run(() => vmNotifierApproval.NotifierBtbApproved(id, user));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }


                return RedirectToAction("Commercial_BTBLCIndex");
            }
            return RedirectToAction("Commercial_BTBLCIndex");
        }


        [HttpGet]
        public async Task<ActionResult> Commercial_BTBLCEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            await Task.Run(() => VmCommercial_B2bLC.SelectSingleForBTB(id.Value));

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommon_SupplierForDropDown();
            ViewBag.Common_Supplier = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetBTBItemDropDown();
            ViewBag.ComBTBItem = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetUDDropDown();
            ViewBag.UD = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmForDropDown.DDownData = VmForDropDown.GetLCOreginDropDown();
            ViewBag.LCOregin = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            ViewBag.CurrencyList = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetBtBLCTypeDropDown();
            ViewBag.BtBLCType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            if (VmCommercial_B2bLC.Commercial_B2bLC == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_B2bLC);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Commercial_BTBLCEdit(VmCommercial_B2bLC x)
        {

            if (ModelState.IsValid)
            {
                // chart of account integration
                try
                {
                    VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                    {
                        UserId = UserID,
                        AccountName = "BTB Account No. " + x.Commercial_B2bLC.Name,
                        Chart2Id = 177,
                        ExistAccId = x.Commercial_B2bLC.Acc_AcNameFk
                    };
                    x.Commercial_B2bLC.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.EditFromIntegratedModule());

                }
                catch (Exception e)
                {
                    x.Commercial_B2bLC.Acc_AcNameFk = 0;
                }

                string s = MvcApplication.FilePrefix;
                await Task.Run(() => x.EditUser(UserID, Server.MapPath(MvcApplication.FilePrefix + "CustomeFile/Commercial/")));

                // journal integration
                try
                {
                    var commercial_UD = db.Commercial_UD.Find(x.Commercial_B2bLC.Commercial_UDFK);
                    var common_Supplier = db.Common_Supplier.Find(x.Commercial_B2bLC.Common_SupplierFK);
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = this.UserID,
                        Date = x.Commercial_B2bLC.Date,
                        Title = "BTB Foreign L/C",
                        FromAccNo = commercial_UD.Acc_AcNameFk,
                        ToAccNo = x.Commercial_B2bLC.Acc_AcNameFk,
                        CurrencyType = x.Commercial_B2bLC.CurrencyType,
                        CurrencyRate = x.Commercial_B2bLC.CurrencyRate,
                        Amount = (decimal)(x.Commercial_B2bLC.Amount),
                        Descriptions = "UD: " + commercial_UD.UdNo + " Payable to BTB Foreign L/C : " + x.Commercial_B2bLC.Name + ". Party Name: " + common_Supplier.Name,
                        TableIdFk = VmAcc_Database_Table.TableName(x.Commercial_B2bLC.GetType().Name),
                        TableRowIdFk = x.Commercial_B2bLC.ID,
                    };
                    var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
                }
                catch (Exception e)
                {

                }

                return RedirectToAction("Commercial_BTBLCIndex");
            }
            return RedirectToAction("Commercial_BTBLCIndex");
        }

        public ActionResult Commercial_BTBLCDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_B2bLC b = new Commercial_B2bLC();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("Commercial_BTBLCIndex");
        }

        //...........Commercial_BTBItem.................//
        public async Task<ActionResult> VmCommercial_BTBItemIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_BTBItem = new VmCommercial_BTBItem();
            await Task.Run(() => VmCommercial_BTBItem.InitialDataLoad());
            return View(VmCommercial_BTBItem);
        }
        [HttpGet]
        public ActionResult VmCommercial_BTBItemCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_BTBItem = new VmCommercial_BTBItem();


            return View(VmCommercial_BTBItem);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BTBItemCreate(VmCommercial_BTBItem a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));

                return RedirectToAction("VmCommercial_BTBItemIndex");
            }
            return RedirectToAction("VmCommercial_BTBItemIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmCommercial_BTBItemEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            VmCommercial_BTBItem = new VmCommercial_BTBItem();
            await Task.Run(() => VmCommercial_BTBItem.SelectSingle(id.Value));


            if (VmCommercial_BTBItem.Commercial_BTBItem == null)
            {
                return HttpNotFound();
            }
            return View(VmCommercial_BTBItem);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommercial_BTBItemEdit(VmCommercial_BTBItem a)
        {
            if (ModelState.IsValid)
            {

                await Task.Run(() => a.Edit(UserID));
                return RedirectToAction("VmCommercial_BTBItemIndex");
            }
            return View(VmCommercial_BTBItem);
        }
        public ActionResult VmCommercial_BTBItemDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Commercial_BTBItem b = new Commercial_BTBItem();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommercial_BTBItemIndex");
        }

        public async Task<ActionResult> LCStatementReport(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                Commercial_B2BLCStatementReport model = new Commercial_B2BLCStatementReport();
                VmCommercial_UDSlave = new VmCommercial_UDSlave();
                VmCommercial_UDSlave.VmCommercial_B2bLC = new VmCommercial_B2bLC();
                List<ReportDocType> report = new List<ReportDocType>();
                VmCommercial_MasterLC masterLc = new VmCommercial_MasterLC();
                model.Report = masterLc.LCStatementDocDataLoad(id.Value);
                model.BtbList = VmCommercial_UDSlave.VmCommercial_B2bLC.GetB2BLCByUDForReport(id.Value);
                return View(model);
            }
            return RedirectToAction("Commercial_BTBLCIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmCommercial_BTBLCDetails()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_Report = new VmCommercial_Report();
            await Task.Run(() => VmCommercial_Report.GetBTBDetails());
            return View(VmCommercial_Report);
        }

        public async Task<ActionResult> VmCommercial_SetBTBLcStatus(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            VmCommercial_B2bLC.Commercial_B2bLC = new Commercial_B2bLC();
            VmCommercial_B2bLC.Commercial_B2bLC.ID = id;
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetBTBStatus();
            ViewBag.Status = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            return View(VmCommercial_B2bLC);
        }

        [HttpPost]
        public async Task<ActionResult> VmCommercial_SetBTBLcStatus(VmCommercial_B2bLC VmCommercial_B2bLC)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string status = VmCommercial_B2bLC.Commercial_B2bLC.Status;
            await Task.Run(() => VmCommercial_B2bLC.SelectSingle(VmCommercial_B2bLC.Commercial_B2bLC.ID));
            VmCommercial_B2bLC.Commercial_B2bLC.Status = status;
            await Task.Run(() => VmCommercial_B2bLC.Edit(0));
            return RedirectToAction("VmCommercial_ImportIndex/" + VmCommercial_B2bLC.Commercial_B2bLC.ID);

        }
        public async Task<ActionResult> VmCommercial_EditBTBLcStatus(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_B2bLC = new VmCommercial_B2bLC();
            await Task.Run(() => VmCommercial_B2bLC.SelectSingle(id));

            if (VmCommercial_B2bLC.Commercial_B2bLC == null)
            {
                return HttpNotFound();
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetBTBStatus();
            ViewBag.Status = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            return View(VmCommercial_B2bLC);
        }

        [HttpPost]
        public async Task<ActionResult> VmCommercial_EditBTBLcStatus(VmCommercial_B2bLC VmCommercial_B2bLC, string FromPage = "")
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string status = VmCommercial_B2bLC.Commercial_B2bLC.Status;
            await Task.Run(() => VmCommercial_B2bLC.SelectSingle(VmCommercial_B2bLC.Commercial_B2bLC.ID));
            VmCommercial_B2bLC.Commercial_B2bLC.Status = status;
            await Task.Run(() => VmCommercial_B2bLC.Edit(0));
            if (FromPage == "InHouse")
            {
                return RedirectToAction("VmInHouseImportReportIndex");
            }
            return RedirectToAction("VmImportReportIndex");
        }

        public ActionResult VmInHouseImportReportIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetFilterType();
            ViewBag.Filter = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmImportReport = new VmImportReport();
            VmImportReport.VmFilter = new VmFilter();
            VmImportReport.VmFilter.FromDate = DateTime.Now.AddMonths(-6);
            VmImportReport.VmFilter.ToDate = DateTime.Now;
            return View(VmImportReport);
        }
        [HttpPost]
        public async Task<ActionResult> VmInHouseImportReportIndex(VmImportReport model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetFilterType();
            ViewBag.Filter = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            if (ModelState.IsValid)
            {
                int result = DateTime.Compare(model.VmFilter.FromDate, model.VmFilter.ToDate);
                if (result < 0)
                {
                    await Task.Run(() => model.ImportReportLoad(model.VmFilter));
                }
                else
                {
                    ModelState.AddModelError("error", "From date cannot be grater than to date.");
                }
            }

            return View(model);
        }



        public ActionResult SupplierFollowupReportDoc(string id)
        {
            int iD = 0;
            iD = Convert.ToInt32(id);
            if (iD == 0)
            {
                IdNullDivertion("POReportDoc", "Id is Null");
            }
            try
            {
                VmImportReport p = new VmImportReport();
                //VmMkt_PO p = new VmMkt_PO();


                p.ForeignBtbImport(iD);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/CommercialImportDoc.rpt");
                cr.Load();
                cr.SetDataSource(p.ReportDoc);
                //cr.SummaryInfo.ReportTitle = "Report Of " + p.Mkt_PO.CID;
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
                // }
            }
            catch (Exception ex)
            {
                IdNullDivertion("POReportDoc", ex.Message);
            }
            return null;
        }

        public async Task<ActionResult> BillOfExchangedInvoice()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_BillOfExchange = new VmCommercial_BillOfExchange();
            VmCommercial_BillOfExchange.VmCommercial_UDSlave = new VmCommercial_UDSlave();
            await Task.Run(() => VmCommercial_BillOfExchange.InitialDataLoad());

            return View(VmCommercial_BillOfExchange);


        }

        public async Task<ActionResult> WithoutBillOfExchangedInvoice()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_BillOfExchange = new VmCommercial_BillOfExchange();
            VmCommercial_BillOfExchange.VmCommercial_UDSlave = new VmCommercial_UDSlave();
            await Task.Run(() => VmCommercial_BillOfExchange.InitialDataLoad());

            return View(VmCommercial_BillOfExchange);


        }

        public async Task<ActionResult> RealizedBillOfExchanged()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_BillOfExchange = new VmCommercial_BillOfExchange();
            VmCommercial_BillOfExchange.VmCommercial_UDSlave = new VmCommercial_UDSlave();
            await Task.Run(() => VmCommercial_BillOfExchange.RealizedBillOfExchange());

            return View(VmCommercial_BillOfExchange);


        }
        public async Task<ActionResult> UnRealizedBillOfExchanged()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommercial_BillOfExchange = new VmCommercial_BillOfExchange();
            VmCommercial_BillOfExchange.VmCommercial_UDSlave = new VmCommercial_UDSlave();
            await Task.Run(() => VmCommercial_BillOfExchange.UnRealizedBillOfExchange());

            return View(VmCommercial_BillOfExchange);


        }
    }
}