﻿using CrystalDecisions.CrystalReports.Engine;
using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Planning;
using Oss.Romo.ViewModels.Home;
using Oss.Romo.ViewModels.Store;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models.User;
using Oss.Romo.Models.Common;
using Oss.Romo.ViewModels.User;
using System.Web.Routing;
using Oss.Romo.ViewModels.Account;
using System.Data.Entity;

namespace Oss.Romo.Controllers
{
    [SoftwareAdmin]
    public class MarketingController : Controller
    {

        public int ID { get; set; }
        VmControllerHelper VmControllerHelper;
        VmMkt_BOM vmMkt_BOM;
        VmMkt_BOMSlave VmMkt_BOMSlave;
        VmMkt_BOMSlaveFabric VmMkt_BOMSlaveFabric;
        VmMkt_PO VmMkt_PO;
        VmPlan_OrderStep vmPlan_OrderStep;
        Int32 UserID = 0;
        VmMkt_Category VmMkt_Category;
        VmMkt_SubCategory VmMkt_SubCategory;
        VmMkt_Item VmMkt_Item;
        VmMktPO_Slave VmMktPO_Slave;
        VmForDropDown VmForDropDown;

        VmMkt_CBS VmMkt_CBS;
        VmMkt_YarnCalculation VmMkt_YarnCalculation;
        VmMkt_OrderDeliverySchedule VmMkt_OrderDeliverySchedule;
        VmMkt_OrderColorAndSizeRatio VmMkt_OrderColorAndSizeRatio;
        VmMkt_CBSMaster VmMkt_CBSMaster;
        //.........VmMkt_BOM ......///
        private xOssContext db;
        public MarketingController()
        {
            db = new xOssContext();
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        public ActionResult DashboardTemp()
        {
            return View();
        }
        // To Get Buyer Information.
        public ActionResult VmMkt_BuyerIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_Buyer model = new VmMkt_Buyer();
            //VmMktBuyerVM model = new VmMktBuyerVM();
            //model.DataList = (from t1 in db.Mkt_Buyer
            //             join t2 in db.Common_Country on t1.Common_CountryFk equals t2.ID
            //             where t1.Active == true
            //             select new VmMktBuyerVM
            //             {
            //                 ID = t1.ID,
            //                 BuyerID = t1.BuyerID,
            //                 Name = t1.Name,
            //                 Country = t2.Name,
            //                 Address = t1.Address,
            //                 Phone = t1.Phone
            //             }).OrderByDescending(x => x.ID).AsEnumerable();

            model.InitialDataLoad();

            return View(model);
        }
        // To Insert Buyer Information. -Get
        public ActionResult VmMkt_BuyerCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Country = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetAccountBuyerHeadForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Chart2Head = sl3;

            return View();
        }
        // To Insert Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BuyerCreate(VmMkt_Buyer vmMktBuyer)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            // chart of account integration
            try
            {
                VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                {
                    UserId = UserID,
                    AccountName = vmMktBuyer.Name,
                    Chart2Id = vmMktBuyer.Acc_Chart2IDFk
                };
                vmMktBuyer.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.AddFromIntegratedModule());
            }
            catch
            {
                vmMktBuyer.Acc_AcNameFk = 0;
            }

            if (Request.Cookies["OssRomo"] != null)
            {
                string username = Request.Cookies["OssRomo"].Values["UserName"];
            }
            if (ModelState.IsValid)
            {
                //Mkt_Buyer model = new Mkt_Buyer()
                //{
                //    BuyerID = VmMktBuyer.BuyerID,
                //    Name = VmMktBuyer.Name,
                //    Address = VmMktBuyer.Address,
                //    Email = VmMktBuyer.Email,
                //    Phone = VmMktBuyer.Phone,
                //    Common_CountryFk = VmMktBuyer.Common_CountryFk,
                //    ContactPerson = VmMktBuyer.ContactPerson,
                //    FirstNotyfyParty = VmMktBuyer.FirstNotyfyParty,
                //    SecondNotyfyParty = VmMktBuyer.SecondNotyfyParty,
                //    ThirdNotyfyParty = VmMktBuyer.ThirdNotyfyParty,
                //    BillTo = VmMktBuyer.BillTo,
                //    Description = VmMktBuyer.Description
                //};

                //model.AddReady(user.User_User.ID);
                //model.Add();

                vmMktBuyer.Mkt_Buyer = new Mkt_Buyer()
                {
                    BuyerID = vmMktBuyer.BuyerID,
                    Name = vmMktBuyer.Name,
                    Address = vmMktBuyer.Address,
                    Email = vmMktBuyer.Email,
                    Phone = vmMktBuyer.Phone,
                    Common_CountryFk = vmMktBuyer.Common_CountryFk,
                    ContactPerson = vmMktBuyer.ContactPerson,
                    FirstNotyfyParty = vmMktBuyer.FirstNotyfyParty,
                    SecondNotyfyParty = vmMktBuyer.SecondNotyfyParty,
                    ThirdNotyfyParty = vmMktBuyer.ThirdNotyfyParty,
                    BillTo = vmMktBuyer.BillTo,
                    Description = vmMktBuyer.Description,
                    Acc_AcNameFk = vmMktBuyer.Acc_AcNameFk,
                    FirstCreatedBy = user.User_User.ID,
                };
                await Task.Run(() => vmMktBuyer.Add((int)user.User_User.ID));
                return RedirectToAction("VmMkt_BuyerIndex");
            }

            return RedirectToAction("VmMkt_BuyerIndex");
        }
        // To update Buyer Information. -Get
        [HttpGet]
        public async Task<ActionResult> VmMkt_BuyerEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_Buyer vmMktvBuyer = new VmMkt_Buyer();
            //await Task.Run(() => VmMkt_Buyer.SelectSingle(id));
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Country = sl2;


            VmForDropDown.DDownData = VmForDropDown.GetAccountBuyerHeadForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Chart2Head = sl3;


            await Task.Run(() => vmMktvBuyer.SelectSingle(id));

            var accAcName = db.Acc_AcName.Find(vmMktvBuyer.Mkt_Buyer.Acc_AcNameFk);
            if (accAcName?.Acc_Chart2FK != null) vmMktvBuyer.Acc_Chart2IDFk = (int)accAcName.Acc_Chart2FK;

            if (vmMktvBuyer.Mkt_Buyer == null)
            {
                return HttpNotFound();
            }
            return View(vmMktvBuyer);
        }
        // To update Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BuyerEdit(VmMkt_Buyer VmMkt_Buyer)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //VmControllerHelper vmControllerHelper = new VmControllerHelper();
            //mkt_Buyer.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;

            // chart of account integration
            try
            {
                VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                {
                    UserId = UserID,
                    AccountName = VmMkt_Buyer.Name,
                    Chart2Id = VmMkt_Buyer.Acc_Chart2IDFk,
                    ExistAccId = VmMkt_Buyer.Acc_AcNameFk
                };
                VmMkt_Buyer.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.EditFromIntegratedModule());
            }
            catch (Exception e)
            {
                VmMkt_Buyer.Acc_AcNameFk = 0;
            }

            //var errors = ModelState.Select(x => x.Value.Errors)
            //               .Where(y => y.Count > 0)
            //               .ToList();

            if (ModelState.IsValid)
            {
                VmMkt_Buyer.Mkt_Buyer = new Mkt_Buyer()
                {
                    ID = VmMkt_Buyer.ID,
                    Acc_AcNameFk = VmMkt_Buyer.Acc_AcNameFk,
                    Address = VmMkt_Buyer.Address,
                    BillTo = VmMkt_Buyer.BillTo,
                    BuyerID = VmMkt_Buyer.BuyerID,
                    Common_CountryFk = VmMkt_Buyer.Common_CountryFk,
                    ContactPerson = VmMkt_Buyer.ContactPerson,
                    Description = VmMkt_Buyer.Description,
                    FirstNotyfyParty = VmMkt_Buyer.FirstNotyfyParty,
                    Email = VmMkt_Buyer.Email,
                    LastEditeddBy = user.User_User.ID,
                    Name = VmMkt_Buyer.Name,
                    Phone = VmMkt_Buyer.Phone,
                    SecondNotyfyParty = VmMkt_Buyer.SecondNotyfyParty,
                    ThirdNotyfyParty = VmMkt_Buyer.ThirdNotyfyParty

                };
                await Task.Run(() => VmMkt_Buyer.Edit(0));
                return RedirectToAction("VmMkt_BuyerIndex");
            }
            return View(VmMkt_Buyer);
        }
        // To remove Buyer Information. -Get
        public ActionResult VmMkt_BuyerDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            int iD = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            Mkt_Buyer b = new Mkt_Buyer();
            b.ID = iD;
            b.Delete(0);
            return RedirectToAction("VmMkt_BuyerIndex");
        }


        public async Task<ActionResult> Mkt_BOMSlaveItem(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            vmMkt_BOM = new VmMkt_BOM();
            await Task.Run(() => vmMkt_BOM = vmMkt_BOM.SelectSingleJoined(id));
            Mkt_BOMSlave a = new Mkt_BOMSlave();
            VmControllerHelper = new VmControllerHelper();
            await Task.Run(() => vmMkt_BOM.BOMReportDocLoad(id));
            vmMkt_BOM.VmMkt_BOMSlave = new VmMkt_BOMSlave();

            await Task.Run(() => vmMkt_BOM.VmMkt_BOMSlave.GetBOMSlaveItem(id, vmMkt_BOM.Mkt_BOM.IsNew));
            string s = id;
            int bomFk = 0;
            s = VmControllerHelper.Decrypt(s);
            Int32.TryParse(s, out bomFk);
            a.Mkt_BOMFK = bomFk;
            //vmMkt_BOM.SuggestedVmMkt_PO = new VmMkt_PO();
            //await Task.Run(() => vmMkt_BOM.SuggestedVmMkt_PO.GetSuggestedPOFromBOM(id.Value));

            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            vmMkt_BOM.User_User = vMUser.User_User;

            return View(vmMkt_BOM);
        }

        public async Task<ActionResult> VmMkt_CBSIndex(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmMkt_CBSMaster = new VmMkt_CBSMaster();
            await Task.Run(() => VmMkt_CBSMaster = VmMkt_CBSMaster.SelectSingleJoined(id));
            Mkt_CBS a = new Mkt_CBS();
            VmControllerHelper = new VmControllerHelper();
            //await Task.Run(() => vmMkt_BOM.BOMReportDocLoad(id));
            VmMkt_CBSMaster.VmMkt_CBS = new VmMkt_CBS();

            await Task.Run(() => VmMkt_CBSMaster.VmMkt_CBS.GetCBSItem(id));
            string s = id;
            int CBSMaster = 0;
            s = VmControllerHelper.Decrypt(s);
            Int32.TryParse(s, out CBSMaster);
            a.Mkt_CBSMasterFK = CBSMaster;

            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_CBSMaster.User_User = vMUser.User_User;

            return View(VmMkt_CBSMaster);
        }
        [HttpGet]
        public ActionResult VmMkt_CBSCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;
            VmMkt_CBSMaster vmMktCBSMaster = new VmMkt_CBSMaster();
            vmMktCBSMaster.Mkt_CBSMaster = new Mkt_CBSMaster();
            vmMktCBSMaster.Mkt_CBSMaster.UpdateDate = DateTime.Now;

            vmMktCBSMaster.SelectSingle(id);
            ViewModels.Marketing.VmMkt_CBS VmMkt_CBS = new VmMkt_CBS();
            VmMkt_CBS.Mkt_CBS = new Mkt_CBS();

            VmMkt_CBS.Mkt_CBS.Mkt_CBSMasterFK = id;
            VmMkt_CBS.Mkt_CBSMaster = vmMktCBSMaster.Mkt_CBSMaster;
            VmMkt_CBS.Mkt_CBS.RequiredQuantity = 0;
            return View(VmMkt_CBS);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_CBSCreate(VmMkt_CBS vmMktCbs)
        {

            VmControllerHelper vmControllerHelper = new VmControllerHelper();

            vmMktCbs.Mkt_CBS.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            //if (ModelState.IsValid)
            //{
            vmMktCbs.Mkt_CBS.AddReady(UserID);

            if (vmMktCbs.Mkt_CBS.RequiredQuantity == 0 || vmMktCbs.Mkt_CBS.RequiredQuantity == null)
            {
                vmMktCbs.Mkt_CBS.RequiredQuantity = vmMktCbs.Mkt_CBS.Consumption * vmMktCbs.Mkt_CBSMaster.QPack * vmMktCbs.Mkt_CBSMaster.Quantity / 12;
            }

            //if (vmMktCbs.Mkt_CBS.Common_SupplierFK == 1)
            //{
            //    vmMktCbs.Mkt_CBS.Price = 0;
            //}
            vmMkt_BOM = new VmMkt_BOM();

            await Task.Run(() => vmMktCbs.Add(0));

            int? bomFk = vmMktCbs.Mkt_CBS.Mkt_CBSMasterFK;
            //await Task.Run(() => VmMkt_BOMSlave.EditBOMUpdateDate(VmMkt_BOMSlave.Mkt_BOMSlave.FirstCreatedBy, bomFk.Value));
            string boMFK = bomFk.ToString();
            string redirectID = vmControllerHelper.Encrypt(boMFK);
            return RedirectToAction("VmMkt_CBSIndex/" + redirectID);

        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_CBSEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_CBS = new VmMkt_CBS();

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            await Task.Run(() => VmMkt_CBS.GetCBSByID(id));

            if (VmMkt_CBS.Mkt_CBS == null)
            {
                return HttpNotFound();
            }
            VmControllerHelper = new VmControllerHelper();
            //no chg
            VmForDropDown.DDownData = VmForDropDown.GetRaw_ItemDropDownForEdit(VmMkt_CBS.Mkt_CBS.Raw_ItemFK);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Item = sl2;
            int i = VmMkt_CBS.Mkt_CBS.Raw_ItemFK;
            string s = i.ToString();
            string ItemFK = VmControllerHelper.Encrypt(s);

            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryDropDownForEdit(ItemFK);
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryDropDownForEdit(VmMkt_CBS.Mkt_CBS.Raw_ItemFK);
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            //VmMkt_BOMSlave.RawCatID = catID.GetValueOrDefault();
            //VmMkt_BOMSlave.RawSubCatID = sabCatID.GetValueOrDefault();

            //VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = 0;
            return View(VmMkt_CBS);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_CBSEdit(VmMkt_CBS vmMktCbs)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            vmMktCbs.Mkt_CBS.LastEditeddBy = user.User_User.ID;

            if (ModelState.IsValid)
            {

                if (vmMktCbs.Mkt_CBS.RequiredQuantity == 0 || vmMktCbs.Mkt_CBS.RequiredQuantity == null)
                {
                    VmMkt_CBSMaster vmMktCBSMaster = new VmMkt_CBSMaster();
                    vmMktCBSMaster.SelectSingle(vmMktCbs.Mkt_CBS.Mkt_CBSMasterFK.Value);
                    vmMktCbs.Mkt_CBSMaster = vmMktCBSMaster.Mkt_CBSMaster;
                    vmMktCBSMaster.Mkt_CBSMaster.UpdateDate = DateTime.Now;
                    vmMktCbs.Mkt_CBS.RequiredQuantity = vmMktCbs.Mkt_CBS.Consumption * vmMktCbs.Mkt_CBSMaster.QPack * vmMktCbs.Mkt_CBSMaster.Quantity / 12;
                }
                await Task.Run(() => vmMktCbs.Edit(0));

                return RedirectToAction("VmMkt_CBSIndex/" + vmMktCbs.Mkt_CBS.Mkt_CBSMasterFK.Value);
            }
            return View(vmMktCbs);
        }

        public ActionResult VmMkt_CBSDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("尺")), out deleteID);
            int bOMid = 0;
            id = id.Replace(deleteID.ToString() + "尺", "");
            int.TryParse(id, out bOMid);


            Mkt_CBS b = new Mkt_CBS();
            b.ID = deleteID;
            b.Delete(0);
            return RedirectToAction("VmMkt_CBSIndex/" + bOMid);
        }



        public async Task<ActionResult> VmMkt_CBSMasterIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_CBSMaster = new VmMkt_CBSMaster();

            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_CBSMaster.User_User = vMUser.User_User;


            await Task.Run(() => VmMkt_CBSMaster.InitialDataLoad());
            return View(VmMkt_CBSMaster);
        }
        [HttpGet]
        public ActionResult VmMkt_CBSMasterCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_CBSMaster = new VmMkt_CBSMaster();
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetMktCategoryForDropDown();
            SelectList sl9 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktCategory = sl9;

            VmForDropDown.DDownData = VmForDropDown.GetMktSubCategoryForDropDown();
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktsCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetMktItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Item = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Buyer = sl1;

            VmMkt_CBSMaster.Mkt_CBSMaster = new Mkt_CBSMaster();
            VmMkt_CBSMaster.Mkt_CBSMaster.FirstMove = DateTime.Today;
            VmMkt_CBSMaster.Mkt_CBSMaster.UpdateDate = DateTime.Today;
            int iD = 0;
            VmControllerHelper = new VmControllerHelper();

            return View(VmMkt_CBSMaster);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_CBSMasterCreate(VmMkt_CBSMaster VmMkt_CBSMaster)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper vmControllerHelper = new VmControllerHelper();

            VmMkt_CBSMaster.Mkt_CBSMaster.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            if (ModelState.IsValid)
            {
                VmMkt_CBSMaster.Mkt_CBSMaster.UpdateDate = VmMkt_CBSMaster.Mkt_CBSMaster.FirstMove;
                await Task.Run(() => VmMkt_CBSMaster.Add(UserID));
                return RedirectToAction("VmMkt_CBSMasterIndex");
            }
            return RedirectToAction("VmMkt_CBSMasterIndex");

        }

        [HttpGet]
        public async Task<ActionResult> VmMkt_CBSMasterEdit(int id)
        {

            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_CBSMaster = new VmMkt_CBSMaster();

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Buyer = sl1;

            //await Task.Run(() => vmMkt_BOM.InitialDataLoad());
            await Task.Run(() => VmMkt_CBSMaster.SelectSingle(id));

            VmMkt_Item x = new VmMkt_Item();
            int itemID = VmMkt_CBSMaster.Mkt_CBSMaster.Mkt_ItemFK;
            string itemiD = itemID.ToString();
            string EnItemID = VmControllerHelper.Encrypt(itemiD);

            x.SelectSingle(EnItemID);
            VmMkt_CBSMaster.Mkt_Item = x.Mkt_Item;

            if (VmMkt_CBSMaster.Mkt_CBSMaster == null)
            {
                return HttpNotFound();
            }
            VmForDropDown.DDownData = VmForDropDown.GetMktCategoryForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktCategory = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetTempMktItemForEdit(EnItemID);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Item = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetTempMktSubCategoryForEdit(EnItemID);
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktSubCategory = sl5;
            return View(VmMkt_CBSMaster);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_CBSMasterEdit(VmMkt_CBSMaster VmMkt_CBSMaster)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_CBSMaster.Mkt_CBSMaster.UpdateDate = DateTime.Now;
            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            VmMkt_CBSMaster.Mkt_CBSMaster.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmMkt_CBSMaster.Edit(0));
                return RedirectToAction("VmMkt_CBSMasterIndex", "Marketing");
                //return View(VmMkt_BOM);

            }
            return View(VmMkt_CBSMaster);
        }


        public ActionResult VmMkt_CBSMasterDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Mkt_CBSMaster b = new Mkt_CBSMaster();
            b.ID = id;
            b.Delete(0);

            return RedirectToAction("VmMkt_CBSMasterIndex", "Marketing");
        }



        public async Task<ActionResult> AddFabricItem(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_BOMSlave = new VmMkt_BOMSlave();
            await Task.Run(() => VmMkt_BOMSlave = VmMkt_BOMSlave.SelectSingleJoined(id.Value));
            Mkt_BOMSlaveFabric a = new Mkt_BOMSlaveFabric();
            a.Mkt_BOMSlaveFK = id;
            VmMkt_BOMSlave.VmMkt_BOMSlaveFabric = new VmMkt_BOMSlaveFabric();
            await Task.Run(() => VmMkt_BOMSlave.VmMkt_BOMSlaveFabric.InitialDataLoadFabric(id.Value));
            return View(VmMkt_BOMSlave);
        }
        public async Task<ActionResult> Mkt_BOMSlaveFabric(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_BOMSlave = new VmMkt_BOMSlave();
            await Task.Run(() => VmMkt_BOMSlave.GetBOMSlaveFabric());
            return View(VmMkt_BOMSlave);
        }
        [HttpPost]
        public async Task<ActionResult> Mkt_BOMSlaveFabric(VmMkt_BOMSlave VmMkt_BOMSlave)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string searchString = VmMkt_BOMSlave.SearchString;
            VmMkt_BOMSlave = new VmMkt_BOMSlave();
            await Task.Run(() => VmMkt_BOMSlave.GetBOMSlaveFabricSearch(searchString));
            return View(VmMkt_BOMSlave);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_POSuggestedItemEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == "")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            ViewModels.Marketing.VmMkt_PO VmMkt_PO = new VmMkt_PO();
            VmMkt_PO.Mkt_PO = new Mkt_PO();
            int itemID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out itemID);
            int bOMid = 0;
            id = id.Replace(itemID.ToString() + "x", "");
            int.TryParse(id, out bOMid);
            string bomID = bOMid.ToString();
            VmMkt_PO.Mkt_PO.Mkt_BOMFK = itemID;

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingle(itemID));
            VmMkt_BOM tt = new VmMkt_BOM();
            tt.SelectSingle(bomID);
            VmMkt_PO.Mkt_PO.Mkt_BOMFK = tt.Mkt_BOM.ID;


            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_BOMSlave);

        }

        [HttpGet]
        public ActionResult VmMkt_BOMSlaveCreate(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;
            VmMkt_BOM VmMkt_BOM = new VmMkt_BOM();
            VmMkt_BOM.Mkt_BOM = new Mkt_BOM();
            VmMkt_BOM.Mkt_BOM.UpdateDate = DateTime.Now;

            VmMkt_BOM.SelectSingle(id);
            ViewModels.Marketing.VmMkt_BOMSlave vmMkt_BOMSlave = new VmMkt_BOMSlave();
            vmMkt_BOMSlave.Mkt_BOMSlave = new Mkt_BOMSlave();
            int sId = 0;
            VmControllerHelper = new VmControllerHelper();
            string s = VmControllerHelper.Decrypt(id);
            Int32.TryParse(s, out sId);
            vmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK = sId;
            vmMkt_BOMSlave.Mkt_BOM = VmMkt_BOM.Mkt_BOM;
            vmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = 0;
            return View(vmMkt_BOMSlave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMSlaveCreate(VmMkt_BOMSlave VmMkt_BOMSlave)
        {

            VmControllerHelper vmControllerHelper = new VmControllerHelper();

            VmMkt_BOMSlave.Mkt_BOMSlave.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            //if (ModelState.IsValid)
            //{
            VmMkt_BOMSlave.Mkt_BOMSlave.AddReady(UserID);

            if (VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity == 0 || VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity == null)
            {
                VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = VmMkt_BOMSlave.Mkt_BOMSlave.Consumption * VmMkt_BOMSlave.Mkt_BOM.QPack * VmMkt_BOMSlave.Mkt_BOM.Quantity / 12;
            }

            if (VmMkt_BOMSlave.Mkt_BOMSlave.Common_SupplierFK == 1)
            {
                VmMkt_BOMSlave.Mkt_BOMSlave.Price = 0;
            }
            vmMkt_BOM = new VmMkt_BOM();

            await Task.Run(() => VmMkt_BOMSlave.Add(0));

            int? bomFk = VmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK;
            await Task.Run(() => VmMkt_BOMSlave.EditBOMUpdateDate(VmMkt_BOMSlave.Mkt_BOMSlave.FirstCreatedBy, bomFk.Value));
            string boMFK = bomFk.ToString();
            string redirectID = vmControllerHelper.Encrypt(boMFK);
            return RedirectToAction("Mkt_BOMSlaveItem/" + redirectID);

        }


        [HttpGet]
        public ActionResult VmMkt_BOMFabricCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            ViewModels.Marketing.VmMkt_BOMSlave vmMkt_BOMSlave = new VmMkt_BOMSlave();
            vmMkt_BOMSlave.Mkt_BOMSlave = new Mkt_BOMSlave();
            vmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK = id;
            return View(vmMkt_BOMSlave);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMFabricCreate(Mkt_BOMSlave mkt_BOMSlave)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                mkt_BOMSlave.IsFabric = true;
                mkt_BOMSlave.AddReady(UserID);
                await Task.Run(() => mkt_BOMSlave.Add());
                return RedirectToAction("Mkt_BOMSlaveFabric/" + mkt_BOMSlave.Mkt_BOMFK);
            }
            return RedirectToAction("Mkt_BOMSlaveFabric/" + mkt_BOMSlave.Mkt_BOMFK);
        }


        public List<object> GetTempRaw_Category()
        {
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }
        int? sabCatID;
        int? catID;
        int? mktSabCatID;
        int? mktCatID;
        public List<object> GetTempRawSubCategoryForEdit(int? id)
        {
            var scl = new List<object>();

            sabCatID = (from t3 in db.Raw_Item where t3.ID == id select t3.Raw_SubCategoryFK).FirstOrDefault();
            catID = (
                    from t2 in db.Raw_SubCategory
                    where t2.ID == sabCatID
                    select t2.Raw_CategoryFK

                    ).FirstOrDefault();

            var x = (from t1 in db.Raw_SubCategory
                     where t1.Raw_CategoryFK == catID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
                 ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }
        public List<object> GetTempMktSubCategoryForEdit(int? id)
        {
            var scl = new List<object>();

            mktSabCatID = (from t3 in db.Mkt_Item where t3.ID == id select t3.Mkt_SubCategoryFK).FirstOrDefault();
            mktCatID = (
                    from t2 in db.Mkt_SubCategory
                    where t2.ID == mktSabCatID
                    select t2.Mkt_CategoryFK

                    ).FirstOrDefault();

            var x = (from t1 in db.Mkt_SubCategory
                     where t1.Mkt_CategoryFK == mktCatID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
                 ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }
        public List<object> GetTempRaw_CategoryForEdit(int? id)
        {
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }
        public List<object> GetTempRaw_ItemForEdit(int? id)
        {
            var rawItemList = new List<object>();
            var x = (from t1 in db.Raw_Item
                     where t1.Raw_SubCategoryFK == (from t2 in db.Raw_Item where t2.ID == id select t2.Raw_SubCategoryFK).FirstOrDefault()
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
                   ).ToList();


            foreach (var raw_Item in x)
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });

            }
            return rawItemList;
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_BOMSlaveEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_BOMSlave = new VmMkt_BOMSlave();

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            await Task.Run(() => VmMkt_BOMSlave.SelectSingle(id));

            if (VmMkt_BOMSlave.Mkt_BOMSlave == null)
            {
                return HttpNotFound();
            }
            VmControllerHelper = new VmControllerHelper();
            //no chg
            VmForDropDown.DDownData = VmForDropDown.GetRaw_ItemDropDownForEdit(VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Item = sl2;
            int i = VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK;
            string s = i.ToString();
            string ItemFK = VmControllerHelper.Encrypt(s);

            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryDropDownForEdit(ItemFK);
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryDropDownForEdit(VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK);
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            //VmMkt_BOMSlave.RawCatID = catID.GetValueOrDefault();
            //VmMkt_BOMSlave.RawSubCatID = sabCatID.GetValueOrDefault();

            //VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = 0;
            return View(VmMkt_BOMSlave);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMSlaveEdit(VmMkt_BOMSlave VmMkt_BOMSlave)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            VmMkt_BOMSlave.Mkt_BOMSlave.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;

            if (ModelState.IsValid)
            {

                if (VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity == 0 || VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity == null)
                {
                    VmMkt_BOM temp = new VmMkt_BOM();
                    temp.SelectSingle(VmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK.Value.ToString());
                    VmMkt_BOMSlave.Mkt_BOM = temp.Mkt_BOM;
                    temp.Mkt_BOM.UpdateDate = DateTime.Now;
                    VmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = VmMkt_BOMSlave.Mkt_BOMSlave.Consumption * VmMkt_BOMSlave.Mkt_BOM.QPack * VmMkt_BOMSlave.Mkt_BOM.Quantity / 12;
                }

                await Task.Run(() => VmMkt_BOMSlave.Edit(0));


                await Task.Run(() => VmMkt_BOMSlave.EditBOMUpdateDate(0, VmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK.Value));
                int i = VmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK.Value;
                string s = i.ToString();
                string BOMFK = vmControllerHelper.Encrypt(s);
                VmMkt_BOMSlave.VmMktPO_Slave = new VmMktPO_Slave();
                VmMkt_BOMSlave.VmMktPO_Slave.Mkt_POSlave = new Mkt_POSlave();

                await Task.Run(() => VmMkt_BOMSlave.EditPOwithBOMslave(0, VmMkt_BOMSlave.Mkt_BOMSlave.ID));
                return RedirectToAction("Mkt_BOMSlaveItem/" + BOMFK);
            }
            return View(VmMkt_BOMSlave);
        }




        [HttpGet]
        public async Task<ActionResult> VmMkt_BOMFabricEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            VmMkt_BOMSlave = new VmMkt_BOMSlave();
            await Task.Run(() => VmMkt_BOMSlave.SelectSingle(id));


            if (VmMkt_BOMSlave.Mkt_BOMSlave == null)
            {
                return HttpNotFound();
            }
            //no chg
            VmForDropDown.DDownData = VmForDropDown.GetRaw_ItemDropDownEdit(VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Item = sl2;
            int i = VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK;
            string s = i.ToString();
            VmControllerHelper = new VmControllerHelper();
            string ItemFK = VmControllerHelper.Encrypt(s);

            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryDropDownForEdit(ItemFK);
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryDropDownForEdit(VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK);
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            VmMkt_BOMSlave.RawCatID = catID.GetValueOrDefault();

            VmMkt_BOMSlave.RawSubCatID = sabCatID.GetValueOrDefault();
            return View(VmMkt_BOMSlave);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMFabricEdit(Mkt_BOMSlave mkt_BOMSlave)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => mkt_BOMSlave.Edit(0));

                VmControllerHelper = new VmControllerHelper();
                int i = mkt_BOMSlave.Mkt_BOMFK.Value;
                string s = i.ToString();
                string BOMFK = VmControllerHelper.Encrypt(s);
                return RedirectToAction("Mkt_BOMSlaveFabric/" + BOMFK);
            }
            return View(VmMkt_BOMSlave);
        }

        public ActionResult VmMkt_BOMSlaveDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string str1 = id.Substring(0, id.LastIndexOf("尺"));
            string bomID = id.Substring(id.Length - id.LastIndexOf("尺"));

            VmControllerHelper = new VmControllerHelper();
            int deletID = 0;
            string s = VmControllerHelper.Decrypt(str1);
            Int32.TryParse(s, out deletID);
            int bomFk = 0;
            int.TryParse(bomID, out bomFk);
            // x.Mkt_BOMSlave = new Mkt_BOMSlave();
            EditUpdateDate(0, bomFk);
            Mkt_BOMSlave b = new Mkt_BOMSlave();
            b.ID = deletID;
            var sds = b.Delete(0);
            return RedirectToAction("Mkt_BOMSlaveItem/" + bomID);
        }

        public async Task<ActionResult> LockedBOMSlave(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_BOMSlave = new VmMkt_BOMSlave();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_BOMSlave.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_BOMSlave.GetBOMSlaveByID(id.Value));
            return View(VmMkt_BOMSlave);
        }

        [HttpPost]
        public async Task<ActionResult> LockedBOMSlave(VmMkt_BOMSlave VmMkt_BOMSlave)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_BOMSlave.User_User = vMUser.User_User;

            await Task.Run(() => VmMkt_BOMSlave.Edit(0));

            return RedirectToAction("Mkt_BOMSlaveItem/" + VmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK);
        }

        public ActionResult VmMkt_BOMFabricDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string str1 = id.Substring(0, id.LastIndexOf("م"));
            string str2 = id.Substring(id.Length - id.LastIndexOf("م"));
            VmControllerHelper = new VmControllerHelper();
            string fId = VmControllerHelper.Decrypt(str1);
            int deletID = 0, bomID = 0;
            Int32.TryParse(fId, out deletID);
            id = str1 + "م" + str2;
            string bId = VmControllerHelper.Decrypt(str2);
            Int32.TryParse(bId, out bomID);
            Mkt_BOMSlave b = new Mkt_BOMSlave();
            b.ID = deletID;
            b.Delete(0);
            return RedirectToAction("Mkt_BOMSlaveFabric/" + bomID);
        }
        //[HttpGet]
        //public async Task<ActionResult> BOM_Part()
        //{
        //    vmMkt_BOM = new VmMkt_BOM();
        //    await Task.Run(() => vmMkt_BOM.InitialDataLoad());
        //    return PartialView("BOM_Part", vmMkt_BOM);
        //}
        ////for Exsprement
        //public async Task<ActionResult> VmMkt_BOMIndexTemp()
        //{
        //    vmMkt_BOM = new VmMkt_BOM();
        //    await Task.Run(() => vmMkt_BOM.InitialDataLoad());

        //        return View(vmMkt_BOM);
        //}

        public async Task<ActionResult> ChangeBOMStatus(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_BOM x = new VmMkt_BOM();


            await Task.Run(() => x.ChangeBOMStatus(id));

            return RedirectToAction("VmMkt_BOMIndex");
        }

        public async Task<ActionResult> VmMkt_BOMIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            bool flag = false;
            vmMkt_BOM = new VmMkt_BOM();

            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            vmMkt_BOM.User_User = vMUser.User_User;
            vmMkt_BOM.Header = "Bill Of Materials(In Progress)";
            vmMkt_BOM.ButtonName = "Close";

            if (id != null)
            {
                flag = true;
                vmMkt_BOM.Header = "Bill Of Materials(Complete)";
                vmMkt_BOM.ButtonName = "ReOpen";

            }

            await Task.Run(() => vmMkt_BOM.InitialDataLoad(flag));
            return View(vmMkt_BOM);
        }


        //private VmMkt_CBSMaster VmMkt_CBSMaster;

        //public async Task<ActionResult> VmMkt_CBSMasterIndex(int? id)
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }

        //    bool flag = false;
        //    VmMkt_CBSMaster = new VmMkt_CBSMaster();

        //    VmControllerHelper VmControllerHelper = new VmControllerHelper();
        //    VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
        //    VmMkt_CBSMaster.User_User = vMUser.User_User;
        //    VmMkt_CBSMaster.Header = "Bill Of Materials(In Progress)";
        //    VmMkt_CBSMaster.ButtonName = "Close";

        //    if (id != null)
        //    {
        //        flag = true;
        //        VmMkt_CBSMaster.Header = "Bill Of Materials(Complete)";
        //        VmMkt_CBSMaster.ButtonName = "ReOpen";

        //    }

        //    await Task.Run(() => VmMkt_CBSMaster.InitialDataLoad(flag));
        //    return View(VmMkt_CBSMaster);
        //}
        [HttpPost]
        public async Task<ActionResult> VmMkt_BOMIndex(VmMkt_BOM VmMkt_BOM, int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string SearchString = VmMkt_BOM.SearchString;
            string ButtonName = VmMkt_BOM.ButtonName;
            bool flag = false;
            vmMkt_BOM = new VmMkt_BOM();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            vmMkt_BOM.User_User = vMUser.User_User;
            vmMkt_BOM.Header = "Bill Of Materials(In Progress)";
            vmMkt_BOM.ButtonName = "Close";
            if (id != null)
            {
                flag = true;
                vmMkt_BOM.Header = "Bill Of Materials(Complete)";
                vmMkt_BOM.ButtonName = "ReOpen";

            }

            await Task.Run(() => vmMkt_BOM.InitialBOMSearch(flag, SearchString));
            return View(vmMkt_BOM);
        }


        public async Task<ActionResult> AuthorizedBOMIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            bool flag = false;
            vmMkt_BOM = new VmMkt_BOM();

            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            vmMkt_BOM.User_User = vMUser.User_User;
            vmMkt_BOM.Header = "Bill Of Materials(In Progress)";
            vmMkt_BOM.ButtonName = "Close";

            if (id != null)
            {
                flag = true;
                vmMkt_BOM.Header = "Bill Of Materials(Complete)";
                vmMkt_BOM.ButtonName = "ReOpen";

            }

            await Task.Run(() => vmMkt_BOM.GetAuthorizedBOM(flag));
            return View(vmMkt_BOM);
        }
        [HttpPost]
        public async Task<ActionResult> AuthorizedBOMIndex(VmMkt_BOM VmMkt_BOM, int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string SearchString = VmMkt_BOM.SearchString;
            string ButtonName = VmMkt_BOM.ButtonName;
            bool flag = false;
            vmMkt_BOM = new VmMkt_BOM();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            vmMkt_BOM.User_User = vMUser.User_User;
            vmMkt_BOM.Header = "Bill Of Materials(In Progress)";
            vmMkt_BOM.ButtonName = "Close";
            if (id != null)
            {
                flag = true;
                vmMkt_BOM.Header = "Bill Of Materials(Complete)";
                vmMkt_BOM.ButtonName = "ReOpen";

            }

            await Task.Run(() => vmMkt_BOM.InitialBOMSearch(flag, SearchString));
            return View(vmMkt_BOM);
        }





        [HttpGet]
        public ActionResult VmMkt_BOMCreate(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            vmMkt_BOM = new VmMkt_BOM();
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetMktCategoryForDropDown();
            SelectList sl9 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktCategory = sl9;

            VmForDropDown.DDownData = VmForDropDown.GetMktSubCategoryForDropDown();
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktsCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetMktItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Item = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Buyer = sl1;

            vmMkt_BOM.Mkt_BOM = new Mkt_BOM();
            vmMkt_BOM.Mkt_BOM.FirstMove = DateTime.Today;
            vmMkt_BOM.Mkt_BOM.UpdateDate = DateTime.Today;
            vmMkt_BOM.Mkt_BOM.IsNew = true;
            vmMkt_BOM.Mkt_BOM.SetQuantity = 1;
            int iD = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            vmMkt_BOM.Mkt_BOM.Common_TheOrderFk = iD;
            return View(vmMkt_BOM);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMCreate(VmMkt_BOM vmMkt_BOM)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            vmMkt_BOM.Mkt_BOM.UpdateDate = vmMkt_BOM.Mkt_BOM.FirstMove;
            vmMkt_BOM.Mkt_BOM.UnitPrice = vmMkt_BOM.Mkt_BOM.PackPrice / vmMkt_BOM.Mkt_BOM.Quantity;
            VmControllerHelper vmControllerHelper = new VmControllerHelper();

            vmMkt_BOM.Mkt_BOM.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;

            await Task.Run(() => vmMkt_BOM.Add(UserID));
            if (vmMkt_BOM.Mkt_BOM.Common_TheOrderFk != null)
            {
                return RedirectToAction("Mkt_OrderSlaveBOM/" + vmMkt_BOM.Mkt_BOM.Common_TheOrderFk, "Common");
            }

            return RedirectToAction("VmMkt_BOMIndex");

        }

        [HttpGet]
        public async Task<ActionResult> VmMkt_BOMEdit(string id)
        {

            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();

            string editID = id.Substring(0, id.LastIndexOf("ʁ"));
            string rID = id.Substring(id.Length - id.LastIndexOf("ʁ"));

            id = editID + "ʁ" + rID;

            if (editID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vmMkt_BOM = new VmMkt_BOM();

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Buyer = sl1;


            //await Task.Run(() => vmMkt_BOM.InitialDataLoad());
            await Task.Run(() => vmMkt_BOM.SelectSingle(editID));

            VmMkt_Item x = new VmMkt_Item();
            int itemID = vmMkt_BOM.Mkt_BOM.Mkt_ItemFK;

            string EnItemID = VmControllerHelper.Encrypt(itemID.ToString());

            x.SelectSingle(EnItemID);
            vmMkt_BOM.Mkt_Item = x.Mkt_Item;

            if (vmMkt_BOM.Mkt_BOM == null)
            {
                return HttpNotFound();
            }
            VmForDropDown.DDownData = VmForDropDown.GetMktCategoryForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktCategory = sl6;


            VmForDropDown.DDownData = VmForDropDown.GetTempMktItemForEdit(EnItemID);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Item = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetTempMktSubCategoryForEdit(EnItemID);
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktSubCategory = sl5;


            return View(vmMkt_BOM);


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMEdit(VmMkt_BOM VmMkt_BOM)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_BOM.Mkt_BOM.UpdateDate = DateTime.Now;

            string commonOrId = "";
            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            VmMkt_BOM.Mkt_BOM.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            VmMkt_BOM.Mkt_BOM.UnitPrice = VmMkt_BOM.Mkt_BOM.PackPrice / VmMkt_BOM.Mkt_BOM.Quantity;

            await Task.Run(() => VmMkt_BOM.Edit(0));
            if (VmMkt_BOM.Mkt_BOM.Common_TheOrderFk != null)
            {
                int? comOrID = VmMkt_BOM.Mkt_BOM.Common_TheOrderFk;
                string comOrderID = comOrID.ToString();
                VmControllerHelper = new VmControllerHelper();
                commonOrId = VmControllerHelper.Encrypt(comOrderID);

                return RedirectToAction("Mkt_OrderSlaveBOM/" + commonOrId, "Common");


            }
            return RedirectToAction("Mkt_OrderSlaveBOM/" + commonOrId, "Common");
        }

        [HttpGet]
        public async Task<ActionResult> ChangeBOMAuthorizationStatus(string id, string pageType = "")
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_BOM VmMkt_BOM = new VmMkt_BOM();
            VmMkt_BOM.Mkt_BOM = new Mkt_BOM();
            VmMkt_BOM.PageType = pageType;

            await Task.Run(() => VmMkt_BOM.GetBOMByID(id));
            if (VmMkt_BOM.Mkt_BOM == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_BOM);
        }

        [HttpPost]

        public async Task<ActionResult> ChangeBOMAuthorizationStatus(VmMkt_BOM a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            await Task.Run(() => a.BOMAuthorization(0));
            if (a.PageType == "BOMDetails")
            {
                return RedirectToAction("Mkt_OrderSlaveBOM", "Common", new { id = a.Mkt_BOM.Common_TheOrderFk });
            }
            return RedirectToAction("VmMkt_BOMIndex");


        }



        //.......For Dropdown.......//

        public List<object> GetTempRaw_Item()
        {
            var rawItemList = new List<object>();

            foreach (var raw_Item in db.Raw_Item.Where(d => d.Active == true))
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });

            }
            return rawItemList;
        }
        public List<object> GetTempBuyer()
        {
            var BuyerList = new List<object>();

            foreach (var buyer in db.Mkt_Buyer.Where(d => d.Active == true))
            {
                BuyerList.Add(new { Text = buyer.Name, Value = buyer.ID });
            }
            return BuyerList;
        }
        //.......For Dropdown.......//
        public List<object> GetTempMktCategory()
        {
            var categoryList = new List<object>();

            foreach (var category in db.Mkt_Category.Where(x => x.Active == true))
            {
                categoryList.Add(new { Text = category.Name, Value = category.ID });
            }
            return categoryList;
        }

        public List<object> GetTempMktSubCategory()
        {
            var sCategoryList = new List<object>();

            foreach (var sCategory in db.Mkt_SubCategory.Where(x => x.Active == true))
            {
                sCategoryList.Add(new { Text = sCategory.Name, Value = sCategory.ID });
            }
            return sCategoryList;
        }

        public List<object> GetTempRawSubCategory()
        {
            var scl = new List<object>();

            foreach (var sc in db.Raw_SubCategory.Where(d => d.Active == true))
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }

        public List<object> GetTempItem()
        {
            var ItemList = new List<object>();

            foreach (var item in db.Mkt_Item.Where(d => d.Active == true))
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }
        public List<object> GetTempCurrency()
        {
            var CurrencyList = new List<object>();

            foreach (var buyer in db.Common_Currency.Where(d => d.Active == true))
            {
                CurrencyList.Add(new { Text = buyer.Name, Value = buyer.ID });
            }
            return CurrencyList;
        }
        public List<object> GetTempUnit()
        {
            var unitList = new List<object>();

            foreach (var unit in db.Common_Unit.Where(d => d.Active == true))
            {
                unitList.Add(new { Text = unit.Name, Value = unit.ID });
            }
            return unitList;
        }
        public List<object> GetTempMktPO()
        {
            var POList = new List<object>();

            foreach (var po in db.Mkt_PO.Where(d => d.Active == true))
            {
                POList.Add(new { Text = po.CID, Value = po.ID });
            }
            return POList;
        }
        public List<object> GetTempSupplier()
        {
            var supplierList = new List<object>();

            foreach (var supplier in db.Common_Supplier.Where(d => d.Active == true))
            {
                supplierList.Add(new { Text = supplier.Name, Value = supplier.ID });
            }
            return supplierList;
        }
        public List<object> GetTempBOMCID()
        {
            var List = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM
                     on t1.ID equals t2.Common_TheOrderFk
                     where t2.Active == true
                     select new
                     {
                         CID = t1.CID + "/" + t2.Style,
                         ID = t2.ID
                     }).OrderByDescending(x => x.ID);

            foreach (var cid in v)
            {
                List.Add(new { Text = cid.CID, Value = cid.ID });
            }
            return List;
        }
        public List<object> GetRequisitionCID()
        {
            var List = new List<object>();
            var v = (from t1 in db.Prod_Requisition
                     join t2 in db.Mkt_BOM on t1.Mkt_BOMFK equals t2.ID
                     where t2.Active == true
                     select new
                     {
                         CID = t2.CID + "/Req/" + t1.ID,
                         ID = t2.ID
                     }).OrderByDescending(x => x.ID);

            foreach (var cid in v)
            {
                List.Add(new { Text = cid.CID, Value = cid.ID });
            }
            return List;
        }
        public List<object> GetTempBOM()
        {
            var BOMList = new List<object>();

            foreach (var bom in db.Mkt_BOM.Where(d => d.Active == true))
            {
                BOMList.Add(new { Text = bom.CID, Value = bom.ID });
            }
            return BOMList;
        }

        public List<object> GetTempRawItem()
        {
            var rawItemList = new List<object>();

            foreach (var raw_Item in db.Raw_Item.Where(d => d.Active == true))
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });
            }
            return rawItemList;
        }

        public ActionResult VmMkt_BOMDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmControllerHelper = new VmControllerHelper();

            //string s = id.Substring(0, id.LastIndexOf("ʁ"));
            //string rID = id.Substring(id.Length - id.LastIndexOf("ʁ"));
            //string dID = VmControllerHelper.Decrypt(s);
            int deletID = 0, rID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("ʁ")), out deletID);

            id = id.Replace(deletID.ToString() + "ʁ", "");
            int.TryParse(id, out rID);
            //int deleteId = 0;
            //Int32.TryParse(dID, out deleteId);
            //VmMkt_BOM x = new VmMkt_BOM();
            EditUpdateDate(0, deletID);
            //x.Mkt_BOM.UpdateDate = DateTime.Now;
            //x.Edit(0);

            Mkt_BOM b = new Mkt_BOM();
            b.ID = deletID;
            b.Delete(0);
            //vmMkt_BOM = new VmMkt_BOM();
            //vmMkt_BOM.

            if (rID != null)
            {
                return RedirectToAction("Mkt_OrderSlaveBOM/" + rID, "Common");
            }

            return RedirectToAction("VmMkt_BOMIndex");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditUpdateDate(int userID, int bomID)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_BOM x = new VmMkt_BOM();
            x.SelectSingle(bomID.ToString());
            x.Mkt_BOM.UpdateDate = DateTime.Now;
            await Task.Run(() => x.Mkt_BOM.Edit(userID));
            return View();
        }

        //.............PurchaseOrderWaiting.............//
        //public async Task<ActionResult> PurchaseOrderWaiting()
        //{
        //    VmMkt_PO = new VmMkt_PO();
        //    await Task.Run(() => VmMkt_PO.POWaitingDataLoad());
        //    return View(VmMkt_PO);
        //}
        //............PurchaseOrderReady............//

        public async Task<ActionResult> PurchaseOrderReady()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.POReadyDataLoad());
            return View(VmMkt_BOMSlave);
        }
        //............PurchaseOrderSetteled............//

        public async Task<ActionResult> PurchaseOrderSetteled()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.POSettledDataLoad());
            return View(VmMkt_PO);
        }


        [HttpGet]
        public async Task<ActionResult> PurchaseOrdersEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForCreatePO();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetBropDownBOMCID();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BomCID = sl1;

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSinglePO(id.Value));

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PurchaseOrdersEdit(Mkt_PO Mkt_PO)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            Mkt_PO.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;

            if (ModelState.IsValid)
            {
                await Task.Run(() => Mkt_PO.Edit(0));
                return RedirectToAction("PurchaseOrders");
            }
            return View(VmMkt_PO);
        }
        public ActionResult PurchaseOrdersDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Mkt_PO mkt_PO = new Mkt_PO();
            mkt_PO.ID = id;
            mkt_PO.Delete(0);
            VmMktPO_Slave = new VmMktPO_Slave();
            VmMktPO_Slave.DaleteBulk(UserID, id);

            return RedirectToAction("PurchaseOrders");
        }

        public async Task<ActionResult> VmMkt_PODetails(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO = VmMkt_PO.SelectSingleJoined(id));
            VmMkt_PO.VmMktPO_Slave = new VmMktPO_Slave();
            await Task.Run(() => VmMkt_PO.VmMktPO_Slave.GetPODetails(id));
            return View(VmMkt_PO);
        }



        //Those Method used for cascading Dropdown
        //[HttpGet]
        public JsonResult GetRaw_SubCategory(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_SubCategory.Where(p => p.Raw_CategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetMkt_SubCategory(int? id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_SubCategory.Where(p => p.Mkt_CategoryFK == id).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        public JsonResult GetRaw_Item(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_Item.Where(p => p.Raw_SubCategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        public JsonResult GetMkt_Item(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_Item.Where(p => p.Mkt_SubCategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        public JsonResult GetMKT_SubCategory(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_SubCategory.Where(p => p.Mkt_CategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }

        public ActionResult VmMkt_PODeletePurchaseOrderWaiting(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Mkt_PO mkt_PO = new Mkt_PO();
            mkt_PO.ID = id;
            mkt_PO.Delete(0);
            return RedirectToAction("PurchaseOrderWaiting");
        }
        public ActionResult VmMkt_POPurchaseOrderWaiting(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ///////////
            VmMkt_PO vmMkt_PO = new VmMkt_PO();
            vmMkt_PO.SelectSingle(id);

            vmMkt_PO.Mkt_PO.CID = vmMkt_PO.Mkt_BOM.CID + "/" + id.ToString();
            vmMkt_PO.Mkt_PO.Status = 1;
            vmMkt_PO.Mkt_PO.Edit(UserID);
            return RedirectToAction("PurchaseOrderWaiting");
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_PORecieve(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingle(id.Value));

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);


        }



        [HttpGet]
        public async Task<ActionResult> VmMkt_POSend(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingle(id.Value));

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        //..........VmMkt_Category..........//
        public async Task<ActionResult> VmMkt_CategoryIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_Category = new VmMkt_Category();
            await Task.Run(() => VmMkt_Category.InitialDataLoad());
            return View(VmMkt_Category);
        }


        public ActionResult VmMkt_CategoryCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_CategoryCreate(VmMkt_Category a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmMkt_CategoryIndex");
            }
            return RedirectToAction("VmMkt_CategoryIndex");
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_CategoryEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_Category = new VmMkt_Category();
            await Task.Run(() => VmMkt_Category.SelectSingle(id));

            if (VmMkt_Category.Mkt_Category == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_Category);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_CategoryEdit(VmMkt_Category vmmkt_Category)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => vmmkt_Category.Edit(0));
                return RedirectToAction("VmMkt_CategoryIndex");
            }
            return View(vmmkt_Category);
        }
        public ActionResult VmMkt_CategoryDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            int cid = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out cid);
            Mkt_Category b = new Mkt_Category();
            b.ID = cid;
            b.Delete(0);
            return RedirectToAction("VmMkt_CategoryIndex");
        }
        /////.....VmMkt_SubCategory.....////
        public async Task<ActionResult> VmMkt_SubCategoryIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_SubCategory = new VmMkt_SubCategory();
            await Task.Run(() => VmMkt_SubCategory.InitialDataLoad());
            return View(VmMkt_SubCategory);
        }
        public ActionResult VmMkt_SubCategoryCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMktCategoryForDropDown();

            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktCategory = sl2;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_SubCategoryCreate(VmMkt_SubCategory a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmMkt_SubCategoryIndex");
            }
            return RedirectToAction("VmMkt_SubCategoryIndex");
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_SubCategoryEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMktCategoryForDropDown();

            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktCategory = sl2;

            VmMkt_SubCategory = new VmMkt_SubCategory();
            await Task.Run(() => VmMkt_SubCategory.SelectSingle(id));

            if (VmMkt_SubCategory.Mkt_SubCategory == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_SubCategory);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_SubCategoryEdit(VmMkt_SubCategory sub)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => sub.Edit(0));
                return RedirectToAction("VmMkt_SubCategoryIndex");
            }
            return View(sub);

        }

        public ActionResult VmMkt_SubCategoryDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int scatid = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out scatid);
            Mkt_SubCategory b = new Mkt_SubCategory();
            b.ID = scatid;
            b.Delete(0);
            return RedirectToAction("VmMkt_SubCategoryIndex");
        }

        ///.....VmMkt_Item.....///
        public async Task<ActionResult> VmMkt_ItemIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_Item = new VmMkt_Item();
            await Task.Run(() => VmMkt_Item.InitialDataLoad());
            return View(VmMkt_Item);
        }

        public ActionResult VmMkt_ItemCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMktCategoryForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktCategory = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetMktSubCategoryForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktsCategory = sl3;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_ItemCreate(VmMkt_Item a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmMkt_ItemIndex");
            }
            return RedirectToAction("VmMkt_ItemIndex");
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_ItemEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMktCategoryForDropDown();

            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktCategory = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetTempMktSubCategoryForEdit(id);
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MktSubCategory = sl5;

            VmMkt_Item = new VmMkt_Item();
            await Task.Run(() => VmMkt_Item.SelectSingle(id));

            if (VmMkt_Item.Mkt_Item == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_Item);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_ItemEdit(VmMkt_Item item)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => item.Edit(0));
                return RedirectToAction("VmMkt_ItemIndex");
            }
            return View(item);
        }
        public ActionResult VmMkt_ItemDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int iId = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iId);
            Mkt_Item b = new Mkt_Item();
            b.ID = iId;
            b.Delete(0);
            return RedirectToAction("VmMkt_ItemIndex");
        }
        //Dashboard of Marketing

        public ActionResult Dashboard()
        {

            return View();
        }

        //........PurchaseOrders......//
        public async Task<ActionResult> PurchaseOrders(VmMkt_PO VmMkt_PO, int? id, bool isclosed = false)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            string SearchString = VmMkt_PO.SearchString;
            int theID = 0;
            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.GetPO(theID, isclosed));
            if (VmMkt_PO.POHeader == "Purchase Orders :")
            {
                if (id != null && id.Value <= 2)
                {
                    VmMkt_PO.Header = "Purchase Orders";
                    theID = id.Value;
                }
                return View(VmMkt_PO);
            }
            return View(VmMkt_PO);
        }

        [HttpPost]
        public async Task<ActionResult> PurchaseOrders(VmMkt_PO VmMkt_PO)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string SearchString = VmMkt_PO.SearchString;

            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.PurchaseOrderSearch(SearchString));

            return View(VmMkt_PO);

        }


        public async Task<ActionResult> PurchaseOrdersAll(VmMkt_PO VmMkt_PO, int? id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            string SearchString = VmMkt_PO.SearchString;
            int theID = 0;
            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.GetPOAll(theID));
            if (VmMkt_PO.POHeader == "All Purchase Orders :")
            {
                if (id != null && id.Value <= 2)
                {
                    VmMkt_PO.Header = "All Purchase Orders";
                    theID = id.Value;
                }
                return View(VmMkt_PO);
            }
            return View(VmMkt_PO);
        }

        [HttpPost]
        public async Task<ActionResult> PurchaseOrdersAll(VmMkt_PO VmMkt_PO)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string SearchString = VmMkt_PO.SearchString;

            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.PurchaseOrderSearch(SearchString));

            return View(VmMkt_PO);

        }
        public async Task<ActionResult> PurchaseOrdersByBOM(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            VmMkt_PO = new VmMkt_PO();
            vmMkt_BOM = new VmMkt_BOM();
            await Task.Run(() => VmMkt_PO.VmMkt_BOM = vmMkt_BOM.SelectSingleJoined(id.ToString()));
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.GetPOByBOM(id));

            return View(VmMkt_PO);
        }

        public async Task<ActionResult> AuthorizedPurchaseOrders()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            //string SearchString = VmMkt_PO.SearchString;

            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;

            await Task.Run(() => VmMkt_PO.GetAuthorizedPurchaseOrders());


            return View(VmMkt_PO);
        }
        [HttpPost]
        public async Task<ActionResult> AuthorizedPurchaseOrders(VmMkt_PO VmMkt_PO)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string SearchString = VmMkt_PO.SearchString;

            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.GetAuthorizedPurchaseOrdersSearch(SearchString));

            return View(VmMkt_PO);

        }
        public async Task<ActionResult> AlsoATransferMktPo(int id, string returnurl)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.AlsoATransfer(id, user.User_User.ID));

            return RedirectToAction(returnurl);

        }


        public async Task<ActionResult> PurchaseOrdersAuthorization(VmMkt_PO VmMkt_PO, int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            string SearchString = VmMkt_PO.SearchString;
            int theID = 0;
            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.PurchaseOrderView(theID));

            if (id != null && id.Value <= 2)
            {
                VmMkt_PO.Header = "Purchase Orders";
                theID = id.Value;
            }


            return View(VmMkt_PO);


        }
        [HttpPost]
        public async Task<ActionResult> PurchaseOrdersAuthorization(VmMkt_PO VmMkt_PO)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string SearchString = VmMkt_PO.SearchString;

            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.PurchaseOrderSearch(SearchString));

            return View(VmMkt_PO);

        }



        [HttpGet]
        public ActionResult PurchaseOrdersCreate(int bomId = 0, string pageType = "")
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();


            VmForDropDown = new VmForDropDown();
            VmForDropDown.User_User = vMUser.User_User;
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;
            VmForDropDown.DDownData = VmForDropDown.GetSupplierForCreatePO();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetDropDownBOMCID();
            SelectList sl = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BomCID = sl;

            VmForDropDown.DDownData = VmForDropDown.GetRequisitionDropDownCID();
            SelectList sR = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RequisitionCID = sR;

            VmForDropDown.DDownData = VmForDropDown.GetStatusDropDown();
            SelectList status = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Status = status;

            VmMkt_PO = new VmMkt_PO();
            VmMkt_PO.Mkt_PO = new Mkt_PO();
            VmMkt_PO.Mkt_PO.Date = DateTime.Today;
            VmMkt_PO.Mkt_PO.Mkt_BOMFK = bomId;
            VmMkt_PO.PageType = pageType;
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PurchaseOrdersCreate(VmMkt_PO a)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //this line added by mamun
            VmForDropDown = new VmForDropDown();
            //-- end
            VmForDropDown.DDownData = VmForDropDown.GetPOPaymentType();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PaymentList = sl2;

            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            a.Mkt_PO.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            UserID = vmControllerHelper.GetCurrentUser().User_User.ID;

            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (ModelState.IsValid)
            {
                a.Mkt_PO.Status = 0;
                await Task.Run(() => a.Add(UserID));

                if (a.PageType == "POByBOM")
                {
                    return RedirectToAction("PurchaseOrdersByBOM", "Marketing", new { id = a.Mkt_PO.Mkt_BOMFK });
                }
                return RedirectToAction("PurchaseOrders");
            }
            return RedirectToAction("PurchaseOrders");
        }

        [HttpGet]
        public async Task<ActionResult> ChangePOAuthorizationStatus(int? id, string pageType = "")
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPOPaymentType();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PaymentList = sl2;

            VmMkt_PO vmMkt_PO = new VmMkt_PO();
            vmMkt_PO.Mkt_PO = new Mkt_PO();
            vmMkt_PO.Mkt_PO.ID = id.Value;
            vmMkt_PO.PageType = pageType;
            await Task.Run(() => vmMkt_PO.SelectSingle(id));
            if (vmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            vmMkt_PO.Mkt_PO.AuthorizeDate = DateTime.Now;

            return View(vmMkt_PO);
        }

        [HttpPost]

        public async Task<ActionResult> ChangePOAuthorizationStatus(VmMkt_PO a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            await Task.Run(() => a.POAuthorization(0));

            if (a.PageType == "POByBOM")
            {
                return RedirectToAction("PurchaseOrdersByBOM", "Marketing", new { id = a.Mkt_PO.Mkt_BOMFK });
            }

            return RedirectToAction("Purchaseorders", "Marketing");
        }


        public async Task<ActionResult> PurchaseOrdersSlave(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var returnurl = id;
            int poID = 0, bomID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out poID);
            int supplierid = 0;
            id = id.Replace(poID.ToString() + "x", "");
            int.TryParse(id.Substring(0, id.LastIndexOf("y")), out supplierid);
            id = id.Replace(supplierid.ToString() + "y", "");
            int.TryParse(id, out bomID);

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO = VmMkt_PO.SelectSingleJoined(poID));

            Mkt_POSlave pos = new Mkt_POSlave();
            pos.Mkt_POFK = poID;

            VmMkt_PO.VmMktPO_Slave = new VmMktPO_Slave();
            await Task.Run(() => VmMkt_PO.VmMktPO_Slave.GetPOSlave(poID, supplierid, bomID));
            await Task.Run(() => VmMkt_PO.VmMktPO_Slave.GetPOStepJobs(poID, supplierid, bomID));

            VmMkt_PO.TotalVal = VmMkt_PO.VmMktPO_Slave.DataList.Sum(x => x.LineTotal);

            VmMkt_PO.VmMkt_POExtTransfer = new VmMkt_POExtTransfer();
            await Task.Run(() => VmMkt_PO.VmMkt_POExtTransfer.InitialDataLoad_POExtTransfer(poID));
            VmMkt_PO.VmMkt_POExtTransfer.ReturnController = "marketing/PurchaseOrdersSlave/" + returnurl;

            return View(VmMkt_PO);
        }
        [HttpGet]
        public ActionResult VmMktPO_SlaveCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetMktPODropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PO = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            ViewModels.Store.VmMktPO_Slave vmMkt_POSlave = new VmMktPO_Slave();
            vmMkt_POSlave.Mkt_POSlave = new Mkt_POSlave();
            vmMkt_POSlave.Mkt_POSlave.Mkt_POFK = id;

            return View(vmMkt_POSlave);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMktPO_SlaveCreate(Mkt_POSlave mkt_POSlave)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                mkt_POSlave.AddReady(UserID);
                await Task.Run(() => mkt_POSlave.Add());
                return RedirectToAction("PurchaseOrdersSlave/" + mkt_POSlave.Mkt_POFK);
            }
            return RedirectToAction("PurchaseOrdersSlave/" + mkt_POSlave.Mkt_POFK);
        }

        [HttpGet]
        public async Task<ActionResult> VmMktPOSlaveEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMktPODropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PO = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmMktPO_Slave = new VmMktPO_Slave();
            await Task.Run(() => VmMktPO_Slave.SelectSingle(id.Value));
            if (VmMktPO_Slave.Mkt_POSlave == null)
            {
                return HttpNotFound();
            }
            //no chg

            VmForDropDown.DDownData = VmForDropDown.GetTempRaw_ItemForEdit(VmMktPO_Slave.Mkt_POSlave.Raw_ItemFK);

            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Item = sl2;
            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryDropDownForEdit(VmMktPO_Slave.Mkt_POSlave.Raw_ItemFK.ToString());
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryDropDownForEdit(VmMktPO_Slave.Mkt_POSlave.Raw_ItemFK);
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;
            return View(VmMktPO_Slave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMktPOSlaveEdit(VmMktPO_Slave VmMktPO_Slave)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmMktPO_Slave.Edit(0));
                return RedirectToAction("PurchaseOrdersSlave/" + VmMktPO_Slave.Mkt_POSlave.Mkt_POFK);
            }
            return View(VmMktPO_Slave);
        }


        public ActionResult VmMkt_POSlaveDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deletID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("z")), out deletID);

            id = id.Replace(deletID.ToString() + "z", "");
            Mkt_POSlave b = new Mkt_POSlave();
            b.ID = deletID;
            b.Delete(0);
            return RedirectToAction("PurchaseOrdersSlave/" + id);
        }

        /////////////////// //-------Report-------//////////////////////////

        //........BOMViewerReport............//

        public ActionResult BOMReportDoc(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                IdNullDivertion("BOMReportDoc", "Id is Null");
            }
            try
            {
                VmMkt_BOM b = new VmMkt_BOM();
                /////
                VmCommon_TheOrder y = new VmCommon_TheOrder();
                y.Common_TheOrder = new Common_TheOrder();
                b.Mkt_BOM = new Mkt_BOM();
                b.SelectSingle(id);
                y.SelectSingle(b.Mkt_BOM.Common_TheOrderFk.Value.ToString());
                b.Common_TheOrder = new Common_TheOrder();
                b.BOMReportDocLoad(id);
                ////////////////////
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/BOMBaseDoc.rpt");
                cr.Load();
                cr.SetDataSource(b.BOMReportDoc);
                /////
                cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("BOMReportDoc", ex.Message);
            }
            return null;
        }

        public ActionResult CBSReportDoc(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                IdNullDivertion("BOMReportDoc", "Id is Null");
            }
            try
            {
                VmMkt_CBS b = new VmMkt_CBS();

                b.SelectSingle(id);


                b.CBSReportDocLoad(id);
                ///////
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/CBSBaseDoc.rpt");
                cr.Load();
                cr.SetDataSource(b.CBCReportDoc);
                /////
                cr.SummaryInfo.ReportTitle = "Report of " + b.Mkt_CBSMaster.Style;
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {
                IdNullDivertion("BOMReportDoc", ex.Message);
            }
            return null;
        }



        //........FabricViewerReport............//
        public ActionResult FabricReportDoc(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                IdNullDivertion("FabricReportDoc", "Id is Null");
            }
            try
            {
                VmMkt_BOMSlaveFabric f = new VmMkt_BOMSlaveFabric();

                VmMkt_BOMSlave x = new VmMkt_BOMSlave();
                x.Mkt_BOMSlave = new Mkt_BOMSlave();
                VmControllerHelper = new VmControllerHelper();

                x.SelectSingle(id);

                VmMkt_BOM b = new VmMkt_BOM();
                b.Mkt_BOM = new Mkt_BOM();
                b.SelectSingle(x.Mkt_BOMSlave.Mkt_BOMFK.Value.ToString());

                VmCommon_TheOrder y = new VmCommon_TheOrder();
                y.Common_TheOrder = new Common_TheOrder();
                y.SelectSingle(b.Mkt_BOM.Common_TheOrderFk.Value.ToString());

                f.FabricReportDocLoad(id);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/FabricBaseDoc.rpt");
                cr.Load();
                cr.SetDataSource(f.FebricReportDoc);
                cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("FabricReportDoc", ex.Message);
            }
            return null;
        }



        /////////////////// //-------PO_Report-------//////////////////////////

        public ActionResult POReportDoc(int? id)
        {

            if (!id.HasValue)
            {
                IdNullDivertion("POReportDoc", "Id is Null");
            }
            try
            {
                VmMkt_PO p = new VmMkt_PO();
                p.Mkt_PO = new Mkt_PO();
                p.SelectSingle(id.Value);

                //if (p.Mkt_PO.IsAuthorize == false)
                // {
                //return Redirect("/Home/ErrorView/This PO is not authorized yet. Please Contact Admin");
                //NotAuthorisedDivertion("POReportDoc", "This PO is not Authorized yet!");
                // }
                // else
                // {
                p.POReportDocLoad(id.Value);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/POBaseDoc.rpt");
                cr.Load();
                cr.SetDataSource(p.POReportDoc);
                cr.SummaryInfo.ReportTitle = "Report Of " + p.Mkt_PO.CID;
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
                // }


            }
            catch (Exception ex)
            {
                IdNullDivertion("POReportDoc", ex.Message);
            }
            return null;
        }
        private ActionResult IdNullDivertion(string methodName, string messege)
        {
            ErrorReports er = new ErrorReports();
            er.ControllerName = this.ToString();
            er.MethodName = methodName;
            er.ErrorDescription = "Id is Null";
            er.EmailErrorAuto();

            return View("../Home/ErrorView", er);
        }
        private ActionResult NotAuthorisedDivertion(string methodName, string messege)
        {
            ErrorReports er = new ErrorReports();
            er.ControllerName = this.ToString();
            er.MethodName = methodName;
            er.ErrorDescription = messege;
            //er.EmailErrorAuto();
            //return  RedirectToAction("ErrorView" + messege + "/", "Home");
            return Redirect("/Home/ErrorView");
            //        return RedirectToAction("ErrorView", new RouteValueDictionary(
            //new { controller = "Home", action = "ErrorView" }));
        }

        //----------------Item Waiting----------------////
        public async Task<ActionResult> PurchaseOrderWaiting()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_BOMSlave = new VmMkt_BOMSlave();
            await Task.Run(() => VmMkt_BOMSlave.POWaitingDataLoad());
            return View(VmMkt_BOMSlave);
        }


        public async Task<ActionResult> Plan_OrderProcessIndex(int? id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            vmPlan_OrderStep = new VmPlan_OrderStep();
            await Task.Run(() => vmPlan_OrderStep = vmPlan_OrderStep.SelectSingleJoined(id.Value));
            Plan_OrderStep plan_OrderStep = new Plan_OrderStep();
            plan_OrderStep.CommonTheOrderFK = id;


            await Task.Run(() => vmPlan_OrderStep.GetPlan_OrderStep(id.Value));

            //vmMkt_BOM.SuggestedVmMkt_PO = new VmMkt_PO();
            //await Task.Run(() => vmMkt_BOM.SuggestedVmMkt_PO.GetSuggestedPOFromBOM(id.Value));

            return View(vmPlan_OrderStep);
        }

        //----------------------------MKT BOM Slave Fabric--------------------------//
        public async Task<ActionResult> VmMkt_BOMSlaveFabricIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_BOMSlaveFabric = new VmMkt_BOMSlaveFabric();
            await Task.Run(() => VmMkt_BOMSlaveFabric.InitialDataLoad());
            return View(VmMkt_BOMSlaveFabric);
        }

        public ActionResult VmMkt_BOMSlaveFabricCreate(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            //VmMkt_BOMSlaveFabric VmMkt_BOMSlaveFabric = new VmMkt_BOMSlaveFabric();
            //VmMkt_BOMSlaveFabric.SelectSingle(id.Value);
            VmMkt_BOMSlave VmMkt_BOMSlave = new VmMkt_BOMSlave();
            //VmMkt_BOMSlave.SelectSingle(VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Mkt_BOMSlaveFK.Value);
            VmMkt_BOMSlave.SelectSingle(id);
            VmMkt_BOM VmMkt_BOM = new VmMkt_BOM();
            VmMkt_BOM.Mkt_BOMSlave = new Mkt_BOMSlave();
            int _BOMFK = VmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK.Value;
            string mkt_BOMFK = _BOMFK.ToString();
            VmControllerHelper = new VmControllerHelper();
            string BOMFK = VmControllerHelper.Encrypt(mkt_BOMFK);
            VmMkt_BOM.SelectSingle(BOMFK);

            VmMkt_BOMSlaveFabric VmMkt_BOMSlaveFabric = new VmMkt_BOMSlaveFabric();
            VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric = new Mkt_BOMSlaveFabric();
            int fabricid = 0;
            string str = VmControllerHelper.Decrypt(id);
            Int32.TryParse(str, out fabricid);
            VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Mkt_BOMSlaveFK = fabricid;

            VmMkt_BOMSlaveFabric.Mkt_BOM = VmMkt_BOM.Mkt_BOM;
            VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.RequiredQuantity = 0;

            return View(VmMkt_BOMSlaveFabric);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMSlaveFabricCreate(VmMkt_BOMSlaveFabric VmMkt_BOMSlaveFabric)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.RequiredQuantity == 0 || VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.RequiredQuantity == null)
            {

                VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.RequiredQuantity = VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Consumption * VmMkt_BOMSlaveFabric.Mkt_BOM.QPack * VmMkt_BOMSlaveFabric.Mkt_BOM.Quantity / 12;
            }
            VmControllerHelper = new VmControllerHelper();
            int i = VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Mkt_BOMSlaveFK.Value;
            string s = i.ToString();
            string redirectTo = VmControllerHelper.Encrypt(s);
            await Task.Run(() => VmMkt_BOMSlaveFabric.Add(UserID));
            return RedirectToAction("AddFabricItem/" + redirectTo);

        }

        [HttpGet]
        public async Task<ActionResult> VmMkt_BOMSlaveFabricEdit(string id)
        {

            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string editID = id.Substring(0, id.LastIndexOf("ط"));
            string redID = id.Substring(id.Length - id.LastIndexOf("ط"));
            VmControllerHelper = new VmControllerHelper();
            string s = VmControllerHelper.Decrypt(redID);
            int reid = 0;
            Int32.TryParse(s, out reid);



            //int.TryParse(id, out redID);
            VmMkt_BOMSlaveFabric = new VmMkt_BOMSlaveFabric();
            VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric = new Models.Marketing.Mkt_BOMSlaveFabric();

            VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Mkt_BOMSlaveFK = reid;

            VmMkt_BOMSlave VmMkt_BOMSlave = new VmMkt_BOMSlave();
            int i = VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Mkt_BOMSlaveFK.Value;
            string BOMSlaveFK = i.ToString();
            string bOMSlaveFK = VmControllerHelper.Encrypt(BOMSlaveFK);
            VmMkt_BOMSlave.SelectSingle(bOMSlaveFK);
            VmMkt_BOM VmMkt_BOM = new VmMkt_BOM();
            VmMkt_BOM.Mkt_BOMSlave = new Mkt_BOMSlave();
            int BOMFK = VmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK.Value;
            string bOMFK = BOMFK.ToString();
            VmControllerHelper.Encrypt(bOMFK);
            VmMkt_BOM.SelectSingle(bOMFK);
            VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric = new Mkt_BOMSlaveFabric();

            VmMkt_BOMSlaveFabric.Mkt_BOM = VmMkt_BOM.Mkt_BOM;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            await Task.Run(() => VmMkt_BOMSlaveFabric.SelectSingle(editID));

            if (VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric == null)
            {
                return HttpNotFound();
            }
            VmForDropDown.DDownData = VmForDropDown.GetRaw_ItemDropDownForEdit(VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Raw_ItemFK);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Item = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryDropDownForEdit(VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Raw_ItemFK.ToString());
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryDropDownForEdit(VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Raw_ItemFK);
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            //VmMkt_BOMSlaveFabric.RawCatID = catID.GetValueOrDefault();
            //VmMkt_BOMSlaveFabric.RawSubCatID = sabCatID.GetValueOrDefault();

            id = editID + "ط" + redID;
            return View(VmMkt_BOMSlaveFabric);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMSlaveFabricEdit(VmMkt_BOMSlaveFabric VmMkt_BOMSlaveFabric)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.RequiredQuantity == 0 || VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.RequiredQuantity == null)
            {
                VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.RequiredQuantity = VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Consumption * VmMkt_BOMSlaveFabric.Mkt_BOM.QPack * VmMkt_BOMSlaveFabric.Mkt_BOM.Quantity / 12;
            }
            VmControllerHelper = new VmControllerHelper();
            int i = VmMkt_BOMSlaveFabric.Mkt_BOMSlaveFabric.Mkt_BOMSlaveFK.Value;
            string s = i.ToString();
            string bOMSlaveFK = VmControllerHelper.Encrypt(s);

            await Task.Run(() => VmMkt_BOMSlaveFabric.Edit(0));
            return RedirectToAction("AddFabricItem/" + bOMSlaveFK);

        }
        public ActionResult VmMkt_BOMSlaveFabricDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string editID = id.Substring(0, id.LastIndexOf("ط"));
            string redID = id.Substring(id.Length - id.LastIndexOf("ط"));

            VmControllerHelper = new VmControllerHelper();
            int deletID = 0, bomSlaveId = 0;
            string s = VmControllerHelper.Decrypt(editID);
            Int32.TryParse(s, out deletID);

            string s2 = VmControllerHelper.Decrypt(redID);
            int.TryParse(s2, out bomSlaveId);

            id = editID + "ط" + redID;


            Mkt_BOMSlaveFabric b = new Mkt_BOMSlaveFabric();
            b.ID = deletID;
            b.Delete(0);
            return RedirectToAction("AddFabricItem/" + bomSlaveId);
        }
        //...........................Mkt_BOMSlaveType............................


        //-------------------------------------//---------------------------------------

        public ActionResult ReportPo()
        {

            try
            {
                VmMkt_Buyer x = new VmMkt_Buyer();
                x.ReportDocLoad();
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/Bom.rpt");
                cr.Load();
                cr.SetDataSource(x.ReportDoc);
                Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                ErrorReports er = new ErrorReports();
                er.ControllerName = "Marketing";
                er.MethodName = "BMR";
                er.ErrorDescription = ex.Message;
                er.EmailErrorAuto();
                return View("../Home/ErrorView", er);
            }
        }

        #region Yarn Calculation 


        public async Task<ActionResult> VmMkt_YarnCalculationIndex(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            vmMkt_BOM = new VmMkt_BOM();
            await Task.Run(() => vmMkt_BOM = vmMkt_BOM.SelectSingleJoined(id.ToString()));

            VmControllerHelper = new VmControllerHelper();
            vmMkt_BOM.VmMkt_YarnCalculation = new VmMkt_YarnCalculation();
            vmMkt_BOM.VmMkt_YarnCalculation.Mkt_YarnCalculation = new Mkt_YarnCalculation();
            if (vmMkt_BOM.Mkt_BOM.IsNew == true)
            {
                await Task.Run(() => vmMkt_BOM.VmMkt_YarnCalculation.GetYarnCalculationByID(id.ToString()));
            }
            else
            {
                await Task.Run(() => vmMkt_BOM.VmMkt_YarnCalculation.GetYarnCalculationByIdOld(id.ToString()));
            }



            vmMkt_BOM.VmMkt_YarnCalculation.Mkt_YarnCalculation.Mkt_BOMFK = id;
            return View(vmMkt_BOM);
        }


        public ActionResult VmMkt_YarnCalculationCreate(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            int orderID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("=")), out orderID);

            id = id.Replace(orderID.ToString() + "=", "");

            VmMkt_YarnCalculation = new VmMkt_YarnCalculation();
            VmMkt_YarnCalculation.Mkt_YarnCalculation = new Mkt_YarnCalculation();
            VmMkt_YarnCalculation.VmMkt_BOM = new VmMkt_BOM();
            VmMkt_YarnCalculation.VmMkt_BOM.SelectSingle(id);

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            ViewBag.RawItem = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            ViewBag.Currency = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            ViewBag.Supplier = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            ViewBag.Unit = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetStyleByOrderDropDown(orderID);
            ViewBag.BOM = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryByCategory();
            ViewBag.Raw_SubCategory = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetMkrYarnType();
            ViewBag.YarnType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmMkt_YarnCalculation.Mkt_YarnCalculation.QntyPcs = VmMkt_YarnCalculation.VmMkt_BOM.Mkt_BOM.QPack * VmMkt_YarnCalculation.VmMkt_BOM.Mkt_BOM.Quantity;
            return View(VmMkt_YarnCalculation);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_YarnCalculationCreate(VmMkt_YarnCalculation VmMkt_YarnCalculation)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (VmMkt_YarnCalculation.Mkt_YarnCalculation.QntyPcs == 0 || VmMkt_YarnCalculation.Mkt_YarnCalculation.QntyPcs == null)
            {
                VmMkt_YarnCalculation.Mkt_YarnCalculation.QntyPcs = VmMkt_YarnCalculation.VmMkt_BOM.Mkt_BOM.QPack * VmMkt_YarnCalculation.VmMkt_BOM.Mkt_BOM.Quantity;
            }

            await Task.Run(() => VmMkt_YarnCalculation.Add(UserID));
            return RedirectToAction("VmMkt_YarnCalculationIndex/" + VmMkt_YarnCalculation.Mkt_YarnCalculation.Mkt_BOMFK);

        }

        [HttpGet]
        public async Task<ActionResult> VmMkt_YarnCalculationEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, orderID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);

            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out orderID);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_YarnCalculation = new VmMkt_YarnCalculation();


            VmForDropDown = new VmForDropDown();


            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            ViewBag.Supplier = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            ViewBag.Unit = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetStyleByOrderDropDown(orderID);
            ViewBag.BOM = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            await Task.Run(() => VmMkt_YarnCalculation.SelectSingle(editId));

            VmForDropDown.DDownData = VmForDropDown.GetRaw_ItemDropDownForEdit(VmMkt_YarnCalculation.Mkt_YarnCalculation.Raw_ItemFK);
            ViewBag.Raw_Item = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryDropDownForEdit(VmMkt_YarnCalculation.Mkt_YarnCalculation.Raw_ItemFK.ToString());
            ViewBag.Raw_SubCategory = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryDropDownForEdit(VmMkt_YarnCalculation.Mkt_YarnCalculation.Raw_ItemFK);
            ViewBag.Raw_Category = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetMkrYarnType();
            ViewBag.YarnType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            if (VmMkt_YarnCalculation.Mkt_YarnCalculation == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_YarnCalculation);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_YarnCalculationEdit(VmMkt_YarnCalculation VmMkt_YarnCalculation)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            await Task.Run(() => VmMkt_YarnCalculation.Edit(0));

            await Task.Run(() => VmMkt_YarnCalculation.EditYarnOnBOMSlave(VmMkt_YarnCalculation.Mkt_YarnCalculation.ID, VmMkt_YarnCalculation.Mkt_YarnCalculation.Mkt_BOMFK));

            return RedirectToAction("VmMkt_YarnCalculationIndex/" + VmMkt_YarnCalculation.Mkt_YarnCalculation.Mkt_BOMFK);

        }
        public ActionResult VmMkt_YarnCalculationDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deletID = 0, bomID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deletID);

            id = id.Replace(deletID.ToString() + "x", "");
            int.TryParse(id, out bomID);

            Mkt_YarnCalculation b = new Mkt_YarnCalculation();
            b.ID = deletID;
            b.Delete(0);
            VmMkt_YarnCalculation vmMktYarnCalculation = new VmMkt_YarnCalculation();
            vmMktYarnCalculation.DeleteBOMSlave(deletID, bomID);

            return RedirectToAction("VmMkt_YarnCalculationIndex/" + bomID);
        }

        public ActionResult YarnCalculationReport(string id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                IdNullDivertion("YarnCalculationReport", "Id is Null");
            }
            try
            {
                VmMkt_BOM b = new VmMkt_BOM();
                b.YarnCalculationByStyle(id);
                ///////
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/YarnCalculationReport.rpt");
                cr.Load();
                cr.SetDataSource(b.BOMReportDoc);
                /////
                //cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {
                IdNullDivertion("YarnCalculationReport", ex.Message);
            }
            return null;
        }
        #endregion


        #region Yarn Statement
        public async Task<ActionResult> YarnStatementLedger()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_BOM vmMktBom = new VmMkt_BOM();
            vmMktBom.FromDate = DateTime.Now.AddDays(-30);
            vmMktBom.ToDate = DateTime.Now;

            return View(vmMktBom);



        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> YarnStatementLedger(VmMkt_BOM vmMktBom)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_BOM x = new VmMkt_BOM();

            ViewBag.YarnRequirement = x.GetYarnRequirement(vmMktBom.FromDate, vmMktBom.ToDate);
            ViewBag.OpenedLC = x.GetOpenedLCForYarn(vmMktBom.FromDate, vmMktBom.ToDate);

            ///ViewBag.YarnSummery = x.GetYarnSummery(vmMktBom.FromDate, vmMktBom.ToDate);
            return View(vmMktBom);



        }
        #endregion
        public async Task<ActionResult> BOMSlaveFabricsPurchaseAndProcessing(string id)
        {
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            vmMkt_BOM = new VmMkt_BOM();
            vmMkt_BOM.User_User = new User_User();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();

            vmMkt_BOM.User_User = vMUser.User_User;
            int iD = 0, bOMid = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("=")), out iD);
            id = id.Replace(iD.ToString() + "=", "");
            int.TryParse(id, out bOMid);

            vmMkt_BOM.VmMkt_BOMSlave = new VmMkt_BOMSlave();
            vmMkt_BOM.VmMkt_BOMSlave.Mkt_BOMSlave = new Mkt_BOMSlave();
            vmMkt_BOM.VmMkt_BOMSlave.Mkt_BOMSlave.Mkt_BOMFK = bOMid;
            await Task.Run(() => vmMkt_BOM.VmMkt_BOMSlave.BOMSlaveFabricsPurchaseAndProcessing(iD));
            await Task.Run(() => vmMkt_BOM.VmMkt_BOMSlave.BOMSlaveFabricsStepJobs(iD));



            return View(vmMkt_BOM);
        }

        [HttpGet]
        public ActionResult VmMkt_BOMStepJobsCreate(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int iD = 0, bOMid = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("=")), out iD);
            id = id.Replace(iD.ToString() + "=", "");
            int.TryParse(id, out bOMid);
            string bomStr = bOMid.ToString();
            string bomSlaveStr = iD.ToString();
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;
            VmMkt_BOM VmMkt_BOM = new VmMkt_BOM();
            VmMkt_BOM.SelectSingle(bomStr);
            VmMkt_BOM.Mkt_BOM = new Mkt_BOM();
            VmMkt_BOM.Mkt_BOM.UpdateDate = DateTime.Now;

            ViewModels.Marketing.VmMkt_BOMSlave vmMkt_BOMSlave = new VmMkt_BOMSlave();
            vmMkt_BOMSlave.SelectSingle(bomSlaveStr);
            int sId = 0;
            VmControllerHelper = new VmControllerHelper();
            string s = VmControllerHelper.Decrypt(bomStr);
            Int32.TryParse(s, out sId);
            //vmMkt_BOMSlave.Mkt_BOM = VmMkt_BOM.Mkt_BOM;
            vmMkt_BOMSlave.Mkt_BOMSlave = vmMkt_BOMSlave.Mkt_BOMSlave;
            vmMkt_BOMSlave.Mkt_BOMSlave.RequiredQuantity = 0;
            vmMkt_BOMSlave.Mkt_BOMStepJobs = new Mkt_BOMStepJobs();
            vmMkt_BOMSlave.Mkt_BOMStepJobs.Mkt_BOMFK = bOMid;
            vmMkt_BOMSlave.Mkt_BOMStepJobs.Mkt_BOMSlaveFK = iD;

            return View(vmMkt_BOMSlave);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMStepJobsCreate(VmMkt_BOMSlave VmMkt_BOMSlave)
        {

            VmControllerHelper vmControllerHelper = new VmControllerHelper();

            VmMkt_BOMSlave.Mkt_BOMStepJobs.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            //if (ModelState.IsValid)
            //{          

            if (VmMkt_BOMSlave.Mkt_BOMStepJobs.RequiredQuantity == 0 || VmMkt_BOMSlave.Mkt_BOMStepJobs.RequiredQuantity == null)
            {
                VmMkt_BOMSlave.Mkt_BOMStepJobs.RequiredQuantity = VmMkt_BOMSlave.Mkt_BOMStepJobs.Consumption * VmMkt_BOMSlave.Mkt_BOM.QPack * VmMkt_BOMSlave.Mkt_BOM.Quantity / 12;
            }
            await Task.Run(() => VmMkt_BOMSlave.AddStepJobs(0));
            int? bomFk = VmMkt_BOMSlave.Mkt_BOMStepJobs.Mkt_BOMFK;
            await Task.Run(() => VmMkt_BOMSlave.EditBOMUpdateDate(VmMkt_BOMSlave.Mkt_BOMStepJobs.FirstCreatedBy, bomFk.Value));
            string boMFK = bomFk.ToString();

            return RedirectToAction("BOMSlaveFabricsPurchaseAndProcessing/" + VmMkt_BOMSlave.Mkt_BOMStepJobs.Mkt_BOMSlaveFK + "=" + VmMkt_BOMSlave.Mkt_BOMStepJobs.Mkt_BOMFK);

        }



        [HttpGet]
        public async Task<ActionResult> VmMkt_BOMStepJobsEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_BOMSlave = new VmMkt_BOMSlave();

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;


            //////////////////////////////////////////
            int editId = 0;
            Int32.TryParse(id, out editId);

            await Task.Run(() => VmMkt_BOMSlave.SelectSingleStepJobs(editId));

            if (VmMkt_BOMSlave.Mkt_BOMSlave == null)
            {
                return HttpNotFound();
            }
            VmControllerHelper = new VmControllerHelper();
            //no chg
            VmForDropDown.DDownData = VmForDropDown.GetRaw_ItemDropDownForEdit(VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Item = sl2;
            int i = VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK;
            string s = i.ToString();
            string ItemFK = VmControllerHelper.Encrypt(s);

            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryDropDownForEdit(ItemFK);
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryDropDownForEdit(VmMkt_BOMSlave.Mkt_BOMSlave.Raw_ItemFK);
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;


            return View(VmMkt_BOMSlave);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_BOMStepJobsEdit(VmMkt_BOMSlave x)
        {


            //if (ModelState.IsValid)
            //{
            await Task.Run(() => x.EditStepJobs(UserID));

            return RedirectToAction("BOMSlaveFabricsPurchaseAndProcessing/" + x.Mkt_BOMStepJobs.Mkt_BOMSlaveFK + "=" + x.Mkt_BOMStepJobs.Mkt_BOMFK);
            //}
            //return RedirectToAction("BOMSlaveFabricsPurchaseAndProcessing/" + x.Mkt_BOMStepJobs.Mkt_BOMSlaveFK + "=" + x.Mkt_BOMStepJobs.Mkt_BOMFK);
        }

        public ActionResult VmMkt_BOMStepJobsDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");


            Mkt_BOMStepJobs b = new Mkt_BOMStepJobs();
            b.ID = deleteId;
            b.Delete(0);
            return RedirectToAction("BOMSlaveFabricsPurchaseAndProcessing/" + id);
        }

        public async Task<ActionResult> LockedStepJobs(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_BOMSlave = new VmMkt_BOMSlave();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_BOMSlave.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_BOMSlave.GetStepJobsByID(id.Value));
            return View(VmMkt_BOMSlave);
        }

        [HttpPost]
        public async Task<ActionResult> LockedStepJobs(VmMkt_BOMSlave a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            a.User_User = vMUser.User_User;

            await Task.Run(() => a.EditStepJobs(0));

            return RedirectToAction("BOMSlaveFabricsPurchaseAndProcessing/" + a.Mkt_BOMStepJobs.Mkt_BOMSlaveFK + "=" + a.Mkt_BOMStepJobs.Mkt_BOMFK);
        }


        //...............Style Delivery Schedule ............................//

        [HttpGet]
        public async Task<ActionResult> VmMkt_OrderDeliveryScheduleCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule = new Mkt_OrderDeliverySchedule();
            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Date = DateTime.Now;
            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk = id;
            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity = 0;


            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Country = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetStyleByOrderDropDown(id);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Style = sl2;

            var bom = db.Mkt_BOM.Where(x => x.Common_TheOrderFk == id && x.Active == true).FirstOrDefault();

            //VmMkt_OrderDeliverySchedule.GetOrderWiseTotalQuantity(id);
            int totalQuantiy = bom.QPack * bom.Quantity * bom.SetQuantity; // VmMkt_OrderDeliverySchedule.GetOrderWiseTotalQuantity(id);//VmMkt_OrderDeliverySchedule.DataList1.Sum(x => x.Mkt_BOM.QPack * x.Mkt_BOM.Quantity);
            VmMkt_OrderDeliverySchedule.GetDeliveryQuantity(id);

            decimal qty = VmMkt_OrderDeliverySchedule.DataList.Sum(x => x.Mkt_OrderDeliverySchedule.Quantity);

            decimal currentQty = totalQuantiy - qty;
            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity = currentQty;
            VmMkt_OrderDeliverySchedule.SetQuantity = bom.SetQuantity;
            return View(VmMkt_OrderDeliverySchedule);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_OrderDeliveryScheduleCreate(VmMkt_OrderDeliverySchedule VmMkt_OrderDeliverySchedule)
        {

            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmMkt_OrderDeliverySchedule.GetSizeList(VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.ColorSize));

                if (string.IsNullOrEmpty(VmMkt_OrderDeliverySchedule.Result))
                {
                    if (VmMkt_OrderDeliverySchedule.ColorList.Any())
                    {
                        int AvailableQty = (int)VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity;
                        int PortTotalShipmentQty = VmMkt_OrderDeliverySchedule.ColorList.Sum(a => a.ColorQty);
                        //decimal exShipmentQty =
                        //    db.Mkt_OrderDeliverySchedule.Where(
                        //            x =>
                        //                x.Active == true &&
                        //                x.Common_TheOrderFk ==
                        //                VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk).Select(l => l.Quantity)
                        //                .DefaultIfEmpty(0)
                        //        .Sum();

                        //decimal Total = PortTotalShipmentQty + exShipmentQty;


                        int NextAvailableQty = AvailableQty - PortTotalShipmentQty;

                        if (NextAvailableQty >= 0)
                        {
                            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity = PortTotalShipmentQty;
                            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.ColorSize = VmMkt_OrderDeliverySchedule.PortColor;
                            await Task.Run(() => ID = VmMkt_OrderDeliverySchedule.Add(UserID));

                            foreach (var v in VmMkt_OrderDeliverySchedule.ColorList)
                            {
                                if (v.QuantityList.Any())
                                {
                                    foreach (var size in v.QuantityList)
                                    {
                                        VmMkt_OrderColorAndSizeRatio = new VmMkt_OrderColorAndSizeRatio();
                                        VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio = new Mkt_OrderColorAndSizeRatio();
                                        VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Color = v.ColorName;
                                        VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Size = size.Size;
                                        VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Quantity = size.Quantity;
                                        VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Ratio = size.Ratio;

                                        VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.OrderDeliveryScheduleFk = ID;
                                        VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Common_TheOrderFk = VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk;
                                        VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Mkt_BOMFk = VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Mkt_BOMFk;
                                        await Task.Run(() => VmMkt_OrderColorAndSizeRatio.Add(UserID));
                                    }
                                }
                            }

                            if (VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk != null)
                            {
                                return RedirectToAction("Mkt_OrderSlaveBOM/" + VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk, "Common");
                            }
                        }
                        else
                        {
                            //VmMkt_OrderDeliverySchedule.ErrorMessage = "Delivery Schedule Quantity Must be greater then Order";
                            return RedirectToAction("Mkt_OrderSlaveBOM/" + VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk, "Common", new { error = true });
                        }

                    }

                }
                else
                {
                    return RedirectToAction("Mkt_OrderSlaveBOM/" + VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk, "Common");
                }
            }

            return RedirectToAction("Mkt_OrderSlaveBOM", "Common");
        }

        [HttpGet]
        public async Task<ActionResult> VmMkt_OrderDeliveryScheduleEdit(string id)
        {

            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
            //VmMkt_StyleDeliverySchedule.Mkt_StyleDeliverySchedule.Common_TheOrderFk = id;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Country = sl1;

            string editID = id.Substring(0, id.LastIndexOf("ʁ"));
            string rID = id.Split('ʁ').Last();


            int IDforEdit = Convert.ToInt32(editID);
            int id1 = Convert.ToInt32(rID);

            VmForDropDown.DDownData = VmForDropDown.GetStyleByOrderDropDown(id1);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Style = sl2;

            VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
            await Task.Run(() => VmMkt_OrderDeliverySchedule.SelectSingle(IDforEdit));

            if (VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule == null)
            {
                return HttpNotFound();
            }

            return View(VmMkt_OrderDeliverySchedule);


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_OrderDeliveryScheduleEdit(VmMkt_OrderDeliverySchedule VmMkt_OrderDeliverySchedule)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            if (ModelState.IsValid)
            {
                //int id = Convert.ToInt32(VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Mkt_BOMFk);
                //await Task.Run(() => VmMkt_OrderDeliverySchedule.GetOrderWiseTotalQuantity(id));
                //int totalQuantiy = VmMkt_OrderDeliverySchedule.DataList1.Sum(x => x.Mkt_BOM.QPack * x.Mkt_BOM.Quantity);

                //await Task.Run(() => VmMkt_OrderDeliverySchedule.GetDeliveryQuantity(id));
                //decimal qty = VmMkt_OrderDeliverySchedule.DataList.Where(a => a.Mkt_OrderDeliverySchedule.ID != VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.ID).Sum(x => x.Mkt_OrderDeliverySchedule.Quantity);
                //decimal currentQty = totalQuantiy - qty;

                //if (currentQty >= Convert.ToDecimal(VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity))
                //{
                //    await Task.Run(() => VmMkt_OrderDeliverySchedule.Edit(VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.LastEditeddBy));
                //}
                await Task.Run(() => VmMkt_OrderDeliverySchedule.Edit(VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.LastEditeddBy));
            }
            return RedirectToAction("Mkt_OrderSlaveBOM/" + VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk, "Common");
        }

        [HttpGet]
        public async Task<ActionResult> VmMkt_OrderDeliveryScheduleDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            string deleteID = id.Substring(0, id.LastIndexOf("ʁ"));
            string rID = id.Split('ʁ').Last();


            int IDforDelete = Convert.ToInt32(deleteID);
            int id1 = Convert.ToInt32(rID);

            Mkt_OrderDeliverySchedule b = new Mkt_OrderDeliverySchedule();
            b.ID = IDforDelete;
            b.Delete(UserID);
            VmMkt_OrderColorAndSizeRatio = new VmMkt_OrderColorAndSizeRatio();
            await Task.Run(() => VmMkt_OrderColorAndSizeRatio.BulkDelete(UserID, IDforDelete));

            return RedirectToAction("Mkt_OrderSlaveBOM/" + id1, "Common");
        }

        public async Task<ActionResult> GetDeliveryScheduleQuantity(int id)
        {
            VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
            await Task.Run(() => VmMkt_OrderDeliverySchedule.GetOrderWiseTotalQuantity(id));

            int totalQuantiy = VmMkt_OrderDeliverySchedule.DataList1.Sum(x => x.Mkt_BOM.QPack * x.Mkt_BOM.Quantity);

            await Task.Run(() => VmMkt_OrderDeliverySchedule.GetDeliveryQuantity(id));

            decimal qty = VmMkt_OrderDeliverySchedule.DataList.Sum(x => x.Mkt_OrderDeliverySchedule.Quantity);

            decimal currentQty = totalQuantiy - qty;

            return Json(new { success = true, qty = currentQty }, JsonRequestBehavior.AllowGet);
        }


        //............... Order color and size ratio ............................//
        #region Order color and size ratio

        [HttpGet]
        public ActionResult VmMkt_OrderColorAndSizeRatioCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_OrderColorAndSizeRatio = new VmMkt_OrderColorAndSizeRatio();
            VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio = new Mkt_OrderColorAndSizeRatio();
            VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Common_TheOrderFk = id;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStyleByOrderDropDown(id);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Style = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetOrderDeliverySchedule(id);
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.OrderDeliverySchedule = sl3;
            return View(VmMkt_OrderColorAndSizeRatio);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_OrderColorAndSizeRatioCreate(VmMkt_OrderColorAndSizeRatio VmMkt_OrderColorAndSizeRatio)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //if (ModelState.IsValid)
            //{
            VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
            await Task.Run(() => VmMkt_OrderDeliverySchedule.GetSizeList(VmMkt_OrderColorAndSizeRatio.ColorSize));

            if (string.IsNullOrEmpty(VmMkt_OrderDeliverySchedule.Result))
            {
                if (VmMkt_OrderDeliverySchedule.ColorList.Any())
                {
                    VmMkt_OrderDeliverySchedule.SelectSingle(VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.OrderDeliveryScheduleFk.Value);
                    //int AvailableQty = (int)VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity;


                    //int NextAvailableQty = AvailableQty - PortTotalShipmentQty;

                    //if (NextAvailableQty >= PortTotalShipmentQty || NextAvailableQty == 0)
                    //{
                    // VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity = PortTotalShipmentQty;
                    VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity = VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity + VmMkt_OrderDeliverySchedule.ColorList.Sum(a => a.ColorQty);
                    VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.ColorSize = VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.ColorSize + "/" + VmMkt_OrderDeliverySchedule.PortColor;
                    await Task.Run(() => VmMkt_OrderDeliverySchedule.Edit(UserID));

                    foreach (var v in VmMkt_OrderDeliverySchedule.ColorList)
                    {
                        if (v.QuantityList.Any())
                        {
                            foreach (var size in v.QuantityList)
                            {
                                //VmMkt_OrderColorAndSizeRatio = new VmMkt_OrderColorAndSizeRatio();
                                //VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio = new Mkt_OrderColorAndSizeRatio();
                                VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Color = v.ColorName;
                                VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Size = size.Size;
                                VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Quantity = size.Quantity;
                                VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Ratio = size.Ratio;

                                VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.OrderDeliveryScheduleFk = VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.OrderDeliveryScheduleFk;
                                VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Common_TheOrderFk = VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Common_TheOrderFk;
                                VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Mkt_BOMFk = VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Mkt_BOMFk;
                                await Task.Run(() => VmMkt_OrderColorAndSizeRatio.Add(UserID));
                            }
                        }
                    }

                    if (VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk != null)
                    {
                        return RedirectToAction("Mkt_OrderSlaveBOM/" + VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk, "Common");
                    }
                    //}
                    //else
                    //{
                    //    return RedirectToAction("Mkt_OrderSlaveBOM/" + VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk, "Common");
                    //}

                }

            }
            else
            {
                return RedirectToAction("Mkt_OrderSlaveBOM/" + VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk, "Common");
            }
            //}

            return RedirectToAction("Mkt_OrderSlaveBOM/" + VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Common_TheOrderFk, "Common");
        }

        [HttpGet]
        public async Task<ActionResult> VmMkt_OrderColorAndSizeRatioEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_OrderColorAndSizeRatio = new VmMkt_OrderColorAndSizeRatio();

            string editID = id.Substring(0, id.LastIndexOf("ʁ"));
            string rID = id.Split('ʁ').Last();

            int IDforEdit = Convert.ToInt32(editID);
            int id1 = Convert.ToInt32(rID);

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStyleByOrderDropDown(id1);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Style = sl2;

            VmMkt_OrderColorAndSizeRatio = new VmMkt_OrderColorAndSizeRatio();

            await Task.Run(() => VmMkt_OrderColorAndSizeRatio.SelectSingle(IDforEdit));
            VmMkt_OrderColorAndSizeRatio.Quantity = VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Quantity;
            if (VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio == null)
            {
                return HttpNotFound();
            }

            VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
            VmMkt_OrderDeliverySchedule.GetOrderWiseTotalQuantity(id1);
            int totalQuantiy = VmMkt_OrderDeliverySchedule.DataList1.Sum(x => x.Mkt_BOM.QPack * x.Mkt_BOM.Quantity);

            VmMkt_OrderDeliverySchedule.GetDeliveryQuantity(id1);

            decimal? qty = VmMkt_OrderDeliverySchedule.DataList.Sum(x => x.Mkt_OrderDeliverySchedule.Quantity);

            decimal? currentQty = totalQuantiy - qty;
            VmMkt_OrderColorAndSizeRatio.AvailableQty = currentQty;
            return View(VmMkt_OrderColorAndSizeRatio);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_OrderColorAndSizeRatioEdit(VmMkt_OrderColorAndSizeRatio VmMkt_OrderColorAndSizeRatio)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            if (ModelState.IsValid)
            {
                decimal BeforeUpdateQty = (decimal)VmMkt_OrderColorAndSizeRatio.Quantity;
                decimal LatestQty = VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Quantity - BeforeUpdateQty;
                decimal UpdateQty = LatestQty > 0 ? (decimal)VmMkt_OrderColorAndSizeRatio.AvailableQty - LatestQty : (decimal)VmMkt_OrderColorAndSizeRatio.AvailableQty + LatestQty;
                if (UpdateQty >= 0)
                {
                    await Task.Run(() => VmMkt_OrderColorAndSizeRatio.Edit(VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.LastEditeddBy));
                    await Task.Run(() => VmMkt_OrderColorAndSizeRatio.GetColorSummery((int)VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.OrderDeliveryScheduleFk));

                    VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
                    await Task.Run(() => VmMkt_OrderDeliverySchedule.SelectSingle((int)VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.OrderDeliveryScheduleFk));
                    VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity += LatestQty;
                    VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.ColorSize = VmMkt_OrderColorAndSizeRatio.ShipmentColor;
                    await Task.Run(() => VmMkt_OrderDeliverySchedule.Edit(VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.LastEditeddBy));
                }
            }
            return RedirectToAction("Mkt_OrderSlaveBOM/" + VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Common_TheOrderFk, "Common");
        }

        [HttpGet]
        public async Task<ActionResult> VmMkt_OrderColorAndSizeRatioDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            string deleteID = id.Substring(0, id.LastIndexOf("ʁ"));
            string rID = id.Split('ʁ').Last();

            int IDforDelete = Convert.ToInt32(deleteID);
            int id1 = Convert.ToInt32(rID);

            VmMkt_OrderColorAndSizeRatio = new VmMkt_OrderColorAndSizeRatio();
            await Task.Run(() => VmMkt_OrderColorAndSizeRatio.SelectSingle(IDforDelete));

            Mkt_OrderColorAndSizeRatio b = new Mkt_OrderColorAndSizeRatio();
            b.ID = IDforDelete;
            b.Delete(UserID);

            await Task.Run(() => VmMkt_OrderColorAndSizeRatio.GetColorSummery((int)VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.OrderDeliveryScheduleFk));

            VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
            await Task.Run(() => VmMkt_OrderDeliverySchedule.SelectSingle((int)VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.OrderDeliveryScheduleFk));
            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.Quantity -= VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.Quantity;
            VmMkt_OrderDeliverySchedule.Mkt_OrderDeliverySchedule.ColorSize = VmMkt_OrderColorAndSizeRatio.ShipmentColor;
            await Task.Run(() => VmMkt_OrderDeliverySchedule.Edit(VmMkt_OrderColorAndSizeRatio.Mkt_OrderColorAndSizeRatio.LastEditeddBy));

            return RedirectToAction("Mkt_OrderSlaveBOM/" + id1, "Common");
        }

        public async Task<ActionResult> GetOrderColorAndSizeQuantity(int id)
        {
            VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
            await Task.Run(() => VmMkt_OrderDeliverySchedule.GetOrderWiseTotalQuantity(id));

            int totalQuantiy = VmMkt_OrderDeliverySchedule.DataList1.Sum(x => x.Mkt_BOM.QPack * x.Mkt_BOM.Quantity);

            VmMkt_OrderColorAndSizeRatio = new VmMkt_OrderColorAndSizeRatio();
            await Task.Run(() => VmMkt_OrderColorAndSizeRatio.GetColorAndSizeQuantity(id));

            decimal qty = VmMkt_OrderColorAndSizeRatio.DataList.Sum(x => x.Mkt_OrderColorAndSizeRatio.Quantity);

            decimal currentQty = totalQuantiy - qty;

            return Json(new { success = true, qty = currentQty }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public async Task<ActionResult> StyleAddedMLC()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            bool flag = false;
            vmMkt_BOM = new VmMkt_BOM();

            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            vmMkt_BOM.User_User = vMUser.User_User;


            await Task.Run(() => vmMkt_BOM.InitialStyleMLCDataLoad());
            return View(vmMkt_BOM);
        }

        public async Task<ActionResult> StyleWithoutAddedMLC()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            bool flag = false;
            vmMkt_BOM = new VmMkt_BOM();

            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            vmMkt_BOM.User_User = vMUser.User_User;


            await Task.Run(() => vmMkt_BOM.InitialStyleWithoutMLCDataLoad());
            return View(vmMkt_BOM);
        }

        public async Task<ActionResult> MktPoByStyle(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            bool flag = false;
            vmMkt_BOM = new VmMkt_BOM();

            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            vmMkt_BOM.User_User = vMUser.User_User;


            await Task.Run(() => vmMkt_BOM.GetPoByStyle(id));
            return View(vmMkt_BOM);
        }

        //.....................SummaryReport Details...........................//
        #region SummaryReportLinkAction

        [HttpGet]
        public async Task<ActionResult> BOMSummeryDetails()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            vmMkt_BOM = new VmMkt_BOM();
            vmMkt_BOM.Header = "Bill Of Materials";
            vmMkt_BOM.ButtonName = "Close";

            await Task.Run(() => vmMkt_BOM.InitialPeriodicDataLoad());
            return View("VmMkt_BOMIndex", vmMkt_BOM);
        }

        [HttpGet]
        public async Task<ActionResult> POSummaryDetails()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.PeriodicGetPO());

            return View("PurchaseOrders", VmMkt_PO);
        }

        #endregion



        //public async Task<ActionResult> VmMkt_BuyerIndex()
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //    VmMkt_Buyer = new VmMkt_Buyer();
        //    await Task.Run(() => VmMkt_Buyer.InitialDataLoad());
        //    return View(VmMkt_Buyer);
        //}
        //// To Insert Buyer Information. -Get
        //public ActionResult VmMkt_BuyerCreate()
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //    VmForDropDown = new VmForDropDown();
        //    VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
        //    SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
        //    ViewBag.Country = sl2;

        //    VmForDropDown.DDownData = VmForDropDown.GetAccountBuyerHeadForDropDown();
        //    SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
        //    ViewBag.Chart2Head = sl3;

        //    return View();
        //}
        //// To Insert Buyer Information. -Post
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VmMkt_BuyerCreate(VmMkt_Buyer VmMkt_Buyer)
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }

        //    // chart of account integration
        //    try
        //    {
        //        VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
        //        {
        //            UserId = UserID,
        //            AccountName = VmMkt_Buyer.Mkt_Buyer.Name,
        //            Chart2Id = VmMkt_Buyer.Acc_Chart2IDFk
        //        };
        //        VmMkt_Buyer.Mkt_Buyer.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.AddFromIntegratedModule());
        //    }
        //    catch (Exception e)
        //    {
        //        VmMkt_Buyer.Mkt_Buyer.Acc_AcNameFk = 0;
        //    }

        //    if (Request.Cookies["OssRomo"] != null)
        //    {
        //        string username = Request.Cookies["OssRomo"].Values["UserName"];
        //    }
        //    if (ModelState.IsValid)
        //    {
        //        await Task.Run(() => VmMkt_Buyer.Add(UserID));
        //        return RedirectToAction("VmMkt_BuyerIndex");
        //    }

        //    return RedirectToAction("VmMkt_BuyerIndex");
        //}
        //// To update Buyer Information. -Get
        //[HttpGet]
        //public async Task<ActionResult> VmMkt_BuyerEdit(string id)
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }

        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    VmMkt_Buyer = new VmMkt_Buyer();
        //    await Task.Run(() => VmMkt_Buyer.SelectSingle(id));
        //    VmForDropDown = new VmForDropDown();
        //    VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
        //    SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
        //    ViewBag.Country = sl2;


        //    VmForDropDown.DDownData = VmForDropDown.GetAccountBuyerHeadForDropDown();
        //    SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
        //    ViewBag.Chart2Head = sl3;

        //    VmMkt_Buyer = new VmMkt_Buyer();
        //    await Task.Run(() => VmMkt_Buyer.SelectSingle(id));

        //    var accAcName = db.Acc_AcName.Find(VmMkt_Buyer.Mkt_Buyer.Acc_AcNameFk);
        //    if (accAcName?.Acc_Chart2FK != null) VmMkt_Buyer.Acc_Chart2IDFk = (int)accAcName.Acc_Chart2FK;

        //    if (VmMkt_Buyer.Mkt_Buyer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(VmMkt_Buyer);
        //}
        //// To update Buyer Information. -Post
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VmMkt_BuyerEdit(VmMkt_Buyer VmMkt_Buyer)
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //    //VmControllerHelper vmControllerHelper = new VmControllerHelper();
        //    //mkt_Buyer.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;

        //    // chart of account integration
        //    try
        //    {
        //        VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
        //        {
        //            UserId = UserID,
        //            AccountName = VmMkt_Buyer.Mkt_Buyer.Name,
        //            Chart2Id = VmMkt_Buyer.Acc_Chart2IDFk,
        //            ExistAccId = VmMkt_Buyer.Mkt_Buyer.Acc_AcNameFk
        //        };
        //        VmMkt_Buyer.Mkt_Buyer.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.EditFromIntegratedModule());
        //    }
        //    catch (Exception e)
        //    {
        //        VmMkt_Buyer.Mkt_Buyer.Acc_AcNameFk = 0;
        //    }


        //    if (ModelState.IsValid)
        //    {
        //        await Task.Run(() => VmMkt_Buyer.Edit(0));
        //        return RedirectToAction("VmMkt_BuyerIndex");
        //    }
        //    return View(VmMkt_Buyer);
        //}
        //// To remove Buyer Information. -Get
        //public ActionResult VmMkt_BuyerDelete(string id)
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }

        //    int iD = 0;
        //    VmControllerHelper = new VmControllerHelper();
        //    id = VmControllerHelper.Decrypt(id);
        //    Int32.TryParse(id, out iD);
        //    Mkt_Buyer b = new Mkt_Buyer();
        //    b.ID = iD;
        //    b.Delete(0);
        //    return RedirectToAction("VmMkt_BuyerIndex");
        //}

    }
}