﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using Oss.Romo.Models;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.Commercial;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Home;
using Oss.Romo.ViewModels.MisDashboard;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.ViewModels.Store;
using Oss.Romo.ViewModels.User;

namespace Oss.Romo.Controllers
{
    public class MisDashboardController : Controller
    {
        private xOssContext db;
        private VmCommercial_Report VmCommercial_Report;
        private VmMis_AdminBoard VmMis_AdminBoard;
        private VmForDropDown VmForDropDown;
        private VmAcc_AcName VmAcc_AcName;
        private VmAcc_Transaction VmAcc_Transaction;
        private VmAcc_IntegrationJournal VmAcc_IntegrationJournal;
        private VmAcc_ChequeIntegration VmAcc_ChequeIntegration;
        // GET: MisDashboard
        public MisDashboardController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }

        public async Task<ActionResult> VmMis_DashboardIndex()
        {
            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.GetFactoryOverview(DateTime.Now));
            ViewBag.FactoryOverview = VmMis_AdminBoard.FactoryOverview;

            await Task.Run(() => VmMis_AdminBoard.GetBusinessCashFlow(DateTime.Now));
            ViewBag.BusinessCashFlow = VmMis_AdminBoard.BusinessCashFlow;

            VmMis_AdminBoard.GetCollectionDueReceivableAmountLast12Months(DateTime.Now);
            ViewBag.CollectionDueReceivableAmount12Months = VmMis_AdminBoard.CollectionDueReceivableAmount;

            await Task.Run(() => VmMis_AdminBoard.GetSummaryReport());
            VmAcc_SupplierPH VmAcc_SupplierPH = new VmAcc_SupplierPH();
            SumofPayableDueSupplier supplier = await Task.Run(() => VmAcc_SupplierPH.SumOfDueAndPayableForSupplier());
            ViewBag.SupplierData = supplier;
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMkt_BuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BuyerName = sl1;

            await Task.Run(() => VmMis_AdminBoard.GetAllOrder(0));
            await Task.Run(() => VmMis_AdminBoard.GetPlan_OrderStepProgres());



            return View(VmMis_AdminBoard);
        }

        [HttpPost]
        public async Task<ActionResult> VmMis_DashboardIndex(VmMis_AdminBoard x)
        {
            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.GetFactoryOverview(DateTime.Now));
            ViewBag.FactoryOverview = VmMis_AdminBoard.FactoryOverview;

            await Task.Run(() => VmMis_AdminBoard.GetBusinessCashFlow(DateTime.Now));
            ViewBag.BusinessCashFlow = VmMis_AdminBoard.BusinessCashFlow;

            VmMis_AdminBoard.GetCollectionDueReceivableAmountLast12Months(DateTime.Now);
            ViewBag.CollectionDueReceivableAmount12Months = VmMis_AdminBoard.CollectionDueReceivableAmount;

            await Task.Run(() => VmMis_AdminBoard.GetSummaryReport());
            VmAcc_SupplierPH VmAcc_SupplierPH = new VmAcc_SupplierPH();
            SumofPayableDueSupplier supplier = await Task.Run(() => VmAcc_SupplierPH.SumOfDueAndPayableForSupplier());
            ViewBag.SupplierData = supplier;

            await Task.Run(() => VmMis_AdminBoard.GetAllOrder(0));
            await Task.Run(() => VmMis_AdminBoard.GetPlan_OrderStepProgres());


            int bid = x.Buyer;
            await Task.Run(() => VmMis_AdminBoard.GetAllOrder(x.Buyer));
            await Task.Run(() => VmMis_AdminBoard.GetPlan_OrderStepProgres());



            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMkt_BuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BuyerName = sl1;

            return View(VmMis_AdminBoard);
        }

        public async Task<ActionResult> VmMis_Dashboard2Index()
        {
            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.SupplierPayble());
            ViewBag.SupplierList = VmMis_AdminBoard.SupplierInfoChartList;


            await Task.Run(() => VmMis_AdminBoard.BuyerChart());
            ViewBag.BuyerList = VmMis_AdminBoard.BuyerInfoChartList;

            await Task.Run(() => VmMis_AdminBoard.GetSummaryReport());
            VmAcc_SupplierPH VmAcc_SupplierPH = new VmAcc_SupplierPH();
            SumofPayableDueSupplier supplier = await Task.Run(() => VmAcc_SupplierPH.SumOfDueAndPayableForSupplier());
            ViewBag.SupplierData = supplier;

            VmMis_AdminBoard.VmAcc_AcName = new VmAcc_AcName();

            await Task.Run(() => VmMis_AdminBoard.VmAcc_AcName.AccountTypeWiseInventory());

            VmMis_AdminBoard.VmStoreInventory = new VmStoreInventory();

            await Task.Run(() => VmMis_AdminBoard.VmStoreInventory.CategoryWiseInventory());

            return View(VmMis_AdminBoard);
        }
        public async Task<ActionResult> AtAGlance()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.GetSummaryReport());
            VmAcc_SupplierPH VmAcc_SupplierPH = new VmAcc_SupplierPH();
            SumofPayableDueSupplier supplier = await Task.Run(() => VmAcc_SupplierPH.SumOfDueAndPayableForSupplier());
            ViewBag.SupplierData = supplier;


            return View(VmMis_AdminBoard);
        }

        public async Task<ActionResult> BuyerCreditors()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMis_AdminBoard = new VmMis_AdminBoard();

            await Task.Run(() => VmMis_AdminBoard.BuyerChart());
            ViewBag.BuyerList = VmMis_AdminBoard.BuyerInfoChartList;

            return View(VmMis_AdminBoard);
        }

        public async Task<ActionResult> BuyerDebtors()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMis_AdminBoard = new VmMis_AdminBoard();

            await Task.Run(() => VmMis_AdminBoard.BuyerChartDebtors());
            ViewBag.BuyerList = VmMis_AdminBoard.BuyerInfoChartList;


            return View(VmMis_AdminBoard);
        }


        public async Task<ActionResult> CollectionStatus()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMis_AdminBoard = new VmMis_AdminBoard();
            VmMis_AdminBoard.GetCollectionDueReceivableAmountLast12Months(DateTime.Now);
            ViewBag.CollectionDueReceivableAmount12Months = VmMis_AdminBoard.CollectionDueReceivableAmount;
            return View(VmMis_AdminBoard);
        }

        public async Task<ActionResult> SupplierPayable()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.SupplierPayble());
            ViewBag.SupplierList = VmMis_AdminBoard.SupplierInfoChartList;
            return View(VmMis_AdminBoard);
        }
        public async Task<ActionResult> CategoryStock()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_SupplierPH VmAcc_SupplierPH = new VmAcc_SupplierPH();
            SumofPayableDueSupplier s = await Task.Run(() => VmAcc_SupplierPH.SumOfDueAndPayableForSupplier());
            ViewBag.SupplierData = s;

            VmMis_AdminBoard VmMis_AdminBoard = new VmMis_AdminBoard();
            VmMis_AdminBoard.GetCollectionDueReceivableAmountLastMonths(DateTime.Now);
            ViewBag.CollectionDueReceivableAmount = VmMis_AdminBoard.CollectionDueReceivableAmount;


            VmMis_AdminBoard = new VmMis_AdminBoard();
            VmMis_AdminBoard.GetCollectionDueReceivableAmountLast12Months(DateTime.Now);
            ViewBag.CollectionDueReceivableAmount12Months = VmMis_AdminBoard.CollectionDueReceivableAmount;

            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.BuyerChart());
            ViewBag.BuyerList = VmMis_AdminBoard.BuyerInfoChartList;


            VmStoreInventory VmStoreInventory = new VmStoreInventory();
            var data = await Task.Run(() => VmStoreInventory.CategoryWiseStock());
            ViewBag.CategoryStock = data;

            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.GetSummaryReport());
            await Task.Run(() => VmMis_AdminBoard.GetRealisedAmountLoad());
            await Task.Run(() => VmMis_AdminBoard.GetTotalLcTtAmount());
            //await Task.Run(() => VmMis_AdminBoard.GetTotalLcBtbLcPoAmount());
            return View(VmMis_AdminBoard);
        }
        public async Task<ActionResult> AccessAllApplication()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMis_AdminBoard VmMis_AdminBoard = new VmMis_AdminBoard();

            return View(VmMis_AdminBoard);
        }


        public async Task<ActionResult> BusinessCashFlow()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.GetBusinessCashFlow(DateTime.Now));
            ViewBag.BusinessCashFlow = VmMis_AdminBoard.BusinessCashFlow;
            return View(VmMis_AdminBoard);
        }
        public async Task<ActionResult> ProductionStatus()
        {


            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.GetAllOrder(0));
            await Task.Run(() => VmMis_AdminBoard.GetPlan_OrderStepProgres());


            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMkt_BuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BuyerName = sl1;

            return View(VmMis_AdminBoard);
        }
        [HttpPost]
        public async Task<ActionResult> ProductionStatus(VmMis_AdminBoard x)
        {


            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }



            VmMis_AdminBoard = new VmMis_AdminBoard();
            int bid = x.Buyer;
            await Task.Run(() => VmMis_AdminBoard.GetAllOrder(x.Buyer));
            await Task.Run(() => VmMis_AdminBoard.GetPlan_OrderStepProgres());



            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMkt_BuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BuyerName = sl1;
            return View(VmMis_AdminBoard);
        }
        public async Task<ActionResult> FactoryOverview()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.GetFactoryOverview(DateTime.Now));
            ViewBag.FactoryOverview = VmMis_AdminBoard.FactoryOverview;

            return View(VmMis_AdminBoard);
        }


        public async Task<ActionResult> TimeAndActionStatus()
        {

            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMis_AdminBoard = new VmMis_AdminBoard();
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommon_TheOrderForDropDown();
            ViewBag.Style = new SelectList(VmForDropDown.DDownData, "Value", "Text");



            return View(VmMis_AdminBoard);
        }
        public ActionResult LedgerAC(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;
            x.Acc_AcName = new Acc_AcName();
            x.Acc_AcName.ID = id;
            //x.GetTransactionOfAccount(1);

            return View(x);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LedgerAC(VmAccounting x)
        {
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/Ledger.rpt");
            cr.Load();
            x.GetLedgerAccountHead(x.Acc_AcName.ID, x.LedgerName);
            cr.SetDataSource(x.LedgerReport);
            ///
            //cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;

            Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        public ActionResult Notifier(string type)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmNotifier x = new VmNotifier();

            x.InitialDataLoad();

            x.VmCommercial_UD = new VmCommercial_UD();
            //x.VmCommercial_UD.InitialDataLoad();




            return View(x);
        }
        public async Task<ActionResult> VmAcc_AccNotifierFinalized(int id, string type)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //aproval method
            VmNotifierApproval vmNotifierApproval = new VmNotifierApproval();
            await Task.Run(() => vmNotifierApproval.NotifierFinalized(id, user));
            // then check if true finalized your transaction otherwise not finalized

            return RedirectToAction("Notifier", new { type = type });

        }
        public async Task<ActionResult> VmAcc_AccNotifierBtbApproved(int id, string type)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //aproval method
            VmNotifierApproval vmNotifierApproval = new VmNotifierApproval();
            await Task.Run(() => vmNotifierApproval.NotifierBtbApproved(id, user));

            return RedirectToAction("Notifier", new { type = type });
        }
        public async Task<ActionResult> VmAcc_AccNotifierDocApproved(int id, string type)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            //aproval method
            VmNotifierApproval vmNotifierApproval = new VmNotifierApproval();
            await Task.Run(() => vmNotifierApproval.NotifierDocApproved(id, user));

            return RedirectToAction("Notifier", new { type = type });
        }
        public async Task<ActionResult> VmAcc_AccNotifierHold(int id, string type)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            db = new xOssContext();
            Acc_Notifier accNotifier = db.Acc_Notifier.Find(id);
            accNotifier.ApprovedBy = user.User_User.ID;
            accNotifier.ApprovedDate = DateTime.Now;
            accNotifier.IsApproved = 2;

            this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
            {
                UserId = user.User_User.ID,
                Acc_Notifier = accNotifier
            };
            // update status
            await Task.Run(() => this.VmAcc_ChequeIntegration.EditCheque(user.User_User.ID));

            return RedirectToAction("Notifier", new { type = type });
        }
        public async Task<ActionResult> VmAcc_AccNotifierRequestAgain(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            db = new xOssContext();
            Acc_Notifier accNotifier = db.Acc_Notifier.Find(id);
            accNotifier.ApprovedBy = user.User_User.ID;
            accNotifier.ApprovedDate = DateTime.Now;
            accNotifier.IsApproved = 0;

            this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
            {
                UserId = user.User_User.ID,
                Acc_Notifier = accNotifier
            };
            // update status
            await Task.Run(() => this.VmAcc_ChequeIntegration.EditCheque(user.User_User.ID));

            return RedirectToAction("NotifierStatus", "Accounts");
        }
        public async Task<ActionResult> VmAcc_AccNotifierReject(int id, string type)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            db = new xOssContext();
            Acc_Notifier accNotifier = db.Acc_Notifier.Find(id);
            accNotifier.ApprovedBy = user.User_User.ID;
            accNotifier.ApprovedDate = DateTime.Now;
            accNotifier.IsApproved = 3;

            this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
            {
                UserId = user.User_User.ID,
                Acc_Notifier = accNotifier
            };
            // update status
            await Task.Run(() => this.VmAcc_ChequeIntegration.EditCheque(user.User_User.ID));

            if (accNotifier.TableIdFk == 14)
            {
                Acc_Transaction b = new Acc_Transaction();
                b.ID = accNotifier.RowIdFk;
                await Task.Run(() => b.Delete(user.User_User.ID));
            }
            else if (accNotifier.TableIdFk == 21)
            {
                Commercial_B2BPaymentInformation b = new Commercial_B2BPaymentInformation();
                b.ID = accNotifier.RowIdFk;
                await Task.Run(() => b.Delete(user.User_User.ID));
            }
            else if (accNotifier.TableIdFk == 17)
            {
                Commercial_B2bLC b = new Commercial_B2bLC();
                b.ID = accNotifier.RowIdFk;
                await Task.Run(() => b.Delete(user.User_User.ID));
            }

            return RedirectToAction("Notifier", new { type = type });
        }
        public async Task<ActionResult> VmAcc_AccNotifierSeen(int id, string type)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            db = new xOssContext();
            Acc_Notifier accNotifier = db.Acc_Notifier.Find(id);
            accNotifier.IsSeen = true;

            this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
            {
                UserId = user.User_User.ID,
                Acc_Notifier = accNotifier
            };
            // update status
            await Task.Run(() => this.VmAcc_ChequeIntegration.EditCheque(user.User_User.ID));

            return RedirectToAction("Notifier", new { type = type });
        }
    }
}