﻿using CrystalDecisions.CrystalReports.Engine;
using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Marketing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Oss.Romo.Models.Accounts;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Home;
using Oss.Romo.ViewModels.User;
using static Oss.Romo.ViewModels.Common.VmForDropDown;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.ViewModels.MisDashboard;

namespace Oss.Romo.Controllers
{
    [SoftwareAdmin]
    public class AccountsController : Controller
    {
        private xOssContext db = new xOssContext();
        VmAcc_SupplierPH VmAcc_SupplierPH;
        VmAcc_SupplierReceiveH VmAcc_SupplierReceiveH;
        VmControllerHelper VmControllerHelper;
        VmMkt_PO VmMkt_PO;
        VmAcc_POPH VmAcc_POPH;
        VmAcc_Chart1 VmAcc_Chart1;
        VmAcc_Chart2 VmAcc_Chart2;
        VmAcc_AcName VmAcc_AcName;
        VmAcc_Journal VmAcc_Journal;
        VmAcc_JournalSlave VmAcc_JournalSlave;
        VmCommon_TheOrder VmCommon_TheOrder;
        VmLedger VmLedger;
        VmAccounting VmAccounting;
        VmAcc_LCAlocation VmAcc_LCAlocation;
        VmForDropDown VmForDropDown;
        VmAcc_Expenses_History VmAcc_Expenses_History;
        VmAcc_Transaction VmAcc_Transaction;
        VmAcc_IntegrationJournal VmAcc_IntegrationJournal;
        VmAcc_AccExportRealisationExpense accTransactionExpense;
        VmAcc_ChequeIntegration VmAcc_ChequeIntegration;
        VmAcc_SupplierBill VmAcc_SupplierBill;
        VmAcc_SupplierBillSlave VmAcc_SupplierBillSalve;
        VmAcc_DollarRate VmAcc_DollarRate;
        VmAcc_SupplierAdjustment vmAccSupplierAdjustment;
        Int32 UserID = 0;
        public AccountsController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }

        public async Task<ActionResult> VmAcc_SupplierPHIndex(FormCollection formCollection)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var date = formCollection["DateSelect"];
            if (!string.IsNullOrEmpty(date))
            {
                ViewBag.Date = date;

                VmAcc_SupplierPH = new VmAcc_SupplierPH();
                VmAcc_SupplierPH.Date = DateTime.ParseExact(date, "yyyy/M/d", null);
                await Task.Run(() => VmAcc_SupplierPH.InitialDataLoadforSupplierSearch(VmAcc_SupplierPH.Date));
                return View(VmAcc_SupplierPH);
            }
            else
            {
                var searchDate = DateTime.Now;
                ViewBag.Date = searchDate.ToString("yyyy/M/d");
                VmAcc_SupplierPH = new VmAcc_SupplierPH();
                await Task.Run(() => VmAcc_SupplierPH.InitialDataLoadforSupplier());
            }
           
            return View(VmAcc_SupplierPH);
        }
        public async Task<ActionResult> VmAccSupplierAdjustmentPreview()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
          
                VmAcc_SupplierPH = new VmAcc_SupplierPH();
                await Task.Run(() => VmAcc_SupplierPH.SupplierAdjustmentPreview());
          
            return View(VmAcc_SupplierPH);
        }

        public async Task<ActionResult> VmAccSupplierPaymentAdjustmentPreview()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_SupplierPH = new VmAcc_SupplierPH();
            await Task.Run(() => VmAcc_SupplierPH.SupplierPaymentAdjustmentPreview());

            return View(VmAcc_SupplierPH);
        }
        //[HttpPost]
        //public async Task<ActionResult> VmAcc_SupplierPHIndex(VmAcc_SupplierPH vmAccSupplierPH)
        //{
        //    var user = (VmUser_User)Session["One"];
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //    var date = DateTime.Now;
        //    ViewBag.Date = date.ToString("yyyy/M/d");
        //    VmAcc_SupplierPH = new VmAcc_SupplierPH();
        //    await Task.Run(() => VmAcc_SupplierPH.InitialDataLoadforSupplier());
        //    return View(VmAcc_SupplierPH);
        //}
        public async Task<ActionResult> OpenedBTBBySupplier(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_SupplierPH = new VmAcc_SupplierPH();
            await Task.Run(() => VmAcc_SupplierPH = VmAcc_SupplierPH.SelectSingleSupplier(id));
            await Task.Run(() => VmAcc_SupplierPH.OpenedBTBBySupplier(id));
            return View(VmAcc_SupplierPH);
        }


        //public async Task<ActionResult> VmAcc_SupplierPHIndex()
        //{
        //    VmAcc_SupplierPH = new VmAcc_SupplierPH();
        //    await Task.Run(() => VmAcc_SupplierPH.InitialDataLoadforSupplier());
        //    return View(VmAcc_SupplierPH);
        //}
        //public ActionResult VmAcc_SupplierPHCreate()
        //{
        //    return View();
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VmAcc_SupplierPHCreate(VmAcc_SupplierPH a)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        await Task.Run(() => a.Add(UserID));
        //        return RedirectToAction("VmAcc_SupplierPHIndex");
        //    }
        //    return RedirectToAction("VmAcc_SupplierPHIndex");
        //}
        [HttpGet]
        public async Task<ActionResult> VmAcc_SupplierPHEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmAcc_SupplierPH = new VmAcc_SupplierPH();
            await Task.Run(() => VmAcc_SupplierPH.SelectSingleForSupplierPH(id.Value));
            await Task.Run(() => VmAcc_SupplierPH.InitialDataLoadSupplier(id.Value));


            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart2();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text", "Category", 1);
            ViewBag.Acc_AcName = c3;

            VmForDropDown.DDownData = VmForDropDown.GetLienBankForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.LienBank = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmAcc_SupplierPH.Acc_SupplierPH.DollarRate = 1;

            return View(VmAcc_SupplierPH);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_SupplierPHEdit(VmAcc_SupplierPH vmAcc_SupplierPH)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper vmControllerHelper = new VmControllerHelper();

            vmAcc_SupplierPH.Acc_SupplierPH.Commercial_BankFK = 1016; //Hard coded with consult with Rezwan Vai
            vmAcc_SupplierPH.Acc_SupplierPH.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            if (vmAcc_SupplierPH.Acc_SupplierPH.Amount == null)
                vmAcc_SupplierPH.Acc_SupplierPH.Amount = 0;

            // Notifier Integration Closing Balance Get
            var accountname = db.Acc_AcName.Find(vmAcc_SupplierPH.Acc_SupplierPH.Acc_AcNameFK);
            var common_Supplier = db.Common_Supplier.Find(vmAcc_SupplierPH.Acc_SupplierPH.Common_SupplierFK);

            VmNotifier vmNotifier = new VmNotifier();
            var tobalance = vmNotifier.PartyPaymentLedger(common_Supplier);
            // Notifier Integration Closing Balance Get


            if (accountname.Acc_Chart2FK == 56 || accountname.Acc_Chart2FK == 57 || accountname.Acc_Chart2FK == 58 ||
                accountname.Acc_Chart2FK == 59 || accountname.Acc_Chart2FK == 60 || accountname.Acc_Chart2FK == 61)
            {
                vmAcc_SupplierPH.Acc_SupplierPH.IsApproved = false;
            }
            else
            {
                vmAcc_SupplierPH.Acc_SupplierPH.IsApproved = true;
            }

            var value = await Task.Run(() => vmAcc_SupplierPH.Add(UserID));


            if (accountname.Acc_Chart2FK == 56 || accountname.Acc_Chart2FK == 57 || accountname.Acc_Chart2FK == 58 ||
                accountname.Acc_Chart2FK == 59 || accountname.Acc_Chart2FK == 60 || accountname.Acc_Chart2FK == 61)
            {
                try
                {
                    VmForDropDown vmForDropDown = new VmForDropDown();
                    VmAccounting accounting = new VmAccounting();
                    accounting.FromDate = new DateTime(2001, 1, 1);
                    accounting.ToDate = DateTime.Today;

                    accounting.GetLedgerAccountHead(vmAcc_SupplierPH.Acc_SupplierPH.Acc_AcNameFK, "");
                    var frombalance = accounting.Transaction.Any() ? accounting.Transaction[accounting.Transaction.Count() - 1].Balance : 0;
                    var frominfo = db.Acc_AcName.Find(vmAcc_SupplierPH.Acc_SupplierPH.Acc_AcNameFK);

                    Acc_Notifier accNotifier = new Acc_Notifier
                    {
                        Type = (int)Acc_NotifierType.Cheque,
                        Amount = vmAcc_SupplierPH.Acc_SupplierPH.Amount ?? 0,
                        FromBalance = frombalance,
                        ChequeNo = vmAcc_SupplierPH.Acc_SupplierPH.ChequeVoucherNo,
                        ToBalance = tobalance,
                        FromName = frominfo.Name,
                        ToName = common_Supplier.Name,
                        Particulars = $@"Chq No:{vmAcc_SupplierPH.Acc_SupplierPH.ChequeVoucherNo} || A/C - {accountname.Name}",
                        TableIdFk = VmAcc_Database_Table.TableName(vmAcc_SupplierPH.Acc_SupplierPH.GetType().Name),
                        RowIdFk = value,
                        FromFk = vmAcc_SupplierPH.Acc_SupplierPH.Acc_AcNameFK,
                        ToFk = common_Supplier.Acc_AcNameFk,
                        IsApproved = 0,
                        Date = DateTime.Now
                    };

                    this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
                    {
                        UserId = this.UserID,
                        Acc_Notifier = accNotifier
                    };
                    // call integration and receive boolean. if true data add in journal successfully else not added
                    var id = await Task.Run(() => this.VmAcc_ChequeIntegration.AddCheque());
                    //aproval method
                    VmNotifierApproval vmNotifierApproval = new VmNotifierApproval();
                    await Task.Run(() => vmNotifierApproval.NotifierFinalized(id, user));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            else
            {
                // journal integration
                try
                {
                    var chqno = "";
                    if (!string.IsNullOrEmpty(vmAcc_SupplierPH.Acc_SupplierPH.ChequeVoucherNo))
                        chqno = ", Cheque No: " + vmAcc_SupplierPH.Acc_SupplierPH.ChequeVoucherNo;
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = this.UserID,
                        Date = vmAcc_SupplierPH.Acc_SupplierPH.Date,
                        Title = "L/C Other Payment",
                        CurrencyType = vmAcc_SupplierPH.Acc_SupplierPH.Common_CurrencyFK,
                        CurrencyRate = (decimal)vmAcc_SupplierPH.Acc_SupplierPH.DollarRate,
                        FromAccNo = vmAcc_SupplierPH.Acc_SupplierPH.Acc_AcNameFK,
                        ToAccNo = common_Supplier.Acc_AcNameFk,
                        Amount = (decimal)(vmAcc_SupplierPH.Acc_SupplierPH.Amount),
                        Descriptions = "BTB L/C Other Payment from " + accountname.Name + " to " + common_Supplier.Name + ", Des: " + vmAcc_SupplierPH.Acc_SupplierPH.Description + chqno,
                        TableIdFk = VmAcc_Database_Table.TableName(vmAcc_SupplierPH.Acc_SupplierPH.GetType().Name),
                        TableRowIdFk = value,
                    };
                    var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
                }
                catch (Exception e)
                {

                }
            }


            return RedirectToAction("VmAcc_SupplierPHEdit", "Accounts", new { id = vmAcc_SupplierPH.Acc_SupplierPH.Common_SupplierFK });


        }
        [HttpGet]
        public async Task<ActionResult> VmAcc_SupplierReceiveHEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmAcc_SupplierReceiveH = new VmAcc_SupplierReceiveH();
            await Task.Run(() => VmAcc_SupplierReceiveH.SelectSingleForSupplierReceiveH(id.Value));

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart2();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text", "Category", 1);
            ViewBag.Acc_AcName = c3;

            VmForDropDown.DDownData = VmForDropDown.GetLienBankForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.LienBank = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmAcc_SupplierReceiveH.Acc_SupplierReceiveH.DollarRate = 1;

            return View(VmAcc_SupplierReceiveH);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_SupplierReceiveHEdit(VmAcc_SupplierReceiveH vmAcc_SupplierReceiveH)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            if (vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Amount == null)
                vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Amount = 0;

            // Notifier Integration Closing Balance Get
            var accountname = db.Acc_AcName.Find(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Acc_AcNameFK);
            var common_Supplier = db.Common_Supplier.Find(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Common_SupplierFK);

            VmNotifier vmNotifier = new VmNotifier();
            var tobalance = vmNotifier.PartyPaymentLedger(common_Supplier);
            // Notifier Integration Closing Balance Get


            if (accountname.Acc_Chart2FK == 56 || accountname.Acc_Chart2FK == 57 || accountname.Acc_Chart2FK == 58 ||
                accountname.Acc_Chart2FK == 59 || accountname.Acc_Chart2FK == 60 || accountname.Acc_Chart2FK == 61)
            {
                vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.IsApproved = false;
            }
            else
            {
                vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.IsApproved = true;
            }

            var value = await Task.Run(() => vmAcc_SupplierReceiveH.Add(UserID));


            if (accountname.Acc_Chart2FK == 56 || accountname.Acc_Chart2FK == 57 || accountname.Acc_Chart2FK == 58 ||
                accountname.Acc_Chart2FK == 59 || accountname.Acc_Chart2FK == 60 || accountname.Acc_Chart2FK == 61)
            {
                try
                {
                    VmForDropDown vmForDropDown = new VmForDropDown();
                    VmAccounting accounting = new VmAccounting();
                    accounting.FromDate = new DateTime(2001, 1, 1);
                    accounting.ToDate = DateTime.Today;

                    accounting.GetLedgerAccountHead(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Acc_AcNameFK, "");
                    var frombalance = accounting.Transaction.Any() ? accounting.Transaction[accounting.Transaction.Count() - 1].Balance : 0;
                    var frominfo = db.Acc_AcName.Find(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Acc_AcNameFK);

                    Acc_Notifier accNotifier = new Acc_Notifier
                    {
                        Type = (int)Acc_NotifierType.ChequeReceive,
                        Amount = vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Amount ?? 0,
                        FromBalance = frombalance,
                        ChequeNo = vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.ChequeVoucherNo,
                        ToBalance = tobalance,
                        ToName = frominfo.Name,
                        FromName = common_Supplier.Name,
                        Particulars = $@"Chq No:{vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.ChequeVoucherNo} || A/C - {accountname.Name}",
                        TableIdFk = VmAcc_Database_Table.TableName(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.GetType().Name),
                        RowIdFk = value,
                        ToFk = vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Acc_AcNameFK,
                        FromFk = common_Supplier.Acc_AcNameFk,
                        IsApproved = 0,
                        Date = DateTime.Now
                    };

                    this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
                    {
                        UserId = this.UserID,
                        Acc_Notifier = accNotifier
                    };
                    // call integration and receive boolean. if true data add in journal successfully else not added
                    var id = await Task.Run(() => this.VmAcc_ChequeIntegration.AddCheque());
                    //aproval method
                    VmNotifierApproval vmNotifierApproval = new VmNotifierApproval();
                    await Task.Run(() => vmNotifierApproval.NotifierChequeReceiveFinalized(id, user));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            else
            {
                // journal integration
                try
                {
                    var chqno = "";
                    if (!string.IsNullOrEmpty(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.ChequeVoucherNo))
                        chqno = ", Cheque No: " + vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.ChequeVoucherNo;
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = this.UserID,
                        Date = vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Date,
                        Title = "L/C Cash Receive",
                        CurrencyType = vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Common_CurrencyFK,
                        CurrencyRate = (decimal)vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.DollarRate,
                        ToAccNo = vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Acc_AcNameFK,
                        FromAccNo = common_Supplier.Acc_AcNameFk,
                        Amount = (decimal)(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Amount),
                        Descriptions = "BTB L/C Cash Receive to " + accountname.Name + " from " + common_Supplier.Name + ", Des: " + vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.Description + chqno,
                        TableIdFk = VmAcc_Database_Table.TableName(vmAcc_SupplierReceiveH.Acc_SupplierReceiveH.GetType().Name),
                        TableRowIdFk = value,
                    };
                    var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
                }
                catch (Exception e)
                {

                }
            }


            return RedirectToAction("VmAcc_SupplierPHIndex", "Accounts");


        }

        [HttpGet]
        public async Task<ActionResult> VmAccSupplierAdjustment(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vmAccSupplierAdjustment = new VmAcc_SupplierAdjustment();
            vmAccSupplierAdjustment.SupplierAdjustmentById(id);
            await Task.Run(() => vmAccSupplierAdjustment.SelectSingleSupplier(id));

            vmAccSupplierAdjustment.Acc_SupplierAdjustment = new Acc_SupplierAdjustment();
            vmAccSupplierAdjustment.Acc_SupplierAdjustment.Date = DateTime.Now;
            vmAccSupplierAdjustment.Acc_SupplierAdjustment.Common_SupplierFk = id;

            return View(vmAccSupplierAdjustment);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAccSupplierAdjustment(VmAcc_SupplierAdjustment model)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int count = db.Acc_SupplierAdjustment.Count();
            if (count > 0)
            {
                count = count + 1;
            }
            else
            {
                count = 1;
            }
            string str = "ADJ-" + count.ToString().PadLeft(5, '0');

            Acc_SupplierAdjustment supplierAdjustment = new Acc_SupplierAdjustment()
            {
                 CID = str,
                AdjustmentAmmount = model.Acc_SupplierAdjustment.AdjustmentAmmount,
                Common_SupplierFk = model.Acc_SupplierAdjustment.Common_SupplierFk,
                Active = true,
                Date = DateTime.Now,
                FirstCreatedBy = user.User_User.ID,
                Remarks = model.Acc_SupplierAdjustment.Remarks,
                FirstCreated = DateTime.Now
            };
            db.Acc_SupplierAdjustment.Add(supplierAdjustment);
            int id = await db.SaveChangesAsync();

            return RedirectToAction("VmAccSupplierAdjustment", new { id = model.Acc_SupplierAdjustment.Common_SupplierFk });


        }

        private void DoSomethingWith(ModelError error)
        {
            throw new NotImplementedException();
        }

        //For InventoryCrystal
        public ActionResult InventoryView()
        {
            try
            {

                VmAcc_Inventory a = new VmAcc_Inventory();
                DataTable dt = a.Inventory();

                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Reports/InventoryReport.rpt");
                cr.Load();
                cr.SetDataSource(dt);
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //.....PurchaseOrder..../
        public async Task<ActionResult> PurchaseOrderListIndex(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            int theID = 0;
            if (id != null && id.Value <= 2)
            {
                theID = id.Value;
            }
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.PurchaseOrderView(theID));
            return View(VmMkt_PO);
        }
        [HttpGet]
        public async Task<ActionResult> PurchaseOrderListEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingleForPOList(id.Value));

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PurchaseOrderListEdit(VmMkt_PO VmMkt_PO)
        {
            if (ModelState.IsValid)
            {
                UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
                await Task.Run(() => VmMkt_PO.Edit(0));
                return RedirectToAction("PurchaseOrderListIndex");
            }
            return View(VmMkt_PO);
        }


        //.....Acc_POPH.....//

        public async Task<ActionResult> VmAcc_POPHIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_POPH = new VmAcc_POPH();
            await Task.Run(() => VmAcc_POPH.InitialDataloadForAccPOPh());
            return View(VmAcc_POPH);
        }

        [HttpGet]
        public async Task<ActionResult> VmAcc_POPHEdit(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == "")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewModels.Marketing.VmMkt_PO VmMkt_PO = new VmMkt_PO();
            VmMkt_PO.Mkt_PO = new Mkt_PO();
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetB2BLCForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.b2bLc = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetCommercial_BankForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BankName = sl2;


            //int supplierID = 0;
            int poid = 0;



            bool flag = false;
            if (id.Contains("x"))
            {
                int.TryParse(id.Substring(0, id.LastIndexOf("x")), out poid);
            }
            else
            {
                int.TryParse(id, out poid);
            }

            //id = id.Replace(supplierID.ToString() + "x", "");

            //VmMkt_PO.Mkt_PO.Common_SupplierFK = supplierID;
            VmAcc_POPH = new VmAcc_POPH();
            await Task.Run(() => VmAcc_POPH.SelectSingleForAccPoPH(poid));
            if (!id.Contains("x"))
            {
                VmAcc_POPH.Common_Supplier.ID = 0;
            }
            return View(VmAcc_POPH);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_POPHEdit(VmAcc_POPH a)
        {
            //if (ModelState.IsValid)
            //{
            await Task.Run(() => a.Add(UserID, a.FinalPayment));

            //SupplierIndex
            if (a.Common_Supplier.ID == 0)
            {
                return RedirectToAction("purchaseorderlistindex");
            }
            return RedirectToAction("SupplierIndex/" + a.Common_Supplier.ID);


            //}

            //return RedirectToAction("ModelError", "Common");
        }

        public async Task<ActionResult> SupplierIndex(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_POPH = new VmAcc_POPH();
            await Task.Run(() => VmAcc_POPH.SelectSingleSupplier(id.Value));

            VmMkt_PO = new VmMkt_PO();
            //int theID = 0;
            //if (id != null && id.Value <= 2)
            //{
            //    theID = id.Value;
            //}

            await Task.Run(() => VmAcc_POPH.SupplierOrderView(id.Value));
            return View(VmAcc_POPH);
        }

        //...........Acc_Chart1.............//
        public async Task<ActionResult> VmAcc_Chart1Index()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_Chart1 = new VmAcc_Chart1();
            await Task.Run(() => VmAcc_Chart1.InitialDataLoad());
            return View(VmAcc_Chart1);
        }
        public ActionResult VmAcc_Chart1Create()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            //DropDownData y = new DropDownData();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList b = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = b;
            //VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            //SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            //ViewBag.Acc_Type = a;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_Chart1Create(VmAcc_Chart1 a)
        {
            if (ModelState.IsValid)
            {
                #region Generate Code
                int accChart1 = (from t1 in db.Acc_Chart1
                                 select t1).Count();
                if (accChart1 == 0)
                {
                    accChart1 = 1;
                }
                else
                {
                    accChart1++;
                }
                var newString = accChart1.ToString().PadLeft(2, '0');
                #endregion

                a.Acc_Chart1.Code = newString;

                await Task.Run(() => a.Add(UserID));



                return RedirectToAction("VmAcc_Chart1Index");
            }
            return RedirectToAction("VmAcc_Chart1Index");
        }

        [HttpGet]
        public async Task<ActionResult> VmAcc_Chart1Edit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmAcc_Chart1 = new VmAcc_Chart1();
            await Task.Run(() => VmAcc_Chart1.SelectSingleAcc_Chart1(id.Value));
            VmForDropDown = new VmForDropDown();


            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();

            SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = list;
            if (VmAcc_Chart1.Acc_Chart1 == null)
            {
                return HttpNotFound();
            }

            return View(VmAcc_Chart1);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_Chart1Edit(VmAcc_Chart1 VmAcc_Chart1)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmAcc_Chart1.Edit(0));
                return RedirectToAction("VmAcc_Chart1Index");
            }
            return View(VmAcc_Chart1);
        }
        public ActionResult VmAcc_Chart1Delete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Acc_Chart1 b = new Acc_Chart1();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmAcc_Chart1Index");
        }

        //-------------------Acc_Expenses_History-------------------------//
        public async Task<ActionResult> VmAcc_Expenses_HistoryIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_Expenses_History = new VmAcc_Expenses_History();
            await Task.Run(() => VmAcc_Expenses_History.InitialDataLoad());
            return View(VmAcc_Expenses_History);
        }
        public ActionResult VmAcc_Expenses_HistoryCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAllAcc_AcNameFrom();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameFrom = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAllAcc_AcNameTo();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameTo = c4;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_Expenses_HistoryCreate(VmAcc_Expenses_History a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.AddExp(UserID));
                return RedirectToAction("VmAcc_Expenses_HistoryIndex");
            }
            return RedirectToAction("VmAcc_Expenses_HistoryIndex");
        }
        //-------------------Acc_Income_History-------------------------//
        public async Task<ActionResult> VmAcc_Income_HistoryIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_Expenses_History = new VmAcc_Expenses_History();
            await Task.Run(() => VmAcc_Expenses_History.InitialDataLoad());
            return View(VmAcc_Expenses_History);
        }
        public ActionResult VmAcc_Income_HistoryCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAllAcc_AcNameFrom();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameFrom = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAllAcc_AcNameTo();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameTo = c4;



            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_Income_HistoryCreate(VmAcc_Expenses_History a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmAcc_Income_HistoryIndex");
            }
            return RedirectToAction("VmAcc_Income_HistoryIndex");
        }
        [HttpGet]
        public async Task<ActionResult> VmAcc_Expenses_HistoryEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmAcc_Expenses_History = new VmAcc_Expenses_History();
            await Task.Run(() => VmAcc_Expenses_History.SelectSingleAcc_Expenses_History(id.Value));
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = c3;
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            if (VmAcc_Expenses_History.Acc_Expenses_History == null)
            {
                return HttpNotFound();
            }

            return View(VmAcc_Expenses_History);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_Expenses_HistoryEdit(VmAcc_Expenses_History VmAcc_Expenses_History)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmAcc_Expenses_History.Edit(0));
                return RedirectToAction("VmAcc_Expenses_HistoryIndex");
            }
            return View(VmAcc_Expenses_History);
        }
        [HttpGet]
        public async Task<ActionResult> VmAcc_Income_HistoryEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmAcc_Expenses_History = new VmAcc_Expenses_History();
            await Task.Run(() => VmAcc_Expenses_History.SelectSingleAcc_Expenses_History(id.Value));
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = c3;
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            if (VmAcc_Expenses_History.Acc_Expenses_History == null)
            {
                return HttpNotFound();
            }

            return View(VmAcc_Expenses_History);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_Income_HistoryEdit(VmAcc_Expenses_History VmAcc_Expenses_History)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmAcc_Expenses_History.Edit(0));
                return RedirectToAction("VmAcc_Income_HistoryIndex");
            }
            return View(VmAcc_Expenses_History);
        }
        public ActionResult VmAcc_Expenses_HistoryDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Acc_Expenses_History b = new Acc_Expenses_History();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmAcc_Expenses_HistoryIndex");
        }

        //...........Acc_Chart2.............//
        public async Task<ActionResult> VmAcc_Chart2Index()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_Chart2 = new VmAcc_Chart2();
            await Task.Run(() => VmAcc_Chart2.InitialDataLoad());
            return View(VmAcc_Chart2);
        }
        public ActionResult VmAcc_Chart2Create()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = a;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart1();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = c1;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_Chart2Create(VmAcc_Chart2 a)
        {
            if (ModelState.IsValid)
            {

                #region Generate Code
                int accChart2 = (from t1 in db.Acc_Chart2
                                 select t1).Count();
                if (accChart2 == 0)
                {
                    accChart2 = 1;
                }
                else
                {
                    accChart2++;
                }
                var newString = accChart2.ToString().PadLeft(3, '0');
                #endregion

                a.Acc_Chart2.Code = newString;
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmAcc_Chart2Index");
            }
            return RedirectToAction("VmAcc_Chart2Index");
        }

        [HttpGet]
        public async Task<ActionResult> VmAcc_Chart2Edit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmAcc_Chart2 = new VmAcc_Chart2();
            await Task.Run(() => VmAcc_Chart2.SelectSingleAcc_Chart2(id.Value));


            //..............DropDown......//
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList list1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = list1;

            VmForDropDown.DDownData = VmForDropDown.GetChart1ForEdit(id);
            SelectList list2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = list2;

            if (VmAcc_Chart2.Acc_Chart2 == null)
            {
                return HttpNotFound();
            }
            return View(VmAcc_Chart2);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_Chart2Edit(VmAcc_Chart2 VmAcc_Chart2)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmAcc_Chart2.Edit(0));
                return RedirectToAction("VmAcc_Chart2Index");
            }
            return View(VmAcc_Chart2);
        }
        public ActionResult VmAcc_Chart2Delete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Acc_Chart2 b = new Acc_Chart2();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmAcc_Chart2Index");
        }


        //...........Acc_AcName.............//
        public async Task<ActionResult> VmAcc_AcNameIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_AcName = new VmAcc_AcName();
            await Task.Run(() => VmAcc_AcName.InitialDataLoad());
            return View(VmAcc_AcName);
        }
        public ActionResult VmAcc_AcNameCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = a;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart1();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = c1;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart2();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_AcNameCreate(VmAcc_AcName a)
        {
            if (ModelState.IsValid)
            {
                #region Generate Code
                int accName = (from t1 in db.Acc_AcName
                               select t1).Count();
                if (accName == 0)
                {
                    accName = 1;
                }
                else
                {
                    accName++;
                }

                var CodeChart2 = db.Acc_Chart2.Where(x => x.ID == a.Acc_AcName.Acc_Chart2FK).SingleOrDefault();
                var CodeChart1 = db.Acc_Chart1.Where(x => x.ID == CodeChart2.Acc_Chart1FK).SingleOrDefault();

                Acc_Type t = new Acc_Type();
                var accType = (from tbl in t.GetAcc_Type()
                               where tbl.ID == CodeChart1.Acc_TypeFK
                               select new Acc_Type { Code = tbl.Code }).SingleOrDefault();


                var newString = accType.Code + CodeChart1.Code + CodeChart2.Code + accName.ToString().PadLeft(3, '0');

                #endregion
                a.Acc_AcName.Code = newString;
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmAcc_AcNameIndex");
            }

            return RedirectToAction("VmAcc_AcNameIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmAcc_AcNameEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmAcc_AcName = new VmAcc_AcName();
            await Task.Run(() => VmAcc_AcName.SelectSingleAcc_AcName(id.Value));

            //..........DropDown.............//

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_TypeDropDownForEdit(VmAcc_AcName.Acc_AcName.Acc_Chart2FK);
            SelectList list1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = list1;

            VmForDropDown.DDownData = VmForDropDown.GetChart1ForEdit(VmAcc_AcName.Acc_AcName.Acc_Chart2FK);
            SelectList list2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = list2;

            VmForDropDown.DDownData = VmForDropDown.Acc_Chart2DropDownEdit(VmAcc_AcName.Acc_AcName.Acc_Chart2FK);
            SelectList list3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = list3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;


            if (VmAcc_AcName.Acc_AcName == null)
            {
                return HttpNotFound();
            }
            return View(VmAcc_AcName);
        }

        public class ItemDetail
        {
            public int AccNameID { get; set; }
            public string AccName { get; set; }
            public int Count { get; set; }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_AcNameEdit(VmAcc_AcName VmAcc_AcName)
        {
            if (ModelState.IsValid)
            {
                //var items = db.Acc_AcName.AsEnumerable().Select((u, index) => new ItemDetail { AccNameID = u.ID, AccName = u.Name, Count = index + 1 }).ToList();

                //foreach (var x in items)
                //{
                //    VmAcc_AcName.SelectSingleAcc_AcName(x.AccNameID);
                //    VmAcc_AcName.Acc_AcName.Code = GetAllCode(x.AccNameID) + x.Count.ToString().PadLeft(3, '0');
                //    await Task.Run(() => VmAcc_AcName.Edit(0));
                //}
                await Task.Run(() => VmAcc_AcName.Edit(0));
                return RedirectToAction("VmAcc_AcNameIndex");
            }
            return View(VmAcc_AcName);
        }

        private string GetAllCode(int AccNameID)
        {
            db = new xOssContext();


            var accName = db.Acc_AcName.Where(x => x.ID == AccNameID).SingleOrDefault();

            var accCart2 = db.Acc_Chart2.Where(x => x.ID == accName.Acc_Chart2FK).SingleOrDefault();

            var accCart1 = db.Acc_Chart1.Where(x => x.ID == accCart2.Acc_Chart1FK).SingleOrDefault();

            Acc_Type t = new Acc_Type();
            var accType = (from tbl in t.GetAcc_Type() where tbl.ID == accCart1.Acc_TypeFK select new Acc_Type { Code = tbl.Code }).SingleOrDefault();

            return accType.Code + accCart1.Code + accCart2.Code;


        }

        public ActionResult VmAcc_AcNameDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            #region DeadCode
            //var data = db.Acc_JournalSlave.Where(x => x.ID == id).ToList();
            //if (data.Any())
            //{

            //}
            //else
            //{
            //    Acc_AcName b = new Acc_AcName();
            //    b.ID = id;
            //    b.Delete(0);
            //}
            #endregion
            Acc_AcName b = new Acc_AcName();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmAcc_AcNameIndex");
        }
        //...................Acc_Journal..................//
        public async Task<ActionResult> VmAcc_JournalIndex(FormCollection formCollection)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var date = formCollection["DateSelect"];
            if (!string.IsNullOrEmpty(date))
            {
                ViewBag.TransactionDate = date;

                VmAcc_Journal = new VmAcc_Journal();
                VmAcc_Journal.Date = DateTime.ParseExact(date, "yyyy/M/d", null);
                await Task.Run(() => VmAcc_Journal.InitialDataLoad());
                return View(VmAcc_Journal);
            }
            else
            {
                var transactionDate = DateTime.Now;
                ViewBag.TransactionDate = transactionDate.ToString("yyyy/M/d");

                VmAcc_Journal = new VmAcc_Journal();
                VmAcc_Journal.Date = DateTime.ParseExact(transactionDate.ToString("yyyy/M/d"), "yyyy/M/d", null);
                await Task.Run(() => VmAcc_Journal.InitialDataLoad());
                return View(VmAcc_Journal);
            }


        }
        public ActionResult VmAcc_JournalCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_JournalCreate(VmAcc_Journal a)
        {
            if (ModelState.IsValid)
            {
                a.Acc_Journal.Title = "JE:" + a.Acc_Journal.Title;
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmAcc_JournalIndex");
            }
            return RedirectToAction("VmAcc_JournalIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmAcc_JournalEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_Journal = new VmAcc_Journal();
            await Task.Run(() => VmAcc_Journal.SelectSingleAcc_Type(id.Value));

            if (VmAcc_Journal.Acc_Journal == null)
            {
                return HttpNotFound();
            }
            return View(VmAcc_Journal);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_JournalEdit(VmAcc_Journal VmAcc_Journal)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmAcc_Journal.Edit(0));
                return RedirectToAction("VmAcc_JournalIndex");
            }
            return View(VmAcc_Journal);
        }
        public ActionResult VmAcc_JournalDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Acc_Journal b = new Acc_Journal();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmAcc_JournalIndex");
        }

        //...................Acc_JournalSlave..................//
        public async Task<ActionResult> VmAcc_JournalSlaveIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_JournalSlave = new VmAcc_JournalSlave();
            await Task.Run(() => VmAcc_JournalSlave.InitialDataLoad());
            return View(VmAcc_JournalSlave);
        }
        public ActionResult VmAcc_JournalSlaveCreate(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //........DropDown...........//
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = a;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart1();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = c1;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart2();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = c3;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CurrencyList = c4;

            VmAcc_JournalSlave = new VmAcc_JournalSlave();
            VmAcc_JournalSlave.Acc_JournalSlave = new Models.Accounts.Acc_JournalSlave();
            VmAcc_JournalSlave.Acc_JournalSlave.Acc_JournalFK = id.Value;

            VmAcc_JournalSlave.Acc_Journal = new Models.Accounts.Acc_Journal();
            VmAcc_JournalSlave.Acc_Journal = db.Acc_Journal.Find(id);

            return View(VmAcc_JournalSlave);
        }
        public ActionResult VmAcc_JournalSlaveCreditCreate(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //........DropDown...........//
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = a;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart1();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = c1;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart2();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = c3;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CurrencyList = c4;

            VmAcc_JournalSlave = new VmAcc_JournalSlave();
            VmAcc_JournalSlave.Acc_JournalSlave = new Models.Accounts.Acc_JournalSlave();
            VmAcc_JournalSlave.Acc_JournalSlave.Acc_JournalFK = id.Value;

            VmAcc_JournalSlave.Acc_Journal = new Models.Accounts.Acc_Journal();
            VmAcc_JournalSlave.Acc_Journal = db.Acc_Journal.Find(id);

            return View(VmAcc_JournalSlave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_JournalSlaveCreate(VmAcc_JournalSlave a)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));

                return RedirectToAction("Acc_JournalSlave/" + a.Acc_JournalSlave.Acc_JournalFK);

            }
            return RedirectToAction("VmAcc_JournalSlaveIndex");

        }

        [HttpGet]
        public async Task<ActionResult> VmAcc_JournalSlaveEdit(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editID = 0, redID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editID);

            id = id.Replace(editID.ToString() + "x", "");
            int.TryParse(id, out redID);



            if (editID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmAcc_JournalSlave = new VmAcc_JournalSlave();
            await Task.Run(() => VmAcc_JournalSlave.SelectSingleAcc_JournalSlave(editID));
            //............DropDown..........//


            if (VmAcc_JournalSlave.Acc_JournalSlave == null)
            {
                return HttpNotFound();
            }

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_TypeDropDownForEdit(VmAcc_JournalSlave.Acc_JournalSlave.Acc_AcNameFK);
            SelectList list1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = list1;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameForEdit(VmAcc_JournalSlave.Acc_JournalSlave.Acc_AcNameFK);
            SelectList list2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = list2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart2DropDownEdit(VmAcc_JournalSlave.Acc_JournalSlave.Acc_AcNameFK);
            SelectList list3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = list3;


            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameDropDownEdit(VmAcc_JournalSlave.Acc_JournalSlave.Acc_AcNameFK);
            SelectList list4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = list4;

            return View(VmAcc_JournalSlave);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_JournalSlaveEdit(VmAcc_JournalSlave VmAcc_JournalSlave)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmAcc_JournalSlave.Edit(0));
                if (VmAcc_JournalSlave.Acc_JournalSlave.Acc_JournalFK != null)
                {
                    return RedirectToAction("Acc_JournalSlave/" + VmAcc_JournalSlave.Acc_JournalSlave.Acc_JournalFK);
                }
                return RedirectToAction("VmAcc_JournalSlaveIndex");

            }
            return View(VmAcc_JournalSlave);
        }
        public ActionResult VmAcc_JournalSlaveDelete(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);
            Acc_JournalSlave b = new Acc_JournalSlave();
            b.ID = deleteId;
            b.Delete(0);
            if (redirictId != null)
            {
                return RedirectToAction("Acc_JournalSlave/" + redirictId);
            }
            return RedirectToAction("VmAcc_JournalSlaveIndex");
        }
        public async Task<ActionResult> Acc_JournalSlave(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_JournalSlave = new VmAcc_JournalSlave();
            await Task.Run(() => VmAcc_JournalSlave.SingleAcc_Journal(id.Value));
            await Task.Run(() => VmAcc_JournalSlave.InitialDataLoadForAcc_Journal(id.Value));
            return View(VmAcc_JournalSlave);
        }
        public async Task<ActionResult> JournalSlaveFinalise(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int idForFinalise = Convert.ToInt32(id);
            VmAcc_JournalSlave accJournalSlave = new VmAcc_JournalSlave();
            accJournalSlave.InitialDataLoadForAcc_Journal(idForFinalise);
            if (accJournalSlave.CrSummary == accJournalSlave.DrSummary)
            {
                VmAcc_Journal o = new VmAcc_Journal();
                await Task.Run(() => o.ChangeFinaliseStatus(idForFinalise));
                return RedirectToAction("VmAcc_JournalIndex");
            }
            else
            {
                Session["error_div"] = "true";
                Session["error_msg"] = "Debit credit mismatch.";
                return RedirectToAction("Acc_JournalSlave", new { id });
            }
        }

        //........VmLedger...//
        //......For Demo..//
        //public async Task<ActionResult> xTempCheckLedger()
        //{
        //    //DateTime d1 = DateTime.Now.AddDays(-5);
        //    //DateTime d2 = DateTime.Now.AddDays(5);
        //    VmAccounting x = new VmAccounting();
        //    x.FromDate= DateTime.Now.AddDays(-5);
        //    x.ToDate= DateTime.Now.AddDays(5);
        //    x.GetTransactionOfAccount(1);
        //    return View();
        //}

        //.............. Acc_LCAlocation.............//
        public async Task<ActionResult> VmAcc_LCAlocationIndex(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmAcc_LCAlocation = new VmAcc_LCAlocation();
                await Task.Run(() => VmAcc_LCAlocation = VmAcc_LCAlocation.SelectSingleSupplier(id.Value));
                await Task.Run(() => VmAcc_LCAlocation.InitialDataLoad(id.Value));
                return View(VmAcc_LCAlocation);
            }

            return View("SupplierIndex");
        }

        [HttpGet]
        public ActionResult VmAcc_LCAlocationCreate(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {

                //...For dropdrown...///
                VmForDropDown = new VmForDropDown();
                VmForDropDown.DDownData = VmForDropDown.GetCommercial_MasterLCForDropDown();
                SelectList m = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.Commercial_MasterLC = m;

                VmForDropDown.DDownData = VmForDropDown.GetMkt_POForDropDownSpecificSupplier(id.Value);
                SelectList p = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.Mkt_PO = p;

                VmAcc_LCAlocation = new VmAcc_LCAlocation();
                VmAcc_LCAlocation.Acc_LCAlocation = new Acc_LCAlocation();
                VmAcc_LCAlocation.Common_Supplier = new Models.Common.Common_Supplier();
                VmAcc_LCAlocation.Common_Supplier.ID = id.Value;
                VmAcc_LCAlocation.Acc_LCAlocation.Date = DateTime.Today;

                return View(VmAcc_LCAlocation);


            }

            return RedirectToAction("VmAcc_LCAlocationIndex/" + VmAcc_LCAlocation.Common_Supplier.ID);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_LCAlocationCreate(VmAcc_LCAlocation a)
        {
            await Task.Run(() => a.Add(UserID));
            return RedirectToAction("VmAcc_LCAlocationIndex/" + a.Common_Supplier.ID);

        }
        [HttpGet]
        public async Task<ActionResult> VmAcc_LCAlocationEdit(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int editId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out editId);
            id = id.Replace(editId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //...For dropdrown...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMkt_POForDropDown();
            ViewBag.Mkt_PO = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmForDropDown.DDownData = VmForDropDown.GetCommercial_MasterLCForDropDown();
            ViewBag.Commercial_MasterLC = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmAcc_LCAlocation = new VmAcc_LCAlocation();
            await Task.Run(() => VmAcc_LCAlocation.SelectSingle(editId));

            return View(VmAcc_LCAlocation);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_LCAlocationEdit(VmAcc_LCAlocation x)
        {
            await Task.Run(() => x.Edit(0));
            return RedirectToAction("VmAcc_LCAlocationIndex/" + x.Common_Supplier.ID);
        }
        public ActionResult VmAcc_LCAlocationDelete(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteId = 0, redirictId = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteId);
            id = id.Replace(deleteId.ToString() + "x", "");
            int.TryParse(id, out redirictId);

            Acc_LCAlocation b = new Acc_LCAlocation();
            b.ID = deleteId;
            b.Delete(0);

            if (redirictId != null)
            {
                return RedirectToAction("VmAcc_LCAlocationIndex/" + redirictId);

            }
            return RedirectToAction("SupplierIndex", "Accounts");
        }

        public ActionResult VmAccountingview()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();

        }


        //............Ledger of A/C Type.................//
        public ActionResult VmLedgerType()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = a;
            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmLedgerType(VmAccounting x)
        {
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/Ledger.rpt");
            cr.Load();
            x.GetLedgerAccountType(x.Acc_Type.ID);
            cr.SetDataSource(x.LedgerReport);
            ///
            //cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;
            Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        //............Ledger of Category 1......//

        public ActionResult VmLedgerofCat1()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = a;
            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart1();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = c1;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CurrencyList = sl3;

            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;


            return View(x);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmLedgerofCat1(VmAccounting x)
        {


            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/Ledger.rpt");
            cr.Load();

            x.GetLedgerAccountChart1(x.Acc_Chart1.ID, x.LedgerName);
            cr.SetDataSource(x.LedgerReport);
            ///
            //cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;

            Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");


        }
        //............Ledger of Category 2......//
        public ActionResult VmLedgerofCat2()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();



            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = a;
            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart1();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = c1;
            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart2();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = c2;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CurrencyList = sl3;


            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmLedgerofCat2(VmAccounting x)
        {


            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/Ledger.rpt");
            cr.Load();

            x.GetLedgerAccountChart2(x.Acc_Chart2.ID, x.LedgerName);
            cr.SetDataSource(x.LedgerReport);
            ///
            //cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;

            Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");


        }

        //............Balance Sheet......//
        //public ActionResult VmBalanceSheet()
        //{
        //    VmAccounting x = new VmAccounting();
        //    x.FromDate = DateTime.Now.AddDays(-30);
        //    x.ToDate = DateTime.Now;
        //    return View(x);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult VmBalanceSheet(VmAccounting x)
        //{


        //    ReportClass cr = new ReportClass();
        //    cr.FileName = Server.MapPath("~/Views/Reports/Ledger.rpt");
        //    cr.Load();

        //    x.GetLedgerAccountHead(16);
        //    cr.SetDataSource(x.ImportReport);
        //    ///
        //    //cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;

        //    Stream stream =
        //        cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    return File(stream, "application/pdf");


        //}
        //............Journal Report......//
        public ActionResult VmJournalReport()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmJournalReport(VmAccounting x)
        {


            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/Ledger.rpt");
            cr.Load();

            x.GetLedgerAccountHead(16, x.LedgerName);
            cr.SetDataSource(x.LedgerReport);
            ///
            //cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;

            Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");


        }

        //............LedgerAC......//
        public ActionResult VmLedgerAC()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = a;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart1();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = c1;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart2();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = c3;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CurrencyList = c4;

            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;
            //x.GetTransactionOfAccount(1);

            return View(x);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmLedgerAC(VmAccounting x)
        {
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/Ledger.rpt");
            cr.Load();
            x.GetLedgerAccountHead(x.Acc_AcName.ID, x.LedgerName);
            cr.SetDataSource(x.LedgerReport);
            ///
            //cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;

            Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }



        public ActionResult VmLedgerCashBook()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCashDr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = c3;
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CurrencyList = c4;


            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;

            return View(x);
        }
        public ActionResult VmLedgerBankBook()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameBankDr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = c3;
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CurrencyList = c4;

            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        public ActionResult VmLedgerLoanBook()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameLoanDr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = c3;
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CurrencyList = c4;

            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;

            return View(x);
        }
        //............LedgerAC......//
        public ActionResult VmLedgerReceivePayment()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Type();
            SelectList a = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Type = a;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart1();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart1 = c1;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_Chart2();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_Chart2 = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcName = c3;
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CurrencyList = c4;

            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now;
            //x.GetTransactionOfAccount(1);

            return View(x);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmReceivePaymentPartViewLedgerReport(VmAccounting x)
        {
            VmAccounting vmAccounting = new VmAccounting();
            vmAccounting.FromDate = x.FromDate;
            vmAccounting.ToDate = x.ToDate;
            vmAccounting.Acc_AcName = x.Acc_AcName;
            vmAccounting.LedgerName = x.LedgerName;
            vmAccounting.CurrencyType = x.CurrencyType;
            vmAccounting.GetLedgerAccountHead(x.Acc_AcName.ID, x.LedgerName);
            return View(vmAccounting);
        }
        public ActionResult SupplierLedger1LCIssuedDoc(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            if (!id.HasValue)
            {
                IdNullDivertion("SupplierLedger1LCIssuedDoc", "Id is Null");
            }
            try
            {

                VmAcc_POPH = new VmAcc_POPH();
                VmAcc_POPH.GetSupplierLedger1LCIssuedInfo(id.Value);
                ///////
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/SupplierLedger1LCIssuedDoc.rpt");
                cr.Load();
                cr.SetDataSource(VmAcc_POPH.SupplierLedger);
                /////
                cr.SummaryInfo.ReportTitle = "Report of Supplier Ledger1 L/C Issued";

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("SupplierLedger1LCIssuedDoc", ex.Message);
            }
            return null;
        }

        public ActionResult SupplierLedger2B2bDoc(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (!id.HasValue)
            {
                IdNullDivertion("SupplierLedger2B2bDoc", "Id is Null");
            }
            try
            {

                VmAcc_POPH = new VmAcc_POPH();
                VmAcc_POPH.GetSupplierLedger2B2bDocInfo(id.Value);
                ///////
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/SupplierLedger2B2bDoc.rpt");
                cr.Load();
                cr.SetDataSource(VmAcc_POPH.SupplierLedger);
                /////
                cr.SummaryInfo.ReportTitle = "Report of Supplier Ledger2 B2b Document";

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("SupplierLedger2B2bDoc", ex.Message);
            }
            return null;
        }
        private ActionResult IdNullDivertion(string methodName, string messege)
        {
            ErrorReports er = new ErrorReports();
            er.ControllerName = this.ToString();
            er.MethodName = methodName;
            er.ErrorDescription = "Id is Null";
            er.EmailErrorAuto();
            return View("../Home/ErrorView", er);
        }
        //............Ledger1 of Supplier  L/C Allocation.................//
        public ActionResult VmLedger1LCallocation(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);

            x.ToDate = DateTime.Now.AddDays(1);
            //x.Common_Supplier = new Models.Common.Common_Supplier();
            //x.Common_Supplier.ID = id.Value;
            x.SelectSingle(id.Value);
            return View(x);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmLedger1LCallocation(VmAccounting x)
        {
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/LCOpeningLedger.rpt");
            cr.Load();
            x.GetSupplierLedgerLCOpening(x.Common_Supplier.ID);
            cr.SetDataSource(x.LedgerReport);
            cr.SummaryInfo.ReportTitle = "LC Opening Ledger";
            Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        public ActionResult VmLedgerSupplierLiabilities(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);

            x.ToDate = DateTime.Now.AddDays(1);
            //x.Common_Supplier = new Models.Common.Common_Supplier();
            //x.Common_Supplier.ID = id.Value;
            x.SelectSingle(id.Value);
            return View(x);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmLedgerSupplierLiabilities(VmAccounting x)
        {
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/SupplierLiabilitiesLedger.rpt");
            cr.Load();
            //x.GetSupplierLedgerLCOpening(x.Common_Supplier.ID);
            x.GetPOWiseSupplierLedgerOpening(x.Common_Supplier.ID);
            cr.SetDataSource(x.LedgerReport);
            cr.SummaryInfo.ReportTitle = "PO Wise Supplier Liabilities";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        //............Ledger1 of Supplier  Payment.................//
        public ActionResult VmLedger2SupplierPayment(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);
            x.ToDate = DateTime.Now.AddDays(1);
            //x.Common_Supplier = new Models.Common.Common_Supplier();
            //x.Common_Supplier.ID = id.Value;
            x.SelectSingle(id.Value);
            return View(x);
        }

        public ActionResult VmDeletedPOList(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddDays(-30);

            x.ToDate = DateTime.Now.AddDays(1);
            //x.Common_Supplier = new Models.Common.Common_Supplier();
            //x.Common_Supplier.ID = id.Value;
            x.SelectSingle(id.Value);
            return View(x);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmDeletedPOList(VmAccounting x)
        {
            x.DeletedPOList(x);
            return View(x);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmLedger2SupplierPayment(VmAccounting x)
        {
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/PaymentLedger.rpt");
            cr.Load();
            x.GetSupplierPaymentLedger(x.Common_Supplier.ID);
            cr.SetDataSource(x.LedgerReport);
            cr.SummaryInfo.ReportTitle = "Payment ledger";
            Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        public async Task<ActionResult> POwiseGoodsReceived(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_SupplierPH = new VmAcc_SupplierPH();
            await Task.Run(() => VmAcc_SupplierPH = VmAcc_SupplierPH.SelectSingleSupplier(id));
            await Task.Run(() => VmAcc_SupplierPH.POwiseReceivedGoods(id));
            return View(VmAcc_SupplierPH);
        }
        //..................PO wise Goods Received.............................//
        public ActionResult POwiseGoodsReceivedDoc(int id)
        {

            if (id == null)
            {
                IdNullDivertion("GoodsReceivedDoc", "Id is Null");
            }
            try
            {
                VmAcc_SupplierPH b = new VmAcc_SupplierPH();
                b.GoodsReceivedDocLoad(id);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/POgoodsReceivedDoc.rpt");
                cr.Load();
                cr.SetDataSource(b.GoodsReportDoc);
                //cr.SummaryInfo.ReportTitle = "PO Wise Goods Received From" + b.Common_Supplier.Name;
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("GoodsReportDoc", ex.Message);
            }
            return null;
        }

        //............Income Statement......//
        public ActionResult VmIncomeStatement()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddMonths(-6);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        //............Income Statement......//
        public ActionResult VmProfitLossAccount()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetChartType();
            ViewBag.ChartType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddMonths(-6);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        //............Income Statement......//
        public ActionResult VmAcc_ExpenseStatement()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddMonths(-6);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        public ActionResult VmAcc_AssetStatement()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddMonths(-6);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult VmIncomeStatement(VmAccounting x)
        //{

        //    ReportClass cr = new ReportClass();
        //    cr.FileName = Server.MapPath("~/Views/Reports/IncomeStatement.rpt");
        //    cr.Load();

        //    x.GetIncomeStatementReport();
        //    cr.SetDataSource(x.LedgerReport);
        //    //cr.SummaryInfo.ReportTitle = "Report of " + y.Common_TheOrder.CID + "/" + b.Mkt_BOM.Style;

        //    Stream stream =
        //        cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    return File(stream, "application/pdf");

        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmAcc_ProfitLossStatementReport(VmAccounting x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_IncomeStatement vmAccIncomeStatement = new VmAcc_IncomeStatement();
            vmAccIncomeStatement.FromDate = x.FromDate;
            vmAccIncomeStatement.ToDate = x.ToDate;
            vmAccIncomeStatement.InitialDataLoad(vmAccIncomeStatement);
            return View(vmAccIncomeStatement);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmAcc_IncomeStatementReport(VmAccounting x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_IncomeStatement vmAccIncomeStatement = new VmAcc_IncomeStatement();
            vmAccIncomeStatement.FromDate = x.FromDate;
            vmAccIncomeStatement.ToDate = x.ToDate;
            vmAccIncomeStatement.InitialDataLoad(vmAccIncomeStatement);
            return View(vmAccIncomeStatement);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmAcc_OnlyIncomeStatementReport(VmAccounting x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_IncomeStatement vmAccIncomeStatement = new VmAcc_IncomeStatement();
            vmAccIncomeStatement.FromDate = x.FromDate;
            vmAccIncomeStatement.ToDate = x.ToDate;
            vmAccIncomeStatement.InitialDataLoadOnlyIncome(vmAccIncomeStatement);
            return View(vmAccIncomeStatement);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmAcc_OnlyExpenseStatementReport(VmAccounting x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_IncomeStatement vmAccIncomeStatement = new VmAcc_IncomeStatement();
            vmAccIncomeStatement.FromDate = x.FromDate;
            vmAccIncomeStatement.ToDate = x.ToDate;
            vmAccIncomeStatement.InitialDataLoadOnlyExpense(vmAccIncomeStatement);
            return View(vmAccIncomeStatement);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmAcc_OnlyAssetStatementReport(VmAccounting x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_AssetStatement vmAccAssetStatement = new VmAcc_AssetStatement();
            vmAccAssetStatement.FromDate = x.FromDate;
            vmAccAssetStatement.ToDate = x.ToDate;
            vmAccAssetStatement.InitialDataLoadOnlyAsset(vmAccAssetStatement);
            return View(vmAccAssetStatement);
        }

        //public ActionResult VmIncomeStatementHTML()
        //{
        //    VmAccounting x = new VmAccounting();
        //    x.FromDate = DateTime.Now.AddMonths(-6);
        //    x.ToDate = DateTime.Now;
        //    return View(x);
        //}
        //public async Task<ActionResult> VmIncomeStatementHTML(VmAccounting x)
        public async Task<ActionResult> VmIncomeStatementHTML()
        {
            VmAccounting x = new VmAccounting();
            await Task.Run(() => x.IncomeStatementHTML());
            //ViewBag.Item = x.CostofGoods;         
            //ViewBag.Sold = x.Sold;
            //ViewBag.GrossP = x.Sold-x.CostofGoods;
            return View(VmAccounting);
        }

        //............Balance Sheet......//
        public ActionResult VmBalanceSheet()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetChartType();
            ViewBag.ChartType = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddMonths(-6);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmAcc_BalanceSheetReport(VmAccounting x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_BalanceSheet accBalanceSheet = new VmAcc_BalanceSheet();
            accBalanceSheet.FromDate = x.FromDate;
            accBalanceSheet.ToDate = x.ToDate;
            accBalanceSheet.InitialDataLoad(accBalanceSheet);
            return View(accBalanceSheet);
        }
        //............Balance Sheet......//
        public ActionResult VmReceiptPayment()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAccounting x = new VmAccounting();
            x.FromDate = DateTime.Now.AddMonths(-6);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmAcc_ReceiptPaymentReport(VmAccounting x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_ReceiptPayment accReceiptPayment = new VmAcc_ReceiptPayment();
            accReceiptPayment.FromDate = x.FromDate;
            accReceiptPayment.ToDate = x.ToDate;
            accReceiptPayment.InitialDataLoad(accReceiptPayment);
            return View(accReceiptPayment);
        }




        //view Chart of account 

        public async Task<ActionResult> VmAcc_ChartOfAccountView()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_AcName = new VmAcc_AcName();
            await Task.Run(() => VmAcc_AcName.InitialDataLoad());
            return View(VmAcc_AcName);
        }


        public ActionResult VmAcc_TrialBalance()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmAcc_TrialBalanceClass x = new VmAcc_TrialBalanceClass();
            x.ChartOfAccountType = new List<string>(new string[] { "Chart Of 1 Account Head", "Chart Of 2 Account Head", "Account Head" });
            x.FromDate = DateTime.Now.AddMonths(-1);
            x.ToDate = DateTime.Now;
            return View(x);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VmAcc_TrialBalanceReport(VmAcc_TrialBalanceClass x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_TrialBalance vmAccTrialBalance = new VmAcc_TrialBalance();
            vmAccTrialBalance.FromDate = x.FromDate;
            vmAccTrialBalance.ToDate = x.ToDate;
            vmAccTrialBalance.CurrencyType = x.CurrencyType;
            vmAccTrialBalance.ChartOfAccHeadNo = x.ChartOfAccountType[0];
            vmAccTrialBalance.InitialDataLoad();
            return View(vmAccTrialBalance);
        }

        public async Task<ActionResult> VmAcc_TransactionIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_Transaction VmAcc_Transaction = new VmAcc_Transaction();
            VmAcc_Transaction.Date = DateTime.Today;
            await Task.Run(() => VmAcc_Transaction.InitialDataLoad());
            return View(VmAcc_Transaction);
        }
        public async Task<ActionResult> VmAcc_BankWithdrawCreate()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCr();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameBankDr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;
            try
            {
                ViewBag.VoucherNo = await Task.Run(() => (from c in db.Acc_Transaction select c).Max(c => c.VoucherNo) + 1);
            }
            catch (Exception e)
            {
                ViewBag.VoucherNo = 1;
            }


            VmAcc_Transaction vmAccTransaction = new VmAcc_Transaction();

            Acc_Transaction acctrns = new Acc_Transaction();
            acctrns.Date = DateTime.Today;
            vmAccTransaction.Acc_Transaction = acctrns;

            return View(vmAccTransaction);
        }
        public async Task<ActionResult> VmAcc_CashWithdrawCreate()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCashDr();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;
            try
            {
                ViewBag.VoucherNo = await Task.Run(() => (from c in db.Acc_Transaction select c)
                .Where(c => c.Date.Year == DateTime.Now.Year)
                .Max(c => c.VoucherNo) + 1);
            }
            catch (Exception e)
            {
                ViewBag.VoucherNo = 1;
            }


            VmAcc_Transaction vmAccTransaction = new VmAcc_Transaction();

            Acc_Transaction acctrns = new Acc_Transaction();
            acctrns.Date = DateTime.Today;
            vmAccTransaction.Acc_Transaction = acctrns;

            return View(vmAccTransaction);
        }
        public async Task<ActionResult> VmAcc_BankDepositCreate()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameBankDr();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;
            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;
            try
            {
                ViewBag.VoucherNo = await Task.Run(() => (from c in db.Acc_Transaction select c)
                                                         .Where(c => c.Date.Year == DateTime.Now.Year)
                                                         .Max(c => c.VoucherNo) + 1);
            }
            catch (Exception e)
            {
                ViewBag.VoucherNo = 1;
            }


            VmAcc_Transaction vmAccTransaction = new VmAcc_Transaction();

            Acc_Transaction acctrns = new Acc_Transaction();
            acctrns.Date = DateTime.Today;
            vmAccTransaction.Acc_Transaction = acctrns;

            return View(vmAccTransaction);
        }
        public async Task<ActionResult> VmAcc_CashDepositCreate()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCr();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCashDr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;
            try
            {
                ViewBag.VoucherNo = await Task.Run(() => (from c in db.Acc_Transaction select c)
                                                         .Where(c => c.Date.Year == DateTime.Now.Year)
                                                         .Max(c => c.VoucherNo) + 1);
            }
            catch (Exception e)
            {
                ViewBag.VoucherNo = 1;
            }



            VmAcc_Transaction vmAccTransaction = new VmAcc_Transaction();

            Acc_Transaction acctrns = new Acc_Transaction();
            acctrns.Date = DateTime.Today;
            vmAccTransaction.Acc_Transaction = acctrns;

            return View(vmAccTransaction);
        }
        public async Task<ActionResult> VmAcc_FundTransferCreate()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameFundTransfer();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameFundTransfer();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;
            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;
            try
            {
                ViewBag.VoucherNo = await Task.Run(() => (from c in db.Acc_Transaction select c).Max(c => c.VoucherNo) + 1);
            }
            catch (Exception e)
            {
                ViewBag.VoucherNo = 1;
            }



            VmAcc_Transaction vmAccTransaction = new VmAcc_Transaction();

            Acc_Transaction acctrns = new Acc_Transaction();
            acctrns.Date = DateTime.Today;
            vmAccTransaction.Acc_Transaction = acctrns;

            return View(vmAccTransaction);
        }

        public async Task<ActionResult> VmAcc_JournalEntryCreate()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameJournalEntry();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameJournalEntry();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;
            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;
            try
            {
                ViewBag.VoucherNo = await Task.Run(() => (from c in db.Acc_Transaction select c).Max(c => c.VoucherNo) + 1);
            }
            catch (Exception e)
            {
                ViewBag.VoucherNo = 1;
            }



            VmAcc_Transaction vmAccTransaction = new VmAcc_Transaction();

            Acc_Transaction acctrns = new Acc_Transaction();
            acctrns.Date = DateTime.Today;
            vmAccTransaction.Acc_Transaction = acctrns;

            return View(vmAccTransaction);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_BankWithdrawCreate(VmAcc_Transaction a)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            a.Acc_Transaction.Active = true;
            a.Acc_Transaction.Editable = true;
            a.Acc_Transaction.IsFinal = false;
            a.Acc_Transaction.CreatedBy = user.User_User.ID;
            try
            {
                a.Acc_Transaction.VoucherNo = (from c in db.Acc_Transaction select c)
                    .Where(c => c.Date.Year == a.Acc_Transaction.Date.Year).Max(c => c.VoucherNo) + 1;
            }
            catch (Exception e)
            {
                a.Acc_Transaction.VoucherNo = 1;
            }
            var id = await Task.Run(() => a.Add(UserID));

            return RedirectToAction("VmAcc_TransactionIndex");
        }
        public async Task<ActionResult> VmAcc_BankWithdrawEdit(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameBankDr();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;

            VmAcc_Transaction VmAcc_Transaction = new VmAcc_Transaction();
            await Task.Run(() => VmAcc_Transaction.SelectSingleAcc_Transaction(id));
            return View(VmAcc_Transaction);
        }
        public async Task<ActionResult> VmAcc_CashWithdrawEdit(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCashDr();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;

            VmAcc_Transaction VmAcc_Transaction = new VmAcc_Transaction();
            await Task.Run(() => VmAcc_Transaction.SelectSingleAcc_Transaction(id));
            return View(VmAcc_Transaction);
        }
        public async Task<ActionResult> VmAcc_BankDepositEdit(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameBankDr();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;

            VmAcc_Transaction VmAcc_Transaction = new VmAcc_Transaction();
            await Task.Run(() => VmAcc_Transaction.SelectSingleAcc_Transaction(id));
            return View(VmAcc_Transaction);
        }
        public async Task<ActionResult> VmAcc_CashDepositEdit(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCr();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameCashDr();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;

            VmAcc_Transaction VmAcc_Transaction = new VmAcc_Transaction();
            await Task.Run(() => VmAcc_Transaction.SelectSingleAcc_Transaction(id));
            return View(VmAcc_Transaction);
        }
        public async Task<ActionResult> VmAcc_FundTransferEdit(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameFundTransfer();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameFundTransfer();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;

            VmAcc_Transaction VmAcc_Transaction = new VmAcc_Transaction();
            await Task.Run(() => VmAcc_Transaction.SelectSingleAcc_Transaction(id));
            return View(VmAcc_Transaction);
        }

        public async Task<ActionResult> VmAcc_JournalEntryEdit(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameJournalEntry();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameDr = c2;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameJournalEntry();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_AcNameCr = c3;

            VmForDropDown.DDownData = VmForDropDown.GetAcc_CostPool();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Acc_CostPool = c4;

            VmAcc_Transaction VmAcc_Transaction = new VmAcc_Transaction();
            await Task.Run(() => VmAcc_Transaction.SelectSingleAcc_Transaction(id));
            return View(VmAcc_Transaction);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_BankWithdrawEdit(VmAcc_Transaction a)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            a.Acc_Transaction.EditedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Edit(UserID));
                return RedirectToAction("VmAcc_TransactionIndex");

            }
            return RedirectToAction("VmAcc_TransactionIndex");
        }
        public ActionResult VmAcc_AccTransactionDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Acc_Transaction b = new Acc_Transaction();
            b.ID = id;
            b.Active = false;
            var s = b.Delete(0);
            if (s)
            {
                VmAcc_IntegrationJournal vmAccIntegrationJournal = new VmAcc_IntegrationJournal
                {
                    TableRowIdFk = b.ID,
                    TableIdFk = VmAcc_Database_Table.TableName(typeof(Acc_Transaction).Name)
                };
                vmAccIntegrationJournal.DeleteJounalFromIntegratedTable();

                Acc_Notifier accNotifier = new Acc_Notifier
                {
                    RowIdFk = b.ID,
                    TableIdFk = VmAcc_Database_Table.TableName(typeof(Acc_Transaction).Name)
                };

                VmAcc_ChequeIntegration accChequeIntegration = new VmAcc_ChequeIntegration
                {
                    Acc_Notifier = accNotifier
                };
                accChequeIntegration.AddDelete(user.User_User.ID);
            }
            return RedirectToAction("VmAcc_TransactionIndex");
        }

        public async Task<ActionResult> VmAcc_AccTransactionFinalized(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            // to retrieve transaction data(payment/received)
            this.VmAcc_Transaction = new VmAcc_Transaction();
            await Task.Run(() => this.VmAcc_Transaction.SelectSingleAcc_Transaction(id));
            var checkno = "";
            if (!string.IsNullOrEmpty(VmAcc_Transaction.Acc_Transaction.ChequeNo))
            {
                checkno = ", Cheque No. :" + VmAcc_Transaction.Acc_Transaction.ChequeNo;
            }
            var description = "";
            if (!string.IsNullOrEmpty(VmAcc_Transaction.Acc_Transaction.Description))
            {
                description = ", Des :" + VmAcc_Transaction.Acc_Transaction.Description;
            }
            var checkdate = "";
            if (VmAcc_Transaction.Acc_Transaction.ChequeDate != null)
            {
                checkdate = ", Cheque Date :" + VmAcc_Transaction.Acc_Transaction.ChequeDate.ToString().Substring(0, 10);
            }
            // to initialize transaction data in journal model
            this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
            {
                UserId = this.UserID,
                Date = this.VmAcc_Transaction.Acc_Transaction.Date,
                CostPool = this.VmAcc_Transaction.Acc_Transaction.Acc_CostPoolFK,
                Title = this.VmAcc_Transaction.Acc_Transaction.TransactionType,
                FromAccNo = this.VmAcc_Transaction.Acc_Transaction.From_Acc_NameFK,
                ToAccNo = this.VmAcc_Transaction.Acc_Transaction.To_Acc_NameFK,
                Amount = this.VmAcc_Transaction.Acc_Transaction.Amount,
                Descriptions = "Voucher no: " + VmAcc_Transaction.Acc_Transaction.VoucherNo + checkno + checkdate + description,
                TableIdFk = VmAcc_Database_Table.TableName("Acc_Transaction"),
                TableRowIdFk = this.VmAcc_Transaction.Acc_Transaction.ID,
            };

            // call integration and receive boolean. if true data add in journal successfully else not added
            var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());

            // then check if true finalized your transaction otherwise not finalized
            if (!string.IsNullOrEmpty(journalstatus))
            {
                VmAcc_Transaction o = new VmAcc_Transaction();
                await Task.Run(() => o.ChangeFinaliseStatus(id));
            }


            if ((VmAcc_Transaction.Acc_Transaction.TransactionType == "Bank Withdraw" || VmAcc_Transaction.Acc_Transaction.TransactionType == "Fund Transfer") &&
                !string.IsNullOrEmpty(VmAcc_Transaction.Acc_Transaction.ChequeNo) && id > 0)
            {
                VmForDropDown vmForDropDown = new VmForDropDown();
                VmAccounting accounting = new VmAccounting();
                accounting.FromDate = new DateTime(2001, 1, 1);
                accounting.ToDate = DateTime.Today;

                accounting.GetLedgerAccountHead(VmAcc_Transaction.Acc_Transaction.From_Acc_NameFK, "");
                var frombalance = accounting.Transaction.Any() ? accounting.Transaction[accounting.Transaction.Count() - 1].Balance : 0;
                var frominfo = db.Acc_AcName.Find(VmAcc_Transaction.Acc_Transaction.From_Acc_NameFK);

                accounting.GetLedgerAccountHead(VmAcc_Transaction.Acc_Transaction.To_Acc_NameFK, "");
                var tobalance = accounting.Transaction.Any() ? accounting.Transaction[accounting.Transaction.Count() - 1].Balance : 0;
                var toinfo = db.Acc_AcName.Find(VmAcc_Transaction.Acc_Transaction.To_Acc_NameFK);

                Acc_Notifier accNotifier = new Acc_Notifier
                {
                    Type = (int)Acc_NotifierType.Cheque,
                    Amount = VmAcc_Transaction.Acc_Transaction.Amount,
                    FromBalance = frombalance,
                    ChequeNo = VmAcc_Transaction.Acc_Transaction.ChequeNo,
                    ToBalance = tobalance,
                    FromName = frominfo.Name,
                    ToName = toinfo.Name,
                    Particulars = $@"Chq No:{VmAcc_Transaction.Acc_Transaction.ChequeNo} || A/C - {frominfo.Name}",
                    TableIdFk = VmAcc_Database_Table.TableName(VmAcc_Transaction.Acc_Transaction.GetType().Name),
                    RowIdFk = id,
                    FromFk = VmAcc_Transaction.Acc_Transaction.From_Acc_NameFK,
                    ToFk = VmAcc_Transaction.Acc_Transaction.To_Acc_NameFK,
                    IsApproved = 1,
                    Date = DateTime.Now
                };

                this.VmAcc_ChequeIntegration = new VmAcc_ChequeIntegration
                {
                    UserId = this.UserID,
                    Acc_Notifier = accNotifier
                };
                // call integration and receive boolean. if true data add in journal successfully else not added
                await Task.Run(() => this.VmAcc_ChequeIntegration.AddCheque());
            }


            return RedirectToAction("VmAcc_TransactionIndex");

        }
        public async Task<ActionResult> VmAcc_FinamizedTransactionIndex(FormCollection formCollection)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var date = formCollection["DateSelect"];
            if (!string.IsNullOrEmpty(date))
            {
                ViewBag.TransactionDate = date;

                VmAcc_Transaction VmAcc_Transaction = new VmAcc_Transaction();
                VmAcc_Transaction.Date = DateTime.ParseExact(date, "yyyy/M/d", null);
                await Task.Run(() => VmAcc_Transaction.FinalizedDataLoad());
                return View(VmAcc_Transaction);
            }
            else
            {
                var transactionDate = DateTime.Now;
                ViewBag.TransactionDate = transactionDate.ToString("yyyy/M/d");

                VmAcc_Transaction VmAcc_Transaction = new VmAcc_Transaction();
                VmAcc_Transaction.Date = DateTime.ParseExact(transactionDate.ToString("yyyy/M/d"), "yyyy/M/d", null);
                await Task.Run(() => VmAcc_Transaction.FinalizedDataLoad());
                return View(VmAcc_Transaction);

            }
        }

        public async Task<ActionResult> VmAcc_AccTransactionReport(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_Transaction accTransaction = new VmAcc_Transaction();
            await Task.Run(() => accTransaction.SingleDataLoad(id));
            return View(accTransaction);

        }
        public async Task<ActionResult> VmAcc_AccExportRealisationIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_AccExportRealisation accAccExportRealisation = new VmAcc_AccExportRealisation();
            await Task.Run(() => accAccExportRealisation.InitialDataLoad());
            return View(accAccExportRealisation);

        }

        public ActionResult VmAcc_AccExportRealisationCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetUDDropDown();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UdNo = c3;


            VmForDropDown.DDownData = VmForDropDown.GetMasterLCByUDNo();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MasterLC = c4;

            VmForDropDown.DDownData = VmForDropDown.GetBillOfExchangeByMasterLC();
            SelectList c5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BillOfExchange = c5;

            VmAcc_AccExportRealisation accTransaction = new VmAcc_AccExportRealisation();
            accTransaction.Acc_ExportRealisation = new Acc_ExportRealisation();
            accTransaction.Acc_ExportRealisation.Date = DateTime.Now;
            accTransaction.Acc_ExportRealisation.ValueDate = DateTime.Now;
            return View(accTransaction);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_AccExportRealisationCreate(VmAcc_AccExportRealisation a)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_AccExportRealisation accTransaction = new VmAcc_AccExportRealisation();
            accTransaction.Acc_ExportRealisation = a.Acc_ExportRealisation;
            accTransaction.Acc_ExportRealisation.IsFinal = false;

            await Task.Run(() => accTransaction.Add(UserID));
            return RedirectToAction("VmAcc_AccExportRealisationIndex");
        }
        public ActionResult VmAcc_AccExportRealisationEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetUDDropDown();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UdNo = c3;

            VmForDropDown.DDownData = VmForDropDown.GetMasterLCByUDNo();
            SelectList c4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.MasterLC = c4;

            VmForDropDown.DDownData = VmForDropDown.GetBillOfExchangeByMasterLC();
            SelectList c5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BillOfExchange = c5;

            VmAcc_AccExportRealisation accTransaction = new VmAcc_AccExportRealisation();
            accTransaction.SingleDataLoad(id);
            accTransaction.Commercial_UDSlave = accTransaction.SingleData.Commercial_UDSlave;
            accTransaction.Commercial_MasterLC = accTransaction.SingleData.Commercial_MasterLC;
            accTransaction.Acc_ExportRealisation = accTransaction.SingleData.Acc_ExportRealisation;
            return View(accTransaction);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_AccExportRealisationEdit(VmAcc_AccExportRealisation a)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_AccExportRealisation accTransaction = new VmAcc_AccExportRealisation();
            accTransaction.Acc_ExportRealisation = a.Acc_ExportRealisation;
            var s = await Task.Run(() => accTransaction.Edit(UserID));
            return RedirectToAction("VmAcc_AccExportRealisationIndex");
        }
        public async Task<ActionResult> VmAcc_AccExportRealisationDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Acc_ExportRealisation b = new Acc_ExportRealisation();
            b.ID = id;
            var s = await Task.Run(() => b.Delete(0));

            return RedirectToAction("VmAcc_AccExportRealisationIndex");
        }
        public async Task<ActionResult> VmAcc_AccExportRealisationFinalized(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_AccExportRealisation accTransaction = new VmAcc_AccExportRealisation();
            accTransaction.SingleDataLoad(id);
            accTransaction.Acc_ExportRealisation = accTransaction.SingleData.Acc_ExportRealisation;
            var s = await Task.Run(() => accTransaction.Finalized(UserID));
            if (s)
            {
                // journal integration
                try
                {
                    var tableidFk = VmAcc_Database_Table.TableName(accTransaction.Acc_ExportRealisation.GetType().Name);
                    try
                    {
                        db = new xOssContext();
                        var accExistJournal = db.Acc_Journal.Where(x => x.TableIdFk == tableidFk && x.TableRowIdFk == accTransaction.Acc_ExportRealisation.ID);

                        foreach (var accJournal in accExistJournal)
                        {
                            db = new xOssContext();
                            List<Acc_JournalSlave> journalslave = db.Acc_JournalSlave.Where(x => x.Acc_JournalFK == accJournal.ID).ToList();
                            foreach (Acc_JournalSlave accJournalSlave in journalslave)
                            {
                                accJournalSlave.Delete(UserID);
                            }
                            accJournal.Delete(UserID);
                        }

                    }
                    catch (Exception e)
                    {
                        //
                    }

                    int toAccExportProcessIncome = 0;
                    int toAccFcbPar = 519;
                    int toAccErq = 0;
                    int toAccFxTrading = 0;
                    int toAccPcAdjust = 0;
                    int toAccTimeLoad = 0;
                    int toAccBuyCommission = 0;
                    int toAccFdr = 0;
                    int toAccCurierExp = 0;
                    int toAccRmg = 0;
                    int toAccAit = 0;
                    int toAccSod = 0;
                    int toAccCd = 0;
                    int toAccbillpurchase = 0;
                    int toAccothercharge = 0;
                    int toAccdochandalingchrg = 0;
                    if (accTransaction.SingleData.Commercial_MasterLC.Commercial_LienBankFK == 1002) //JAMUNA BANK LIMITED
                    {
                        toAccExportProcessIncome = 169;
                        toAccFcbPar = 519;
                        toAccErq = 150;
                        toAccFxTrading = 145;
                        toAccPcAdjust = 153;
                        toAccTimeLoad = 3359;
                        toAccBuyCommission = 427;
                        toAccFdr = 155;
                        toAccCurierExp = 162;
                        toAccRmg = 426;
                        toAccAit = 425;
                        toAccSod = 57;
                        toAccCd = 91;

                        toAccbillpurchase = 3424;
                        toAccothercharge = 3426;
                        toAccdochandalingchrg = 160;
                    }
                    else //SOUTHEAST BANK LTD
                    {
                        toAccExportProcessIncome = 168;
                        toAccFcbPar = 566;
                        toAccErq = 149;
                        toAccFxTrading = 431;
                        toAccPcAdjust = 154;
                        toAccTimeLoad = 3360;
                        toAccBuyCommission = 432;
                        toAccFdr = 156;
                        toAccCurierExp = 161;
                        toAccRmg = 436;
                        toAccAit = 435;
                        toAccSod = 57;
                        toAccCd = 167;

                        toAccbillpurchase = 3425;
                        toAccothercharge = 3427;
                        toAccdochandalingchrg = 433;
                    }

                    var commercial_ud = db.Commercial_UD.Find(accTransaction.SingleData.Commercial_UD.ID);
                    var accAcName = db.Acc_AcName.Find(toAccExportProcessIncome);
                    var dollarrate = accTransaction.SingleData.Acc_ExportRealisation.DollarValue;
                    //Export Proceed
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed",
                        FromAccNo = commercial_ud.Acc_AcNameFk,
                        ToAccNo = toAccExportProcessIncome,
                        CurrencyRate = dollarrate,
                        CurrencyType = 1,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.RealisedAmount,
                        Descriptions = "Export process from UD No " + commercial_ud.UdNo + " to " + accAcName.Name,
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Export Proceed

                    //FcbPar
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccFcbPar,
                        CurrencyRate = dollarrate,
                        CurrencyType = 1,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.FcbParAmt,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //FcbPar

                    //Erq
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccErq,
                        CurrencyRate = dollarrate,
                        CurrencyType = 1,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.ERQAmt,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Erq

                    //FxTrading
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccFxTrading,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.FxTrading,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //FxTrading

                    //PcAdjustment
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccPcAdjust,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.PcAdjust,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //PcAdjustment

                    //BuyingComm
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccBuyCommission,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.BuyingCommission,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //BuyingComm

                    //Fdr
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccFdr,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.FdrAmount,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Fdr

                    //Corier Exp
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccCurierExp,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.CourierExpense,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Corier Exp

                    //Rmg
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccRmg,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.RmgAmount,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Rmg

                    //Ait
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccAit,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.AitAmount,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Ait

                    //Sod
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccSod,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.SodAcAmount,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Sod

                    //Cd
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccCd,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.CdAmount,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //cd

                    //Time Loan
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccTimeLoad,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.PcAdjust,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Time Loan

                    //Bill Purchase
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccbillpurchase,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.BillPurchase,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Bill Purchase

                    //Others Bill
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccothercharge,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.Others,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Others Bill

                    //Doc Handaling
                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        Date = accTransaction.SingleData.Acc_ExportRealisation.Date,
                        Title = "Export Proceed Expense",
                        FromAccNo = toAccExportProcessIncome,
                        ToAccNo = toAccdochandalingchrg,
                        Amount = accTransaction.SingleData.Acc_ExportRealisation.DocHandlingCharge,
                        Descriptions = "Export process Expense from Export Proceed ",
                        TableIdFk = tableidFk,
                        TableRowIdFk = accTransaction.SingleData.Acc_ExportRealisation.ID,
                    };
                    journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.AddJounalFromIntegratedTable());
                    //Doc Handaling

                    //if (!string.IsNullOrEmpty(journalstatus))
                    //{
                    //    db = new xOssContext();
                    //    var expensdata = (from t1 in db.Acc_ExportRealisationExpense.AsEnumerable()
                    //                      join t2 in db.Acc_AcName on t1.Acc_AcNameFK equals t2.ID
                    //                      where t1.Acc_ExportRealisationFK == accTransaction.Acc_ExportRealisation.ID
                    //                      select new
                    //                      {
                    //                          Expdata = t1,
                    //                          Acc_AcName = t2
                    //                      });
                    //    var tableid = VmAcc_Database_Table.TableName("Acc_ExportRealisationExpense");
                    //    foreach (var item in expensdata)
                    //    {
                    //        this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    //        {
                    //            UserId = this.UserID,
                    //            Date = accTransaction.Acc_ExportRealisation.Date,
                    //            Title = "Export Proceed Expense",
                    //            FromAccNo = toAcc,
                    //            ToAccNo = item.Expdata.Acc_AcNameFK,
                    //            Amount = (decimal)(item.Expdata.TotalAmount),
                    //            Descriptions = "Export process Expense from Sales " + accAcName.Name + " to " + item.Acc_AcName.Name,
                    //            TableIdFk = tableid,
                    //            TableRowIdFk = item.Expdata.ID,
                    //        };
                    //        journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());

                    //    }

                    //}
                }
                catch (Exception e)
                {

                }
            }
            return RedirectToAction("VmAcc_AccExportRealisationIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmAcc_AccExportRealisationExpenseIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetUDDropDown();
            ViewBag.UDList = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmAcc_AccExportRealisation accAccExportRealisation = new VmAcc_AccExportRealisation();
            accAccExportRealisation.VmFilter = new VmFilter();

            await Task.Run(() => accAccExportRealisation.InitialDataLoadByFilter(accAccExportRealisation.VmFilter));
            return View(accAccExportRealisation);
        }

        [HttpPost]
        public async Task<ActionResult> VmAcc_AccExportRealisationExpenseIndex(VmAcc_AccExportRealisation accAccExportRealisation)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetUDDropDown();
            ViewBag.UDList = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            await Task.Run(() => accAccExportRealisation.InitialDataLoadByFilter(accAccExportRealisation.VmFilter));

            return View(accAccExportRealisation);
        }
        public ActionResult VmAcc_AccExportRealisationExpenseCreate(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameExportRealisationExpName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ExportRealisationExpName = c3;

            this.accTransactionExpense = new VmAcc_AccExportRealisationExpense();
            this.accTransactionExpense.Acc_ExportRealisationExpense = new Acc_ExportRealisationExpense();
            this.accTransactionExpense.Acc_ExportRealisationExpense.Acc_ExportRealisationFK = id.Value;
            return View(accTransactionExpense);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_AccExportRealisationExpenseCreate(VmAcc_AccExportRealisationExpense a)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var s = await Task.Run(() => a.Add(UserID));

            return RedirectToAction("VmAcc_AccExportRealisationExpenseIndex", new { id = a.Acc_ExportRealisationExpense.Acc_ExportRealisationFK });
        }
        public ActionResult VmAcc_AccExportRealisationExpenseEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAcc_AcNameExportRealisationExpName();
            SelectList c3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ExportRealisationExpName = c3;

            this.accTransactionExpense = new VmAcc_AccExportRealisationExpense();
            this.accTransactionExpense.SingleDataLoad(id);
            this.accTransactionExpense.Acc_ExportRealisationExpense = accTransactionExpense.SingleData.Acc_ExportRealisationExpense;

            return View(accTransactionExpense);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmAcc_AccExportRealisationExpenseEdit(VmAcc_AccExportRealisationExpense a)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            this.accTransactionExpense = new VmAcc_AccExportRealisationExpense();
            this.accTransactionExpense.Acc_ExportRealisationExpense = a.Acc_ExportRealisationExpense;
            var s = await Task.Run(() => a.Edit(UserID));

            return RedirectToAction("VmAcc_AccExportRealisationExpenseIndex", new { id = a.Acc_ExportRealisationExpense.Acc_ExportRealisationFK });
        }
        public async Task<ActionResult> VmAcc_AccExportRealisationExpenseDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            this.accTransactionExpense = new VmAcc_AccExportRealisationExpense();
            this.accTransactionExpense.SingleDataLoad(id);
            this.accTransactionExpense.Acc_ExportRealisationExpense = accTransactionExpense.SingleData.Acc_ExportRealisationExpense;
            var s = await Task.Run(() => this.accTransactionExpense.Delete(UserID));

            return RedirectToAction("VmAcc_AccExportRealisationExpenseIndex", new { id = accTransactionExpense.SingleData.Acc_ExportRealisationExpense.Acc_ExportRealisationFK });
        }


        public async Task<ActionResult> AccountsHeadIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_AcName = new VmAcc_AcName();

            await Task.Run(() => VmAcc_AcName.AccountTypeWiseInventory());
            return View(VmAcc_AcName);
        }

        public async Task<ActionResult> AccountsChartOneIndex(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_AcName = new VmAcc_AcName();
            await Task.Run(() => VmAcc_AcName = VmAcc_AcName.SelectSingleAccountType(id));
            await Task.Run(() => VmAcc_AcName.AccountChart1WiseInventory(id));
            return View(VmAcc_AcName);
        }

        public async Task<ActionResult> AccountsChartTwoIndex(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_AcName = new VmAcc_AcName();
            await Task.Run(() => VmAcc_AcName = VmAcc_AcName.SelectSingleChartOne(id));
            await Task.Run(() => VmAcc_AcName.AccountChart2WiseInventory(id));
            return View(VmAcc_AcName);
        }

        public async Task<ActionResult> AccountsNameIndex(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_AcName = new VmAcc_AcName();
            await Task.Run(() => VmAcc_AcName = VmAcc_AcName.SelectSingleAccountsName(id));
            await Task.Run(() => VmAcc_AcName.AccountNameWiseInventory(id));
            return View(VmAcc_AcName);
        }
        public async Task<ActionResult> SupplierBillIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_SupplierBill = new VmAcc_SupplierBill();
            await Task.Run(() => VmAcc_SupplierBill.InitialDataLoad());
            return View(VmAcc_SupplierBill);
        }
        public async Task<ActionResult> SupplierBillCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetSupplerBillType();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.SupplerBillType = c1;

            VmForDropDown.DDownData = VmForDropDown.GetSupplerCnfTransport();
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CommonSupplier = c2;

            VmAcc_SupplierBill = new VmAcc_SupplierBill();
            Acc_SupplierBill accSupplierBill = new Acc_SupplierBill();
            accSupplierBill.Date = DateTime.Today;
            VmAcc_SupplierBill.Acc_SupplierBill = accSupplierBill;

            return await Task.Run(() => View(VmAcc_SupplierBill));
        }
        [HttpPost]
        public async Task<ActionResult> SupplierBillCreate(VmAcc_SupplierBill x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            x.Acc_SupplierBill.IsApproved = false;
            x.Acc_SupplierBill.CreatedBy = user.User_User.ID;
            await Task.Run(() => x.Add(user.User_User.ID));

            return RedirectToAction("SupplierBillIndex");
        }
        public async Task<ActionResult> SupplierBillEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            VmAcc_SupplierBill = new VmAcc_SupplierBill();
            await Task.Run(() => VmAcc_SupplierBill.SelectSingleAcc_SupplierBill(id));

            int suppliertype = 0;
            if (VmAcc_SupplierBill.Acc_SupplierBill.Common_BillTypeFk == 1)
            {
                suppliertype = 2;
            }
            else
            {
                suppliertype = 3;
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetSupplerBillType();
            SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.SupplerBillType = c1;

            VmForDropDown.DDownData = VmForDropDown.GetSupplerCnfTransportById(suppliertype);
            SelectList c2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CommonSupplier = c2;

            return View(VmAcc_SupplierBill);
        }

        [HttpPost]
        public async Task<ActionResult> SupplierBillEdit(VmAcc_SupplierBill x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            x.Acc_SupplierBill.IsApproved = false;
            x.Acc_SupplierBill.EditedBy = user.User_User.ID;
            await Task.Run(() => x.Edit(user.User_User.ID));

            return RedirectToAction("SupplierBillIndex");
        }

        public async Task<ActionResult> SupplierBillDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Acc_SupplierBill b = new Acc_SupplierBill();
            b.ID = id;
            await Task.Run(() => b.Delete(0));

            return RedirectToAction("SupplierBillIndex");
        }

        public async Task<ActionResult> SupplierBillSlaveIndex(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_SupplierBillSalve = new VmAcc_SupplierBillSlave();
            VmAcc_SupplierBillSalve.SupplierBillId = id;
            await Task.Run(() => VmAcc_SupplierBillSalve.InitialDataLoad());
            // await Task.Run(() => VmAcc_SupplierBillSalve.SelectSingleAcc_SupplierBill());
            return View(VmAcc_SupplierBillSalve);
        }
        public async Task<ActionResult> SupplierBillSlaveCreate(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmAcc_SupplierBillSalve = new VmAcc_SupplierBillSlave();
            VmAcc_SupplierBillSalve.SelectSingleAcc_SupplierBillSlave(id);

            VmForDropDown = new VmForDropDown();
            if (VmAcc_SupplierBillSalve.Acc_SupplierBill.Common_BillTypeFk == 1)
            {
                VmForDropDown.DDownData = VmForDropDown.GetInvoiceList();
                SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.GetInvoiceList = c1;
            }
            else if (VmAcc_SupplierBillSalve.Acc_SupplierBill.Common_BillTypeFk == 2)
            {
                VmForDropDown.DDownData = VmForDropDown.GetChallanList();
                SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.GetInvoiceList = c1;
            }
            else if (VmAcc_SupplierBillSalve.Acc_SupplierBill.Common_BillTypeFk == 3)
            {
                VmForDropDown.DDownData = VmForDropDown.GetExpenseBillList();
                SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.GetInvoiceList = c1;
            }

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            Acc_SupplierBillSlave accSupplierBillSlave = new Acc_SupplierBillSlave();
            accSupplierBillSlave.Acc_SupplierBillFk = id;
            accSupplierBillSlave.Date = DateTime.Now;
            VmAcc_SupplierBillSalve.Acc_SupplierBillSlave = accSupplierBillSlave;

            return await Task.Run(() => View(VmAcc_SupplierBillSalve));
        }

        [HttpPost]
        public async Task<ActionResult> SupplierBillSlaveCreate(VmAcc_SupplierBillSlave x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            x.Acc_SupplierBillSlave.IsApproved = false;
            x.Acc_SupplierBillSlave.CreatedBy = user.User_User.ID;
            await Task.Run(() => x.Add(user.User_User.ID));

            return RedirectToAction("SupplierBillSlaveIndex", new { id = x.Acc_SupplierBillSlave.Acc_SupplierBillFk });
        }
        public async Task<ActionResult> SupplierBillSlaveEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            var data = db.Acc_SupplierBillSlave.Find(id);


            VmAcc_SupplierBillSalve = new VmAcc_SupplierBillSlave();
            VmAcc_SupplierBillSalve.Acc_SupplierBillSlave = data;
            VmAcc_SupplierBillSalve.SelectSingleAcc_SupplierBillSlave(data.Acc_SupplierBillFk);

            VmForDropDown = new VmForDropDown();
            if (VmAcc_SupplierBillSalve.Acc_SupplierBill.Common_BillTypeFk == 1)
            {
                VmForDropDown.DDownData = VmForDropDown.GetInvoiceList();
                SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.GetInvoiceList = c1;
            }
            else if (VmAcc_SupplierBillSalve.Acc_SupplierBill.Common_BillTypeFk == 2)
            {
                VmForDropDown.DDownData = VmForDropDown.GetChallanList();
                SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.GetInvoiceList = c1;
            }
            else
            {
                VmForDropDown.DDownData = VmForDropDown.GetExpenseBillList();
                SelectList c1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                ViewBag.GetInvoiceList = c1;
            }

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            return await Task.Run(() => View(VmAcc_SupplierBillSalve));
        }
        [HttpPost]
        public async Task<ActionResult> SupplierBillSlaveEdit(VmAcc_SupplierBillSlave x)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            x.Acc_SupplierBillSlave.EditedBy = user.User_User.ID;
            await Task.Run(() => x.Edit(user.User_User.ID));

            return RedirectToAction("SupplierBillSlaveIndex", new { id = x.Acc_SupplierBillSlave.Acc_SupplierBillFk });
        }
        public async Task<ActionResult> SupplierBillSlaveDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Acc_SupplierBillSlave b = new Acc_SupplierBillSlave();
            b.ID = id;
            await Task.Run(() => b.Delete(user.User_User.ID));

            var data = db.Acc_SupplierBillSlave.Find(id);

            return RedirectToAction("SupplierBillSlaveIndex", new { id = data.Acc_SupplierBillFk });
        }
        public async Task<ActionResult> SupplierBillSlaveApproved(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var data = db.Acc_SupplierBillSlave.Find(id);
            VmAcc_SupplierBillSlave accSupplierBillSlave = new VmAcc_SupplierBillSlave();
            accSupplierBillSlave.Acc_SupplierBillSlave = data;
            accSupplierBillSlave.Acc_SupplierBillSlave.IsApproved = true;
            accSupplierBillSlave.Acc_SupplierBillSlave.ApprovedDate = DateTime.Now;
            accSupplierBillSlave.Acc_SupplierBillSlave.ApprovedBy = user.User_User.ID;
            var s = await Task.Run(() => accSupplierBillSlave.Edit(user.User_User.ID));
            if (s)
            {
                // journal integration
                try
                {
                    var accSupplierBill = db.Acc_SupplierBill.Find(accSupplierBillSlave.Acc_SupplierBillSlave.Acc_SupplierBillFk); ;
                    var common_Supplier = db.Common_Supplier.Find(accSupplierBill.Common_SupplierFk);

                    var chqno = "";
                    var title = "";
                    var description = "";
                    int to = 0;

                    if (accSupplierBill.Common_BillTypeFk == 1)
                    {
                        db.Commercial_Invoice.Find(accSupplierBillSlave.Acc_SupplierBillSlave.ChallanOrInvoiceFk);

                        var invoice = db.Commercial_Invoice.Find(accSupplierBillSlave.Acc_SupplierBillSlave.ChallanOrInvoiceFk);
                        title = "C&F Bill";
                        if (invoice != null)
                        {
                            description = "Invoice No: " + invoice.InvoiceNo + ". From " + common_Supplier.Name;
                        }
                        else
                            description = "Invoice No: " + accSupplierBillSlave.Acc_SupplierBillSlave.ChallanInvoiceNo + ". From " + common_Supplier.Name;
                        to = 1114;
                    }
                    else if (accSupplierBill.Common_BillTypeFk == 2)
                    {
                        var challan = db.Shipment_CNFDelivaryChallan.Find(accSupplierBillSlave.Acc_SupplierBillSlave.ChallanOrInvoiceFk);
                        title = "Transport Bill";
                        if (challan != null)
                        {
                            description = "Challan No: " + challan.ChallanNo + ". From " + common_Supplier.Name;
                        }
                        else
                            description = "Challan No: " + accSupplierBillSlave.Acc_SupplierBillSlave.ChallanInvoiceNo + ". From " + common_Supplier.Name;
                        to = 128;
                    }
                    else if (accSupplierBill.Common_BillTypeFk == 3)
                    {
                        var challan = db.Acc_AcName.Find(accSupplierBillSlave.Acc_SupplierBillSlave.ChallanOrInvoiceFk);
                        title = "Others Bill";
                        description = challan.Name + "'s bill No: " + accSupplierBillSlave.Acc_SupplierBillSlave.ChallanInvoiceNo + ". From " + common_Supplier.Name;
                        to = accSupplierBillSlave.Acc_SupplierBillSlave.ChallanOrInvoiceFk;
                    }


                    this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                    {
                        UserId = user.User_User.ID,
                        CostPool = 1,
                        Date = accSupplierBillSlave.Acc_SupplierBillSlave.Date,
                        Title = title,
                        CurrencyType = accSupplierBillSlave.Acc_SupplierBillSlave.Common_CurrencyFK,
                        CurrencyRate = accSupplierBillSlave.Acc_SupplierBillSlave.DollarRate,
                        FromAccNo = common_Supplier.Acc_AcNameFk,
                        ToAccNo = to,
                        Amount = (decimal)(accSupplierBillSlave.Acc_SupplierBillSlave.Amount),
                        Descriptions = description,
                        TableIdFk = VmAcc_Database_Table.TableName(accSupplierBillSlave.Acc_SupplierBillSlave.GetType().Name),
                        TableRowIdFk = accSupplierBillSlave.Acc_SupplierBillSlave.ID,
                    };
                    var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
                }
                catch (Exception e)
                {

                }
            }
            return RedirectToAction("SupplierBillSlaveIndex", new { id = data.Acc_SupplierBillFk });
        }
        public ActionResult NotifierStatus()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmNotifier x = new VmNotifier();

            x.MirrorDataLoad();

            x.VmCommercial_UD = new VmCommercial_UD();
            //x.VmCommercial_UD.InitialDataLoad();
            return View(x);
        }


        #region DollarRate

        public async Task<ActionResult> Acc_DollarRate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_DollarRate = new VmAcc_DollarRate();
            await Task.Run(() => VmAcc_DollarRate.InitialDataLoad());

            return View(VmAcc_DollarRate);
        }

        public ActionResult Acc_DollarRateCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_DollarRate = new VmAcc_DollarRate();
            return View(VmAcc_DollarRate);
        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Acc_DollarRateCreate(VmAcc_DollarRate vmAcc_DollarRate)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            vmAcc_DollarRate.Acc_DollarRate.EffectDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                await Task.Run(() => vmAcc_DollarRate.Add(UserID));
                return RedirectToAction("Acc_DollarRate");
            }
            return RedirectToAction("Acc_DollarRate");
        }

        [HttpGet]
        public async Task<ActionResult> Acc_DollarRateEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmAcc_DollarRate = new VmAcc_DollarRate();
            await Task.Run(() => VmAcc_DollarRate.SelectSingle(id.Value));

            return View(VmAcc_DollarRate);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Acc_DollarRateEdit(VmAcc_DollarRate VmAcc_DollarRate)
        {
            VmUser_User user = (VmUser_User)Session["One"];

            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmAcc_DollarRate.Edit(0));
                return RedirectToAction("Acc_DollarRate");
            }
            return View(VmAcc_DollarRate);
        }

        public ActionResult Acc_DollarRateDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Acc_DollarRate b = new Acc_DollarRate();
            b.ID = id;
            b.Delete(0);

            return RedirectToAction("Acc_DollarRate");
        }

        #endregion
    }
}
