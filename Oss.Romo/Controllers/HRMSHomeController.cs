﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models;
using Oss.Romo.Models.Entity.User;
using Oss.Romo.Models.Services;
using Oss.Romo.Models.ViewModels;
using Oss.Romo.Models.ViewModels.HRMS;
using Oss.Romo.ViewModels.User;

namespace Oss.Romo.Controllers
{
    //public class HRMSHomeController : Controller
    //{
    //    // GET: HRMSHome
    //    public ActionResult Index()
    //    {
    //        return View();
    //    }
    //}
    public class HRMSHomeController : Controller
    {
        private xOssContext db = new xOssContext();
        public ActionResult Index()
        {
            // return View();
            return RedirectToAction("Login");
        }
        public HRMSHomeController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        public ActionResult Login()
        {
            HttpCookie currentUserCookie = HttpContext.Request.Cookies["UserLogin"];
            HttpContext.Response.Cookies.Remove("UserLogin");
            return View();
        }
        [HttpPost]
        public ActionResult Login(VM_User user)
        {
            if (user.UserName == null || user.Password == null)
            {
                user.LblMessage = "Please enter Username and Password";
                return View(user);
            }

            bool Athourised = false;
            int theId = 0;
            theId = user.LoginUser1(Athourised);
            if (theId > 0)
            {
                CurrentUser cu = new CurrentUser { ID = theId, UserName = user.UserName, Password = user.Password };
                Session["User"] = cu;
                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("Name", cu.UserName);
                cookie.Values.Add("Password", cu.Password);
                cookie.Expires = DateTime.Now.AddMinutes(300);
                Request.Cookies.Add(cookie);

                Response.Cookies.Add(cookie);
                return RedirectToAction("Index", "HRMSEmployeeRecord");
            }
            else
            {
                user.LblMessage = "User/Password does not match!";
            }
            return View(user);
        }

        public ActionResult SignOut()
        {

            Oss.Romo.Models.Services.CurrentUser user = (Oss.Romo.Models.Services.CurrentUser)Session["User"];
            if (user != null)
            {
                Session["User"] = null;
                HttpCookie currentUserCookie = HttpContext.Request.Cookies["UserLogin"];
                HttpContext.Response.Cookies.Remove("UserLogin");
            }

            return RedirectToAction("Login", "Home");
        }


        //public int LoginUser(bool authorised)
        //{
        //    db = new xOssContext();
        //    int flag = 0;
        //    var v = (from t1 in db.Users
        //        where t1.Status == true && t1.UserName == this.UserName && t1.Password == this.Password)
        //        select new VM_User()
        //        {
        //            ID = o.ID,
        //            UserName = o.UserName,
        //            Password = o.Password
        //        }).FirstOrDefault();
        //    if (v != null)
        //    {
        //        flag = v.ID;
        //        //User.ID=flag
        //        //this.User.ID = flag;
        //        this.User.UserName = v.UserName;
        //        this.User.Password = v.Password;
        //    }
        //    if (flag > 0)
        //    {
        //        if (v.Status == false)
        //        {
        //            this.LblMessage = "You are not authorised!";
        //            return 0;
        //        }
        //    }
        //    else
        //    {
        //        this.LblMessage = "User / Password does not match!";
        //    }
        //    return flag;
        //}

    }
}