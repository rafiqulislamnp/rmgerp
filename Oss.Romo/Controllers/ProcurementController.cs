﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Planning;
using Oss.Romo.ViewModels.Procurement;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.ViewModels.Store;
using Oss.Romo.ViewModels.User;

namespace Oss.Romo.Controllers
{
    [SoftwareAdmin]
    public class ProcurementController : Controller
    {
        private xOssContext db = new xOssContext();
        VmControllerHelper VmControllerHelper;
        VmProc_Requisition VmProc_Requisition;
        VmProc_Purchase VmProc_Purchase;
        VmProd_Requisition_Slave VmProd_Requisition_Slave;
        VmProd_Requisition VmProd_Requisition;
        VmMkt_PO VmMkt_PO;
        VmMktPO_Slave VmMktPO_Slave;
        VmForDropDown VmForDropDown;
        Int32 UserID = 0;

        public ProcurementController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        #region Requisitaion

        //******************Requisitaion Index********************
        public async Task<ActionResult> VmProc_RequisitaionIndex(int? id)
        {
            VmUser_User u = (VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProc_Requisition = new VmProc_Requisition();            
            await Task.Run(() => VmProc_Requisition.InitialDataLoad());
            return View(VmProc_Requisition);
        }


        //******************Create New Requisitaion********************
        public ActionResult VmProc_RequisitionCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStoreType();
            SelectList storeType = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.StoreType = storeType;


            VmForDropDown.DDownData = VmForDropDown.GetInternalStore();
            SelectList internalStore = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.InternalStore = internalStore;

            VmForDropDown.DDownData = VmForDropDown.GetPriority();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Priority = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetDepartment();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Department = sl4;

            ViewBag.DepartmentId = u.User_User.User_DepartmentFK;
            VmProd_Requisition = new VmProd_Requisition();
            VmProd_Requisition.Prod_Requisition = new Prod_Requisition();
            VmProd_Requisition.Prod_Requisition.Date = DateTime.Now;

            return View(VmProd_Requisition);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_RequisitionCreate(VmProd_Requisition VmProd_Requisition)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Requisition.Prod_Requisition.FromUser_DeptFK = user.User_User.User_DepartmentFK;

            VmProd_Requisition.Prod_Requisition.Internal = true;
            if (VmProd_Requisition.InternalValue != 1)
            {
                VmProd_Requisition.Prod_Requisition.Internal = false;
                VmProd_Requisition.Prod_Requisition.ToUser_DeptFK = 7;
            }


            VmProd_Requisition.Prod_Requisition.Mkt_BOMFK = 1;
            await Task.Run(() => VmProd_Requisition.Add(UserID));
            return RedirectToAction("VmProc_RequisitaionIndex");

        }


        //******************Get Cascading Data for Create Requisitaion********************
        public JsonResult Get_SubCategory(int? categoryid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_SubCategory.Where(p => p.Raw_CategoryFK == categoryid).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get_Item(int? subcategoryid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_Item.Where(p => p.Raw_SubCategoryFK == subcategoryid).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }

        //******************Edit Requisitaion********************
        [HttpGet]
        public async Task<ActionResult> VmProc_RequisitionEdit(int? id)
        {

            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPriority();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Priority = sl1;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetDepartment();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Department = sl4;

            VmProd_Requisition = new VmProd_Requisition();
            await Task.Run(() => VmProd_Requisition.SelectSingle(id.Value));

            if (VmProd_Requisition.Prod_Requisition == null)
            {
                return HttpNotFound();
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(VmProd_Requisition);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_RequisitionEdit(VmProd_Requisition VmProd_Requisition)
        {
            if (ModelState.IsValid)
            {

                await Task.Run(() => VmProd_Requisition.Edit(UserID));
                return RedirectToAction("VmProc_RequisitaionIndex");
            }
            return View(VmProd_Requisition);
        }

        //******************Delete Requisitaion********************
        public ActionResult VmProc_RequisitionDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Prod_Requisition b = new Prod_Requisition();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmProc_RequisitaionIndex");
        }

        #endregion

        #region Requisitaion Slave
        //******************Requisitaion Slave Index********************
        public async Task<ActionResult> VmProc_Requisition_SlaveIndex(int? id, bool dept = true)
        {
            VmUser_User u = (VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            decimal? total = 0;
            VmProc_Requisition = new VmProc_Requisition();
            await Task.Run(() => VmProc_Requisition = VmProc_Requisition.SelectSingleJoined(id.Value));
            VmProc_Requisition.VmProd_Requisition_Slave = new VmProd_Requisition_Slave();

            await Task.Run(() => VmProc_Requisition.VmProd_Requisition_Slave.GetRequisitionSlaveItem(id.Value));
            foreach (var x in VmProc_Requisition.VmProd_Requisition_Slave.DataList)
            {
                total += (x.Prod_Requisition_Slave.TotalRequired * x.Prod_Requisition_Slave.EstimatedPrice);
            }

            VmProc_Requisition.TotalEstimatedPrice = total;
            VmProc_Requisition.IsFromProduction = dept;
            return View(VmProc_Requisition);
        }

        //******************Create New Requisitaion Slave********************
        [HttpGet]
        public ActionResult VmProc_RequisitionSlaveExternalCreate(int id)
        {
            VmUser_User u = (VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAllRawCategory();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Category = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

           
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            SelectList order = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BomCID = order;

            VmProd_Requisition_Slave VmProd_Requisition_Slave = new VmProd_Requisition_Slave();
            VmProd_Requisition_Slave.Prod_Requisition_Slave = new Prod_Requisition_Slave();
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Prod_RequisitionFK = id;
            VmProd_Requisition_Slave.UserDepartmentId = u.User_User.User_DepartmentFK.Value;
            return View(VmProd_Requisition_Slave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_RequisitionSlaveExternalCreate(VmProd_Requisition_Slave x)
        {
            if (x.UserDepartmentId != 13)
            {
                x.Prod_Requisition_Slave.Mkt_BOMFK = 1;
            }           
            x.Prod_Requisition_Slave.Prod_TransitionItemInventoryFk = 1;
            if (x.Prod_Requisition_Slave.Mkt_POSlaveFK==null)
            {
                x.Prod_Requisition_Slave.Mkt_POSlaveFK = 1;
            }

            x.Prod_Requisition_Slave.AddReady(UserID);
            await Task.Run(() => x.Prod_Requisition_Slave.Add());
            return RedirectToAction("VmProc_Requisition_SlaveIndex/" + x.Prod_Requisition_Slave.Prod_RequisitionFK);

        }

        [HttpGet]
        public ActionResult VmProc_RequisitionSlaveInternalCreate(int id,int uId)
        {
            VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BomCID = sl1;

            VmProc_Requisition = new VmProc_Requisition();
            
            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();
            VmProd_Requisition_Slave.Prod_Requisition_Slave = new Prod_Requisition_Slave();
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Prod_RequisitionFK = id;
            VmProd_Requisition_Slave.Prod_Requisition = new Prod_Requisition();
            VmProd_Requisition_Slave.Prod_Requisition.FromUser_DeptFK = uId;
            return View(VmProd_Requisition_Slave);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_RequisitionSlaveInternalCreate(VmProd_Requisition_Slave VmProd_Requisition_Slave)
        {
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Prod_TransitionItemInventoryFk = 1;
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Raw_ItemFK = 1;
            var s = await Task.Run(() => VmProd_Requisition_Slave.Add(UserID));
            return RedirectToAction("VmProc_Requisition_SlaveIndex/" + VmProd_Requisition_Slave.Prod_Requisition_Slave.Prod_RequisitionFK);
        }

        [HttpGet]
        public ActionResult VmProc_TransitionItemInventory(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");

            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetTransitionItemsWithoutInReqSlave();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.TransitionItems = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();
            VmProd_Requisition_Slave.Prod_Requisition_Slave = new Prod_Requisition_Slave();
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Prod_RequisitionFK = id;
            return View(VmProd_Requisition_Slave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_TransitionItemInventory(VmProd_Requisition_Slave VmProd_Requisition_Slave)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Consumption = 1;
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Common_UnitFK = 1;
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Raw_ItemFK = 1;
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Mkt_BOMFK = 1;
            VmProd_Requisition_Slave.Prod_Requisition_Slave.Mkt_POSlaveFK = 1;
            VmProd_Requisition_Slave.Prod_Requisition_Slave.EstimatedPrice = 0;
            var s = await Task.Run(() => VmProd_Requisition_Slave.Add(UserID));
            return RedirectToAction("VmProc_Requisition_SlaveIndex/" + VmProd_Requisition_Slave.Prod_Requisition_Slave.Prod_RequisitionFK);
        }



        //******************Edit Requisitaion Slave********************
        [HttpGet]
        public async Task<ActionResult> VmProc_Requisition_SlaveEdit(int? prSlaveId)
        {
            VmUser_User u = (VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (prSlaveId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();
            await Task.Run(() => VmProd_Requisition_Slave.SelectSingle(prSlaveId.Value));

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAllRawCategory();
            ViewBag.Category = new SelectList(VmForDropDown.DDownData, "Value", "Text");
                      
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            ViewBag.BomCID = new SelectList(VmForDropDown.DDownData, "Value", "Text");
           
            //VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDownasdf(VmProd_Requisition_Slave.Mkt_PO.Mkt_BOMFK);
            //ViewBag.ItemName = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            

            if (VmProd_Requisition_Slave.Prod_Requisition_Slave == null)
            {
                return HttpNotFound();
            }

            return View(VmProd_Requisition_Slave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_Requisition_SlaveEdit(Prod_Requisition_Slave Prod_Requisition_Slave)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => Prod_Requisition_Slave.Edit(0));
                return RedirectToAction("VmProc_Requisition_SlaveIndex/" + Prod_Requisition_Slave.Prod_RequisitionFK);
            }
            return View(VmProd_Requisition_Slave);
        }

        [HttpGet]
        public async Task<ActionResult> VmProc_RequisitionSlaveExternalEdit(int? prSlaveId)
        {
            VmUser_User u = (VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (prSlaveId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();
            await Task.Run(() => VmProd_Requisition_Slave.SelectSingle(prSlaveId.Value));
            VmProd_Requisition_Slave.UserDepartmentId = u.User_User.User_DepartmentFK.Value;

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetAllRawCategory();
            ViewBag.Category = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetRaw_SubCategoryForDropDown();
            ViewBag.SubCategory = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            ViewBag.RawItem = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            ViewBag.Unit = new SelectList(VmForDropDown.DDownData, "Value", "Text");            

            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            ViewBag.BomCID = new SelectList(VmForDropDown.DDownData, "Value", "Text");            

            //VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDownasdf(VmProd_Requisition_Slave.Mkt_PO.Mkt_BOMFK);
            //ViewBag.ItemName = new SelectList(VmForDropDown.DDownData, "Value", "Text");
           

            if (VmProd_Requisition_Slave.Prod_Requisition_Slave == null)
            {
                return HttpNotFound();
            }

            return View(VmProd_Requisition_Slave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_RequisitionSlaveExternalEdit(Prod_Requisition_Slave Prod_Requisition_Slave)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => Prod_Requisition_Slave.Edit(0));
                return RedirectToAction("VmProc_Requisition_SlaveIndex/" + Prod_Requisition_Slave.Prod_RequisitionFK);
            }
            return View(VmProd_Requisition_Slave);
        }

        #region UpdateInternalRequisitionSlave

        [HttpGet]
        public async Task<ActionResult> VmProc_RequisitionSlaveInternalEdit(int? prSlaveId)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            ViewBag.BomCID = new SelectList(VmForDropDown.DDownData, "Value", "Text");

           
            

            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();
            await Task.Run(() => VmProd_Requisition_Slave.SelectSingle(prSlaveId.Value));
            VmProd_Requisition_Slave.AvailableQty = 0;


            VmForDropDown.DDownData = VmForDropDown.GetStyleItemByBomPoSlave(VmProd_Requisition_Slave.Prod_Requisition_Slave.Mkt_BOMFK.Value);
            ViewBag.POItem = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            if (VmProd_Requisition_Slave.Prod_Requisition_Slave == null)
            {
                return HttpNotFound();
            }

            return View(VmProd_Requisition_Slave);
        }

        [HttpPost]
        public async Task<ActionResult> VmProc_RequisitionSlaveInternalEdit(VmProd_Requisition_Slave VmProd_Requisition_Slave)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            await Task.Run(() => VmProd_Requisition_Slave.Edit(UserID));

            return RedirectToAction("VmProc_Requisition_SlaveIndex/" + VmProd_Requisition_Slave.Prod_Requisition_Slave.Prod_RequisitionFK);
        }

        #endregion

        [HttpGet]
        public async Task<ActionResult> VmProc_TransitionItemInventoryEdit(int? prSlaveId)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetTransitionItems();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.TransitionItems = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();
            await Task.Run(() => VmProd_Requisition_Slave.SelectSingle(prSlaveId.Value));
            VmProd_Requisition_Slave.AvailableQty = 0;

            if (VmProd_Requisition_Slave.Prod_Requisition_Slave == null)
            {
                return HttpNotFound();
            }

            return View(VmProd_Requisition_Slave);
        }
        [HttpPost]
        public async Task<ActionResult> VmProc_TransitionItemInventoryEdit(VmProd_Requisition_Slave VmProd_Requisition_Slave)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            await Task.Run(() => VmProd_Requisition_Slave.Edit(UserID));

            return RedirectToAction("VmProc_Requisition_SlaveIndex/" + VmProd_Requisition_Slave.Prod_Requisition_Slave.Prod_RequisitionFK);
        }

        //******************Delete Requisitaion Slave********************
        public ActionResult VmProc_Requisition_SlaveDelete(string id)
        {
            VmUser_User u = (VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            int deletID = 0, RequisitionID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deletID);

            id = id.Replace(deletID.ToString() + "x", "");
            int.TryParse(id, out RequisitionID);
            Prod_Requisition_Slave b = new Prod_Requisition_Slave();
            b.ID = deletID;
            b.Delete(0);
            return RedirectToAction("VmProc_Requisition_SlaveIndex/" + RequisitionID);
        }

        #endregion

        #region  Autherization

        [HttpGet]
        public async Task<ActionResult> VmProc_Requisition_Autherize(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmProd_Requisition = new VmProd_Requisition();

            await Task.Run(() => VmProd_Requisition.SelectSingle(id.Value));

            if (VmProd_Requisition.Prod_Requisition == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_Requisition);
        }

        [HttpPost]
        public async Task<ActionResult> VmProc_Requisition_Autherize(VmProd_Requisition VmProd_Requisition)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_Requisition.Edit(0));
                return RedirectToAction("VmProc_RequisitaionIndex");
            }
            return View(VmProd_Requisition);
        }

        #endregion

        //public ActionResult PurchaseIndex()
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (u == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }

        //    VmForDropDown = new VmForDropDown();
        //    VmForDropDown.DDownData = VmForDropDown.GetRequisitionList();
        //    SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
        //    ViewBag.Requisition = sl1;
        //    return View();
        //}

        public async Task<ActionResult> VmProc_PurchaseIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProc_Purchase = new VmProc_Purchase();
            bool flag = false;
            VmProc_Purchase.Header = "Purchase Orders(In Progress)";
            VmProc_Purchase.ButtonName = "Close";
            if (id != null)
            {
                VmProc_Purchase.Header = "Purchase Orders(Complete)";
                VmProc_Purchase.ButtonName = "ReOpen";
            }
            await Task.Run(() => VmProc_Purchase.InitialDataLoad());
            return View(VmProc_Purchase);
        }
        [HttpGet]
        public ActionResult VmProc_PurchaseOrdersCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();


            VmForDropDown = new VmForDropDown();
            VmForDropDown.User_User = vMUser.User_User;

            VmForDropDown.DDownData = VmForDropDown.GetRequisitionList();
            SelectList s1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RequisitionCID = s1;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForCreatePO();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl2;



            VmMkt_PO = new VmMkt_PO();
            VmMkt_PO.Mkt_PO = new Mkt_PO();
            VmMkt_PO.Mkt_PO.Date = DateTime.Today;
            return View(VmMkt_PO);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_PurchaseOrdersCreate(VmMkt_PO a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int poid = 0;
            VmProc_Purchase VmProc_Purchase = new VmProc_Purchase();
            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            a.Mkt_PO.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            UserID = vmControllerHelper.GetCurrentUser().User_User.ID;

            if (ModelState.IsValid)
            {
                a.Mkt_PO.Status = 0;
                await Task.Run(() =>
                {
                    poid = a.Add(UserID);
                });

                await Task.Run(() =>
                {
                    if (a.Mkt_PO.RequisitionFK != null)
                        VmProc_Purchase.InsertRequisitionItemIntoMkt_PO(a.Mkt_PO.RequisitionFK.Value, poid);
                });

                return RedirectToAction("VmProc_PurchaseIndex");
            }
            return RedirectToAction("VmProc_PurchaseIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmProc_PurchaseOrdersEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForCreatePO();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl2;

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSinglePO(id.Value));

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_PurchaseOrdersEdit(VmMkt_PO VmMkt_PO)
        {
            if (ModelState.IsValid)
            {
                UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
                await Task.Run(() => VmMkt_PO.Edit(0));
                return RedirectToAction("VmProc_PurchaseIndex");
            }
            return View(VmMkt_PO);
        }


        public ActionResult VmProc_PurchaseOrderDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Mkt_PO mkt_PO = new Mkt_PO();
            mkt_PO.ID = id;
            mkt_PO.Delete(0);
            VmMktPO_Slave = new VmMktPO_Slave();
            VmMktPO_Slave.DaleteBulk(UserID, id);

            return RedirectToAction("VmProc_PurchaseIndex");
        }
        public async Task<ActionResult> VmProc_PurchaseOrdersSlaveIndex(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO = VmMkt_PO.SelectSingleJoinedForRequisitionPo(id));

            Mkt_POSlave pos = new Mkt_POSlave();
            pos.Mkt_POFK = id;

            VmMkt_PO.VmMktPO_Slave = new VmMktPO_Slave();
            await Task.Run(() => VmMkt_PO.VmMktPO_Slave.GetPOSlaveFromRequisition(id));
            VmMkt_PO.TotalVal = VmMkt_PO.VmMktPO_Slave.DataList2.Sum(x => x.LineTotal);
            return View(VmMkt_PO);
        }

        [HttpGet]
        public async Task<ActionResult> VmProc_PurchaseOrdersSlaveEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl3;

            VmMktPO_Slave = new VmMktPO_Slave();
            await Task.Run(() => VmMktPO_Slave.SelectSingleForPOSlaveRequisition(id.Value));
            if (VmMktPO_Slave.Mkt_POSlave == null)
            {
                return HttpNotFound();
            }
            return View(VmMktPO_Slave);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProc_PurchaseOrdersSlaveEdit(VmMktPO_Slave VmMktPO_Slave)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmMktPO_Slave.Edit(0));
                return RedirectToAction("VmProc_PurchaseOrdersSlaveIndex/" + VmMktPO_Slave.Mkt_POSlave.Mkt_POFK);
            }
            return View(VmMktPO_Slave);
        }

        public ActionResult VmProc_PurchaseOrdersSlaveDelete(int sid, int pid)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Mkt_POSlave Mkt_POSlave = new Mkt_POSlave();
            Mkt_POSlave.ID = sid;
            Mkt_POSlave.Delete(0);

            return RedirectToAction("VmProc_PurchaseOrdersSlaveIndex/" + pid);
        }

        [HttpGet]
        public async Task<ActionResult> VmProc_Purchase_Autherize(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();

            await Task.Run(() => VmMkt_PO.SelectSingle(id.Value));

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }

        [HttpPost]
        public async Task<ActionResult> VmProc_Purchase_Autherize(VmMkt_PO VmMkt_PO)
        {
            if (ModelState.IsValid)
            {
                UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
                await Task.Run(() => VmMkt_PO.Edit(0));
                return RedirectToAction("VmProc_PurchaseIndex");
            }
            return View(VmMkt_PO);
        }


        [HttpGet]
        public async Task<ActionResult> VmProc_Requision_Complete(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmProd_Requisition = new VmProd_Requisition();

            await Task.Run(() => VmProd_Requisition.SelectSingle(id.Value));

            if (VmProd_Requisition.Prod_Requisition == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_Requisition);
        }
        [HttpPost]
        public async Task<ActionResult> VmProc_Requision_Complete(VmProd_Requisition VmProd_Requisition)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_Requisition.Edit(0));
                return RedirectToAction("VmProc_RequisitaionIndex");
            }
            return View(VmProd_Requisition);
        }
        
    }
}