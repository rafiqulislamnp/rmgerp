﻿using Oss.Romo.Models;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CrystalDecisions.Web.HtmlReportRender;
using static Oss.Romo.ViewModels.Common.VmForDropDown;
using Oss.Romo.ViewModels.Marketing;
using System.Threading.Tasks;
//using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Oss.Romo.Models.Planning;
using Oss.Romo.ViewModels.Home;
using Oss.Romo.ViewModels.Planning;
using Oss.Romo.ViewModels.User;
using Oss.Romo.ViewModels.Commercial;

namespace Oss.Romo.Controllers
{
    public class HelperController : Controller
    {
        xOssContext db = new xOssContext();
        VmMis_AdminBoard VmMis_AdminBoard;
        public DataTable DataTable { get; set; }

        public HelperController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        public JsonResult GetUserMenuRole(int roleId)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return Json(db.User_RoleMenuItem.Where(x => x.User_RoleFK == roleId).ToList(), JsonRequestBehavior.AllowGet);
            }
        }
        //Those Method used for cascading Dropdown

        public JsonResult GetRaw_SubCategory(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_SubCategory.Where(p => p.Raw_CategoryFK == ID && p.Active == true && p.IsNew == true), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRawSubCategory(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_SubCategory.Where(p => p.Raw_CategoryFK == ID && p.Active == true && p.IsNew == true && p.ID != 2085
 && p.ID != 2086 && p.ID != 2087), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRaw_Item(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_Item.Where(p => p.Raw_SubCategoryFK == ID).Where(x => x.Active == true && x.IsNew == true), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRaw_SubCategoryNew(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_SubCategory.Where(p => p.Raw_CategoryFK == ID).Where(x => x.Active == true && x.IsNew == true), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRaw_ItemNew(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_Item.Where(p => p.Raw_SubCategoryFK == ID).Where(x => x.Active == true && x.IsNew == true), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetMkt_SubCategory(int? id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_SubCategory.Where(p => p.Mkt_CategoryFK == id).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        public JsonResult GetMKT_SubCategory(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_SubCategory.Where(p => p.Mkt_CategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        public JsonResult GetMkt_Item(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_Item.Where(p => p.Mkt_SubCategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        //...Those Method used for cascading Dropdown for Accounting....//


        public JsonResult GetAcc_Chart1DD(int? ID)
        {

            db.Configuration.ProxyCreationEnabled = false;

            return Json(db.Acc_Chart1.Where(a => a.Acc_TypeFK == ID).Where(c => c.Active == true), JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetUserSubMenu(int? ID)
        {

            db.Configuration.ProxyCreationEnabled = false;

            return Json(db.User_SubMenu.Where(a => a.User_MenuFk == ID).Where(c => c.Active == true), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetAcc_Chart2DD(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Acc_Chart2.Where(a => a.Acc_Chart1FK == ID).Where(c => c.Active == true), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetAcc_NameDD(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Acc_AcName.Where(a => a.Acc_Chart2FK == ID).Where(c => c.Active == true), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetAcc_JournalSlaveDD(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Acc_JournalSlave.Where(a => a.Acc_AcNameFK == ID).Where(c => c.Active == true), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetItemIdByRequisition(int reqSlaveId)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var rawItemId = (from t1 in db.Prod_Requisition_Slave                                      
                                       where t1.Active == true  && t1.ID == reqSlaveId
                                       select new
                                       {
                                           POSlaveId = t1.Mkt_POSlaveFK                                          
                                       }).SingleOrDefault();

            return Json(rawItemId, JsonRequestBehavior.AllowGet);

        }
        

        public JsonResult GetBOMWiseSupplierDD(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var bOMWiseSupplierList = (from t1 in db.Common_Supplier
                                       join t2 in db.Mkt_BOMSlave
                                       on t1.ID equals t2.Common_SupplierFK
                                       where t1.Active == true
                                       && t2.Mkt_BOMFK == ID
                                       select new
                                       {
                                           ID = t2.Common_SupplierFK,
                                           Name = t1.Name
                                       }).Distinct();
            return Json(bOMWiseSupplierList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetBOMByMasterLCDD(int BOMID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var a = (from t1 in db.Mkt_BOM
                     join t2 in db.Common_TheOrder
                     on t1.Common_TheOrderFk equals t2.ID
                     join t3 in db.Commercial_MasterLCBuyerPO
                     on t1.ID equals t3.MKTBOMFK
                     join t4 in db.Commercial_MasterLC
                     on t3.Commercial_MasterLCFK equals t4.ID
                     where t1.Active == true
                     && t2.Active == true
                     && t3.Active == true
                     && t4.Active == true
                     && t1.ID == BOMID
                     select new
                     {
                         BOMID = t3.MKTBOMFK,
                         QPack = t1.QPack,
                         MasterLcID = t4.ID
                     }).Distinct();
            return Json(a, JsonRequestBehavior.AllowGet);
        }
        public int SumOfExistingQPackOrderInvoince = 0;
        //public JsonResult GetBOMExistingQPackInvoinceByMasterLCDD(int bOMID)
        //{
        //    db.Configuration.ProxyCreationEnabled = false;
        //    var a = (from t1 in db.Commercial_InvoiceSlave
        //             join t2 in db.Commercial_Invoice
        //             on t1.Commercial_InvoiceFk equals t2.ID

        //             //join t4 in db.Commercial_MasterLC
        //             //on t2.Commercial_MasterLCFk equals t4.ID
        //             //join t5 in db.Mkt_BOM
        //             //on t1.Mkt_BOMFK equals t5.ID
        //             where t1.Active == true
        //             && t2.Active == true
        //            // && t5.Active == true
        //            // && t4.Active == true
        //             select new
        //             {
        //                 BOMID = t1.Mkt_BOMFK,
        //                 ExistingQPackOrderInvoince = t1.InvoiceQPack,
        //                 //MasterLcID = t4.ID
        //             }).Where(x => x.BOMID == bOMID).Distinct();
        //    return Json(a, JsonRequestBehavior.AllowGet);

        //}
        VmAcc_SupplierPH x = new VmAcc_SupplierPH();
        public JsonResult SupplierPayableValue(int id)
        {
            db = new xOssContext();
            var a = (from Common_Supplier in db.Common_Supplier
                     join t1 in db.Mkt_PO on Common_Supplier.ID equals t1.Common_SupplierFK
                     where Common_Supplier.Active == true && t1.Active == true &&
                     Common_Supplier.ID == id
                     select new VmAcc_SupplierPH
                     {
                         Common_Supplier = Common_Supplier

                     }).Distinct();

            List<VmAcc_SupplierPH> tempDl = new List<VmAcc_SupplierPH>();

            foreach (VmAcc_SupplierPH vmAcc_SupplierPH in a)
            {
                decimal? tempD = x.LCallocationHistory(vmAcc_SupplierPH.Common_Supplier.ID);
                decimal? B2Bpayment = x.GetB2BLCWisePaymentSummary(vmAcc_SupplierPH.Common_Supplier.ID);
                decimal? ChequePayment = x.ChequeWisePaymentSummary(vmAcc_SupplierPH.Common_Supplier.ID);
                decimal? ChallanValue = x.GetPoRecevingChallanValue(vmAcc_SupplierPH.Common_Supplier.ID);
                decimal? openBTB = x.GetOpenBTBValueForThisSupplier(vmAcc_SupplierPH.Common_Supplier.ID);
                if (tempD == null)
                {
                    tempD = 0;
                }
                if (B2Bpayment == null)
                {
                    B2Bpayment = 0;
                }
                if (ChequePayment == null)
                {
                    ChequePayment = 0;
                }
                if (ChallanValue == null)
                {
                    ChallanValue = 0;
                }
                VmAcc_SupplierPH vmAcc_SupplierPHTemp = new VmAcc_SupplierPH();
                vmAcc_SupplierPHTemp.Common_Supplier = vmAcc_SupplierPH.Common_Supplier;
                vmAcc_SupplierPHTemp.Due = x.POallocationValue(vmAcc_SupplierPH.Common_Supplier.ID) - tempD;
                decimal? payable = (ChallanValue - (B2Bpayment + ChequePayment)) - openBTB;
                vmAcc_SupplierPHTemp.Payable = payable;
                tempDl.Add(vmAcc_SupplierPHTemp);
            }

            return Json(tempDl, JsonRequestBehavior.AllowGet);



        }
        public JsonResult PlanOrderProcess(int? categoryid)
        {

            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Plan_OrderProcess.Where(p => p.Plan_OrderProcessCategoryFk == categoryid).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);

        }


        public JsonResult GetLycraValue(int ID)
        {
            // int id = 0;
            //Int32.TryParse(ID, out id);
            db = new xOssContext();

            var x = (from Mkt_YarnCalculation in db.Mkt_YarnCalculation
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_YarnCalculation.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join User_User in db.User_User
                     on Mkt_BOM.FirstCreatedBy equals User_User.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_YarnCalculation.Raw_ItemFK equals Raw_Item.ID
                     where Mkt_YarnCalculation.Mkt_BOMFK == ID
                     select ((((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption / 100 * Mkt_YarnCalculation.ProcessLoss + (Mkt_YarnCalculation.QntyPcs / 12)

                  * Mkt_YarnCalculation.Consumption) / 100) * Mkt_YarnCalculation.Lycra));


            decimal TotalLycra = 0;
            if (x.Count() != 0)
            {
                TotalLycra = x.Sum();
            }

            return Json(TotalLycra.ToString("F"), JsonRequestBehavior.AllowGet);

        }



        public JsonResult GetYarnValue(int ID)
        {
            //int id = 0;
            //Int32.TryParse(ID, out id);
            db = new xOssContext();
            var x = (from Mkt_YarnCalculation in db.Mkt_YarnCalculation
                     join Mkt_BOM in db.Mkt_BOM
                     on Mkt_YarnCalculation.Mkt_BOMFK equals Mkt_BOM.ID
                     join Common_TheOrder in db.Common_TheOrder
                     on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                     join Mkt_Item in db.Mkt_Item
                     on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                     join User_User in db.User_User
                     on Mkt_BOM.FirstCreatedBy equals User_User.ID
                     join Raw_Item in db.Raw_Item
                     on Mkt_YarnCalculation.Raw_ItemFK equals Raw_Item.ID
                     where Mkt_YarnCalculation.Mkt_BOMFK == ID
                     select ((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption / 100 * Mkt_YarnCalculation.ProcessLoss + (Mkt_YarnCalculation.QntyPcs / 12)
                           * Mkt_YarnCalculation.Consumption - (((Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption / 100
                           * Mkt_YarnCalculation.ProcessLoss + (Mkt_YarnCalculation.QntyPcs / 12) * Mkt_YarnCalculation.Consumption) / 100) * Mkt_YarnCalculation.Lycra));


            decimal TotalYarn = 0;
            if (x.Count() != 0)
            {
                TotalYarn = x.Sum();
            }
            return Json(TotalYarn.ToString("F"), JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetAverageConsumption(int ID)
        {
            // int id = 0;
            //Int32.TryParse(ID, out id);
            db = new xOssContext();
            var TotalYarn = (from Mkt_YarnCalculation in db.Mkt_YarnCalculation
                             join Mkt_BOM in db.Mkt_BOM
                             on Mkt_YarnCalculation.Mkt_BOMFK equals Mkt_BOM.ID
                             join Common_TheOrder in db.Common_TheOrder
                             on Mkt_BOM.Common_TheOrderFk equals Common_TheOrder.ID
                             join Mkt_Item in db.Mkt_Item
                             on Mkt_BOM.Mkt_ItemFK equals Mkt_Item.ID
                             join User_User in db.User_User
                             on Mkt_BOM.FirstCreatedBy equals User_User.ID
                             join Raw_Item in db.Raw_Item
                             on Mkt_YarnCalculation.Raw_ItemFK equals Raw_Item.ID
                             where Mkt_YarnCalculation.Mkt_BOMFK == ID
                             select Mkt_YarnCalculation.Consumption).ToList();


            int count = TotalYarn.Count();
            decimal consumption = 0;
            if (count != 0)
            {
                consumption = TotalYarn.Sum() / count;
            }



            return Json(consumption.ToString("F"), JsonRequestBehavior.AllowGet);

        }


        public JsonResult GetReqItem(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var v = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Raw_Item
                         on t1.Raw_ItemFK equals t2.ID
                     join t3 in db.Prod_Requisition
                         on t1.Prod_RequisitionFK equals t3.ID
                     where t1.Active == true && t1.IsFinalize == false
                           && t3.ID == ID
                     select new
                     {
                         Text = t2.Name,
                         ID = t1.ID
                     }).ToList();
            return Json(v, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTranstionItem(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var v = (from
                      t1 in db.Prod_TransitionItemInventory
                     where t1.Active == true

                     select new
                     {
                         Text = t1.ItemName,
                         ID = t1.ID,

                     }).ToList();
            return Json(v, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemByRequsition(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            //var v = (from t1 in db.Prod_Requisition_Slave
            //         join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
            //         join t3 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t3.ID
            //         where t1.Active == true
            //         && t1.IsFinalize == false
            //         && t1.Raw_ItemFK != 1
            //         && t3.ID == ID
            //         select new
            //         {
            //             Text = t2.Name,
            //             ID = t1.ID
            //         }).ToList();

            var v = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                     join t10 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t10.ID
                     join t3 in db.Raw_Item on t10.Raw_ItemFK equals t3.ID
                     join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                     join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                     where t1.Active == true && t2.ID == ID && t1.Mkt_POSlaveFK != 1 && t2.Active == true
                     && t2.IsAutherized == true && t10.Active == true && t3.Active == true
                     select new
                     {
                         Text = t10.Description == null ? t3.Name + " | " + t5.BuyerPO : t3.Name + " | " + t10.Description + "("+ t5.BuyerPO + ")" ,
                         ID = t1.ID,
                         POSlaveId = t10.ID
                     }).ToList();

            var v1 = (from t1 in db.Prod_Requisition_Slave
                      join t2 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFk equals t2.ID
                      join t3 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t3.ID
                      where t1.Active == true
                      && t1.IsFinalize == false
                      && t1.Prod_TransitionItemInventoryFk != 1
                      && t3.ID == ID && t3.IsAutherized == true
                      select new
                      {
                          Text = t2.ItemName,
                          ID = t1.ID,
                          POSlaveId = t2.ID
                      }).ToList();

            var x = v.Union(v1);
            return Json(x, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRequsitionWithTransitionItem(int? ID)
        {
            db = new xOssContext();

            var a = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                     join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                     join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
                     where
                     t1.Active == true
                     && t1.Prod_RequisitionFK == ID
                     && t1.Raw_ItemFK != 1
                     select new
                     {
                         Text = t3.Name,
                         ID = t3.ID,
                         Quantity = t1.TotalRequired,
                         ReqSlaveId = t1.ID,
                         ReqId = t1.Prod_RequisitionFK,
                         BomFK = t1.Mkt_BOMFK,
                         Common_UnitFK = t1.Common_UnitFK
                     }).OrderByDescending(x => x.ReqSlaveId).AsEnumerable();

            var b = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Prod_Requisition on t1.Prod_RequisitionFK equals t2.ID
                     join t3 in db.Prod_TransitionItemInventory on t1.Prod_TransitionItemInventoryFk equals t3.ID
                     join t4 in db.Common_Unit on t1.Common_UnitFK equals t4.ID
                     where
                     t1.Active == true
                     && t1.Prod_RequisitionFK == ID
                     && t1.Prod_TransitionItemInventoryFk != 1
                     select new
                     {
                         Text = t3.ItemName,
                         ID = t3.ID,
                         Quantity = t1.TotalRequired,
                         ReqSlaveId = t1.ID,
                         ReqId = t1.Prod_RequisitionFK,
                         BomFK = t1.Mkt_BOMFK,
                         Common_UnitFK = t1.Common_UnitFK

                     }).OrderByDescending(x => x.ReqSlaveId).AsEnumerable();

            var v = a.Union(b);
            return Json(v, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetItemRequiredQty(int? ID)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                     where t1.ID == ID && t1.Active == true
                     select new
                     {
                         TotalRequired = t1.TotalRequired,
                         Common_UnitFK = t1.Common_UnitFK,
                         Mkt_POSlaveFK = t1.Mkt_POSlaveFK,
                         Mkt_BOMFK = t1.Mkt_BOMFK,
                         Raw_ItemFK = t1.Raw_ItemFK,
                         Prod_TransitionItemInventoryFk = t1.Prod_TransitionItemInventoryFk,
                         UnitName = t2.Name
                     }).FirstOrDefault();
            return Json(v, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDelivaryChallanByID(int? ID)
        {
            db = new xOssContext();



            var v = (from t1 in db.Commercial_InvoiceSlave
                     join t2 in db.Shipment_OrderDeliverdSchedule on t1.Shipment_OrderDeliverdScheduleFK equals t2.ID

                     where t1.ID == ID && t1.Active == true
                     select new
                     {
                         Shipment_OrderDeliverdScheduleFK = t1.Shipment_OrderDeliverdScheduleFK,
                         Shipment_CNFDelivaryChallanFk = t1.Shipment_CNFDelivaryChallanFk,
                         MktBOMFK = t1.MktBOMFK,
                         DeliverdQty = t2.DeliverdQty
                     }).FirstOrDefault();


            return Json(v, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetReqItemQty(int? id)
        {
            var v = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID

                     where t1.ID == id && t1.Active == true
                     select new
                     {
                         Itemid = t1.Raw_ItemFK,
                         RequestQuantity = t1.TotalRequired,
                         EstimatedPrice = t1.EstimatedPrice,
                         Unit = t2.Name
                     }).FirstOrDefault();

            var d = db.Mkt_POSlave.Where(a => a.RequisitionSlaveFK == id && a.Raw_ItemFK == v.Itemid && a.Active == true);
            decimal? p = 0;
            if (d.Count() != 0)
            {
                p = d.Sum(a => a.TotalRequired);
            }

            string qty;
            if (p == null)
            {
                Debug.Assert(v != null, nameof(v) + " != null");
                qty = v.RequestQuantity + " " + v.Unit;
            }
            else
            {
                qty = (v?.RequestQuantity - p.Value) + " " + v?.Unit;
            }
            Debug.Assert(v != null, nameof(v) + " != null");
            var result = new { Quantity = qty, UnitPrice = v.EstimatedPrice };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public string GetReqItemQtyForEdit(int id)
        {
            var v = (from t1 in db.Prod_Requisition_Slave
                     join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                     where t1.ID == id && t1.Active == true
                     select new
                     {
                         Itemid = t1.Raw_ItemFK,
                         RequestQuantity = t1.TotalRequired,
                         Unit = t2.Name
                     }).FirstOrDefault();
            var p = db.Mkt_POSlave.Where(a => a.RequisitionSlaveFK == id && a.Raw_ItemFK == v.Itemid && a.Active == true)
                .Sum(a => a.TotalRequired);
            string x = null;
            var qty = x;
            if (p == null)
            {
                qty = v.RequestQuantity + " " + v.Unit;
            }
            else
            {
                qty = (v.RequestQuantity - p.Value) + " " + v.Unit;
            }
            return qty;
        }

        public string GetEstimatedPriceForEdit(int id)
        {
            var price = db.Prod_Requisition_Slave.FirstOrDefault(x => x.ID == id);
            return price != null ? price.EstimatedPrice.ToString() : "";
        }

        public JsonResult GetReqItemForSupplierPoList(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var v = (from t1 in db.Mkt_PO
                     join t2 in db.Mkt_POSlave on t1.ID equals t2.Mkt_POFK
                     join t3 in db.Raw_Item on t2.Raw_ItemFK equals t3.ID
                     where t1.ID == id && t1.Active == true && t2.Active == true && t3.Active == true
                     select new
                     {

                         Text = t3.Name + " | " + t2.Description,
                         ID = t2.ID
                     }).ToList();
            return Json(v, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuantityForSupplierPoList(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var v = (from t1 in db.Mkt_POSlave
                     join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                     where t1.ID == id && t1.Active == true && t2.Active == true
                     select new
                     {
                         Quantity = t1.TotalRequired,
                         Unit = t2.Name
                     }).FirstOrDefault();

            var p = db.Commercial_B2bLcSupplierPOSlave.Where(x => x.Mkt_POSlaveFK == id && x.Active == true).Sum(x => x.TotalRequired);

            string nowReq = " ";
            if (p == null || p == 0)
            {
                nowReq = v.Quantity + " " + v.Unit;
            }
            else
            {
                nowReq = (v.Quantity - p.Value) + " " + v.Unit;
            }
            var result = new { Total = v.Quantity + " " + v.Unit, NowReq = nowReq };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSizeByPlanReferenceProduction(int id, string color, int orderDeliveryScheduleId)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderColorAndSizeRatio
                     join t3 in db.Prod_PlanReference on t1.OrderDeliveryScheduleFk equals t3.Mkt_OrderDeliveryScheduleFK
                     where t1.Mkt_BOMFk == id && t1.Color == color && t1.Active == true && t3.Mkt_OrderDeliveryScheduleFK == orderDeliveryScheduleId
                     select new
                     {
                         Text = t1.Size,
                         ID = t1.Size,
                         Quantity = t1.Quantity,
                         Ratio = t1.Ratio
                     }).Distinct().ToList();
            return Json(v, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBomByDelivaryChallan(int ChallanId)
        {
            db = new xOssContext();
            var bomID = (from t1 in db.Shipment_OrderDeliverdSchedule
                             //join t2 in db.Shipment_OrderDeliverdSchedule on t1.ID equals t2.Mkt_BOMFK
                         where t1.Shipment_CNFDelivaryChallanFk == ChallanId
                         select new
                         {
                             BOMID = t1.Mkt_BOMFK
                         }).FirstOrDefault();
            return Json(bomID, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetScheduleByDelivaryChallan(int ChallanId)
        {
            db = new xOssContext();
            var bomID = (from t1 in db.Shipment_OrderDeliverdSchedule
                         join t2 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t2.ID
                         join t5 in db.Common_TheOrder on t1.Common_TheOrderFk equals t5.ID
                         //join t3 in db.Mkt_OrderDeliverySchedule on t1.Mkt_BOMFK equals t3.Mkt_BOMFk
                         where t1.Shipment_CNFDelivaryChallanFk == ChallanId && t1.Active == true && t2.Active == true
                         select new
                         {
                             //Commercial_InvoiceSlave
                             Text = t1.DeliverdQty + " | " + SqlFunctions.DateName("day", t1.Date) + "/" + SqlFunctions.DateName("month", t1.Date) + "/" + SqlFunctions.DateName("year", t1.Date) + "" + " | " + t5.CID,
                             Value = t1.ID
                         }).ToList();
            var bomIdDelivered = (from t1 in db.Shipment_OrderDeliverdSchedule
                                  join t2 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t2.ID
                                  join t3 in db.Commercial_InvoiceSlave on t1.ID equals t3.Shipment_OrderDeliverdScheduleFK
                                  join t4 in db.Commercial_Invoice on t3.Commercial_InvoiceFk equals t4.ID
                                  join t5 in db.Common_TheOrder on t1.Common_TheOrderFk equals t5.ID
                                  //join t3 in db.Mkt_OrderDeliverySchedule on t1.Mkt_BOMFK equals t3.Mkt_BOMFk
                                  where t1.Shipment_CNFDelivaryChallanFk == ChallanId &&
                                  t1.Active == true &&
                                  t2.Active == true &&
                                  t3.Active == true &&
                                  t4.Active == true
                                  select new
                                  {
                                      //Commercial_InvoiceSlave
                                      Text = t1.DeliverdQty + " | " + SqlFunctions.DateName("day", t1.Date) + "/" + SqlFunctions.DateName("month", t1.Date) + "/" + SqlFunctions.DateName("year", t1.Date) + "" + " | " + t5.CID,
                                      Value = t1.ID
                                  }).ToList();
            var returnable = bomID.Except(bomIdDelivered);
            return Json(returnable, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDelivaryChallanQuantity(int ID)
        {
            db = new xOssContext();
            var qty = (from t1 in db.Shipment_OrderDeliverdSchedule
                       where t1.ID == ID
                       select new
                       {
                           DeliverdQuantity = t1.DeliverdQty
                       }).SingleOrDefault();

            return Json(qty, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuantitiyByPlanReferenceProduction(int id, string sizeid)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderColorAndSizeRatio
                     where t1.Mkt_BOMFk == id && t1.Size == sizeid
                     select new
                     {
                         Text = t1.Size,
                         ID = t1.Size
                     }).ToList();
            return Json(v, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPortForProduction(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     join t2 in db.Common_Country on t1.Destination equals t2.ID
                     where t1.Mkt_BOMFk == id && t1.Active == true && t2.Active == true
                     && !(from x in db.Prod_PlanReference join y in db.Mkt_OrderDeliverySchedule on x.Mkt_OrderDeliveryScheduleFK equals y.ID where x.Active == true && y.Active == true select x.Mkt_OrderDeliveryScheduleFK)
                         .Contains(t1.ID)
                     select new
                     {
                         Text = t2.Name + " | " + t1.PortNo + " (" + SqlFunctions.DateName("day", t1.Date) + "/" + SqlFunctions.DateName("month", t1.Date) + "/" + SqlFunctions.DateName("year", t1.Date) + ")",
                         ID = t1.ID,

                     }).ToList();
            return Json(v, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuantityForProduction(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Mkt_OrderDeliverySchedule
                     join t2 in db.Common_Country on t1.Destination equals t2.ID
                     where t1.ID == id && t1.Active == true && t2.Active == true
                     select new
                     {
                         Quantity = t1.Quantity

                     }).FirstOrDefault();
            return Json(v, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStatusDetails(int id)
        {
            db = new xOssContext();
            var v = db.Common_Status.Where(a => a.ID == id);
            if (v.Any())
            {
                return Json(v.FirstOrDefault().Description, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetRequiredQty(int? poSlaveId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var result = (from t1 in db.Mkt_POSlave
                          join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                          where t1.ID == poSlaveId && t1.Active == true
                          select new
                          {
                              Description = t1.Description,
                              RequiredQuantity = t1.TotalRequired,
                              UnitName = t2.Name,
                              Common_UnitFK = t2.ID,
                              Consumption = t1.Consumption
                          }).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStyleItem(int id)
        {
            db = new xOssContext();
            var itemList = new List<object>();

            var item = (from t1 in db.Mkt_BOMSlave
                        join t2 in db.Raw_Item on t1.Raw_ItemFK equals t2.ID
                        where t1.Mkt_BOMFK == id && t1.Active == true
                        select new
                        {
                            Name = t2.Name + " | " + t1.Description,
                            ID = t1.Raw_ItemFK
                        });

            foreach (var v in item)
            {
                itemList.Add(new { Text = v.Name, Value = v.ID });
            }

            return Json(itemList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStyleItemByBomPoSlave(int id)
        {
            db = new xOssContext();
            var itemList = new List<object>();

            var item = (from t1 in db.Mkt_POSlave
                        join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                        join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                        where t1.Active == true && t2.Mkt_BOMFK == id
                        select new
                        {
                            Name = t3.Name + " | " + t1.Description,
                            ID = t1.ID,
                            RequiredQuantity = t1.TotalRequired
                        });

            foreach (var v in item)
            {
                itemList.Add(new { Text = v.Name, Value = v.ID, RequiredQuantity = v.RequiredQuantity });
            }

            return Json(itemList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStyleFinishFabricBomPoSlave(int id)
        {
            db = new xOssContext();
            var itemList = new List<object>();

            var item = (from t1 in db.Mkt_POSlave
                        join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                        join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                        join t4 in db.Mkt_POSlave_Receiving on t1.ID equals t4.Mkt_POSlaveFK
                        where t1.Active == true && t2.Active==true && t2.Mkt_BOMFK == id && t4.IsFinishFabric==true
                        select new
                        {
                            Name = t3.Name + " | " + t1.Description,
                            ID = t1.ID,
                            RequiredQuantity = t1.TotalRequired
                        });

            foreach (var v in item)
            {
                itemList.Add(new { Text = v.Name, Value = v.ID, RequiredQuantity = v.RequiredQuantity });
            }

            return Json(itemList, JsonRequestBehavior.AllowGet);
        }


        public decimal? GetAvailableReceivedQuantity(int id)
        {
            db = new xOssContext();
            var receiveItem = (from t1 in db.Mkt_POSlave_Receiving
                               where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                               select t1.Quantity).DefaultIfEmpty(0).Sum();

            var returnItem = (from t1 in db.Mkt_POSlave_Receiving
                              where t1.Mkt_POSlaveFK == id && t1.IsReturn == true
                              select t1.Quantity).DefaultIfEmpty(0).Sum();

            var internalTransfer = (from t1 in db.Mkt_POSlave_Consumption
                                    where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                                    select t1.Quantity).DefaultIfEmpty(0).Sum();
            var internalTransferReturn = (from t1 in db.Mkt_POSlave_Consumption
                                    where t1.Mkt_POSlaveFK == id && t1.IsReturn == true
                                    select t1.Quantity).DefaultIfEmpty(0).Sum();
            var receive = receiveItem - returnItem;
            var delivery = internalTransfer - internalTransferReturn;
            

            return (receive - delivery);
        }

        public decimal? GetAvailableFabricReceivedQuantity(int id,string color)
        {
            db = new xOssContext();
            var receiveItem = (from t1 in db.Mkt_POSlave_Receiving
                               where t1.Mkt_POSlaveFK == id && t1.IsReturn == false && t1.IsFinishFabric==true && t1.FabricColor.Equals(color)
                               select t1.Quantity-t1.StockLoss ).DefaultIfEmpty(0).Sum();
            
            var returnItem = (from t1 in db.Mkt_POSlave_Receiving
                              where t1.Mkt_POSlaveFK == id && t1.IsReturn == true && t1.IsFinishFabric == true && t1.FabricColor.Equals(color)
                              select t1.Quantity-t1.StockLoss).DefaultIfEmpty(0).Sum();

            var internalTransfer = (from t1 in db.Mkt_POSlave_Consumption
                                    where t1.Mkt_POSlaveFK == id && t1.IsReturn == false
                                    select t1.Quantity).DefaultIfEmpty(0).Sum();

            var internalTransferReturn = (from t1 in db.Mkt_POSlave_Consumption
                                          where t1.Mkt_POSlaveFK == id && t1.IsReturn == true
                                          select t1.Quantity).DefaultIfEmpty(0).Sum();
            var receive = receiveItem - returnItem;
            var delivery = internalTransfer - internalTransferReturn;


            return (receive - delivery);
        }

        public JsonResult GetRealisationPercentValue(int realistionId, decimal percent)
        {
            object result = null;
            db = new xOssContext();
            var accExportRealisation = db.Acc_ExportRealisation.Find(realistionId);
            if (accExportRealisation != null)
            {
                var realisationamtbdt = accExportRealisation.RealisedAmount * accExportRealisation.DollarValue;
                var realisationpercentamtbdt = (realisationamtbdt * percent) / 100;
                result = new { realisationpercentamtbdt };
            }

            return Json(result);
        }
        public List<object> GetStoreExternalReqIdDropDown()
        {
            db = new xOssContext();
            var StoreReqList = new List<object>();

            foreach (var req in db.Prod_Requisition.Where(x => x.Active == true && x.Internal == false))
            {
                StoreReqList.Add(new { Text = req.RequisitionCID, Value = req.ID });
            }
            return StoreReqList;
        }
        public object GetCollectionDueReceivableAmountLast12Months()
        {
            object result = null;
            VmMis_AdminBoard = new VmMis_AdminBoard();
            VmMis_AdminBoard.GetCollectionDueReceivableAmountLast12Months(DateTime.Now);
            var s = VmMis_AdminBoard.CollectionDueReceivableAmount;
            var t = JsonConvert.SerializeObject(s);
            result = new { data = t };
            return result;
        }
        public object GetCollectionDueReceivableAmountLastMonths()
        {
            object result = null;
            VmMis_AdminBoard = new VmMis_AdminBoard();
            VmMis_AdminBoard.GetCollectionDueReceivableAmountLastMonths(DateTime.Now);
            var s = VmMis_AdminBoard.CollectionDueReceivableAmount;
            var t = JsonConvert.SerializeObject(s);
            result = new { data = t };
            return result;
        }
        public JsonResult GetTransitionalItemQuantity(int id)
        {
            db = new xOssContext();
            var v = (from t1 in db.Prod_TransitionItemInventory
                     where t1.ID == id && t1.Active == true
                     select new
                     {
                         Quantity = t1.Quantity,
                     }).FirstOrDefault();
            return Json(v, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMasterLCByUDNo(int id)
        {
            db = new xOssContext();
            var StoreReqList = new List<object>();
            var item = (from t1 in db.Commercial_MasterLC
                        join t2 in db.Commercial_UDSlave
                            on t1.ID equals t2.Commercial_MasterLCFK
                        where t2.Commercial_UDFK == id
                        select new
                        {
                            ID = t1.ID,
                            Name = t1.Name
                        });
            foreach (var req in item)
            {
                StoreReqList.Add(new { Text = req.Name, Value = req.ID });
            }
            return Json(StoreReqList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBillOfExchangeByMasterLC(int id)
        {
            db = new xOssContext();
            var StoreReqList = new List<object>();
            var item = (from t1 in db.Commercial_BillOfExchange
                        where t1.Commercial_MasterLCFK == id
                        select new
                        {
                            ID = t1.ID,
                            Name = t1.Name
                        });
            foreach (var req in item)
            {
                StoreReqList.Add(new { Text = req.Name, Value = req.ID });
            }
            return Json(StoreReqList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBillOfExchangeData(int id)
        {

            db = new xOssContext();

            var invoiceList = (from tt in db.Commercial_InvoiceSlave
                               join yy in db.Commercial_Invoice on tt.Commercial_InvoiceFk equals yy.ID
                               join xx in db.Commercial_BillOfExchangeSlave on yy.ID equals xx.Commercial_InvoiceFK
                               where xx.Active == true && tt.Active == true && xx.Commercial_BillOfExchangeFk == id
                               select tt.Commercial_InvoiceFk).Distinct().ToList();
            decimal ttlInvoiceValue = GetInvoiceValue(invoiceList);

            var item = (from t1 in db.Commercial_BillOfExchange
                            //join xx in db.Commercial_BillOfExchangeSlave on t1.ID equals xx.Commercial_BillOfExchangeFk
                        where t1.ID == id
                        select new
                        {
                            ID = t1.ID,
                            Name = t1.Name,
                            Date = t1.BillDate.Year + "/" + t1.BillDate.Month + "/" + t1.BillDate.Day,
                            FDBCNO = t1.FDBCNO,
                            FDBCDate = t1.FDBCDate.Year + "/" + t1.FDBCDate.Month + "/" + t1.FDBCDate.Day,

                            BillValue = ttlInvoiceValue

                            //(from x in db.Commercial_InvoiceSlave
                            //             join t2 in db.Commercial_Invoice on x.Commercial_InvoiceFk equals t2.ID
                            //             join t3 in db.Shipment_CNFDelivaryChallan on x.Shipment_CNFDelivaryChallanFk equals t3.ID
                            //             join t0 in db.Shipment_OrderDeliverdSchedule on x.Shipment_OrderDeliverdScheduleFK equals t0.ID
                            //             join t4 in db.Mkt_BOM on t0.Mkt_BOMFK equals t4.ID                                        
                            //             where invoiceList.Contains(x.Commercial_InvoiceFk) && t1.Active == true
                            //             select t4.PackPrice * t0.PackQty).DefaultIfEmpty(0).Sum()
                        }).SingleOrDefault();

            


            return Json(item, JsonRequestBehavior.AllowGet);
        }

        private decimal GetInvoiceValue(List<int> invoiceList)
        {
            decimal ttlInvoice = 0;
            foreach (var item in invoiceList)
            {
                db = new xOssContext();
              

                var a = (from t1 in db.Commercial_InvoiceSlave
                         join t2 in db.Commercial_Invoice on t1.Commercial_InvoiceFk equals t2.ID
                         join t3 in db.Shipment_CNFDelivaryChallan on t1.Shipment_CNFDelivaryChallanFk equals t3.ID
                         join t0 in db.Shipment_OrderDeliverdSchedule on t1.Shipment_OrderDeliverdScheduleFK equals t0.ID
                         join t4 in db.Mkt_BOM on t0.Mkt_BOMFK equals t4.ID
                         join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                         join t6 in db.Mkt_Item on t4.Mkt_ItemFK equals t6.ID
                         join t7 in db.Common_Currency on t5.Common_CurrencyFK equals t7.ID
                         where t1.Commercial_InvoiceFk == item && t1.Active == true
                         select (t4.PackPrice * t0.PackQty)).ToList();

                decimal deliverdQty = 0;
                if (a != null)
                {
                    var invoice = db.Commercial_Invoice.SingleOrDefault(x => x.ID == item);
                    if (invoice.IsIncrease == true)
                    {
                        deliverdQty = a.Sum() + invoice.IncDecAmount;
                    }
                    else
                    {
                        deliverdQty = a.Sum() - invoice.IncDecAmount;
                    }

                }
                ttlInvoice += deliverdQty;
            }
            return ttlInvoice;
        }

        public JsonResult GetSerachedPlan(DateTime? StartDate = null, DateTime? EndDate = null, int BuyerId = 0)
        {
            db = new xOssContext();
            if (StartDate.HasValue && EndDate.HasValue && BuyerId == 0)
            {
                var v = (from t1 in db.Prod_Planning

                         join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                         join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                         join t6 in db.Mkt_Buyer on t5.Mkt_BuyerFK equals t6.ID
                         join t7 in db.Mkt_Item on t4.Mkt_ItemFK equals t7.ID

                         where t1.StartDate >= StartDate && t1.StartDate <= EndDate
                         select new
                         {
                             PlanningID = t1.ID,
                             StartDate = t1.StartDate,
                             FinishDate = t1.FinishDate,
                             BOMCID = t5.CID + "/" + t4.Style,
                             ItemName = t7.Name,
                             OrderQuantity = t4.Quantity * t4.QPack,
                             MachineQuantity = t1.MachineQuantity,
                             Capacity = t1.Capacity,
                             TargetDays = t1.TargetDays,
                             WorkingHours = t1.WorkingHour
                         }).OrderByDescending(x => x.PlanningID).AsEnumerable();
                return Json(v, JsonRequestBehavior.AllowGet);
            }

            if (!StartDate.HasValue && !EndDate.HasValue && BuyerId != 0)
            {
                var v = (from t1 in db.Prod_Planning

                         join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                         join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                         join t6 in db.Mkt_Buyer on t5.Mkt_BuyerFK equals t6.ID
                         join t7 in db.Mkt_Item on t4.Mkt_ItemFK equals t7.ID
                         where t6.ID == BuyerId
                         select new
                         {
                             PlanningID = t1.ID,
                             StartDate = t1.StartDate,
                             FinishDate = t1.FinishDate,
                             BOMCID = t5.CID + "/" + t4.Style,
                             ItemName = t7.Name,
                             OrderQuantity = t4.Quantity * t4.QPack,
                             MachineQuantity = t1.MachineQuantity,
                             Capacity = t1.Capacity,
                             TargetDays = t1.TargetDays,
                             WorkingHours = t1.WorkingHour
                         }).OrderByDescending(x => x.PlanningID).AsEnumerable();
                return Json(v, JsonRequestBehavior.AllowGet);
            }
            else if (StartDate.HasValue && EndDate.HasValue && BuyerId != 0)
            {
                var v = (from t1 in db.Prod_Planning

                         join t4 in db.Mkt_BOM on t1.Mkt_BOMFK equals t4.ID
                         join t5 in db.Common_TheOrder on t4.Common_TheOrderFk equals t5.ID
                         join t6 in db.Mkt_Buyer on t5.Mkt_BuyerFK equals t6.ID
                         join t7 in db.Mkt_Item on t4.Mkt_ItemFK equals t7.ID
                         where t6.ID == BuyerId && (t1.StartDate >= StartDate && t1.StartDate <= EndDate)
                         select new
                         {

                             PlanningID = t1.ID,
                             StartDate = t1.StartDate,
                             FinishDate = t1.FinishDate,
                             BOMCID = t5.CID + "/" + t4.Style,
                             ItemName = t7.Name,
                             OrderQuantity = t4.Quantity * t4.QPack,
                             MachineQuantity = t1.MachineQuantity,
                             Capacity = t1.Capacity,
                             TargetDays = t1.TargetDays,
                             WorkingHours = t1.WorkingHour
                         }).OrderByDescending(x => x.PlanningID).AsEnumerable();
                return Json(v, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTimeAndActionStatusJsonResult(int orderID)
        {

            db = new xOssContext();
            var a = (from t1 in db.Plan_OrderStep
                     join t2 in db.Plan_OrderProcess on t1.Plan_OrderProcessFK equals t2.ID
                     join t3 in db.Plan_OrderProcessCategory on t2.Plan_OrderProcessCategoryFk equals t3.ID
                     join t4 in db.Common_TheOrder on t1.CommonTheOrderFK equals t4.ID
                     where t1.Active == true && t1.CommonTheOrderFK == orderID
                     select new
                     {
                         StepID = t2.ID,
                         StepName = t2.StepName,
                         PlannedStartDate = t1.PlannedStartDate,
                         ActualStartDate = t1.ActualStartDate,
                         PlannedFinishDate = t1.PlannedFinishDate,
                         ActualFinishDate = t1.ActualFinishDate,
                         AssignedTo = t1.AssignedTo,
                         Remarks = t1.Remarks
                     }).OrderByDescending(x => x.StepID).AsEnumerable();

            return Json(a, JsonRequestBehavior.AllowGet);

        }

        internal JsonResult GetPlan_OrderStepProgressBar(int id)
        {
            db = new xOssContext();
            var a = (from t1 in db.Plan_OrderStep
                     join t2 in db.Plan_OrderProcess on t1.Plan_OrderProcessFK equals t2.ID
                     join t3 in db.Plan_OrderProcessCategory on t2.Plan_OrderProcessCategoryFk equals t3.ID
                     join t4 in db.Common_TheOrder on t1.CommonTheOrderFK equals t4.ID
                     where t1.Active == true && t1.CommonTheOrderFK == id
                     select new
                     {
                         StepID = t2.ID,
                         StepName = t2.StepName,
                         PlannedStartDate = t1.PlannedStartDate,
                         ActualStartDate = t1.ActualStartDate,
                         PlannedFinishDate = t1.PlannedFinishDate,
                         ActualFinishDate = t1.ActualFinishDate,
                         AssignedTo = t1.AssignedTo,
                         Remarks = t1.Remarks,
                         CommonTheOrderFK = t1.CommonTheOrderFK,
                         OrderDate = t4.OrderDate
                     }).OrderByDescending(x => x.StepID).AsEnumerable();
            DataTable dt = new DataTable("");

            if (a.Count() != 0)
            {
                var maxDate = (from t1 in a where t1.CommonTheOrderFK == id select t1.PlannedStartDate).
                    Union((from t1 in a where t1.CommonTheOrderFK == id select t1.PlannedFinishDate)).
                    Union((from t1 in a where t1.CommonTheOrderFK == id select t1.ActualStartDate)).
                    Union((from t1 in a where t1.CommonTheOrderFK == id select t1.ActualFinishDate)).Max(dateTime => dateTime.Date);

                var minDate = (from t1 in a where t1.CommonTheOrderFK == id select t1.PlannedStartDate).
                        Union((from t1 in a where t1.CommonTheOrderFK == id select t1.PlannedFinishDate)).
                        Union((from t1 in a where t1.CommonTheOrderFK == id select t1.ActualStartDate)).
                        Union((from t1 in a where t1.CommonTheOrderFK == id select t1.ActualFinishDate)).Min(dateTime => dateTime.Date);


                dt.Columns.Add("StepName", typeof(string));
                dt.Columns.Add("AssignedTo", typeof(string));

                for (double i = 0; i <= (maxDate - minDate).TotalDays; i++)
                {
                    dt.Columns.Add(minDate.AddDays(i).ToString(CultureInfo.CurrentCulture).Substring(0, 10), typeof(string));
                }

                foreach (var vp in a)
                {
                    DataRow theRow = dt.NewRow();
                    theRow[0] = vp.StepName;
                    theRow[1] = vp.AssignedTo;
                    int count = 0, rowCount = 0;
                    bool running = false;
                    DateTime dateTime1 = vp.OrderDate;
                    DateTime dateTime2 = vp.PlannedStartDate;

                    //if (dateTime2 > dateTime1)
                    //{
                    foreach (DataColumn cl in dt.Columns)
                    {
                        if (count > 1)
                        {
                            if (cl.ColumnName == vp.PlannedStartDate.ToString(CultureInfo.InvariantCulture).Substring(0, 10))
                            {
                                theRow[count] = "#";// cl.ColumnName;
                                running = true;
                            }
                            else if (cl.ColumnName == vp.PlannedFinishDate.ToString(CultureInfo.InvariantCulture).Substring(0, 10))
                            {
                                theRow[count] = "#";// cl.ColumnName;
                                running = false;
                            }
                            else if (running == true)
                            {
                                theRow[count] = "#";// cl.ColumnName;
                            }
                            else
                            {
                                theRow[count] = "";
                            }

                        }
                        count++;
                    }

                    //}                 
                    running = false;
                    count = 0;
                    rowCount = 0;
                    //if (dateTime2 > dateTime1)
                    //{
                    foreach (DataColumn cl in dt.Columns)
                    {

                        if (count > 1)
                        {
                            if (cl.ColumnName == vp.ActualStartDate.ToString(CultureInfo.CurrentCulture).Substring(0, 10))
                            {
                                theRow[count] = theRow[count] + "*";// cl.ColumnName;
                                running = true;
                            }
                            else if (cl.ColumnName == vp.ActualFinishDate.ToString(CultureInfo.CurrentCulture).Substring(0, 10))
                            {
                                theRow[count] = theRow[count] + "*"; // cl.ColumnName;
                                running = false;
                            }
                            else if (running == true)
                            {
                                theRow[count] = theRow[count] + "*";// cl.ColumnName;
                            }
                            //else
                            //{
                            //    theRow[count] = "";
                            //}

                        }
                        count++;
                    }
                    // }

                    dt.Rows.InsertAt(theRow, rowCount++);
                }

                DataTable = dt;
            }

            return Json(DataTable, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetItemByOrder(int bomid)
        {
            db = new xOssContext();
            var list = new List<object>();
            var items = (from t1 in db.Mkt_POSlave
                         join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                         join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                         where t2.Mkt_BOMFK == bomid && t1.Active == true && t2.Active == true && t3.Active == true
                         select new
                         {
                             ID = t1.ID,
                             Name = t3.Name + " | " + t1.Description
                         });
            foreach (var item in items)
            {
                list.Add(new { Text = item.Name, Value = item.ID });
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrderByBuyer(int buyerId)
        {
            db = new xOssContext();
            var list = new List<object>();
            var order = (from t1 in db.Mkt_BOM
                         join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                         where t2.Mkt_BuyerFK == buyerId && t1.Active == true && t2.Active == true
                         select new
                         {
                             ID = t1.ID,
                             Name = t2.CID + " | " + t1.Style
                         });
            foreach (var item in order)
            {
                list.Add(new { Text = item.Name, Value = item.ID });
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetYarnItemByOrder(int bomid)
        {
            db = new xOssContext();
            var list = new List<object>();
            var items = (from t1 in db.Mkt_POSlave
                         join t2 in db.Mkt_PO on t1.Mkt_POFK equals t2.ID
                         join t3 in db.Raw_Item on t1.Raw_ItemFK equals t3.ID
                         join t4 in db.Raw_SubCategory on t3.Raw_SubCategoryFK equals t4.ID
                         where t2.Mkt_BOMFK == bomid && t4.Raw_CategoryFK == 1020 && t1.Active == true && t2.Active == true && t3.Active == true

                         || t2.Mkt_BOMFK == bomid && t4.Raw_CategoryFK == 1013 && t1.Active == true && t2.Active == true && t3.Active == true

                         select new
                         {
                             POID = t2.ID,
                             ID = t1.ID,
                             Name = t3.Name + " | " + t1.Description
                         }).OrderByDescending(x => x.POID);
            foreach (var item in items)
            {
                list.Add(new { Text = item.Name, Value = item.ID });
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetShipmentOrderDeliverdScheduleByBOM(int bomid)
        {
            db = new xOssContext();
            var list = new List<object>();
            var items = (from t1 in db.Shipment_OrderDeliverdSchedule
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryFk equals t2.ID
                         join t3 in db.Common_Country on t2.Destination equals t3.ID
                         where t1.Mkt_BOMFK == bomid && t1.Active == true

                         select new
                         {
                             Port = t3.Name + " | " + t2.PortNo + "(" + t1.Date + ") Quantity - " + t1.DeliverdQty,
                             ID = t1.ID,

                         }).OrderByDescending(x => x.ID);
            foreach (var item in items)
            {
                list.Add(new { Text = item.Port, Value = item.ID });
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetshipmentOrderDeliverdScheduleQty(int ID)
        {
            db = new xOssContext();
            var items = (from t1 in db.Shipment_OrderDeliverdSchedule
                         join t2 in db.Mkt_OrderDeliverySchedule on t1.Mkt_OrderDeliveryFk equals t2.ID
                         join t3 in db.Common_Country on t2.Destination equals t3.ID
                         where t1.ID == ID && t1.Active == true

                         select new
                         {
                             Quantity = t1.DeliverdQty,
                             Date = t1.Date,
                             Port = t3.Name + " | " + t2.PortNo
                         }).FirstOrDefault();
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCurrentCurrencyRate(int ID)
        {
            if (ID == 2)
            {
                return Json(new { CurrencyRate = 1, ReadOnly = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                db = new xOssContext();
                var rate = db.Acc_DollarRate.Where(x => x.EffectDate <= DateTime.Now).OrderByDescending(x => x.EffectDate).Take(1).SingleOrDefault();
                return Json(new { CurrencyRate = rate.DollarRate, ReadOnly = false }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetDateWiseCurrencyRate(int ID, DateTime date)
        {
            if (ID == 2)
            {
                return Json(new { CurrencyRate = 1, ReadOnly = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                db = new xOssContext();
                var rate = db.Acc_DollarRate.Where(x => x.EffectDate <= date).OrderByDescending(x => x.EffectDate).Take(1).SingleOrDefault();
                return Json(new { CurrencyRate = rate.DollarRate, ReadOnly = false }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetASupplierByCnfTransport(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (ID == 1)
                return Json(db.Common_Supplier.Where(a => a.Common_SupplierTypeFk == 2).Where(c => c.Active == true), JsonRequestBehavior.AllowGet);
            else if (ID == 2)
                return Json(db.Common_Supplier.Where(a => a.Common_SupplierTypeFk == 3).Where(c => c.Active == true), JsonRequestBehavior.AllowGet);
            else
                return Json(db.Common_Supplier.Where(c => c.Active == true), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrderDeliverdScheduleByBOM(int bomid)
        {
            db = new xOssContext();
            var list = new List<object>();
            // decimal deliverdQty = (from t4 in db.Shipment_OrderDeliverdSchedule
            // where t4.Mkt_OrderDeliveryFk == 664 && t4.Active == true
            // select t4.DeliverdQty).ToList().DefaultIfEmpty(0).Sum();

            var items = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t3 in db.Common_Country on t1.Destination equals t3.ID
                         join t4 in db.Mkt_BOM on t1.Mkt_BOMFk equals t4.ID
                         where t1.Mkt_BOMFk == bomid && t1.Active == true
                         select new
                         {
                             Port = t3.Name + " | " + t1.PortNo,
                             ID = t1.ID,
                             Date= t1.Date,
                             PCS = t1.Quantity / t4.SetQuantity,
                             AvailablePCS = ((t1.Quantity - (from t4 in db.Shipment_OrderDeliverdSchedule
                                                            where t4.Mkt_OrderDeliveryFk == t1.ID && t4.Active == true
                                                            select t4.DeliverdQty).ToList().DefaultIfEmpty(0).Sum()) / t4.SetQuantity),
                             Pack = ((t1.Quantity / t4.Quantity) / t4.SetQuantity),
                             AvailablePack = (((t1.Quantity - (from t4 in db.Shipment_OrderDeliverdSchedule
                                                              where t4.Mkt_OrderDeliveryFk == t1.ID && t4.Active == true
                                                              select t4.DeliverdQty).ToList().DefaultIfEmpty(0).Sum()) / t4.Quantity) / t4.SetQuantity)
                         }).OrderByDescending(x => x.ID).ToList();
            foreach (var item in items)
            {
                list.Add(new
                {
                    Text = "Port: <b>" + item.Port + "</b>(" + item.Date.ToShortDateString() + ")" + " PCS: " + item.PCS.ToString("##,###") + " Available PCS:" + item.AvailablePCS.ToString("##,###") + " Pack: " + item.Pack.ToString("##,###") + " Available Pack: " + item.AvailablePack.ToString("##,###"),
                    Value = item.ID,
                    //AvailablePack = item.AvailablePack
                });

              
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAvailablePackQuantity(int mktDeliveryId)
        {
            db = new xOssContext();
          

            var items = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t3 in db.Common_Country on t1.Destination equals t3.ID
                         join t4 in db.Mkt_BOM on t1.Mkt_BOMFk equals t4.ID
                         where t1.ID == mktDeliveryId && t1.Active == true
                         select new
                         {                             
                             AvailablePack = (((t1.Quantity  - (from t4 in db.Shipment_OrderDeliverdSchedule
                                                               where t4.Mkt_OrderDeliveryFk == t1.ID && t4.Active == true
                                                               select t4.DeliverdQty).ToList().DefaultIfEmpty(0).Sum()) / t4.Quantity) / t4.SetQuantity)
                         }).FirstOrDefault();   
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrderBySMVPaper(int Id)
        {
            db = new xOssContext();

            var vData = (from o in db.Prod_SMVLayout
                         join p in db.Mkt_BOM on o.Mkt_BOMFk equals p.ID
                         join q in db.Mkt_Item on p.Mkt_ItemFK equals q.ID
                         where o.ID == Id && o.Active == true
                         select new
                         {
                             Mkt_BOMFk = o.Mkt_BOMFk,
                             NoOfOperator = o.NoOfOperator,
                             NoOfHelper = o.NoOfHelper,
                             TotalSMV = o.TotalSMV,
                             HourTarget=o.PerHourTarget,
                             DailyWorkHour = o.DailyWorkHour,
                             OrderItem = o.Description + "/" + q.Name,
                         }).AsEnumerable();
            if (vData.Any())
            {
                var getData = vData.FirstOrDefault();
                return Json(getData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetLineWiseChiefSupervisor(int LineId)
        {
            db = new xOssContext();
            var vData = (from o in db.Prod_LineOfficer
                         join p in db.Prod_LineChief on o.Prod_LineChiefFK equals p.ID
                         join q in db.Prod_SVName on o.Prod_LineSuperVisorFk equals q.ID
                         where o.Plan_ProductionLineFK == LineId && o.Active == true
                         select new
                         {
                             CheifId = p.ID,
                             CheifName = p.Name,
                             SVID = q.ID,
                             SVName = q.Name
                         }).ToList();

            if (vData.Any())
            {
                var getData = vData.FirstOrDefault();
                return Json(getData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult TimeAndActionUpdate(int ID, string updateVal, string actionName)
        {
            try
            {
                db = new xOssContext();
                var data = db.Plan_OrderStep.Find(ID);
                if (actionName == "PlannedStartDate")
                {
                    DateTime date = DateTime.ParseExact(updateVal, "yyyy/M/d",
                                          System.Globalization.CultureInfo.InvariantCulture);
                    data.PlannedStartDate = date;
                }
                else if (actionName == "PlannedFinishDate")
                {
                    DateTime date = DateTime.ParseExact(updateVal, "yyyy/M/d",
                                          System.Globalization.CultureInfo.InvariantCulture);
                    data.PlannedFinishDate = date;
                }
                else if (actionName == "ActualStartDate")
                {
                    DateTime date = DateTime.ParseExact(updateVal, "yyyy/M/d",
                                          System.Globalization.CultureInfo.InvariantCulture);
                    data.ActualStartDate = date;
                }
                else if (actionName == "ActualFinishDate")
                {
                    DateTime date = DateTime.ParseExact(updateVal, "yyyy/M/d",
                                          System.Globalization.CultureInfo.InvariantCulture);
                    data.ActualFinishDate = date;
                }
                else if (actionName == "Remarks")
                {
                    data.Remarks = updateVal;
                }
                var s = db.Entry(data).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { Update = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Update = false, ExceptionName = ex }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetSwingOrderByLine(int ID, int PlanOrderRefId, int BomId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var vData = (from o in db.Prod_PlanReferenceOrder
                         join q in db.Plan_ProductionLine on o.Plan_ProductionLineFk equals q.ID
                         where 
                         o.Prod_ReferenceFK == PlanOrderRefId 
                         && o.MktBomFk == BomId 
                         && o.SectionId == 2
                         && o.ID==ID
                         group o.SMV by new { q.Name, o.Plan_ProductionLineFk } into a
                         select new
                         {
                             Name = a.Key.Name,
                             ID = a.Key.Plan_ProductionLineFk
                         }).OrderBy(a => a.ID).ToList();

            if (vData.Any())
            {
                return Json(vData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSwingOrderByColor(int BomId)
        {
            db = new xOssContext();
            var vData = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                         where t1.Active == true && t2.Active == true && t1.Mkt_BOMFk == BomId
                         group t1.Quantity by new { t2.Color } into a
                         select new
                         {
                             ID = a.Key.Color,
                             Name = a.Key.Color
                         }).ToList();
            if (vData.Any())
            {
                return Json(vData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetColorByOrderID(int BomId)
        {
            db = new xOssContext();
            var vData = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                         where t1.Active == true && t2.Active == true && t1.Mkt_BOMFk == BomId
                         group t2.Quantity by new { t2.Color } into a
                         select new
                         {
                             ID = a.Key.Color,
                             Name = a.Key.Color + "[ "+a.Sum()+" ]"
                         }).ToList();
            if (vData.Any())
            {
                return Json(vData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSizeByOrderColorID(int BomId,string Color)
        {
            db = new xOssContext();
            var vData = (from t1 in db.Mkt_OrderDeliverySchedule
                         join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
                         where t1.Active == true && t2.Active == true && t1.Mkt_BOMFk == BomId && t2.Color.Equals(Color)
                         group t2.Quantity by new { t2.Size } into a
                         select new
                         {
                             ID = a.Key.Size,
                             Name = a.Key.Size + "[ " + a.Sum() + " ]"
                         }).ToList();
            if (vData.Any())
            {
                return Json(vData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetRequisitionAvailableByColorSizeID(int ColorSizeId)
        {
            db = new xOssContext();
            //var vData = (from t1 in db.Mkt_OrderDeliverySchedule
            //             join t2 in db.Mkt_OrderColorAndSizeRatio on t1.ID equals t2.OrderDeliveryScheduleFk
            //             where t1.Active == true && t2.Active == true && t1.Mkt_BOMFk == BomId
            //             select new
            //             {
            //                 ID = t2.ID,
            //                 Name = t2.Color + "[ " + t2.Size + " ]" + " - " + t2.Quantity
            //             }).ToList();
            //if (vData.Any())
            //{
            //    return Json(vData, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json("", JsonRequestBehavior.AllowGet);
            //}
            int Qty = 0;
            return Json(Qty, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckOverlapLine(int LineId, DateTime StartDate, DateTime FinishDate)
        {
            db = new xOssContext();
            bool IsNotOverlaped = true;

            var vData = (from t1 in db.Prod_Planning
                         where t1.IsClosed == false && t1.Plan_ProductionLineFk == LineId
                         select new
                         {
                             startDate = t1.StartDate,
                             finishDate = t1.FinishDate
                         }).ToList();
            if (vData.Any())
            {
                if (vData.Any(a => a.startDate >= StartDate && a.finishDate >= StartDate && a.startDate<=FinishDate && a.finishDate>=FinishDate))
                {
                    IsNotOverlaped = false;
                }
                else if (vData.Any(a => a.startDate <= StartDate && a.finishDate >= StartDate && a.startDate<=FinishDate && a.finishDate<=FinishDate))
                {
                    IsNotOverlaped = false;
                }
                else if (vData.Any(a => a.startDate <= StartDate && a.finishDate>=StartDate && a.startDate<=FinishDate && a.finishDate >= FinishDate))
                {
                    IsNotOverlaped = false;
                }
                else if (vData.Any(a => a.startDate > StartDate && a.finishDate> StartDate && a.startDate<FinishDate && a.finishDate < FinishDate))
                {
                    IsNotOverlaped = false;
                }
            }
            return Json(IsNotOverlaped, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllShipmentInfo(int BomId)
        {
            db = new xOssContext();
            
            var vPlaned = (from t1 in db.Prod_OrderPlanning
                        where t1.Mkt_BOMFK == BomId && t1.Active == true
                        select new DropList
                        {
                            ID = t1.ShipmentDate,
                            Value = 0
                        }).ToList();
            
            var vShipment = (from t1 in db.Mkt_OrderDeliverySchedule
                             join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                             join t3 in db.Mkt_BOM on t1.Mkt_BOMFk equals t3.ID
                             where t1.Active == true && t3.Active == true && t2.Active == true && t1.Mkt_BOMFk == BomId
                             group t1.Quantity by new { t1.Date } into a
                             select new DropList
                             {
                                ID = (DateTime)a.Key.Date,
                                Value = (int)a.Sum()
                         }).ToList();
            
            if (vShipment.Any())
            {
                List<DropList> lst = new List<DropList>();
                foreach (var v in vShipment)
                {
                    if (!vPlaned.Any(a=>a.ID==v.ID))
                    {
                        v.Name = v.ID;
                        lst.Add(v);
                    }
                    v.Name = v.ID;
                    //v.Name = string.Format("{0:MM/dd/yyyy}", v.ID) + " " + v.Value;
                }


                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetActiveBuyerPO()
        {
            db = new xOssContext();
            var list = new List<object>();
            var order = db.Common_TheOrder.Where(d => d.Active == true && d.IsComplete == false).Select(a => new {
                ID = a.ID,
                Name = a.CID
            });
                         
            foreach (var item in order)
            {
                list.Add(new { Text = item.Name, Value = item.ID });
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActiveBuyer()
        {
            db = new xOssContext();
            var list = new List<object>();
            var order = (from o in db.Common_TheOrder
                         join p in db.Mkt_Buyer on o.Mkt_BuyerFK equals p.ID
                         where o.Active == true && o.IsComplete == false && p.Active == true
                         group o.BuyerPO by new { p.Name,p.ID } into g
                         select new {
                             ID = g.Key.ID,
                             Name = g.Key.Name
                         });
                
            foreach (var item in order)
            {
                list.Add(new { Text = item.Name, Value = item.ID });
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFreeLine(DateTime StartDate, DateTime FinishDate)
        {
            db = new xOssContext();
            var list = new List<object>();
            var vPlanLine = (from a in db.Prod_Planning
                             join b in db.Plan_ProductionLine on a.Plan_ProductionLineFk equals b.ID
                             where a.Active == true && a.IsClosed == false
                             && (a.StartDate >= StartDate && a.StartDate <= FinishDate)|| (a.StartDate <= StartDate && a.FinishDate >= FinishDate) || (a.FinishDate >= StartDate && a.FinishDate <= FinishDate)
                             select new
                             {
                                 ID = a.Plan_ProductionLineFk,
                                 Name = b.Name
                             }).ToList();

            var vAll = db.Plan_ProductionLine.Where(d => d.Plan_OrderProcessFK == 32).Select(a=> new { ID=a.ID, Name=a.Name }).ToList();
            
            foreach (var item in vAll)
            {
                if (!vPlanLine.Any(a=>a.ID==item.ID))
                {
                    list.Add(new { Text = item.Name, Value = item.ID });
                }
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInputReqDepartment(int ID)
        {
            db = new xOssContext();
            var list = new List<object>();
            var vData = (from a in db.Prod_InputRequisition
                             join b in db.User_Department on a.FromUser_DeptFK equals b.ID
                             where a.Active == true && a.ID==ID
                             select new
                             {
                                 ID = a.FromUser_DeptFK,
                                 Name = b.Name
                             }).ToList();
            if (vData.Any())
            {
                var take = vData.FirstOrDefault();
                return Json(take, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInputReqItemQty(int ID,int userId)
        {
            db = new xOssContext();
            MixData model = new MixData();
            List<MixData> lstData = new List<MixData>();
            var list = new List<object>();
            var vData = (from a in db.Prod_InputRequisitionSlave
                         where a.Active == true && a.ID == ID
                         select new
                         {
                             ID = a.ID,
                             bomId = a.Mkt_BOMFK,
                             color = a.ColorName,
                             size = a.Size,
                             reqQty = a.TotalRequired
                         }).FirstOrDefault();

            var vTransfer = from o in db.Prod_FinishItemTransfer
                            join p in db.Prod_FinishItemTransferSlave on o.ID equals p.Prod_FinishItemTransferFK
                            join q in db.Prod_InputRequisitionSlave on p.Prod_InputRequisitionSlaveFK equals q.ID
                            where p.Active == true && o.IsAuthorize == true && q.ColorName.Equals(vData.color) && q.Size.Equals(vData.size)
                            group p.Quantity by new { q.Size } into all
                            select new { qty = all.Sum() };
            if (userId == 9)
            {
                var vCutting = from o in db.Prod_PlanReferenceOrder
                               join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                               join q in db.Mkt_OrderColorAndSizeRatio on p.Mkt_OrderColorAndSizeRatioFk equals q.ID
                               join r in db.Mkt_OrderDeliverySchedule on q.OrderDeliveryScheduleFk equals r.ID
                               where p.Plan_ReferenceSectionFk == 1 && r.Mkt_BOMFk == vData.bomId && q.Color.Equals(vData.color) && q.Size.Equals(vData.size)
                               group p.Quantity by new { q.Size } into all
                               select new { qty = all.Sum() };

                model.TotalFollowup = vCutting.Any() == true ? (int)vCutting.Sum(a => a.qty) : 0;
            }
            else if (userId == 10)
            {
                var vSewing = from o in db.Prod_PlanReferenceOrder
                              join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                              join q in db.Mkt_OrderColorAndSizeRatio on p.Mkt_OrderColorAndSizeRatioFk equals q.ID
                              join r in db.Mkt_OrderDeliverySchedule on q.OrderDeliveryScheduleFk equals r.ID
                              where p.Plan_ReferenceSectionFk == 2 && r.Mkt_BOMFk == vData.bomId && q.Color.Equals(vData.color) && q.Size.Equals(vData.size)
                              group p.Quantity by new { q.Size } into all
                              select new { qty = all.Sum() };

                model.TotalFollowup = vSewing.Any() == true ? (int)vSewing.Sum(a => a.qty) : 0;
            }
            else if (userId == 11)
            {
                var vPacking = from o in db.Prod_PlanReferenceOrder
                              join p in db.Prod_PlanReferenceOrderSection on o.ID equals p.Prod_PlanReferenceOrderFk
                              join q in db.Mkt_OrderColorAndSizeRatio on p.Mkt_OrderColorAndSizeRatioFk equals q.ID
                              join r in db.Mkt_OrderDeliverySchedule on q.OrderDeliveryScheduleFk equals r.ID
                              where p.Plan_ReferenceSectionFk == 4 && r.Mkt_BOMFk == vData.bomId && q.Color.Equals(vData.color) && q.Size.Equals(vData.size)
                              group p.Quantity by new { q.Size } into all
                              select new { qty = all.Sum() };

                model.TotalFollowup = vPacking.Any() == true ? (int)vPacking.Sum(a => a.qty) : 0;
            }
            model.TotalTransfer = vTransfer.Any() == true ? (int)vTransfer.Sum(a => a.qty) : 0;
            model.AvailableQty = model.TotalFollowup - model.TotalTransfer;
            model.RequestQty = (int)vData.reqQty;
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrderStyleColor(int BomId)
        {
            db = new xOssContext();
            var list = new List<object>();

            var vData = (from a in db.Mkt_OrderDeliverySchedule
                         join b in db.Mkt_OrderColorAndSizeRatio on a.ID equals b.OrderDeliveryScheduleFk
                         where a.Active == true && b.Active == true && a.Mkt_BOMFk == BomId
                         group b.Quantity by new { b.Color } into all
                         select new
                         {
                             ID = all.Key.Color,
                             Name = all.Key.Color
                         }).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    list.Add(new { Text = v.Name, Value = v.ID });
                }
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}

public class DropList
{
    public DateTime ID { get; set; }
    
    public int Value { get; set; }

    public DateTime Name { get; set; }
}

public class MixData
{
    public int TotalFollowup { get; set; }

    public int TotalTransfer { get; set; }

    public int AvailableQty { get; set; }

    public int RequestQty { get; set; }
        
}