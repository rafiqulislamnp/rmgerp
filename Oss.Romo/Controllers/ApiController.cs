﻿using Newtonsoft.Json;
using Oss.Romo.Models;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Api;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Oss.Romo.Controllers
{
    [AllowAnonymous]
    public class ApiController : Controller
    {
        VmForDropDown VmForDropDown;
        public ApiController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        //VmControllerHelper VmControllerHelper;
        //[HttpGet]
        //public ActionResult GetOrderDetails(string buyerOrder,string buyerName, string fromDate, string toDate)
        //{

        //    VmApiOrder a = new VmApiOrder();
        //    a.BuyerOrtherSearch( buyerOrder, buyerName, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));

        //    return Json(a, JsonRequestBehavior.AllowGet);
        //}
 
        [HttpGet]
        public ActionResult Login(string userAndPass)
        {
            bool flag = false;
            int id = 0;
            VmApiUser v = new VmApiUser();
            string user = userAndPass.Substring(0, userAndPass.IndexOf("-"));
            string pass = userAndPass.Replace(user + "-", "");
            VmUser_User vmUser_User = new VmUser_User();
            User_User user_User = new User_User();
            user_User.UserName = user;
            user_User.Password = pass;
            vmUser_User.User_User = user_User;
            //if (vmUser_User.LoginUser() > 0)
            //{
            //    flag = true;
            //}
            id = vmUser_User.LoginUser(false);
            return Json(id, JsonRequestBehavior.AllowGet);



        }

        //...........UserProfile....................//
        [HttpGet]

        public ActionResult GetProfile(int id)
        {
            VmApiUser profile = new VmApiUser();
            profile.Profile(id);

            return Json(profile, JsonRequestBehavior.AllowGet);
        }

        //..............Order Get By YearMonth,  Day Wise...............//

        [HttpGet]
        public ActionResult OrderByDay(string p)
        {
            string year = p.Substring(0, 4);
            string month = p.Replace(year + "-", "");
            VmApiNumberCount a = new VmApiNumberCount();
            a.BuyerOrder = new List<BuyerOrderMonth>();
            a.BuyerOrder = a.OrderBuyerForMonth(Convert.ToInt32(year), Convert.ToInt32(month));

            return Json(a, JsonRequestBehavior.AllowGet);
        }


        //..............Get Only BuyerName...............//
        [HttpGet]
        public ActionResult GetBuyer()
        {
            VmBuyer b = new VmBuyer();
            b.GetBuyerName();

            return Json(b, JsonRequestBehavior.AllowGet);
        }

        //..............Get Only BuyerOrder ID...............//

        [HttpGet]
        public ActionResult GetOrderId()
        {
            VmOrderId b = new VmOrderId();
            b.GetOrderID();

            return Json(b, JsonRequestBehavior.AllowGet);
        }


        //..............Order Get By Year BuyerName,Month Wise...............//
        [HttpGet]
        public ActionResult GetByBuYearOrder(string p)
        {
            string year = p.Substring(0, 4);
            string BuyerName = p.Replace(year + "-", "");

            VmApiBuyerOrderCount a = new VmApiBuyerOrderCount();
            a.BuyerName = new List<BuyerCount>();
            a.BuyerName = a.BuyerNameWiseOrder(Convert.ToInt32(year), BuyerName);
            return Json(a, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetOrderDetails(string orderID, string buyerName, string fromDate, string toDate)
        {
            VmApiOrder a = new VmApiOrder();
            a.BuyerOtherSearch(orderID, buyerName, fromDate, toDate);

            return Json(a, JsonRequestBehavior.AllowGet);
        }



        //.............Get Order By Year, Month Wise Count..............//
        [HttpGet]
        public ActionResult OrderByYear(int p)
        {

            VmApiNumberCount a = new VmApiNumberCount();
            a.BuyerOrder = new List<BuyerOrderMonth>();
            a.BuyerOrder = a.OrderBuyMonth(p);

            return Json(a, JsonRequestBehavior.AllowGet);
        }
        //..............PO Get By YearMonth,Day Wise...............//

        [HttpGet]
        public ActionResult POGetByMonthYear(string p)
        {

            string year = p.Substring(0, 4);
            string month = p.Replace(year + "-", "");

            VmApiPOCount a = new VmApiPOCount();
            a.po = new List<PoCount>();

            a.po = a.PurchaseOrderByDay(Convert.ToInt32(year), Convert.ToInt32(month));
            return Json(a, JsonRequestBehavior.AllowGet);
        }



        //................Date Wise Purchase Order Search................//
        [HttpGet]
        public ActionResult POInDateRange(string from, string to)
        {
            VmApiPo a = new VmApiPo();
            a.PODate(Convert.ToDateTime(from), Convert.ToDateTime(to));

            return Json(a, JsonRequestBehavior.AllowGet);

        }



        //.............Get Master LC in Date And Month Wise............//

        [HttpGet]
        public ActionResult GetByMasterLc(string p)
        {

            string year = p.Substring(0, 4);
            string month = p.Replace(year + "-", "");

            VmApiMasterLC a = new VmApiMasterLC();
            a.masterLc = new List<MasterLC>();
            a.masterLc = a.GetMasterLc(Convert.ToInt32(year), Convert.ToInt32(month));
            return Json(a, JsonRequestBehavior.AllowGet);
        }

        //.............Get Purchase Item Buyer Order Wise............//
        [HttpGet]

        public ActionResult GetOrderPo(string s)
        {

            VmApiOrderPo a = new VmApiOrderPo();
            a.OrderPoWiseSearch(s);
            return Json(a, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetOrderPoDetails(string orderId, string poCid, string fromDate, string toDate)
        {
            VmApiOrderPo a = new VmApiOrderPo();
            a.OrderPoSearch(orderId, poCid, fromDate, toDate);

            return Json(a, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetOrderByPo(string orderId)
        {
            VmApiPoList a = new VmApiPoList();
            a.OrderByPo(orderId);

            return Json(a, JsonRequestBehavior.AllowGet);
        }





        //[HttpGet]
        //public ActionResult GetOrderSummary( string buyerName)
        //{


        //    VmOrderSummaryDetails a = new VmOrderSummaryDetails();
        //    a.OrderSummary(buyerName);

        //    return Json(a, JsonRequestBehavior.AllowGet);
        //}



        [HttpGet]
        public ActionResult GetOrderSummary(string buyerName, string date, string ID)
        {
            VmOrderSummaryDetails a = new VmOrderSummaryDetails();

            string noValue = string.Empty;
            
           if (string.IsNullOrEmpty(date))
            {
                a.OrderSummary(buyerName, noValue, noValue, ID);
            }
            else
            {
                string year = string.Empty;
                string month = string.Empty;
                year = date.Substring(0, 4);
                month = date.Replace(year + "-", "");

                a.OrderSummary(buyerName,year, month, ID);
            }

            return Json(a, JsonRequestBehavior.AllowGet);
           }



        [HttpGet]

        public ActionResult GetBuyerOrder( string name)
        {

            VmApiBuyerOrderList a = new VmApiBuyerOrderList();
            a.BuyerOrder(name);
            return Json(a, JsonRequestBehavior.AllowGet);


        }

        //public ActionResult GetUDSummary(string fromDate,string toDate)
        //{



        //    DateTime fdate = new DateTime();
        //    DateTime tdate = new DateTime();



        //    if (fromDate != "")
        //    {

        //        fdate = Convert.ToDateTime(fromDate);
        //    }
        //    if (toDate != "")
        //    {

        //        //tdate = Convert.ToDateTime(todate).AddDays(-30);
        //        tdate = Convert.ToDateTime(toDate);
        //    }

        //    VmCommercial_Report a = new VmCommercial_Report();

        //    //a.GetSummaryReport(fdate, tdate);

     
        //    return Json(a, JsonRequestBehavior.AllowGet);


        //}


        public ActionResult GetUDSummary()
        {

            VmCommercial_Report a = new VmCommercial_Report();
            a.GetSummaryReport();
            return Json(a, JsonRequestBehavior.AllowGet);


        }
    }

}
