﻿using Oss.Romo.Models;
using Oss.Romo.ViewModels.AssetManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models.AssetManagement;
using System.Data.Entity;
using Newtonsoft.Json;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.User;


namespace Oss.Romo.Controllers
{
    [SoftwareAdmin]
    public class AssetManagementController : Controller
    {
        public AssetManagementController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        private xOssContext db = new xOssContext();

        //GET: AssetManagement
        public ActionResult Index()
        {
            return View();
        }

        #region Category Info

        [HttpGet]
        public async Task<ActionResult> ViewAssetCategoryList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetCategory model = new VM_AssetCategory();
                    await Task.Run(() => model.LoadAssetCategory());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> AddAssetCategory()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetCategory model = new VM_AssetCategory();
                    await Task.Run(() => model.LoadAssetCategory());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        [HttpPost]
        public async Task<ActionResult> AddAssetCategory(VM_AssetCategory model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_Category categoryinfo = new Asset_Category()
                    {
                        CategoryName = model.CategoryName,
                        Status = model.Status,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now
                    };
                    db.AssetCategory.Add(categoryinfo);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset category Added Successfully.";
                    return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Asset Category's  can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        public async Task<ActionResult> EditAssetCategory(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    VM_AssetCategory model = new VM_AssetCategory();
                    await Task.Run(() => model = model.LoadSpecificAssetCategory(id.Value));
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Asset Category Not Found!";
                return RedirectToAction("AddAssetCategory", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAssetCategory(VM_AssetCategory model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_Category assetcategory = new Asset_Category
                    {
                        ID = model.ID,
                        CategoryName = model.CategoryName,
                        Status = model.Status,
                        Update_Date = DateTime.Now,
                        Update_By = user.User_User.ID.ToString()
                    };
                    db.Entry(assetcategory).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Category Info Updated Successfully.";
                    return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");

        }

        [HttpGet]
        public async Task<ActionResult> DeleteAssetCategory(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Asset Category Info Not Found!";
                    return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                }
                Asset_Category assetcategory = await db.AssetCategory.FindAsync(id);
                if (assetcategory != null)
                {
                    try
                    {
                        db.AssetCategory.Remove(assetcategory);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Asset Category Deleted Successfully.";
                        return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Asset Category Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAssetCategoryList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #region SubCategory Info

        [HttpGet]
        public async Task<ActionResult> ViewAssetSubCategoryList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    VM_AssetSubCategory model = new VM_AssetSubCategory();
                    await Task.Run(() => model.LoadAssetSubCategory());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> AddAssetSubCategory()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.AssetProductType = new SelectList(ddd.GetAssetCategoryList(), "Value", "Text");
                    VM_AssetSubCategory model = new VM_AssetSubCategory();
                    await Task.Run(() => model.LoadAssetSubCategory());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> AddAssetSubCategory(VM_AssetSubCategory model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_SubCategory subcategoryinfo = new Asset_SubCategory()
                    {
                        CategoryId = model.CategoryId,
                        SubCategoryName = model.SubCategoryName,
                        Status = model.Status,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now
                    };
                    db.AssetSubCategory.Add(subcategoryinfo);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Sub category Added Successfully.";
                    return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Asset Sub category Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        public async Task<ActionResult> EditAssetSubCategory(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    VM_AssetSubCategory model = new VM_AssetSubCategory();
                    await Task.Run(() => model = model.LoadSpecificAssetSubCategory(id.Value));
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Asset Sub Category Info Not Found!";
                return RedirectToAction("AddAssetSubCategory", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAssetSubCategory(VM_AssetSubCategory model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_SubCategory assetsubcategory = new Asset_SubCategory
                    {
                        ID = model.ID,
                        SubCategoryName = model.SubCategoryName,
                        Status = model.Status,
                        Update_Date = DateTime.Now,
                        Update_By = user.User_User.ID.ToString()
                    };
                    db.Entry(assetsubcategory).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Sub Category Info Updated Successfully.";
                    return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");

        }

        [HttpGet]
        public async Task<ActionResult> DeleteAssetSubCategory(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Asset Category Info Not Found!";
                    return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                }
                Asset_SubCategory assetsubcategory = await db.AssetSubCategory.FindAsync(id);
                if (assetsubcategory != null)
                {
                    try
                    {
                        db.AssetSubCategory.Remove(assetsubcategory);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Asset Category Deleted Successfully.";
                        return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Asset Category Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAssetSubCategoryList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #region Asset Product Type Entry
        public ActionResult AssetProductType()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                ViewBag.Type = new SelectList(DropDownData.GetAssetType(), "Value", "Text");
                RedirectToAction("AddAssetProduct", "AssetManagement");
            }
            return View();

        }

        [HttpGet]
        public async Task<ActionResult> ViewAssetProductList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetProduct model = new VM_AssetProduct();
                    await Task.Run(() => model.LoadAssetProductList());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> AddAssetProduct()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.AssetCategoryList = new SelectList(ddd.GetAssetCategoryList(), "Value", "Text");
                    ViewBag.AssetSubCategoryList = new SelectList(ddd.GetAssetSubCategoryList(), "Value", "Text");
                    VM_AssetProduct model = new VM_AssetProduct();
                    await Task.Run(() => model.LoadAssetProductList());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAssetProductList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> AddAssetProduct(VM_AssetProduct model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_Product product = new Asset_Product
                    {
                        ProductName = model.ProductName,
                        ProductAbbreviation = model.ProductAbbreviation,
                        CategoryId = model.CategoryId,
                        //SubCategoryId = model.SubCategoryId,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now
                    };
                    db.AssetProducts.Add(product);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Product Added Successfully.";
                    return RedirectToAction("ViewAssetProductList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Asset Product Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAssetProductList", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAssetProductList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        public async Task<ActionResult> EditAssetProduct(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {

                    //DropDownData ddd = new DropDownData();
                    //VM_AssetProduct model = new VM_AssetProduct();
                    //List<SelectListItem> list = new List<SelectListItem>();
                    //list.Add(new SelectListItem { Text = "", Value = "Selected Items" });
                    //var categories = (from c in db.AssetSubCategory select c).ToArray();
                    //for (int i = 0; i < categories.Length; i++)
                    //{
                    //    list.Add(new SelectListItem
                    //    {
                    //        Text = categories[i].SubCategoryName,
                    //        Value = categories[i].CategoryId.ToString()
                    //    });
                    //}
                    //ViewData["categories"] = list;
                    //return View();
                    //}


                    //DropDownData ddd = new DropDownData();
                    //VM_AssetProduct model = new VM_AssetProduct();
                    //ViewBag.AssetCategoryList = new SelectList(ddd.AssetCategoryList(), "Value", "Text");
                    //await Task.Run(() => model = model.LoadSpecificAssetProductInfo(id.Value));
                    //return View(model);

                    try
                    {
                        DropDownData ddd = new DropDownData();
                        //VM_AssetProduct model = new VM_AssetProduct();
                        ViewBag.AssetCategoryList = new SelectList(ddd.GetAssetCategoryList(), "Value", "Text");
                        ViewBag.AssetSubCategoryList = new SelectList(ddd.GetAssetSubCategoryList(), "Value", "Text");
                        VM_AssetProduct m = (from t1 in db.AssetProducts
                                                 join c in db.AssetCategory on t1.CategoryId equals c.ID
                                                 
                                                 where t1.Status == true && t1.ID == id
                                                 select new VM_AssetProduct
                                                 {
                                                     ID = t1.ID,
                                                     ProductName = t1.ProductName,
                                                     CategoryName = c.CategoryName,
                                                     //SubCategoryName = d.SubCategoryName,
                                                     ProductAbbreviation = t1.ProductAbbreviation,
                                                     Status = t1.Status,

                                                 }).FirstOrDefault();
                        return View(m);
                    }
                    catch (Exception ex)
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Error Happened. " + ex.Message;
                        return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                    }



                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Asset Product Not Found!";
                return RedirectToAction("AddAssetProduct", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAssetProduct(VM_AssetProduct model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_Product assetProduct = new Asset_Product
                    {
                        ID = model.ID,
                        ProductName = model.ProductName,
                        ProductAbbreviation = model.ProductAbbreviation,
                        CategoryId = model.CategoryId,
                        //SubCategoryId = model.SubCategoryId,
                        Update_Date = DateTime.Now,
                        Update_By = user.User_User.ID.ToString()
                    };
                    db.Entry(assetProduct).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Product Type Updated Successfully.";
                    return RedirectToAction("ViewAssetProductList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAssetProductList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");

        }

        [HttpGet]
        public async Task<ActionResult> DeleteAssetProduct(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Asset Product Type Not Found!";
                    return RedirectToAction("ViewAssetProductList", "AssetManagement");
                }
                Asset_Product assetProduct = await db.AssetProducts.FindAsync(id);
                if (assetProduct != null)
                {
                    try
                    {
                        db.AssetProducts.Remove(assetProduct);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Asset Product Deleted Successfully.";
                        return RedirectToAction("ViewAssetProductList", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Asset Product Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAssetProductList", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAssetProductList", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAssetProductList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #region Asset/Accessories Brand
        [HttpGet]
        public async Task<ActionResult> ViewAssetProductBrandList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                //try
                //{
                //    Asset_ProductBrand brand = new Asset_ProductBrand();
                //    Asset_Product productType = new Asset_Product();

                //    VM_AssetProductBrand model = new VM_AssetProductBrand();
                //    model.DataList = (from t1 in db.AssetProductBrands
                //                      join c in db.AssetProducts on t1.ProductId equals c.ID
                //                      where t1.Status == true
                //                      select new VM_AssetProductBrand
                //                      {
                //                          ID = t1.ID,
                //                          ProductName = c.ProductName,
                //                          BrandName = t1.BrandName
                //                      }).AsEnumerable();

                //    return View(model);
                //}
                //catch (Exception ex)
                //{

                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Error Happened." + ex.Message;
                //    return RedirectToAction("Index", "AssetManagement");
                //}


                Asset_ProductBrand brand = new Asset_ProductBrand();
                Asset_Product productType = new Asset_Product();

                VM_AssetProductBrand model = new VM_AssetProductBrand();
                model.DataList = (from t1 in db.AssetProductBrands
                                  join c in db.AssetProducts on t1.ProductId equals c.ID
                                  where t1.Status == true
                                  select new VM_AssetProductBrand
                                  {
                                      ID = t1.ID,
                                      ProductName = c.ProductName,
                                      BrandName = t1.BrandName
                                  }).AsEnumerable();

                return View(model);

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        // Below Action used to Add AssetBrand Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductBrand
        // Domain Model Name: Asset_ProductBrand
        // View Model Name: VMAssetProductBrand
        // Developed by: Oyaliul and Rozy
        // Date: 2018-04-16
        [HttpGet]
        public async Task<ActionResult> AddAssetProductBrand()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {



                //try
                //{
                //    DropDownData ddd = new DropDownData();
                //    ViewBag.AssetProductType = new SelectList(ddd.GetAssetProductList(), "Value", "Text");
                //    VM_AssetProductBrand model = new VM_AssetProductBrand();
                //    return View(model);
                //}
                //catch (Exception ex)
                //{
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Error Happened." + ex.Message;
                //    return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                //}

                DropDownData ddd = new DropDownData();
                ViewBag.AssetProductType = new SelectList(ddd.GetAssetProductList(), "Value", "Text");
                VM_AssetProductBrand model = new VM_AssetProductBrand();
                return View(model);
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }




        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Rozy 
        // Date: 2018-05-17

        [HttpPost]
        public async Task<ActionResult> AddAssetProductBrand(VM_AssetProductBrand model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                //try
                //{
                //    Asset_ProductBrand assetbrand = new Asset_ProductBrand { BrandName = model.BrandName, ProductId = model.ProductId, IsAccessories = 0, Entry_By = user.User_User.ID.ToString(), Entry_Date = DateTime.Now };
                //    db.AssetProductBrands.Add(assetbrand);
                //    await db.SaveChangesAsync();

                //    Session["success_div"] = "true";
                //    Session["success_msg"] = "Asset Brand  Added Successfully.";
                //    return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                //}
                //catch (Exception ex)
                //{
                //    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                //    {
                //        Session["warning_div"] = "true";
                //        Session["warning_msg"] = "Asset Brand Can't Add, It is Already Exist!";
                //        return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                //    }
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                //    return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                //}


                Asset_ProductBrand assetbrand = new Asset_ProductBrand { BrandName = model.BrandName, ProductId = model.ProductId, IsAccessories = 0, Entry_By = user.User_User.ID.ToString(), Entry_Date = DateTime.Now };
                db.AssetProductBrands.Add(assetbrand);
                await db.SaveChangesAsync();

                Session["success_div"] = "true";
                Session["success_msg"] = "Asset Brand  Added Successfully.";
                return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Rozy 
        // Date: 2018-05-17


        public async Task<ActionResult> EditAssetProductBrand(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    //try
                    //{
                    //    DropDownData ddd = new DropDownData();
                    //    ViewBag.AssetBrandList = new SelectList(ddd.GetAssetProductList(), "Value", "Text");

                    //    VM_AssetProductBrand model = (from t1 in db.AssetProductBrands
                    //                                  join c in db.AssetProducts on t1.ProductId equals c.ID
                    //                                  where t1.ID == id
                    //                                  select new VM_AssetProductBrand
                    //                                  {
                    //                                      ID = t1.ID,
                    //                                      ProductName = c.ProductName,
                    //                                      BrandName = t1.BrandName
                    //                                  }).FirstOrDefault();
                    //    return View(model);
                    //}
                    //catch (Exception ex)
                    //{
                    //    Session["warning_div"] = "true";
                    //    Session["warning_msg"] = "Error Happened. " + ex.Message;
                    //    return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                    //}

                    DropDownData ddd = new DropDownData();
                    ViewBag.AssetBrandList = new SelectList(ddd.GetAssetProductList(), "Value", "Text");

                    VM_AssetProductBrand model = (from t1 in db.AssetProductBrands
                                                  join c in db.AssetProducts on t1.ProductId equals c.ID
                                                  where t1.ID == id
                                                  select new VM_AssetProductBrand
                                                  {
                                                      ID = t1.ID,
                                                      ProductName = c.ProductName,
                                                      BrandName = t1.BrandName
                                                  }).FirstOrDefault();
                    return View(model);

                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Asset Brand Not Found!";
                return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Rozy 
        // Date: 2018-05-17

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAssetProductBrand(VM_AssetProductBrand model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                //try
                //{
                //    Asset_ProductBrand assetBrand = new Asset_ProductBrand { BrandName = model.BrandName, ProductId = model.ProductId, ID = model.ID };
                //    db.Entry(assetBrand).State = EntityState.Modified;
                //    await db.SaveChangesAsync();
                //    Session["success_div"] = "true";
                //    Session["success_msg"] = "Asset Brand Updated Successfully.";
                //    return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                //}
                //catch (Exception ex)
                //{
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Error Happened." + ex.Message;
                //    return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                //}
                Asset_ProductBrand assetBrand = new Asset_ProductBrand { BrandName = model.BrandName, ProductId = model.ProductId, ID = model.ID };
                db.Entry(assetBrand).State = EntityState.Modified;
                await db.SaveChangesAsync();
                Session["success_div"] = "true";
                Session["success_msg"] = "Asset Brand Updated Successfully.";
                return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Rozy 
        // Date: 2018-05-17

        [HttpGet]
        public async Task<ActionResult> DeleteAssetProductBrandt(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Asset Brand Not Found!";
                    return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                }
                Asset_ProductBrand brand = await db.AssetProductBrands.FindAsync(id);
                if (brand != null)
                {
                    try
                    {
                        db.AssetProductBrands.Remove(brand);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Asset Brand Deleted Successfully.";
                        return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Asset Brand Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAssetProductBrandList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        #endregion

        #region Asset/Accessories Model

        [HttpGet]
        public async Task<ActionResult> ViewAssetProductModelList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                //try
                //{
                //    Asset_ProductBrand brand = new Asset_ProductBrand();
                //    Asset_Product productType = new Asset_Product();

                //    VM_AssetProductModel model = new VM_AssetProductModel();
                //    model.DataList = (from t1 in db.AssetProductModels
                //                      join c in db.AssetProductBrands on t1.BrandId equals c.ID
                //                      join d in db.AssetProducts on c.ProductId equals d.ID
                //                      where t1.Status == true
                //                      select new VM_AssetProductModel
                //                      {
                //                          ID = t1.ID,
                //                          ProductName = d.ProductName,
                //                          BrandName = c.BrandName,
                //                          ModelName = t1.ModelName
                //                      }).AsEnumerable();

                //    return View(model);
                //}
                //catch (Exception ex)
                //{

                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Error Happened." + ex.Message;
                //    return RedirectToAction("Index", "AssetManagement");
                //}

                Asset_ProductBrand brand = new Asset_ProductBrand();
                Asset_Product productType = new Asset_Product();

                VM_AssetProductModel model = new VM_AssetProductModel();
                model.DataList = (from t1 in db.AssetProductModels
                                  join c in db.AssetProductBrands on t1.BrandId equals c.ID
                                  join d in db.AssetProducts on c.ProductId equals d.ID
                                  where t1.Status == true
                                  select new VM_AssetProductModel
                                  {
                                      ID = t1.ID,
                                      ProductName = d.ProductName,
                                      BrandName = c.BrandName,
                                      ModelName = t1.ModelName
                                  }).AsEnumerable();

                return View(model);

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Rozy 
        // Date: 2018-05-17
        [HttpGet]
        public async Task<ActionResult> AddAssetProductModel()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                //try
                //{
                //    DropDownData ddd = new DropDownData();
                //    ViewBag.AssetProductType = new SelectList(ddd.GetAssetProductList(), "Value", "Text");
                //    ViewBag.AssetBrandList = new SelectList(ddd.GetAssetBrandList(), "Value", "Text");
                //    VM_AssetProductModel model = new VM_AssetProductModel();
                //    return View(model);
                //}
                //catch (Exception ex)
                //{
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Error Happened." + ex.Message;
                //    return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                //}

                DropDownData ddd = new DropDownData();
                ViewBag.AssetProductType = new SelectList(ddd.GetAssetProductList(), "Value", "Text");
                ViewBag.AssetBrandList = new SelectList(ddd.GetAssetBrandList(), "Value", "Text");
                VM_AssetProductModel model = new VM_AssetProductModel();
                return View(model);

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Rozy 
        // Date: 2018-05-17

        [HttpPost]
        public async Task<ActionResult> AddAssetProductModel(VM_AssetProductModel model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                //try
                //{
                //    Asset_ProductModel assetmodel = new Asset_ProductModel { ModelName = model.ModelName, BrandId = model.BrandId, IsAccessories = 0, Entry_By = user.User_User.ID.ToString(), Entry_Date = DateTime.Now };
                //    db.AssetProductModels.Add(assetmodel);
                //    await db.SaveChangesAsync();

                //    Session["success_div"] = "true";
                //    Session["success_msg"] = "Asset Model Added Successfully.";
                //    return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                //}
                //catch (Exception ex)
                //{
                //    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                //    {
                //        Session["warning_div"] = "true";
                //        Session["warning_msg"] = "Asset Model Can't Add, It is Already Exist!";
                //        return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                //    }
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                //    return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                //}
                Asset_ProductModel assetmodel = new Asset_ProductModel { ModelName = model.ModelName, BrandId = model.BrandId, IsAccessories = 0, Entry_By = user.User_User.ID.ToString(), Entry_Date = DateTime.Now };
                db.AssetProductModels.Add(assetmodel);
                await db.SaveChangesAsync();

                Session["success_div"] = "true";
                Session["success_msg"] = "Asset Model Added Successfully.";
                return RedirectToAction("ViewAssetProductModelList", "AssetManagement");

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Rozy 
        // Date: 2018-05-17

        public async Task<ActionResult> EditAssetProductModel(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    //try
                    //{
                    //    DropDownData ddd = new DropDownData();
                    //    ViewBag.AssetProductType = new SelectList(ddd.GetAssetProductList(), "Value", "Text");
                    //    ViewBag.AssetBrandList = new SelectList(ddd.GetAssetBrandList(), "Value", "Text");

                    //    VM_AssetProductModel model = (from t1 in db.AssetProductModels
                    //                                  join c in db.AssetProductBrands on t1.BrandId equals c.ID
                    //                                  join d in db.AssetProducts on c.ProductId equals d.ID
                    //                                  where t1.ID == id
                    //                                  select new VM_AssetProductModel
                    //                                  {
                    //                                      ID = t1.ID,
                    //                                      ProductName = d.ProductName,
                    //                                      BrandName = c.BrandName,
                    //                                      ModelName = t1.ModelName
                    //                                  }).FirstOrDefault();
                    //    return View(model);
                    //}
                    //catch (Exception ex)
                    //{
                    //    Session["warning_div"] = "true";
                    //    Session["warning_msg"] = "Error Happened. " + ex.Message;
                    //    return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                    //}

                    DropDownData ddd = new DropDownData();
                    ViewBag.AssetProductType = new SelectList(ddd.GetAssetProductList(), "Value", "Text");
                    ViewBag.AssetBrandList = new SelectList(ddd.GetAssetBrandList(), "Value", "Text");

                    VM_AssetProductModel model = (from t1 in db.AssetProductModels
                                                  join c in db.AssetProductBrands on t1.BrandId equals c.ID
                                                  join d in db.AssetProducts on c.ProductId equals d.ID
                                                  where t1.ID == id
                                                  select new VM_AssetProductModel
                                                  {
                                                      ID = t1.ID,
                                                      ProductName = d.ProductName,
                                                      BrandName = c.BrandName,
                                                      ModelName = t1.ModelName
                                                  }).FirstOrDefault();
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Asset Model Not Found!";
                return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Rozy 
        // Date: 2018-05-17

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAssetProductModel(VM_AssetProductModel model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                //try
                //{
                //    Asset_ProductModel assetModel = new Asset_ProductModel
                //    {
                //        ModelName = model.ModelName,
                //        BrandId = model.BrandId,
                //        ID = model.ID
                //    };
                //    db.Entry(assetModel).State = EntityState.Modified;
                //    await db.SaveChangesAsync();
                //    Session["success_div"] = "true";
                //    Session["success_msg"] = "Asset Model Updated Successfully.";
                //    return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                //}
                //catch (Exception ex)
                //{
                //    Session["warning_div"] = "true";
                //    Session["warning_msg"] = "Error Happened." + ex.Message;
                //    return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                //}
                Asset_ProductModel assetModel = new Asset_ProductModel
                {
                    ModelName = model.ModelName,
                    BrandId = model.BrandId,
                    ID = model.ID
                };
                db.Entry(assetModel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                Session["success_div"] = "true";
                Session["success_msg"] = "Asset Model Updated Successfully.";
                return RedirectToAction("ViewAssetProductModelList", "AssetManagement");

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Rozy 
        // Date: 2018-05-17

        [HttpGet]
        public async Task<ActionResult> DeleteAssetProductModel(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Asset Model Not Found!";
                    return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                }
                Asset_ProductModel model = await db.AssetProductModels.FindAsync(id);
                if (model != null)
                {
                    try
                    {
                        db.AssetProductModels.Remove(model);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Asset Model Deleted Successfully.";
                        return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Asset Model Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #endregion

        #region Vendor Info

        [HttpGet]
        public async Task<ActionResult> ViewAssetVendorList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetVendorInfo model = new VM_AssetVendorInfo();
                    await Task.Run(() => model.LoadAssetVendorInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> AddAssetVendorInfo()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetVendorInfo model = new VM_AssetVendorInfo();
                    await Task.Run(() => model.LoadAssetVendorInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        [HttpPost]
        public async Task<ActionResult> AddAssetVendorInfo(VM_AssetVendorInfo model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {

                try
                {
                    Asset_VendorInfo vendorinfo = new Asset_VendorInfo
                    {
                        VendorName = model.VendorName,
                        Address = model.Address,
                        ContactPersonName = model.ContactPersonName,
                        Designation = model.Designation,
                        Phone = model.Phone,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now
                    };
                    db.AssetVendorInfo.Add(vendorinfo);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Vendor Info Added Successfully.";
                    return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Asset Vendor's info Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        public async Task<ActionResult> EditAssetVendorInfo(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    VM_AssetVendorInfo model = new VM_AssetVendorInfo();
                    await Task.Run(() => model = model.LoadSpecificAssetVendorInfo(id.Value));
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Asset Vendor Info Not Found!";
                return RedirectToAction("AddAssetVendorInfo", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAssetVendorInfo(VM_AssetVendorInfo model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_VendorInfo assetVendorInfo = new Asset_VendorInfo
                    {
                        ID = model.ID,
                        VendorName = model.VendorName,
                        Address = model.Address,
                        ContactPersonName = model.ContactPersonName,
                        Designation = model.Designation,
                        Phone = model.Phone,
                        Update_Date = DateTime.Now,
                        Update_By = user.User_User.ID.ToString()
                    };
                    db.Entry(assetVendorInfo).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Vendor Info Updated Successfully.";
                    return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");

        }

        [HttpGet]
        public async Task<ActionResult> DeleteAssetVendorInfo(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Asset Vendor Info Not Found!";
                    return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                }
                Asset_VendorInfo assetVendorInfo = await db.AssetVendorInfo.FindAsync(id);
                if (assetVendorInfo != null)
                {
                    try
                    {
                        db.AssetVendorInfo.Remove(assetVendorInfo);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Asset Vendor Info Deleted Successfully.";
                        return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Asset Vendor Info Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAssetVendorList", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAssetVendorList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #region Asset Location

        [HttpGet]
        public async Task<ActionResult> ViewAssetProductLocationList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetLocation model = new VM_AssetLocation();
                    await Task.Run(() => model.LoadAssetProductLocationInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> AddAssetProductLocation()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    VM_AssetLocation model = new VM_AssetLocation();
                    await Task.Run(() => model.LoadAssetProductLocationInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> AddAssetProductLocation(VM_AssetLocation model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {

                try
                {
                    Asset_Location productlocation = new Asset_Location()
                    {
                        ProductLocation = model.ProductLocation,
                        IsAccessories = model.IsAccessories,
                        Block = model.Block,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now
                    };
                    db.AssetLocations.Add(productlocation);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Product Location Added Successfully.";
                    return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Asset Product Location Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        public async Task<ActionResult> EditAssetProductLocation(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    DropDownData ddd = new DropDownData();
                    VM_AssetLocation model = new VM_AssetLocation();
                    await Task.Run(() => model = model.LoadAssetProductLocationInfo(id.Value));
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Asset Product Location Not Found!";
                return RedirectToAction("AddAssetProductLocation", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAssetProductLocation(VM_AssetLocation model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_Location assetProductlocation = new Asset_Location
                    {
                        ID = model.ID,
                        ProductLocation = model.ProductLocation,
                        IsAccessories = model.IsAccessories,
                        Block = model.Block,
                        Update_Date = DateTime.Now,
                        Update_By = user.User_User.ID.ToString()
                    };
                    db.Entry(assetProductlocation).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Product location Updated Successfully.";
                    return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");

        }



        [HttpGet]
        public async Task<ActionResult> DeleteAssetProductLocation(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Asset Product Location Not Found!";
                    return RedirectToAction("ViewAssetProductList", "AssetManagement");
                }
                Asset_Location assetProductlocation = await db.AssetLocations.FindAsync(id);
                if (assetProductlocation != null)
                {
                    try
                    {
                        db.AssetLocations.Remove(assetProductlocation);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Asset Product Location Deleted Successfully.";
                        return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Asset Product Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAssetProductLocationList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #region Accessories Product Type Entry

        [HttpGet]
        public async Task<ActionResult> ViewAccessoriesProduct()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetProduct model = new VM_AssetProduct();
                    await Task.Run(() => model.LoadAccessoriesProductInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> AddAccessoriesProduct()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    VM_AssetProduct model = new VM_AssetProduct();
                    await Task.Run(() => model.LoadAccessoriesProductInfo());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> AddAccessoriesProduct(VM_AssetProduct model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {

                try
                {
                    Asset_Product product = new Asset_Product { ProductName = model.ProductName, Status = model.Status, IsAccessories = 1, Entry_By = user.User_User.ID.ToString(), Entry_Date = DateTime.Now };
                    db.AssetProducts.Add(product);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Accessories Product Added Successfully.";
                    return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Acessories Product Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        public async Task<ActionResult> EditAccessoriesProduct(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    DropDownData ddd = new DropDownData();
                    VM_AssetProduct model = new VM_AssetProduct();
                    await Task.Run(() => model = model.LoadSpecificAccessoriesProductInfo(id.Value));
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Accesories Product Not Found!";
                return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAccessoriesProduct(VM_AssetProduct model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_Product accessoriesProduct = new Asset_Product
                    {
                        ID = model.ID,
                        ProductName = model.ProductName,
                        Status = model.Status,
                        IsAccessories = 1,
                        Update_Date = DateTime.Now,
                        Update_By = user.User_User.ID.ToString()
                    };
                    db.Entry(accessoriesProduct).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Accessories Product Updated Successfully.";
                    return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");

        }

        [HttpGet]
        public async Task<ActionResult> DeleteAccessoriesProduct(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Accessories Product Not Found!";
                    return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                }
                Asset_Product accessoriesProduct = await db.AssetProducts.FindAsync(id);
                if (accessoriesProduct != null)
                {
                    try
                    {
                        db.AssetProducts.Remove(accessoriesProduct);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Accesories Product Deleted Successfully.";
                        return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Accesories Product Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #region Asset/Accessories Brand
        [HttpGet]
        public async Task<ActionResult> ViewAccessoriesProductBrand()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_ProductBrand brand = new Asset_ProductBrand();
                    Asset_Product productType = new Asset_Product();

                    VM_AssetProductBrand model = new VM_AssetProductBrand();
                    model.DataList = (from t1 in db.AssetProductBrands
                                      join c in db.AssetProducts on t1.ProductId equals c.ID
                                      where (t1.Status == true && c.IsAccessories == 1)
                                      select new VM_AssetProductBrand
                                      {
                                          ID = t1.ID,
                                          ProductName = c.ProductName,
                                          BrandName = t1.BrandName,
                                          IsAccessories = c.IsAccessories

                                      }).AsEnumerable();

                    return View(model);
                }
                catch (Exception ex)
                {

                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        // Below Action used to Add AssetBrand Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductBrand
        // Domain Model Name: Asset_ProductBrand
        // View Model Name: VMAssetProductBrand
        // Developed by: Oyaliul and Rozy
        // Date: 2018-04-16
        [HttpGet]
        public async Task<ActionResult> AddAccessoriesProductBrand()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.AssetProductType = new SelectList(ddd.GetAccessoriesProductList(), "Value", "Text");
                    VM_AssetProductBrand model = new VM_AssetProductBrand();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }



        // Below Action used to AddAsset ProductBrand Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductBrand
        // Domain Model Name: Asset_ProductBrand
        // View Model Name: VMAssetProductBrand
        // Developed by: Oyaliul 
        // Date: 2018-04-16

        [HttpPost]
        public async Task<ActionResult> AddAccessoriesProductBrand(VM_AssetProductBrand model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_ProductBrand assetbrand = new Asset_ProductBrand { BrandName = model.BrandName, ProductId = model.ProductId, IsAccessories = 1, Entry_By = user.User_User.ID.ToString(), Entry_Date = DateTime.Now };
                    db.AssetProductBrands.Add(assetbrand);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Accessories Brand  Added Successfully.";
                    return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Accessories Brand Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit Asset Brand Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductBrand
        // Domain Model Name: Asset_ProductBrand
        // View Model Name: VM_AssetProductBrand
        // Developed by: Oyaliul 
        // Date: 2018-04-17

        public async Task<ActionResult> EditAccessoriesProductBrand(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    try
                    {
                        DropDownData ddd = new DropDownData();
                        ViewBag.AssetBrandList = new SelectList(ddd.GetAccessoriesProductList(), "Value", "Text");

                        VM_AssetProductBrand model = (from t1 in db.AssetProductBrands
                                                      join c in db.AssetProducts on t1.ProductId equals c.ID
                                                      where t1.ID == id
                                                      select new VM_AssetProductBrand
                                                      {
                                                          ID = t1.ID,
                                                          ProductName = c.ProductName,
                                                          BrandName = t1.BrandName,
                                                          IsAccessories = t1.IsAccessories
                                                      }).FirstOrDefault();
                        return View(model);
                    }
                    catch (Exception ex)
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Error Happened. " + ex.Message;
                        return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Accessories Brand Not Found!";
                return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit AssetProductBrand Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductBrand
        // Domain Model Name: Asset_ProductBrand
        // View Model Name: VM_AssetProductBrand
        // Developed by: Oyaliul 
        // Date: 2018-04-17

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAccessoiresProductBrand(VM_AssetProductBrand model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_ProductBrand assetBrand = new Asset_ProductBrand { BrandName = model.BrandName, ProductId = model.ProductId, IsAccessories = 1, ID = model.ID };
                    db.Entry(assetBrand).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Accessories Brand Updated Successfully.";
                    return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Delete AssetProductBrand Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductBrand
        // Domain Model Name: Asset_ProductBrand
        // View Model Name: VM_AssetProductBrand
        // Developed by: Oyaliul 
        // Date: 2018-04-17

        [HttpGet]
        public async Task<ActionResult> DeleteAccessoriesProductBrand(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Accessories Brand Not Found!";
                    return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                }
                Asset_ProductBrand brand = await db.AssetProductBrands.FindAsync(id);
                if (brand != null)
                {
                    try
                    {
                        db.AssetProductBrands.Remove(brand);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Accessories Brand Deleted Successfully.";
                        return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Accessories Brand Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAccessoriesProductBrand", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        #endregion

        #region Asset/Accessories Model

        [HttpGet]
        public async Task<ActionResult> ViewAccessoriesProductModel()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_ProductBrand brand = new Asset_ProductBrand();
                    Asset_Product product = new Asset_Product();

                    VM_AssetProductModel model = new VM_AssetProductModel();
                    model.DataList = (from t1 in db.AssetProductModels
                                      join c in db.AssetProductBrands on t1.BrandId equals c.ID
                                      join d in db.AssetProducts on c.ProductId equals d.ID
                                      where t1.Status == true && d.IsAccessories == 1
                                      select new VM_AssetProductModel
                                      {
                                          ID = t1.ID,
                                          ProductName = d.ProductName,
                                          BrandName = c.BrandName,
                                          ModelName = t1.ModelName,
                                          IsAccessories = t1.IsAccessories
                                      }).AsEnumerable();

                    return View(model);
                }
                catch (Exception ex)
                {

                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        // Below Action used to Add AssetModel Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VMAssetProductModel
        // Developed by: Oyaliul
        // Date: 2018-04-17
        [HttpGet]
        public async Task<ActionResult> AddAccessoriesProductModel()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.AssetProductType = new SelectList(ddd.GetAccessoriesProductList(), "Value", "Text");
                    ViewBag.AssetBrandList = new SelectList(ddd.GetAccessoriesBrandList(), "Value", "Text");
                    VM_AssetProductModel model = new VM_AssetProductModel();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        // Below Action used to Asset ProductModel Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VMAssetProductModel
        // Developed by: Oyaliul
        // Date: 2018-04-17

        [HttpPost]
        public async Task<ActionResult> AddAccessoriesProductModel(VM_AssetProductModel model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_ProductModel accesoriesmodel = new Asset_ProductModel { ModelName = model.ModelName, BrandId = model.BrandId, IsAccessories = 1, Entry_By = user.User_User.ID.ToString(), Entry_Date = DateTime.Now };
                    db.AssetProductModels.Add(accesoriesmodel);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Accessories Model Added Successfully.";
                    return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Accessories Model Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit Asset Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Oyaliul 
        // Date: 2018-04-17


        public async Task<ActionResult> EditAccessoriesProductModel(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    try
                    {
                        DropDownData ddd = new DropDownData();
                        ViewBag.AccessoriesProduct = new SelectList(ddd.GetAccessoriesProductList(), "Value", "Text");
                        ViewBag.AccessorisBrandList = new SelectList(ddd.GetAccessoriesBrandList(), "Value", "Text");

                        VM_AssetProductModel model = (from t1 in db.AssetProductModels
                                                      join c in db.AssetProductBrands on t1.BrandId equals c.ID
                                                      join d in db.AssetProducts on c.ProductId equals d.ID
                                                      where t1.ID == id
                                                      select new VM_AssetProductModel
                                                      {
                                                          ID = t1.ID,
                                                          ProductName = d.ProductName,
                                                          BrandName = c.BrandName,
                                                          ModelName = t1.ModelName,
                                                          IsAccessories = t1.IsAccessories
                                                      }).FirstOrDefault();
                        return View(model);
                    }
                    catch (Exception ex)
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Error Happened. " + ex.Message;
                        return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Accessories Model Not Found!";
                return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to Edit Asset Product Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Oyaliul 
        // Date: 2018-04-17

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAccessoriesProductModel(VM_AssetProductModel model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_ProductModel assetModel = new Asset_ProductModel { ModelName = model.ModelName, BrandId = model.BrandId, IsAccessories = 1, ID = model.ID };
                    db.Entry(assetModel).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Accessories Model Updated Successfully.";
                    return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        // Below Action used to DeleteAsset Model Information
        // Database: CrimsonERP
        // Table Name : Asset_ProductModel
        // Domain Model Name: Asset_ProductModel
        // View Model Name: VM_AssetProductModel
        // Developed by: Oyaliul 
        // Date: 2018-04-17

        [HttpGet]
        public async Task<ActionResult> DeleteAccessoriesProductModel(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Accessories Model Not Found!";
                    return RedirectToAction("ViewAssetProductModelList", "AssetManagement");
                }
                Asset_ProductModel model = await db.AssetProductModels.FindAsync(id);
                if (model != null)
                {
                    try
                    {
                        db.AssetProductModels.Remove(model);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Accessories Model Deleted Successfully.";
                        return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Accessories Model Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAccessoriesProductModel", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #endregion


        #endregion

        #region AssetInfo Entry with ViewAssetList

        public async Task<ActionResult> ViewAllAssetInfoList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetProductInfo assetList = new VM_AssetProductInfo();
                    await Task.Run(() => assetList.LoadAllAssetList());
                    return View(assetList);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        public async Task<ActionResult> ViewAvailableAssetInfoList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetProductInfo model = new VM_AssetProductInfo();
                    await Task.Run(() => model.LoadAvailableAssetList());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> AssetInfoEntry()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddl = new DropDownData();
                    ViewBag.BusinessUnitall = new SelectList(ddl.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.GetLifeTime = new SelectList(DropDownData.GetLifeTime(), "Value", "Text");
                    ViewBag.ListOfDepartment = new SelectList(ddl.GetDepartmentList(), "Value", "Text");
                    ViewBag.AssetProduct = new SelectList(ddl.GetAssetProductList(), "Value", "Text");
                    ViewBag.AssetBrandList = new SelectList(ddl.GetAssetBrandList(), "Value", "Text");
                    ViewBag.ListOfVendor = new SelectList(ddl.GetAssetVendorList(), "Value", "Text");
                    ViewBag.ListOfProductLocation = new SelectList(ddl.GetAssetLocationList(), "Value", "Text");
                    return View();
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> AssetInfoEntry(VM_AssetProductInfo model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_ProductInfo assetProductInfo = new Asset_ProductInfo
                    {

                        Asset_Serial_No = model.Asset_Serial_No,
                        ProductId = model.ProductId,
                        ProductName = model.ProductName,
                        BrandId = model.BrandId,
                        ModelId = model.ModelId,
                        BU_Id = model.BU_Id,
                        Dept_Id = model.Dept_Id,
                        Product_SL_No = model.Product_SL_No,
                        AssetConfiguration = model.AssetConfiguration,
                        Remarks = model.Remarks,
                        Price = model.Price,
                        IsWarranty = model.IsWarranty,
                        WarrantryDate = model.WarrantryDate,
                        VendorId = model.VendorId,
                        PurchaseDate = model.PurchaseDate,
                        Location_Id = model.Location_Id,
                        IsAccessories = 0,
                        LifeTime = model.LifeTime,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now

                    };
                    db.AssetProductInfo.Add(assetProductInfo);

                    db.SaveChanges();

                    int productId = assetProductInfo.ID;

                    var data = JsonConvert.DeserializeObject<List<DepreciationHistory>>(model.listDepriciatiionc);
                    Asset_DepreciationHistory abc = new Asset_DepreciationHistory();

                    abc.ProductIdFK = productId;
                    abc.Entry_By = user.User_User.ID.ToString();
                    abc.Entry_Date = DateTime.Now;
                    foreach (var item in data)
                    {
                        abc.Year = item.year;
                        abc.DepreciationPercent = item.prtnge;
                        db.AssetDepreciationInfo.Add(abc);
                        var s = db.SaveChanges();
                    }

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset ProductInfo Added Successfully.";
                    return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Asset ProductInfo Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> DeleteAssetProductInfo(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Product ID Not Found!";
                    return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                }
                Asset_ProductInfo assetProduct = await db.AssetProductInfo.FindAsync(id);
                if (assetProduct != null)
                {
                    try
                    {
                        db.AssetProductInfo.Remove(assetProduct);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Asset Product Info Deleted Successfully.";
                        return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Asset Product Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        public ActionResult EditAssetProductInfo(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                if (id != null)
                {
                    DropDownData ddl = new DropDownData();
                    ViewBag.BusinessUnitall = new SelectList(ddl.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.GetLifeTime = new SelectList(DropDownData.GetLifeTime(), "Value", "Text");
                    ViewBag.Unitall = new SelectList(ddl.GetUnitList(), "Value", "Text");
                    ViewBag.ListModelByBrand = new SelectList(ddl.GetModelByBrand(), "Value", "Text");
                    ViewBag.ListOfDepartment = new SelectList(ddl.GetDepartmentList(), "Value", "Text");
                    ViewBag.AssetProduct = new SelectList(ddl.GetAssetProductList(), "Value", "Text");
                    ViewBag.AssetBrandList = new SelectList(ddl.GetAssetBrandList(), "Value", "Text");
                    ViewBag.ListOfVendor = new SelectList(ddl.GetAssetVendorList(), "Value", "Text");
                    ViewBag.ListOfProductLocation = new SelectList(ddl.GetAssetLocationList(), "Value", "Text");

                    var asset = db.AssetProductInfo.FirstOrDefault(s => s.ID == id);
                    var vendor = db.AssetVendorInfo.FirstOrDefault(x => x.ID == asset.VendorId);
                    var brand = db.AssetProductBrands.FirstOrDefault(x => x.ID == asset.BrandId);
                    var models = db.AssetProductModels.FirstOrDefault(x => x.ID == asset.ModelId);
                    var location = db.AssetLocations.FirstOrDefault(x => x.ID == asset.Location_Id);
                    var department = db.Departments.FirstOrDefault(x => x.ID == asset.Dept_Id);
                    var businessUnits = db.BusinessUnits.FirstOrDefault(x => x.ID == asset.BU_Id);
                    var products = db.AssetProducts.FirstOrDefault(x => x.ID == asset.ProductId);
                    VM_AssetProductInfo model = new VM_AssetProductInfo
                    {
                        ID = asset.ID,
                        Asset_Serial_No = asset.Asset_Serial_No,
                        ProductId = asset.ProductId,
                        ProductName = "",
                        BrandId = asset.BrandId,
                        ModelId = asset.ModelId,
                        BU_Id = asset.BU_Id,
                        Dept_Id = asset.Dept_Id,
                        // EmployeeId=1,
                        Product_SL_No = asset.Product_SL_No,
                        AssetConfiguration = asset.AssetConfiguration,
                        Price = asset.Price,
                        IsWarranty = asset.IsWarranty,
                        Remarks = asset.Remarks,
                        VendorId = asset.VendorId,
                        Vendor = vendor,
                        Brand = brand,
                        Location = location,
                        Model = models,
                        BusinessUnit = businessUnits,
                        Department = department,
                        Product = products,
                        PurchaseDate = asset.PurchaseDate,
                        WarrantryDate = asset.WarrantryDate,
                        Location_Id = asset.Location_Id,
                        IsAccessories = 0,
                        LifeTime = asset.LifeTime,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now

                    };
                    var data = db.AssetDepreciationInfo.Where(s => s.ProductIdFK == id).ToList();

                    ViewBag.DepreciationInfo = data;

                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Asset Product Info Not Found!";
                return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult EditAssetProductInfo(VM_AssetProductInfo model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {

                    var vendor = db.AssetVendorInfo.FirstOrDefault(x => x.ID == model.VendorId);
                    var brand = db.AssetProductBrands.FirstOrDefault(x => x.ID == model.BrandId);
                    var models = db.AssetProductModels.FirstOrDefault(x => x.ID == model.ModelId);
                    var location = db.AssetLocations.FirstOrDefault(x => x.ID == model.Location_Id);
                    var department = db.Departments.FirstOrDefault(x => x.ID == model.Dept_Id);
                    var businessUnits = db.BusinessUnits.FirstOrDefault(x => x.ID == model.BU_Id);
                    var units = db.Units.FirstOrDefault(x => x.ID == model.Unit_Id);
                    var products = db.AssetProducts.FirstOrDefault(x => x.ID == model.ProductId);
                    Asset_ProductInfo assetProduct = new Asset_ProductInfo
                    {
                        ID = model.ID,
                        Asset_Serial_No = model.Asset_Serial_No,
                        ProductId = model.ProductId,
                        ProductName = "",
                        BrandId = model.BrandId,
                        ModelId = model.ModelId,
                        BU_Id = model.BU_Id,
                        Dept_Id = model.Dept_Id,
                        Product_SL_No = model.Product_SL_No,
                        AssetConfiguration = model.AssetConfiguration,
                        Remarks = model.Remarks,
                        Price = model.Price,
                        IsWarranty = model.IsWarranty,
                        VendorId = model.VendorId,
                        PurchaseDate = model.PurchaseDate,
                        WarrantryDate = model.WarrantryDate,
                        Location_Id = model.Location_Id,
                        LifeTime = model.LifeTime,
                        IsAccessories = 0,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now,
                        Update_By = user.User_User.ID.ToString(),
                        Vendor = vendor,
                        Brand = brand,
                        Location = location,
                        Model = models,
                        BusinessUnit = businessUnits,
                        Department = department,
                        Product = products,
                        Update_Date = DateTime.Now,
                        Deliver = 1,
                        ExpiredDate = DateTime.Now,
                        MasureUnit = "",
                        IsAssigned = 0,
                        IsDamaged = 0,
                        IsDeleted = 0,
                        IsDisposed = 0,
                        IsHeadOffice = 0,
                        PurchaseQuantity = 0,
                        Status = true,
                        Stock = 0,
                        
                        
                    };
                    db.Entry(assetProduct).State = EntityState.Modified;
                    db.SaveChanges();


                    //to delete previous details data..................
                    var detailsdatalist = db.AssetDepreciationInfo.Where(x => x.ProductIdFK == assetProduct.ID).ToList();
                    detailsdatalist.ForEach(p => db.AssetDepreciationInfo.Remove(p));

                    var data = JsonConvert.DeserializeObject<List<DepreciationHistory>>(model.listDepriciatiionc);
                    Asset_DepreciationHistory abc = new Asset_DepreciationHistory();

                    abc.ProductIdFK = assetProduct.ID;
                    abc.Entry_By = user.User_User.ID.ToString();
                    abc.Entry_Date = DateTime.Now;
                    abc.Update_Date = DateTime.Now;
                    foreach (var item in data)
                    {
                        abc.Year = item.year;
                        abc.DepreciationPercent = item.prtnge;
                        db.AssetDepreciationInfo.Add(abc);
                        var s = db.SaveChanges();
                    }
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Info Updated Successfully.";
                    return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewAllAssetInfoList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #region Asset Assign Section

        [HttpGet]
        public async Task<ActionResult> ViewAssignedAssetList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetProductAssign model = new VM_AssetProductAssign();
                    await Task.Run(() => model.LoadAssignedAssetList());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> AssetAssign()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddl = new DropDownData();
                    ViewBag.AssetProductList = new SelectList(ddl.GetAssetProductListForAssign(), "Value", "Text");
                    ViewBag.ListOfDepartments = new SelectList(ddl.GetDepartmentList(), "Value", "Text");
                    ViewBag.ListOfBusinessUnit = new SelectList(ddl.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.ListOfEmployee = new SelectList(ddl.GetEmployeeList(), "Value", "Text");
                    ViewBag.ListOfAssignLocation = new SelectList(ddl.GetAssetLocationList(), "Value", "Text");

                    return View();
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAssignedAssetList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> AssetAssign(VM_AssetProductAssign model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_ProductAssign assetAssign = new Asset_ProductAssign
                    {
                        Asset_Serial_No = model.Asset_Serial_No,
                        ProductId = model.ProductId,
                        AssetProductInfoId = model.AssetProductInfoId,
                        ProductName = model.ProductName,
                        BrandId = model.BrandId,
                        ModelId = model.ModelId,
                        BU_Id = model.BU_Id,
                        Dept_Id = model.Dept_Id,
                        EmployeeId = model.EmployeeId,
                        AssignDate = model.AssignDate,
                        Location_Id = model.Location_Id,
                        IsAccessories = 0,
                        IsAssigned = 1,
                        IsReturned = 0,
                        Remarks = model.Remarks,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now
                    };
                    db.AssetAssign.Add(assetAssign);
                    await db.SaveChangesAsync();

                    //Insert data in Asset_ProductAssign_History table for tracking AssetProduct
                    Asset_ProductAssign_History assetAssignHisotry = new Asset_ProductAssign_History
                    {
                        Asset_Serial_No = model.Asset_Serial_No,
                        ProductId = model.ProductId,
                        BrandId = model.BrandId,
                        ModelId = model.ModelId,
                        BU_Id = model.BU_Id,
                        Dept_Id = model.Dept_Id,
                        EmployeeId = model.EmployeeId,
                        Designation_Id = 1,
                        AssetAssignDate = model.AssignDate,
                        Location_Id = model.Location_Id,
                        IsAssigned = 1,
                        AssetAction = "Assigned",
                        IsAccessories = 0,
                        IsReturned = 0,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now
                    };
                    db.AssetAssignHistory.Add(assetAssignHisotry);
                    await db.SaveChangesAsync();

                    //Update IsAssigned=1 in AssetProductInfo table
                    var result = db.AssetProductInfo.SingleOrDefault(b => b.Asset_Serial_No == model.Asset_Serial_No);
                    if (result != null)
                    {
                        result.IsAssigned = 1;
                        await db.SaveChangesAsync();
                    }

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Product Assigned Successfully.";
                    return RedirectToAction("ViewAssignedAssetList", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Asset ProductInfo Can't Assign, It is Already Exist!";
                        return RedirectToAction("ViewAssignedAssetList", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAssignedAssetList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        #endregion

        #region Accessoreis Info Entry

        [HttpGet]
        public async Task<ActionResult> AccesoriesProductEntry()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddl = new DropDownData();

                    //ViewBag.ListOfCompanies = new SelectList(ddl.GetCompanyList(), "Value", "Text");
                    ViewBag.BusinessUnitall = new SelectList(ddl.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.Unitall = new SelectList(ddl.GetUnitList(), "Value", "Text");
                    ViewBag.ListOfDepartmentsHeadOffice = new SelectList(ddl.GetDepartmentList(), "Value", "Text");
                    // ViewBag.ListOfEmployee = new SelectList(ddl.GetEmployeeList(), "Value", "Text");
                    ViewBag.AssetProduct = new SelectList(ddl.GetAssetProductList(), "Value", "Text");
                    ViewBag.AssetBrandList = new SelectList(ddl.GetAssetBrandList(), "Value", "Text");
                    ViewBag.ListOfVendor = new SelectList(ddl.GetAssetVendorList(), "Value", "Text");
                    ViewBag.ListOfProductLocation = new SelectList(ddl.GetAssetLocationList(), "Value", "Text");
                    return View();
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewAssetProductList", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> AccesoriesProductEntry(VM_AssetProductInfo model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    Asset_ProductInfo assetProductInfo = new Asset_ProductInfo
                    {

                        Asset_Serial_No = model.Asset_Serial_No,
                        ProductId = model.ProductId,
                        ProductName = model.ProductName,
                        BrandId = model.BrandId,
                        ModelId = model.ModelId,
                        BU_Id = model.BU_Id,
                        Dept_Id = model.Dept_Id,
                        // EmployeeId=1,
                        Product_SL_No = model.Product_SL_No,
                        AssetConfiguration = model.AssetConfiguration,
                        Price = model.Price,
                        //PurchaseQuantity = model.PurchaseQuantity,
                        IsWarranty = model.IsWarranty,
                        VendorId = model.VendorId,
                        PurchaseDate = model.PurchaseDate,
                        Location_Id = model.Location_Id,
                        IsAccessories = 0,
                        Entry_By = user.User_User.ID.ToString(),
                        Entry_Date = DateTime.Now

                    };
                    db.AssetProductInfo.Add(assetProductInfo);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Accesories ProductInfo Model Added Successfully.";
                    return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Accesories ProductInfo Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewAccessoriesProduct", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        #endregion

        #region Asset Returns

        [HttpGet]
        public async Task<ActionResult> ViewAssetReturnList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetReturnedHistory model = new VM_AssetReturnedHistory();
                    await Task.Run(() => model.LoadAssetProductReturnList());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> AssetReturnEntry()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    DropDownData ddl = new DropDownData();
                    ViewBag.BusinessUnitall = new SelectList(ddl.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.ListOfDepartmentsHeadOffice = new SelectList(ddl.GetDepartmentList(), "Value", "Text");
                    ViewBag.ListOfEmployee = new SelectList(ddl.GetEmployeeList(), "Value", "Text");
                    ViewBag.ListOfProductLocation = new SelectList(ddl.GetAssetLocationList(), "Value", "Text");
                    return View();
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("AssetReturnEntry", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }


        [HttpGet]
        public async Task<ActionResult> AssetReturnsToAnother(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                DropDownData ddl = new DropDownData();

                VM_AssetProductAssign asset = new VM_AssetProductAssign
                {
                    AssetProductInfoId = Convert.ToInt32(id),
                    ID = Convert.ToInt32(id)
                };
                var assetinfo = asset.GetAssignedAssetInfo(id);
                ViewBag.AssignedAsset = assetinfo;

                ViewBag.ListOfDepartments = new SelectList(ddl.GetDepartmentListForAssetReturn(id), "Value", "Text");
                ViewBag.ListOfProductLocation = new SelectList(ddl.GetAssetLocationList(), "Value", "Text");
                return View(asset);
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Dear Admin, you must log in to continue";
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> AssetReturnsToAnother(VM_AssetProductAssign model, int returnType)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                var result = model.ReturnAsset(model, returnType);
                if (result == "Data Updated Successfully")
                {
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Asset Returned Successfully.";
                    return RedirectToAction("ViewAssignedAssetList");
                }

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Dear User, you must log in to Return";
            return RedirectToAction("Login", "Home");
        }

        public async Task<ActionResult> ViewDamagedAssetList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetProductAssign model = new VM_AssetProductAssign();
                    await Task.Run(() => model.LoadDamagedAssetList());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        public async Task<ActionResult> ViewDisposedAssetList()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                try
                {
                    VM_AssetProductInfo model = new VM_AssetProductInfo();
                    await Task.Run(() => model.LoadDisposedAssetList());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "AssetManagement");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Please Login to continue..!";
            return RedirectToAction("Login", "Home");
        }

        public async Task<ActionResult> DisposeAssetProduct(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                VM_AssetReturnedHistory model = new VM_AssetReturnedHistory();
                if (model.DisposeAssetProduct(id) == "Data Deleted Successfully")
                {
                    Session["success_div"] = "true";
                    Session["success_msg"] = "This Product Info Disposed Successfully.";
                    return RedirectToAction("ViewDisposedAssetList");
                }

            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Dear User, you must log in to Return";
            return RedirectToAction("Login", "Home");

        }

        #endregion

        #region Reporting

        [HttpGet]
        public ActionResult GetAssetReportByDeptAndEmp()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                DropDownData ddd = new DropDownData();
                ViewBag.BusinessUnitall = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
                ViewBag.ListOfDepartments = new SelectList(ddd.GetDepartmentList(), "Value", "Text");
                ViewBag.ListOfEmployee = new SelectList(ddd.GetEmployeeList(), "Value", "Text");
                VM_AseetReporting model = new VM_AseetReporting();
                return View();
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Dear Admin, you must log in to continue.";
            return RedirectToAction("Login", "Home");
        }


        [HttpGet]
        public ActionResult GetAssetTrackingReport()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)//&& user.ToString() == "Admin")
            {
                //try
                //{
                //    DropDownData ddd = new DropDownData();
                //    ViewBag.ListOfProduct = new SelectList(ddd.GetAssetProductList(), "Value", "Text");
                //    //ViewBag.BusinessUnitall = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");

                //    VM_AseetReporting model = new VM_AseetReporting();
                //    return View();
                //}
                //catch (Exception ex)
                //{
                //    Session["error_div"] = "true";
                //    Session["error_msg"] = "Error happend! Error Message is: " + ex.Message;
                //    return RedirectToAction("Dashboard", "AssetManagement");
                //}


                if (user != null)
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.ListOfProduct = new SelectList(ddd.GetAssetProductList(), "Value", "Text");
                    ViewBag.ListOfCategory = new SelectList(ddd.GetAssetCategoryList(), "Value", "Text");
                    ViewBag.ListOfSubCategory = new SelectList(ddd.GetAssetSubCategoryList(), "Value", "Text");
                    VM_AseetReporting model = new VM_AseetReporting();
                    return View();
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "Dear Admin, you must log in to continue";
            return RedirectToAction("Login", "Home");
        }


        #endregion

        #region JSON Method Segment
        public JsonResult GetUnitList(int bUnitId)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                var list = db.Units.ToList().Where(u => u.BusinessUnit_ID == bUnitId);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public JsonResult GetCategoryList(int categoryId)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                var list = db.AssetSubCategory.ToList().Where(u => u.CategoryId == categoryId);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public JsonResult GetBrandListByProduct(int productID)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                DropDownData dd = new DropDownData();
                var list = db.AssetProductBrands.ToList().Where(u => u.ProductId == productID);
                var assetSerialNo = dd.GetAssetSerialNo(productID);
                var retval = Json(new { list, assetSerialNo }, JsonRequestBehavior.AllowGet);
                //return Json(list, JsonRequestBehavior.AllowGet);

                return retval;

            }
            return null;
        }

        public JsonResult GetModelByBrand(int brandId)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                var list = db.AssetProductModels.ToList().Where(u => u.BrandId == brandId);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public JsonResult GetBrandListByProductForAssign(int productID)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                DropDownData dd = new DropDownData();
                var list = db.AssetProductBrands.ToList().Where(u => u.ProductId == productID);
                return Json(list, JsonRequestBehavior.AllowGet);

            }
            return null;
        }


        public JsonResult GetAssetProductSerialNoForAssign(int productId, int Brand, int Model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)//&& user.ToString() == "Admin")
            {
                DropDownData assetAssign = new DropDownData();
                // var lstList = assetAssign.GetAssetSlNoForAssign(productId, Brand, Model);
                var list = db.AssetProductInfo.Where(a => a.ProductId == productId && a.BrandId == Brand && a.ModelId == Model).ToList();
                return Json(list, JsonRequestBehavior.AllowGet);

            }
            return null;
        }

        public JsonResult GetEmployeeByDepartmentJson(int bunit, int dept)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                DropDownData dd = new DropDownData();
                var list = db.Employees.Where(a => a.Business_Unit_Id == bunit && a.Department_Id == dept).ToList();
                return Json(list, JsonRequestBehavior.AllowGet);

            }
            return null;
        }

        public JsonResult GetProductInfoByBUnitDeptEmpJson(int Bunit, int Department, int Employee)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)


            {
                //var lstList = db.Employees.Where(a => a.Business_Unit_Id == Bunit && a.Department_Id == Department).ToList();
                //var retval = Json(new { lstList }, JsonRequestBehavior.AllowGet);
                //return retval;

                VM_Asset_ReturnInfo model = new VM_Asset_ReturnInfo();
                var lstList = model.GetProductInfoList(Bunit, Department, Employee);
                var retval = Json(new { lstList }, JsonRequestBehavior.AllowGet);
                return retval;
            }
            return null;
        }

        public JsonResult GetReportByBUnitDeptEmpJson(int Bunit, int Department, int Employee)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                VM_AseetReporting model = new VM_AseetReporting();
                var lstList = model.GetAssetReportListByDeptAndEmp(Bunit, Department, Employee);
                var retval = Json(new { lstList }, JsonRequestBehavior.AllowGet);
                return retval;
            }
            return null;
        }


        public JsonResult GetReportByProductJson(int Category, int SubCategory, int Product)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                VM_AseetReporting model = new VM_AseetReporting();
                var lstList = model.GetAssetReportListByProduct(Category, SubCategory, Product);
                var retval = Json(new { lstList }, JsonRequestBehavior.AllowGet);
                return retval;
            }
            return null;
        }
        #endregion
    }

}