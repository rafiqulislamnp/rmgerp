﻿using CrystalDecisions.CrystalReports.Engine;
using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Shipment;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Shipment;
using Oss.Romo.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Commercial;

namespace Oss.Romo.Controllers
{
    //[SoftwareAdmin]
    public class ShipmentController : Controller
    {
        VmShipment_History VmShipment_History;
        VmShipment_Details VmShipment_Details;
        VmForDropDown VmForDropDown;
        Int32 UserID = 0;
        private VmShipment_UnloadingPort VmShipment_UnloadingPort;
        VmCommon_TheOrder VmCommon_TheOrder;
        VmCommon_User vmCommon_User;
        VmShipment_OrderDeliverdSchedule VmShipment_OrderDeliverdSchedule;
        VmShipment_OrderColorAndSizeRatio VmShipment_OrderColorAndSizeRatio;

        VmMkt_OrderDeliverySchedule VmMkt_OrderDeliverySchedule;
        VmShipment_Alert VmShipment_Alert;
        VmCommercial_BillOfExchange VmCommercial_BillOfExchange;
        VmCommercial_BillOfExchangeSlave VmCommercial_BillOfExchangeSlave;

        VmCommercial_Invoice VmCommercial_Invoice;
        VmCommercial_InvoiceSlave VmCommercial_InvoiceSlave;
        VmShipment_PortOfLoading VmShipment_PortOfLoading;
        VmShipment_PortOfDischarge VmShipment_PortOfDischarge;
        VmShipment_TermsOfShipment VmShipment_TermsOfShipment;
        VmAcc_IntegrationJournal VmAcc_IntegrationJournal;
        VmShipment_CNFDelivaryChallan VmShipment_CNFDelivaryChallan;
        VmShipment_CNFDelivaryChallanSlave VmShipment_CNFDelivaryChallanSlave;
        private xOssContext db = new xOssContext();


        public ShipmentController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        //.............................ShipmentHistory..............................//
        public async Task<ActionResult> VmShipment_HistoryIndex()
        {

            VmShipment_History = new VmShipment_History();
            await Task.Run(() => VmShipment_History.InitialDataLoad());

            return View(VmShipment_History);
        }

        public ActionResult VmShipment_HistoryCreate()
        {
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommonOrderDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ComOrder = sl1;
            return View();
        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_HistoryCreate(VmShipment_History VmShipment_History)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_History.Add(UserID));
                return RedirectToAction("VmShipment_HistoryIndex");
            }

            return RedirectToAction("VmShipment_HistoryIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmShipment_HistoryEdit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmShipment_History = new VmShipment_History();
            await Task.Run(() => VmShipment_History.SelectSingle(id));
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCommonOrderDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ComOrder = sl1;
            if (VmShipment_History.Shipment_History == null)
            {
                return HttpNotFound();
            }
            return View(VmShipment_History);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_HistoryEdit(Shipment_History Shipment_History)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => Shipment_History.Edit(0));
                return RedirectToAction("VmShipment_HistoryIndex", "Shipment");
            }
            return RedirectToAction("VmShipment_HistoryIndex", "Shipment");
        }
        public ActionResult VmShipment_HistoryDelete(int id)
        {

            Shipment_History b = new Shipment_History();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmShipment_HistoryIndex");
        }

        //.........................................ShipmentDetails.........................................//
        public async Task<ActionResult> VmShipment_DetailsInfo(int id)
        {

            VmShipment_Details = new VmShipment_Details();
            VmShipment_History = new VmShipment_History();
            VmShipment_Details.VmShipment_History = new VmShipment_History();
            await Task.Run(() => VmShipment_Details.VmShipment_History.SelectSingalJoined(id));
            await Task.Run(() => VmShipment_Details.InitialDataLoad(id));

            return View(VmShipment_Details);
        }

        [HttpGet]
        public ActionResult VmShipment_DetailsCreate(int id)
        {

            VmShipment_Details = new VmShipment_Details();
            VmShipment_Details.Shipment_Details = new Shipment_Details();
            VmShipment_Details.Shipment_Details.Shipment_HistoryFK = id;
            //await Task.Run(() => VmShipment_Details.SelectSinglDetailInfo(id));
            return View(VmShipment_Details);
        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_DetailsCreate(VmShipment_Details VmShipment_Details)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_Details.Add(UserID));
                return RedirectToAction("VmShipment_DetailsInfo/" + VmShipment_Details.Shipment_Details.Shipment_HistoryFK);
            }

            return RedirectToAction("VmShipment_DetailsInfo/" + VmShipment_Details.Shipment_Details.Shipment_HistoryFK);
        }


        [HttpGet]
        public async Task<ActionResult> VmShipment_DetailsEdit(string id)
        {
            string editID = id.Substring(0, id.LastIndexOf("x"));
            string rID = id.Substring(id.Length - id.LastIndexOf("x"));

            int sHostory = 0;
            int.TryParse(rID, out sHostory);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmShipment_Details = new VmShipment_Details();
            await Task.Run(() => VmShipment_Details.SelectSingle(editID));

            if (VmShipment_Details.Shipment_Details == null)
            {
                return HttpNotFound();
            }
            return View(VmShipment_Details);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_DetailsEdit(VmShipment_Details x)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Edit(0));
                return RedirectToAction("VmShipment_DetailsInfo/" + x.Shipment_Details.Shipment_HistoryFK);
            }
            return View(x);
        }
        public ActionResult VmShipment_DetailsDelete(string id)
        {
            int deletID = 0, rID = 0;
            int.TryParse(id.ToString().Substring(0, id.ToString().LastIndexOf("x")), out deletID);

            id = id.Replace(deletID.ToString() + "x", "");
            int.TryParse(id.ToString(), out rID);

            Shipment_Details b = new Shipment_Details();
            b.ID = deletID;
            b.Delete(0);
            return RedirectToAction("VmShipment_DetailsInfo/" + rID);
        }


        //........Shipment_Report............//

        //public ActionResult ShipmentReportDoc(int id)
        //{
        //    try
        //    {
        //        VmShipment_History b = new VmShipment_History();
        //        b.Shipment_History = new Shipment_History();
        //        b.SelectSingle(id);
        //        b.ShipmentDocLoad(id);
        //        ///////
        //        ReportClass cr = new ReportClass();
        //        cr.FileName = Server.MapPath("~/Views/Reports/BOMBaseDoc.rpt");
        //        cr.Load();
        //        cr.SetDataSource(b.ShipmentReportDoc);
        //        /////
        //        cr.SummaryInfo.ReportTitle = "Report of " + b.Shipment_History.ShipmentID;

        //        Stream stream =
        //            cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //        return File(stream, "application/pdf");

        //    }
        //    catch (Exception ex)
        //    {
        //        IdNullDivertion("ShipmentReportDoc", ex.Message);
        //    }
        //    return null;
        //}


        //........Shipment_Schedule............//

        public async Task<ActionResult> ShipmentScheduleIndex(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool flag = false;
            VmCommon_TheOrder = new VmCommon_TheOrder();
            VmCommon_TheOrder.Header = "Order List(In Progress):";
            VmCommon_TheOrder.ButtonName = "Close";
            if (id != null)
            {
                flag = true;
                VmCommon_TheOrder.Header = "Order List(Complete):";
                VmCommon_TheOrder.ButtonName = "ReOpen";
            }
            //-------
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmCommon_TheOrder.User_User = vMUser.User_User;
            await Task.Run(() => VmCommon_TheOrder.InitialDataLoad(flag));
            return View(VmCommon_TheOrder);
        }


        [HttpGet]
        public async Task<ActionResult> VmShipment_OrderSlaveBOM(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommon_TheOrder = new VmCommon_TheOrder();
                VmCommon_TheOrder.Common_TheOrder = new Common_TheOrder();
                await Task.Run(() => VmCommon_TheOrder = VmCommon_TheOrder.SelectSingleJoined(id));
                Mkt_BOM a = new Mkt_BOM();
                a.Common_TheOrderFk = VmCommon_TheOrder.Common_TheOrder.ID;
                VmCommon_TheOrder.VmMkt_BOM = new VmMkt_BOM();
                await Task.Run(() => VmCommon_TheOrder.VmMkt_BOM.GetBOM(id));

                int id1 = Convert.ToInt32(id);
                VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
                await Task.Run(() => VmMkt_OrderDeliverySchedule.InitialDataLoad(id1));

                VmCommon_TheOrder.VmMkt_OrderDeliverySchedule = VmMkt_OrderDeliverySchedule;

                VmShipment_OrderDeliverdSchedule = new VmShipment_OrderDeliverdSchedule();
                await Task.Run(() => VmShipment_OrderDeliverdSchedule.ShipmentReceivedQtyDataLoad(id1));
                VmCommon_TheOrder.VmShipment_OrderDeliverdSchedule = VmShipment_OrderDeliverdSchedule;


                //VmShipment_OrderColorAndSizeRatio = new VmShipment_OrderColorAndSizeRatio();
                //await Task.Run(() => VmShipment_OrderColorAndSizeRatio.InitialDataLoad(id1));
                //VmCommon_TheOrder.VmShipment_OrderColorAndSizeRatio = VmShipment_OrderColorAndSizeRatio;

                //ViewData["AllData"] = VmCommon_TheOrder.VmShipment_OrderColorAndSizeRatio.GetShipmentColorAndSizeRatio(id1);

                //VmShipment_BillOfExchange = new VmShipment_BillOfExchange();
                //await Task.Run(() => VmShipment_BillOfExchange.ShipmentDocumentList(id1));
                //VmCommon_TheOrder.VmShipment_BillOfExchange = VmShipment_BillOfExchange;

                //VmShipment_Invoice = new VmShipment_Invoice();
                //await Task.Run(() => VmShipment_Invoice.InitialDataLoad(id1));
                //VmCommon_TheOrder.VmShipment_Invoice = VmShipment_Invoice;

            }
            return View(VmCommon_TheOrder);
        }

        [HttpGet]
        public async Task<ActionResult> ShipmentInfoList(string id = "")
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_OrderDeliverdSchedule = new VmShipment_OrderDeliverdSchedule();
            await Task.Run(() => VmShipment_OrderDeliverdSchedule.ShipmentInfoDataLoad(id));

            return View(VmShipment_OrderDeliverdSchedule);
        }
        [HttpGet]
        public async Task<ActionResult> ShipmentPlanningInfo(string id = "")
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_OrderDeliverdSchedule = new VmShipment_OrderDeliverdSchedule();
            await Task.Run(() => VmShipment_OrderDeliverdSchedule.ShipmentPlanningInfoDataLoad(id));

            return View(VmShipment_OrderDeliverdSchedule);
        }
        [HttpGet]
        public async Task<ActionResult> ShipmentInfoSummery()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_OrderDeliverdSchedule = new VmShipment_OrderDeliverdSchedule();
            var now = DateTime.Now;
            var nextSix = DateTime.Now.AddMonths(6);
            var startOfMonth = new DateTime(now.Year, now.Month, 1);
            VmShipment_OrderDeliverdSchedule.FromDate = startOfMonth;

            var LastOfMonth = new DateTime(nextSix.Year, nextSix.Month, DateTime.DaysInMonth(nextSix.Year, nextSix.Month));

            VmShipment_OrderDeliverdSchedule.ToDate = LastOfMonth;
            //await Task.Run(() => VmShipment_OrderDeliverdSchedule.ShipmentInfoDataLoad1(VmShipment_OrderDeliverdSchedule));
            return View(VmShipment_OrderDeliverdSchedule);
        }

        [HttpPost]
        public async Task<ActionResult> ShipmentInfoSummery(VmShipment_OrderDeliverdSchedule model)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_OrderDeliverdSchedule = new VmShipment_OrderDeliverdSchedule();
            await Task.Run(() => VmShipment_OrderDeliverdSchedule.ShipmentInfoDataLoad1(model));

            return View(VmShipment_OrderDeliverdSchedule);
        }

        [HttpGet]
        public ActionResult VmShipment_OrderDeliverdCreate(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_OrderDeliverdSchedule = new VmShipment_OrderDeliverdSchedule();
            VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule = new Shipment_OrderDeliverdSchedule();
            VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Common_TheOrderFk = id;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStyleByOrderDropDown(id);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Style = sl2;

            return View(VmShipment_OrderDeliverdSchedule);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_OrderDeliverdCreate(VmShipment_OrderDeliverdSchedule VmShipment_OrderDeliverdSchedule)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                // var value = VmShipment_OrderDeliverdSchedule.CheckDeliverdSchedule(Convert.ToInt32(VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Mkt_OrderDeliveryFk));
                //if (Convert.ToDecimal(value) >= Convert.ToDecimal(VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.DeliverdQty))
                //{
                //    await Task.Run(() => VmShipment_OrderDeliverdSchedule.Add(UserID));

                //    if (VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Common_TheOrderFk != null)
                //    {
                //        return RedirectToAction("VmShipment_OrderSlaveBOM/" + VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Common_TheOrderFk, "Shipment");
                //    }
                //}
                //else
                //{
                //    return RedirectToAction("VmShipment_OrderSlaveBOM/" + VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Common_TheOrderFk, "Shipment");
                //}
            }

            return RedirectToAction("VmShipment_OrderSlaveBOM");
        }


        [HttpGet]
        public async Task<ActionResult> VmShipment_OrderDeliverdEdit(int orderDeliverySchedule, int commonTheOrderID, int mktBOMID)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmShipment_OrderDeliverdSchedule = new VmShipment_OrderDeliverdSchedule();
            VmForDropDown = new VmForDropDown();



            //await Task.Run(() => VmShipment_OrderDeliverdSchedule.SelectSingle(IDforEdit));
            await Task.Run(() => VmShipment_OrderDeliverdSchedule.SelectSingleForMktDeliverySchedule(orderDeliverySchedule));

            ViewBag.TotalReceivedQty = VmShipment_OrderDeliverdSchedule.GetDeliverdQuantity(orderDeliverySchedule);
            decimal AvialableQty = VmShipment_OrderDeliverdSchedule.Mkt_OrderDeliverySchedule.Quantity - Convert.ToDecimal(ViewBag.TotalReceivedQty);

            VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule = new Shipment_OrderDeliverdSchedule();
            VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Common_TheOrderFk = commonTheOrderID;
            VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Mkt_BOMFK = mktBOMID;
            VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Mkt_OrderDeliveryFk = orderDeliverySchedule;
            VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.DeliverdQty = AvialableQty;
            VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Date = DateTime.Now;
            return View(VmShipment_OrderDeliverdSchedule);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_OrderDeliverdEdit(VmShipment_OrderDeliverdSchedule VmShipment_OrderDeliverdSchedule)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int id = Convert.ToInt32(VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Mkt_OrderDeliveryFk);
            decimal val = VmShipment_OrderDeliverdSchedule.GetDeliverdQuantity(id);
            decimal deliverdQty = val + Convert.ToDecimal(VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.DeliverdQty);
            decimal addQty = Convert.ToDecimal(VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.DeliverdQty);

            decimal AvialableQty = VmShipment_OrderDeliverdSchedule.Mkt_OrderDeliverySchedule.Quantity;
            if (AvialableQty >= deliverdQty)
            {
                if (addQty != 0)
                {
                    await Task.Run(() => VmShipment_OrderDeliverdSchedule.Add(UserID));
                }
            }
            return RedirectToAction("VmShipment_OrderSlaveBOM/" + VmShipment_OrderDeliverdSchedule.Shipment_OrderDeliverdSchedule.Common_TheOrderFk, "Shipment");
        }

        [HttpGet]
        public ActionResult VmShipment_OrderDeliverdDelete(int shipmentOrderDeliverdScheduleID, int commonTheOrderID)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Shipment_OrderDeliverdSchedule b = new Shipment_OrderDeliverdSchedule();
            b.ID = shipmentOrderDeliverdScheduleID;
            b.Delete(UserID);
            return RedirectToAction("VmShipment_OrderSlaveBOM/" + commonTheOrderID, "Shipment");
        }

        [HttpGet]
        public JsonResult GetDeliveryScheduleProtNo(int? id)
        {
            xOssContext db = new xOssContext();

            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_OrderDeliverySchedule.Where(p => p.Mkt_BOMFk == id).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetOrderDeliveryQty(int id)
        {
            VmShipment_OrderDeliverdSchedule = new VmShipment_OrderDeliverdSchedule();
            string val = VmShipment_OrderDeliverdSchedule.GetOrderDeliveryRequiredQty(id);

            decimal currentQty = Convert.ToDecimal(val);

            return Json(new { success = true, qty = currentQty }, JsonRequestBehavior.AllowGet);
        }

        // Shipment color and size deliverd

        [HttpGet]
        public ActionResult VmShipment_ColorAndSizeCreate(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_OrderColorAndSizeRatio = new VmShipment_OrderColorAndSizeRatio();
            VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio = new Shipment_OrderColorAndSizeRatio();
            VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Common_TheOrderFk = id;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStyleByOrderDropDown(id);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Style = sl2;

            return View(VmShipment_OrderColorAndSizeRatio);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_ColorAndSizeCreate(VmShipment_OrderColorAndSizeRatio VmShipment_OrderColorAndSizeRatio)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                var value = VmShipment_OrderColorAndSizeRatio.CheckDeliverdColorAndSizeQty(Convert.ToInt32(VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Mkt_OrderDeliveryFk));
                if (Convert.ToDecimal(value) >= Convert.ToDecimal(VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Quantity))
                {
                    await Task.Run(() => VmShipment_OrderColorAndSizeRatio.Add(UserID));

                    if (VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Common_TheOrderFk != null)
                    {
                        return RedirectToAction("VmShipment_OrderSlaveBOM/" + VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Common_TheOrderFk, "Shipment");
                    }
                }
                else
                {
                    return RedirectToAction("VmShipment_OrderSlaveBOM/" + VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Common_TheOrderFk, "Shipment");
                }
            }

            return RedirectToAction("VmShipment_OrderSlaveBOM");
        }

        [HttpGet]
        public async Task<ActionResult> VmShipment_ColorAndSizeEdit(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmShipment_OrderColorAndSizeRatio = new VmShipment_OrderColorAndSizeRatio();
            VmForDropDown = new VmForDropDown();

            string editID = id.Substring(0, id.LastIndexOf("ʁ"));
            string rID = id.Split('ʁ').Last();


            int IDforEdit = Convert.ToInt32(editID);
            int id1 = Convert.ToInt32(rID);

            VmForDropDown.DDownData = VmForDropDown.GetStyleByOrderDropDown(id1);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Style = sl2;

            VmShipment_OrderColorAndSizeRatio = new VmShipment_OrderColorAndSizeRatio();
            await Task.Run(() => VmShipment_OrderColorAndSizeRatio.SelectSingle(IDforEdit));

            VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Mkt_OrderDeliveryFk = VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Mkt_OrderDeliveryFk;
            if (VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio == null)
            {
                return HttpNotFound();
            }

            return View(VmShipment_OrderColorAndSizeRatio);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_ColorAndSizeEdit(VmShipment_OrderColorAndSizeRatio VmShipment_OrderColorAndSizeRatio)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper vmControllerHelper = new VmControllerHelper();
            VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;

            int editid = Convert.ToInt32(VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.ID);

            if (ModelState.IsValid)
            {
                var value = VmShipment_OrderColorAndSizeRatio.CheckDeliverdColorAndSizeQtyForEdit(Convert.ToInt32(VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Mkt_OrderDeliveryFk), editid);
                if (Convert.ToDecimal(value) >= Convert.ToDecimal(VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Quantity))
                {
                    await Task.Run(() => VmShipment_OrderColorAndSizeRatio.Edit(VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.LastEditeddBy));
                }
            }

            return RedirectToAction("VmShipment_OrderSlaveBOM/" + VmShipment_OrderColorAndSizeRatio.Shipment_OrderColorAndSizeRatio.Common_TheOrderFk, "Shipment");
        }

        [HttpGet]
        public ActionResult VmShipment_OrderColorAndSizeDelete(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            string deleteID = id.Substring(0, id.LastIndexOf("ʁ"));
            string rID = id.Split('ʁ').Last();

            int IDforDelete = Convert.ToInt32(deleteID);
            int id1 = Convert.ToInt32(rID);

            Shipment_OrderColorAndSizeRatio b = new Shipment_OrderColorAndSizeRatio();
            b.ID = IDforDelete;
            b.Delete(UserID);
            return RedirectToAction("VmShipment_OrderSlaveBOM/" + id1, "Shipment");
        }

        [HttpGet]
        public JsonResult GetDeliveryScheduleColorAndSize(int? id)
        {
            xOssContext db = new xOssContext();

            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_OrderColorAndSizeRatio.Where(p => p.Mkt_BOMFk == id).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOrderColorAndSizeDeliveryQty(int id)
        {
            VmShipment_OrderColorAndSizeRatio = new VmShipment_OrderColorAndSizeRatio();
            string val = VmShipment_OrderColorAndSizeRatio.GetOrderColorAndSizeDeliveryRequiredQty(id);

            decimal currentQty = Convert.ToDecimal(val);

            return Json(new { success = true, qty = currentQty }, JsonRequestBehavior.AllowGet);
        }

        //Shipment alert 

        public async Task<ActionResult> ShipmentAlertIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmShipment_Alert = new VmShipment_Alert();
            await Task.Run(() => VmShipment_Alert.InitialDataLoad());
            return View(VmShipment_Alert);
        }

        [HttpGet]
        public async Task<ActionResult> VmShipmentAlertEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmShipment_Alert = new VmShipment_Alert();
            await Task.Run(() => VmShipment_Alert.SelectSingle(id));
            if (VmShipment_Alert.Alert == null)
            {
                return HttpNotFound();
            }
            return View(VmShipment_Alert);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipmentAlertEdit(VmShipment_Alert VmShipment_Alert)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {

                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                if (VmShipment_Alert.Alert.Type == "Expired")
                {

                }
                else
                {
                    await Task.Run(() => VmShipment_Alert.Edit(UserID));
                }
            }
            return RedirectToAction("ShipmentAlertIndex");
        }

        public ActionResult BillOfExchangeReport(int id)
        {
            try
            {
                VmShipment_BillOfExchange b = new VmShipment_BillOfExchange();
                b.ShipmentBillOfExchange(id);

                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/CommercialBillOfExchangeMaster.rpt");
                cr.Load();
                cr.SetDataSource(b.BillOfExchage);
                cr.SummaryInfo.ReportTitle = "Bill Of Exchange";
                cr.Subreports["CommercialBillOfExchange.rpt"].SetDataSource(b.BillOfExchage);
                cr.Subreports["CommercialBillOfExchange2.rpt"].SetDataSource(b.BillOfExchage);
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {

            }
            return null;
        }


        // Shipment Invoice report


        public ActionResult InvoiceReport(int id)
        {
            try
            {
                VmShipment_BillOfExchange b = new VmShipment_BillOfExchange();
                b.ShipmentInvoice(id);

                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/CommercialInvoice.rpt");
                cr.Load();
                cr.SetDataSource(b.BillOfExchage);
                cr.SummaryInfo.ReportTitle = "Invoice Report";
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {

            }
            return null;
        }


        // Shipment Invoice report

        public ActionResult PackingListReport(int id)
        {
            try
            {
                VmShipment_BillOfExchange b = new VmShipment_BillOfExchange();
                b.PackingList(id);

                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/CommercialPackingList.rpt");
                cr.Load();
                cr.SetDataSource(b.packingList);
                cr.SummaryInfo.ReportTitle = "Packing List";
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpGet]
        public async Task<ActionResult> PortOfLoadingIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmShipment_PortOfLoading = new VmShipment_PortOfLoading();
            await Task.Run(() => VmShipment_PortOfLoading.InitialDataLoad());
            return View(VmShipment_PortOfLoading);
        }

        [HttpGet]
        public ActionResult PortOfLoadingCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PortOfLoadingCreate(VmShipment_PortOfLoading VmShipment_PortOfLoading)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_PortOfLoading.Add(UserID));
            }
            return RedirectToAction("PortOfLoadingIndex");
        }


        [HttpGet]
        public async Task<ActionResult> PortOfLoadingEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmShipment_PortOfLoading = new VmShipment_PortOfLoading();
            await Task.Run(() => VmShipment_PortOfLoading.SelectSingle(id));
            if (VmShipment_PortOfLoading.Shipment_PortOfLoading == null)
            {
                return HttpNotFound();
            }
            return View(VmShipment_PortOfLoading);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PortOfLoadingEdit(VmShipment_PortOfLoading VmShipment_PortOfLoading)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_PortOfLoading.Edit(UserID));
            }
            return RedirectToAction("PortOfLoadingIndex");
        }

        [HttpGet]
        public ActionResult PortOfLoadingDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Shipment_PortOfLoading b = new Shipment_PortOfLoading();
            b.ID = id;
            b.Delete(UserID);
            return RedirectToAction("PortOfLoadingIndex");
        }



        [HttpGet]
        public async Task<ActionResult> PortOfDischargeIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmShipment_PortOfDischarge = new VmShipment_PortOfDischarge();
            await Task.Run(() => VmShipment_PortOfDischarge.InitialDataLoad());
            return View(VmShipment_PortOfDischarge);
        }

        [HttpGet]
        public ActionResult PortOfDischargeCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PortOfDischargeCreate(VmShipment_PortOfDischarge VmShipment_PortOfDischarge)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_PortOfDischarge.Add(UserID));
            }
            return RedirectToAction("PortOfDischargeIndex");
        }


        [HttpGet]
        public async Task<ActionResult> PortOfDischargeEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmShipment_PortOfDischarge = new VmShipment_PortOfDischarge();
            await Task.Run(() => VmShipment_PortOfDischarge.SelectSingle(id));
            if (VmShipment_PortOfDischarge.Shipment_PortOfDischarge == null)
            {
                return HttpNotFound();
            }
            return View(VmShipment_PortOfDischarge);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PortOfDischargeEdit(VmShipment_PortOfDischarge VmShipment_PortOfDischarge)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_PortOfDischarge.Edit(UserID));
            }
            return RedirectToAction("PortOfDischargeIndex");
        }

        [HttpGet]
        public ActionResult PortOfDischargeDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Shipment_PortOfDischarge b = new Shipment_PortOfDischarge();
            b.ID = id;
            b.Delete(UserID);
            return RedirectToAction("PortOfDischargeIndex");
        }



        [HttpGet]
        public async Task<ActionResult> VmTermsOfShipmentIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmShipment_TermsOfShipment = new VmShipment_TermsOfShipment();
            await Task.Run(() => VmShipment_TermsOfShipment.InitialDataLoad());
            return View(VmShipment_TermsOfShipment);
        }

        [HttpGet]
        public ActionResult VmTermsOfShipmentCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmTermsOfShipmentCreate(VmShipment_TermsOfShipment VmShipment_TermsOfShipment)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_TermsOfShipment.Add(UserID));
            }
            return RedirectToAction("VmTermsOfShipmentIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmTermsOfShipmentEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmShipment_TermsOfShipment = new VmShipment_TermsOfShipment();
            await Task.Run(() => VmShipment_TermsOfShipment.SelectSingle(id));
            if (VmShipment_TermsOfShipment.Shipment_TermsOfShipment == null)
            {
                return HttpNotFound();
            }
            return View(VmShipment_TermsOfShipment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmTermsOfShipmentEdit(VmShipment_TermsOfShipment VmShipment_TermsOfShipment)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_TermsOfShipment.Edit(UserID));
            }
            return RedirectToAction("VmTermsOfShipmentIndex");
        }

        [HttpGet]
        public ActionResult VmTermsOfShipmentDelete(int id)
        {
            VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Shipment_TermsOfShipment b = new Shipment_TermsOfShipment();
            b.ID = id;
            b.Delete(UserID);
            return RedirectToAction("VmTermsOfShipmentIndex");
        }

        #region CNFDelivaryChallan

        public async Task<ActionResult> VmCNFDelivaryChallan()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
           
            VmShipment_CNFDelivaryChallan = new VmShipment_CNFDelivaryChallan();
            VmShipment_CNFDelivaryChallan.User_User = user.User_User;
            VmShipment_CNFDelivaryChallan.User_User.ID = user.User_User.ID;
            await Task.Run(() => VmShipment_CNFDelivaryChallan.InitialDataLoad());
            return View(VmShipment_CNFDelivaryChallan);
        }
        public ActionResult VmCNFDelivaryChallanCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmShipment_CNFDelivaryChallan = new VmShipment_CNFDelivaryChallan();
            VmShipment_CNFDelivaryChallan.Shipment_CNFDelivaryChallan = new Shipment_CNFDelivaryChallan();

            VmForDropDown.DDownData = VmForDropDown.GetShipment_UnloadingPort();
            SelectList sl0 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UnloadingPort = sl0;

            VmForDropDown.DDownData = VmForDropDown.GetSupplerCnfTransportById(2);
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CNFSuppliers = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetSupplerCnfTransportById(3);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CNFTransportSuppliers = sl2;

            VmShipment_CNFDelivaryChallan.Shipment_CNFDelivaryChallan.Date = DateTime.Now;
            VmShipment_CNFDelivaryChallan.Shipment_CNFDelivaryChallan.GatePassDate = DateTime.Now;
            VmShipment_CNFDelivaryChallan.Shipment_CNFDelivaryChallan.FirstCreatedBy = user.User_User.ID;
            VmShipment_CNFDelivaryChallan.Shipment_CNFDelivaryChallan.Description = "100% Complete Garments. Delivery For Export.";
            return View(VmShipment_CNFDelivaryChallan);
        }
        [HttpPost]
        public async Task<ActionResult> VmCNFDelivaryChallanCreate(VmShipment_CNFDelivaryChallan VmShipment_CNFDelivaryChallan)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {

                await Task.Run(() => VmShipment_CNFDelivaryChallan.Add(UserID));
            }
            return RedirectToAction("VmCNFDelivaryChallan");
        }

        public ActionResult VmCNFDelivaryChallanDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Shipment_CNFDelivaryChallan b = new Shipment_CNFDelivaryChallan { ID = id };
            b.Delete(UserID);
            return RedirectToAction("VmCNFDelivaryChallan");
        }
        [HttpGet]
        public async Task<ActionResult> VmCNFDelivaryChallanEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmShipment_CNFDelivaryChallan = new VmShipment_CNFDelivaryChallan();

            await Task.Run(() => VmShipment_CNFDelivaryChallan.SelectSingle(id));
            VmShipment_CNFDelivaryChallan.Shipment_CNFDelivaryChallan.LastEditeddBy = user.User_User.ID;
            VmForDropDown.DDownData = VmForDropDown.GetShipment_UnloadingPort();
            SelectList sl0 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UnloadingPort = sl0;

            VmForDropDown.DDownData = VmForDropDown.GetSupplerCnfTransport();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CNFSuppliers = sl1;
            VmForDropDown.DDownData = VmForDropDown.GetCnfTransportSuppler();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CNFTransportSuppliers = sl2;

            if (VmShipment_CNFDelivaryChallan.Shipment_CNFDelivaryChallan == null)
            {
                return HttpNotFound();
            }
            return View(VmShipment_CNFDelivaryChallan);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCNFDelivaryChallanEdit(VmShipment_CNFDelivaryChallan VmShipment_CNFDelivaryChallan)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_CNFDelivaryChallan.Edit(UserID));
            }
            return RedirectToAction("VmCNFDelivaryChallan");
        }

        public async Task<ActionResult> VmCNFDelivaryChallanSlaveIndex(int id, int buyerId = 0)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_CNFDelivaryChallanSlave = new VmShipment_CNFDelivaryChallanSlave();
            VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule = new Shipment_OrderDeliverdSchedule();
            VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.ID = id;
            VmShipment_CNFDelivaryChallanSlave.BuyerId = buyerId;
            await Task.Run(() => VmShipment_CNFDelivaryChallanSlave.InitialDataLoad(id));
            //await Task.Run(() => VmShipment_CNFDelivaryChallanSlave.SelectSingle(id));

            return View(VmShipment_CNFDelivaryChallanSlave);
        }
        public ActionResult VmCNFDelivaryChallanSlaveCreate(int id, int buyerId = 0)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmShipment_CNFDelivaryChallanSlave VmShipment_CNFDelivaryChallanSlave = new VmShipment_CNFDelivaryChallanSlave();
            VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule = new Shipment_OrderDeliverdSchedule();
            VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.Shipment_CNFDelivaryChallanFk = id;
            VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.FirstCreatedBy = user.User_User.ID;

            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            SelectList s1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Buyer = s1;

            VmForDropDown.DDownData = VmForDropDown.GetBropDownBOMCID();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl1;
            VmShipment_CNFDelivaryChallanSlave.BuyerId = buyerId;
            return View(VmShipment_CNFDelivaryChallanSlave);
        }

        [HttpPost]
        public async Task<ActionResult> VmCNFDelivaryChallanSlaveCreate(VmShipment_CNFDelivaryChallanSlave VmShipment_CNFDelivaryChallanSlave)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var a = db.Mkt_BOM.FirstOrDefault(x =>
                    x.ID == VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.Mkt_BOMFK);
            if (a != null)
            {
                VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.Common_TheOrderFk =
                    a.Common_TheOrderFk;
                VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.DeliverdQty =
                    a.Quantity * VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.PackQty;
            }
            VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.Date = DateTime.Now;
            //if (ModelState.IsValid)
            //{

            await Task.Run(() => VmShipment_CNFDelivaryChallanSlave.Add(UserID));
            //}
            return RedirectToAction("VmCNFDelivaryChallanSlaveIndex", new { id = VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.Shipment_CNFDelivaryChallanFk, buyerId = VmShipment_CNFDelivaryChallanSlave.BuyerId });
        }

        public ActionResult VmCNFDelivaryChallanSlaveDelete(int cid, int mid, int buyerId)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Shipment_OrderDeliverdSchedule b = new Shipment_OrderDeliverdSchedule { ID = cid };
            b.Delete(UserID);
            return RedirectToAction("VmCNFDelivaryChallanSlaveIndex", new { id = mid, buyerId = buyerId });
        }
        [HttpGet]
        public async Task<ActionResult> VmCNFDelivaryChallanSlaveEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            SelectList s1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Buyer = s1;

            VmShipment_CNFDelivaryChallanSlave = new VmShipment_CNFDelivaryChallanSlave();
            await Task.Run(() => VmShipment_CNFDelivaryChallanSlave.SelectSingle(id));
            VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.LastEditeddBy = user.User_User.ID;
            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            SelectList s3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Buyer = s3;

            VmForDropDown.DDownData = VmForDropDown.GetBropDownBOMCID();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl1;

            if (VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.Mkt_BOMFK != null)
                VmForDropDown.DDownData = VmForDropDown.GetOrderDeliverdScheduleByBOM(VmShipment_CNFDelivaryChallanSlave
                    .Shipment_OrderDeliverdSchedule.Mkt_BOMFK.Value);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Port = sl2;

            if (VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule == null)
            {
                return HttpNotFound();
            }
            return View(VmShipment_CNFDelivaryChallanSlave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCNFDelivaryChallanSlaveEdit(VmShipment_CNFDelivaryChallanSlave VmShipment_CNFDelivaryChallanSlave)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                var a = db.Mkt_BOM.FirstOrDefault(x =>
                    x.ID == VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.Mkt_BOMFK);
                if (a != null)
                {
                    VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.Common_TheOrderFk =
                        a.Common_TheOrderFk;
                    VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.DeliverdQty =
                        a.Quantity * VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.PackQty;
                }

                await Task.Run(() => VmShipment_CNFDelivaryChallanSlave.Edit(UserID));

            }
            return RedirectToAction("VmCNFDelivaryChallanSlaveIndex", new { id = VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule.Shipment_CNFDelivaryChallanFk, buyerId = VmShipment_CNFDelivaryChallanSlave.BuyerId });
        }

        public async Task<ActionResult> VmCNFDelivaryChallanReport(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_CNFDelivaryChallan = new VmShipment_CNFDelivaryChallan();
            await Task.Run(() => VmShipment_CNFDelivaryChallan.CNFDelivaryChallan(id));
            return View(VmShipment_CNFDelivaryChallan);
        }


        public async Task<ActionResult> VmShipment_UnloadingPortIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_UnloadingPort = new VmShipment_UnloadingPort();
            await Task.Run(() => VmShipment_UnloadingPort.InitialDataLoad());

            return View(VmShipment_UnloadingPort);
        }

        public ActionResult VmShipment_UnloadingPortCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_UnloadingPortCreate(VmShipment_UnloadingPort VmShipment_UnloadingPort)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_UnloadingPort.Add(UserID));
                return RedirectToAction("VmShipment_UnloadingPortIndex");
            }

            return RedirectToAction("VmShipment_UnloadingPortIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmShipment_UnloadingPortEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmShipment_UnloadingPort = new VmShipment_UnloadingPort();
            await Task.Run(() => VmShipment_UnloadingPort.SelectSingle(id));

            if (VmShipment_UnloadingPort.Shipment_UnloadingPort == null)
            {
                return HttpNotFound();
            }
            return View(VmShipment_UnloadingPort);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmShipment_UnloadingPortEdit(VmShipment_UnloadingPort VmShipment_UnloadingPort)
        {
            VmUser_User user = (VmUser_User)Session["One"];

            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmShipment_UnloadingPort.Edit(0));
                return RedirectToAction("VmShipment_UnloadingPortIndex");
            }
            return View(VmShipment_UnloadingPort);
        }
        public ActionResult VmShipment_UnloadingPortDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Shipment_UnloadingPort b = new Shipment_UnloadingPort();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmShipment_UnloadingPortIndex");
        }

        public async Task<ActionResult> InvoicedDeliveryChallan()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_CNFDelivaryChallan = new VmShipment_CNFDelivaryChallan();
            await Task.Run(() => VmShipment_CNFDelivaryChallan.InvoicedDeliveryChallan());

            return View(VmShipment_CNFDelivaryChallan);
        }

        public async Task<ActionResult> WithoutInvoicedDeliveryChallan()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_CNFDelivaryChallan = new VmShipment_CNFDelivaryChallan();
            await Task.Run(() => VmShipment_CNFDelivaryChallan.WithoutInvoicedDeliveryChallan());

            return View(VmShipment_CNFDelivaryChallan);
        }
        public async Task<ActionResult> ViewWithoutInvoicedDeliveryChallanDetails(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmShipment_CNFDelivaryChallanSlave = new VmShipment_CNFDelivaryChallanSlave();
            VmShipment_CNFDelivaryChallanSlave.Shipment_OrderDeliverdSchedule = new Shipment_OrderDeliverdSchedule();
            await Task.Run(() => VmShipment_CNFDelivaryChallanSlave.InitialDataLoadForWithoutInvoice(id));
            //await Task.Run(() => VmShipment_CNFDelivaryChallanSlave.SelectSingle(id));

            return View(VmShipment_CNFDelivaryChallanSlave);
        }
        #endregion

    }
}