﻿using CrystalDecisions.CrystalReports.Engine;
using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.ViewModels.Common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.User;
using Oss.Romo.ViewModels.Home;
using System.Data;
using Oss.Romo.Models.Accounts;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Commercial;
using Oss.Romo.ViewModels.Store;

namespace Oss.Romo.Controllers
{

    public class HomeController : Controller
    {
        private xOssContext db = new xOssContext();
        VmControllerHelper VmControllerHelper;
        VmMis_AdminBoard VmMis_AdminBoard;
        VmAccounting VmAccounting;
        VmMkt_PO VmMkt_PO;

        string UserName;

        public HomeController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }

        public DataTable GetTable()
        {
            DataTable dtResult = new DataTable();
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("Name", typeof(string));
            
            // Here we add five DataRows.
            table.Rows.Add(1, "Indocin");
            table.Rows.Add(2, "Enebrel");
            table.Rows.Add(3, "Hydralazine");
            table.Rows.Add(4, "Combivent");
            table.Rows.Add(5, "Dilantin");

            return table;
        }
        
        public ActionResult Index()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                VmControllerHelper vmControllerHelper = new VmControllerHelper();
                UserName = vmControllerHelper.GetCurrentUser().User_User.Name;
                int userDepartment = (int)vmControllerHelper.GetCurrentUser().User_User.User_DepartmentFK;

                if (UserName.Equals("MOHSIN"))
                {
                    return RedirectToAction("AdminBoard", "Home");
                }
                else if (UserName.Equals("MIS"))
                {
                    return RedirectToAction("Notifier", "MisDashboard");
                }
                else if (UserName.Equals("HRMS"))
                {
                    return RedirectToAction("Index", "HRMSEmployeeRecord");
                }
                //else
                //{
                //VmAccounting = new VmAccounting();
                //VmMkt_PO = new VmMkt_PO();
                //VmMkt_PO.ValuePo = VmMkt_PO.Tpo();
                //VmMkt_PO.ValueMlc = VmMkt_PO.Tmlc();
                //VmMkt_PO.ValueB2blc = VmMkt_PO.Tb2blc();
                //return View(VmMkt_PO);
                ViewBag.DepartmentFk = userDepartment;
                    return View();
                //}
            }

        }

        public ActionResult BackLaout()
        {
            return View();
        }

        public ActionResult SignOut()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user != null)
            {
                Session["One"] = null;

                HttpCookie currentUserCookie = HttpContext.Request.Cookies["UserLogin"];
                HttpContext.Response.Cookies.Remove("UserLogin");
                //currentUserCookie.Expires = DateTime.Now.AddDays(-10);
                //currentUserCookie.Value = null;
            }

            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public ActionResult Login()
        {
            //HttpCookie AloneC = Request.Cookies["Menu"];
            //try
            //{
            //    Request.Cookies["One"].Expires = DateTime.Now;
            //    Request.Cookies["Menu"].Expires = DateTime.Now;
            //    Request.Cookies["Alone"].Expires = DateTime.Now;
            //    Request.Cookies["UserID"].Expires = DateTime.Now;
            //    Request.Cookies["UserName"].Expires = DateTime.Now;
            //}
            //catch { }
            //ViewBag.ReturnUrl = returnUrl;
            HttpCookie currentUserCookie = HttpContext.Request.Cookies["UserLogin"];
            HttpContext.Response.Cookies.Remove("UserLogin");
            //currentUserCookie.Expires = DateTime.Now.AddDays(-10);
            //currentUserCookie.Value = null;
            return View();
        }

        [HttpPost]
        public ActionResult Login(VmUser_User VmUser_User)
        {

            if (VmUser_User.User_User.UserName == null || VmUser_User.User_User.Password == null)
            {
                VmUser_User.LblError = "Please enter Username and Password";
                return View(VmUser_User);
            }
            bool hasCokkie = false;
            if (VmUser_User.LoginUser(hasCokkie) > 0)
            {

                Session["One"] = VmUser_User;
                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("Name", VmUser_User.User_User.UserName);
                cookie.Values.Add("Password", VmUser_User.User_User.Password);
                cookie.Values.Add("UserID", VmUser_User.User_User.ID.ToString());
                if (VmUser_User.User_User.HRMS_EmployeeFK != null)
                {
                    cookie.Values.Add("Photo", VmUser_User.User_User.Photo.ToString());
                    cookie.Values.Add("EmployeeId", VmUser_User.User_User.HRMS_EmployeeFK.ToString());
                }
                cookie.Values.Add("IsSupperUser", VmUser_User.User_User.IsSupperUser.ToString());

                cookie.Expires = DateTime.Now.AddMinutes(300);
                Request.Cookies.Add(cookie);

                Response.Cookies.Add(cookie);

                var s = Request.Cookies["UserLogin"];
                var ss = Response.Cookies["UserLogin"];
                var asddfsd = s.Values["Name"];
                var sadfds = s.Values["Password"];

                // MvcApplication.userName  = VmUser_User.User_User.Name;

                return RedirectToAction("Index", "Home");

            }
            else
            {
                VmUser_User.LblError = "User/Password does not match!";
            }
            
            return View(VmUser_User);
        }

        public ActionResult UserNamePassword()
        {
            return View();
        }

        private void GetSession()
        {

        }

        public async Task<ActionResult> AdminBoard()
        {
            GetSession();
            ViewModels.User.VmUser_User user = (ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmAcc_SupplierPH VmAcc_SupplierPH = new VmAcc_SupplierPH();
            SumofPayableDueSupplier s = await Task.Run(() => VmAcc_SupplierPH.SumOfDueAndPayableForSupplier());
            ViewBag.SupplierData = s;

            VmMis_AdminBoard VmMis_AdminBoard = new VmMis_AdminBoard();
            VmMis_AdminBoard.GetCollectionDueReceivableAmountLastMonths(DateTime.Now);
            ViewBag.CollectionDueReceivableAmount = VmMis_AdminBoard.CollectionDueReceivableAmount;


            VmMis_AdminBoard = new VmMis_AdminBoard();
            VmMis_AdminBoard.GetCollectionDueReceivableAmountLast12Months(DateTime.Now);
            ViewBag.CollectionDueReceivableAmount12Months = VmMis_AdminBoard.CollectionDueReceivableAmount;

            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.BuyerChart());
            ViewBag.BuyerList = VmMis_AdminBoard.BuyerInfoChartList;


            VmStoreInventory VmStoreInventory = new VmStoreInventory();
            //var data = VmStoreInventory.CategoryWiseStock();
            var data = await Task.Run(() => VmStoreInventory.CategoryWiseStock());
            ViewBag.CategoryStock = data;

            VmMis_AdminBoard.GetCollectionDueReceivableAmountLast12Months(DateTime.Now);
            ViewBag.CollectionDueReceivableAmount12Months = VmMis_AdminBoard.CollectionDueReceivableAmount;

            VmMis_AdminBoard = new VmMis_AdminBoard();
            await Task.Run(() => VmMis_AdminBoard.GetSummaryReport());
            await Task.Run(() => VmMis_AdminBoard.GetRealisedAmountLoad());
            await Task.Run(() => VmMis_AdminBoard.GetTotalLcTtAmount());
            await Task.Run(() => VmMis_AdminBoard.GetTotalLcBtbLcPoAmount());
            return View(VmMis_AdminBoard);
        }
        
        // GET: Authorization
        public ActionResult UnAuthorized()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult ErrorView()
        {
            //string messege
            //ErrorReports x = new ErrorReports();
            //x.ErrorDescription = messege;

            return View();
        }
        [HttpPost]
        public ActionResult ErrorView(ErrorReports er)
        {

            er.EmailErrorCustom();
            return View();
        }
        public ActionResult ErrorView404()
        {
            //string messege
            //ErrorReports x = new ErrorReports();
            //x.ErrorDescription = messege;

            return View();
        }
        public ActionResult ErrorView500()
        {
            //string messege
            //ErrorReports x = new ErrorReports();
            //x.ErrorDescription = messege;

            return View();
        }

        public ActionResult About()
        {
            //string messege
            //ErrorReports x = new ErrorReports();
            //x.ErrorDescription = messege;

            return View();
        }
    }
}