﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.Models;
using Oss.Romo.Models.Entity;
using Oss.Romo.Models.Entity.Common;
using Oss.Romo.Models.Entity.HRMS;
using Oss.Romo.Models.Services;
using Oss.Romo.Models.ViewModels.HRMS;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using Oss.Romo.Helper;
using Oss.Romo.Models.ViewModels.Reports;
using Oss.Romo.Models.ViewModels.Bonus;
using Oss.Romo.Models.ViewModels.Salary;
using CrystalDecisions.Shared;
using Oss.Romo.ViewModels.User;

namespace Oss.Romo.Controllers
{
   

    public class HRMSPayrollController : Controller
    {
        DropDownData d = new DropDownData();
        xOssContext db = new xOssContext();
        private VM_HRMS_Payroll VM_HRMS_Payroll;
        VMEmployee VMEmployees;
        VmUser_User VmUser_User;

        //CurrentUser user;

        public HRMSPayrollController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            VMEmployees = new VMEmployee();
          //  this.user = sessionHandler.CurrentUser;
        }

        #region ManagePayroll
        public async Task<ActionResult> ManagePayroll()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetPayrollList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreatePayroll()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Monthly = new SelectList(d.GetMonthlyList(), "Value", "Text");
            //DateTime date = DateTime.Now;
            //int totalDay = DateTime.DaysInMonth(date.Year, date.Month-1);

            //DateTime FromDate = new DateTime(date.Year, date.Month-1, 1);
            //DateTime ToDate = new DateTime(date.Year, date.Month-1, totalDay);

            //VM_HRMS_Payroll Payroll = new VM_HRMS_Payroll();
            //Payroll.Monthly = 1;
            //Payroll.HasFastivalBonus = 0;
            //Payroll.FromDate = FromDate;
            //Payroll.ToDate = ToDate;

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreatePayroll(VM_HRMS_Payroll model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Monthly = new SelectList(d.GetMonthlyList(), "Value", "Text");
            model.Entry_By = u.User_User.ID.ToString();
            model.Entry_Date = DateTime.Now;

            await Task.Run(() => model.CreatePayroll1(model));
            if (string.IsNullOrEmpty(model.PayrollStatus))
            {
                await Task.Run(() => model.Save());

            }
            return RedirectToAction("ManagePayroll", "HRMSPayroll");
        }

        [HttpGet]
        public async Task<ActionResult> ClosePayroll(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.ClosePayroll(id));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManagePayroll", "HRMSPayroll");
        }

        #endregion

        #region PayrollProcess
        [HttpGet]
        public async Task<ActionResult> ProcessPayroll(int id, bool AID)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            ViewBag.UnitList = new SelectList(d.GetUnitList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            model.AddCompliance = AID;
            await Task.Run(() => model.GetPayrollDetails(id));
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ProcessPayroll(VM_HRMS_Payroll model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = u.User_User.ID.ToString();
            model.Update_By = u.User_User.ID.ToString();
            model.Entry_Date = DateTime.Now;
            model.Update_Date = DateTime.Now;
            if (model.AddCompliance)
            {
                await Task.Run(() => model.GetProcessAuditPayroll(model));
                await Task.Run(() => model.Save());
                return RedirectToAction("PayrollDetails", "HRMSPayroll", new { id = model.PayRollID, AID = model.AddCompliance });
            }
            else
            {
                //await Task.Run(() => model.GetProcessPayroll(model));
                await Task.Run(() => model.GetProcessRegularPayroll(model));
                await Task.Run(() => model.Save());
                return RedirectToAction("PayrollDetails", "HRMSPayroll", new { id = model.PayRollID, AID = model.AddCompliance });
            }
        }

        public ActionResult PreviousPayrollView()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Month = new SelectList(d.GetOpenPayrollList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult PreviousPayrollView(VM_HRMS_Payroll VM_HRMS_Payroll)
        {
            string id = VM_HRMS_Payroll.Month.ToString();
            try
            {
                return RedirectToAction("PayrollDetails", new { ID = id });
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public async Task<ActionResult> PayrollDetails(int id, bool AID)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.AddCompliance = AID;
            if (VM_HRMS_Payroll.AddCompliance)
            {
                await Task.Run(() => VM_HRMS_Payroll.AuditPayrollList(id));
            }
            else
            {
                await Task.Run(() => VM_HRMS_Payroll.GeneralPayrollList(id));
            }

            return View(VM_HRMS_Payroll);
        }

        [HttpGet]
        public async Task<ActionResult> VmPayrollDetails(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int PayrollID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("X")), out PayrollID);
            int EmployeeID = 0;
            id = id.Replace(PayrollID.ToString() + "X", "");
            int.TryParse(id, out EmployeeID);

            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.PayRollID = PayrollID;
            await Task.Run(() => VM_HRMS_Payroll.ListVmPayrollDetails(PayrollID, EmployeeID));

            return View(VM_HRMS_Payroll);
        }

        [HttpGet]
        public async Task<ActionResult> VmPayrollEdit(string id, bool AID)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int PayrollID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("X")), out PayrollID);
            int EmployeeID = 0;
            id = id.Replace(PayrollID.ToString() + "X", "");
            int.TryParse(id, out EmployeeID);

            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.EmployeeID = EmployeeID;
            VM_HRMS_Payroll.PayRollID = PayrollID;
            VM_HRMS_Payroll.Update_By = u.User_User.ID.ToString();
            VM_HRMS_Payroll.Update_Date = DateTime.Today;
            VM_HRMS_Payroll.AddCompliance = AID;
            if (VM_HRMS_Payroll.AddCompliance)
            {
                await Task.Run(() => VM_HRMS_Payroll.GetUpdateAuditEmployeePayroll(VM_HRMS_Payroll));
            }
            else
            {
                await Task.Run(() => VM_HRMS_Payroll.GetUpdateEmployeePayroll(VM_HRMS_Payroll));
            }

            return RedirectToAction("PayrollDetails", "HRMSPayroll", new { id = VM_HRMS_Payroll.PayRollID, AID = VM_HRMS_Payroll.AddCompliance });
        }
        #endregion

        #region Payslip
        [HttpGet]
        public ActionResult PaySlip()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.PayrollList = new SelectList(d.GetPayrollList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult PaySlip(VM_HRMS_Payroll model)
        {
            DtPayroll ds = new DtPayroll();

            ReportClass cr = new ReportClass();
            if (model.SectionID > 0)
            {
                ds = model.LoadPaySlip(model);
            }
            else
            {
                ds = model.LoadPaySlipAll(model);
            }
            cr.FileName = Server.MapPath("~/Views/Reports/PaySlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "PaySlip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        public ActionResult GeneratePaySlip(VM_HRMS_Payroll model)
        {
            DtPayroll ds = new DtPayroll();
            ReportClass cr = new ReportClass();
            if (model.SectionID > 0)
            {
                ds = model.LoadPaySlip(model);
            }
            else
            {
                ds = model.LoadPaySlipAll(model);
            }
            cr.FileName = Server.MapPath("~/Views/Reports/PaySlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "PaySlip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }


        public ActionResult PaySlipByEmployeeID(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int PayrollID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("X")), out PayrollID);
            int EmployeeID = 0;
            id = id.Replace(PayrollID.ToString() + "X", "");
            int.TryParse(id, out EmployeeID);

            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.EmployeeID = EmployeeID;
            VM_HRMS_Payroll.ID = PayrollID;

            DtPayroll ds = new DtPayroll();
            ds = VM_HRMS_Payroll.LoadPaySlip(VM_HRMS_Payroll);
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/PaySlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "PaySlip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        #endregion

        #region ManageSalary
        [HttpGet]
        public async Task<ActionResult> ManageSalary()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetSalaryList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateSalary()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetSalaryCreateEmployeeList(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            model.OTAllow = null;
            model.Default = true;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateSalary(VM_HRMS_Payroll model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            model.Entry_By = u.User_User.ID.ToString();
            model.Entry_Date = DateTime.Now;

            await Task.Run(() => model.GetEmployeeInfo(model.EmployeeID));
            await Task.Run(() => model.GetCreateSalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageSalary", "HRMSPayroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateSalary(int Id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            await Task.Run(() => model.GetEmployeeGrossSalary(Id));
            //await Task.Run(() => model.GetCompliance(Id));
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateSalary(VM_HRMS_Payroll model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = u.User_User.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateSalary(model));
            //await Task.Run(() => model.Save());

            return RedirectToAction("ManageSalary", "HRMSPayroll");
        }

        public ActionResult DeleteSalary(int Id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.GetEmployeeSalaryDelete(Id);

            return RedirectToAction("ManageSalary", "HRMSPayroll");
        }

        #endregion

        #region AuditManageSalary

        [HttpGet]
        public async Task<ActionResult> ManageASalary()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetAuditSalaryList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateASalary()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetAuditSalaryCreateEmployeeList(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            model.OTAllow = null;
            model.Default = true;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateASalary(VM_HRMS_Payroll model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            model.Entry_By = u.User_User.ID.ToString();
            model.Entry_Date = DateTime.Now;

            await Task.Run(() => model.GetEmployeeInfo(model.EmployeeID));
            await Task.Run(() => model.GetCreateASalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageASalary", "HRMSPayroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateASalary(int Id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            //await Task.Run(() => model.GetEmployeeGrossSalary(Id));
            await Task.Run(() => model.GetAuditGrossSalary(Id));

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateASalary(VM_HRMS_Payroll model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = u.User_User.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateASalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageASalary", "HRMSPayroll");
        }

        public async Task<ActionResult> ManageAPayroll()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetPayrollList());
            return View(model);
        }

        #endregion

        #region StaffNightAndHolydayBill
        public async Task<ActionResult> ManageBill()
        {

            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
                VM_HRMS_Payroll model = new VM_HRMS_Payroll();
                await Task.Run(() => model.GetUnassignBill());
                return View(model);
          
        }

        public async Task<ActionResult> ManageAssignedBill()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
                await Task.Run(() => model.GetBillList());
                return View(model);
        }

        [HttpGet]
        public ActionResult CreateBill()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.DesignationList = new SelectList(d.GetDesignationList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateBill(VM_HRMS_Payroll model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = u.User_User.ID.ToString();
            model.Entry_Date = DateTime.Now;

            await Task.Run(() => model.GetCreateNightBill(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBill", "HRMSPayroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateBill(int Id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            await Task.Run(() => model.GetEmployeeBill(Id));

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateBill(VM_HRMS_Payroll model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = u.User_User.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateNightBill(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBill", "HRMSPayroll");
        }

        public ActionResult DeleteBill(int Id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.GetDeleteNightBill(Id);

            return RedirectToAction("ManageBill", "HRMSPayroll");
        }
        #endregion

        #region ManageBonus
        [HttpGet]
        public async Task<ActionResult> ManageBonus()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VMBonus model = new VMBonus();
            await Task.Run(() => model.GetBonusList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateBonus()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //ViewBag.DesignationList = new SelectList(d.GetNightBillCreateDesignationList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateBonus(VMBonus model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = u.User_User.ID.ToString();
            model.Entry_Date = DateTime.Now;
            await Task.Run(() => model.GetCreateBonus(model));

            return RedirectToAction("ManageBonus", "HRMSPayroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateBonus(int Id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            await Task.Run(() => model.GetEmployeeGrossSalary(Id));

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateBonus(VM_HRMS_Payroll model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = u.User_User.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateSalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBonus", "HRMSPayroll");
        }

        [HttpGet]
        public async Task<ActionResult> ProcessBonus(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            ViewBag.UnitList = new SelectList(d.GetUnitList(), "Value", "Text");
            //ViewBag.PayrollList = new SelectList(d.GetOpenPayrollList(), "Value", "Text");
            //ViewBag.Fastival = new SelectList(d.GetFastivalList(), "Value", "Text");
            VMBonus model = new VMBonus();
            await Task.Run(() => model.GetBonusInfo(id));
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ProcessBonus(VMBonus model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = u.User_User.ID.ToString();
            model.Update_By = u.User_User.ID.ToString();
            model.Entry_Date = DateTime.Now;
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetProcessBonus(model));
            await Task.Run(() => model.Save());
            return RedirectToAction("BonusDetails", "HRMSPayroll", new { id = model.BonusId });

        }

        [HttpGet]
        public async Task<ActionResult> BonusDetails(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VMBonus model = new VMBonus();
            await Task.Run(() => model.GetEmployeeBonus(id));
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> CloseBonus(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VMBonus model = new VMBonus();
            await Task.Run(() => model.CloseBonus(id));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBonus", "HRMSPayroll");
        }

        #endregion

        #region SalaryIncrement
        [HttpGet]
        public ActionResult SalaryIncrement()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            VMSalaryIncrement model = new VMSalaryIncrement();
            model.DataList = new List<VMSalaryIncrement>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SalaryIncrement(VMSalaryIncrement model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            model.DataList = new List<VMSalaryIncrement>();
            await Task.Run(() => model.GetIncrementListByTitleWise(model.CardTitleId));
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateIncrementSalary(VMSalaryIncrement model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            model.Entry_By = u.User_User.ID.ToString();
            model.Entry_Date = DateTime.Now;
            await Task.Run(() => model.CreateYearlyIncrement(model));
            await Task.Run(() => model.Save());
            return RedirectToAction("SalaryIncrement");
        }

        [HttpGet]
        public ActionResult IncrementList()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            VMSalaryIncrement model = new VMSalaryIncrement();
            model.DataList = new List<VMSalaryIncrement>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> IncrementList(VMSalaryIncrement model)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            model.DataList = new List<VMSalaryIncrement>();
            await Task.Run(() => model.GetIncrementedListByTitleWise(model.CardTitleId));
            return View(model);
        }

        #endregion

        #region Report

        [HttpGet]
        public ActionResult GetSalarySheet(int id, bool AID)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.StatusList = new SelectList(d.GetEmployeePayrollStatus(), "Value", "Text");
            VM_HRMS_Payroll Payroll = new VM_HRMS_Payroll();
            Payroll.PayRollID = id;
            Payroll.AddCompliance = AID;
            return View(Payroll);
        }

        [HttpPost]
        public ActionResult GetSalarySheet(VM_HRMS_Payroll model)
        {
            DsPayrollReport ds = new DsPayrollReport();
            VMReportPayroll vmmodel = new VMReportPayroll();
            ReportClass cr = new ReportClass();
            if (model.AddCompliance)
            {
                if (model.PayrollStatusId == 1)
                {
                    ds = vmmodel.GetAuditSheetForResign(model.SectionID.Value, model.PayRollID);
                }
                else if (model.PayrollStatusId == 2)
                {
                    ds = vmmodel.GetAuditSalarySheetForLefty(model.SectionID.Value, model.PayRollID);
                }
                else
                {
                    ds = vmmodel.GetAuditSalarySheet(model.SectionID.Value, model.PayRollID);
                }
            }
            else
            {
                if (model.PayrollStatusId == 1)
                {
                    ds = vmmodel.GetSalarySheetForResign(model.SectionID.Value, model.PayRollID);
                }
                else if (model.PayrollStatusId == 2)
                {
                    ds = vmmodel.GetSalarySheetForLefty(model.SectionID.Value, model.PayRollID);
                }
                else
                {
                    ds = vmmodel.GetSalarySheet(model.SectionID.Value, model.PayRollID);
                }
            }


            cr.FileName = Server.MapPath("~/Views/Reports/CrpSalarySheet.rpt");
            cr.SummaryInfo.ReportTitle = "SalarySheet";
            cr.Load();
            cr.SetDataSource(ds);
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        #endregion
    }
}