﻿using Oss.Romo.Models.Services;
using Oss.Romo.Models.ViewModels.HRMS;
using Oss.Romo.Models.ViewModels.Promotion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.ViewModels.User;

namespace Oss.Romo.Controllers
{
    public class HRMSPromotionController : Controller
    {
        public DropDownData d;
        CurrentUser user;
        public HRMSPromotionController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            d = new DropDownData();
            sessionHandler.Adjust();
           // this.user = sessionHandler.CurrentUser;
        }

        // GET: Promotion
        [HttpGet]
        public ActionResult PromotionDetails()
        {
            if (user != null)
            {
                ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
                ViewBag.Department = new SelectList(d.GetDepartmentList(), "Value", "Text");
                ViewBag.Section = new SelectList(d.GetSectionList(), "Value", "Text");
                ViewBag.Designation = new SelectList(d.GetDesignationList(), "Value", "Text");
                ViewBag.Grade = new SelectList(d.GetGradeStaticList(), "Value", "Text");
                ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                ViewBag.Unit = new SelectList(d.GetUnitList(), "Value", "Text");
                ViewBag.BusinessUnit = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                VMPromotions model = new VMPromotions();
                model.VMEmployee = new VMEmployee();
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "HRMSHome");
            }
        }

        [HttpPost]
        public async Task<ActionResult> PromotionDetails(VMPromotions model)
        {
            ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
            ViewBag.Department = new SelectList(d.GetDepartmentList(), "Value", "Text");
            ViewBag.Section = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.Designation = new SelectList(d.GetDesignationList(), "Value", "Text");
            ViewBag.Grade = new SelectList(d.GetGradeStaticList(), "Value", "Text");
            ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
            ViewBag.Unit = new SelectList(d.GetUnitList(), "Value", "Text");
            ViewBag.BusinessUnit = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
            VMEmployee VMEmployees = new VMEmployee();

            VMPromotions vmPromotions = new VMPromotions();
            vmPromotions.VMEmployee = new VMEmployee();
            await Task.Run(() => vmPromotions.VMEmployee = VMEmployees.GetEmployeeDetails(model.EmployeeID));
            return View(vmPromotions);
        }

        [HttpPost]
        public async Task<ActionResult> CreatePromotion(VMPromotions model)
        {
            if (user != null)
            {
                ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
                ViewBag.Department = new SelectList(d.GetDepartmentList(), "Value", "Text");
                ViewBag.Section = new SelectList(d.GetSectionList(), "Value", "Text");
                ViewBag.Designation = new SelectList(d.GetDesignationList(), "Value", "Text");
                ViewBag.Grade = new SelectList(d.GetGradeStaticList(), "Value", "Text");
                ViewBag.Line = new SelectList(d.GetLineList(), "Value", "Text");
                ViewBag.Unit = new SelectList(d.GetUnitList(), "Value", "Text");
                ViewBag.BusinessUnit = new SelectList(d.GetBusinessUnitList(), "Value", "Text");
                VMPromotions vmPromotions = new VMPromotions();
                model.Entry_Date = DateTime.Today;
                model.Entry_By = user.ID.ToString();
                await Task.Run(() => vmPromotions.GetCreatePromotion(model));

                return RedirectToAction("PromotionList", "HRMSPromotion");
            }
            else
            {
                return RedirectToAction("Login", "HRMSHome");
            }

        }

        [HttpGet]
        public async Task<ActionResult> PromotionList()
        {
            VMPromotions vmPromotions = new VMPromotions();
            await Task.Run(() => vmPromotions.GetPromotionList());
            return View(vmPromotions);
        }
    }
}