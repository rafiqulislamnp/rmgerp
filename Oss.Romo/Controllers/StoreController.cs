﻿using CrystalDecisions.CrystalReports.Engine;
using Oss.Romo.ViewModels.Store;
using Oss.Romo.Models;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Store;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Production;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.ViewModels.User;
using Oss.Romo.Models.Production;
using Oss.Romo.ViewModels.Home;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Accounts;
using Oss.Romo.ViewModels.Account;

namespace Oss.Romo.Controllers
{

    [SoftwareAdmin]
    public class StoreController : Controller
    {
        public xOssContext db = new xOssContext();
        VmControllerHelper VmControllerHelper;
        VmUser_Department VmUser_Department;
        VmMkt_PO VmMkt_PO;
        VmForDropDown VmForDropDown;
        VmMktPO_Slave VmMktPO_Slave;
        VmRaw_Category VmRaw_Category;
        VmRaw_SubCategory VmRaw_SubCategory;
        VmRaw_Item VmRaw_Item;
        VmPOSlave_Receiving VmPOSlave_Receiving;
        VmMktPOSlave_Consumption VmMktPOSlave_Consumption;
        VmGeneral_PO VmGeneral_PO;
        VmGeneral_POSlave VmGeneral_POSlave;
        VmProd_Requisition VmProd_Requisition;
        VmProd_Requisition_Slave VmProd_Requisition_Slave;
        VmStoreInventory VmStoreInventory;
        VmInternalStore VmInternalStore;
        VmStore_InterTransfer VmStore_InterTransfer;
        VmStore_InternalTransferSlave VmStore_InternalTransferSlave;
        VmAcc_IntegrationJournal VmAcc_IntegrationJournal;
        VmMkt_POExtTransfer VmMkt_POExtTransfer;
        VmProd_MachineDIA VmProd_MachineDIA;
        private HelperController helperController;


        Int32 UserID = 0;
        public StoreController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        //.......Raw_Category.......//

        public async Task<ActionResult> PurchaseOrderReady()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.POReadyDataLoad());
            return View(VmMkt_PO);
        }

        public async Task<ActionResult> PurchaseOrderReceive(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper = new VmControllerHelper();

            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.PurchaseOrderAccessoriesView(0));

            return View(VmMkt_PO);
        }

        public async Task<ActionResult> PurchaseOrderFabricReceive(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.PurchaseOrderFabricView(0));
            //await Task.Run(() => VmMkt_PO.POReceiveDataLoad());
            return View(VmMkt_PO);


        }
        //-----------------Po stock report------------------------//    
        public async Task<ActionResult> PO_StockInRecevedReport(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.PoDetails(id));
            await Task.Run(() => VmMkt_PO.PoStockInRecevedReport(id));
            return View(VmMkt_PO);
        }

        public async Task<ActionResult> PO_StockInReturnReport(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.PoDetails(id));
            await Task.Run(() => VmMkt_PO.PoStockInReturnReport(id));
            return View(VmMkt_PO);
        }

        //--------------------------------------------------------//
        public async Task<ActionResult> PO_DeliveredReport(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            //await Task.Run(() => VmMkt_PO.PoDetails(id));
            await Task.Run(() => VmMkt_PO.PODeliveredReport(id));
            return View(VmMkt_PO);
        }
        public async Task<ActionResult> GeneralPurchaseOrderReceive(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper = new VmControllerHelper();

            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.GeneralPurchaseOrderView(0));
            //await Task.Run(() => VmMkt_PO.POReceiveDataLoad());
            return View(VmMkt_PO);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_POEditFromReceive(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)

            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRaw_StoreDropDown();
            SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Store = list;

            await Task.Run(() => VmMkt_PO.SelectSingle(id.Value));
            VmMkt_PO.Mkt_POSlave_Receiving = new Mkt_POSlave_Receiving();
            VmMkt_PO.Mkt_POSlave_Receiving.Date = DateTime.Today;


            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderStyleColor((int)VmMkt_PO.Mkt_PO.Mkt_BOMFK);
            SelectList Colorlist = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Colorlist = Colorlist;

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            Session["SweetAlert"] = null;
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_POEditFromReceive(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {

            VmMkt_PO.Challan = VmMkt_PO.Mkt_POSlave_Receiving.Challan;
            //if (ModelState.IsValid)
            //{
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            int receiveId = 0;
            await Task.Run(() => receiveId = VmMkt_PO.VmPOSlave_ReceivingCreate(UserID, DataList2));
            if (receiveId > 0)
            {
                Session["Success"] = "Yes";
                Session["Message"] = "Successfully received under Challan No: " + VmMkt_PO.Mkt_POSlave_Receiving.Challan;
            }
            else
            {
                Session["Success"] = "No";
            }
            // journal integration
            try
            {
                var v = (from t1 in db.Mkt_POSlave_Receiving
                         join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                         join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                         join t4 in db.Common_Supplier on t3.Common_SupplierFK equals t4.ID
                         where t1.Active == true && t3.ID == VmMkt_PO.Mkt_PO.ID && t1.IsReturn == false
                         group new { t1, t2, t4 } by new
                         {
                             t3.ID,
                             t4.Acc_AcNameFk,
                             t4.Name,
                             t3.CurrencyRate,
                             t3.Common_CurrencyFK,
                             t3.CID,
                         } into g
                         select new Vm_Mkt_POJournalIntegration
                         {
                             Id = g.Key.ID,
                             Acc_AcNameFk = g.Key.Acc_AcNameFk,
                             Name = g.Key.Name,
                             Cid = g.Key.CID,
                             CurrencyRate = g.Key.CurrencyRate,
                             CurrencyType = g.Key.Common_CurrencyFK,
                             Total = g.Sum(p => p.t1.Quantity * p.t2.Price),
                             TotalQty = g.Sum(p => p.t1.Quantity)
                         }).Single();

                this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                {
                    UserId = this.UserID,
                    Date = VmMkt_PO.Mkt_POSlave_Receiving.Date,
                    Title = "Product Receive",
                    CurrencyType = v.CurrencyType,
                    CurrencyRate = v.CurrencyRate,
                    FromAccNo = v.Acc_AcNameFk,
                    ToAccNo = 255,
                    Amount = (decimal)(v.Total ?? 0),
                    Descriptions = "Stock in to Store from " + v.Name + ", as per " + v.Cid + ", Receive Qty: " + v.TotalQty,
                    TableIdFk = VmAcc_Database_Table.TableName(VmMkt_PO.Mkt_PO.GetType().Name),
                    TableRowIdFk = v.Id,
                };
                var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
            }
            catch (Exception e)
            {
                //string message = e.Message;
            }

            return RedirectToAction("VmMkt_POEditFromReceive", new { id = VmMkt_PO.Mkt_PO.ID});
            //}
            //return View(VmMkt_PO);
        }

        #region RequsitionPOReceive

        public async Task<ActionResult> RequsitionPOReceive()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
          
                await Task.Run(() => VmMkt_PO.GetPurchaseOrderAccessories());
          
            return View(VmMkt_PO);
        }

        public async Task<ActionResult> PO_StockInRequsitionRecevedReport(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.PoRequsitionDetails(id));
            await Task.Run(() => VmMkt_PO.PoStockInRecevedReport(id));
            return View(VmMkt_PO);
        }

        public async Task<ActionResult> PO_StockInRequsitionReturnReport(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.PoRequsitionDetails(id));
            await Task.Run(() => VmMkt_PO.PoStockInRequsitionReturnReport(id));
            return View(VmMkt_PO);
        }
        #endregion

        [HttpGet]
        public async Task<ActionResult> VmMkt_POStoreReceive(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRaw_StoreDropDown();
            SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Store = list;


            await Task.Run(() => VmMkt_PO.SelectSingle(id.Value));
            VmMkt_PO.Mkt_POSlave_Receiving = new Mkt_POSlave_Receiving();
            VmMkt_PO.Mkt_POSlave_Receiving.Date = DateTime.Today;
            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_POStoreReceive(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            VmMkt_PO.Challan = VmMkt_PO.Mkt_POSlave_Receiving.Challan;
            //if (ModelState.IsValid)
            //{

            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;


            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmMkt_PO.VmPOSlave_ReceivingCreate(UserID, DataList2));

            // journal integration
            try
            {
                var v = (from t1 in db.Mkt_POSlave_Receiving
                         join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                         join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                         join t4 in db.Common_Supplier on t3.Common_SupplierFK equals t4.ID
                         where t1.Active == true && t3.ID == VmMkt_PO.Mkt_PO.ID && t1.IsReturn == false
                         group new { t1, t2, t4 } by new
                         {
                             t3.ID,
                             t4.Acc_AcNameFk,
                             t4.Name,
                             t3.CID,
                         } into g
                         select new Vm_Mkt_POJournalIntegration
                         {
                             Id = g.Key.ID,
                             Acc_AcNameFk = g.Key.Acc_AcNameFk,
                             Name = g.Key.Name,
                             Cid = g.Key.CID,
                             Total = g.Sum(p => p.t1.Quantity * p.t2.Price),
                             TotalQty = g.Sum(p => p.t1.Quantity)
                         }).Single();

                this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                {
                    UserId = this.UserID,
                    Date = VmMkt_PO.Mkt_POSlave_Receiving.Date,
                    Title = "Product Receive",
                    FromAccNo = v.Acc_AcNameFk,
                    ToAccNo = 255,
                    Amount = (decimal)(v.Total ?? 0),
                    Descriptions = "Stock in to Store from " + v.Name + ", as per " + v.Cid + ", Receive Qty: " + v.TotalQty,
                    TableIdFk = VmAcc_Database_Table.TableName(VmMkt_PO.Mkt_PO.GetType().Name),
                    TableRowIdFk = v.Id,
                };
                var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("PurchaseOrderReceive");
            //}
            //return View(VmMkt_PO);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_TransferStoreItem(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRaw_StoreDropDown();
            SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Store = list;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingleReceivedItem(id.Value));

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_TransferStoreItem(VmMkt_PO VmMkt_PO)
        {
            //if (ModelState.IsValid)
            //{
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmMkt_PO.VmPOSlave_ReceivingEdit(UserID));
            return RedirectToAction("PurchaseOrderReceive");
            //}
            //return View(VmMkt_PO);
        }
        
        [HttpGet]
        public async Task<ActionResult> VmMkt_GeneralPOEditFromReceive(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingle(id.Value));
            VmMkt_PO.Mkt_POSlave_Receiving = new Mkt_POSlave_Receiving();
            VmMkt_PO.Mkt_POSlave_Receiving.Date = DateTime.Today;
            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_GeneralPOEditFromReceive(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {

            //if (ModelState.IsValid)
            //{
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmMkt_PO.VmPOSlave_ReceivingCreate(UserID, DataList2));

            // journal integration
            try
            {
                var v = (from t1 in db.Mkt_POSlave_Receiving
                         join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                         join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                         join t4 in db.Common_Supplier on t3.Common_SupplierFK equals t4.ID
                         where t1.Active == true && t3.ID == VmMkt_PO.Mkt_PO.ID && t1.IsReturn == false
                         group new { t1, t2, t4 } by new
                         {
                             t3.ID,
                             t4.Acc_AcNameFk,
                             t4.Name,
                             t3.CID,
                         } into g
                         select new Vm_Mkt_POJournalIntegration
                         {
                             Id = g.Key.ID,
                             Acc_AcNameFk = g.Key.Acc_AcNameFk,
                             Name = g.Key.Name,
                             Cid = g.Key.CID,
                             Total = g.Sum(p => p.t1.Quantity * p.t2.Price),
                             TotalQty = g.Sum(p => p.t1.Quantity)
                         }).Single();

                this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                {
                    UserId = this.UserID,
                    Date = VmMkt_PO.Mkt_POSlave_Receiving.Date,
                    Title = "Product Receive",
                    FromAccNo = v.Acc_AcNameFk,
                    ToAccNo = 255,
                    Amount = (decimal)(v.Total ?? 0),
                    Descriptions = "Stock in to General Store from " + v.Name + ", as per " + v.Cid + ", Receive Qty: " + v.TotalQty,
                    TableIdFk = VmAcc_Database_Table.TableName(VmMkt_PO.Mkt_PO.GetType().Name),
                    TableRowIdFk = v.Id,
                };
                var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("GeneralPurchaseOrderReceive");
            //}
            //return View(VmMkt_PO);
        }
        //......................Return to Supplier..................//
        [HttpGet]
        public async Task<ActionResult> VmMkt_PO_ItemReturn(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRaw_StoreDropDown();
            SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Store = list;


            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingle1(id.Value));
            VmMkt_PO.Mkt_POSlave_Receiving = new Mkt_POSlave_Receiving();
            VmMkt_PO.Mkt_POSlave_Receiving.Date = DateTime.Today;
            VmMkt_PO.Mkt_POSlave_Receiving.Mkt_POExtTransferFK = 1;
            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_PO_ItemReturn(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {

            //if (ModelState.IsValid)
            //{
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmMkt_PO.VmPOSlave_Return(UserID, DataList2));

            // journal integration
            try
            {
                var v = (from t1 in db.Mkt_POSlave_Receiving
                         join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                         join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                         join t4 in db.Common_Supplier on t3.Common_SupplierFK equals t4.ID
                         where t1.Active == true && t3.ID == VmMkt_PO.Mkt_PO.ID && t1.IsReturn == true
                         group new { t1, t2, t4 } by new
                         {
                             t3.ID,
                             t4.Acc_AcNameFk,
                             t4.Name,
                             t3.CID,
                             t3.CurrencyRate,
                             t3.Common_CurrencyFK,
                         } into g
                         select new Vm_Mkt_POJournalIntegration
                         {
                             Id = g.Key.ID,
                             Acc_AcNameFk = g.Key.Acc_AcNameFk,
                             Name = g.Key.Name,
                             CurrencyRate = g.Key.CurrencyRate,
                             CurrencyType = g.Key.Common_CurrencyFK,
                             Cid = g.Key.CID,
                             Total = g.Sum(p => p.t1.Quantity * p.t2.Price),
                             TotalQty = g.Sum(p => p.t1.Quantity)
                         }).Single();

                this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                {
                    UserId = this.UserID,
                    Date = VmMkt_PO.Mkt_POSlave_Receiving.Date,
                    Title = "Product Return",
                    FromAccNo = 255,
                    CurrencyRate = v.CurrencyRate,
                    CurrencyType = v.CurrencyType,
                    ToAccNo = v.Acc_AcNameFk,
                    Amount = (decimal)(v.Total ?? 0),
                    Descriptions = "Stock return from Store to " + v.Name + ", as per " + v.Cid + ", Return Qty: " + v.TotalQty,
                    TableIdFk = VmAcc_Database_Table.TableName(VmMkt_PO.Mkt_PO.GetType().Name),
                    TableRowIdFk = v.Id,
                };
                var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("VmMkt_PO_ItemReturn", new { id = VmMkt_PO.Mkt_PO.ID});
            //}
            //return View(VmMkt_PO);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_PO_StoreReturn(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingle1(id.Value));
            VmMkt_PO.Mkt_POSlave_Receiving = new Mkt_POSlave_Receiving();
            VmMkt_PO.Mkt_POSlave_Receiving.Date = DateTime.Today;
            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_PO_StoreReturn(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {

            //if (ModelState.IsValid)
            //{
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmMkt_PO.VmPOSlave_Return(UserID, DataList2));

            // journal integration
            try
            {
                var v = (from t1 in db.Mkt_POSlave_Consumption
                         join t2 in db.Mkt_POSlave on t1.Mkt_POSlaveFK equals t2.ID
                         join t3 in db.Mkt_PO on t2.Mkt_POFK equals t3.ID
                         join t4 in db.Common_Supplier on t3.Common_SupplierFK equals t4.ID
                         where t1.Active == true && t3.ID == VmMkt_PO.Mkt_PO.ID && t1.IsReturn == true
                         group new { t1, t2, t4 } by new
                         {
                             t3.ID,
                             t4.Acc_AcNameFk,
                             t4.Name,
                             t3.CID,
                         } into g
                         select new Vm_Mkt_POJournalIntegration
                         {
                             Id = g.Key.ID,
                             Acc_AcNameFk = g.Key.Acc_AcNameFk,
                             Name = g.Key.Name,
                             Cid = g.Key.CID,
                             Total = g.Sum(p => p.t1.Quantity * p.t2.Price),
                             TotalQty = g.Sum(p => p.t1.Quantity)
                         }).Single();

                this.VmAcc_IntegrationJournal = new VmAcc_IntegrationJournal
                {
                    UserId = this.UserID,
                    Date = VmMkt_PO.Mkt_POSlave_Consumption.Date,
                    Title = "Product Return",
                    FromAccNo = 255,
                    ToAccNo = v.Acc_AcNameFk,
                    Amount = (decimal)(v.Total ?? 0),
                    Descriptions = "Stock return from Store to " + v.Name + ", as per " + v.Cid + ", Return Qty: " + v.TotalQty,
                    TableIdFk = VmAcc_Database_Table.TableName(VmMkt_PO.Mkt_PO.GetType().Name),
                    TableRowIdFk = v.Id,
                };
                var journalstatus = await Task.Run(() => this.VmAcc_IntegrationJournal.EditJounalFromIntegratedTable());
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("PurchaseOrderReceive");
            //}
            //return View(VmMkt_PO);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_GeneralPO_ItemReturn(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingle1(id.Value));
            VmMkt_PO.Mkt_POSlave_Receiving = new Mkt_POSlave_Receiving();
            VmMkt_PO.Mkt_POSlave_Receiving.Date = DateTime.Today;
            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_GeneralPO_ItemReturn(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {

            //if (ModelState.IsValid)
            //{
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmMkt_PO.VmPOSlave_Return(UserID, DataList2));
            return RedirectToAction("GeneralPurchaseOrderReceive");
            //}
            //return View(VmMkt_PO);
        }
        //......................////////////.........................///////
        public async Task<ActionResult> PurchaseOrderDeliver()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();

            VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.GetItemForConsmtion());
            return View(VmMkt_PO.VmMkt_BOM);
        }
        public async Task<ActionResult> OrderWiseInventory()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();
            VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmMkt_PO.User_User = vMUser.User_User;
            await Task.Run(() => VmMkt_PO.GetItemForConsmtion());
            return View(VmMkt_PO.VmMkt_BOM);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_POEditFromDeliver(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRaw_StoreDropDown();
            SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Store = list;


            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.InitialDataLoad_PoSlaveSend(id.Value));
            VmMkt_PO.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
            VmMkt_PO.Mkt_POSlave_Consumption.Date = DateTime.Today;
            VmMkt_PO.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
            VmMkt_PO.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = 1;
            //if (VmMkt_PO.Mkt_PO == null)
            //{
            //    return HttpNotFound();
            //}
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_POEditFromDeliver(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    string s = error.ErrorMessage;
                }
            }
            //if (ModelState.IsValid)
            //{
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmMkt_PO.VmPOSlave_DeliveryCreate(UserID, DataList2));
            return RedirectToAction("VmMkt_POEditFromDeliver", new { id = VmMkt_PO.Mkt_PO.Mkt_BOMFK});
            //}
            //return RedirectToAction("PurchaseOrderDeliver");
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_POStoreDeliver(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.InitialDataLoad_PoSlaveSend(id.Value));
            VmMkt_PO.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
            VmMkt_PO.Mkt_POSlave_Consumption.Date = DateTime.Today;
            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_POStoreDeliver(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    string s = error.ErrorMessage;
                }
            }
            //if (ModelState.IsValid)
            //{
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmMkt_PO.VmPOSlave_DeliveryCreate(UserID, DataList2));
            return RedirectToAction("PurchaseOrderDeliver");
            //}
            //return View(VmMkt_PO);
        }
        //--------------------------Purchase Order Accoding to Requisition------------------//
        public async Task<ActionResult> PurchaseOrderDeliverREQ()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Requisition = new VmProd_Requisition();
            await Task.Run(() => VmProd_Requisition.GetItemForConsmtionREQ());
            return View(VmProd_Requisition);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_POEditFromDeliverREQ(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.InitialDataLoad_PoSlaveSendREQ(id.Value));

            if (VmMkt_PO.Prod_Requisition == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_POEditFromDeliverREQ(VmMkt_PO VmMkt_PO, IEnumerable<VmProd_Requisition_Slave> DataList3)
        {
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    string s = error.ErrorMessage;
                }
            }
            //if (ModelState.IsValid)
            //{
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmMkt_PO.VmPOSlave_DeliveryCreateREQ(UserID, DataList3));
            return RedirectToAction("PurchaseOrderDeliverREQ");
            //}
            //return View(VmMkt_PO);
        }
        //------------------------Return From Production ----------------------------//
        [HttpGet]
        public async Task<ActionResult> VmMkt_ReturnFrom_Production(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRaw_StoreDropDown();
            SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Store = list;


            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.InitialDataLoad_ReturnFromProduction(id.Value));
            VmMkt_PO.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
            VmMkt_PO.Mkt_POSlave_Consumption.Date = DateTime.Today;
            VmMkt_PO.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
            VmMkt_PO.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = 1;
            //if (VmMkt_PO.Mkt_PO == null)
            //{
            //    return HttpNotFound();
            //}
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_ReturnFrom_Production(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    string s = error.ErrorMessage;
                }
            }
            //if (ModelState.IsValid)
            //{
            await Task.Run(() => VmMkt_PO.VmPOSlave_DeliveryReturn(UserID, DataList2));
            return RedirectToAction("VmMkt_ReturnFrom_Production", new { id = VmMkt_PO.Mkt_PO.Mkt_BOMFK });
            //}
            //return View(VmMkt_PO);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_ReturnProductionToStore(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.InitialDataLoad_ReturnFromProduction(id.Value));
            VmMkt_PO.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
            VmMkt_PO.Mkt_POSlave_Consumption.Date = DateTime.Today;
            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_ReturnProductionToStore(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    string s = error.ErrorMessage;
                }
            }
            //if (ModelState.IsValid)
            //{
            await Task.Run(() => VmMkt_PO.VmPOSlave_DeliveryReturn(UserID, DataList2));
            return RedirectToAction("PurchaseOrderDeliver");
            //}
            //return View(VmMkt_PO);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_ReturnFrom_ProductionREQ(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.InitialDataLoad_PoSlaveReturnREQ(id.Value));

            if (VmMkt_PO.Prod_Requisition == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_ReturnFrom_ProductionREQ(VmMkt_PO VmMkt_PO, IEnumerable<VmProd_Requisition_Slave> DataList3)
        {
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    string s = error.ErrorMessage;
                }
            }
            //if (ModelState.IsValid)
            //{
            await Task.Run(() => VmMkt_PO.VmRequisition_ReturnCreateREQ(UserID, DataList3));
            return RedirectToAction("PurchaseOrderDeliverREQ");
            //}
            //return View(VmMkt_PO);
        }

        public ActionResult VmMkt_PODelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Mkt_PO mkt_PO = new Mkt_PO();
            mkt_PO.ID = id;
            mkt_PO.Delete(0);
            return RedirectToAction("PurchaseOrders", "Marketing");
        }

        public async Task<ActionResult> VmRaw_CategoryIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmRaw_Category = new VmRaw_Category();
            await Task.Run(() => VmRaw_Category.InitialDataLoad());
            return View(VmRaw_Category);
        }
        public ActionResult VmRaw_CategoryCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmRaw_CategoryCreate(VmRaw_Category x)
        {
            if (ModelState.IsValid)
            {
                x.Raw_Category.IsNew = true;
                await Task.Run(() => x.Add(UserID));
                return RedirectToAction("VmRaw_CategoryIndex");
            }

            return RedirectToAction("VmRaw_CategoryIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmRaw_CategoryEdit(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmRaw_Category = new VmRaw_Category();
            await Task.Run(() => VmRaw_Category.SelectSingle(id));

            if (VmRaw_Category.Raw_Category == null)
            {
                return HttpNotFound();
            }
            return View(VmRaw_Category);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmRaw_CategoryEdit(Raw_Category raw_Category)
        {
            if (ModelState.IsValid)
            {
                raw_Category.IsNew = true;
                await Task.Run(() => raw_Category.Edit(0));
                return RedirectToAction("VmRaw_CategoryIndex");
            }
            return View(VmRaw_Category);
        }
        public ActionResult VmRaw_CategoryDelete(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int rawCid = 0;

            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out rawCid);
            Raw_Category b = new Raw_Category();
            b.ID = rawCid;
            b.Delete(0);
            return RedirectToAction("VmRaw_CategoryIndex");
        }






        //---------Raw_SubCategory----------//


        public async Task<ActionResult> VmRaw_SubCategoryIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmRaw_SubCategory = new VmRaw_SubCategory();
            await Task.Run(() => VmRaw_SubCategory.InitialDataLoad());
            return View(VmRaw_SubCategory);
        }

        public ActionResult VmRaw_SubCategoryCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();

            SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = list;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmRaw_SubCategoryCreate(VmRaw_SubCategory x)
        {
            if (ModelState.IsValid)
            {
                x.Raw_SubCategory.IsNew = true;
                await Task.Run(() => x.Add(UserID));
                return RedirectToAction("VmRaw_SubCategoryIndex");
            }

            return RedirectToAction("VmRaw_SubCategoryIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmRaw_SubCategoryEdit(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmRaw_SubCategory = new VmRaw_SubCategory();
            await Task.Run(() => VmRaw_SubCategory.SelectSingle(id));

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();
            SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = list;


            if (VmRaw_SubCategory.Raw_SubCategory == null)
            {
                return HttpNotFound();
            }
            return View(VmRaw_SubCategory);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmRaw_SubCategoryEdit(Raw_SubCategory raw_SubCategory)
        {
            if (ModelState.IsValid)
            {
                raw_SubCategory.IsNew = true;
                await Task.Run(() => raw_SubCategory.Edit(0));
                return RedirectToAction("VmRaw_SubCategoryIndex");
            }
            return RedirectToAction("VmRaw_SubCategoryIndex");
        }
        public ActionResult VmRaw_SubCategoryDelete(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int rSCid = 0;

            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out rSCid);
            Raw_SubCategory b = new Raw_SubCategory();
            b.ID = rSCid;
            b.Delete(0);
            return RedirectToAction("VmRaw_SubCategoryIndex");
        }


        //............Raw-Item...........//
        public async Task<ActionResult> VmRaw_ItemIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmRaw_Item = new VmRaw_Item();
            await Task.Run(() => VmRaw_Item.InitialDataLoad());
            return View(VmRaw_Item);
        }
        public List<object> GetTempRawItem()
        {
            var rawItemList = new List<object>();

            foreach (var raw_Item in db.Raw_Item.Where(x => x.Active == true))
            {
                rawItemList.Add(new { Text = raw_Item.Name, Value = raw_Item.ID });
            }
            return rawItemList;
        }

        //[HttpGet]
        public JsonResult GetRaw_SubCategory(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_SubCategory.Where(p => p.Raw_CategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        public JsonResult GetRaw_Item(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_Item.Where(p => p.Raw_SubCategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRaw_ItemChange(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_Item.Where(p => p.Raw_SubCategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        public ActionResult VmRaw_ItemCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();


            VmForDropDown.DDownData = VmForDropDown.GetAllRaw_CategoryForDropDown();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_SubCategoryForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl2;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmRaw_ItemCreate(VmRaw_Item vmRaw_Item)
        {
            //foreach (ModelState modelState in ViewData.ModelState.Values)
            //{
            //    foreach (ModelError error in modelState.Errors)
            //    {
            //        string s = error.ErrorMessage;
            //        if (s != null)
            //        {
            //            string ss = s;
            //        }
            //    }
            //}
            vmRaw_Item.Raw_Item.IsNew = true;
            if (ModelState.IsValid)

            {

                await Task.Run(() => vmRaw_Item.Add(UserID));
                return RedirectToAction("VmRaw_ItemIndex");
            }

            return RedirectToAction("VmRaw_ItemIndex");
        }

        public List<object> GetTempRaw_SubCategory()
        {
            var raw_SubCategoryList = new List<object>();

            foreach (var raw_subCategory in db.Raw_SubCategory.Where(x => x.Active == true))
            {
                raw_SubCategoryList.Add(new { Text = raw_subCategory.Name, Value = raw_subCategory.ID });
            }
            return raw_SubCategoryList;
        }
        public List<object> GetTempRaw_Category()
        {
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(x => x.Active == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }
        int? sabCatID;
        int? catID;
        public List<object> GetTempRawSubCategoryForEdit(int? id)
        {
            var scl = new List<object>();

            sabCatID = (from t3 in db.Raw_Item where t3.ID == id select t3.Raw_SubCategoryFK).FirstOrDefault();
            catID = (
                    from t2 in db.Raw_SubCategory
                    where t2.ID == sabCatID
                    select t2.Raw_CategoryFK

                    ).FirstOrDefault();

            var x = (from t1 in db.Raw_SubCategory

                     where t1.Raw_CategoryFK == catID
                     select new
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }
                 ).ToList();
            foreach (var sc in x)
            {
                scl.Add(new { Text = sc.Name, Value = sc.ID });
            }
            return scl;
        }
        public List<object> GetTempRaw_CategoryForEdit()
        {
            var raw_CategoryList = new List<object>();

            foreach (var raw_Category in db.Raw_Category.Where(s => s.Active == true))
            {
                raw_CategoryList.Add(new { Text = raw_Category.Name, Value = raw_Category.ID });
            }
            return raw_CategoryList;
        }
        [HttpGet]

        public async Task<ActionResult> VmRaw_ItemEdit(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetTempRaw_CategoryForEdit();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            VmForDropDown.DDownData = VmForDropDown.GetRawSubCategoryDropDownForEdit(id);
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl8;

            VmRaw_Item = new VmRaw_Item();
            await Task.Run(() => VmRaw_Item.SelectSingle(id));

            if (VmRaw_Item.Raw_Item == null)
            {
                return HttpNotFound();
            }
            return View(VmRaw_Item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmRaw_ItemEdit(VmRaw_Item VmRaw_Item)
        {
            if (ModelState.IsValid)
            {
                VmRaw_Item.Raw_Item.IsNew = true;
                await Task.Run(() => VmRaw_Item.Edit(UserID));
                return RedirectToAction("VmRaw_ItemIndex");
            }
            return View(VmRaw_Item);
        }
        public ActionResult VmRaw_ItemDelete(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int iId = 0;

            //id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iId);
            Raw_Item b = new Raw_Item();
            b.ID = iId;
            b.Delete(0);
            return RedirectToAction("VmRaw_ItemIndex");
        }


        //.................StoreInventory....................//

        public async Task<ActionResult> StoreInventory()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStoreInventory = new VmStoreInventory();

            //var data = VmStoreInventory.CategoryWiseStock();
            await Task.Run(() => VmStoreInventory.InitialDataLoad());

            return View(VmStoreInventory);
        }

        //...............Raw_ItemDetails.................//
        public async Task<ActionResult> Raw_ItemDetails(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStoreInventory = new VmStoreInventory();
            await Task.Run(() => VmStoreInventory = VmStoreInventory.SelectJoinedItem(id));

            await Task.Run(() => VmStoreInventory.SelectSingleItem(id));
            return View(VmStoreInventory);
        }
        //..............Raw_SubCategoryDetails..............//
        public async Task<ActionResult> Raw_SubCategoryDetails(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStoreInventory = new VmStoreInventory();
            await Task.Run(() => VmStoreInventory = VmStoreInventory.SelectJoinedRaw_SubCategory(id));
            await Task.Run(() => VmStoreInventory.SelectSingleSubCategory(id));
            return View(VmStoreInventory);
        }

        //..............Raw_CategoryDetails..............//
        public async Task<ActionResult> CategoryDetailsIndex(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStoreInventory = new VmStoreInventory();
            await Task.Run(() => VmStoreInventory.SelectSingleCategory(id));
            return View(VmStoreInventory);

        }

        //Dashboard of store
        public ActionResult Dashboard()
        {
            return View();
        }



        //--------------------------Mkt_POSlave_Receiving----------------------------//
        public async Task<ActionResult> VmPOSlave_ReceivingChallanIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPOSlave_Receiving = new VmPOSlave_Receiving();
            await Task.Run(() => VmPOSlave_Receiving.InitialDataLoad_PoSlaveReceiving());
            return View(VmPOSlave_Receiving);
        }
        public async Task<ActionResult> VmPOSlave_ReturnChallanIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPOSlave_Receiving = new VmPOSlave_Receiving();
            await Task.Run(() => VmPOSlave_Receiving.InitialDataLoad_PoSlaveReturn());
            return View(VmPOSlave_Receiving);
        }

        public async Task<ActionResult> VmPOSlave_ReceivingChallanDeteils(string challan)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPOSlave_Receiving = new VmPOSlave_Receiving();
            await Task.Run(() => VmPOSlave_Receiving = VmPOSlave_Receiving.SelectSingleJoined(challan));
            Mkt_POSlave_Receiving a = new Mkt_POSlave_Receiving();
            a.Challan = challan;
            VmPOSlave_Receiving.Mkt_POSlave_Receiving = new Mkt_POSlave_Receiving();
            await Task.Run(() => VmPOSlave_Receiving.GetReceivedChallanItem(challan));
            return View(VmPOSlave_Receiving);
        }
        public async Task<ActionResult> VmPOSlave_ReturnChallanDeteils(string challan)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPOSlave_Receiving = new VmPOSlave_Receiving();
            await Task.Run(() => VmPOSlave_Receiving = VmPOSlave_Receiving.SelectSingleJoined(challan));
            Mkt_POSlave_Receiving a = new Mkt_POSlave_Receiving();
            a.Challan = challan;
            VmPOSlave_Receiving.Mkt_POSlave_Receiving = new Mkt_POSlave_Receiving();
            await Task.Run(() => VmPOSlave_Receiving.GetReturnChallanItem(challan));
            return View(VmPOSlave_Receiving);
        }
        //--------------------------Mkt_POSlave_Consumption----------------------------//
        public async Task<ActionResult> VmMktPOSlave_ConsumptionIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMktPOSlave_Consumption = new VmMktPOSlave_Consumption();
            await Task.Run(() => VmMktPOSlave_Consumption.InitialDataLoadDelivery());
            return View(VmMktPOSlave_Consumption);
        }
        public async Task<ActionResult> VmMktPOSlave_ConsumptionReturnIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMktPOSlave_Consumption = new VmMktPOSlave_Consumption();
            await Task.Run(() => VmMktPOSlave_Consumption.InitialDataLoadDeliveryReturn());
            return View(VmMktPOSlave_Consumption);
        }
        public async Task<ActionResult> VmPOSlave_DeliveryConsumptionDeteils(string requisition)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMktPOSlave_Consumption = new VmMktPOSlave_Consumption();
            await Task.Run(() => VmMktPOSlave_Consumption = VmMktPOSlave_Consumption.SelectSingleJoined(requisition));
            Mkt_POSlave_Consumption a = new Mkt_POSlave_Consumption();
            a.Requisition = requisition;
            VmMktPOSlave_Consumption.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
            await Task.Run(() => VmMktPOSlave_Consumption.GetDeliveryItem(requisition));
            return View(VmMktPOSlave_Consumption);
        }
        public async Task<ActionResult> VmPOSlave_DeliveryReturnDeteils(string requisition)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMktPOSlave_Consumption = new VmMktPOSlave_Consumption();
            await Task.Run(() => VmMktPOSlave_Consumption = VmMktPOSlave_Consumption.SelectSingleJoined(requisition));
            Mkt_POSlave_Consumption a = new Mkt_POSlave_Consumption();
            a.Requisition = requisition;
            VmMktPOSlave_Consumption.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
            await Task.Run(() => VmMktPOSlave_Consumption.GetDeliveryReturnItem(requisition));
            return View(VmMktPOSlave_Consumption);
        }
        [HttpGet]
        public async Task<ActionResult> VmMktPOSlave_ConsumptionEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMktPOSlave_Consumption = new VmMktPOSlave_Consumption();
            await Task.Run(() => VmMktPOSlave_Consumption.SelectSingle(id.Value));

            if (VmMktPOSlave_Consumption.Mkt_POSlave_Consumption == null)
            {
                return HttpNotFound();
            }
            return View(VmMktPOSlave_Consumption);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMktPOSlave_ConsumptionEdit(Mkt_POSlave_Consumption POSlave_Consumption, IEnumerable<VmMktPO_Slave> DataList2)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmMktPOSlave_Consumption.VmPOSlave_DeliveryCreate(UserID, DataList2));
                return RedirectToAction("PurchaseOrderDeliver");
            }
            return RedirectToAction("PurchaseOrderDeliver");
        }

        //..----------------------POCrystallReport--------------------------------..//
        public ActionResult VmMkt_POReport(int? id)
        {
            try
            {
                VmMkt_PO x = new VmMkt_PO();
                Acc_Type xx = new Acc_Type();
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Reports/StoreReport/PO.rpt");
                cr.Load();
                cr.SetDataSource(xx.GetAcc_Type());
                Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return null;


        }

        //------------------------------------------General Store----------------------------------------//

        public async Task<ActionResult> VmGeneral_POIndex()//-------General_PO------//
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmGeneral_PO = new VmGeneral_PO();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmGeneral_PO.User_User = vMUser.User_User;
           
            await Task.Run(() => VmGeneral_PO.InitialDataLoad());
            return View(VmGeneral_PO);
        }

        public async Task<ActionResult> VmGeneral_StockOut()//-------General_Stock Out------//
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmGeneral_PO = new VmGeneral_PO();
            await Task.Run(() => VmGeneral_PO.InitialDataLoadGeneral());
            return View(VmGeneral_PO);
        }
        public ActionResult VmGeneral_POCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl2;


            VmForDropDown.DDownData = VmForDropDown.GetDropDownBOMCID();
            ViewBag.BomCID = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetStatusDropDown();
            SelectList Status = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Status = Status;

            VmGeneral_PO = new VmGeneral_PO();
            VmGeneral_PO.Mkt_PO = new Mkt_PO();
            VmGeneral_PO.Mkt_PO.Date = DateTime.Today;
            VmGeneral_PO.DepartmentID = user.User_User.User_DepartmentFK.Value;
            return View(VmGeneral_PO);
        }


        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmGeneral_POCreate(VmGeneral_PO VmGeneral_PO)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmGeneral_PO.Mkt_PO.FirstCreatedBy = user.User_User.ID;
           
            if (VmGeneral_PO.Mkt_PO.Mkt_BOMFK == null)
            {
                VmGeneral_PO.Mkt_PO.Mkt_BOMFK = 1;
            }
            if (ModelState.IsValid)
            {
                try
                {
                    await Task.Run(() => VmGeneral_PO.Add(UserID));
                }
                catch (Exception ex)
                {
                    
                    //throw;
                }
               
                return RedirectToAction("VmGeneral_POIndex");
            }
            VmMkt_PO = new VmMkt_PO();

            return RedirectToAction("VmGeneral_POIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmGeneral_POEdit(int? id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();

            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;

            VmForDropDown.DDownData = VmForDropDown.GetDropDownBOMCID();
            ViewBag.BomCID = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl2;
            VmGeneral_PO = new VmGeneral_PO();
            await Task.Run(() => VmGeneral_PO.SelectSingle(id.Value));
            VmGeneral_PO.DepartmentID = user.User_User.User_DepartmentFK.Value;

            if (VmGeneral_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmGeneral_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmGeneral_POEdit(Mkt_PO Mkt_PO)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                Mkt_PO.LastEditeddBy = user.User_User.ID;

                await Task.Run(() => Mkt_PO.Edit(0));
                return RedirectToAction("vmgeneral_poindex", "Store");
            }
            return RedirectToAction("vmgeneral_poindex", "Store");
        }
        public ActionResult VmGeneral_PODelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Mkt_PO b = new Mkt_PO();
            b.ID = id;
            b.Delete(0);
            VmMktPO_Slave = new VmMktPO_Slave();
            VmMktPO_Slave.DaleteBulk(UserID, id);
            return RedirectToAction("vmgeneral_poindex", "Store");
        }
        public async Task<ActionResult> VmGeneral_POAuthorize(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();

            VmGeneral_PO = new VmGeneral_PO();
            VmGeneral_PO.Mkt_PO = new Mkt_PO();
            await Task.Run(() => VmGeneral_PO.SelectSingle(id.Value));
            if (VmGeneral_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }

            VmGeneral_PO.Mkt_PO.AuthorizeDate = DateTime.Now;
            return View(VmGeneral_PO);
        }

        [HttpPost]
        public async Task<ActionResult> VmGeneral_POAuthorize(VmGeneral_PO VmGeneral_PO)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            VmGeneral_PO.Mkt_PO.LastEditeddBy = user.User_User.ID;
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmGeneral_PO.Edit(0));
                return RedirectToAction("vmgeneral_poindex", "Store");
            }
            return View(VmGeneral_PO);
        }

        public async Task<ActionResult> VmGeneral_PurchaseInvoice(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmMkt_PO = new VmMkt_PO();                
                await Task.Run(() => VmMkt_PO = VmMkt_PO.SelectSingleJoinedGEN(id.Value));
                Mkt_POSlave a = new Mkt_POSlave();
                a.Mkt_POFK = id;
                VmMkt_PO.VmMktPO_Slave = new VmMktPO_Slave();
                await Task.Run(() => VmMkt_PO.VmMktPO_Slave.GetPOSlaveGEN(id.Value));
                VmMkt_PO.Total_Value = VmMkt_PO.TotlaValue(id.Value);
            }
            VmForDropDown dropDown = new VmForDropDown();
            if (VmMkt_PO.Total_Value != null)
            {
                if (VmMkt_PO.Common_Currency.Code == "USD")
                {
                    ViewBag.TotalInWord = dropDown.NumberToWords(VmMkt_PO.Total_Value.Value, CurrencyType.USD) + ".";
                }
                else if (VmMkt_PO.Common_Currency.Code == "BDT")
                {
                    ViewBag.TotalInWord = "BDT. " + InWordFunction.ConvertToWords(VmMkt_PO.Total_Value.Value.ToString("0.##"));
                }
            }
            if (VmMkt_PO.Mkt_PO.AlsoATransfer)
            {
                VmMkt_PO.VmMkt_POExtTransfer = new VmMkt_POExtTransfer();
                await Task.Run(() => VmMkt_PO.VmMkt_POExtTransfer.InitialDataLoad_POExtTransfer(id.Value));
            }          
            return View(VmMkt_PO);

        }

        [HttpGet]
        public async Task<ActionResult> VmMkt_GeneralStockDeliver(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.InitialDataLoad_GeneralPoSlaveSend(id.Value));

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_GeneralStockDeliver(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        {
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    string s = error.ErrorMessage;
                }
            }
            //if (ModelState.IsValid)
            //{
            await Task.Run(() => VmMkt_PO.VmGeneralPOSlave_DeliveryCreate(UserID, DataList2));
            return RedirectToAction("VmGeneral_StockOut");
            //}
            //return View(VmMkt_PO);
        }
        //---------------------------General_PO Slave
        public async Task<ActionResult> VmGeneral_POSlaveDetails(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmMkt_PO = new VmMkt_PO();               
                await Task.Run(() => VmMkt_PO = VmMkt_PO.SelectSingleJoinedGEN(id.Value));
                Mkt_POSlave a = new Mkt_POSlave();
                a.Mkt_POFK = id;
                VmMkt_PO.VmMktPO_Slave = new VmMktPO_Slave();
                await Task.Run(() => VmMkt_PO.VmMktPO_Slave.GetPOSlaveByPO(id.Value));
                VmMkt_PO.VmMkt_POExtTransfer = new VmMkt_POExtTransfer();
                if (VmMkt_PO.Mkt_PO.AlsoATransfer)
                {
                    await Task.Run(() => VmMkt_PO.VmMkt_POExtTransfer.InitialDataLoad_POExtTransfer(id.Value));
                }              
                VmMkt_PO.Total_Value = VmMkt_PO.TotlaValue(id.Value);
                VmMkt_PO.VmMkt_POExtTransfer.ReturnController = "store/VmGeneral_POSlaveDetails/" + id.Value;
            }
            return View(VmMkt_PO);
        }

        #region POAlsoTransferItemCreate

        [HttpGet]
        public ActionResult POAlsoTransferItemCreate(int? id, string returnurl)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetTransferFilterType();
            ViewBag.TransferFilterList = new SelectList(VmForDropDown.DDownData, "Value", "Text");            

            VmForDropDown.DDownData = VmForDropDown.GetBropDownBOMCID();
            ViewBag.BOMList = new SelectList(VmForDropDown.DDownData, "Value", "Text");             

            VmForDropDown.DDownData = VmForDropDown.GetDropDownRawItem();
            ViewBag.RawItemList = new SelectList(VmForDropDown.DDownData, "Value", "Text");             

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            ViewBag.UnitList = new SelectList(VmForDropDown.DDownData, "Value", "Text");             

            VmForDropDown.DDownData = VmForDropDown.GetDropDownTransitionItemInventory();
            ViewBag.TransitionItem = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetProdMachineDIA();
            ViewBag.MachineDIA = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmMkt_POExtTransfer = new VmMkt_POExtTransfer();
            VmMkt_POExtTransfer.Mkt_POExtTransfer = new Mkt_POExtTransfer();
            VmMkt_POExtTransfer.Mkt_POExtTransfer.Mkt_POFK = id;
            VmMkt_POExtTransfer.ReturnController = returnurl;
            return View(VmMkt_POExtTransfer);
        }

        [HttpPost]
        public async Task<ActionResult> POAlsoTransferItemCreate(VmMkt_POExtTransfer a)
        {
            int inserted = 0;
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            if (a.TransferType == 1)
            {
                a.Mkt_POExtTransfer.Mkt_POSlaveFK = 1;
                a.Mkt_POExtTransfer.Prod_TransitionItemInventoryFK = 1;
            }
            else if (a.TransferType == 2)
            {
                a.Mkt_POExtTransfer.Raw_ItemFK = 1;
                a.Mkt_POExtTransfer.Prod_TransitionItemInventoryFK = 1;
            }
            else if (a.TransferType == 3)
            {
                a.Mkt_POExtTransfer.Raw_ItemFK = 1;
                a.Mkt_POExtTransfer.Mkt_POSlaveFK = 1;
            }

            await Task.Run(() => inserted = a.Add(UserID));


            if (inserted > 0)
            {
                int conInserted = 0;
                if (a.Mkt_POExtTransfer.Mkt_POSlaveFK != 1)
                {
                    VmMktPOSlave_Consumption vmConsumption = new VmMktPOSlave_Consumption();
                    vmConsumption.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
                    vmConsumption.Mkt_POSlave_Consumption.Raw_InternaleTransferSlaveFk = 1;
                    vmConsumption.Mkt_POSlave_Consumption.Date = DateTime.Now;
                    vmConsumption.Mkt_POSlave_Consumption.Description = "";
                    vmConsumption.Mkt_POSlave_Consumption.FirstCreatedBy = UserID;
                    vmConsumption.Mkt_POSlave_Consumption.IsReturn = false;
                    vmConsumption.Mkt_POSlave_Consumption.LastEditeddBy = 0;
                    vmConsumption.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = inserted;
                    vmConsumption.Mkt_POSlave_Consumption.Mkt_POSlaveFK = a.Mkt_POExtTransfer.Mkt_POSlaveFK;
                    vmConsumption.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = 1;
                    vmConsumption.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = 1;
                    vmConsumption.Mkt_POSlave_Consumption.Quantity = a.Mkt_POExtTransfer.TotalRequired;
                    vmConsumption.Mkt_POSlave_Consumption.Requisition = "Requisition";
                    vmConsumption.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                    vmConsumption.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                    vmConsumption.Mkt_POSlave_Consumption.FirstCreated = DateTime.Now;
                    vmConsumption.Mkt_POSlave_Consumption.LastEdited = null;
                    vmConsumption.Mkt_POSlave_Consumption.Active = true;

                    db.Mkt_POSlave_Consumption.Add(vmConsumption.Mkt_POSlave_Consumption);
                    db.SaveChanges();
                    //conInserted =vmConsumption.Add(0);
                    //int sss = 00;
                }
                else if (a.Mkt_POExtTransfer.Prod_TransitionItemInventoryFK != 1)
                {
                    VmMktPOSlave_Consumption vmConsumption = new VmMktPOSlave_Consumption();
                    vmConsumption.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
                    vmConsumption.Mkt_POSlave_Consumption.Raw_InternaleTransferSlaveFk = 1;
                    vmConsumption.Mkt_POSlave_Consumption.Date = DateTime.Now;
                    vmConsumption.Mkt_POSlave_Consumption.Description = "";
                    vmConsumption.Mkt_POSlave_Consumption.FirstCreatedBy = UserID;
                    vmConsumption.Mkt_POSlave_Consumption.IsReturn = false;
                    vmConsumption.Mkt_POSlave_Consumption.LastEditeddBy = 0;
                    vmConsumption.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = inserted;
                    vmConsumption.Mkt_POSlave_Consumption.Mkt_POSlaveFK = 1;
                    vmConsumption.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = 1;
                    vmConsumption.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = a.Mkt_POExtTransfer.Prod_TransitionItemInventoryFK;
                    vmConsumption.Mkt_POSlave_Consumption.Quantity = a.Mkt_POExtTransfer.TotalRequired;
                    vmConsumption.Mkt_POSlave_Consumption.Requisition = "Requisition";
                    vmConsumption.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                    vmConsumption.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                    vmConsumption.Mkt_POSlave_Consumption.FirstCreated = DateTime.Now;
                    vmConsumption.Mkt_POSlave_Consumption.LastEdited = null;
                    vmConsumption.Mkt_POSlave_Consumption.Active = true;
                    db.Mkt_POSlave_Consumption.Add(vmConsumption.Mkt_POSlave_Consumption);
                    db.SaveChanges();
                    //conInserted = vmConsumption.Add(0);

                }
            }
            return Redirect("../../"+a.ReturnController);
        }
        [HttpGet]
        public ActionResult POAlsoTransferItemEdit(int? id, string returnurl)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetTransferFilterType();
            SelectList s5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.TransferFilterList = s5;

            VmForDropDown.DDownData = VmForDropDown.GetBropDownBOMCID();
            SelectList sl = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOMList = sl;

            VmForDropDown.DDownData = VmForDropDown.GetDropDownRawItem();
            SelectList s2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItemList = s2;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList s3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UnitList = s3;

            VmForDropDown.DDownData = VmForDropDown.GetDropDownTransitionItemInventory();
            SelectList s4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.TransitionItem = s4;

            VmMkt_POExtTransfer = new VmMkt_POExtTransfer();
            VmMkt_POExtTransfer.SelectSingle(id.Value);
            VmMkt_POExtTransfer.ReturnController = returnurl;

            return View(VmMkt_POExtTransfer);
        }

        [HttpPost]
        public async Task<ActionResult> POAlsoTransferItemEdit(VmMkt_POExtTransfer a)
        {
            bool updateded = false;
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            //if (a.TransferType == 1)
            //{
            //    a.Mkt_POExtTransfer.Mkt_BOMSlaveFK = 1;
            //    a.Mkt_POExtTransfer.Prod_TransitionItemInventoryFK = 1;
            //}
            //else if (a.TransferType == 2)
            //{
            //    a.Mkt_POExtTransfer.Raw_ItemFK = 1;
            //    a.Mkt_POExtTransfer.Prod_TransitionItemInventoryFK = 1;
            //}
            //else if (a.TransferType == 3)
            //{
            //    a.Mkt_POExtTransfer.Raw_ItemFK = 1;
            //    a.Mkt_POExtTransfer.Mkt_BOMSlaveFK = 1;
            //}

            await Task.Run(() => a.Edit(UserID));


            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
            VmStore_InternalTransferSlave.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
            await Task.Run(() => VmStore_InternalTransferSlave.UpdateConsumptionExt(a.Mkt_POExtTransfer.ID));
            if (a.Mkt_POExtTransfer.Mkt_POSlaveFK != 1)
            {
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Date = DateTime.Now;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Description = "";
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreatedBy = VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreatedBy;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.IsReturn = false;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.LastEditeddBy = UserID;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Raw_InternaleTransferSlaveFk = 1;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = a.Mkt_POExtTransfer.ID;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Mkt_POSlaveFK = a.Mkt_POExtTransfer.Mkt_POSlaveFK;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = 1;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = 1;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Quantity = a.Mkt_POExtTransfer.TotalRequired;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Requisition = "Requisition";
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreated = a.Mkt_POExtTransfer.FirstCreated;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.LastEdited = DateTime.Now;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Active = true;
                db.Entry(VmStore_InternalTransferSlave.Mkt_POSlave_Consumption).State = EntityState.Modified;
                db.SaveChanges();
            }
            else if (a.Mkt_POExtTransfer.Prod_TransitionItemInventoryFK != 1)
            {
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Date = DateTime.Now;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Description = "";
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreatedBy = VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreatedBy;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.IsReturn = false;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.LastEditeddBy = UserID;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Raw_InternaleTransferSlaveFk = 1;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = a.Mkt_POExtTransfer.ID; ;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Mkt_POSlaveFK = 1;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = 1;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = a.Mkt_POExtTransfer.Prod_TransitionItemInventoryFK;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Quantity = a.Mkt_POExtTransfer.TotalRequired;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Requisition = "Requisition";
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreated = a.Mkt_POExtTransfer.FirstCreated;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.LastEdited = DateTime.Now;
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Active = true;
                db.Entry(VmStore_InternalTransferSlave.Mkt_POSlave_Consumption).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Redirect("../../" + a.ReturnController);
        }

        public async Task<ActionResult> POAlsoTransferItemDelete(string id, string returnurl)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deleteID = 0, mktPOID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deleteID);

            id = id.Replace(deleteID.ToString() + "x", "");
            int.TryParse(id, out mktPOID);
            Mkt_POExtTransfer b = new Mkt_POExtTransfer();
            b.ID = deleteID;
            b.Delete(0);

            VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
            await Task.Run(() => VmStore_InternalTransferSlave.UpdateConsumptionExt(deleteID));
            if (VmStore_InternalTransferSlave.Mkt_POSlave_Consumption != null)
            {
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Active = false;
                db.Entry(VmStore_InternalTransferSlave.Mkt_POSlave_Consumption).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Redirect("../../" + returnurl);
        }

        #endregion

        [HttpGet]
        public ActionResult StoreRequisitionItem(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStoreExternalReqIdDropDown();
            SelectList sl = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.StoreReq = sl;


            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();



            VmGeneral_POSlave VmGeneral_POSlave = new VmGeneral_POSlave();

            VmProd_Requisition_Slave.VmGeneral_POSlave = new VmGeneral_POSlave();
            VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave = new Mkt_POSlave();

            VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.Mkt_POFK = id;
            VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.Mkt_BOMSlaveFK = 1;
            return View(VmProd_Requisition_Slave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StoreRequisitionItem(VmProd_Requisition_Slave VmProd_Requisition_Slave)
        {
            int aid = 0;
            aid = VmProd_Requisition_Slave.Prod_Requisition_Slave.ID;
            VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.RequisitionSlaveFK = aid;
            VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.Raw_ItemFK = VmProd_Requisition_Slave.GetRawItemID(aid);
            VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.Common_UnitFK = VmProd_Requisition_Slave.GetCommonUintID(aid);
            VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.AddReady(UserID);
            VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.FirstCreatedBy = UserID;
            await Task.Run(() => VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.Add());
            return RedirectToAction("VmGeneral_POSlaveDetails/" + VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.Mkt_POFK);
            //}
            //return RedirectToAction("VmGeneral_POSlaveDetails/" + VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.Mkt_POFK);
        }

        [HttpGet]
        public async Task<ActionResult> RequisionItem_Finalize(int? reqid, int? poid)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (reqid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();

            await Task.Run(() => VmProd_Requisition_Slave.SelectSingle(reqid.Value));
            VmProd_Requisition_Slave.Mkt_PO = new Mkt_PO();
            VmProd_Requisition_Slave.Mkt_PO.ID = poid.Value;
            if (VmProd_Requisition_Slave.Prod_Requisition_Slave == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_Requisition_Slave);
        }

        [HttpPost]
        public async Task<ActionResult> RequisionItem_Finalize(VmProd_Requisition_Slave VmProd_Requisition_Slave)
        {
            //if (ModelState.IsValid)
            // {
            await Task.Run(() => VmProd_Requisition_Slave.Edit(0));
            return RedirectToAction("VmGeneral_POSlaveDetails", new { id = VmProd_Requisition_Slave.Mkt_PO.ID });
            //}

        }



        [HttpGet]
        public ActionResult VmGeneral_POSlaveCreate(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetMktPODropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PO = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetAllRawCategory();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            VmGeneral_POSlave VmGeneral_POSlave = new VmGeneral_POSlave();
            VmGeneral_POSlave.Mkt_POSlave = new Mkt_POSlave();
            VmGeneral_POSlave.Mkt_POSlave.Mkt_POFK = id;
            VmGeneral_POSlave.Mkt_POSlave.Mkt_BOMSlaveFK = 1;
            return View(VmGeneral_POSlave);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmGeneral_POSlaveCreate(Mkt_POSlave mkt_POSlave)
        {
            if (ModelState.IsValid)
            {
                mkt_POSlave.RequisitionSlaveFK = 1;
                mkt_POSlave.Consumption = 0;
                mkt_POSlave.AddReady(UserID);
                await Task.Run(() => mkt_POSlave.Add());
                return RedirectToAction("VmGeneral_POSlaveDetails/" + mkt_POSlave.Mkt_POFK);
            }
            return RedirectToAction("VmGeneral_POSlaveDetails/" + mkt_POSlave.Mkt_POFK);
        }

        [HttpGet]
        public async Task<ActionResult> VmGeneral_POSlaveEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetMktPODropDown();

            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PO = sl6;

            VmForDropDown.DDownData = VmForDropDown.GetSupplierForDropDown();
            SelectList sl5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Supplier = sl5;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmGeneral_POSlave = new VmGeneral_POSlave();
            await Task.Run(() => VmGeneral_POSlave.SelectSingle(id.Value));
            if (VmGeneral_POSlave.Mkt_POSlave == null)
            {
                return HttpNotFound();
            }
            //no chg
            VmForDropDown.DDownData = VmForDropDown.GetTempRaw_ItemForEdit(VmGeneral_POSlave.Mkt_POSlave.Raw_ItemFK);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Item = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetTempRawSubCategoryForEdit(VmGeneral_POSlave.Mkt_POSlave.Raw_ItemFK);
            SelectList sl8 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_SubCategory = sl8;

            VmForDropDown.DDownData = VmForDropDown.GetTempRaw_CategoryForEdit(VmGeneral_POSlave.Mkt_POSlave.Raw_ItemFK);
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;
            return View(VmGeneral_POSlave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmGeneral_POSlaveEdit(VmGeneral_POSlave VmGeneral_POSlave)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmGeneral_POSlave.Edit(0));
                return RedirectToAction("VmGeneral_POSlaveDetails/" + VmGeneral_POSlave.Mkt_POSlave.Mkt_POFK);
            }
            return View(VmGeneral_POSlave);
        }


        [HttpGet]
        public async Task<ActionResult> VmGeneral_POSlaveEditFromRequisition(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmGeneral_POSlave = new VmGeneral_POSlave();
            await Task.Run(() => VmGeneral_POSlave.SelectSingle(id.Value));
            if (VmGeneral_POSlave.Mkt_POSlave == null)
            {
                return HttpNotFound();
            }

            int reqslaveid = 0;
            if (VmGeneral_POSlave.Mkt_POSlave.RequisitionSlaveFK != null)
            {
                reqslaveid = VmGeneral_POSlave.Mkt_POSlave.RequisitionSlaveFK.Value;
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStoreExternalReqIdDropDown();
            SelectList sl = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.StoreReq = sl;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetItemsForMkt_POSlaveEditFromRequisition(reqslaveid);
            SelectList s2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ItemList = s2;
            VmGeneral_POSlave.Prod_Requisition_Slave = new Prod_Requisition_Slave();
            VmGeneral_POSlave.Prod_Requisition_Slave.ID = reqslaveid;

            VmGeneral_POSlave.Mkt_POSlave.RequisitionSlaveFK = db.Prod_Requisition_Slave.Where(x => x.ID == reqslaveid)
                .Select(x => x.Prod_RequisitionFK).FirstOrDefault();
            helperController = new HelperController();
            VmGeneral_POSlave.RequiredQty = helperController.GetReqItemQtyForEdit(reqslaveid);
            VmGeneral_POSlave.EstimatedPrice = helperController.GetEstimatedPriceForEdit(reqslaveid);


            return View(VmGeneral_POSlave);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmGeneral_POSlaveEditFromRequisition(VmGeneral_POSlave VmGeneral_POSlave)
        {
            //if (ModelState.IsValid)
            //{
            int aid = 0;
            aid = VmGeneral_POSlave.Prod_Requisition_Slave.ID;
            VmGeneral_POSlave.Mkt_POSlave.RequisitionSlaveFK = aid;
            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();
            VmGeneral_POSlave.Mkt_POSlave.Raw_ItemFK = VmProd_Requisition_Slave.GetRawItemID(aid);
            VmGeneral_POSlave.Mkt_POSlave.Common_UnitFK = VmProd_Requisition_Slave.GetCommonUintID(aid);
            await Task.Run(() => VmGeneral_POSlave.Edit(0));
            return RedirectToAction("VmGeneral_POSlaveDetails/" + VmGeneral_POSlave.Mkt_POSlave.Mkt_POFK);
            //}
            //return View(VmGeneral_POSlave);
        }

        public ActionResult VmGeneral_POSlaveDelete(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deletID = 0, poID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deletID);

            id = id.Replace(deletID.ToString() + "x", "");
            int.TryParse(id, out poID);
            Mkt_POSlave b = new Mkt_POSlave();
            b.ID = deletID;
            b.Delete(0);
            return RedirectToAction("VmGeneral_POSlaveDetails/" + poID);
        }


        #region RequsitionPOInventoryReport

        public ActionResult POInventoryReport(int? id)
        {
            if (!id.HasValue)
            {
                IdNullDivertion("POInventoryReport", "Id is Null");
            }
            try
            {
                VmGeneral_PO b = new VmGeneral_PO();
                b.POInventoryReportDocLoad(id.Value);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/POInventoryProcurement.rpt");
                cr.Load();
                cr.SetDataSource(b.OrderReportDoc);
                cr.SummaryInfo.ReportTitle = "PO Inventory";
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("POInventoryReport", ex.Message);
            }
            return null;
        }

        #endregion

        //..........OrderInventoryBalanceReportDoc.......//
        public ActionResult OrderInventoryBalanceReportDoc(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                IdNullDivertion("OrderReportDoc", "Id is Null");
            }
            try
            {
                VmCommon_TheOrder b = new VmCommon_TheOrder();
                b.Common_TheOrder = new Common_TheOrder();
                b.SelectSingleBOM(id);
                b.OrderInventoryBalanceReport(id);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/OrderBaseInventoryDoc.rpt");
                cr.Load();
                cr.SetDataSource(b.OrderReportDoc);
                cr.SummaryInfo.ReportTitle = "Inventory of " + b.Common_TheOrder.CID + " Order";

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("OrderReportDoc", ex.Message);
            }
            return null;
        }
        private ActionResult IdNullDivertion(string methodName, string messege)
        {
            ErrorReports er = new ErrorReports();
            er.ControllerName = this.ToString();
            er.MethodName = methodName;
            er.ErrorDescription = "Id is Null";
            er.EmailErrorAuto();
            return View("../Home/ErrorView", er);
        }

        // ..............item wise order inventory..............//

        public async Task<ActionResult> ItemLeftoverReport()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStoreInventory = new VmStoreInventory();
            await Task.Run(() => VmStoreInventory.ItemLeftOverInventory());

            return View(VmStoreInventory);



        }

        public ActionResult ItemWiseReport()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStoreInventory a = new VmStoreInventory();
            a.ItemInventory();

            return View(a);



        }

        public async Task<ActionResult> CategoryInventoryIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmStoreInventory = new VmStoreInventory();

            await Task.Run(() => VmStoreInventory.CategoryWiseInventory());
            return View(VmStoreInventory);
        }


        public async Task<ActionResult> SubCategoryInventoryIndex(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmStoreInventory = new VmStoreInventory();

            await Task.Run(() => VmStoreInventory = VmStoreInventory.SelectSingleCategoryInventory(id));
            await Task.Run(() => VmStoreInventory.SubCategoryWiseInventory(id));


            return View(VmStoreInventory);
        }
        public async Task<ActionResult> SubCategoryColsingInventoryIndex(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmStoreInventory = new VmStoreInventory();

            await Task.Run(() => VmStoreInventory = VmStoreInventory.SelectSingleCategoryInventory(id));
            await Task.Run(() => VmStoreInventory.SubCategoryWiseClosingInventory(id));


            return View(VmStoreInventory);
        }

        public async Task<ActionResult> ItemInventoryIndex(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmStoreInventory = new VmStoreInventory();

            await Task.Run(() => VmStoreInventory = VmStoreInventory.SelectJoinedRaw_SubCategory(id));
            await Task.Run(() => VmStoreInventory.ItemWiseInventory(id));


            return View(VmStoreInventory);
        }
        public async Task<ActionResult> ItemWiseClosingInventoryIndex(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmStoreInventory = new VmStoreInventory();

            await Task.Run(() => VmStoreInventory = VmStoreInventory.SelectJoinedRaw_SubCategory(id));
            await Task.Run(() => VmStoreInventory.ItemWiseClosingInventory(id));


            return View(VmStoreInventory);
        }

        public async Task<ActionResult> ItemDetailsInventoryIndex(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmStoreInventory = new VmStoreInventory();

            await Task.Run(() => VmStoreInventory = VmStoreInventory.SelectSingleRaw_Item(id));
            await Task.Run(() => VmStoreInventory.ItemWiseDetailsInventory(id));


            return View(VmStoreInventory);
        }

        //............Item Leftover inventory...............//

        //public ActionResult ItemLeftoverReport()
        //{
        //    try
        //    {
        //        VmStoreInventory a = new VmStoreInventory();
        //        a.ItemInventory();
        //        ReportClass cr = new ReportClass();
        //        cr.FileName = Server.MapPath("~/Views/Reports/ItemWiseLeftoverInventory.rpt");
        //        cr.Load();
        //        cr.SetDataSource(a.ItemInventoryReport);
        //        Stream stream =
        //        cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //        return File(stream, "application/pdf");
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    return null;


        //}

        public ActionResult InventoryLedgerIndex(int itemid, string name)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.ItemId = itemid;
            ViewBag.ItemName = name;
            return View();
        }

        public ActionResult InventoryLedger(FormCollection collection)
        {

            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var fromdate = collection["FromDate"];
            var todate = collection["ToDate"];
            var itemid = collection["ItemId"];
            var itemName = collection["ItemName"];

            if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
            {
                db = new xOssContext();
                var ledger = db.Database.SqlQuery<StoreLedger>($@"EXEC dbo.StoreItemLedgerItemWise '{itemid}', '{fromdate}', '{todate}'").ToList();
                var opening = db.Database.SqlQuery<decimal>($@"EXEC dbo.StoreItemLedgerOpeningBalance '{itemid}', '{fromdate}', '{todate}'").FirstOrDefault();

                ViewBag.ItemName = itemName;
                ViewBag.StoreLedger = ledger;
                ViewBag.OpeningDate = fromdate;
                ViewBag.ToDate = todate;
                ViewBag.OpenigBalance = opening;
            }
            return View();
        }



        public async Task<ActionResult> InternalStoreIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmInternalStore = new VmInternalStore();
            await Task.Run(() => VmInternalStore.InitialDataLoadForInternalStore());


            return View(VmInternalStore);
        }


        public async Task<ActionResult> ExternalStoreIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmInternalStore = new VmInternalStore();
            await Task.Run(() => VmInternalStore.InitialDataLoadForExternalStore());


            return View(VmInternalStore);
        }

        //[HttpGet]
        //public async Task<ActionResult> VmMkt_POEditFromDeliver(int? id)
        //{
        //    VmUser_User user = (VmUser_User)Session["One"];
        //    if (user == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    //VmForDropDown = new VmForDropDown();
        //    //VmForDropDown.DDownData = VmForDropDown.GetRaw_StoreDropDown();
        //    //SelectList list = new SelectList(VmForDropDown.DDownData, "Value", "Text");
        //    //ViewBag.Raw_Store = list;



        //    await Task.Run(() => VmMkt_PO.InitialDataLoad_PoSlaveSend(id.Value));
        //    VmMkt_PO.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
        //    VmMkt_PO.Mkt_POSlave_Consumption.Date = DateTime.Today;
        //    //if (VmMkt_PO.Mkt_PO == null)
        //    //{
        //    //    return HttpNotFound();
        //    //}
        //    return View(VmMkt_PO);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VmMkt_POEditFromDeliver(VmMkt_PO VmMkt_PO, IEnumerable<VmMktPO_Slave> DataList2)
        //{
        //    foreach (ModelState modelState in ViewData.ModelState.Values)
        //    {
        //        foreach (ModelError error in modelState.Errors)
        //        {
        //            string s = error.ErrorMessage;
        //        }
        //    }
        //    //if (ModelState.IsValid)
        //    //{
        //    await Task.Run(() => VmMkt_PO.VmPOSlave_DeliveryCreate(UserID, DataList2));
        //    return RedirectToAction("PurchaseOrderDeliver");
        //    //}
        //    //return View(VmMkt_PO);
        //}

        public async Task<ActionResult> VmGeneral_InterTransfer()//-------Store Inter Transfer------//
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStore_InterTransfer VmStore_InterTransfer = new VmStore_InterTransfer();

            await Task.Run(() => VmStore_InterTransfer.InitialDataLoad());
            return View(VmStore_InterTransfer);
        }


        public ActionResult VmStore_TransferCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetInternalStore();
            SelectList internalStore = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.InternalStore = internalStore;
            VmForDropDown.DDownData = VmForDropDown.GetDepartment();
            SelectList Def = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Department = Def;


            VmStore_InterTransfer = new VmStore_InterTransfer();
            VmStore_InterTransfer.Raw_InternaleTransfer = new Raw_InternaleTransfer();
            VmStore_InterTransfer.Raw_InternaleTransfer.Date = DateTime.Today;
            VmStore_InterTransfer.Raw_InternaleTransfer.FromUser_DeptFK = user.User_User.User_DepartmentFK;
            VmStore_InterTransfer.Raw_InternaleTransfer.AcknowledgeDate = DateTime.Now;
            return View(VmStore_InterTransfer);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmStore_TransferCreate(VmStore_InterTransfer VmStore_InterTransfer)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmStore_InterTransfer.Add(UserID));
                return RedirectToAction("VmGeneral_InterTransfer");
            }
            return RedirectToAction("VmGeneral_InterTransfer");
        }

        [HttpGet]
        public async Task<ActionResult> VmStore_TransferEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetInternalStore();
            SelectList internalStore = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.InternalStore = internalStore;

            VmStore_InterTransfer = new VmStore_InterTransfer();
            await Task.Run(() => VmStore_InterTransfer.SelectSingle(id.Value));

            if (VmStore_InterTransfer.Raw_InternaleTransfer == null)
            {
                return HttpNotFound();
            }
            return View(VmStore_InterTransfer);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmStore_TransferEdit(VmStore_InterTransfer a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Edit(0));
                return RedirectToAction("VmGeneral_InterTransfer");
            }
            return View(VmStore_InterTransfer);
        }
        public ActionResult VmStore_TransferDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Raw_InternaleTransfer b = new Raw_InternaleTransfer();
            b.ID = id;
            b.Delete(0);

            return RedirectToAction("VmGeneral_InterTransfer");
        }
      

        [HttpGet]
        public async Task<ActionResult> VmStoreInterTransferAcknowledgement(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmStore_InterTransfer VmStore_InterTransfer = new VmStore_InterTransfer();           
            await Task.Run(() => VmStore_InterTransfer.SelectSingle(id.Value));
            if (VmStore_InterTransfer.Raw_InternaleTransfer == null)
            {
                return HttpNotFound();
            }
            VmStore_InterTransfer.Raw_InternaleTransfer.AcknowledgeDate = DateTime.Now;
            return View(VmStore_InterTransfer);
        }

        [HttpPost]
        public async Task<ActionResult> VmStoreInterTransferAcknowledgement(VmStore_InterTransfer a)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            await Task.Run(() => a.StoreInterTransferAcknowledgement(0));

            return RedirectToAction("VmGeneral_InterTransfer", "Store");
        }

        
        public async Task<ActionResult> VmGeneral_InternalTransferSlaveIndex(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();

            await Task.Run(() => VmStore_InternalTransferSlave = VmStore_InternalTransferSlave.SelectSingleJoinedInternaleTransfer(id.Value));
            await Task.Run(() => VmStore_InternalTransferSlave.InitialDataLoadForInternaleTransferSlave(id.Value));



            return View(VmStore_InternalTransferSlave);
        }

        [HttpGet]
        public ActionResult VmGeneral_InternalTransferSlaveCreate(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItem = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Unit = sl4;

            VmForDropDown.DDownData = VmForDropDown.GetRaw_CategoryForDropDown();
            SelectList sl7 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Raw_Category = sl7;

            VmStore_InternalTransferSlave VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
            VmStore_InternalTransferSlave.Raw_InternaleTransferSlave = new Raw_InternaleTransferSlave();
            VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Raw_InternaleTransferFK = id;

            return View(VmStore_InternalTransferSlave);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmGeneral_InternalTransferSlaveCreate(VmStore_InternalTransferSlave a)
        {
            //if (ModelState.IsValid)
            //{
            //a.AddReady(UserID);

            a.Raw_InternaleTransferSlave.Prod_TransitionItemInventoryFK = 1;
            a.Raw_InternaleTransferSlave.Mkt_BOMFK = 1;
            int rawInternaleTransferSlaveID = 0;
            await Task.Run(() => rawInternaleTransferSlaveID = a.Add(UserID));

            return RedirectToAction("VmGeneral_InternalTransferSlaveIndex/" + a.Raw_InternaleTransferSlave.Raw_InternaleTransferFK);
            //}
            //return RedirectToAction("VmGeneral_InternalTransferSlaveIndex/" + a.Raw_InternaleTransferSlave.Raw_InternaleTransferFK);
        }

        [HttpGet]
        public async Task<ActionResult> VmGeneral_InternalTransferSlaveEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStoreReqIdTrue();
            SelectList sl = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.StoreReq = sl;

            VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
            await Task.Run(() => VmStore_InternalTransferSlave.SelectSingle(id.Value));
            if (VmStore_InternalTransferSlave.Raw_InternaleTransferSlave == null)
            {
                return HttpNotFound();
            }


            return View(VmStore_InternalTransferSlave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmGeneral_InternalTransferSlaveEdit(VmStore_InternalTransferSlave a)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Edit(0));
                VmControllerHelper = new VmControllerHelper();
                UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
                VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
                await Task.Run(() => VmStore_InternalTransferSlave.UpdateConsumption(a.Raw_InternaleTransferSlave.ID));

                if (a.Raw_InternaleTransferSlave.Mkt_POSlaveFK != 1)
                {

                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Date = DateTime.Now;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Description = "";
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreatedBy = UserID;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.IsReturn = false;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.LastEditeddBy = 0;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Mkt_POSlaveFK = a.Raw_InternaleTransferSlave.Mkt_POSlaveFK;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = a.Raw_InternaleTransferSlave.RequisitionSlaveFK;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = 1;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Quantity = a.Raw_InternaleTransferSlave.Quantity;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Requisition = "Requisition";
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreated = a.Raw_InternaleTransferSlave.FirstCreated;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.LastEdited = DateTime.Now;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Active = true;
                    db.Entry(VmStore_InternalTransferSlave.Mkt_POSlave_Consumption).State = EntityState.Modified;
                    db.SaveChanges();
                    //conInserted =vmConsumption.Add(0);
                    //int sss = 00;
                }
                else if (a.Raw_InternaleTransferSlave.Prod_TransitionItemInventoryFK != 1)
                {

                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Date = DateTime.Now;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Description = "";
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreatedBy = UserID;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.IsReturn = false;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.LastEditeddBy = 0;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Mkt_POSlaveFK = 1;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = a.Raw_InternaleTransferSlave.RequisitionSlaveFK;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = a.Raw_InternaleTransferSlave.Prod_TransitionItemInventoryFK;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Quantity = a.Raw_InternaleTransferSlave.Quantity;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Requisition = "Requisition";
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.FirstCreated = a.Raw_InternaleTransferSlave.FirstCreated;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.LastEdited = DateTime.Now;
                    VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Active = true;
                    db.Entry(VmStore_InternalTransferSlave.Mkt_POSlave_Consumption).State = EntityState.Modified;
                    db.SaveChanges();
                    //conInserted = vmConsumption.Add(0);

                }



                return RedirectToAction("VmGeneral_InternalTransferSlaveIndex/" + a.Raw_InternaleTransferSlave.Raw_InternaleTransferFK);
            }
            return View(VmStore_InternalTransferSlave);
        }
        
        [HttpGet]
        public ActionResult InternalTransferRequisitionItem(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStoreReqIdTrue();
            ViewBag.StoreReq = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
            VmStore_InternalTransferSlave.Raw_InternaleTransferSlave = new Raw_InternaleTransferSlave();
            VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Raw_InternaleTransferFK = id;
          

            var transfer = db.Raw_InternaleTransfer.FirstOrDefault(x => x.ID == id);
            VmStore_InternalTransferSlave.Raw_InternaleTransfer = new Raw_InternaleTransfer();
            VmStore_InternalTransferSlave.Raw_InternaleTransfer.FromUser_DeptFK = transfer.FromUser_DeptFK;
            VmStore_InternalTransferSlave.Raw_InternaleTransfer.ToUser_DeptFK = transfer.ToUser_DeptFK;



            return View(VmStore_InternalTransferSlave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> InternalTransferRequisitionItem(VmStore_InternalTransferSlave VmStore_InternalTransferSlave)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            //VmControllerHelper = new VmControllerHelper();
            //UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            int inserted = 0;
            var requisitionSlave = db.Prod_Requisition_Slave.FirstOrDefault(a => a.ID == VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.RequisitionSlaveFK);
            var requisition = db.Prod_Requisition.FirstOrDefault(a => a.ID == requisitionSlave.Prod_RequisitionFK);
            
            if (requisitionSlave.Mkt_POSlaveFK != 1)
            {
                VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Mkt_POSlaveFK = requisitionSlave.Mkt_POSlaveFK;
                VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Raw_ItemFK = 1;
                VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Prod_TransitionItemInventoryFK = 1;
            }
            else if (requisitionSlave.Prod_TransitionItemInventoryFk != 1)
            {
                VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Mkt_POSlaveFK = 1;
                VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Raw_ItemFK = 1;
                VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Prod_TransitionItemInventoryFK =
                    requisitionSlave.Prod_TransitionItemInventoryFk;
            }
            await Task.Run(() => inserted = VmStore_InternalTransferSlave.Add(user.User_User.ID));
            if (inserted > 0)
            {
                int conInserted = 0;
                int? fromUserDeptId = VmStore_InternalTransferSlave.Raw_InternaleTransfer.FromUser_DeptFK;
                int? toUserDeptId = VmStore_InternalTransferSlave.Raw_InternaleTransfer.ToUser_DeptFK;
                if (VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Mkt_POSlaveFK != 1)
                {
                    if (toUserDeptId != 2 || toUserDeptId != 13)
                    {
                        VmMktPOSlave_Consumption vmConsumption = new VmMktPOSlave_Consumption();
                        vmConsumption.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
                        vmConsumption.Mkt_POSlave_Consumption.IsReturn = false;
                        vmConsumption.Mkt_POSlave_Consumption.Raw_InternaleTransferSlaveFk = inserted;
                        vmConsumption.Mkt_POSlave_Consumption.Date = DateTime.Now;
                        vmConsumption.Mkt_POSlave_Consumption.Description = requisition.Description;
                        vmConsumption.Mkt_POSlave_Consumption.FirstCreatedBy = UserID;
                        vmConsumption.Mkt_POSlave_Consumption.LastEditeddBy = 0;
                        vmConsumption.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
                        vmConsumption.Mkt_POSlave_Consumption.Mkt_POSlaveFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Mkt_POSlaveFK;
                        vmConsumption.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.RequisitionSlaveFK;
                        vmConsumption.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = 1;
                        vmConsumption.Mkt_POSlave_Consumption.Quantity = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Quantity;
                        vmConsumption.Mkt_POSlave_Consumption.Requisition = requisition.RequisitionCID;
                        vmConsumption.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                        vmConsumption.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                        vmConsumption.Mkt_POSlave_Consumption.FirstCreated = DateTime.Now;
                        vmConsumption.Mkt_POSlave_Consumption.LastEdited = null;
                        vmConsumption.Mkt_POSlave_Consumption.Active = true;
                        db.Mkt_POSlave_Consumption.Add(vmConsumption.Mkt_POSlave_Consumption);
                        db.SaveChanges();
                    }
                    if (toUserDeptId == 2 && fromUserDeptId != 13)
                    {
                        VmMktPOSlave_Consumption vmConsumption = new VmMktPOSlave_Consumption();
                        vmConsumption.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
                        vmConsumption.Mkt_POSlave_Consumption.IsReturn = true;   //return to store
                        vmConsumption.Mkt_POSlave_Consumption.Raw_InternaleTransferSlaveFk = inserted;
                        vmConsumption.Mkt_POSlave_Consumption.Date = DateTime.Now;
                        vmConsumption.Mkt_POSlave_Consumption.Description = requisition.Description;
                        vmConsumption.Mkt_POSlave_Consumption.FirstCreatedBy = UserID;
                        vmConsumption.Mkt_POSlave_Consumption.LastEditeddBy = 0;
                        vmConsumption.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
                        vmConsumption.Mkt_POSlave_Consumption.Mkt_POSlaveFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Mkt_POSlaveFK;
                        vmConsumption.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.RequisitionSlaveFK;
                        vmConsumption.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = 1;
                        vmConsumption.Mkt_POSlave_Consumption.Quantity = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Quantity;
                        vmConsumption.Mkt_POSlave_Consumption.Requisition = requisition.RequisitionCID;
                        vmConsumption.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                        vmConsumption.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                        vmConsumption.Mkt_POSlave_Consumption.FirstCreated = DateTime.Now;
                        vmConsumption.Mkt_POSlave_Consumption.LastEdited = null;
                        vmConsumption.Mkt_POSlave_Consumption.Active = true;
                        db.Mkt_POSlave_Consumption.Add(vmConsumption.Mkt_POSlave_Consumption);
                        db.SaveChanges();
                    }

                    //if ((fromUserDeptId == 2 || fromUserDeptId == 13) && (toUserDeptId == 17 || toUserDeptId == 9 || toUserDeptId == 10 || toUserDeptId == 11 || toUserDeptId == 12 || toUserDeptId == 5)) // 2 : Store , 13: Knitting Store
                    //{
                    //    VmMktPOSlave_Consumption vmConsumption = new VmMktPOSlave_Consumption();
                    //    vmConsumption.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
                    //    vmConsumption.Mkt_POSlave_Consumption.IsReturn = false;
                    //    vmConsumption.Mkt_POSlave_Consumption.Raw_InternaleTransferSlaveFk = inserted;
                    //    vmConsumption.Mkt_POSlave_Consumption.Date = DateTime.Now;
                    //    vmConsumption.Mkt_POSlave_Consumption.Description = requisition.Description;
                    //    vmConsumption.Mkt_POSlave_Consumption.FirstCreatedBy = UserID;
                    //    vmConsumption.Mkt_POSlave_Consumption.LastEditeddBy = 0;
                    //    vmConsumption.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
                    //    vmConsumption.Mkt_POSlave_Consumption.Mkt_POSlaveFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Mkt_POSlaveFK;
                    //    vmConsumption.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.RequisitionSlaveFK;
                    //    vmConsumption.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = 1;
                    //    vmConsumption.Mkt_POSlave_Consumption.Quantity = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Quantity;
                    //    vmConsumption.Mkt_POSlave_Consumption.Requisition = requisition.RequisitionCID;
                    //    vmConsumption.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                    //    vmConsumption.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                    //    vmConsumption.Mkt_POSlave_Consumption.FirstCreated = DateTime.Now;
                    //    vmConsumption.Mkt_POSlave_Consumption.LastEdited = null;
                    //    vmConsumption.Mkt_POSlave_Consumption.Active = true;

                    //    db.Mkt_POSlave_Consumption.Add(vmConsumption.Mkt_POSlave_Consumption);
                    //    db.SaveChanges();
                    //}
                    //if ((fromUserDeptId == 17 || fromUserDeptId == 9 || fromUserDeptId == 10 || fromUserDeptId == 11 || fromUserDeptId == 12 || fromUserDeptId == 5) && (toUserDeptId == 2 || toUserDeptId == 13)) // 2 : Store , 13: Knitting Store
                    //{
                    //    VmMktPOSlave_Consumption vmConsumption = new VmMktPOSlave_Consumption();
                    //    vmConsumption.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
                    //    vmConsumption.Mkt_POSlave_Consumption.IsReturn = true;   //return to store
                    //    vmConsumption.Mkt_POSlave_Consumption.Raw_InternaleTransferSlaveFk = inserted;
                    //    vmConsumption.Mkt_POSlave_Consumption.Date = DateTime.Now;
                    //    vmConsumption.Mkt_POSlave_Consumption.Description = requisition.Description;
                    //    vmConsumption.Mkt_POSlave_Consumption.FirstCreatedBy = UserID;
                    //    vmConsumption.Mkt_POSlave_Consumption.LastEditeddBy = 0;
                    //    vmConsumption.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
                    //    vmConsumption.Mkt_POSlave_Consumption.Mkt_POSlaveFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Mkt_POSlaveFK;
                    //    vmConsumption.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.RequisitionSlaveFK;
                    //    vmConsumption.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = 1;
                    //    vmConsumption.Mkt_POSlave_Consumption.Quantity = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Quantity;
                    //    vmConsumption.Mkt_POSlave_Consumption.Requisition = requisition.RequisitionCID;
                    //    vmConsumption.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                    //    vmConsumption.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                    //    vmConsumption.Mkt_POSlave_Consumption.FirstCreated = DateTime.Now;
                    //    vmConsumption.Mkt_POSlave_Consumption.LastEdited = null;
                    //    vmConsumption.Mkt_POSlave_Consumption.Active = true;
                    //    db.Mkt_POSlave_Consumption.Add(vmConsumption.Mkt_POSlave_Consumption);
                    //    db.SaveChanges();
                    //}
                    //conInserted =vmConsumption.Add(0);
                    //int sss = 00;
                }
                //else if (VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Prod_TransitionItemInventoryFK != 1)
                //{


                //    VmMktPOSlave_Consumption vmConsumption = new VmMktPOSlave_Consumption();
                //    vmConsumption.Mkt_POSlave_Consumption = new Mkt_POSlave_Consumption();
                //    vmConsumption.Mkt_POSlave_Consumption.Raw_InternaleTransferSlaveFk = inserted;
                //    vmConsumption.Mkt_POSlave_Consumption.Date = DateTime.Now;
                //    vmConsumption.Mkt_POSlave_Consumption.Description = "";
                //    vmConsumption.Mkt_POSlave_Consumption.FirstCreatedBy = UserID;
                //    vmConsumption.Mkt_POSlave_Consumption.IsReturn = false;
                //    vmConsumption.Mkt_POSlave_Consumption.LastEditeddBy = 0;
                //    vmConsumption.Mkt_POSlave_Consumption.Mkt_POExtTransferFK = 1;
                //    vmConsumption.Mkt_POSlave_Consumption.Mkt_POSlaveFK = 1;
                //    vmConsumption.Mkt_POSlave_Consumption.Prod_Requisition_SlaveFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.RequisitionSlaveFK;
                //    vmConsumption.Mkt_POSlave_Consumption.Prod_TransitionItemInventoryFK = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Prod_TransitionItemInventoryFK;
                //    vmConsumption.Mkt_POSlave_Consumption.Quantity = VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Quantity;
                //    vmConsumption.Mkt_POSlave_Consumption.Requisition = requisition.RequisitionCID;
                //    vmConsumption.Mkt_POSlave_Consumption.Supervisor = "Supervisor";
                //    vmConsumption.Mkt_POSlave_Consumption.User_DepartmentFk = user.User_User.User_DepartmentFK;
                //    vmConsumption.Mkt_POSlave_Consumption.FirstCreated = DateTime.Now;
                //    vmConsumption.Mkt_POSlave_Consumption.LastEdited = null;
                //    vmConsumption.Mkt_POSlave_Consumption.Active = true;
                //    db.Mkt_POSlave_Consumption.Add(vmConsumption.Mkt_POSlave_Consumption);
                //    db.SaveChanges();
                //    //conInserted = vmConsumption.Add(0);

                //}
            }




            return RedirectToAction("VmGeneral_InternalTransferSlaveIndex/" + VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Raw_InternaleTransferFK);

        }
        
        public async Task<ActionResult> VmGeneral_InternalTransferSlaveDelete(string id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deletID = 0, InternaleTransferID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deletID);

            id = id.Replace(deletID.ToString() + "x", "");
            int.TryParse(id, out InternaleTransferID);
            Raw_InternaleTransferSlave b = new Raw_InternaleTransferSlave();
            b.ID = deletID;
            b.Delete(0);
            VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
            await Task.Run(() => VmStore_InternalTransferSlave.UpdateConsumption(deletID));
            if (VmStore_InternalTransferSlave.Mkt_POSlave_Consumption != null)
            {
                VmStore_InternalTransferSlave.Mkt_POSlave_Consumption.Active = false;
                db.Entry(VmStore_InternalTransferSlave.Mkt_POSlave_Consumption).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("VmGeneral_InternalTransferSlaveIndex/" + InternaleTransferID);
        }
        [HttpGet]
        public async Task<ActionResult> VmInternalTransferFromRequisition(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStoreReqIdTrue();
            SelectList sl = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.StoreReq = sl;

            VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
            await Task.Run(() => VmStore_InternalTransferSlave.SelectSingle(id.Value));
            await Task.Run(() => VmStore_InternalTransferSlave.GetRequsitionID((int)VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.RequisitionSlaveFK));
            VmForDropDown.DDownData = VmForDropDown.GetItemsForStyle((int)VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Mkt_BOMFK);
            SelectList s2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            ViewBag.RequisitionItem = s2;

            if (VmStore_InternalTransferSlave.Raw_InternaleTransferSlave == null)
            {
                return HttpNotFound();
            }

            return View(VmStore_InternalTransferSlave);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmInternalTransferFromRequisition(VmStore_InternalTransferSlave a)
        {
            await Task.Run(() => a.Edit(0));
            return RedirectToAction("VmGeneral_InternalTransferSlaveIndex/" + a.Raw_InternaleTransferSlave.Raw_InternaleTransferFK);
        }

        public async Task<ActionResult> InternalTransferAuthorize(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStore_InterTransfer = new VmStore_InterTransfer();
            await Task.Run(() => VmStore_InterTransfer.SelectSingle(id.Value));

            if (VmStore_InterTransfer.Raw_InternaleTransfer == null)
            {
                return HttpNotFound();
            }
            return View(VmStore_InterTransfer);
        }


        [HttpPost]
        public async Task<ActionResult> InternalTransferAuthorize(VmStore_InterTransfer a)
        {
            await Task.Run(() => a.Edit(0));
            return RedirectToAction("VmGeneral_InterTransfer");
        }

        [HttpGet]
        public ActionResult InternalTransferTranstionsCreate(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetStoreReqIdTrue();
            SelectList sl = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.StoreReq = sl;

            VmProd_Requisition_Slave = new VmProd_Requisition_Slave();


            VmProd_Requisition_Slave.VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
            VmProd_Requisition_Slave.VmStore_InternalTransferSlave.Raw_InternaleTransferSlave = new Raw_InternaleTransferSlave();

            VmProd_Requisition_Slave.VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Raw_InternaleTransferFK = id;


            return View(VmProd_Requisition_Slave);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> InternalTransferTranstionsCreate(VmStore_InternalTransferSlave VmStore_InternalTransferSlave)
        {
            //if (ModelState.IsValid)
            //{

            var a = db.Prod_TransitionItemInventory.FirstOrDefault(x =>
               x.ID == VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Raw_InternaleTransferFK);
            if (a != null) VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Mkt_BOMFK = a.Mkt_BOMFk;
            VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Raw_ItemFK = 1;

            await Task.Run(() => VmStore_InternalTransferSlave.Add(0));
            return RedirectToAction("VmGeneral_InternalTransferSlaveIndex/" + VmStore_InternalTransferSlave.Raw_InternaleTransferSlave.Raw_InternaleTransferFK);
            //}
            //return RedirectToAction("VmGeneral_POSlaveDetails/" + VmProd_Requisition_Slave.VmGeneral_POSlave.Mkt_POSlave.Mkt_POFK);
        }

        [HttpGet]
        public async Task<ActionResult> StoreReceivedItem(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_PO = new VmMkt_PO();

            await Task.Run(() => VmMkt_PO.GetStoreReceivedItem(id.Value));


            return View(VmMkt_PO);
        }
        [HttpGet]
        public async Task<ActionResult> VmMkt_POSlave_ReceivingEdit(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_PO = new VmMkt_PO();
            await Task.Run(() => VmMkt_PO.SelectSingleReceiving(id.Value));

            if (VmMkt_PO.Mkt_PO == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_PO);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_POSlave_ReceivingEdit(VmMkt_PO vmMktPO)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                vmMktPO.Mkt_PO.LastEditeddBy = user.User_User.ID;

                await Task.Run(() => vmMktPO.EditReceiving(0));
                return RedirectToAction("StoreReceivedItem", "Store");
            }
            return RedirectToAction("StoreReceivedItem", "Store");
        }

        public ActionResult VmMkt_POSlave_ReceivingDelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Mkt_POSlave_Receiving x = new Mkt_POSlave_Receiving();
            x.ID = id;
            x.Delete(0);
            return RedirectToAction("StoreReceivedItem", "Store");
        }



       
        public async Task<ActionResult> VmProd_MachineDIAIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_MachineDIA = new VmProd_MachineDIA();
            await Task.Run(() => VmProd_MachineDIA.InitialDataLoad());
            return View(VmProd_MachineDIA);
        }
        public ActionResult VmProd_MachineDIACreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_MachineDIACreate(VmProd_MachineDIA x)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => x.Add(UserID));
                return RedirectToAction("VmProd_MachineDIAIndex");
            }

            return RedirectToAction("VmProd_MachineDIAIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_MachineDIAEdit(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmProd_MachineDIA = new VmProd_MachineDIA();
            await Task.Run(() => VmProd_MachineDIA.SelectSingle(id));

            if (VmProd_MachineDIA.Prod_MachineDIA == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_MachineDIA);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_MachineDIAEdit(VmProd_MachineDIA VmProd_MachineDIA)
        {
            if (ModelState.IsValid)
            {
               
                await Task.Run(() => VmProd_MachineDIA.Edit(0));
                return RedirectToAction("VmProd_MachineDIAIndex");
            }
            return RedirectToAction("VmProd_MachineDIAIndex");
        }
        public ActionResult VmProd_MachineDIADelete(int id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_MachineDIA b = new Prod_MachineDIA();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmProd_MachineDIAIndex");
        }
    }
}
