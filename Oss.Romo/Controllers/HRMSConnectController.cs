﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Oss.Romo.Models;
using Oss.Romo.Models.Entity.HRMS;
using Oss.Romo.Models.Services;
using Oss.Romo.ViewModels.User;

namespace Oss.Romo.Controllers
{
    public class HRMSConnectController : Controller
    {
        CurrentUser user;
        public HRMSConnectController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
           // this.user = sessionHandler.CurrentUser;
        }

        // GET: Connect
        xOssContext db = new xOssContext();
        public async Task<ActionResult> Index()
        {

            using (var webClient = new System.Net.WebClient())
            {
                var lastSourceID = db.CheckInOut.Select(x => x.SourceId).Max();
                bool reTry = true;
                while (reTry)
                {
                    try
                    {
                        var json = webClient.DownloadString("http://49.0.35.196/TempHRM/HRM/GetData/" + lastSourceID);
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        List<CheckInOut> checkInOut = jsonSerializer.Deserialize<List<CheckInOut>>(json);

                        foreach (var v in checkInOut)
                        {
                            v.SourceId = v.ID;
                            v.CheckTime = v.CheckTime.AddHours(6);
                            db.CheckInOut.Add(v);

                        }
                        reTry = checkInOut[0].Retry;
                        await db.SaveChangesAsync();
                        if (reTry)
                        {
                            lastSourceID = db.CheckInOut.Select(x => x.SourceId).Max();

                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
            }
            return RedirectToAction("Index", "HRMSEmployeeRecord");
            // return RedirectToAction("Index", "EmployeeRecord");

        }
    }
}