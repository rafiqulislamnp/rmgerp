﻿using Oss.Romo.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Oss.Romo.ViewModels.User;

namespace Oss.Romo.Controllers
{
    public class DemoController : ApiController
    {

        public DemoController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }

        //[ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        [HttpGet]
        [ActionName("GetUser_User")]
        public System.Collections.Generic.List<User_User> GetUser_User()
        {
            //return db.User_User;

            return new System.Collections.Generic.List<User_User>
            {
                new User_User
                {
                    Name="Anb"
                }
            };

        }


    }
}
