﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.ViewModels.Common;
using System.Threading.Tasks;
using Oss.Romo.Models.Common;
using System.Net;
using Oss.Romo.Models;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.Models.Marketing;
using Oss.Romo.ViewModels.Planning;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.User;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using Oss.Romo.ViewModels.Account;
using Oss.Romo.ViewModels.Home;
using Oss.Romo.ViewModels.User;
using Oss.Romo.ViewModels.Reports;
using System.Data;

namespace Oss.Romo.Controllers
{
    [SoftwareAdmin]
    public class CommonController : Controller
    {
        private xOssContext db = new xOssContext();
        VmForDropDown VmForDropDown;
        VmCommon_Unit vmCommon_Unit;
        VmCommon_Status VmCommon_Status;
        VmCommon_Currency vmCommon_Currency;
        VmCommon_User vmCommon_User;
        VmCommon_Supplier vmCommon_Supplier;
        VmCommon_Permission vmCommon_Permission;
        VmCommon_Roles VmCommon_Roles;
        VmCommon_Method VmCommon_Method;
        VmCommon_TheOrder VmCommon_TheOrder;
        VmMkt_BOM VmMkt_BOM;
        private VmMkt_YarnType VmMkt_YarnType;
        VmOrderSummary VmOrderSummary;
        VmPlan_OrderLine VmPlan_OrderLine;
        PackingItem vmPackingItem;
        private VmCommon_CompanyBrunch VmCommon_CompanyBrunch;
        VmMkt_OrderDeliverySchedule VmMkt_OrderDeliverySchedule;
        VmMkt_OrderColorAndSizeRatio VmMkt_OrderColorAndSizeRatio;
      
        Int32 UserID = 0;
        // GET: Common
        //public ActionResult Common_Unit()
        //{
        //    VmCommon_Unit VmCommon_Unit = new VmCommon_Unit();
        //    return View(VmCommon_Unit);
        //}
        VmControllerHelper VmControllerHelper;

        public CommonController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }

        public ActionResult ModelError()
        {

            return View();
        }
        #region unit
        public async Task<ActionResult> Common_Unit()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            vmCommon_Unit = new VmCommon_Unit();
            await Task.Run(() => vmCommon_Unit.InitialDataLoad());

            return View(vmCommon_Unit);
        }

        public ActionResult Common_UnitCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Common_UnitCreate(VmCommon_Unit vmCommon_Unit)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => vmCommon_Unit.Add(UserID));
                return RedirectToAction("Common_Unit");
            }

            return RedirectToAction("Common_Unit");
        }


        [HttpGet]
        public async Task<ActionResult> Common_UnitEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vmCommon_Unit = new VmCommon_Unit();
            await Task.Run(() => vmCommon_Unit.SelectSingle(id));

            if (vmCommon_Unit.Common_Unit == null)
            {
                return HttpNotFound();
            }
            return View(vmCommon_Unit);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Common_UnitEdit(Common_Unit common_Unit)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];

            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => common_Unit.Edit(0));
                return RedirectToAction("Common_Unit");
            }
            return View(common_Unit);
        }
        public ActionResult VmCommon_UnitDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int iD = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            Common_Unit b = new Common_Unit();
            b.ID = iD;
            b.Delete(0);
            return RedirectToAction("Common_Unit");
        }
        #endregion

        
        public async Task<ActionResult> Common_Currency()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            vmCommon_Currency = new VmCommon_Currency();
            await Task.Run(() => vmCommon_Currency.InitialDataLoad());
            Common_CurrencyCreateEdit();
            return View(vmCommon_Currency);
        }

        public ActionResult Common_CurrencyCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Common_CurrencyCreate(VmCommon_Currency vmCommon_Currency)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => vmCommon_Currency.Add(UserID));
                return RedirectToAction("Common_Currency");
            }

            return RedirectToAction("Common_Currency");
        }

        [HttpGet]
        public async Task<ActionResult> Common_CurrencyEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vmCommon_Currency = new VmCommon_Currency();
            await Task.Run(() => vmCommon_Currency.SelectSingle(id));

            if (vmCommon_Currency.Common_Currency == null)
            {
                return HttpNotFound();
            }
            return View(vmCommon_Currency);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Common_CurrencyEdit(Common_Currency common_Currency)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => common_Currency.Edit(0));
                return RedirectToAction("Common_Currency");
            }
            return View(vmCommon_Currency);
        }


        public ActionResult Common_CurrencyCreateEdit(string id = null)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return PartialView();
            }
            else
            {
                vmCommon_Currency = new VmCommon_Currency();
                vmCommon_Currency.SelectSingle(id);

                if (vmCommon_Currency.Common_Currency == null)
                {
                    return HttpNotFound();
                }
                return PartialView(vmCommon_Currency);
            }

        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Common_CurrencyCreateEdit(VmCommon_Currency vmCommon_Currency)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                if (vmCommon_Currency.Common_Currency.ID == 0)
                {
                    await Task.Run(() => vmCommon_Currency.Add(UserID));
                    return RedirectToAction("Common_Currency");
                }
                else
                {
                    await Task.Run(() => vmCommon_Currency.Edit(0));
                    return RedirectToAction("Common_Currency");
                }

            }
            return View(vmCommon_Currency);
        }

        public ActionResult VmCommon_CurrencyDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int iD = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            Common_Currency b = new Common_Currency();
            b.ID = iD;
            b.Delete(0);
            return RedirectToAction("Common_Currency");
        }

        public async Task<ActionResult> VmCommon_SupplierIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            vmCommon_Supplier = new VmCommon_Supplier();
            await Task.Run(() => vmCommon_Supplier.InitialDataLoad());
            return View(vmCommon_Supplier);
        }

        public ActionResult VmCommon_SupplierCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Country = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetAccountSupplierHeadForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Chart2Head = sl2;


            VmForDropDown.DDownData = VmForDropDown.GetCommonSupplierTypeDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CommonSupplierType = sl3;

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommon_SupplierCreate(VmCommon_Supplier vmCommon_Supplier)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                // chart of account integration
                try
                {
                    VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                    {
                        UserId = UserID,
                        AccountName = vmCommon_Supplier.Common_Supplier.Name,
                        Chart2Id = vmCommon_Supplier.Acc_Chart2IDFk
                    };
                    vmCommon_Supplier.Common_Supplier.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.AddFromIntegratedModule());

                }
                catch (Exception e)
                {
                    vmCommon_Supplier.Common_Supplier.Acc_AcNameFk = 0;
                }
               
                await Task.Run(() => vmCommon_Supplier.Add(UserID));
                return RedirectToAction("VmCommon_SupplierIndex");
            }

            return RedirectToAction("VmCommon_SupplierIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmCommon_SupplierEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetCountryForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Country = sl1;


            VmForDropDown.DDownData = VmForDropDown.GetAccountSupplierHeadForDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Chart2Head = sl2;

            VmForDropDown.DDownData = VmForDropDown.GetCommonSupplierTypeDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.CommonSupplierType = sl3;

            vmCommon_Supplier = new VmCommon_Supplier();
            await Task.Run(() => vmCommon_Supplier.SelectSingle(id));

            var accAcName = db.Acc_AcName.Find(vmCommon_Supplier.Common_Supplier.Acc_AcNameFk);
            if (accAcName?.Acc_Chart2FK != null) vmCommon_Supplier.Acc_Chart2IDFk = (int)accAcName.Acc_Chart2FK;

            if (vmCommon_Supplier.Common_Supplier == null)
            {
                return HttpNotFound();
            }

            return View(vmCommon_Supplier);
          }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommon_SupplierEdit(VmCommon_Supplier vmCommon_Supplier)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                VmAcc_IntegrationAccountHead vmAccIntegrationAccountHead = new VmAcc_IntegrationAccountHead
                {
                    UserId = UserID,
                    AccountName = vmCommon_Supplier.Common_Supplier.Name,
                    Chart2Id = vmCommon_Supplier.Acc_Chart2IDFk,
                    ExistAccId = vmCommon_Supplier.Common_Supplier.Acc_AcNameFk
                };

                vmCommon_Supplier.Common_Supplier.Acc_AcNameFk = await Task.Run(() => vmAccIntegrationAccountHead.EditFromIntegratedModule());

                await Task.Run(() => vmCommon_Supplier.Edit(0));
                return RedirectToAction("VmCommon_SupplierIndex");
            }
            return View(vmCommon_Supplier);
        }
        public ActionResult VmCommon_SupplierDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int sId = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out sId);
            Common_Supplier b = new Common_Supplier();
            b.ID = sId;
            b.Delete(0);
            return RedirectToAction("VmCommon_SupplierIndex");
        }

        ///////////////////--------------Common_TheOrder---------------------/////////////////
        public List<object> GetBuyer()
        {
            var BuyerList = new List<object>();

            foreach (var buyer in db.Mkt_Buyer.Where(d => d.Active == true))
            {
                BuyerList.Add(new { Text = buyer.Name, Value = buyer.ID });
            }
            return BuyerList;
        }
        public List<object> GetCurrency()
        {
            var CurrencyList = new List<object>();

            foreach (var buyer in db.Common_Currency.Where(d => d.Active == true))
            {
                CurrencyList.Add(new { Text = buyer.Name, Value = buyer.ID });
            }
            return CurrencyList;
        }
        public async Task<ActionResult> ChangeOrderStatus(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommon_TheOrder o = new VmCommon_TheOrder(); 
             //await Task.Run(() => o.ChangeOrderStatus(id));
            await Task.Run(() => o.OrderCloss(id));
            return RedirectToAction("(");
        }


        //------------Common_TheOrder -----------------///
        public async Task<ActionResult> VmCommon_TheOrderIndex(int? id)
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool flag = false;
            //TempData["IsComplete"] = false;
            VmCommon_TheOrder = new VmCommon_TheOrder();
            VmCommon_TheOrder.Header = "Order List(In Progress):";
            VmCommon_TheOrder.ButtonName = "Close";
            if (id != null)
            {
                flag = true;
                VmCommon_TheOrder.Header = "Order List(Complete):";
                VmCommon_TheOrder.ButtonName = "ReOpen";
            }
            //-------
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmCommon_TheOrder.User_User = vMUser.User_User;
            await Task.Run(() => VmCommon_TheOrder.InitialDataLoad(flag));
            return View(VmCommon_TheOrder);
        }
        [HttpPost]
        public async Task<ActionResult> VmCommon_TheOrderIndex(VmCommon_TheOrder VmCommon_TheOrder, int? x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string SearchString = VmCommon_TheOrder.SearchString;

            VmCommon_TheOrder = new VmCommon_TheOrder();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmCommon_TheOrder.User_User = vMUser.User_User;


            string ButtonName = VmCommon_TheOrder.ButtonName;
            bool flag = false;




            VmCommon_TheOrder.Header = "Order List(In Progress):";
            VmCommon_TheOrder.ButtonName = "Close";
            if (ButtonName != "Close")
            {
                flag = true;
                VmCommon_TheOrder.Header = "Order List(Complete):";
                VmCommon_TheOrder.ButtonName = "ReOpen";
            }


            await Task.Run(() => VmCommon_TheOrder.InitialSearchLoad(flag, SearchString));
            return View(VmCommon_TheOrder);
        }

        public async Task<ActionResult> VmInventoryOrderIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool flag = false;
            VmCommon_TheOrder = new VmCommon_TheOrder();
            VmCommon_TheOrder.Header = "Order List(In Progress):";
            VmCommon_TheOrder.ButtonName = "Close";
            if (id != null)
            {
                flag = true;
                VmCommon_TheOrder.Header = "Order List(Complete):";
                VmCommon_TheOrder.ButtonName = "ReOpen";
            }


            await Task.Run(() => VmCommon_TheOrder.InitialDataLoad(flag));
            return View(VmCommon_TheOrder);
        }
        [HttpPost]
        public async Task<ActionResult> VmInventoryOrderIndex(VmCommon_TheOrder VmCommon_TheOrder, int? x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string SearchString = VmCommon_TheOrder.SearchString;
            string ButtonName = VmCommon_TheOrder.ButtonName;
            bool flag = false;

            VmCommon_TheOrder = new VmCommon_TheOrder();
            VmCommon_TheOrder.Header = "Order List(In Progress):";
            VmCommon_TheOrder.ButtonName = "Close";
            if (ButtonName != "Close")
            {
                flag = true;
                VmCommon_TheOrder.Header = "Order List(Complete):";
                VmCommon_TheOrder.ButtonName = "ReOpen";
            }


            await Task.Run(() => VmCommon_TheOrder.InitialSearchLoad(flag, SearchString));
            return View(VmCommon_TheOrder);
        }


        public ActionResult ShipmentPackingItem(string Id, int OId)
        {
            VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
            if (Id != null)
            {
                int PId = Convert.ToInt32(Id);
                VmMkt_OrderDeliverySchedule.GetPackingDetails(PId, OId);
            }
            return PartialView("ShipmentPackingItem", VmMkt_OrderDeliverySchedule);
        }

        public ActionResult VmCommon_TheOrderCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewModels.Common.VmCommon_TheOrder VmCommon_TheOrder = new VmCommon_TheOrder();
            VmCommon_TheOrder.Common_TheOrder = new Common_TheOrder();




            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Buyer = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;
            VmCommon_TheOrder.Common_TheOrder.OrderDate = DateTime.Today;
            return View(VmCommon_TheOrder);
        }
        //public List<object> GetTempSupplier()
        //{
        //    var supplierList = new List<object>();

        //    foreach (var supplier in db.Common_Supplier.Where(d => d.Active == true))
        //    {
        //        supplierList.Add(new { Text = supplier.Name, Value = supplier.ID });
        //    }
        //    return supplierList;
        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommon_TheOrderCreate(VmCommon_TheOrder VmCommon_TheOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper vmControllerHelper = new VmControllerHelper();

            VmCommon_TheOrder.Common_TheOrder.FirstCreatedBy = vmControllerHelper.GetCurrentUser().User_User.ID;

            if (ModelState.IsValid)
            {
                await Task.Run(() => VmCommon_TheOrder.Add(UserID));
                return RedirectToAction("VmCommon_TheOrderIndex");
            }

            return RedirectToAction("VmCommon_TheOrderIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmCommon_TheOrderEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmCommon_TheOrder = new VmCommon_TheOrder();
            await Task.Run(() => VmCommon_TheOrder.SelectSingle(id));

            if (VmCommon_TheOrder.Common_TheOrder == null)
            {
                return HttpNotFound();
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();

            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Buyer = sl1;
            VmForDropDown.DDownData = VmForDropDown.GetCurrencyForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Currency = sl3;
            return View(VmCommon_TheOrder);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommon_TheOrderEdit(VmCommon_TheOrder VmCommon_TheOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper vmControllerHelper = new VmControllerHelper();

            VmCommon_TheOrder.Common_TheOrder.LastEditeddBy = vmControllerHelper.GetCurrentUser().User_User.ID;
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmCommon_TheOrder.Edit(0));
                return RedirectToAction("VmCommon_TheOrderIndex");
            }
            return View(VmCommon_TheOrder);
        }
        public ActionResult VmCommon_TheOrderDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int iD = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            Common_TheOrder b = new Common_TheOrder();
            b.ID = iD;
            b.Delete(0);
            return RedirectToAction("VmCommon_TheOrderIndex");
        }
        //----------------//////////-------------//
        [HttpGet]
        public async Task<ActionResult> Mkt_OrderSlaveBOM(string id,bool error=false)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id != null)
            {
                VmCommon_TheOrder = new VmCommon_TheOrder();
                VmCommon_TheOrder.Common_TheOrder = new Common_TheOrder();
                await Task.Run(() => VmCommon_TheOrder = VmCommon_TheOrder.SelectSingleJoined(id));
                Mkt_BOM a = new Mkt_BOM();
                a.Common_TheOrderFk = VmCommon_TheOrder.Common_TheOrder.ID;
                
                VmCommon_TheOrder.VmMkt_BOM = new VmMkt_BOM();
                await Task.Run(() => VmCommon_TheOrder.VmMkt_BOM.GetBOM(id));

                int id1 = Convert.ToInt32(id);
                VmMkt_OrderDeliverySchedule = new VmMkt_OrderDeliverySchedule();
                await Task.Run(() => VmMkt_OrderDeliverySchedule.OrderDeliveryScheduleDataLoad(id1));

                VmCommon_TheOrder.VmMkt_OrderDeliverySchedule = VmMkt_OrderDeliverySchedule;

                VmMkt_OrderColorAndSizeRatio = new VmMkt_OrderColorAndSizeRatio();
                await Task.Run(() => VmMkt_OrderColorAndSizeRatio.OrderColorAndSizeRatioDataLoad(id1));
                VmCommon_TheOrder.VmMkt_OrderColorAndSizeRatio = VmMkt_OrderColorAndSizeRatio;

                ViewData["AllData"] = VmCommon_TheOrder.VmMkt_OrderColorAndSizeRatio.GetColorAndSizeDimension(id1);
              
                if (error)
                {
                    VmCommon_TheOrder.ErrorMessage = "Delivery Schedule Quantity Must be greater then Order";
                }
            }
            return View(VmCommon_TheOrder);
        }
        //Those Method used for cascading Dropdown
        //[HttpGet]
        public JsonResult GetRaw_SubCategory(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Raw_SubCategory.Where(p => p.Raw_CategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        public JsonResult GetMkt_SubCategory(int? id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_SubCategory.Where(p => p.Mkt_CategoryFK == id).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        public JsonResult GetMkt_Item(int? ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Mkt_Item.Where(p => p.Mkt_SubCategoryFK == ID).Where(x => x.Active == true), JsonRequestBehavior.AllowGet);
        }
        //-------//////////////////////////////--------//
        public async Task<ActionResult> VmPlan_OrderLineIndex(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_BOM = new VmMkt_BOM();
            await Task.Run(() => VmMkt_BOM = VmMkt_BOM.SelectSingleJoined(id));
            Plan_OrderLine pol = new Plan_OrderLine();
            int? iD = pol.Mkt_BOMFK; //pol.Mkt_BOMFK =id;
            string s = iD.ToString();// Check next time mamun in encryption parpucs
            s = id;
            VmMkt_BOM.VmPlan_OrderLine = new VmPlan_OrderLine();
            await Task.Run(() => VmMkt_BOM.VmPlan_OrderLine.GetBOMOrderLine(id));
            return View(VmMkt_BOM);
        }


        //..........OrderReportDoc.......//
        public ActionResult OrderReportDoc(string id)
        {
            if (id == null)
            {
                IdNullDivertion("OrderReportDoc", "Id is Null");
            }
            try
            {
                VmCommon_TheOrder b = new VmCommon_TheOrder();
                b.Common_TheOrder = new Common_TheOrder();
                b.SelectSingle(id);
                b.OrderInventoryReportDocLoad(id);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/OrderBaseDoc.rpt");
                cr.Load();
                cr.SetDataSource(b.OrderReportDoc);
                cr.SummaryInfo.ReportTitle = "Inventory of " + b.Common_TheOrder.CID + " Order";

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("OrderReportDoc", ex.Message);
            }
            return null;
        }
        //......................Closing Report For Order Raw Inventtory.............//
        public ActionResult ClosingRawInventory(string id)
        {


            if (id == null)
            {
                IdNullDivertion("Raw Inventtory", "Id is Null");
            }
            try
            {
                VmCommon_TheOrder b = new VmCommon_TheOrder();
                b.Common_TheOrder = new Common_TheOrder();
                b.SelectSingle(id);
                b.RawInventoryReport(id);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/OrderRawInventory.rpt");
                cr.Load();
                cr.SetDataSource(b.OrderReportDoc);
                cr.SummaryInfo.ReportTitle = "Closing Inventory of " + b.Common_TheOrder.CID + " Order";

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("OrderReportDoc", ex.Message);
            }
            return null;
        }
        public ActionResult POInventoryReport(int? id)
        {
            if (!id.HasValue)
            {
                IdNullDivertion("POInventoryReport", "Id is Null");
            }
            try
            {
                VmCommon_TheOrder b = new VmCommon_TheOrder();
                VmMkt_PO x = new VmMkt_PO();
                x.Mkt_PO = new Mkt_PO();
                x.SelectSingle(id.Value);
                b.POInventoryReportDocLoad(id.Value);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/POInventoryBaseDoc.rpt");
                cr.Load();
                cr.SetDataSource(b.OrderReportDoc);
                cr.SummaryInfo.ReportTitle = "Inventory of " + x.Mkt_PO.CID + " Purchase Order";
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("POInventoryReport", ex.Message);
            }
            return null;
        }

        private ActionResult IdNullDivertion(string methodName, string messege)
        {
            ErrorReports er = new ErrorReports();
            er.ControllerName = this.ToString();
            er.MethodName = methodName;
            er.ErrorDescription = "Id is Null";
            er.EmailErrorAuto();
            return View("../Home/ErrorView", er);
        }


        //...........Order Summary View...........//


        public async Task<ActionResult> OrderSummaryViewIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool flag = false;
            VmOrderSummary = new VmOrderSummary();

            //-------
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            //---------------
            VmOrderSummary.User_User = vMUser.User_User;
            VmOrderSummary.Mkt_PO = new Mkt_PO();

            await Task.Run(() => VmOrderSummary.InitialDataLoaForOrderSummary(flag));

            return View(VmOrderSummary);
        }
        [HttpPost]
        public async Task<ActionResult> OrderSummaryViewIndex(VmCommon_TheOrder VmCommon_TheOrder, int? x)
        {
            string SearchString = VmCommon_TheOrder.SearchString;

            VmCommon_TheOrder = new VmCommon_TheOrder();
            VmControllerHelper VmControllerHelper = new VmControllerHelper();
            VmUser_User vMUser = VmControllerHelper.GetCurrentUser();
            VmCommon_TheOrder.User_User = vMUser.User_User;


            string ButtonName = VmCommon_TheOrder.ButtonName;
            bool flag = false;



            await Task.Run(() => VmCommon_TheOrder.InitialSearchLoad(flag, SearchString));
            return View(VmCommon_TheOrder);
        }


        public ActionResult MarchandierOrderViewReport(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            VmCommon_TheOrder x = new VmCommon_TheOrder();
            x.Common_TheOrder = new Common_TheOrder();
            x.User_User = new User_User();
            x.FromDate = DateTime.Now.AddMonths(-3);
            x.ToDate = DateTime.Now;
            //x.Common_TheOrder.ID= id.Value;

            return View(x);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MarchandierOrderViewReport(VmCommon_TheOrder x)
        {
            VmControllerHelper vmControllerHelper = new VmControllerHelper();


            UserID = vmControllerHelper.GetCurrentUser().User_User.ID;
            x.Common_TheOrder = new Common_TheOrder();
            x.Common_TheOrder.FirstCreatedBy = UserID;

            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/TotalOrderReport.rpt");
            cr.Load();

            x.MarchandierOrderView(UserID);
            cr.SetDataSource(x.OrderReportDoc);
            Stream stream =
            cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        public ActionResult InitialInProcessReport(int? id)
        {
            if (!id.HasValue)
            {
                IdNullDivertion("InitialInProcessReport", "Id is Null");
            }
            try
            {
                VmCommon_TheOrder b = new VmCommon_TheOrder();
                //b.IntailInProcessReport(id.Value);

                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/InitialInProcessReport.rpt");

                cr.Load();
                cr.SetDataSource(b.OrderReportDoc);
                //cr.SummaryInfo.ReportTitle = "Inventory of Invoice No" + b.Commercial_Invoice.InvoiceNo;
                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("InitialInProcessReport", ex.Message);
            }
            return null;
        }


        //public ActionResult ItemwiseLeftoverInventory(string id)
        //{


        //    if (id == null)
        //    {
        //        IdNullDivertion("Raw Inventtory", "Id is Null");
        //    }
        //    try
        //    {
        //        VmCommon_TheOrder b = new VmCommon_TheOrder();
        //        b.Common_TheOrder = new Common_TheOrder();
        //        b.SelectSingle(id);
        //        b.RawInventoryReport(id);
        //        ReportClass cr = new ReportClass();
        //        cr.FileName = Server.MapPath("~/Views/Reports/ItemWiseLeftoverInventory.rpt");
        //        cr.Load();
        //        cr.SetDataSource(b.OrderReportDoc);
        //        cr.SummaryInfo.ReportTitle = "Closing Inventory of " + b.Common_TheOrder.CID + " Order";

        //        Stream stream =
        //            cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //        return File(stream, "application/pdf");

        //    }
        //    catch (Exception ex)
        //    {
        //        IdNullDivertion("OrderReportDoc", ex.Message);
        //    }
        //    return null;
        //}



        //public ActionResult OrderwiseLeftoverInventory(string id)
        //{


        //    if (id == null)
        //    {
        //        IdNullDivertion("Raw Inventtory", "Id is Null");
        //    }
        //    try
        //    {
        //        VmCommon_TheOrder b = new VmCommon_TheOrder();
        //        b.Common_TheOrder = new Common_TheOrder();
        //        b.SelectSingle(id);
        //        b.RawInventoryReport(id);
        //        ReportClass cr = new ReportClass();
        //        cr.FileName = Server.MapPath("~/Views/Reports/OrderWiseLeftoverInventory.rpt");
        //        cr.Load();
        //        cr.SetDataSource(b.OrderReportDoc);
        //        cr.SummaryInfo.ReportTitle = "Closing Inventory of " + b.Common_TheOrder.CID + " Order";

        //        Stream stream =
        //            cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //        return File(stream, "application/pdf");

        //    }
        //    catch (Exception ex)
        //    {
        //        IdNullDivertion("OrderReportDoc", ex.Message);
        //    }
        //    return null;



        //..........OrderReportInProgrseeDoc.......//
        public ActionResult OrderInProgressReportDoc(string id)
        {
            if (id == null)
            {
                IdNullDivertion("OrderReportDoc", "Id is Null");
            }
            try
            {
                VmCommon_TheOrder b = new VmCommon_TheOrder();
                b.Common_TheOrder = new Common_TheOrder();
                b.SelectSingle(id);
                b.OrderInProgressInventoryReportDocLoad(id);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/OrderInProgress.rpt");
                cr.Load();
                cr.SetDataSource(b.OrderReportDoc);
                cr.SummaryInfo.ReportTitle = "Inventory of " + b.Common_TheOrder.CID + " Order";

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("OrderReportDoc", ex.Message);
            }
            return null;
        }

        //......................Order Left Over   Inventory Report   .............//
        public ActionResult ClosingOrderInventory(string id)
        {


            if (id == null)
            {
                IdNullDivertion("Order Left Over", "Id is Null");
            }
            try
            {
                VmCommon_TheOrder b = new VmCommon_TheOrder();
                b.Common_TheOrder = new Common_TheOrder();
                b.SelectSingle(id);
                b.OrderLeftOverInventoryReport(id);
                ReportClass cr = new ReportClass();
                cr.FileName = Server.MapPath("~/Views/Reports/OrderLeftoverInventory.rpt");
                cr.Load();
                cr.SetDataSource(b.OrderReportDoc);
                cr.SummaryInfo.ReportTitle = "Closing Inventory of " + b.Common_TheOrder.CID + " Order";

                Stream stream =
                    cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");

            }
            catch (Exception ex)
            {
                IdNullDivertion("OrderReportDoc", ex.Message);
            }
            return null;
        }



        #region Company Brunch
        public async Task<ActionResult> VmCommon_CompanyBrunchIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommon_CompanyBrunch = new VmCommon_CompanyBrunch();
            await Task.Run(() => VmCommon_CompanyBrunch.InitialDataLoad());
            return View(VmCommon_CompanyBrunch);
        }

        public ActionResult VmCommon_CompanyBrunchCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommon_CompanyBrunch = new VmCommon_CompanyBrunch();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommon_CompanyBrunchCreate(VmCommon_CompanyBrunch a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmCommon_CompanyBrunchIndex");
            }
            return RedirectToAction("VmCommon_CompanyBrunchIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmCommon_CompanyBrunchEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmCommon_CompanyBrunch = new VmCommon_CompanyBrunch();
            await Task.Run(() => VmCommon_CompanyBrunch.SelectSingle(id.Value));

            if (VmCommon_CompanyBrunch.Common_CompanyBrunch == null)
            {
                return HttpNotFound();
            }
            return View(VmCommon_CompanyBrunch);
        }
        // To update Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommon_CompanyBrunchEdit(Common_CompanyBrunch Common_CompanyBrunch)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => Common_CompanyBrunch.Edit(0));
                return RedirectToAction("VmCommon_CompanyBrunchIndex");
            }
            return View(Common_CompanyBrunch);
        }
        public ActionResult VmCommon_CompanyBrunchDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommon_CompanyBrunch b = new VmCommon_CompanyBrunch();
            b.Common_CompanyBrunch.ID = id;
            b.Delete(0);
            return RedirectToAction("VmCommon_CompanyBrunchIndex");
        }

        #endregion

        public async Task<ActionResult> VmExportSummary()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmCommon_TheOrder = new VmCommon_TheOrder();
            await Task.Run(() => VmCommon_TheOrder.OrderSummaryReport());

            return View(VmCommon_TheOrder);
        }

        #region Status

        public async Task<ActionResult> Common_Status()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmCommon_Status = new VmCommon_Status();
            await Task.Run(() => VmCommon_Status.InitialDataLoad());

            return View(VmCommon_Status);
        }

        public ActionResult VmCommon_StatusCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommon_StatusCreate(VmCommon_Status VmCommon_Status)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmCommon_Status.Add(UserID));
                return RedirectToAction("Common_Status");
            }

            return RedirectToAction("Common_Status");
        }
        
        [HttpGet]
        public async Task<ActionResult> VmCommon_StatusEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmCommon_Status = new VmCommon_Status();
            await Task.Run(() => VmCommon_Status.SelectSingle(id.Value));

            if (VmCommon_Status.Common_Status == null)
            {
                return HttpNotFound();
            }
            return View(VmCommon_Status);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmCommon_StatusEdit(VmCommon_Status VmCommon_Status)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];

            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmCommon_Status.Edit(0));
                return RedirectToAction("Common_Status");
            }
            return View(VmCommon_Status);
        }

        public ActionResult VmCommon_StatusDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int iD = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);

            Common_Status b = new Common_Status();
            b.ID = iD;
            b.Delete(0);

            return RedirectToAction("Common_Status");
        }

        #endregion

        #region Yarn Type
        public async Task<ActionResult> VmMkt_YarnTypeIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_YarnType = new VmMkt_YarnType();
            await Task.Run(() => VmMkt_YarnType.InitialDataLoad());

            return View(VmMkt_YarnType);
        }

        public ActionResult VmMkt_YarnTypeCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_YarnTypeCreate(VmMkt_YarnType VmMkt_YarnType)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmMkt_YarnType.Add(UserID));
                return RedirectToAction("VmMkt_YarnTypeIndex");
            }

            return RedirectToAction("VmMkt_YarnTypeIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmMkt_YarnTypeEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmMkt_YarnType = new VmMkt_YarnType();
            await Task.Run(() => VmMkt_YarnType.SelectSingle(id));

            if (VmMkt_YarnType.Mkt_YarnType == null)
            {
                return HttpNotFound();
            }
            return View(VmMkt_YarnType);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmMkt_YarnTypeEdit(VmMkt_YarnType VmMkt_YarnType)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];

            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmMkt_YarnType.Edit(0));
                return RedirectToAction("VmMkt_YarnTypeIndex");
            }
            return RedirectToAction("VmMkt_YarnTypeIndex");
        }
        public ActionResult VmMkt_YarnTypeDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int iD = 0;
            VmControllerHelper = new VmControllerHelper();
            id = VmControllerHelper.Decrypt(id);
            Int32.TryParse(id, out iD);
            Mkt_YarnType b = new Mkt_YarnType();
            b.ID = iD;
            b.Delete(0);
            return RedirectToAction("VmMkt_YarnTypeIndex");
        }
        #endregion


        [HttpGet]
        public ActionResult YarnStatementledgerExcel()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmMkt_BOM vmMktBom = new VmMkt_BOM();
            vmMktBom.FromDate = DateTime.Now.AddDays(-30);
            vmMktBom.ToDate = DateTime.Now;
            return View(vmMktBom);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> YarnStatementledgerExcel(VmMkt_BOM model)
        {
            
            DataTable table = new DataTable();
            var filename = string.Empty;

          
                filename = "Yarn Statement Ledger";
                table = model.GetYarnStatementledgerExcel(model.FromDate, model.ToDate);
            //}
            //else
            //{
            //    return Content("No One");
            //}
            GenerateReport(table, filename);

            return RedirectToAction("AllReport");
        }
       
        private void GenerateReport(DataTable table, string filename)
        {
            string attachment = "attachment; filename=" + "" + filename + ".xls";


            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            Response.Write(@"<!DOCTYPE html>");
            Response.AddHeader("content-disposition", attachment);

            Response.Charset = "utf-8";
            //Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            Response.Write("<br><br><br>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <tr>");
            //am getting my grid's column headers

            foreach (DataColumn dc in table.Columns)
            {      //write in new column
                Response.Write("<td style='background-color:#008080;color:#ffffff;font-size:14'>");
                //Get column headers  and make it as bold in excel columns
                Response.Write("<b>");
                Response.Write(dc.ColumnName);
                Response.Write("</b>");
                Response.Write("</td>");
            }
            Response.Write("</tr>");
            Response.Write("\n");
            string tab = "";

            foreach (DataRow row in table.Rows)
            {//write in new row
                int i = 0;
                Response.Write("<tr>");

                if (row[i].ToString() == "Total")
                {
                    for (i = 0; i < table.Columns.Count; i++)
                    {
                        Response.Write(tab);
                        Response.Write("<td style='background-color:#FFB6C1'><strong>");
                        Response.Write(row[i].ToString());
                        Response.Write("</strong></td>");
                        tab = "\t";
                    }
                }
                else
                {
                    for (i = 0; i < table.Columns.Count; i++)
                    {
                        Response.Write(tab);
                        Response.Write("<td>");
                        Response.Write(row[i].ToString());
                        Response.Write("</td>");
                        tab = "\t";
                        //if (i == 0)
                        //{
                        //    Response.Write("<td style='background-color:#90EE90'>");
                        //    Response.Write(row[i].ToString());
                        //    Response.Write("</td>");
                        //}
                    }
                }
                Response.Write("</tr>");
                Response.Write("\n");
            }
            Response.Write("</table>");
            Response.Write("</font>");
            Response.Flush();
            Response.End();




            ////////////////////////
            // Response.ClearContent();
            // Response.Buffer = true;
            // Response.AddHeader("content-disposition", attachment);
            // Response.ContentType = "application/ms-excel";

            // Response.ContentEncoding = System.Text.Encoding.Unicode;
            // Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            //// System.IO.StringWriter sw = new System.IO.StringWriter();
            //// System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);


            // string tab = "";

            // foreach (DataColumn dc in table.Columns)
            // {
            //     Response.Write(tab + dc.ColumnName);
            //     tab = "\t";
            // }

            // Response.Write("\n");
            // //int i;

            // foreach (DataRow dr in table.Rows)
            // {
            //     tab = "";
            //     for (i = 0; i < table.Columns.Count; i++)
            //     {
            //         Response.Write(tab + dr[i].ToString());
            //         tab = "\t";
            //     }
            //     Response.Write("\n");
            // }

            // Response.Flush();
            // Response.End();
        }
    }

}
