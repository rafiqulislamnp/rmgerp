﻿using Oss.Romo.ViewModels.Planning;
using Oss.Romo.Models;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

using Oss.Romo.ViewModels.User;

namespace Oss.Romo.Controllers
{
    [SoftwareAdmin]
    public class PlanningController : Controller
    {
        private xOssContext db = new xOssContext();
        VmForDropDown VmForDropDown;
        VmPlan_OrderLine VmPlan_OrderLine { get; set; }
        VmPlan_OrderStep VmPlan_OrderStep { get; set; }
        VmPlan_ProductionLine VmPlan_ProductionLine { get; set; }
        VmControllerHelper VmControllerHelper { get; set; }
        VmPlan_OrderProcessCategory VmPlan_OrderProcessCategory { get; set; }
        int UserID = 0;

        public PlanningController()
        {
            var sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }

        //...........Plan_OrderLine.........//
        public async Task<ActionResult> VmPlan_OrderLineIndex()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderLine = new VmPlan_OrderLine();
            await Task.Run(() => VmPlan_OrderLine.InitialDataLoad());
            return View(VmPlan_OrderLine);
        }
        [HttpGet]
        public async Task<ActionResult> VmPlan_OrderLineCreate()
        {

            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ////...OrderID Dropdown Implement...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();

            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.OrderId = sl1;
            ////...Plan_ProductionLine Dropdown Implement...///
            VmForDropDown.DDownData = VmForDropDown.GetProductionLine();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ProductionPlan = sl2;

            VmPlan_OrderLine = new VmPlan_OrderLine();
            await Task.Run(() => VmPlan_OrderLine.InitialDataLoad());
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> VmPlan_OrderStepEdit(int? id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmPlan_OrderStep = new VmPlan_OrderStep();
            await Task.Run(() => VmPlan_OrderStep.SelectSingle(id.Value));
            ////...OrderID Dropdown Implement...///
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOM = sl6;
            ////...Plan_ProductionLine Dropdown Implement...///
            var listOfstep = GetProductionStep();
            SelectList sl3 = new SelectList(listOfstep, "Value", "Text");
            ViewBag.ProductionStep = sl3;
            if (VmPlan_OrderStep.Plan_OrderStep == null)
            {
                return HttpNotFound();
            }
            return View(VmPlan_OrderStep);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_OrderStepEdit(VmPlan_OrderStep vmPlanOrderStep)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => vmPlanOrderStep.Edit(0));
                return RedirectToAction("VmPlan_OrderStepIndex");
            }
            return View(vmPlanOrderStep);
        }
        //[HttpGet]
        //public async Task<ActionResult> VmPlan_OrderLineCreate()
        //{

        //    ////...OrderID Dropdown Implement...///

        //    var listOfOrder = GetOrderId();
        //    SelectList sl1 = new SelectList(listOfOrder, "Value", "Text");
        //    ViewBag.OrderId = sl1;
        //    ////...Plan_ProductionLine Dropdown Implement...///
        //    var listOfProductionPlan = GetProductionLine();
        //    SelectList sl2 = new SelectList(listOfProductionPlan, "Value", "Text");
        //    ViewBag.ProductionPlan = sl2;

        //    VmPlan_OrderLine = new VmPlan_OrderLine();


        //    await Task.Run(() => VmPlan_OrderLine.InitialDataLoad());
        //    return View(VmMkt_BOM);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VmPlan_OrderLineCreate(VmPlan_OrderLine VmPlan_OrderLine)
        //{


        //    if (ModelState.IsValid)
        //    {

        //        await Task.Run(() => VmPlan_OrderLine.Add(UserID));
        //        return RedirectToAction("VmPlan_OrderLineIndex");
        //    }

        //    return RedirectToAction("VmPlan_OrderLineIndex");
        //}
        [HttpGet]
        public async Task<ActionResult> VmPlan_OrderLineEdit(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderLine = new VmPlan_OrderLine();
            if (id != null) await Task.Run(() => VmPlan_OrderLine.SelectSingle(id.Value));

            if (VmPlan_OrderLine.Plan_OrderLine == null)
            {
                return HttpNotFound();
            }
            return View(VmPlan_OrderLine);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_OrderLineEdit(VmPlan_OrderLine vmPlanOrderLine)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => vmPlanOrderLine.Edit(0));
                return RedirectToAction("VmPlan_OrderLineIndex");
            }
            return View(vmPlanOrderLine);
        }

        //....DeleteForPlan_OrderLine..//
        public ActionResult VmPlan_OrderLineDelete(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Plan_OrderLine a = new Plan_OrderLine();
            a.ID = id;
            a.Delete(0);
            return RedirectToAction("VmPlan_OrderLineIndex");
        }
        //.....OrderID Dropdown....//
        public List<object> GetOrderId()
        {
            var orderList = new List<object>();

            foreach (var order in db.Mkt_BOM.Where(x => x.Active == true))
            {
                orderList.Add(new { Text = order.CID, Value = order.ID });
            }
            return orderList;
        }
        //....Plan_ProductionLine Dropdown....//
        public List<object> GetProductionLine()
        {
            var lineList = new List<object>();

            foreach (var plan in db.Plan_ProductionLine.Where(x => x.Active == true))
            {
                lineList.Add(new { Text = plan.LineNumber, Value = plan.ID });
            }
            return lineList;
        }
        
        //......Plan_OrderStep....//
        
        public async Task<ActionResult> VmPlan_OrderStepIndex()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderStep = new VmPlan_OrderStep();
            await Task.Run(() => VmPlan_OrderStep.InitialDataLoad());
            return View(VmPlan_OrderStep);
        }

        public async Task<ActionResult> VmPlan_OrderForBOM(int? id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderStep = new VmPlan_OrderStep();
            if (id != null) await Task.Run(() => VmPlan_OrderStep.SelectSingleOrder(id.Value));
            Plan_OrderStep a = new Plan_OrderStep();
            a.CommonTheOrderFK = id;
            if (id != null) await Task.Run(() => VmPlan_OrderStep.PlanOrderStepDataLoad(id.Value));
            return View(VmPlan_OrderStep);
        }
        
        [HttpGet]
        public ActionResult VmPlan_OrderStepCreate(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetCommon_TheOrderForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.OrderId = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetProductionStepForDropDown();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ProductionStep = sl3;

            VmPlan_OrderStep vmPlanOrderStep = new VmPlan_OrderStep();
            vmPlanOrderStep.Plan_OrderStep = new Plan_OrderStep();

            vmPlanOrderStep.Plan_OrderStep.PlannedStartDate = DateTime.Today;
            vmPlanOrderStep.Plan_OrderStep.PlannedFinishDate = DateTime.Today;
            vmPlanOrderStep.Plan_OrderStep.ActualStartDate = DateTime.Today;
            vmPlanOrderStep.Plan_OrderStep.ActualFinishDate = DateTime.Today;

            return View(vmPlanOrderStep);
            //VmPlan_OrderStep = new VmPlan_OrderStep();
            //await Task.Run(() => VmPlan_OrderStep.InitialDataLoad());

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_OrderStepCreate(VmPlan_OrderStep vmPlanOrderStep)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {

                await Task.Run(() => vmPlanOrderStep.Add(UserID));
                return RedirectToAction("VmPlan_OrderStepIndex");
            }

            return RedirectToAction("VmPlan_OrderStepIndex");
        }
        
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VmPlan_OrderStepCreate(Plan_OrderStep Plan_OrderStep)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        Plan_OrderStep.AddReady(UserID);
        //        await Task.Run(() => Plan_OrderStep.Add());
        //        return RedirectToAction("VmPlan_OrderForBOM/" + Plan_OrderStep.Mkt_BOMFK);
        //    }

        //    return RedirectToAction("VmPlan_OrderForBOM/" + Plan_OrderStep.Mkt_BOMFK);
        //}


        //....DeleteForPlan_OrderStep..//

        public ActionResult VmPlan_OrderStepDelete(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Plan_OrderStep b = new Plan_OrderStep();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmPlan_OrderStepIndex");
        }
        //....Plan_OrderProcess Dropdown....//
        public List<object> GetProductionStep()
        {
            var stepList = new List<object>();

            foreach (var step in db.Plan_OrderProcess.Where(x => x.Active == true))
            {
                stepList.Add(new { Text = step.StepName, Value = step.ID });
            }
            return stepList;
        }

        public async Task<ActionResult> Plan_OrderStepProgress(int? id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderStep = new VmPlan_OrderStep();

            var count = (from t1 in db.Plan_OrderStep
                         where t1.CommonTheOrderFK == id
                         select t1.ID).Count();
            if (count == 0)
            {
                var orderProcess = (from t1 in db.Plan_OrderProcess
                                    where t1.Active == true
                                    select new
                                    {
                                        t1.ID,
                                        t1.StepName,
                                        t1.StartingPoint,
                                        t1.WorkingDistance
                                    }).ToList();
                var orderdate = (from t1 in db.Mkt_BOM
                                 join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                                 where t1.Active == true && t2.ID == id
                                 select new
                                 {
                                     t1.FirstMove,
                                     t2.OrderDate,
                                 }).SingleOrDefault();
                foreach (var op in orderProcess)
                {

                    VmPlan_OrderStep.Plan_OrderStep = new Plan_OrderStep();
                    VmPlan_OrderStep.Plan_OrderStep.Plan_OrderProcessFK = op.ID;
                    VmPlan_OrderStep.Plan_OrderStep.CommonTheOrderFK = id;
                    VmPlan_OrderStep.Plan_OrderStep.ActualStartDate = orderdate.OrderDate.AddDays(-1);
                    VmPlan_OrderStep.Plan_OrderStep.ActualFinishDate = orderdate.OrderDate.AddDays(-1);
                    VmPlan_OrderStep.Plan_OrderStep.PlannedStartDate = orderdate.FirstMove.AddDays(op.StartingPoint);
                    VmPlan_OrderStep.Plan_OrderStep.PlannedFinishDate = VmPlan_OrderStep.Plan_OrderStep.PlannedStartDate.AddDays(op.WorkingDistance);

                    await Task.Run(() => VmPlan_OrderStep.Add(UserID));
                }
            }



            VmPlan_OrderStep.VmCommon_TheOrder = new VmCommon_TheOrder();

            await Task.Run(() => VmPlan_OrderStep.VmCommon_TheOrder = VmPlan_OrderStep.VmCommon_TheOrder.SelectSingleJoined(id.ToString()));
            if (id != null) await Task.Run(() => VmPlan_OrderStep.GetPlan_OrderStep(id.Value));
            if (id != null) await Task.Run(() => VmPlan_OrderStep.GetPlan_OrderStepProgressBar(id.Value));
            VmPlan_OrderStep.Plan_OrderStep = new Plan_OrderStep();
            VmPlan_OrderStep.Plan_OrderStep.CommonTheOrderFK = id;

            return View(VmPlan_OrderStep);
        }
        public async Task<ActionResult> Plan_OrderStepProgressEditAll(int? id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderStep = new VmPlan_OrderStep();

            //var count = (from t1 in db.Plan_OrderStep
            //             where t1.CommonTheOrderFK == id
            //             select t1.ID).Count();
            //if (count == 0)
            //{
            //    var orderProcess = (from t1 in db.Plan_OrderProcess
            //                        where t1.Active == true
            //                        select new
            //                        {
            //                            t1.ID,
            //                            t1.StepName,
            //                            t1.StartingPoint,
            //                            t1.WorkingDistance
            //                        }).ToList();
            //    var orderdate = (from t1 in db.Mkt_BOM
            //                     join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
            //                     where t1.Active == true && t2.ID == id
            //                     select new
            //                     {
            //                         t1.FirstMove,
            //                         t2.OrderDate,
            //                     }).SingleOrDefault();
            //    foreach (var op in orderProcess)
            //    {

            //        VmPlan_OrderStep.Plan_OrderStep = new Plan_OrderStep();
            //        VmPlan_OrderStep.Plan_OrderStep.Plan_OrderProcessFK = op.ID;
            //        VmPlan_OrderStep.Plan_OrderStep.CommonTheOrderFK = id;
            //        VmPlan_OrderStep.Plan_OrderStep.ActualStartDate = orderdate.OrderDate.AddDays(-1);
            //        VmPlan_OrderStep.Plan_OrderStep.ActualFinishDate = orderdate.OrderDate.AddDays(-1);
            //        VmPlan_OrderStep.Plan_OrderStep.PlannedStartDate = orderdate.FirstMove.AddDays(op.StartingPoint);
            //        VmPlan_OrderStep.Plan_OrderStep.PlannedFinishDate = VmPlan_OrderStep.Plan_OrderStep.PlannedStartDate.AddDays(op.WorkingDistance);

            //        await Task.Run(() => VmPlan_OrderStep.Add(UserID));
            //    }
            //}



            VmPlan_OrderStep.VmCommon_TheOrder = new VmCommon_TheOrder();

            await Task.Run(() => VmPlan_OrderStep.VmCommon_TheOrder = VmPlan_OrderStep.VmCommon_TheOrder.SelectSingleJoined(id.ToString()));
            if (id != null) await Task.Run(() => VmPlan_OrderStep.GetPlan_OrderStep(id.Value));
            //if (id != null) await Task.Run(() => VmPlan_OrderStep.GetPlan_OrderStepProgressBar(id.Value));
            VmPlan_OrderStep.Plan_OrderStep = new Plan_OrderStep();
            VmPlan_OrderStep.Plan_OrderStep.CommonTheOrderFK = id;

            return View(VmPlan_OrderStep);
        }
        public ActionResult PlanReportDoc(int? id)
        {
            VmPlan_OrderStep p = new VmPlan_OrderStep();
            if (id != null) p.PlanReportDocLoad(id.Value);
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/PlanBaseDoc.rpt");
            cr.Load();
            cr.SetDataSource(p.PlanReportDoc);

            cr.SummaryInfo.ReportTitle = "Time & Action Calender";

            Stream stream =
                cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
            // }

            //return null;
        }

        public ActionResult Plan_OrderStepProgressCreate(int? id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPlanOrderProcessCategory();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ProcessCategory = sl3;
            VmPlan_OrderStep = new VmPlan_OrderStep();
            VmPlan_OrderStep.Plan_OrderStep = new Plan_OrderStep();

            VmPlan_OrderStep.Plan_OrderStep.ActualStartDate = DateTime.Now;
            VmPlan_OrderStep.Plan_OrderStep.ActualFinishDate = DateTime.Now;
            VmPlan_OrderStep.Plan_OrderStep.CommonTheOrderFK = id;
            return View(VmPlan_OrderStep);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Plan_OrderStepProgressCreate(VmPlan_OrderStep p)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                var orderProcess = (from t1 in db.Plan_OrderProcess
                                    where t1.Active == true
                                    select new
                                    {
                                        t1.StepName,
                                        t1.StartingPoint,
                                        t1.WorkingDistance
                                    }).ToList();
                var firstMove = (from t1 in db.Mkt_BOM
                                 join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
                                 where t1.Active == true && t2.ID == p.Plan_OrderStep.CommonTheOrderFK
                                 select t1.FirstMove
                                    ).SingleOrDefault();


                foreach (var dayCount in orderProcess)
                {
                    p.Plan_OrderStep.PlannedStartDate = firstMove.AddDays(dayCount.StartingPoint);
                    p.Plan_OrderStep.PlannedFinishDate = p.Plan_OrderStep.PlannedStartDate.AddDays(dayCount.WorkingDistance);
                    await Task.Run(() => p.Add(UserID));
                }


                return RedirectToAction("Plan_OrderStepProgress/" + p.Plan_OrderStep.CommonTheOrderFK);
            }

            return RedirectToAction("Plan_OrderStepProgress");
            //return RedirectToAction("VmPlan_OrderStepIndex", "Planning");
        }
        
        public async Task<ActionResult> Plan_OrderStepProgressEdit(int? id, string datetype)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmPlan_OrderStep = new VmPlan_OrderStep();

            if (id != null) await Task.Run(() => VmPlan_OrderStep.SelectSingle(id.Value));

            VmForDropDown = new VmForDropDown();
            if (VmPlan_OrderStep.Plan_OrderStep.Plan_OrderProcessFK != null)
                VmForDropDown.DDownData = VmForDropDown.GetPlanOrderProcessCategoryEdit(VmPlan_OrderStep.Plan_OrderStep.Plan_OrderProcessFK.Value);
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.ProcessCategory = sl3;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPlanOrderProcess();
            SelectList sl4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Process = sl4;


            return View(VmPlan_OrderStep);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Plan_OrderStepProgressEdit(VmPlan_OrderStep vmPlanOrderStep)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                vmPlanOrderStep.Plan_OrderStep.Remarks += vmPlanOrderStep.CurrentRemarks+" | ";
                await Task.Run(() => vmPlanOrderStep.Edit(UserID));
                return RedirectToAction("Plan_OrderStepProgress", "Planning", new { id = vmPlanOrderStep.Plan_OrderStep.CommonTheOrderFK });
            }

            return RedirectToAction("VmPlan_OrderStepIndex", "Planning");
        }
        
        //--------------------------
        public async Task<ActionResult> Plan_OrderTimeLineIndex()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderStep = new VmPlan_OrderStep();
            await Task.Run(() => VmPlan_OrderStep.InitialDataLoadOrderAction());
            return View(VmPlan_OrderStep);
        }
        
        //-------------Plan OrderProcessCategory------------------

        public async Task<ActionResult> VmPlan_OrderProcessCategoryIndex()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderProcessCategory = new VmPlan_OrderProcessCategory();
            await Task.Run(() => VmPlan_OrderProcessCategory.InitialDataLoad());
            return View(VmPlan_OrderProcessCategory);
        }

        public ActionResult VmPlan_OrderProcessCategoryCreate()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_OrderProcessCategoryCreate(VmPlan_OrderProcessCategory vmPlanOrderProcessCategory)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => vmPlanOrderProcessCategory.Add(UserID));
            }
            return RedirectToAction("VmPlan_OrderProcessCategoryIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmPlan_OrderProcessCategoryEdit(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderProcessCategory = new VmPlan_OrderProcessCategory();
            await Task.Run(() => VmPlan_OrderProcessCategory.SelectSingle(id));
            if (VmPlan_OrderProcessCategory.Plan_OrderProcessCategory == null)
            {
                return HttpNotFound();
            }
            return View(VmPlan_OrderProcessCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_OrderProcessCategoryEdit(VmPlan_OrderProcessCategory vmPlanOrderProcessCategory)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => vmPlanOrderProcessCategory.Edit(UserID));
            }
            return RedirectToAction("VmPlan_OrderProcessCategoryIndex");
        }

        [HttpGet]
        public ActionResult VmPlan_OrderProcessCategoryDelete(int id)
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Plan_OrderProcessCategory b = new Plan_OrderProcessCategory();
            b.ID = id;
            b.Delete(UserID);
            return RedirectToAction("VmPlan_OrderProcessCategoryIndex");
        }
        [HttpGet]
        public ActionResult VmPlan_TimeAndActionNotification()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_TimeAndActionNotification actionNotification = new VmPlan_TimeAndActionNotification();
            actionNotification.InitialDataLoad();

            return View(actionNotification);
        }
    }
}