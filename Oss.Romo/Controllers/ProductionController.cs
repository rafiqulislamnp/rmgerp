﻿using CrystalDecisions.CrystalReports.Engine;
using Oss.Romo.Models;
using Oss.Romo.Models.Common;
using Oss.Romo.Models.Marketing;
using Oss.Romo.Models.Planning;
using Oss.Romo.Models.Production;
using Oss.Romo.Models.User;
using Oss.Romo.ViewModels.Common;
using Oss.Romo.ViewModels.Marketing;
using Oss.Romo.ViewModels.Planning;
using Oss.Romo.ViewModels.Production;
using Oss.Romo.ViewModels.Reports;
using Oss.Romo.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Romo.ViewModels.Procurement;
using Oss.Romo.ViewModels.Store;

namespace Oss.Romo.Controllers
{
    [SoftwareAdmin]
    public class ProductionController : Controller
    {
        private xOssContext db = new xOssContext();

        VmProd_InputRequisition VmProd_InputRequisition;
        VmProc_Requisition VmProc_Requisition;
        VmForDropDown VmForDropDown;
        VmPlan_OrderProcess VmPlan_OrderProcess;
        VmProd_PlanReference VmProd_PlanReference;
        private VmProd_LineChief VmProd_LineChief;
        private VmProd_SVName VmProd_SVName;
        private VmProd_PlanReferenceProduction VmProd_PlanReferenceProduction;
        private VmProd_PlanReferenceProductionSection VmProd_PlanReferenceProductionSection;
        private VmProd_PlanReferenceProductionSectionFollowup VmProd_PlanReferenceProductionSectionFollowup;
        VmStore_InternalTransferSlave VmStore_InternalTransferSlave;
        VmProd_Consumption VmProd_Consumption;
        private VmProd_Capacity VmProd_Capacity;
        VmProd_TransitionItemMadeFrom VmProd_TransitionItemMadeFrom;
        private VmProd_Planning VmProd_Planning;
        VmPlan_ProductionLine VmPlan_ProductionLine;
        VmPlan_ProductionLineRunning VmPlan_ProductionLineRunning;
        VmProd_TransitionItemInventory VmProd_TransitionItemInventory;
        VmProd_Machine VmProd_Machine;
        VmProd_MachineType VmProd_MachineType;
        VmProd_MachineLine VmProd_MachineLine;
        VmPlan_ProductionReportOrderWise VmPlan_ProductionReportOrderWise;
        VmPlan_PartyWiseLeftOver VmPlan_PartyWiseLeftOver;
        VmProd_LineOfficer VmProd_LineOfficer;
        VmProd_SMVLayout VmProd_SMVLayout;
        VmProd_PlanReferenceOrder VmProd_PlanReferenceOrder;
        VmProd_Reference VmProd_Reference;
        VmProd_PlanReferenceOrderSection VmProd_PlanReferenceOrderSection;
        VmControllerHelper VmControllerHelper;
        VmProductionReport VmProductionReport;
        VmProd_OrderPlanning VmProd_OrderPlanning;
        VmPlanReport VmPlanReport;
        Int32 UserID = 0;
        
        public ProductionController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
        }
        

        #region FinishItemRequisition

        public async Task<ActionResult> VmProd_RequisitionIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_InputRequisition = new VmProd_InputRequisition();
            await Task.Run(() => VmProd_InputRequisition.InitialDataLoad((int)u.User_User.User_DepartmentFK));
            return View(VmProd_InputRequisition);
        }

        public ActionResult VmProd_RequisitionCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetDepartment();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Department = sl2;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPriority();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Priority = sl3;

            VmProd_InputRequisition = new VmProd_InputRequisition();
            VmProd_InputRequisition.Prod_InputRequisition = new Prod_InputRequisition();
            VmProd_InputRequisition.Prod_InputRequisition.Date = DateTime.Now;
            return View(VmProd_InputRequisition);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_RequisitionCreate(VmProd_InputRequisition VmProd_InputRequisition)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_InputRequisition.Prod_InputRequisition.FromUser_DeptFK = (int)user.User_User.User_DepartmentFK;
            await Task.Run(() => VmProd_InputRequisition.Add(UserID));
            return RedirectToAction("VmProd_RequisitionIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_RequisitionEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetDepartment();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Department = sl2;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPriority();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Priority = sl3;
            
            VmProd_InputRequisition = new VmProd_InputRequisition();
            await Task.Run(() => VmProd_InputRequisition.SelectSingle(id));
            
            return View(VmProd_InputRequisition);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_RequisitionEdit(Prod_InputRequisition Prod_InputRequisition)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => Prod_InputRequisition.Edit(0));
                return RedirectToAction("VmProd_RequisitionIndex");
            }
            return RedirectToAction("VmProd_RequisitionIndex");
        }

        public ActionResult VmProd_RequisitionDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Prod_InputRequisition b = new Prod_InputRequisition();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmProd_RequisitionIndex");
        }

        public async Task<ActionResult> ChangeRequisitionStatus(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_InputRequisition r = new VmProd_InputRequisition();
            await Task.Run(() => r.ChangeRequisitionStatus(id.Value));
            return RedirectToAction("VmProd_RequisitionIndex");
        }

        public async Task<ActionResult> VmReceived_InputRequisitionIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_InputRequisition VmProd_InputRequisition = new VmProd_InputRequisition();
            await Task.Run(() => VmProd_InputRequisition.ReceivedInitialDataLoad((int)user.User_User.User_DepartmentFK));
            return View(VmProd_InputRequisition);
        }

        #endregion

        #region FinishItemRequisitionSlave

        public async Task<ActionResult> VmProd_Requisition_SlaveIndex(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            VmProd_InputRequisition VmProd_InputRequisition = new VmProd_InputRequisition();
            await Task.Run(() => VmProd_InputRequisition = VmProd_InputRequisition.SelectSingleJoined(id));
            VmProd_InputRequisition.VmProd_InputRequisitionSlave = new VmProd_InputRequisitionSlave();
            await Task.Run(() => VmProd_InputRequisition.VmProd_InputRequisitionSlave.GetRequisitionSlaveItem(id));
            return View(VmProd_InputRequisition);
        }

        [HttpGet]
        public ActionResult VmProd_Requisition_SlaveCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAllOrderAndStyle();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.StyleList = sl2;
            
            VmProd_InputRequisitionSlave VmProd_InputRequisitionSlave = new VmProd_InputRequisitionSlave();
            VmProd_InputRequisitionSlave.Prod_InputRequisitionSlave = new Prod_InputRequisitionSlave();
            VmProd_InputRequisitionSlave.Prod_InputRequisitionSlave.Prod_InputRequisitionFk = id;
            return View(VmProd_InputRequisitionSlave);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_Requisition_SlaveCreate(VmProd_InputRequisitionSlave VmProd_InputRequisitionSlave)
        {
            VmProd_InputRequisitionSlave.Prod_InputRequisitionSlave.Common_UnitFK = 2;
            await Task.Run(() => VmProd_InputRequisitionSlave.Add(UserID));
            return RedirectToAction("VmProd_Requisition_SlaveIndex/" + VmProd_InputRequisitionSlave.Prod_InputRequisitionSlave.Prod_InputRequisitionFk);
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_Requisition_SlaveEdit(int ID)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAllOrderAndStyle();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.StyleList = sl2;
            
            VmProd_InputRequisitionSlave VmProd_InputRequisitionSlave = new VmProd_InputRequisitionSlave();
            await Task.Run(() => VmProd_InputRequisitionSlave.SelectSingle(ID));
            
            return View(VmProd_InputRequisitionSlave);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_Requisition_SlaveEdit(Prod_InputRequisitionSlave Prod_InputRequisitionSlave)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => Prod_InputRequisitionSlave.Edit(0));
                return RedirectToAction("VmProd_Requisition_SlaveIndex/" + Prod_InputRequisitionSlave.Prod_InputRequisitionFk);
            }
            return View(Prod_InputRequisitionSlave);
        }

        public ActionResult VmProd_Requisition_SlaveDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            int deletID = 0, RequisitionID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deletID);

            id = id.Replace(deletID.ToString() + "x", "");
            int.TryParse(id, out RequisitionID);
            Prod_InputRequisitionSlave b = new Prod_InputRequisitionSlave();
            b.ID = deletID;
            b.Delete(0);

            return RedirectToAction("VmProd_Requisition_SlaveIndex/" + RequisitionID);
        }

        #endregion

        #region FinishItemTransfer

        public async Task<ActionResult> VmProd_FinishItemTransferIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_FinishItemTransfer VmProd_FinishItemTransfer = new VmProd_FinishItemTransfer();
            await Task.Run(() => VmProd_FinishItemTransfer.InitialDataLoad((int)u.User_User.User_DepartmentFK));
            return View(VmProd_FinishItemTransfer);
        }

        [HttpGet]
        public ActionResult VmProd_FinishItemTransferCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetDepartment();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Department = sl2;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetInputRequisitionList();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RequisitionList = sl3;

            VmProd_FinishItemTransfer VmProd_FinishItemTransfer = new VmProd_FinishItemTransfer();
            VmProd_FinishItemTransfer.Prod_FinishItemTransfer = new Prod_FinishItemTransfer();
            VmProd_FinishItemTransfer.Prod_FinishItemTransfer.Date = DateTime.Now;
            return View(VmProd_FinishItemTransfer);
        }

        [HttpPost]
        public async Task<ActionResult> VmProd_FinishItemTransferCreate(VmProd_FinishItemTransfer VmProd_FinishItemTransfer)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_FinishItemTransfer.Prod_FinishItemTransfer.FromUser_DeptFK = (int)user.User_User.User_DepartmentFK;
            await Task.Run(() => VmProd_FinishItemTransfer.Add(UserID));
            return RedirectToAction("VmProd_FinishItemTransferIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_FinishItemTransferEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetDepartment();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Department = sl2;

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetInputRequisitionList();
            SelectList sl3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RequisitionList = sl3;

            VmProd_FinishItemTransfer VmProd_FinishItemTransfer = new VmProd_FinishItemTransfer();
            await Task.Run(() => VmProd_FinishItemTransfer.SelectSingle(id));

            return View(VmProd_FinishItemTransfer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_FinishItemTransferEdit(VmProd_FinishItemTransfer VmProd_FinishItemTransfer)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_FinishItemTransfer.Edit(0));
                return RedirectToAction("VmProd_FinishItemTransferIndex");
            }
            return RedirectToAction("VmProd_FinishItemTransferIndex");
        }

        public ActionResult VmProd_FinishItemTransferDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_FinishItemTransfer Prod_FinishItemTransfer = new Prod_FinishItemTransfer();
            Prod_FinishItemTransfer.ID = id;
            Prod_FinishItemTransfer.Delete(0);
            return RedirectToAction("VmProd_FinishItemTransferIndex");
        }
        #endregion
        
        #region FinishItemTransferSlave
        public async Task<ActionResult> VmProd_FinishItemTransferSlaveIndex(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            VmProd_FinishItemTransfer VmProd_FinishItemTransfer = new VmProd_FinishItemTransfer();
            await Task.Run(() => VmProd_FinishItemTransfer = VmProd_FinishItemTransfer.SelectSingleJoined(id));
            VmProd_FinishItemTransfer.VmProd_FinishItemTransferSlave = new VmProd_FinishItemTransferSlave();
            await Task.Run(() => VmProd_FinishItemTransfer.VmProd_FinishItemTransferSlave.GetFinishItemTransferSlaveItem(id));
            return View(VmProd_FinishItemTransfer);
        }

        [HttpGet]
        public ActionResult VmProd_FinishItemTransferSlaveCreate(int id,int rid,int did)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetAllInputRequisitionById(rid);
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RequisitionItem = sl2;

            VmProd_FinishItemTransferSlave model = new VmProd_FinishItemTransferSlave();
            model.Prod_FinishItemTransferSlave = new Prod_FinishItemTransferSlave();
            model.Prod_FinishItemTransferSlave.Prod_FinishItemTransferFK = id;
            model.DepartmentId = did;
            return View(model);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VmProd_FinishItemTransferSlaveCreate(VmProd_FinishItemTransferSlave VmProd_FinishItemTransferSlave)
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (u == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //    await Task.Run(() => VmProd_FinishItemTransferSlave.Add(UserID));
        //    return RedirectToAction("VmProd_FinishItemTransferSlaveIndex/" + VmProd_FinishItemTransferSlave.Prod_FinishItemTransferSlave.Prod_FinishItemTransferFK);
        //}

        //[HttpGet]
        //public ActionResult VmProd_FinishItemTransferSlaveEdit(int id, int rid, int did)
        //{
        //    Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
        //    if (u == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }

        //    VmForDropDown = new VmForDropDown();
        //    VmForDropDown.DDownData = VmForDropDown.GetAllInputRequisitionById(rid);
        //    SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
        //    ViewBag.RequisitionItem = sl2;

        //    VmProd_FinishItemTransferSlave model = new VmProd_FinishItemTransferSlave();
        //    model.Prod_FinishItemTransferSlave = new Prod_FinishItemTransferSlave();
        //    model.Prod_FinishItemTransferSlave.Prod_FinishItemTransferFK = id;
        //    model.DepartmentId = did;
        //    return View(model);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_FinishItemTransferSlaveEdit(VmProd_FinishItemTransferSlave VmProd_FinishItemTransferSlave)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            await Task.Run(() => VmProd_FinishItemTransferSlave.Edit(UserID));
            return RedirectToAction("VmProd_FinishItemTransferSlaveIndex/" + VmProd_FinishItemTransferSlave.Prod_FinishItemTransferSlave.Prod_FinishItemTransferFK);
        }

        public ActionResult VmProd_FinishItemTransferSlaveDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            int deletID = 0, TransferID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("x")), out deletID);

            id = id.Replace(deletID.ToString() + "x", "");
            int.TryParse(id, out TransferID);
            Prod_FinishItemTransferSlave b = new Prod_FinishItemTransferSlave();
            b.ID = deletID;
            b.Delete(0);

            return RedirectToAction("VmProd_FinishItemTransferSlaveIndex/" + TransferID);
        }
        #endregion

        #region FinishItemRequisition_Autherization

        [HttpGet]
        public async Task<ActionResult> VmProd_InputRequisitionAutherize(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            VmProd_InputRequisition = new VmProd_InputRequisition();

            await Task.Run(() => VmProd_InputRequisition.SelectSingle(id));

            if (VmProd_InputRequisition.Prod_InputRequisition == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_InputRequisition);
        }

        [HttpPost]
        public async Task<ActionResult> VmProd_InputRequisitionAutherize(VmProd_InputRequisition VmProd_InputRequisition)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_InputRequisition.Edit(0));
                return RedirectToAction("VmProd_RequisitionIndex");
            }
            return View(VmProd_InputRequisition);
        }

        #endregion

        #region FinishItemTransfer_Autherization

        [HttpGet]
        public async Task<ActionResult> VmProd_FinishItemTransferAutherize(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_FinishItemTransfer VmProd_FinishItemTransfer = new VmProd_FinishItemTransfer();

            await Task.Run(() => VmProd_FinishItemTransfer.SelectSingle(id));

            if (VmProd_FinishItemTransfer.Prod_FinishItemTransfer == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_FinishItemTransfer);
        }

        [HttpPost]
        public async Task<ActionResult> VmProd_FinishItemTransferAutherize(VmProd_FinishItemTransfer VmProd_FinishItemTransfer)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_FinishItemTransfer.Edit(0));
                return RedirectToAction("VmProd_FinishItemTransferIndex");
            }
            return View(VmProd_FinishItemTransfer);
        }

        #endregion
        
        #region Production & Planning

        #region Reference
        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Reference = new VmProd_Reference();
            await Task.Run(() => VmProd_Reference.InitialDataLoad());
            return View(VmProd_Reference);
        }
        
        [HttpGet]
        public ActionResult VmProd_PlanReferenceCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceCreate(VmProd_Reference VmProd_Reference)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_Reference.Add(UserID));
                return RedirectToAction("VmProd_PlanReferenceIndex");
            }
            return View(VmProd_Reference);
        }
        
        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_PlanReference = new VmProd_PlanReference();
            await Task.Run(() => VmProd_PlanReference.SelectSingle(id.ToString()));

            if (VmProd_PlanReference.Prod_PlanReference == null)
            {
                return HttpNotFound();
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetDelivaryPort(VmProd_PlanReference.Prod_PlanReference.Mkt_BOMFK);
            ViewBag.OrderDeliverySchedule = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            return View(VmProd_PlanReference);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceEdit(VmProd_PlanReference VmProd_PlanReference)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_PlanReference.Edit(0));
            }
            return RedirectToAction("VmProd_PlanReferenceIndex", "Production");
        }

        public ActionResult VmProd_PlanReferenceDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            Prod_Reference  p = new Prod_Reference();
            p.ID = id;
            p.Delete(0);
            return RedirectToAction("VmProd_PlanReferenceIndex");
        }
        #endregion
        
        #region ReferenceView
        
        [HttpGet]
        public async Task<ActionResult> VmPlanReferenceOrderProcess(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            
            await Task.Run(() => VmProd_PlanReferenceOrder = VmProd_PlanReferenceOrder.SelectSingleReference(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;
            return View(VmProd_PlanReferenceOrder);
        }
        
        #region CuttingProcess

        [HttpGet]
        public async Task<ActionResult> VmOrderProductionCuttingProcess(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPlanOrderDropDown(rid,1);
            ViewBag.OrderName = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            
            VmForDropDown.DDownData = VmForDropDown.GetProd_TableDropDown();
            ViewBag.LineName = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.CuttingInitialDataLoad(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;


            return View(VmProd_PlanReferenceOrder);
        }
        
        public async Task<ActionResult> ManagePlanReferenceOrder(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();

            //await Task.Run(() => VmProd_PlanReferenceOrder.DeleteZeroPlanOrder(rid,1));

            await Task.Run(() => VmProd_PlanReferenceOrder.GetDeletePlanFollowup(rid,2));
            //await Task.Run(() => VmProd_PlanReferenceOrder.GetDeleteFollowupWithoutPlan());

            return RedirectToAction("VmOrderProductionCuttingProcess", "Production", new { rid = rid });
        }

        [HttpGet]
        public ActionResult VmProd_PlanReferenceOrderCreate(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCuttingOrderIdDropDown();
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;
            return View(VmProd_PlanReferenceOrder);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceOrderCreate(VmProd_PlanReferenceOrder VmProd_PlanReferenceOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                VmControllerHelper = new VmControllerHelper();
                VmProd_PlanReferenceOrder.UserId = VmControllerHelper.GetCurrentUser().User_User.ID;
                await Task.Run(() => VmProd_PlanReferenceOrder.AddOrderForCutting(VmProd_PlanReferenceOrder));
                return RedirectToAction("VmOrderProductionCuttingProcess", "Production", new { rid = VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK });
            }
            return View(VmProd_PlanReferenceOrder);
        }

        [HttpGet]
        public async Task<ActionResult> VmOrderProductionCuttingFollowup(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.CuttingFollowupDataLoad(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        public async Task<ActionResult> VmProd_PlanReferenceOrderItemEdit(ProductionDailyPlan ProductionDailyPlan)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;

            await Task.Run(() => VmProd_PlanReferenceOrder.SaveCuttingPlan(ProductionDailyPlan));
            var jsonData = new
            {
                lineName = VmProd_PlanReferenceOrder.LineName,
                cheifsupervisor= VmProd_PlanReferenceOrder.CheifSuperVisorName,
                result = VmProd_PlanReferenceOrder.Status
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> VmProd_PlanReferenceOrderItemUpdate(ProductionDailyPlan ProductionDailyPlan)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            VmProd_PlanReferenceOrderSection = new VmProd_PlanReferenceOrderSection();

            await Task.Run(() => VmProd_PlanReferenceOrderSection.SaveCuttingSection(ProductionDailyPlan));
            var jsonData = new
            {
                qty = VmProd_PlanReferenceOrderSection.TotalQty,
                doneQtyWeight = VmProd_PlanReferenceOrderSection.WeightQty,
                result = 1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region SewingProcess
        [HttpGet]
        public async Task<ActionResult> VmOrderProductionSewingProcess(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProd_LineDropDown();
            ViewBag.LineName = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.SewingInitialDataLoad(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        [HttpGet]
        public async Task<ActionResult> AddSewingPreviousPlan(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.AddPreviousSewingPlanOrder(rid));
            return RedirectToAction("VmOrderProductionSewingProcess", "Production", new { rid = rid });
        }

        [HttpGet]
        public ActionResult VmProd_PlanReferenceOrderSewingCreate(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetSewingOrderIdDropDown();
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetProd_LineDropDown();
            ViewBag.Line = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.ProductionDailyPlan = new ProductionDailyPlan();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk = rid;
            return View(VmProd_PlanReferenceOrder);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceOrderSewingCreate(VmProd_PlanReferenceOrder VmProd_PlanReferenceOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                VmControllerHelper = new VmControllerHelper();
                VmProd_PlanReferenceOrder.ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
                await Task.Run(() => VmProd_PlanReferenceOrder.AddSewingLineOrder(VmProd_PlanReferenceOrder.ProductionDailyPlan));
                return RedirectToAction("VmOrderProductionSewingProcess", "Production", new { rid = VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk });
            }

            return View(VmProd_PlanReferenceOrder);
        }
        
        [HttpGet]
        public async Task<ActionResult> SewingPlanUpdate(int OId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.SelectSingleSewingPlan(OId));
            return View(VmProd_PlanReferenceOrder);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SewingPlanUpdate(VmProd_PlanReferenceOrder VmProd_PlanReferenceOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            await Task.Run(() => VmProd_PlanReferenceOrder.UpdateSewingOrderPlan(VmProd_PlanReferenceOrder.ProductionDailyPlan));
            return RedirectToAction("VmOrderProductionSewingProcess", "Production", new { rid = VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk });
        }

        public async Task<ActionResult> SewingPlanDelete(int OId)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.GetDeletePlanOrderWithoutPlan(OId));
            return RedirectToAction("VmOrderProductionSewingProcess", "Production", new { rid = VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk });            
        }
        
        [HttpGet]
        public async Task<ActionResult> VmOrderProductionSewingFollowup(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.SewingFollowupDataLoad(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        [HttpGet]
        public ActionResult VmCreateSewingOrderFollowup(int rid, int SectionId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetSewingPlanOrderIdDropDown(rid);
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetProd_LineDropDown();
            ViewBag.Line = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            ViewBag.Color = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.ProductionDailyPlan = new ProductionDailyPlan();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        [HttpPost]
        public async Task<ActionResult> VmCreateSewingOrderFollowup(VmProd_PlanReferenceOrder VmProd_PlanReferenceOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                VmControllerHelper = new VmControllerHelper();
                VmProd_PlanReferenceOrder.ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
                await Task.Run(() => VmProd_PlanReferenceOrder.AddOrderForSewingFollowup(VmProd_PlanReferenceOrder.ProductionDailyPlan));
                return RedirectToAction("VmOrderProductionSewingFollowup", "Production", new { rid = VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk });
            }

            return View(VmProd_PlanReferenceOrder);
        }

        public async Task<ActionResult> VmOrderProductionSewingEdit(ProductionDailyPlan ProductionDailyPlan)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;

            await Task.Run(() => VmProd_PlanReferenceOrder.SaveSewingPlan(ProductionDailyPlan));
            var jsonData = new
            {
                //lineName = VmProd_PlanReferenceOrder.LineName,
                //cheifsupervisor = VmProd_PlanReferenceOrder.CheifSuperVisorName,
                result = VmProd_PlanReferenceOrder.Status,
                //UpdateRemainQty = VmProd_PlanReferenceOrder.UpdateRemainQty
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> VmOrderProductionSewingUpdate(ProductionDailyPlan ProductionDailyPlan)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            VmProd_PlanReferenceOrderSection = new VmProd_PlanReferenceOrderSection();

            await Task.Run(() => VmProd_PlanReferenceOrderSection.SaveSewingSection(ProductionDailyPlan));
            var jsonData = new
            {
                qty = VmProd_PlanReferenceOrderSection.TotalQty,
                result = 1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        
        #endregion

        #region IronProcess

        [HttpGet]
        public async Task<ActionResult> VmOrderProductionIronProcess(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProd_LineDropDown();
            ViewBag.LineName = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.IronPlanInitialDataLoad(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        [HttpGet]
        public ActionResult VmProd_PlanReferenceOrderIronCreate(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCuttingOrderIdDropDown();
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.ProductionDailyPlan = new ProductionDailyPlan();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk = rid;
            return View(VmProd_PlanReferenceOrder);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceOrderIronCreate(VmProd_PlanReferenceOrder VmProd_PlanReferenceOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmProd_PlanReferenceOrder.AddOrderForIronPlan(VmProd_PlanReferenceOrder.ProductionDailyPlan));
            return RedirectToAction("VmOrderProductionIronProcess", "Production", new { rid = VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk });
        }

        [HttpGet]
        public async Task<ActionResult> VmOrderProductionIronFollowup(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.IronFollowupDataLoad(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        [HttpGet]
        public ActionResult VmCreateIronOrderFollowup(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetIronPlanOrderIdDropDown(rid);
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            ViewBag.Color = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.ProductionDailyPlan = new ProductionDailyPlan();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        [HttpPost]
        public async Task<ActionResult> VmCreateIronOrderFollowup(VmProd_PlanReferenceOrder VmProd_PlanReferenceOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmProd_PlanReferenceOrder.AddOrderForIronFollowup(VmProd_PlanReferenceOrder.ProductionDailyPlan));
            return RedirectToAction("VmOrderProductionIronFollowup", "Production", new { rid = VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk });
            
        }
        
        public async Task<ActionResult> VmOrderProductionIronEdit(ProductionDailyPlan ProductionDailyPlan)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;

            await Task.Run(() => VmProd_PlanReferenceOrder.SaveIronPlan(ProductionDailyPlan));
            var jsonData = new
            {
                //lineName = VmProd_PlanReferenceOrder.LineName,
                //cheifsupervisor = VmProd_PlanReferenceOrder.CheifSuperVisorName,
                result = VmProd_PlanReferenceOrder.Status,
                //UpdateRemainQty = VmProd_PlanReferenceOrder.UpdateRemainQty
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> VmOrderProductionIronUpdate(ProductionDailyPlan ProductionDailyPlan)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            VmProd_PlanReferenceOrderSection = new VmProd_PlanReferenceOrderSection();

            await Task.Run(() => VmProd_PlanReferenceOrderSection.SaveIroningSection(ProductionDailyPlan));
            var jsonData = new
            {
                qty = VmProd_PlanReferenceOrderSection.TotalQty,
                result = 1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PackingProcess

        [HttpGet]
        public async Task<ActionResult> VmOrderProductionPackingProcess(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProd_LineDropDown();
            ViewBag.LineName = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.PackingInitialDataLoad(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        [HttpGet]
        public ActionResult VmProd_PlanReferenceOrderPackingCreate(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCuttingOrderIdDropDown();
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.ProductionDailyPlan = new ProductionDailyPlan();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk = rid;
            return View(VmProd_PlanReferenceOrder);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceOrderPackingCreate(VmProd_PlanReferenceOrder VmProd_PlanReferenceOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmProd_PlanReferenceOrder.AddOrderForPackingPlan(VmProd_PlanReferenceOrder.ProductionDailyPlan));
            return RedirectToAction("VmOrderProductionPackingProcess", "Production", new { rid = VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk });
            
        }

        [HttpGet]
        public async Task<ActionResult> VmOrderProductionPackingFollowup(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.PackingFollowupDataLoad(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        [HttpGet]
        public ActionResult VmCreatePackOrderFollowup(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPackingPlanOrderIdDropDown(rid);
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            ViewBag.Color = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.ProductionDailyPlan = new ProductionDailyPlan();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk = rid;

            return View(VmProd_PlanReferenceOrder);
        }

        [HttpPost]
        public async Task<ActionResult> VmCreatePackOrderFollowup(VmProd_PlanReferenceOrder VmProd_PlanReferenceOrder)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            VmProd_PlanReferenceOrder.ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            await Task.Run(() => VmProd_PlanReferenceOrder.AddOrderForPackingFollowup(VmProd_PlanReferenceOrder.ProductionDailyPlan));
            return RedirectToAction("VmOrderProductionPackingFollowup", "Production", new { rid = VmProd_PlanReferenceOrder.ProductionDailyPlan.Prod_ReferenceFk });
        }
        
        public async Task<ActionResult> VmOrderProductionPackingEdit(ProductionDailyPlan ProductionDailyPlan)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;

            await Task.Run(() => VmProd_PlanReferenceOrder.SavePackingPlan(ProductionDailyPlan));
            var jsonData = new
            {
                //lineName = VmProd_PlanReferenceOrder.LineName,
                //cheifsupervisor = VmProd_PlanReferenceOrder.CheifSuperVisorName,
                result = VmProd_PlanReferenceOrder.Status,
                //UpdateRemainQty = VmProd_PlanReferenceOrder.UpdateRemainQty
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> VmOrderProductionPackingUpdate(ProductionDailyPlan ProductionDailyPlan)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmControllerHelper = new VmControllerHelper();
            ProductionDailyPlan.UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            VmProd_PlanReferenceOrderSection = new VmProd_PlanReferenceOrderSection();

            await Task.Run(() => VmProd_PlanReferenceOrderSection.SavePackingSection(ProductionDailyPlan));
            var jsonData = new
            {
                qty = VmProd_PlanReferenceOrderSection.TotalQty,
                result = 1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CommonActionSewingIroningPacking
        
        public async Task<ActionResult> DeleteZeroFollowup(int RId, int SId)
        {
            VmProd_PlanReferenceOrderSection = new VmProd_PlanReferenceOrderSection();
            await Task.Run(() => VmProd_PlanReferenceOrderSection.DeleteZeroFollowup(RId, SId));
            string actionName = string.Empty;
            if (SId == 2)
            {
                actionName = "VmOrderProductionSewingFollowup";
            }
            else if (SId == 3)
            {
                actionName = "VmOrderProductionIronFollowup";
            }
            else if (SId==4)
            {
                actionName = "VmOrderProductionPackingFollowup";
            }
            return RedirectToAction(actionName, "Production", new { rid = RId });
        }

        public async Task<ActionResult> PlanReferenceFollowup(int rid, int SectionId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();
            await Task.Run(() => VmProd_PlanReferenceOrder.GetPlanFollowUpDetails(rid, SectionId));
            return View(VmProd_PlanReferenceOrder);
        }

        #endregion

        #endregion

        #region ProductionReport


        [HttpGet]
        public async Task<ActionResult> VmPlanReferenceReportProcess(int rid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceOrder = new VmProd_PlanReferenceOrder();

            await Task.Run(() => VmProd_PlanReferenceOrder = VmProd_PlanReferenceOrder.SelectSingleReference(rid));
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder = new Prod_PlanReferenceOrder();
            VmProd_PlanReferenceOrder.Prod_PlanReferenceOrder.Prod_ReferenceFK = rid;
            return View(VmProd_PlanReferenceOrder);
        }

        [HttpGet]
        public async Task<ActionResult> productionReportView(int RefId)
        {
            VmProductionReport = new VmProductionReport();
            await Task.Run(() => VmProductionReport.GetTargetAchivementReport(RefId));
            return View(VmProductionReport);
        }

        [HttpPost]
        public async Task<ActionResult> productionReportView(VmProductionReport model)
        {
            VmProductionReport = new VmProductionReport();
            await Task.Run(() => VmProductionReport.SaveAchivementRemarks(model));
            await Task.Run(() => VmProductionReport.GetAchivementReport(model.DataList));
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/RptAchivementReport.rpt");
            cr.Load();
            cr.SetDataSource(VmProductionReport.ReportDoc);
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        
        public async Task<ActionResult> AchivementReport(int RefId)
        {
            VmProductionReport = new VmProductionReport();
            //await Task.Run(() => VmProductionReport.GetAchivementReport(model.DataList));
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/RptAchivementReport.rpt");
            cr.Load();
            cr.SetDataSource(VmProductionReport.ReportDoc);
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        
        [HttpGet]
        public ActionResult VmProductionOrderReport()
        {
            return View();
        }
        
        [HttpGet]
        public async Task<ActionResult> ProductionorderReportView(int RefId)
        {
            VmProductionReport = new VmProductionReport();
            await Task.Run(() => VmProductionReport.GetProductionOrderSummary(RefId));
            return View(VmProductionReport);
        }

        public async Task<ActionResult> BuyerProductionDetails(int BomId, int RefId)
        {
            VmProductionReport = new VmProductionReport();
            await Task.Run(() => VmProductionReport.GetBuyerOrderProductionDetails(BomId, RefId));
            return View(VmProductionReport);
        }
        
        public async Task<ActionResult> BuyerProductionView(int RefId)
        {
            VmProductionReport = new VmProductionReport();
            await Task.Run(() => VmProductionReport.GetDailySewingProduction(RefId));
            return View(VmProductionReport);
        }

        #region DailyReport
        
        public async Task<ActionResult> DailyCutting(int RefId)
        {
            VmProductionReport = new VmProductionReport();
            await Task.Run(() => VmProductionReport.GetDailyCuttingProduction(RefId));
            return View(VmProductionReport);
        }

        public async Task<ActionResult> DailyIroning(int RefId)
        {
            VmProductionReport = new VmProductionReport();
            await Task.Run(() => VmProductionReport.GetDailyIroningProduction(RefId));
            return View(VmProductionReport);
        }

        public async Task<ActionResult> DailyPacking(int RefId)
        {
            VmProductionReport = new VmProductionReport();
            await Task.Run(() => VmProductionReport.GetDailyPackingProduction(RefId));
            return View(VmProductionReport);
        }

        [HttpGet]
        public ActionResult OrderProduction()
        {
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderByBuyerDropDown();
            ViewBag.Report = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmProductionReport = new VmProductionReport();
            return View(VmProductionReport);
        }

        [HttpPost]
        public async Task<ActionResult> OrderProduction(VmProductionReport model)
        {
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderByBuyerDropDown();
            ViewBag.Report = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            await Task.Run(() => model.GetOrderProduction(model.ID));
            return View(model);
        }
        #endregion
        
        public ActionResult HourlyLineProductionView(int RefId)
        {
            VmProductionReport = new VmProductionReport();
            ViewData["ProductionData"] = VmProductionReport.GetHourlyLineProduction(RefId);
            return View(VmProductionReport);
        }

        [HttpGet]
        public ActionResult MonthlyProduction()
        {
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProductionReport();
            ViewBag.Report = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmProductionReport = new VmProductionReport();
            VmProductionReport.FromDate = DateTime.Today.AddDays(-30);
            VmProductionReport.ToDate = DateTime.Today;
            return View(VmProductionReport);
        }

        [HttpPost]
        public async Task<ActionResult> MonthlyProduction(VmProductionReport model)
        {
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProductionReport();
            ViewBag.Report = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            if (model.ID == 1)
            {
                ViewData["ProductionData"] = model.GetMonthlyBuyerProduction(model);
            }
            else if (model.ID==2)
            {
                await Task.Run(() => model.GetMonthlyOrderProduction(model));
            }
            else if (model.ID == 3)
            {
                await Task.Run(() => model.GetLineProduction(model));
            }
            else if (model.ID == 4)
            {
                await Task.Run(() => model.GetMonthlyAcheivment(model));
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ProductionClose()
        {
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderByBuyerDropDown();
            ViewBag.Report = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmProductionReport = new VmProductionReport();
            return View(VmProductionReport);
        }

        [HttpPost]
        public async Task<ActionResult> ProductionClose(VmProductionReport model)
        {
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderByBuyerDropDown();
            ViewBag.Report = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            await Task.Run(() => model.GetOrderCloseReport(model.BOMID));
            return View(model);
        }
        #endregion

        #endregion

        #region Production OldProcess
        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceView()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReference = new VmProd_PlanReference();
            await Task.Run(() => VmProd_PlanReference.InitialDataLoad());
            return View(VmProd_PlanReference);
        }
        
        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceProductionIndex(int rid, int bomId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceProduction = new VmProd_PlanReferenceProduction();
            await Task.Run(() => VmProd_PlanReferenceProduction.InitialDataLoad(rid));
            VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction = new Prod_PlanReferenceProduction();
            VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.Prod_PlanReferenceFK = rid;
            VmProd_PlanReferenceProduction.BOMID = bomId;

            VmProd_PlanReference = new VmProd_PlanReference();
            await Task.Run(() => VmProd_PlanReference.SelectSingleData(rid));
            VmProd_PlanReferenceProduction.VmProd_PlanReference = VmProd_PlanReference;

            return View(VmProd_PlanReferenceProduction);
        }
       
        [HttpGet]
        public ActionResult VmProd_PlanReferenceProductionCreate(int rid, int bomId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProductionStep();
            ViewBag.MasterPlan = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmProd_PlanReferenceProduction = new VmProd_PlanReferenceProduction();
            VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction = new Prod_PlanReferenceProduction();
            VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.Prod_PlanReferenceFK = rid;
            VmProd_PlanReferenceProduction.BOMID = bomId;
            VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.StartDate = DateTime.Now;
            VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.FinishDate = DateTime.Now;
            return View(VmProd_PlanReferenceProduction);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceProductionCreate(VmProd_PlanReferenceProduction VmProd_PlanReferenceProduction)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {

                if (VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.Plan_OrderProcessFK == 32)
                {
                    var totalqty = db.Prod_PlanReference.FirstOrDefault(x =>
                    x.ID == VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.Prod_PlanReferenceFK);
                    if (totalqty != null)
                        VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.TargetDays = Math.Ceiling(totalqty.Quantity /
                                  (VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.SMB *
                                   VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.WorkingHour));
                    VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.FinishDate =
                        VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.StartDate.AddDays(Convert.ToDouble(VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.TargetDays));
                }
                await Task.Run(() => VmProd_PlanReferenceProduction.Add(UserID));

                return RedirectToAction("VmProd_PlanReferenceProductionIndex", "Production", new { rid = VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.Prod_PlanReferenceFK, bomId = VmProd_PlanReferenceProduction.BOMID });
            }
            return View(VmProd_PlanReferenceProduction);
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceProductionEdit(int rpid, int bomId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceProduction = new VmProd_PlanReferenceProduction();
            await Task.Run(() => VmProd_PlanReferenceProduction.SelectSingle(rpid.ToString()));
            VmProd_PlanReferenceProduction.BOMID = bomId;
            if (VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction == null)
            {
                return HttpNotFound();
            }
            //no chg
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProductionStep();
            ViewBag.MasterPlan = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            return View(VmProd_PlanReferenceProduction);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceProductionEdit(VmProd_PlanReferenceProduction VmProd_PlanReferenceProduction)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_PlanReferenceProduction.Edit(0));
                return RedirectToAction("VmProd_PlanReferenceProductionIndex", "Production", new { rid = VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.Prod_PlanReferenceFK, bomId = VmProd_PlanReferenceProduction.BOMID });
            }
            return View(VmProd_PlanReferenceProduction);
        }

        public ActionResult VmProd_PlanReferenceProductionDelete(int rpid, int prId, int bomID)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceProduction = new VmProd_PlanReferenceProduction();
            VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction = new Prod_PlanReferenceProduction();
            VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction.ID = rpid;
            VmProd_PlanReferenceProduction.Delete(UserID);
            return RedirectToAction("VmProd_PlanReferenceProductionIndex", "Production", new { rid = prId, bomId = bomID });
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionIndex(int pRefProdid, int bomId, int orderDeliveryScheduleId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();
            await Task.Run(() => ViewBag.ColorSizeDimension = VmProd_PlanReferenceProductionSection.GetColorAndSizeDimension(bomId, pRefProdid));
            await Task.Run(() => VmProd_PlanReferenceProductionSection.InitialDataLoad(pRefProdid));
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection = new Prod_PlanReferenceProductionSection();
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.Prod_PlanReferenceProductionFk = pRefProdid;
            VmProd_PlanReferenceProductionSection.BOMID = bomId;
            VmProd_PlanReferenceProductionSection.OrderDeliveryScheduleId = orderDeliveryScheduleId;
            VmProd_PlanReferenceProduction = new VmProd_PlanReferenceProduction();
            await Task.Run(() => VmProd_PlanReferenceProduction.SelectSingleData(pRefProdid));
            VmProd_PlanReferenceProductionSection.VmProd_PlanReferenceProduction = VmProd_PlanReferenceProduction;

            return View(VmProd_PlanReferenceProductionSection);
        }

        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionView(int pRefProdid, int bomId, int orderDeliveryScheduleId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();
            await Task.Run(() => ViewBag.ColorSizeDimension = VmProd_PlanReferenceProductionSection.GetColorAndSizeDimension(bomId, pRefProdid));
            await Task.Run(() => VmProd_PlanReferenceProductionSection.InitialDataLoad(pRefProdid));
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection = new Prod_PlanReferenceProductionSection();
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.Prod_PlanReferenceProductionFk = pRefProdid;
            VmProd_PlanReferenceProductionSection.BOMID = bomId;
            VmProd_PlanReferenceProductionSection.OrderDeliveryScheduleId = orderDeliveryScheduleId;
            VmProd_PlanReferenceProduction = new VmProd_PlanReferenceProduction();
            await Task.Run(() => VmProd_PlanReferenceProduction.SelectSingleData(pRefProdid));
            VmProd_PlanReferenceProductionSection.VmProd_PlanReferenceProduction = VmProd_PlanReferenceProduction;

            return View(VmProd_PlanReferenceProductionSection);
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionCreate(int pRefProdid, int bomId, int orderDeliveryScheduleId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();
            VmProd_PlanReferenceProductionSection.SelectSinglePlanRefProdByID(pRefProdid);

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProductionLineByOrderProcess(VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProduction.Plan_OrderProcessFK);
            ViewBag.LineName = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetColorByPlanReferenceProduction(bomId, orderDeliveryScheduleId);
            ViewBag.Color = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetProductionChiefForDropDown();
            ViewBag.ProductionChief = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetProductionSVNameForDropDown();
            ViewBag.ProductionSVName = new SelectList(VmForDropDown.DDownData, "Value", "Text");


            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection = new Prod_PlanReferenceProductionSection();
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.Date = DateTime.Now;
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.StartDate = DateTime.Now;
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.FinishDate = DateTime.Now;
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.Prod_PlanReferenceProductionFk = pRefProdid;
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.OrderDeliveryScheduleId = orderDeliveryScheduleId;

            VmProd_PlanReferenceProductionSection.BOMID = bomId;
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProduction = new Prod_PlanReferenceProduction();
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProduction.Plan_OrderProcessFK = VmProd_PlanReferenceProductionSection.GetorderProcessById(pRefProdid);
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProduction.ExtraCutting = VmProd_PlanReferenceProductionSection.GetExtraById(pRefProdid);
            return View(VmProd_PlanReferenceProductionSection);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionCreate(VmProd_PlanReferenceProductionSection o)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (ModelState.IsValid)
            {
                if (o.Prod_PlanReferenceProduction.Plan_OrderProcessFK == 30)
                {
                    await Task.Run(() => o.DataList2 = o.GetSizeAndRatioByColor(o.Prod_PlanReferenceProductionSection.Prod_PlanReferenceProductionFk, o.BOMID, o.Prod_PlanReferenceProductionSection.Color, o.Prod_PlanReference.Mkt_OrderDeliveryScheduleFK));
                    foreach (var x in o.DataList2)
                    {
                        decimal? qty = x.TotalQuantity +
                                      (x.TotalQuantity / 100 * o.Prod_PlanReferenceProduction.ExtraCutting);
                        if (qty != null)
                        {
                            o.Prod_PlanReferenceProductionSection.Size = x.Mkt_OrderColorAndSizeRatio.Size;

                            o.Prod_PlanReferenceProductionSection.Quantity = Math.Ceiling(qty.Value);
                            await Task.Run(() => o.Add(0));
                        }
                       

                    }

                }
                else
                {
                    await Task.Run(() => o.Add(0));
                }




                return RedirectToAction("VmProd_PlanReferenceProductionSectionIndex", "Production", new { pRefProdid = o.Prod_PlanReferenceProductionSection.Prod_PlanReferenceProductionFk, bomId = o.BOMID, orderDeliveryScheduleId = o.Prod_PlanReferenceProductionSection.OrderDeliveryScheduleId });
            }
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionEdit(int pRefProdSecid, int pRefProdid, int bomId, int orderDeliveryScheduleId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();
            VmProd_PlanReferenceProductionSection.OrderDeliveryScheduleId = orderDeliveryScheduleId;
            await Task.Run(() => VmProd_PlanReferenceProductionSection.SelectSingle(pRefProdSecid.ToString()));
            VmProd_PlanReferenceProductionSection.BOMID = bomId;

            if (VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection == null)
            {
                return HttpNotFound();
            }
            //no chg
            VmProd_PlanReferenceProductionSection.SelectSinglePlanRefProdByID(pRefProdid);
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProductionLineByOrderProcess(VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProduction.Plan_OrderProcessFK);
            ViewBag.LineName = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetColorByPlanReferenceProduction(bomId, orderDeliveryScheduleId);
            ViewBag.Color = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetSizeByPlanReferenceProduction(bomId, orderDeliveryScheduleId);
            ViewBag.Size = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetProductionChiefForDropDown();
            ViewBag.ProductionChief = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetProductionSVNameForDropDown();
            ViewBag.ProductionSVName = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProduction = new Prod_PlanReferenceProduction();
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProduction.Plan_OrderProcessFK = VmProd_PlanReferenceProductionSection.GetorderProcessById(pRefProdid);



            return View(VmProd_PlanReferenceProductionSection);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionEdit(VmProd_PlanReferenceProductionSection VmProd_PlanReferenceProductionSection)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_PlanReferenceProductionSection.Edit(UserID));
                return RedirectToAction("VmProd_PlanReferenceProductionSectionIndex", "Production", new { pRefProdid = VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.Prod_PlanReferenceProductionFk, bomId = VmProd_PlanReferenceProductionSection.BOMID, orderDeliveryScheduleId = VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.OrderDeliveryScheduleId });
            }
            return View();
        }

        public ActionResult VmProd_PlanReferenceProductionSectionDelete(int pid, int pRefProdiD, int bomID,int orderDeliveryScheduleId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection = new Prod_PlanReferenceProductionSection();
            VmProd_PlanReferenceProductionSection.Prod_PlanReferenceProductionSection.ID = pid;
            VmProd_PlanReferenceProductionSection.Delete(UserID);
            return RedirectToAction("VmProd_PlanReferenceProductionSectionIndex", "Production", new { pRefProdid = pRefProdiD, bomId = bomID, orderDeliveryScheduleId = orderDeliveryScheduleId });
        }
        
        [HttpGet]
        public async Task<ActionResult> VmProd_ProductionSectionIndex(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }
        
        [HttpGet]
        public async Task<ActionResult> VmProd_ProductionSectionCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_ProductionSectionCreate(VmProd_ProductionSection VmProd_ProductionSection)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_ProductionSectionEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_ProductionSectionEdit(VmProd_ProductionSection VmProd_ProductionSection)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }
        
        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionFollowupIndex(int id)

        {

            VmProd_PlanReferenceProductionSectionFollowup = new VmProd_PlanReferenceProductionSectionFollowup();
            await Task.Run(() => VmProd_PlanReferenceProductionSectionFollowup = VmProd_PlanReferenceProductionSectionFollowup.SelectSingleJoined(id));
            await Task.Run(() => VmProd_PlanReferenceProductionSectionFollowup.InitialDataLoad(id));
            VmProd_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFollowup = new Prod_PlanReferenceProductionSectionFollowup();
            VmProd_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFollowup
                .Prod_PlanReferenceProductionSectionFk = id;
            return View(VmProd_PlanReferenceProductionSectionFollowup);
        }
        
        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionFollowupCreate(int ProdPlanReferenceProductionSectionID, int bomId, int orderProcessId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int? ItemQuantity = 0;
            VmProd_PlanReferenceProductionSectionFollowup = new VmProd_PlanReferenceProductionSectionFollowup();
            await Task.Run(() => ItemQuantity = VmProd_PlanReferenceProductionSectionFollowup.GetFollowupQuantityByID(ProdPlanReferenceProductionSectionID));
            await Task.Run(() => VmProd_PlanReferenceProductionSectionFollowup.GetSectionByID(ProdPlanReferenceProductionSectionID));

            VmProd_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFollowup = new Prod_PlanReferenceProductionSectionFollowup();
            VmProd_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFk = ProdPlanReferenceProductionSectionID;
            VmProd_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFollowup.Quantity =
                Convert.ToInt32(
                    VmProd_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSection.Quantity - ItemQuantity);
            VmProd_PlanReferenceProductionSectionFollowup.BOMID = bomId;
            VmProd_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFollowup.Date = DateTime.Now;
            VmProd_PlanReferenceProductionSectionFollowup.OrderProcessID = orderProcessId;

            return View(VmProd_PlanReferenceProductionSectionFollowup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionFollowupCreate(VmProd_PlanReferenceProductionSectionFollowup followup)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int Prod_PlanReferenceProductionSectionFollowuID = 0;


            if (ModelState.IsValid)
            {
                await Task.Run(() => Prod_PlanReferenceProductionSectionFollowuID = followup.Add(UserID));
                followup.GetProd_PlanReferenceProductionSectionFollowuBySection(followup.Prod_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFk);

                followup.SelectSingle(Prod_PlanReferenceProductionSectionFollowuID.ToString());
                followup.VmMkt_BOM = new VmMkt_BOM();
                followup.VmMkt_BOM.GetBOMByID(followup.BOMID.ToString());
                string itemName = followup.Prod_PlanReference.CID + "/" + followup.Plan_OrderProcess.StepName + " Pices/" + followup.VmMkt_BOM.Common_TheOrder.BuyerPO + "/" + followup.Prod_PlanReferenceProductionSection.Color + "/" + followup.Prod_PlanReferenceProductionSection.Size;
                VmProd_TransitionItemInventory x = new VmProd_TransitionItemInventory();
                x.Prod_TransitionItemInventory = new Prod_TransitionItemInventory();



                if (followup.Prod_TransitionItemInventory == null)
                {
                    x.Prod_TransitionItemInventory.Prod_PlanReferenceProductionSectionFk = followup.Prod_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFk.Value;
                    x.Prod_TransitionItemInventory.Mkt_BOMFk = followup.BOMID;
                    x.Prod_TransitionItemInventory.Quantity = followup.Prod_PlanReferenceProductionSectionFollowup.Quantity;
                    x.Prod_TransitionItemInventory.ItemName = itemName;
                    x.Prod_TransitionItemInventory.FirstCreatedBy = followup.Prod_PlanReferenceProductionSectionFollowup.FirstCreatedBy;
                    x.Prod_TransitionItemInventory.LastEditeddBy = followup.Prod_PlanReferenceProductionSectionFollowup.LastEditeddBy;
                    await Task.Run(() => x.Add(UserID));
                }
                else
                {
                    x.Prod_TransitionItemInventory.ID = followup.Prod_TransitionItemInventory.ID;
                    x.Prod_TransitionItemInventory.Prod_PlanReferenceProductionSectionFk = followup.Prod_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFk.Value;
                    x.Prod_TransitionItemInventory.Mkt_BOMFk = followup.BOMID;
                    x.Prod_TransitionItemInventory.Quantity = followup.Prod_TransitionItemInventory.Quantity + followup.Prod_PlanReferenceProductionSectionFollowup.Quantity;
                    x.Prod_TransitionItemInventory.ItemName = followup.Prod_TransitionItemInventory.ItemName;
                    x.Prod_TransitionItemInventory.FirstCreatedBy = followup.Prod_PlanReferenceProductionSectionFollowup.FirstCreatedBy;
                    x.Prod_TransitionItemInventory.LastEditeddBy = followup.Prod_PlanReferenceProductionSectionFollowup.LastEditeddBy;
                    await Task.Run(() => x.Edit(UserID));
                }


                return RedirectToAction("VmProd_PlanReferenceProductionSectionFollowupIndex", "Production", new { @id = followup.Prod_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFk });
            }
            return View(followup);
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionFollowupEdit(int FollowupId, int orderProcessId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceProductionSectionFollowup = new VmProd_PlanReferenceProductionSectionFollowup();
            await Task.Run(() => VmProd_PlanReferenceProductionSectionFollowup.SelectSingle(FollowupId.ToString()));
            VmProd_PlanReferenceProductionSectionFollowup.OrderProcessID = orderProcessId;
            if (VmProd_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFollowup == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_PlanReferenceProductionSectionFollowup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanReferenceProductionSectionFollowupEdit(VmProd_PlanReferenceProductionSectionFollowup VmProd_PlanReferenceProductionSectionFollowup)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_PlanReferenceProductionSectionFollowup.Edit(UserID));
                return RedirectToAction("VmProd_PlanReferenceProductionSectionFollowupIndex", "Production", new { @id = VmProd_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFollowup.Prod_PlanReferenceProductionSectionFk });
            }
            return View();
        }
        
        [HttpGet]
        public async Task<ActionResult> VmProd_ProductionMasterPlanStepIndex(int id)

        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == 1)
            {
                ViewBag.Name = "Cutting List";
            }
            else if (id == 2)
            {
                ViewBag.Name = "Sewing List";
            }

            VmProd_PlanReferenceProduction = new VmProd_PlanReferenceProduction();
            await Task.Run(() => VmProd_PlanReferenceProduction.InitialDataLoadForMasterPlanStep(id));

            return View(VmProd_PlanReferenceProduction);
        }
        
        #endregion

        #region LineChief

        public async Task<ActionResult> VmProd_LineChiefIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_LineChief = new VmProd_LineChief();
            await Task.Run(() => VmProd_LineChief.InitialDataLoad());
            return View(VmProd_LineChief);
        }

        public ActionResult VmProd_LineChiefCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_LineChiefCreate(VmProd_LineChief VmProd_LineChief)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_LineChief.Add(UserID));
                return RedirectToAction("VmProd_LineChiefIndex");
            }
            return RedirectToAction("VmProd_LineChiefIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_LineChiefEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmProd_LineChief = new VmProd_LineChief();
            await Task.Run(() => VmProd_LineChief.SelectSingle(id));

            if (VmProd_LineChief.Prod_LineChief == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_LineChief);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_LineChiefEdit(VmProd_LineChief VmProd_LineChief)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_LineChief.Edit(0));
                return RedirectToAction("VmProd_LineChiefIndex");
            }
            return View(VmProd_LineChief);
        }
        public ActionResult VmProd_LineChiefDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_LineChief lineChief = new Prod_LineChief();
            lineChief.ID = id;
            lineChief.Delete(0);
            return RedirectToAction("VmProd_LineChiefIndex");
        }

        #endregion

        #region LineChiefLine

        public async Task<ActionResult> VmProd_LineChiefLineIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_LineOfficer = new VmProd_LineOfficer();
            await Task.Run(() => VmProd_LineOfficer = VmProd_LineOfficer.SelectSingleChiefLine(id.Value));
            await Task.Run(() => VmProd_LineOfficer.InitialDataLoad(id.Value));
            
            return View(VmProd_LineOfficer);
        }

        [HttpGet]
        public ActionResult VmProd_LineChiefLineCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProd_AllLineDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.LineList = sl1;

            VmForDropDown.DDownData = VmForDropDown.GetProd_SVDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.SupervisorList = sl2;

            VmProd_LineOfficer = new VmProd_LineOfficer();
            VmProd_LineOfficer.Prod_LineOfficer = new Prod_LineOfficer();
            VmProd_LineOfficer.Prod_LineOfficer.Prod_LineChiefFK = id;
            return View(VmProd_LineOfficer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_LineChiefLineCreate(VmProd_LineOfficer model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            if (ModelState.IsValid)
            {
                await Task.Run(() => model.Add(UserID));
                return RedirectToAction("VmProd_LineChiefLineIndex/" + model.Prod_LineOfficer.Prod_LineChiefFK);
            }
            return RedirectToAction("VmProd_LineChiefLineIndex/" + model.Prod_LineOfficer.Prod_LineChiefFK);
        }
        
        public async Task<ActionResult> VmProd_LineChiefLineDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deletID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("z")), out deletID);
            int ChiefID = 0;
            id = id.Replace(deletID.ToString() + "z", "");
            int.TryParse(id,out ChiefID);

            VmProd_LineOfficer model = new VmProd_LineOfficer();
            await Task.Run(() => model.DeleteAll(deletID,UserID));
            
            return RedirectToAction("VmProd_LineChiefLineIndex/" + ChiefID);
        }

        #endregion

        #region LineSuperVisor
        public async Task<ActionResult> VmProd_SVNameIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_SVName = new VmProd_SVName();
            await Task.Run(() => VmProd_SVName.InitialDataLoad());
            return View(VmProd_SVName);
        }

        public ActionResult VmProd_SVNameCreate()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_SVNameCreate(VmProd_SVName VmProd_SVName)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_SVName.Add(UserID));
                return RedirectToAction("VmProd_SVNameIndex");
            }

            return RedirectToAction("VmProd_SVNameIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_SVNameEdit(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmProd_SVName = new VmProd_SVName();
            await Task.Run(() => VmProd_SVName.SelectSingle(id));

            if (VmProd_SVName.Prod_SVName == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_SVName);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_SVNameEdit(VmProd_SVName VmProd_SVName)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_SVName.Edit(0));
                return RedirectToAction("VmProd_SVNameIndex");
            }
            return View(VmProd_SVName);
        }

        public ActionResult VmProd_SVNameDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_SVName b = new Prod_SVName();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmProd_SVNameIndex");
        }

        #endregion

        #region SMVLayout

        public async Task<ActionResult> VmProd_SMVLayoutIndex()
        {
            VmProd_SMVLayout = new VmProd_SMVLayout();
            await Task.Run(() => VmProd_SMVLayout.InitialDataLoad());
            return View(VmProd_SMVLayout);
        }

        [HttpGet]
        public ActionResult VmProd_SMVLayoutCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCuttingOrderIdDropDown();
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_SMVLayoutCreate(VmProd_SMVLayout model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            if (ModelState.IsValid)
            {
                await Task.Run(() => model.Add(UserID));
            }
            return RedirectToAction("VmProd_SMVLayoutIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmProd_SMVLayoutEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetCuttingOrderIdDropDown();
            ViewBag.Order = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_SMVLayout = new VmProd_SMVLayout();
            await Task.Run(() => VmProd_SMVLayout.SelectSingle(id));
            return View(VmProd_SMVLayout);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_SMVLayoutEdit(VmProd_SMVLayout model)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            if (ModelState.IsValid)
            {
                await Task.Run(() => model.Edit(UserID));
            }
            return RedirectToAction("VmProd_SMVLayoutIndex");
        }

        public ActionResult VmProd_SMVLayoutDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_SMVLayout b = new Prod_SMVLayout();
            b.ID = id;
            b.Delete(UserID);
            return RedirectToAction("VmProd_SMVLayoutIndex");
        }

        #endregion
        
        #region Planning

        #region OrderPlan
        [HttpGet]
        public async Task<ActionResult> VmProd_PlanningIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_OrderPlanning = new VmProd_OrderPlanning();
            await Task.Run(() => VmProd_OrderPlanning.InitialDataLoad());

            return View(VmProd_OrderPlanning);
        }

        [HttpGet]
        public ActionResult VmProd_OrderPlanningCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            ViewBag.MktBomCIDStyle = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            //VmForDropDown.DDownData = VmForDropDown.GetProd_LineDropDown();
            //ViewBag.LineList = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            //VmProd_OrderPlanning = new VmProd_OrderPlanning();
            //VmProd_OrderPlanning.Prod_OrderPlanning = new Prod_OrderPlanning();
            //VmProd_OrderPlanning.Prod_OrderPlanning.ShipmentDate = DateTime.Now;
            //VmProd_OrderPlanning.Prod_OrderPlanning.ShipmentDate = DateTime.Now;
            //VmProd_OrderPlanning.Prod_OrderPlanning.ShipmentDate = DateTime.Now;
            //VmProd_OrderPlanning.Prod_OrderPlanning.ShipmentDate = DateTime.Now;
            //VmProd_OrderPlanning.Prod_OrderPlanning.ShipmentDate = DateTime.Now;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_OrderPlanningCreate(VmProd_OrderPlanning VmProd_OrderPlanning)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_OrderPlanning.Add(UserID));
                return RedirectToAction("VmProd_PlanningIndex", "Production");
            }
            return View(VmProd_OrderPlanning);
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_OrderPlanningEdit(int pid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            ViewBag.MktBomCIDStyle = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_OrderPlanning = new VmProd_OrderPlanning();
            await Task.Run(() => VmProd_OrderPlanning.SelectSingle(pid));
            if (VmProd_OrderPlanning.Prod_OrderPlanning == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_OrderPlanning);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_OrderPlanningEdit(VmProd_OrderPlanning VmProd_OrderPlanning)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_OrderPlanning.Edit(0));
                //return RedirectToAction("VmProd_PlanningIndex", "Production", new { id = VmProd_OrderPlanning.Prod_OrderPlanning.Mkt_BOMFK });
            }
            return RedirectToAction("VmProd_PlanningIndex", "Production");
        }

        #endregion

        [HttpGet]
        public async Task<ActionResult> VmProd_PlanningOrderIndex(int Id, int BomId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Planning = new VmProd_Planning();
            await Task.Run(() => VmProd_Planning = VmProd_Planning.SelectSingleJoined(Id));
            await Task.Run(() => VmProd_Planning.InitialDataLoad(Id,BomId));
            VmProd_Planning.BOMID = BomId;
            VmProd_Planning.Prod_PlanOrderId = Id;
            return View(VmProd_Planning);
        }

        [HttpGet]
        public ActionResult VmProd_PlanningCreate(int Id, int BomId, decimal PId)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            ViewBag.MktBomCIDStyle = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmForDropDown.DDownData = VmForDropDown.GetProd_LineDropDown();
            ViewBag.LineList = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmProd_Planning = new VmProd_Planning();
            VmProd_Planning.Prod_Planning = new Prod_Planning();
            VmProd_Planning.OrderQuantityPC = PId;
            VmProd_Planning.Prod_PlanOrderId = Id;
            VmProd_Planning.BOMID = BomId;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanningCreate(VmProd_Planning VmProd_Planning)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            
            VmControllerHelper = new VmControllerHelper();
            UserID = VmControllerHelper.GetCurrentUser().User_User.ID;
            VmProd_Planning.Prod_Planning.FirstCreatedBy = UserID;
            VmProd_Planning.Prod_Planning.Prod_OrderPlanningFK = VmProd_Planning.Id;
            VmProd_Planning.Prod_Planning.Mkt_BOMFK = VmProd_Planning.BOMID;
            await Task.Run(() => VmProd_Planning.GetLastBalanceQty(VmProd_Planning.Prod_Planning.Mkt_BOMFK));
            VmProd_Planning.Prod_Planning.TargetDays = (VmProd_Planning.Prod_Planning.FinishDate - VmProd_Planning.Prod_Planning.StartDate).Days + 1;
            VmProd_Planning.Prod_Planning.DayOutputQty = VmProd_Planning.Prod_Planning.Capacity * VmProd_Planning.Prod_Planning.WorkingHour;
            VmProd_Planning.Prod_Planning.TargetOutputQty = VmProd_Planning.Prod_Planning.DayOutputQty * VmProd_Planning.Prod_Planning.TargetDays;
            if (VmProd_Planning.LineAdd)
            {
                VmProd_Planning.Prod_Planning.RemainQty = VmProd_Planning.TempRemainQty - VmProd_Planning.Prod_Planning.TargetOutputQty;
            }
            else
            {
                VmProd_Planning.Prod_Planning.RemainQty = VmProd_Planning.OrderQuantityPC - VmProd_Planning.Prod_Planning.TargetOutputQty;
            }
            await Task.Run(() => VmProd_Planning.CheckAndUpdatePlanAchivement(VmProd_Planning.Prod_Planning));
            await Task.Run(() => VmProd_Planning.Add(UserID));

            return RedirectToAction("VmProd_PlanningOrderIndex", "Production", new { Id = VmProd_Planning.Prod_Planning.Prod_OrderPlanningFK, BomId = VmProd_Planning.Prod_Planning.Mkt_BOMFK });
            
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_PlanningEdit(int pid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetOrderIdDropDown();
            ViewBag.MktBomCIDStyle = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmForDropDown.DDownData = VmForDropDown.GetProd_LineDropDown();
            ViewBag.LineList = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_Planning = new VmProd_Planning();
            await Task.Run(() => VmProd_Planning.SelectSingle(pid));

            return View(VmProd_Planning);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_PlanningEdit(VmProd_Planning VmProd_Planning)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            await Task.Run(() => VmProd_Planning.GetUpdate(VmProd_Planning)); 
            return RedirectToAction("VmProd_PlanningOrderIndex", "Production", new {Id= VmProd_Planning.Prod_Planning.Prod_OrderPlanningFK, BomId = VmProd_Planning.Prod_Planning.Mkt_BOMFK });          
        }
        
        public ActionResult VmProd_PlanningDelete(int pid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Planning = new VmProd_Planning();
            VmProd_Planning.Prod_Planning = new Prod_Planning();
            VmProd_Planning.Prod_Planning.ID = pid;
            VmProd_Planning.Delete(UserID);
            return RedirectToAction("VmProd_PlanningOrderIndex", "Production");
        }

        public async Task<ActionResult> VmProd_PlanningPause(int pid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Planning = new VmProd_Planning();
            await Task.Run(() => VmProd_Planning.LinePauseRun(pid,false));
            return RedirectToAction("VmProd_PlanningOrderIndex", "Production", new { Id = VmProd_Planning.Prod_Planning.Prod_OrderPlanningFK, BomId = VmProd_Planning.Prod_Planning.Mkt_BOMFK });
        }

        public async Task<ActionResult> VmProd_PlanningPlay(int pid)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Planning = new VmProd_Planning();
            await Task.Run(() => VmProd_Planning.LinePauseRun(pid, true));
            return RedirectToAction("VmProd_PlanningOrderIndex", "Production", new { Id = VmProd_Planning.Prod_Planning.Prod_OrderPlanningFK, BomId = VmProd_Planning.Prod_Planning.Mkt_BOMFK });
        }
        
        [HttpGet]
        public ActionResult PlanReportFilterView()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> PlanReportFilterView(VmPlanReport model)
        {
            VmPlanReport = new VmPlanReport();
            await Task.Run(() => VmPlanReport.GetPlanReport(model));
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/CrpPlanReport.rpt");
            cr.Load();
            cr.SetDataSource(VmPlanReport.ReportDoc);
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        [HttpGet]
        public ActionResult ShipmentReportView()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetReportTypeList();
            ViewBag.ReportType = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            ViewBag.BuyerList = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            //VmForDropDown = new VmForDropDown();
            //VmForDropDown.DDownData = VmForDropDown.GetCommon_TheOrderRunningForDropDown();
            //ViewBag.OrderCID = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmPlanReport = new VmPlanReport();
            VmPlanReport.FromDate =  DateTime.Today.AddMonths(-1);
            VmPlanReport.ToDate = DateTime.Today;
            return View(VmPlanReport);
        }

        [HttpPost]
        public async Task<ActionResult> ShipmentReportView(VmPlanReport model)
        {
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetReportTypeList();
            ViewBag.ReportType = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            ViewBag.BuyerList = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            if (model.Report == 1)
            {
                //SummaryReport
                await Task.Run(() => model.GetShipmentDetailsReport(model));
                //return View("ShipmentPOReport", model);
            }
            else if (model.Report == 2)
            {
                //DetailsReport
                await Task.Run(() => model.GetShipmentDetailsReport(model));
                //return View("ShipmentPOReport", model);
            }
            //else if(model.Report == 2)
            //{
            //    //PreodicDetailsReport
            //    //ReportClass cr = new ReportClass();
            //    await Task.Run(() => model.GetShipmentDetailsReport(model));
            //    //cr.FileName = Server.MapPath("~/Views/Reports/CrpShipmentDetailsReport.rpt");
            //    //cr.Load();
            //    //cr.SetDataSource(model.ReportDoc);
            //    //Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            //    //return File(stream, "application/pdf");
            //}
            return View(model);
        }

        [HttpGet]
        public ActionResult LinePlanSchedule()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            ViewData["ProductionData"] = null;

            return View();
        }

        [HttpPost]
        public ActionResult LinePlanSchedule(VmPlanReport model)
        {
            //Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            //if (user == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            //await Task.Run(() => model.GetLinePlan(model));
            //return View("PlanSchedule", model);
            ViewData["ProductionData"] = model.GetLinePlan1(model);
            //return View("LinePlan", model);
            return View(model);
        }

        public async Task<ActionResult> GetOrderPlanDetails(string Id)
        {
            VmPlanReport = new VmPlanReport();
            await Task.Run(() => VmPlanReport.GetOrderPlanDetails(Id));
            //var jsonData = new
            //{
            //    data = VmPlanReport,
            //    result = 1
            //};

            return Json(VmPlanReport, JsonRequestBehavior.AllowGet);
        }

        #endregion
        
        public async Task<ActionResult> VmPlan_OrderProcessIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmPlan_OrderProcess = new VmPlan_OrderProcess();
            await Task.Run(() => VmPlan_OrderProcess.InitialDataLoad());
            return View(VmPlan_OrderProcess);
        }

        public ActionResult VmPlan_OrderProcessCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetPlan_OrderProcessCategory();
            SelectList s6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PlanCategory = s6;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_OrderProcessCreate(VmPlan_OrderProcess VmPlan_OrderProcess)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmPlan_OrderProcess.Add(UserID));
                return RedirectToAction("VmPlan_OrderProcessIndex");
            }
            return RedirectToAction("VmPlan_OrderProcessIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmPlan_OrderProcessEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetPlan_OrderProcessCategory();
            SelectList s6 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.PlanCategory = s6;
            VmPlan_OrderProcess = new VmPlan_OrderProcess();
            await Task.Run(() => VmPlan_OrderProcess.SelectSingle(id));

            if (VmPlan_OrderProcess.Plan_OrderProcess == null)
            {
                return HttpNotFound();
            }
            return View(VmPlan_OrderProcess);


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_OrderProcessEdit(VmPlan_OrderProcess VmPlan_OrderProcess)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmPlan_OrderProcess.Edit(0));
                return RedirectToAction("VmPlan_OrderProcessIndex");
            }
            return View(VmPlan_OrderProcess);
        }

        public ActionResult VmPlan_OrderProcessDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Plan_OrderProcess prodStep = new Plan_OrderProcess();
            prodStep.ID = id;
            prodStep.Delete(0);
            return RedirectToAction("VmPlan_OrderProcessIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmPlan_OrderProcesss()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderProcess = new VmPlan_OrderProcess();
            await Task.Run(() => VmPlan_OrderProcess.ProductionStepDataLoad());

            return View(VmPlan_OrderProcess);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_OrderProcesss(VmPlan_OrderProcess x)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            await Task.Run(() => x.GetSelectedStep(UserID));
            return RedirectToAction("VmPlan_OrderProcessSlaveIndex");
        }

        public async Task<ActionResult> VmPlan_OrderProcessSlaveIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmPlan_OrderProcess = new VmPlan_OrderProcess();
            await Task.Run(() => VmPlan_OrderProcess.TodaysPlanStepDataLoad());
            return View(VmPlan_OrderProcess);
        }

        public ActionResult VmPlan_OrderProcessSlaveDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_DailyProductionStepSlave b = new Prod_DailyProductionStepSlave();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmPlan_OrderProcessSlaveIndex");
        }
        
        public async Task<ActionResult> RequisitonSend()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProc_Requisition = new VmProc_Requisition();
            await Task.Run(() => VmProc_Requisition.InitialDataLoadForSpecificDepartmentRS(Convert.ToInt32(user.User_User.User_DepartmentFK)));
            return View(VmProc_Requisition);
        }

        [HttpGet]
        public async Task<ActionResult> ProductionStatusReport(int bomID, int orderprocessId)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_PlanReference = new VmProd_PlanReference();
            await Task.Run(() => VmProd_PlanReference.GetProductionFollowUpByStyleAndOrderProcess(bomID, orderprocessId));

            await Task.Run(() => VmProd_PlanReference.GetProductionSendRequisitionByStyle(bomID));
            return View(VmProd_PlanReference);
        }

        public async Task<ActionResult> RequisitonRecieved()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProc_Requisition = new VmProc_Requisition();
            await Task.Run(() => VmProc_Requisition.InitialDataLoadForSpecificDepartmentRR(Convert.ToInt32(user.User_User.User_DepartmentFK)));
            return View(VmProc_Requisition);
        }

        public async Task<ActionResult> Raw_InternaleTransferSlaveView()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmStore_InternalTransferSlave = new VmStore_InternalTransferSlave();
            await Task.Run(() => VmStore_InternalTransferSlave.GetRaw_InternaleTransferSlave());

            await Task.Run(() => VmStore_InternalTransferSlave.GetProd_Consumption());
            return View(VmStore_InternalTransferSlave);
        }

        public async Task<ActionResult> ProductionReport(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReference = new VmProd_PlanReference();
            await Task.Run(() => VmProd_PlanReference.GetFollowUpByStyle(id));

            await Task.Run(() => VmProd_PlanReference.GetSendRequisitionByStyle(id));
            return View(VmProd_PlanReference);
        }
        
        //...............Plan_ProductionLine.................//

        public ActionResult VmmProd_ConsumptionCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Consumption = new VmProd_Consumption();
            VmProd_Consumption.Prod_Consumption = new Prod_Consumption();
            VmProd_Consumption.Prod_Consumption.Raw_InternaleTransferSlaveFk = id;
            return View(VmProd_Consumption);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmmProd_ConsumptionCreate(VmProd_Consumption p)
        {
            if (ModelState.IsValid)
            {

                await Task.Run(() => p.Add(UserID));
                return RedirectToAction("Raw_InternaleTransferSlaveView");
            }

            return RedirectToAction("Raw_InternaleTransferSlaveView");
        }

        [HttpGet]
        public async Task<ActionResult> VmmProd_ConsumptionEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            VmProd_Consumption = new VmProd_Consumption();
            await Task.Run(() => VmProd_Consumption.SelectSingle(id.Value));

            if (VmProd_Consumption.Prod_Consumption == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_Consumption);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmmProd_ConsumptionEdit(VmProd_Consumption VmProd_Consumption)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_Consumption.Edit(0));
                return RedirectToAction("Raw_InternaleTransferSlaveView");
            }
            return View(VmProd_Consumption);
        }
        public ActionResult VmmProd_ConsumptionDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Consumption b = new VmProd_Consumption();
            b.Prod_Consumption = new Prod_Consumption();
            b.Prod_Consumption.ID = id;
            b.Delete(0);
            return RedirectToAction("Raw_InternaleTransferSlaveView");
        }
        public async Task<ActionResult> VmPlan_ProductionLineIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }


            VmPlan_ProductionLine = new VmPlan_ProductionLine();
            await Task.Run(() => VmPlan_ProductionLine.InitialDataLoad());
            return View(VmPlan_ProductionLine);
        }
        public ActionResult VmPlan_ProductionLineCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPlanOrderProcess();
            ViewBag.PlanOrderProcess = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            return View(VmPlan_ProductionLine);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_ProductionLineCreate(VmPlan_ProductionLine p)
        {
            if (ModelState.IsValid)
            {

                await Task.Run(() => p.Add(UserID));
                return RedirectToAction("VmPlan_ProductionLineIndex");
            }

            return RedirectToAction("VmPlan_ProductionLineIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmPlan_ProductionLineEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPlanOrderProcess();
            ViewBag.PlanOrderProcess = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmPlan_ProductionLine = new VmPlan_ProductionLine();
            await Task.Run(() => VmPlan_ProductionLine.SelectSingle(id));

            if (VmPlan_ProductionLine.Plan_ProductionLine == null)
            {
                return HttpNotFound();
            }
            return View(VmPlan_ProductionLine);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmPlan_ProductionLineEdit(VmPlan_ProductionLine VmPlan_ProductionLine)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmPlan_ProductionLine.Edit(0));
                return RedirectToAction("VmPlan_ProductionLineIndex");
            }
            return View(VmPlan_ProductionLine);
        }

        public ActionResult VmPlan_ProductionLineDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Plan_ProductionLine b = new Plan_ProductionLine();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmPlan_ProductionLineIndex");
        }
        
        //........    VmProd_MachineType.../////////
        public async Task<ActionResult> VmProd_MachineTypeIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_MachineType = new VmProd_MachineType();
            await Task.Run(() => VmProd_MachineType.InitialDataLoad());
            return View(VmProd_MachineType);
        }

        public ActionResult VmProd_MachineTypeCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_MachineType = new VmProd_MachineType();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_MachineTypeCreate(VmProd_MachineType a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));

                return RedirectToAction("VmProd_MachineTypeIndex");
            }
            return RedirectToAction("VmProd_MachineTypeIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_MachineTypeEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmProd_MachineType = new VmProd_MachineType();
            await Task.Run(() => VmProd_MachineType.SelectSingle(id.Value));

            if (VmProd_MachineType.Prod_MachineType == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_MachineType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_MachineTypeEdit(VmProd_MachineType m)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (ModelState.IsValid)
            {
                await Task.Run(() => m.Edit(0));

                return RedirectToAction("VmProd_MachineTypeIndex");
            }
            return RedirectToAction("VmProd_MachineTypeIndex");
        }

        public ActionResult VmProd_MachineTypeDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_MachineType b = new Prod_MachineType();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmProd_MachineTypeIndex");
        }
        
        //.........  VmProd_Machine.................//
        
        public async Task<ActionResult> VmProd_MachineIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Machine = new VmProd_Machine();
            await Task.Run(() => VmProd_Machine.InitialDataLoad());
            return View(VmProd_Machine);
        }

        public ActionResult VmProd_MachineCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }


            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.Prod_MachineTypeDropDown();
            ViewBag.MachineType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_Machine = new VmProd_Machine();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_MachineCreate(VmProd_Machine a)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));

                return RedirectToAction("VmProd_MachineIndex");
            }
            return RedirectToAction("VmProd_MachineIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_MachineEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.Prod_MachineTypeDropDown();
            ViewBag.MachineType = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmProd_Machine = new VmProd_Machine();
            await Task.Run(() => VmProd_Machine.SelectSingle(id.Value));

            if (VmProd_Machine.Prod_Machine == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_Machine);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_MachineEdit(VmProd_Machine m)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (ModelState.IsValid)
            {
                await Task.Run(() => m.Edit(0));

                return RedirectToAction("VmProd_MachineIndex");
            }
            return RedirectToAction("VmProd_MachineIndex");
        }

        public ActionResult VmProd_MachineDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_Machine b = new Prod_Machine();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmProd_MachineIndex");
        }
        
        //.............VmProd_MachineLine....//
        public async Task<ActionResult> VmProd_MachineLineIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_MachineLine = new VmProd_MachineLine();
            await Task.Run(() => VmProd_MachineLine = VmProd_MachineLine.SelectSingleOrderLine(id.Value));
            await Task.Run(() => VmProd_MachineLine.InitialDataLoad(id.Value));


            return View(VmProd_MachineLine);
        }

        [HttpGet]
        public ActionResult VmProd_MachineLineCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProd_MachineDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Machine = sl2;

            VmProd_MachineLine = new VmProd_MachineLine();
            VmProd_MachineLine.Prod_MachineLine = new Prod_MachineLine();
            VmProd_MachineLine.Prod_MachineLine.Plan_ProductionLineFK = id;

            return View(VmProd_MachineLine);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_MachineLineCreate(VmProd_MachineLine m)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {

                await Task.Run(() => m.Add(UserID));
                return RedirectToAction("VmProd_MachineLineIndex/" + m.Prod_MachineLine.Plan_ProductionLineFK);
            }
            return RedirectToAction("VmProd_MachineLineIndex/" + m.Prod_MachineLine.Plan_ProductionLineFK);
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_MachineLineEdit(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetProd_MachineDropDown();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Machine = sl2;



            VmProd_MachineLine = new VmProd_MachineLine();
            await Task.Run(() => VmProd_MachineLine.SelectSingle(id.Value));
            if (VmProd_MachineLine.Prod_MachineLine == null)
            {
                return HttpNotFound();
            }
            return View(VmProd_MachineLine);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_MachineLineEdit(VmProd_MachineLine m)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => m.Edit(0));
                return RedirectToAction("VmProd_MachineLineIndex/" + m.Prod_MachineLine.Plan_ProductionLineFK);
            }
            return View(VmProd_MachineLine);
        }
        
        public ActionResult VmProd_MachineLineDelete(string id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int deletID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("z")), out deletID);

            id = id.Replace(deletID.ToString() + "z", "");
            Prod_MachineLine b = new Prod_MachineLine();
            b.ID = deletID;
            b.Delete(0);
            return RedirectToAction("VmProd_MachineLineIndex/" + id);
        }
        
        public async Task<ActionResult> VmProd_DailyProductionPlanIndex(int rid, int bomId)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            // if User null user redirect to Login page.
            VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();
            VmProd_PlanReferenceProductionSection.BOMID = bomId;
            await Task.Run(() => VmProd_PlanReferenceProductionSection.DailyTargetDataLoad(rid, bomId));

            return View(VmProd_PlanReferenceProductionSection);
        }

        public async Task<ActionResult> VmProd_PreviousProductionPlanIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            // if User null user redirect to Login page.

            VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();

            await Task.Run(() => VmProd_PlanReferenceProductionSection.PreviousTargetDataLoad());
            return View(VmProd_PlanReferenceProductionSection);
        }
        
        public async Task<ActionResult> VmProd_CapacityIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Capacity = new VmProd_Capacity();
            VmProd_Capacity.Prod_Capacity = new Prod_Capacity();
            await Task.Run(() => VmProd_Capacity.InitialDataLoad());
           
            return View(VmProd_Capacity);
        }

        public ActionResult VmProd_CapacityCreate()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetPlanOrderProcess();
            SelectList sl = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.OrderProcess = sl;
            VmForDropDown.DDownData = VmForDropDown.GetMonths();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Months = sl1;
            VmForDropDown.DDownData = VmForDropDown.GetYears();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Years = sl2;
            VmProd_Capacity = new VmProd_Capacity();
            VmProd_Capacity.Prod_Capacity = new Prod_Capacity();
            VmProd_Capacity.Prod_Capacity.Month = DateTime.Now.Month;
            VmProd_Capacity.Prod_Capacity.Year = DateTime.Now.Year.ToString();
            VmProd_Capacity.Prod_Capacity.TotalLineInFactory = VmProd_Capacity.GetTotalLineInFactory(32);
            VmProd_Capacity.Prod_Capacity.TotalWorkingDays = 26;
            VmProd_Capacity.Prod_Capacity.PerDayWorkingHour = 11;
            return View(VmProd_Capacity);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_CapacityCreate(VmProd_Capacity VmProd_Capacity)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_Capacity.Add(UserID));
                return RedirectToAction("VmProd_CapacityIndex");
            }
            return RedirectToAction("VmProd_CapacityIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmProd_CapacityEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmProd_Capacity = new VmProd_Capacity();
            await Task.Run(() => VmProd_Capacity.SelectSingle(id));

            if (VmProd_Capacity.Prod_Capacity == null)
            {
                return HttpNotFound();
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetMonths();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Months = sl1;
            VmForDropDown.DDownData = VmForDropDown.GetYears();
            SelectList sl2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Years = sl2;
            return View(VmProd_Capacity);


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_CapacityEdit(VmProd_Capacity VmProd_Capacity)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_Capacity.Edit(0));
                return RedirectToAction("VmProd_CapacityIndex");
            }
            return View(VmProd_Capacity);
        }

        public ActionResult VmProd_CapacityDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_Capacity capacity = new Prod_Capacity();
            capacity.ID = id;
            capacity.Delete(0);
            return RedirectToAction("VmProd_CapacityIndex");
        }
        
        [HttpGet]
        public ActionResult VmProd_TransitionItemInventoryCreate()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetRawItemForDropDown();
            ViewBag.RawItem = new SelectList(VmForDropDown.DDownData, "Value", "Text");            

            VmForDropDown.DDownData = VmForDropDown.GetUnitForDropDown();
            ViewBag.Unit = new SelectList(VmForDropDown.DDownData, "Value", "Text");            

            VmForDropDown.DDownData = VmForDropDown.GetAllRawCategory();
            ViewBag.Raw_Category = new SelectList(VmForDropDown.DDownData, "Value", "Text");
             
            VmForDropDown.DDownData = VmForDropDown.GetAllOrderAndStyle();
            ViewBag.OrderAndStyle = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetProdMachineDIA();
            ViewBag.MachineDIA = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmProd_TransitionItemInventory vmTansitionItemInventory = new VmProd_TransitionItemInventory();
            vmTansitionItemInventory.Prod_TransitionItemInventory = new Prod_TransitionItemInventory();
            vmTansitionItemInventory.Prod_TransitionItemInventory.Date = DateTime.Now;
            
            return View(vmTansitionItemInventory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_TransitionItemInventoryCreate(VmProd_TransitionItemInventory VmProd_TransitionItemInventory)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_TransitionItemInventory.Prod_TransitionItemInventory.Prod_PlanReferenceProductionSectionFk = 1;

            //if (ModelState.IsValid)
            //{
            await Task.Run(() => VmProd_TransitionItemInventory.Add(UserID));
            //}
            return RedirectToAction("VmProd_TransitionItemInventoryIndex");
        }


        [HttpGet]
        public async Task<ActionResult> VmProd_TransitionItemInventoryEdit(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetBOMForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOMFK = sl1;

            VmProd_TransitionItemInventory = new VmProd_TransitionItemInventory();
            await Task.Run(() => VmProd_TransitionItemInventory.SelectSingle(id));

            return View(VmProd_TransitionItemInventory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_TransitionItemInventoryEdit(VmProd_TransitionItemInventory VmProd_TransitionItemInventory)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid)
            {
                await Task.Run(() => VmProd_TransitionItemInventory.Edit(UserID));
            }
            return RedirectToAction("VmProd_TransitionItemInventoryIndex");
        }

        [HttpGet]
        public ActionResult VmProd_TransitionItemInventoryDelete(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_TransitionItemInventory b = new Prod_TransitionItemInventory();
            b.ID = id;
            b.Delete(UserID);
            return RedirectToAction("VmProd_TransitionItemInventoryIndex");
        }

        public async Task<ActionResult> VmProd_TransitionItemMadeFromIndex(int? id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_TransitionItemMadeFrom = new VmProd_TransitionItemMadeFrom();
            VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom = new Prod_TransitionItemMadeFrom();
            VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom.Prod_TransitionItemInventoryFK = id.Value;
            await Task.Run(() => VmProd_TransitionItemMadeFrom.InitialDataLoad(id.Value));

            VmProd_TransitionItemInventory = new VmProd_TransitionItemInventory();
            await Task.Run(() => VmProd_TransitionItemInventory.SelectSingle(id));

            VmProd_TransitionItemMadeFrom.VmProd_TransitionItemInventory = VmProd_TransitionItemInventory;

            return View(VmProd_TransitionItemMadeFrom);
        }

        public ActionResult VmProd_TransitionItemMadeFromCreate(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_TransitionItemMadeFrom = new VmProd_TransitionItemMadeFrom();
            VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom = new Prod_TransitionItemMadeFrom();
            VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom.Prod_TransitionItemInventoryFK = id;
            var a = db.Prod_TransitionItemInventory.FirstOrDefault(x => x.ID == id);
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetItemsByBOM(a.Mkt_BOMFk);
            SelectList sl = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.Item = sl;
            VmForDropDown.DDownData = VmForDropDown.GetTransitionItemsWithoutItSelf(id);
            SelectList s2 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.TransitionsItem = s2;

            VmForDropDown.DDownData = VmForDropDown.GetDropDownRawItem();
            SelectList s3 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.RawItemList = s3;

            VmForDropDown.DDownData = VmForDropDown.GetTransferFilterType();
            SelectList s4 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.TransferFilterList = s4;

            VmForDropDown.DDownData = VmForDropDown.GetAllOrderAndStyle();
            SelectList s5 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BOMList = s5;
            return View(VmProd_TransitionItemMadeFrom);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmProd_TransitionItemMadeFromCreate(VmProd_TransitionItemMadeFrom VmProd_TransitionItemMadeFrom)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (VmProd_TransitionItemMadeFrom.TransferType == 1)
            {
                VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom.Mkt_POSlaveFK = 1;
                VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom.Prod_TransitionItemInventoryFromFK = 1;

            }
            else if (VmProd_TransitionItemMadeFrom.TransferType == 2)
            {
                VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom.Raw_ItemFK = 1;
                VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom.Prod_TransitionItemInventoryFromFK = 1;

            }
            else if (VmProd_TransitionItemMadeFrom.TransferType == 3)
            {
                VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom.Raw_ItemFK = 1;
                VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom.Mkt_POSlaveFK = 1;
            }
            await Task.Run(() => VmProd_TransitionItemMadeFrom.Add(UserID));
            return RedirectToAction("VmProd_TransitionItemMadeFromIndex", new { id = VmProd_TransitionItemMadeFrom.Prod_TransitionItemMadeFrom.Prod_TransitionItemInventoryFK });
        }

        public ActionResult VmProd_TransitionItemMadeFromDelete(int id, int tID)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Prod_TransitionItemMadeFrom transitionfrom = new Prod_TransitionItemMadeFrom();
            transitionfrom.ID = id;
            transitionfrom.Delete(0);
            return RedirectToAction("VmProd_TransitionItemMadeFromIndex", new { id = tID });
        }
        
        [HttpGet]
        public async Task<ActionResult> VmPlanProductionLineRunningIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmPlan_ProductionLineRunning = new VmPlan_ProductionLineRunning();

            await Task.Run(() => VmPlan_ProductionLineRunning.InitialLoadData());
            // await Task.Run(() => VmPlan_ProductionLineRunning.InitialColorLoadData());
            return View(VmPlan_ProductionLineRunning);
        }

        public async Task<ActionResult> VmPlanProductionLineRunningCreate(string Id)
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int Plan_ProductionLineFK = 0;
            int.TryParse(Id.Substring(0, Id.LastIndexOf("X")), out Plan_ProductionLineFK);
            int TimeType = 0;
            Id = Id.Replace(Plan_ProductionLineFK.ToString() + "X", "");
            int.TryParse(Id, out TimeType);
            VmPlan_ProductionLineRunning VmPlan_ProductionLineRunning = new VmPlan_ProductionLineRunning();
            VmPlan_ProductionLineRunning.Plan_ProductionLineRunning = new Plan_ProductionLineRunning();
            VmPlan_ProductionLineRunning.Plan_ProductionLineRunning.Plan_ProductionLineFK = Plan_ProductionLineFK;
            VmPlan_ProductionLineRunning.Plan_ProductionLineRunning.TimeType = TimeType;
            await Task.Run(() => VmPlan_ProductionLineRunning.Add(0));
            return RedirectToAction("VmPlanProductionLineRunningIndex");
        }

        
        [HttpGet]
        public async Task<ActionResult> VmPlan_PartyWiseLeftOverReport()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmPlan_PartyWiseLeftOver = new VmPlan_PartyWiseLeftOver();

            await Task.Run(() => VmPlan_PartyWiseLeftOver.InitialLoadData());
            return View(VmPlan_PartyWiseLeftOver);
        }
        
        public ActionResult VmProd_MonthlyOrderMc()
        {
            var user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetBuyerForDropDown();
            SelectList sl1 = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.BuyerName = sl1;

            VmProd_MonthlyOrderMc vmProdMonthlyOrderMc = new VmProd_MonthlyOrderMc();
            vmProdMonthlyOrderMc.IndialDataLoad();
            return View(vmProdMonthlyOrderMc);
        }
        
        [HttpGet]
        public async Task<ActionResult> VmPlan_OrderLayoutIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_Planning = new VmProd_Planning();
            await Task.Run(() => VmProd_Planning.LayoutInitialDataLoad());

            return View(VmProd_Planning);
        }

        public List<object> GetTempBOMCID()
        {
            var List = new List<object>();
            var v = (from t1 in db.Common_TheOrder
                     join t2 in db.Mkt_BOM
                     on t1.ID equals t2.Common_TheOrderFk
                     where t2.Active == true
                     select new
                     {
                         CID = t1.CID + "/" + t2.Style,
                         ID = t2.ID
                     }).OrderByDescending(x => x.ID);

            foreach (var cid in v)
            {
                List.Add(new { Text = cid.CID, Value = cid.ID });
            }
            return List;
        }

        public JsonResult Get_RequiredQty(int? mktbomslveId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var result = (from t1 in db.Mkt_BOMSlave
                          join t2 in db.Common_Unit on t1.Common_UnitFK equals t2.ID
                          where t1.ID == mktbomslveId && t1.Active == true
                          select new
                          {
                              Description = t1.Description,
                              RequiredQuantity = t1.RequiredQuantity,
                              UnitName = t2.Name,
                              Common_UnitFK = t2.ID
                          }).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region CloseMenu

        [HttpGet]
        public async Task<ActionResult> VmPlanProductionReportOrderWise()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmPlan_ProductionReportOrderWise = new VmPlan_ProductionReportOrderWise();

            await Task.Run(() => VmPlan_ProductionReportOrderWise.InitialLoadData());
            return View(VmPlan_ProductionReportOrderWise);
        }

        public async Task<ActionResult> VmProd_TransitionItemInventoryIndex()
        {
            VmUser_User user = (VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_TransitionItemInventory = new VmProd_TransitionItemInventory();
            await Task.Run(() => VmProd_TransitionItemInventory.InitialDataLoad());

            return View(VmProd_TransitionItemInventory);
        }
        
        public async Task<ActionResult> VmProd_PlanReferenceProductionView(int id)
        {
            Oss.Romo.ViewModels.User.VmUser_User u = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmProd_PlanReferenceProduction = new VmProd_PlanReferenceProduction();
            await Task.Run(() => VmProd_PlanReferenceProduction.GetPlanReferenceProduction(id));
            VmProd_PlanReferenceProduction.Prod_PlanReferenceProduction = new Prod_PlanReferenceProduction();

            VmProd_PlanReferenceProduction.OrderProcessID = id;

            VmProd_PlanReferenceProduction.VmProd_PlanReference = VmProd_PlanReference;

            return View(VmProd_PlanReferenceProduction);
        }


        public async Task<ActionResult> VmProd_UpcomingProductionPlanIndex()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            // if User null user redirect to Login page.

            VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();

            await Task.Run(() => VmProd_PlanReferenceProductionSection.UpcomingTargetDataLoad());
            return View(VmProd_PlanReferenceProductionSection);
        }

        public ActionResult ProductionTargetAndAchivement()
        {
            //VmProd_PlanReferenceProductionSectionFollowup = new VmProd_PlanReferenceProductionSectionFollowup();
            List<ProductionTargetAndAchivementViewModel> list = new List<ProductionTargetAndAchivementViewModel>();
            //list = await Task.Run(() => VmProd_PlanReferenceProductionSectionFollowup.GetProductionTargetAndAchivement());
            return View(list);
        }

        [HttpGet]
        public async Task<ActionResult> Vm_Prod_PlanReferenceProductionStepProgress()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            //VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();

            //await Task.Run(() => VmProd_PlanReferenceProductionSection.GetProd_ProductionStep());
            //await Task.Run(() => VmProd_PlanReferenceProductionSection.GetProd_ProductionReferenceProgressBar());

            //return View(VmProd_PlanReferenceProductionSection);
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Vm_Prod_PlanReferenceRunningOrderProgress()
        {
            Oss.Romo.ViewModels.User.VmUser_User user = (Oss.Romo.ViewModels.User.VmUser_User)Session["One"];
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmProd_PlanReferenceProductionSection = new VmProd_PlanReferenceProductionSection();

            await Task.Run(() => VmProd_PlanReferenceProductionSection.GetProd_ProdunctionRunningStep());
            //await Task.Run(() => VmProd_PlanReferenceProductionSection.GetProd_ProductionReferenceProgressBar());

            return View(VmProd_PlanReferenceProductionSection);
        }
        #endregion
    }
}