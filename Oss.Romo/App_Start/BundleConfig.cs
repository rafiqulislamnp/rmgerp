﻿using System.Web;
using System.Web.Optimization;

namespace Oss.Romo
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/Oss.Scripts.js").Include(
            //            "~/plugins/jQuery/jquery-2.2.3.min.js",
            //            "~/plugins/jQueryUI/jquery-ui-1.12.0.min.js",
            //            "~/bootstrap/js/bootstrap.min.js",
            //            "~/plugins/datatables/jquery.dataTables.min.js",
            //            "~/plugins/datatables/dataTables.bootstrap.min.js",
            //            "~/plugins/slimScroll/jquery.slimscroll.min.js",
            //            "~/plugins/fastclick/fastclick.js",
            //            "~/dist/js/app.min.js",
            //            "~/dist/js/demo.js",
            //            "~/plugins/datepicker/bootstrap-datepicker.js",
            //            "~/plugins/chartjs/Chart.min.js",
            //            "~/plugins/flot/jquery.flot.min.js",
            //            "~/plugins/flot/jquery.flot.resize.min.js",
            //            "~/plugins/flot/jquery.flot.pie.min.js",
            //            "~/plugins/flot/jquery.flot.categories.min.js"));



           

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));


            //bundles.Add(new StyleBundle("~/Content/Oss.Styles.css").Include(
            //         "~/bootstrap/css/bootstrapLine.css",
            //         "~/bootstrap/css/bootstrap.min.css",
            //         "~/dist/css/AdminLTE.min.css",
            //         "~/plugins/datatables/dataTables.bootstrap.css",
            //         "~/dist/css/skins/_all-skins.min.css",
            //         "~/plugins/datepicker/datepicker3.css",
            //         "~/Content/jquery-ui.css",
            //         "~/Content/OssStyle.css"));

           
        }
    }
}
